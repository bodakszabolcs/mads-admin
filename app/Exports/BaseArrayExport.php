<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class BaseArrayExport implements  FromArray, ShouldAutoSize, WithHeadings, WithTitle, WithEvents
{
	use Exportable;
	public $data =[];
	public $heading = [];
	public $Title = null;
	public function __construct($heading,  $title, $data)
	{
		$this->heading   = $heading;
		$this->Title     = $title;
		$this->data =$data;

	}

	public function array(): array
	{
		return $this->data;
	}
	public function headings(): array
	{
		return $this->heading;
	}
	public function title(): string
	{
		return $this->Title;
	}

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function(AfterSheet $event) {

                $event->getSheet()->getDelegate()->getStyle('A:Q')->applyFromArray(
                    array(
                        'alignment' => [
                              'horizontal' => Alignment::HORIZONTAL_CENTER,
                              'vertical' => Alignment::VERTICAL_CENTER,
                              'wrapText' => true,
                         ],
                    )
                );


            }
        ];
    }
}
