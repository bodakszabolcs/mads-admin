<?php

namespace App\Exports;


use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Modules\Fixing\Entities\FixingAmrest;
use Modules\Partner\Entities\Partner;
class AmrestExport implements FromCollection, WithHeadings, WithStrictNullComparison,ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    private $month;
    public function __construct($month)
    {
        $this->month = $month;
    }

    public function collection()
    {
        return FixingAmrest::select(DB::raw("students.tax_number as tax_number,amrest_id,students.name as student_name,fixing_amrest.position,fixing_amrest.cost_center,company_industries.name,fixing_amrest.recruiter,
         sum_hour,(sum_hour-sum_night) ,sum_night,festive,price_hourly,deficit,deduction,bonus,bonus_presence,company_price,gross_student,net,fixing_amrest.comment,If(fixing_amrest.exited=1,'Igen','Nem') as exited"))
            ->leftJoin('students','students.id','=','student_id')
            ->leftJoin('company_industries','company_industries.id','=','industry_id')
            ->where('gross_student','>',0)
            ->where('month',$this->month)->get();
    }
    public function headings(): array
    {
        return [
            'ADÓSZÁM',
            'ID',
            'NÉV',
            'Pozíció',
            'Cost Center',
            'ÜZLET',
            'Toborozta',
            'Össz óra',
            'Nappal',
            'Éjjel',
            'Ünnep',
            'Bruttó órabér',
            'Pénztárhiány',
            'Levonás',
            'Bónusz',
            'Jelenléti bónusz',
            'MADS Jutalék',
            'Bruttó bér',
            'Nettó számlaérték',
            'Megjegyzés',
            'Kilépett'
        ];
    }
}
