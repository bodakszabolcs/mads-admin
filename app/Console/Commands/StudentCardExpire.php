<?php

namespace App\Console\Commands;

use App\Jobs\SendStudentCardEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Student\Entities\Student;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class StudentCardExpire extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification expire student card';
    protected $signature = 'studentCard:email';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   $list30 = new Student();
           $list2week = new Student();
           $list1week = new Student();

        $list30 = $list30->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates where deleted_at is null group by student_id) as cert'), 'cert.student_id', '=', 'students.id')
            ->where('cert.end', '=', date('Y-m-d',strtotime('+1 month')))->get();

        $list2week = $list2week->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates where deleted_at is null group by student_id) as cert'), 'cert.student_id', '=', 'students.id')
            ->where('cert.end', '=', date('Y-m-d',strtotime('+2 week')))->get();
        $list1week = $list1week->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates where deleted_at is null group by student_id) as cert'), 'cert.student_id', '=', 'students.id')
            ->where('cert.end', '=', date('Y-m-d',strtotime('+1 week')))->get();
        foreach ($list30 as $s){

            SendStudentCardEmail::dispatch($s);
        }
        foreach ($list2week as $s){
            echo $s->name;
            SendStudentCardEmail::dispatch($s);
        }
        foreach ($list1week as $s){
            echo $s->name;
            SendStudentCardEmail::dispatch($s);
        }
    }
}
