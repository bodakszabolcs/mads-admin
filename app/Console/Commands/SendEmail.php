<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Email\Entities\Email;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\Work\Entities\Work;
use PayPal\Api\CountryCode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SendEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email';

    protected $process;


    public function handle()
    {
        try {
            $email =Email::whereNull('sent_date')->get();
            foreach ($email as $e){
                \App\Jobs\SendEmail::dispatch($e);
            }

        } catch (ProcessFailedException $exception) {

        }
    }
}
