<?php

namespace App\Console\Commands;

use App\Jobs\SendPresentsNotifiaction;
use App\Jobs\SendStudentCardEmail;
use App\Mail\SendPresentsNotification;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Company\Entities\Company;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class PresentsNotification extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification presence company';
    protected $signature = 'precence:notification';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   $list = Company::where('online_presence',1)->get();

        foreach ($list as $l){
            $user = User::where('company_id',$l->id)->get();
            foreach ($user as $u){
                SendPresentsNotifiaction::dispatch($u);
            }
        }

    }
}
