<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerateSlug extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';
    protected $signature = 'generate:workslug';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (Work::all() as $w){

                $w->slug=Str::slug($w->name.'-'.$w->id);
                $w->save();

        }
    }
}
