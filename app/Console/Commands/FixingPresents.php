<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Modules\Company\Entities\Industry;
use Modules\Fixing\Entities\Fixing;
use Modules\StudentPresent\Entities\StudentPresent;

class FixingPresents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixing:presents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate fixings from presents';

    protected $process;


    public function handle()
    {

        ini_set('memory_limit', -1);
        ini_set('max_execution_time', -1);
        $presentsByIndustry = StudentPresent::select("company_id", 'industry_id')->leftJoin('companies','companies.id','=','company_id')
            ->whereNull('finalized')
            ->where('companies.online_presence','>=',1)
            ->groupBy('industry_id')->get();
        foreach ($presentsByIndustry as $p) {

            $industry = Industry::where('id', $p->industry_id)->first();
            if($industry) {
                $presentsRows = StudentPresent::whereNull('finalized')->where('industry_id', $industry->id)->where('date','<',date('Y-m-01'))->get();

                foreach ($presentsRows as $row) {

                    $this->createFixing($industry, $row);
                    $row->finalized = 1;
                    $row->save();
                }
            }
        }

    }

    public function createFixing($industry, $presentsRow)
    {
        if($presentsRow->hour >0) {
            $fixing = Fixing::where('date', $presentsRow->date)->where('student_id', $presentsRow->student_id)->where('industry_id', $industry->id)->first();
            if (is_null($fixing)) {
                $fixing = new Fixing();
            }
            if($fixing->status !=1) {

                $fixing->status = 1;
                $fixing->month = date('Ym', strtotime($presentsRow->date));
                $fixing->date = $presentsRow->date;
                $fixing->student_id = $presentsRow->student_id;
                $fixing->company_id = $presentsRow->company_id;
                $fixing->industry_id = $presentsRow->industry_id;
                $fixing->divisor = $industry->divisor;
                $fixing->multiplier = $industry->multiplier;
                $fixing->start = $presentsRow->start;
                $fixing->end = $presentsRow->end;
                $fixing->bonus = 1;
                $fixing->break_from = $presentsRow->break_from;
                $fixing->break_to = $presentsRow->break_to;
                $fixing->price_hour = $industry->price_hour;
                $fixing->price_company_hourly = $industry->price_company_hourly;
                $fixing->fix_company_multiplier = $industry->price_company_fix;
                $fixing->price_company_hourly = $fixing->calculateCompanyHourly($industry->price_company_hourly, $industry->hourly_algorithm);
                $this->calculateSumPrice($fixing, $industry);
            }

        }
    }

    private function calculateSumPrice($fixing, $industry)
    {

        $sum = $this->calculate_diff_hour($fixing->start, $fixing->end);

        $s = null;
        $e = null;
        if ($industry->night_bonus) {
            $s = $industry->night_bonus_from;
            $e = $industry->night_bonus_to;
            $fixing->bonus_type = 1;
            $fixing->bonus_multiplier = $industry->night_bonus;
            $fixing->bonus_multiplier_company = $industry->night_bonus_company;
        }
        if (date('N', strtotime($fixing->date)) == 6 && $industry->saturday_bonus && $fixing->bonus) {
            $s = $industry->saturday_bonus_from;
            $e = $industry->saturday_bonus_to;
            $fixing->bonus_type = 2;
            $fixing->bonus_multiplier = $industry->saturday_bonus;
            $fixing->bonus_multiplier_company = $industry->saturday_bonus_company;
        }
        if (date('N', strtotime($fixing->date)) == 7 && $industry->sunday_bonus && $fixing->bonus) {
            $s = $industry->sunday_bonus_from;
            $e = $industry->sunday_bonus_to;
            $fixing->bonus_type = 3;
            $fixing->bonus_multiplier = $industry->sunday_bonus;
            $fixing->bonus_multiplier_company = $industry->sunday_bonus_company;
        }
        if( $fixing->bonus_type ==1) {
            $fixing->bonus_hour = $this->calculateBonusHour($fixing->start, $fixing->end, $s, $e);
        }
        if( $fixing->bonus_type ==2 ||  $fixing->bonus_type == 3 ||  $fixing->bonus_type ==4){
            $fixing->bonus_hour = $this->calculate_diff_hour($fixing->start, $fixing->end);
        }
        $sum -= $fixing->bonus_hour;
        // calculate szünet
        $minusBonus = 0;
        $minusSum = 0;
        if( $fixing->bonus_type ==1) {
            if ($industry->break && $fixing->break_from && $fixing->break_to) {
                $bf = $this->timeToInt($fixing->break_from);
                $bt = $this->timeToInt($fixing->break_to);
                // 10-11
                // csak akkkor kell ha éjszakai bónusz van

                if ($this->calculate_diff_hour($fixing->break_from, $fixing->break_to) > 0) {

                    if ($bf < $bt) {
                        // a szünet teljesen az éjszakaiba esik
                        if ($this->calculate_beetwen($s, $e, $fixing->break_from) && $this->calculate_beetwen($s, $e, $fixing->break_to)) {
                            $minusBonus = $this->calculate_diff_hour($fixing->break_from, $fixing->break_to);
                        }
                        // ha csak az eleje éjszakai vége nappali
                        else if ($this->calculate_beetwen($s, $e, $fixing->break_from) && !$this->calculate_beetwen($s, $e, $fixing->break_to)) {
                            $minusBonus = $this->calculate_diff_hour($fixing->break_from, $e);
                            $minusSum = $this->calculate_diff_hour($fixing->break_to, $s);

                        }

                        // ha eleje nappali vége esti
                        else if ($this->calculate_beetwen($fixing->start, $s, $fixing->break_from) && $this->calculate_beetwen($s, $e, $fixing->break_to)) {
                            $minusBonus = $this->calculate_diff_hour($s, $fixing->break_to);
                            $minusSum = $this->calculate_diff_hour($fixing->break_from, $s);
                        }
                        else if (!$this->calculate_beetwen($s, $e, $fixing->break_from) && !$this->calculate_beetwen($s, $e, $fixing->break_to)) {
                            $minusSum = $this->calculate_diff_hour($fixing->break_from, $fixing->break_to);
                        }
                    } else {

                        if ($this->calculate_beetwen($s, $e, $fixing->break_from) && $this->calculate_beetwen($s, $e, $fixing->break_to)) {
                            $minusBonus = $this->calculate_diff_hour($fixing->break_from, $fixing->break_to);
                        }
                    }
                } else {

                    if ($this->calculate_beetwen($fixing->start, $fixing->end, $fixing->break_from) && $this->calculate_beetwen($fixing->start, $fixing->end, $fixing->break_to)) {

                        $minusSum = $this->calculate_diff_hour($fixing->break_from, $fixing->break_to);
                    }
                }
            }
        }
        if( $fixing->bonus_type ==2 ||  $fixing->bonus_type == 3 ||  $fixing->bonus_type ==4){
            $minusBonus = $this->calculate_diff_hour($fixing->break_from , $fixing->break_to);
        }
        $sum -= $minusSum;
        $fixing->bonus_hour -= $minusBonus;
        $fixing->hour = $sum;
        $fixing->student_price = $fixing->calculateStudentPrice();
        $fixing->company_price = $fixing->calculateCompanyPrice();
        $fixing->save();
    }

    private function calculateBonusHour($a, $b, $s, $e)
    {
        if (!$s || !$e) return 0;
        if (!$a || !$b || strpos($a, ':') === false || strpos($b, ':') === false) return 0;

        $ma = $this->timeToInt($a);
        $mb = $this->timeToInt($b);
        $na = $this->timeToInt($s);
        $nb = $this->timeToInt($e);

        if ($this->calculate_beetwen($s, $e, $a) && $this->calculate_beetwen($s, $e, $b)) {
            return $this->calculate_diff_hour($a, $b);
        }
        if ($this->calculate_beetwen($a, $b, $s) && $this->calculate_beetwen($a, $b, $e)) {
            return $this->calculate_diff_hour($s, $e);
        }
        if ($ma < $mb && (($ma > $nb && ($ma > $na || $mb < $na)) || ($mb < $na && $mb < $nb && $ma > $nb))) {
            return 0;
        }
        if ($ma > $mb && $ma < $na && $mb > $nb) {
            return 0;
        }
        if ($ma > $nb && $na < $ma && $mb <= $nb) {
            return 0;
        }
        $start = $a;
        if ($ma < $na && $mb > $na) {
            $start = $s;
        }

        $end = $b;
        if ($mb > $nb && $ma - $mb > 0) {
            $end = $e;
        }
        if ($ma < $na && $na > $mb) {
            $end = $e;
        }
        if ($ma < $na && $mb < $nb) {
            $start = $s;
            $end = $b;
        }

        $diffNight = $this->calculate_diff_hour($start, $end);
        $diffHour = $this->calculate_diff_hour($a, $b);
        if ($diffNight > $diffHour) {
            return $diffHour;
        }
        return $diffNight;
    }

    public function calculate_beetwen($a, $b, $c)
    {
        if (!$a || !$b || !$c || strpos($a, ':') === false || strpos($b, ':') === false || strpos($c, ':') === false) {
            return 0;
        }
        $a = $this->timeToInt($a);
        $b = $this->timeToInt($b);
        $c = $this->timeToInt($c);
        if ($a === $b) {
            return false;
        }
        if ($b < $a) {
            if ($c >= $a && $c < 23 * 60 + 59) {
                return true;
                }
            if ($c <= $b && $c >= 0) {
                return true;
                }
            return false;
            } else {
            if ($a === $c && $c < $b) {
                return true;
                }
            if ($a < $c && $c === $b) {
                return true;
                }
            return ($a < $c && $b > $c);
            }
    }

    private function calculate_diff_hour($a, $b)
    {
        if (!$a || !$b || strpos($a, ':') === false || strpos($b, ':') === false) {
            return 0;
        }
        $a = $this->timeToInt($a);
        $b = $this->timeToInt($b);
        $diff = $b - $a;
        if ($diff < 0) {
            $diff += 1440;
        }
        return round(((int)(100 * $diff / 60)) / 100,2);
    }

    private function timeToInt(string $a)
    {
        return ((int)$a) * 60 + (int)explode(':', $a)[1];
    }
}
