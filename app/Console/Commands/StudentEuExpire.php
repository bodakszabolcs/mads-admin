<?php

namespace App\Console\Commands;

use App\Jobs\SendStudentCardEmail;
use App\Jobs\SendStudentEuEmail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Student\Entities\Student;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class StudentEuExpire extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notification expire EÜ';
    protected $signature = 'studentEu:email';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {   $list30 = new Student();
           $list2week = new Student();
           $list1week = new Student();

        $list30 = $list30->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety  where deleted_at is null group by student_id,type) as exp'), 'exp.student_id', '=', 'students.id')
            ->where('type','=',1)->where('expire', '=', date('Y-m-d',strtotime('+1 month')))->whereNotNull('nav_declaration')->get();

        $list2week = $list2week->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety  where deleted_at is null group by student_id,type) as exp'), 'exp.student_id', '=', 'students.id')
            ->where('type','=',1)->where('expire', '=', date('Y-m-d',strtotime('+2 week')))->whereNotNull('nav_declaration')->get();
        $list1week = $list1week->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety  where deleted_at is null group by student_id,type) as exp'), 'exp.student_id', '=', 'students.id')
            ->where('type','=',1)->where('expire', '=', date('Y-m-d',strtotime('+1 week')))->whereNotNull('nav_declaration')->get();
        foreach ($list30 as $s){
            echo $s->name;
            SendStudentEuEmail::dispatch($s);
        }
        foreach ($list2week as $s){
            SendStudentEuEmail::dispatch($s);
        }
        foreach ($list1week as $s){
            SendStudentEuEmail::dispatch($s);
        }
    }
}
