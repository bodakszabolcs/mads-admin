<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerateM30 extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';
    protected $signature = 'generate:m30';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $files = File::files(storage_path('m30/2023/'));
        foreach ($files as $file){
            $m30 = new \App\Helpers\M30Helper($file->getFilename());
            echo $file->getFilename()."\n";
            $m30->generetate();
        }
    }
}
