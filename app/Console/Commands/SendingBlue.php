<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Student\Entities\Student;
use Modules\Work\Entities\Work;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SendingBlue extends Command
{

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup the database';
    protected $signature = 'create:sendingBlueContacts';
    protected $process;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        ini_set("memory_limit",-1);
        ini_set("max_execution_time",-1);
        $students = Student::where('newsletter','=',1)->get();
        foreach ($students as $s){
            $s->updateSendingBlue();
            $s->save();
            sleep(0.1);
        }
    }
}
