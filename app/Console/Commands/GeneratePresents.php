<?php

namespace App\Console\Commands;

use Modules\Project\Entities\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Nationality\Entities\Nationality;
use Modules\Project\Entities\ProjectSchedule;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\StudentPresent\Entities\StudentPresent;
use PayPal\Api\CountryCode;
use PayPal\Api\Presentation;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GeneratePresents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:presents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate presents from schedules';

    protected $process;


    public function handle()
    {

        	ini_set('memory_limit',-1);
        	ini_set('max_execution_time',-1);
            $schedules = ProjectSchedule::where('schedule_date','<=',date('Y-m-d',strtotime('+1 day')))->whereNull('presens_generated')->get();
            foreach ($schedules as $s){
                $presents = StudentPresent::where('industry_id',$s->industry_id)->where('student_id',$s->student_id)->where('date',$s->schedule_date)->first();
                if(!$presents){
                    $presents = new StudentPresent();
                    $presents->student_id = $s->student_id;
                    $presents->company_id = Project::where('id',$s->project_id)->first()->company_id;
                    $presents->industry_id = $s->industry_id;
                    $presents->date =$s->schedule_date;
                    $presents->start = date('H:i',strtotime($s->start));
                    $presents->end = date('H:i',strtotime($s->emd));
                    $presents->hour = abs(strtotime($s->end) - strtotime($s->start))/(60*60);
                    $presents->save();
                }
                $s->presens_generated = 1;
                $s->save();
            }


    }
}
