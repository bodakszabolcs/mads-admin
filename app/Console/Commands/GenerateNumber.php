<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use PayPal\Api\CountryCode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class GenerateNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:number';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate student from old database';

    protected $process;


    public function handle()
    {
        try {
        	ini_set('memory_limit',-1);
        	ini_set('max_execution_time',-1);
           $students = Student::all();
           foreach ($students as $s){
               $s->student_number = 'MADS'. str_pad($s->id.'',6,'0',STR_PAD_LEFT);
               $s->save();
           }

        } catch (ProcessFailedException $exception) {

        }
    }
}
