<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Nwidart\Modules\Facades\Module;

class DeleteModule extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'module:delete {moduleName : The name of the module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName = $this->argument('moduleName');

        $routesString = [
            "const routes" . $moduleName . " = require('./../../Modules/" . $moduleName . "/Resources/assets/js/router/index')",
            ", routes" . $moduleName . ".default"
        ];
        $modulesString = ["require('./../../Modules/" . $moduleName . "/Resources/assets/js/app')"];
        $seederString = [
            '$this->call(\Modules\'' . $moduleName . '\Database\Seeders\'' . $moduleName . 'DatabaseSeeder::class);',
            '\Modules\'' . $moduleName . '\Entities\'' . $moduleName . '::unsetEventDispatcher();',
            'model = factory(\Modules\''.$moduleName.'\Entities\''.$moduleName.'::class, 5)->create();'
        ];

        if (Module::has($moduleName)) {
            if ($this->confirm('Do you wish to continue?')) {
                Module::disable($moduleName);

                $module = Storage::disk('resources')->get('js/modules.js');
                $route = Storage::disk('resources')->get('js/routes.js');

                foreach ($modulesString as $mstring) {
                    $module = Str::replaceFirst($mstring, '', $module);
                }

                $module = Str::replaceLast("\n",'',$module);

                foreach ($routesString as $rstring) {
                    $route = Str::replaceFirst($rstring, '', $route);
                }

                Storage::disk('resources')->put('js/modules.js', $module);
                $this->info('modules.js updated');
                Storage::disk('resources')->put('js/routes.js', $route);
                $this->info('routes.js updated');

                $seeder = Storage::disk('database')->get('seeds/DatabaseSeeder.php');

                foreach ($seederString as $sstring) {
                    $seeder = Str::replaceFirst($sstring, '', $seeder);
                }

                Storage::disk('database')->put('seeds/DatabaseSeeder.php', $seeder);
                $this->info('DatabaseSeeder updated');

                Storage::disk('modules')->deleteDirectory($moduleName);
                $this->info('Module directory successfully deleted!');
            }
        } else {
            $this->error('Module not found!');
        }
    }
}
