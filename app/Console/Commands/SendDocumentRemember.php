<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Modules\Project\Entities\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Nationality\Entities\Nationality;
use Modules\Project\Entities\ProjectSchedule;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\Student\Entities\StudentDocuments;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\User\Entities\User;
use PayPal\Api\CountryCode;
use PayPal\Api\Presentation;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class SendDocumentRemember extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:document-remember';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send-document remember email';

    protected $process;


    public function handle()
    {

        	ini_set('memory_limit',-1);
        	ini_set('max_execution_time',-1);

            $documents = StudentDocuments::where(function($query){
                $query->where('created_at','>=',date('Y-m-d',strtotime('-1 day')))->orWhere('created_at','>=',date('Y-m-d',strtotime('-2 day')));
            })->whereNull('signed_date')->whereNotNull('created')->where('document_type_id','=',1)->where('created_at','>=',date('Y-m-d',strtotime('-3 day')))->get();
            var_dump(sizeof($documents));
            foreach ($documents as $s){
                try {
                    $student = Student::where('id', $s->student_id)->first();

                    $email = new \Modules\Email\Entities\Email();
                    $email->user_id = $s->created;
                    $email->subject = 'MADS - Dokumentum aláírás emlékeztető';
                    $email->recipient_name = $student->name;
                    //$email->recipient_email = 'bodak.szabolcs@gmail.com';
                    $email->recipient_email = $student->email;
                    $email->link_button_enable = true;
                    $email->content = 'Egy új aláírandó dokumentumod érkezett a MADS-tól, ' . $s->name . ' néven. ' . "<br>" . ' A weboldalunkon belépve, akár eleketronikusan is aláírhatod ügyfélkapu segítségével!';
                    $send = Mail::to($email->recipient_email)->send(new \App\Mail\SendEmail($email));
                    if ($s->created_at >= date('Y-m-d', strtotime('-2 day'))) {
                        $email = new \Modules\Email\Entities\Email();
                        $email->user_id = $s->created;
                        $user = User::where('id', $s->created)->first();
                        $email->subject = 'MADS - Dokumentum aláírás emlékeztető ' . $student->name;
                        $email->recipient_name = $user->firstname . ' ' . $user->lastname;
                        //$email->recipient_email = 'bodak.szabolcs@gmail.com';
                        $email->recipient_email = $user->email;
                        $email->link_button_enable = false;
                        $email->content = 'Kérlek vedd fel a kapcsolatoat a következő diákkal, mert 48 órája nem töltötte fel az alírásra váró dokumentumot: <br>' . $student->name . '<br>' . $student->phone . '<br>' . $student->email;
                        $send = Mail::to($email->recipient_email)->send(new \App\Mail\SendEmail($email));
                    }
                }catch (\Exception $e){
                    var_dump($e->getMessage());
                }
            }


    }
}
