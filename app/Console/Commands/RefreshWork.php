<?php

namespace App\Console\Commands;

use App\Jobs\SaveStatistic;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\Work\Entities\Work;
use PayPal\Api\CountryCode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class RefreshWork extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:work';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate student from old database';

    protected $process;


    public function handle()
    {
        try {
            $works =Work::where('user_id',286)->where("is_full",0)->where("archive",0)->get();
            foreach ($works as $w){
                $w->updated_at = date('Y-m-d H:i:s');
                $w->save();
                SaveStatistic::dispatch(286,'refreshed_adversting');
            }

        } catch (ProcessFailedException $exception) {

        }
    }
}
