<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\Student\Entities\StudentCard;
use Modules\Student\Entities\StudentCertificate;
use Modules\Student\Entities\StudentSchool;
use PayPal\Api\CountryCode;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class MigrateStudent extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:student';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate student from old database';

    protected $process;


    public function handle()
    {
        try {
        	ini_set('memory_limit',-1);
        	ini_set('max_execution_time',-1);
            $mads_tagok =DB::table('mads_tagok')->get();
			$country = DB::table('mads_tagok')->select('cim_lak_orsz')->groupBy('cim_lak_orsz')->get();
			$city = DB::table('mads_tagok')->select('szul_hely')->groupBy('szul_hely')->get();
			$nationality = DB::table('mads_tagok')->select('allampolg')->groupBy('allampolg')->get();
			$school = DB::table('mads_tagok')->select('isk_nev','isk_cim')->groupBy('isk_nev')->get();
			/*foreach ($country as $c){
				$co = new Country();
				$co->name = $c->cim_lak_orsz;
				$co->save();
			}
	        foreach ($city as $c){
		        $co = new City();
		        $co->name = $c->szul_hely;
		        $co->save();
	        }
	        foreach ($nationality as $c){
		        $co = new Nationality();
		        $co->name = $c->allampolg;
		        $co->save();
	        }
	        foreach ($school as $c){
		        $co = new School();
		        $co->name = $c->isk_nev;
		        $co->address = $c->isk_cim;
		        $co->save();
	        }*/
	        foreach ($mads_tagok as $m) {
              /*  $arr=[
                    'id'=>$m->id,
                    'name' => $m->nev,
                    'identity_card'=>$m->azon_szigsz,
                    'birth_location'=>$m->szul_hely,
                    'birth_date'=>($m->szul_ido)?date('Y-m-d',$m->szul_ido):null,
                    'mothers_name'=>$m->anyja,
                    'passport'=>$m->azon_utlev,
                    'exit_date'=>($m->dat_kilep)?date('Y-m-d',$m->dat_kilep):null,
                    'gender'=>(empty($m->neme))?null:(($m->neme =='Nő')?1:2),
                    'phone'=>$m->mobil,
                    'email'=>$m->mail,
                    'zip'=>$m->cim_lak_irsz,
                    'street'=>$m->cim_lak_utca,
                    'house_number'=>$m->cim_lak_hazszam,
                    'building'=>$m->cim_lak_epulet,
                    'stairs'=>$m->cim_lak_lepcso,
                    'level'=>$m->cim_lak_emelet,
                    'door'=>$m->cim_lak_ajto,
                    'notification_address'=>$m->cim_ert,
                    'bank_account_number'=>$m->azon_bank,
                    'tax_number'=>$m->azon_ado,
                    'taj'=>$m->azon_taj,
                    'date_of_entry'=>($m->dat_tag)?date('Y-m-d',$m->dat_tag):null,
                    'nav_declaration'=>($m->nav_bej)?date('Y-m-d',$m->nav_bej):null,
                    'membership_agreement'=>($m->dat_tag)?date('Y-m-d',$m->dat_tag):null,
                    'employment_contract'=>($m->nav_bej)?date('Y-m-d',$m->nav_bej):null,
                    'part_ticket'=>$m->reszj_lv,
                    'newsletter'=>$m->hirlev,
                    'comment'=>$m->megj,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];

                    DB::table('students')->insert($arr);
                    */


                echo $m->nev."\n";
                $student = Student::where('tax_number', '=', $m->azon_ado)->first();
                if ($student && $m->azon_ado) {
                    echo 'TARGET: '.$student->id.' - '.$student->name."\n";
                    $student->date_of_entry=($m->dat_tag)?date('Y-m-d',$m->dat_tag):null;
                    $student->nav_declaration=($m->nav_bej)?date('Y-m-d',$m->nav_bej):null;
                    $student->membership_agreement=($m->dat_tag)?date('Y-m-d',$m->dat_tag):null;
                    $student->employment_contract=($m->nav_bej)?date('Y-m-d',$m->nav_bej):null;
                    $student->student_number = 'MADS' . str_pad($student->id . '', 6, '0', STR_PAD_LEFT);
                    $student->save();
                }

	        }

        } catch (ProcessFailedException $exception) {

        }
    }
}
