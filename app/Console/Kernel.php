<?php

namespace App\Console;

use App\Console\Commands\BackupDatabase;
use App\Console\Commands\FixingPresents;
use App\Console\Commands\GenerateLogsTable;
use App\Console\Commands\GeneratePresents;
use App\Jobs\ClearOldDatabaseBackups;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Modules\Webshop\Console\MNBSync;
use Nwidart\Modules\Facades\Module;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        BackupDatabase::class,
        GenerateLogsTable::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->call(BackupDatabase::class)->dailyAt('23:55')->evenInMaintenanceMode();
        $schedule->call(GeneratePresents::class)->dailyAt('23:55');
        if (config('telescope.enabled')) {
            $schedule->command('telescope:prune --hours=72')->daily()->evenInMaintenanceMode();
        }

        $schedule->job(new ClearOldDatabaseBackups)->dailyAt('01:00');
        $schedule->call(GenerateLogsTable::class)
            ->monthlyOn(28)->emailOutputOnFailure(config('app.superuser_email'));


        $schedule->command('fixing:presents')->monthlyOn(3,'12:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'13:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'14:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'15:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'16:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'17:00');
        $schedule->command('fixing:presents')->monthlyOn(3,'18:00');
        $schedule->command('studentCard:email')->dailyAt('08:00');
        $schedule->command('send:document-remember')->dailyAt('07:00');
        $schedule->command('studentEu:email')->dailyAt('08:30');
        $schedule->command('precence:notification')->monthlyOn('1','09:00');
        $schedule->command('generate:sitemap')->hourly()->evenInMaintenanceMode();
        $schedule->command('send:email')->everyFiveMinutes();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
