<?php

namespace App\Http\Controllers;

use App\Contracts\BaseControllerInterface;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Modules\Menu\Entities\Menu;

class AbstractLiquidController extends Controller implements BaseControllerInterface
{
    /**
     * Eloquent ORM class.
     * @var
     */
    protected $model;
    /**
     * JsonResource class;
     * @var string
     */
    protected $viewResource;
    /**
     * JsonResource class;
     * @var string
     */
    protected $listResource;
    /**
     * ResourceCollection class;
     * @var string
     */
    protected $collection;
    /**
     * FormRequest class;
     * @var string
     */
    protected $createRequest = null;
    /**
     * FormRequest class;
     * @var string
     */
    protected $updateRequest = null;
    /**
     * Defines if using pagination in index() method.
     * @var bool
     */
    protected $pagination = true;
    /**
     * Defines if using JsonResource as collection.
     * @var bool
     */
    protected $useResourceAsCollection = false;

    /**
     * Success status code for success response.
     * @var int
     */
    protected $successStatus = 200;
    /**
     * Success message for success response.
     * @var string
     */
    protected $successMessage = 'OK';
    /**
     * Error status code for error response.
     * @var int
     */
    protected $errorStatus = 422;
    /**
     * Error message for error response.
     * @var string
     */
    protected $errorMessage = 'ERROR';

    /**
     * Setting up resource and collection used for responses.
     * AbstractLiquidController constructor.
     */
    public function __construct(Request $request)
    {
        $this->viewResource = JsonResource::class;
        $this->listResource = JsonResource::class;
        $this->collection = ResourceCollection::class;
    }

    /**
     * Base index method for list views with paginations.
     * @param Request $request
     * @param $auth
     * @return ResourceCollection
     */
    public function index(Request $request, $auth = null)
    {
        $list = $this->model->searchInModel($request->input());

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }

    /**
     * Base create method for Eloquent ORM with FormRequest validation
     * @param Request $request
     * @return JsonResource
     */
    public function create(Request $request)
    {
        if ($this->createRequest) {
            app()->make($this->createRequest);
        }
        $this->model->fillAndSave($request->all());

        return new $this->viewResource($this->model);
    }

    /**
     * Base show method for Eloquent ORM.
     * Response can be restricted with auth param.
     * @param Request $request
     * @param int $id
     * @param BigInt $auth
     * @return JsonResource
     */
    public function show(Request $request, $id = 0, $auth = null)
    {
        $model = $this->model;
        $this->model = $this->model->where('id', '=', $id);

        try {
            if ($auth == null) {
                $this->model = $this->model->firstOrFail();
            } else {
                $this->model = $this->model->where('user_id', '=', $auth)->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            $this->model = new $model();
        }

        return new $this->viewResource($this->model);
    }

    /**
     * Base update method for Eloquent ORM.
     * Update can be restricted with auth param.
     * @param Request $request
     * @param $id
     * @param BigInt $auth
     * @return JsonResource|JsonResponse
     */
    public function update(Request $request, $id, $auth = null)
    {
        if ($this->updateRequest) {
            app()->make($this->updateRequest);
        }

        $this->model = $this->model->where('id', '=', $id);

        try {
            if ($auth == null) {
                $this->model = $this->model->firstOrFail();
            } else {
                $this->model = $this->model->where('user_id', '=', $auth)->firstOrFail();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->fillAndSave($request->all());

        return new $this->viewResource($this->model);
    }

    /**
     * Base destroy method for Eloquent ORM.
     * Destroy can be restricted with auth param.
     * @param BigInt $id
     * @param BigInt $auth
     * @return JsonResponse
     */
    public function destroy($id, $auth = null)
    {
        $this->model = $this->model->where('id', '=', $id);

        try {
            if ($auth == null) {
                $this->model = $this->model->firstOrFail();
            } else {
                $this->model = $this->model->where('user_id', '=', $auth)->firstOrfail();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }

    /**
     * Bulk destroy method for Eloquent ORM.
     * Destroy can be restricted with auth param.
     * @param Request $request
     * @param BigInt $auth
     * @return JsonResponse
     */
    public function destroyBulk(Request $request, $auth = null)
    {
        $this->model = $this->model->whereIn('id', $request->input('ids'));

        if ($auth == null) {
            $this->model = $this->model->delete();
        } else {
            $this->model = $this->model->where('user_id', '=', $auth)->delete();
        }

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }

    /**
     * Get admin menu structure.
     * @param Request $request
     * @return array
     */
    protected function getMenu(Request $request)
    {
        return Menu::getMenu();
    }
}
