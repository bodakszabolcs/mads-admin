<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Modules\Page\Entities\Page;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Return admin view layout.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Admin(Request $request)
    {

        return view('layouts.app_admin');
    }
    public function XML(Request $request,$id){
        $xml='';
        switch($id) {
            case 1:
                $xml = 'xml.jofogas';
                break;
            case 2:
                $xml = 'xml.madsjob';
                break;
            case 3:
                $xml = 'xml.madsjoboz';
                break;
            case 4:
                $xml = 'xml.madswork';
                break;
            case 5:
                $xml = 'xml.jobline';
                break;
            case 6:
                $xml = 'xml.madsjob2';
                break;
            case 7:
                $xml = 'xml/cvonline';
                break;
            case 8:
                $xml = 'xml/workline';
                break;
            case 9:
                $xml = 'xml/jobline2';
                break;
            case 10:
                $xml = 'xml/jobhunter';
                break;
        }
        return response()->view($xml,[],200,["Content-type"=>"text/xml; charset=utf-8"]);
    }
    /**
     * Return frontend view layout and setting up meta data.
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function Frontend(Request $request)
    {

        return Page::returnFrontendContent($request);
    }

    /**
     * Handle 404 page.
     * @param Request $request
     */
    public function _404(Request $request)
    {

        abort(404);
    }
    public function ImageRezize(Request $request,$url){
        header('Content-type: image/jpeg');
        $width=640;
        $height=640;
        list($width_orig, $height_orig) = getimagesize($url);
        $ratio_orig = 1;
        if($width_orig<$height_orig) {
            $ratio_orig = $width / $width_orig;
        }
        if($width_orig>$height_orig) {
            $ratio_orig = $height / $height_orig;
        }



        // This resamples the image
        $image_p = imagecreatetruecolor($width_orig*$ratio_orig, $height_orig*$ratio_orig);
        $image = imagecreatefromjpeg($url);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0,$width_orig*$ratio_orig,$height_orig*$ratio_orig, $width_orig, $height_orig);

        // Output the image
        imagejpeg($image_p, null, 100);
    }
}
