<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       //$this->middleware('guest')->except('logout');
    }


    protected function sendLoginResponse(Request $request)
    {

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ],['password'=>__('Jelszó')]);
    }
    protected function authenticated(Request $request, $user)
    {

        if ($user->hasVerifiedEmail()) {
            $token = $user->createToken('login-'.$user->email.'-'.date("YmdHi"))->plainTextToken;

            $user->api_token = $token;

            $user->save();
            $user->token = $token;
            $user->filters = $user->filters()->get();

            Auth::login($user);
            $user->tokens()->where('last_used_at','<',now()->add('-3 day'))->delete();
            return $user;
        }

        return $request->expectsJson()
            ? response()->json(['errors' => ['email' => [__('Your email address is not verified.')]]], 403)
            : Redirect::route($this->redirectTo ?: 'verification.notice');
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return $request->wantsJson()
            ? new Response('', 204)
            : redirect('/');
    }
}
