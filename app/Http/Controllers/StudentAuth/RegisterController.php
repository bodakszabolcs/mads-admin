<?php

namespace App\Http\Controllers\StudentAuth;

use App\Http\Controllers\Controller;

use App\Http\Controllers\Request\RegistrationCreateRequest;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentLanguages;
use Modules\User\Notifications\VerifyEmail;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \Modules\Student\Entities\Student
     */
    protected function create(array $data)
    {

        $u = Student::where('email', $data['email'])->first();
        if($u){
            return response()->json(['data' => ['message' => "A felhasználó már létezik"]], 422);
        }
        $user = Student::create([
            'email' => $data['email']
        ]);
        $user->password = Hash::make($data['password']);
        $user->fill($data);
        $user->email_verified_at = null;
        $user->save();
        if(isset($data['language_id']) && isset($data['value'])) {
            $st = new StudentLanguages();
            $st->student_id = $user->id;
            $st->language_id = $data['language_id'];
            $st->value = $data['value'];
            $st->save();
        }
        $user->notify(new VerifyEmail());

        return response()->json("OK");
    }

    /**
     * Handle a registration request for the application.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(RegistrationCreateRequest $request)
    {

            event(new Registered($user = $this->create($request->all())));
            $response = $this->registered($request, $user);
            return new Response($response, 201, ['hide_alert' => true]);

    }

    protected function registered(Request $request, $user)
    {
        return $user;
    }
    protected function guard()
    {
        return Auth::guard('student');
    }
}
