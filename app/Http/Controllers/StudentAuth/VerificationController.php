<?php

namespace App\Http\Controllers\StudentAuth;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Request\ResendRequest;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Modules\Student\Entities\Student;
use Modules\User\Notifications\VerifyEmail;
use Modules\User\Transformers\UserListResource;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }
    public function show(Request $request)
    {
        return $request->user()->hasVerifiedEmail()
            ? response()->json(['verified'=> true], 200)
            : response()->json(['verified'=> false], 200);
    }
    public function verify(Request $request)
    {

        try {

            $student = Student::where('id', $request->route('id'))->firstOrFail();

        }catch (\Exception $e){
            throw new AuthorizationException;
        }
        Auth::guard('student')->login($student);
        if ($request->route('id') != Auth::guard('student')->id()) {
            throw new AuthorizationException;
        }

        if (Auth::guard('student')->user()->hasVerifiedEmail()) {
            return response()->json(['verified_at'=> true], 200);
        }

        if (Auth::guard('student')->user()->markEmailAsVerified()) {
            event(new Verified($student));
        }

        $user = Auth::guard('student')->user();
        Auth::guard('student')->logout();
        return response()->json(['verified'=> true], 200);
    }
    public function resend(ResendRequest $request)
    {
        try {

            $student = Student::where('email', $request->input('email'))->firstOrFail();
            $student->notify(new VerifyEmail());
            return \response()->json(['resend'=>'true']);
        }catch (\Exception $e){
            return \response()->json(['resend'=>'false'],422);
        }
    }
}
