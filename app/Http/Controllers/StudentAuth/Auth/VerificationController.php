<?php

namespace App\Http\Controllers\StudentAuth\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Student\Entities\Student;
use Modules\User\Transformers\UserListResource;

class VerificationController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Email Verification Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling email verification for any
    | user that recently registered with the application. Emails may also
    | be re-sent if the user didn't receive the original email message.
    |
    */

    use VerifiesEmails;

    /**
     * Where to redirect users after verification.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function verify(Request $request)
    {

        //Auth::guard('student')->loginUsingId($request->route('id'));
        try {
            $student = Student::where('id', $request->route('id'))->firstOrFail();
        }catch (\Exception $e){
            throw new AuthorizationException;
        }
        Auth::guard('student')->login($student);
        if ($request->route('id') != Auth::id()) {
            throw new AuthorizationException;
        }

        if (Auth::user()->hasVerifiedEmail()) {
            return redirect($this->redirectPath());
        }

        if (Auth::user()->markEmailAsVerified()) {
            //event(new Verified(Auth::user()));
        }

        $user = Auth::user();
        Auth::logout();

        if ($request->wantsJson()) {
            return response()->json(new UserListResource($user), 200);
        }

        return redirect($this->redirectPath())->with('verified', true);
    }
}
