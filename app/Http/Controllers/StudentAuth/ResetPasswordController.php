<?php

namespace App\Http\Controllers\StudentAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    protected function guard()
    {
        return Auth::guard('student');
    }
    public function broker()
    {
        return Password::broker('student');
    }
    protected $redirectTo = '/';

    protected function sendResetResponse(Request $request, $response)
    {
        return response()
            ->json(['status' => trans($response)]);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()
            ->json(['errors' => ['email' => [trans($response)]]], 422);
    }

    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            abort(500);
        }

        return response()->json(['status' => 'OK']);
    }

    public function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required',
            'password' => 'required|string|min:6|same:password_confirm|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ];
    }
}
