<?php

namespace App\Http\Controllers\Request;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'max:255', 'unique:students,email,NULL,id,deleted_at,NULL'],
            'name'=>'required',
            'phone'=>'required',
            'gender'=>'required',
            'birth_date'=>'required',
            'education_level'=>'required',
            'education_area'=>'required',
            'work_categories'=>'required',
            'prefered_work_location'=>'required',
            'newsletter'=>'accepted',
            'personal_data'=>'accepted',
            'aszf'=>'accepted',
            'password' => 'required|string|min:6|same:password_confirm|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'

        ];
    }

    public function attributes()
        {
            return [
                'email' => 'Email',
                'name'=>'Név',
                'phone'=>'Telefon',
                'gender'=>'Nem',
                'birthdate'=>'Születési dátum',
                'education_level'=>'Aktuális tanulmányok',
                'education_area'=>'Végzettség szakma',
                'work_categories'=>'Milyen munkák érdekelnek',
                'prefered_work_location'=>'Hol_szeretnél dolgozni',
                'newsletter'=>'Elektorinikus hirdetés hozzájárulás',
                'personal_data'=>'Adattovábbítás',
                'aszf'=>'Adatkezelési tájékoztató',
                'password' => 'Jelszó',

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
