<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'simple/*',
        'telescope/*',
        'telescope/*',
        '*/student/login',
        '*/student/logout',
        '*/student/register',
        '*/email/*',
        '*/student/password/*',
        '*/student/*',
         '*/upload-file',
        '*/upload-document',
         '*/upload-private-file',
         '*/upload-any-document',
         '*/upload-cv',
        '/sociallogin/google',
        '/sociallogin/facebook',
        '/sociallogin/twitter'
    ];
}
