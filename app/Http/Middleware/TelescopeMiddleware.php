<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\App;
use Modules\User\Entities\User;

class TelescopeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('api_token')) {
            try {
                $user = User::where('api_token','=',$request->input('api_token'))->firstOrFail();

                \Auth::login($user);
            } catch (ModelNotFoundException $e)
            {

                abort(403);
            }
        }

        return $next($request);
    }
}
