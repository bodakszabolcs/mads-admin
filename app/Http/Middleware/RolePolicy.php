<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Modules\User\Entities\User;

class RolePolicy
{
    /**
     * Validating if user has sufficient permissions for actions.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('api_token')) {

            try {
                $user = User::where('api_token','=',$request->input('api_token'))->firstOrFail();
                if(!Auth::user()) {
                    \Auth::login($user);
                }
            } catch (ModelNotFoundException $e)
            {

                abort(403);
            }
        }
        if (!Auth::user()->isSuperAdmin()) {
            $this->checkPermission();
        }

        return $next($request);
    }

    private function checkPermission()
    {
        try {
            $convertedRoute = str_replace('\\', '\\\\\\\\', Route::currentRouteAction());
            User::where('id', '=', Auth::id())->whereHas('roles', function ($query) use ($convertedRoute) {
                $query->whereJsonContains('access->' . $convertedRoute, true);
            })->firstOrFail();
        } catch (ModelNotFoundException $e) {
            abort(401);
        }
    }
}
