<?php

namespace App\Imports;



use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Modules\Payroll\Entities\Payroll;
use Modules\Student\Entities\Student;

class PayrollImport implements ToArray,WithCalculatedFormulas, WithStartRow,SkipsEmptyRows
{
    /**
    * @param array $row
    *
    * @return int
     */



    public function __construct(){}
    private $item = [
        'tax',
        'year',
        'month',
        'gross',
        "szja",
        "deduction",
        "part_ticket",
        'monthly',
        'net',
        'payable',


    ];
    public function startRow():int{
        return 2;

    }
    public function array(array $row)
    {
        DB::beginTransaction();
        $taxs =[];
        foreach ($row as $r) {
            if (sizeof($r) > 10) {
                $r = array_slice($r, 0, 10);
            }

            $data = array_combine($this->item, $r);
            $taxs[]= $data['tax'];
        }
        $students = Student::whereNotNull('tax_number')->whereIn('tax_number', $taxs)->get()->pluck('id','tax_number');
        foreach ($row as $r) {
            if (sizeof($r) > 10) {
                $r = array_slice($r, 0, 10);
            }
            Log::info("data",$r);
            $data = array_combine($this->item, $r);


        if ($data['tax']) {



            if (isset($students[$data['tax']])) {
                $payrol = Payroll::where('student_id', $students[$data['tax']])->where('month', '=', $data['year'] . $data['month'])->first();
                if (!$payrol) {
                    $payrol = new Payroll();
                }
                $payrol->fill($data);
                if ($data['part_ticket'] > 0) {
                    DB::table("students")->where('id',$students[$data['tax']])->update(['part_ticket'=>1]);
                }
                $payrol->student_id = $students[$data['tax']];
                $payrol->month = $data['year'] . $data['month'];
                $payrol->save();
            }
        }
        }

        DB::commit();
        DB::statement("update payrolls LEFT JOIN students on students.id = payrolls.student_id set payrolls.name = students.name where  payrolls.name is null; ");
    }
}
