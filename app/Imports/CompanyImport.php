<?php

namespace App\Imports;



use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\ToArray;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Contact;
use Modules\Payroll\Entities\Payroll;
use Modules\Student\Entities\Student;

class CompanyImport implements ToArray,WithCalculatedFormulas, WithStartRow,SkipsEmptyRows
{
    /**
    * @param array $row
    *
    * @return int
     */
    private $id;


    public function __construct($id){
        $this->id =$id;

    }
    private $item = [
        'name',
        'tax',
        'eu_tax',
        'company_number',
        "delegate",
        "delegate_position",
        "address",
        'contact_name',
        'contact_position',
        'contact_email',
        'contact_phone',


    ];
    public function startRow():int{
        return 2;

    }
    public function array(array $row)
    {
        foreach ($row as $r) {
            if (sizeof($r) > 11) {
                $r = array_slice($r, 0, 11);
            }

            $data = array_combine($this->item, $r);

            $company = Company::where('tax','=',$data['tax'])->first();
            if(!$company){
                $company = new Company();
                $company->name = $data['name'];
                $company->tax = $data['tax'];
                $company->tax_eu = $data['eu_tax'];
                $company->company_number = $data['company_number'];
                $company->delegate = $data["delegate"];
                $company->delegate_position = $data["delegate_position"];
                $company->work_address = $data["address"];
                $company->save();
                $cc = new Contact();
                $cc->name= $data['contact_name'];
                $cc->position= $data['contact_position'];
                $cc->email= $data['contact_email'];
                $cc->phone= $data['contact_phone'];
                $cc->company_id = $company->id;
                $cc->save();
            }
            $campaign = CampaignCompany::where('campaign_id',$this->id)->where('company_id',$company->id)->first();
            if(!$campaign){
                $campaign = new CampaignCompany();
                $campaign->campaign_id = $this->id;
                $campaign->company_id = $company->id;
                $campaign->status_id = 1;
                $campaign->save();


            }
        }

    }
}
