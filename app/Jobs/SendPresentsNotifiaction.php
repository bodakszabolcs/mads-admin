<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;

class SendPresentsNotifiaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $student;




    public function __construct(User $student)
    {
        $this->student = $student;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new AutomaticEmail();
        $email->recipient_name= $this->student->name;
        $email->recipient_email= $this->student->email;
        $email->subject= 'elenléti jóváhagyás emlékeztető';
        $email->save();
        Mail::to($this->student->email)->send(new \App\Mail\SendPresentsNotification($this->student));
        //Mail::to('bodak.szabolcs@gmail.com')->send(new \App\Mail\SendPresentsNotification($this->student));
    }


}
