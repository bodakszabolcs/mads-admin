<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Email\Entities\Email;

class SendEmailWithLink implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $subject;
    public $name;
    public $content;
    public $link;
    public $link_title;
    public $attachments;
    public $to;




    public function __construct($to,$subject,$name,$content,$link,$link_title,$attachments)
    {
        $this->subject=$subject;
        $this->to=$to;
        $this->name=$name;
        $this->content=$content;
        $this->link=$link;
        $this->link_title=$link_title;
        $this->attachments=$attachments;

    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {  // $mail =Mail::to($this->mail->recipient_email);


            Mail::to($this->to)->send(new \App\Mail\SendEmailWithLink($this->name,$this->subject, $this->content, $this->link, $this->link_title, $this->attachments));




    }


}
