<?php

namespace App\Jobs;

use App\AccessLog;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Email\Entities\Email;
use Modules\Project\Entities\ProjectStudent;
use Modules\Project\Entities\Project;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;

class SendPostalEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $mail;
    public $user;



    public $student_id;
    public $postal;


    public function __construct($student_id,$postal)
    {
        $this->student_id=$student_id;
        $this->postal=$postal;


    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {


        $psList =ProjectStudent::where('student_id',$this->student_id)->get();
        $student = Student::where('id',$this->student_id)->first();
        $subjectType='Eseti megbízás';
        if($this->postal->post_subject_id ==19){
            $subjectType='Tagsági + Eseti megbízás';
        }
        if($this->postal->post_subject_id ==20){
            $subjectType='Tagsági megállapodás';
        }
        foreach ($psList as $p) {
            $project = Project::where('id',$p->project_id)->first();
            if($project) {
                $user = User::where('id',$project->user_id)->first();
                try {

                    Mail::to($user->mads_email)->send(new \App\Mail\SendPostalEmail($subjectType,$student,$project));
                    //Mail::to('bodak.szabolcs@gmail.com')->send(new \App\Mail\SendPostalEmail($subjectType,$student,$project));
                    //Mail::to('bodak.szabolcs@gmail.com')->send(new \App\Mail\SendEmail($this->mail));


                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }
            }
        }
    }


}
