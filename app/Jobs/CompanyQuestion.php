<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Campaig\Entities\Campaig;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;

class CompanyQuestion implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $question;




    public function __construct(CampaignCompanyQuestion $question)
    {
        $this->question = $question;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        if($this->question->answered){
            $c =CampaignCompany::where('campaign_id',$this->question->campaign_id)->where('company_id',$this->question->company_id)->first();
            $u = User::where('id',$c->user_id)->first();
            //Mail::to("bodak.szabolcs@gmail.com")->send(new \App\Mail\CompanyQuestionEmail($this->question));
            Mail::to($u->mads_email)->send(new \App\Mail\CompanyQuestionEmail($this->question));
        }else{
            Mail::to($this->question->email)->send(new \App\Mail\CompanyQuestionEmail($this->question));
            if($this->question->other_recipients && isset($this->question->other_recipients[0])){
                foreach ($this->question->other_recipients as $rec){
                    $this->question->name=$rec['name'];
                    if(isset($rec['email']) && $rec['email']) {
                        Mail::to($rec['email'])->send(new \App\Mail\CompanyQuestionEmail($this->question));
                    }
                }
            }
        }
    }


}
