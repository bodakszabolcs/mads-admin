<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;

class SendStudentCardEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $student;




    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {


        $email = new AutomaticEmail();
        $email->recipient_name= $this->student->name;
        $email->recipient_email= $this->student->email;
        $email->subject= 'Értesítés lejáró jogviszonyról';
        $email->save();
        //Mail::to('bodak.szabolcs@gmail.com')->send(new \App\Mail\SendStudentCardEmail($this->student));
        Mail::to($this->student->email)->send(new \App\Mail\SendStudentCardEmail($this->student));
    }


}
