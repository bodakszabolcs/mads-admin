<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;

class StudentRegistrationFeedback implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $student;




    public function __construct(Student $student)
    {
        $this->student = $student;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = \Modules\User\Entities\User::where('id',$this->student->register_user_id)->first();


        //Mail::to($user->email)->send(new \App\Mail\SendRegistrationFeedback($this->student));
        Mail::to($user->mads_email)->send(new \App\Mail\StudentRegistrationFeedbackEmail($this->student));
    }


}
