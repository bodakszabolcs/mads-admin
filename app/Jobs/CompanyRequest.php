<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Campaig\Entities\CampaignCompanyRequest;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;

class CompanyRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $request;
    public $email;




    public function __construct(CampaignCompanyRequest $request,$email)
    {
        $this->request = $request;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        if($this->request->answered){
            $c =CampaignCompany::where('campaign_id',$this->request->campaign_id)->where('company_id',$this->request->company_id)->first();
            $u = User::where('id',$c->user_id)->first();;
           //Mail::to("bodak.szabolcs@gmail.com")->send(new \App\Mail\CompanyReuqestEmail($this->request));
            Mail::to($u->mads_email)->send(new \App\Mail\CompanyReuqestEmail($this->request,$this->email));
        }else{
            Mail::to($this->request->email)->send(new \App\Mail\CompanyReuqestEmail($this->request,$this->email));
            if($this->request->other_recipients && isset($this->request->other_recipients[0])) {
                foreach ($this->request->other_recipients as $rec) {
                    $this->request->name = $rec['name'];
                    if (isset($rec['email']) && $rec['email']) {
                        Mail::to($rec['email'])->send(new \App\Mail\CompanyReuqestEmail($this->request,$this->email));
                    }
                }
            }
            //Mail::to("bodak.szabolcs@gmail.com")->send(new \App\Mail\CompanyReuqestEmail($this->request));
        }


    }


}
