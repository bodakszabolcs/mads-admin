<?php

namespace App\Jobs;

use App\AccessLog;
use App\Mail\SendIntroductionEmail;
use App\Mail\SendRealizationEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Email\Entities\Email;

class SendRealization implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $data;




    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {  // $mail =Mail::to($this->mail->recipient_email);

        try {
            Mail::to($this->data['to'])->send(new SendRealizationEmail($this->data));


        }catch (\Exception $e){
            Log::error($e->getMessage());
        }
    }


}
