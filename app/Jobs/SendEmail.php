<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Email\Entities\Email;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $mail;
    public $user;



    public function __construct(Email $mail)
    {
        $this->mail = $mail;
    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {  // $mail =Mail::to($this->mail->recipient_email);
        if($this->mail->attachments) {

        }
        try {
            Mail::to($this->mail->recipient_email)->send(new \App\Mail\SendEmail($this->mail));
            //Mail::to('bodak.szabolcs@gmail.com')->send(new \App\Mail\SendEmail($this->mail));

            $this->mail->sent_date = date('Y-m-d H:i:s');
            $this->mail->save();
        }catch (\Exception $e){
            Log::error($e->getMessage());
            if(str_contains($e->getMessage(),'RFC 2822')){
                $this->mail->forceDelete();
            }
        }
    }


}
