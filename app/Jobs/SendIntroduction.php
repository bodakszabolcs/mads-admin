<?php

namespace App\Jobs;

use App\AccessLog;
use App\Mail\SendIntroductionEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Email\Entities\Email;

class SendIntroduction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $sender;
    public $to;
    public $name;
    public $link;
    public $content;
    public $subject;



    public function __construct($sender,$to,$name,$subject,$link,$content)
    {
        $this->sender = $sender;
        $this->to = $to;
        $this->name = $name;
        $this->link = $link;
        $this->content = $content;
        $this->subject = $subject;

    }

    /**
     * Execute the job
     *
     * @return void
     */
    public function handle()
    {  // $mail =Mail::to($this->mail->recipient_email);

        try {
            Mail::to($this->to)->send(new SendIntroductionEmail($this->sender,$this->name,$this->link,$this->content,$this->subject));


        }catch (\Exception $e){
            Log::error($e->getMessage());
        }
    }


}
