<?php

namespace App\Jobs;

use App\AccessLog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\Email\Entities\Email;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;
use Modules\User\Entities\UserStatistic;

class SaveStatistic implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $userId;
    public $property;



    public function __construct($userId,$property)
    {
        $this->userId = $userId;
        $this->property = $property;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $us =UserStatistic::where('user_id',$this->userId)->where('date',date('Y-m-d'))->first();
        if(!$us){
            $us = new UserStatistic();
        }
        $us->user_id = $this->userId;
        $us->date = date("Y-m-d");


        $us->{$this->property} +=1;
        $us->save();
    }


}
