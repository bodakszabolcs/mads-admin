<?php

	namespace App\Helpers;
	use Illuminate\Support\Facades\Storage;
    use Illuminate\View\View;
    use Mpdf\Mpdf;

    class M30Helper
	{
        private $source = "";
        private $sign = "alairas-viktor.png";
        private $stamp = "belyegzo.png";
        private $path =  'm30/2023/';
        private $pathExport =  'm30/2024/converted/';

	    public function __construct(string $source)
        {
            $this->source = $source;
        }

        public function generetate()
        {

            /*$pdf = new Mpdf();
            $pageCount = $pdf->setSourceFile(Storage::disk('s3')->get($this->path.$this->source));
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }
                if($i== $pageCount){
                    $imgSign= base64_encode(file_get_contents(Storage::disk('s3')->get("alairas-viktor.png")));
                    $imgData = base64_encode(file_get_contents(Storage::disk('s3')->get("belyegzo.png")));
                    $src = 'data: '.mime_content_type(Storage::disk('s3')->get("belyegzo.png")).';base64,'.$imgData;
                    $srcSign = 'data:image/png;base64, '.$imgSign;
                    $pdf->WriteHTML("<img style='margin-top:665px;margin-left:450px;height:100px' src='".$srcSign."'><img style='margin-top:-10px;height:90px' src='".$src."'>");

                }

            }*/
            $fileName = str_replace(['24M30','.'],'',explode("/",$this->source)[7]);

            $fileName = preg_replace("/[^0-9.]/", "", $fileName).'.pdf';


            return rename($this->source,storage_path($this->pathExport.$fileName));
        }

	}
