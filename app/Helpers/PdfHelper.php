<?php

	namespace App\Helpers;
	use Illuminate\View\View;
    use Mpdf\Mpdf;

    class PdfHelper
	{
	    public static $config =  [
            'mode'                 => '',
            'format'               => 'A4',
            'default_font_size'    => '12',
            'default_font'         => 'sans-serif',
            'margin_left'          => 10,
            'margin_right'         => 10,
            'margin_top'           => 45,
            'margin_bottom'        => 20,
            'margin_header'        => 35,
            'margin_footer'        => 5,
            'orientation'          => 'P',
            'title'                => 'Laravel mPDF',
            'author'               => 'MADS- Márton Áron Iskolaszövetkezet',

            'show_watermark'       => true,
            'show_watermarker_image'       => true,
            'watermark_font'       => 'sans-serif',
            'display_mode'         => 'fullpage',
            'watermark_text_alpha' => 0.99,
            'custom_font_dir'      => '',
            'custom_font_data' 	   => [],
            'auto_language_detection'  => false,
            'temp_dir'               => '',
            'pdfa' 			=> false,
            'pdfaauto' 		=> false,
        ];

	    public static function createPdf(View $content,View $header = null,View  $footer=null,$path = null,$config=[],$filename=null)
        {
            if(is_null($header)) {
                self::$config['margin_top'] = 35;
                self::$config['margin_bottom'] = 15;
            }
            $config = array_merge(self::$config,$config);
            $pdf = new Mpdf($config);
           // $pdf->Image(base_path('storage/doc_bg.png'), 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

            $pdf->showWatermarkImage=true;

            $pdf->SetDefaultBodyCSS('background', 'url('.base_path('storage/doc_bg.png').')');
            $pdf->SetDefaultBodyCSS('background-image-resize', 6);
            $pdf->SetDefaultBodyCSS('background-repeat', 'no-repeat');
            $pdf->curlAllowUnsafeSslRequests = true;
            $pdf->debug=true;
            if($header) {
                $pdf->SetHTMLHeader($header->render());
            }
            if($footer) {
                $pdf->SetHTMLFooter($footer->render());
            }
            $pdf->WriteHTML($content);

            if($path == 'S' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::STRING_RETURN);
            }
            if($path == 'I' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::INLINE);
            }
            if($path == 'F' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::FILE);
            }
            if($path ){
                return $pdf->Output($path,\Mpdf\Output\Destination::FILE);
            }
            if($filename){
                return $pdf->Output($filename,\Mpdf\Output\Destination::INLINE);
            }
            return $pdf->Output();
        }
        public static function createContractPdf(View $content,View $header = null,View  $footer=null,$path = null,$config=[],$filename=null)
        {
            if(is_null($header)) {
                self::$config['margin_top'] = 35;
                self::$config['margin_bottom'] = 15;
            }
            $config = array_merge(self::$config,$config);
            $pdf = new Mpdf($config);
            // $pdf->Image(base_path('storage/doc_bg.png'), 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

            $pdf->showWatermarkImage=true;
            if(!isset($config['type']) || $config['type'] == 'mads') {
                $pdf->SetDefaultBodyCSS('background', 'url(' . base_path('storage/doc_contract_bg.png') . ')');
            }
            if($config['type'] == 'gold') {
                $pdf->SetDefaultBodyCSS('background', 'url(' . base_path('storage/doc_contract_bg_gold.png') . ')');
            }
            if($config['type'] == 'work') {
                $pdf->SetDefaultBodyCSS('background', 'url(' . base_path('storage/doc_contract_bg_work.png') . ')');
            }
            if( $config['type'] == 'request') {
                $pdf->SetDefaultBodyCSS('background', 'url(' . base_path('storage/doc_request_bg.png') . ')');
            }
            $pdf->SetDefaultBodyCSS('background-image-resize', 6);
            $pdf->SetDefaultBodyCSS('background-repeat', 'no-repeat');
            $pdf->curlAllowUnsafeSslRequests = true;
            $pdf->debug=true;
            if($header) {
                $pdf->SetHTMLHeader($header->render());
            }
            if($footer) {
                $pdf->SetHTMLFooter($footer->render());
            }
            $pdf->WriteHTML($content);

            if($path == 'S' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::STRING_RETURN);
            }
            if($path == 'I' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::INLINE);
            }
            if($path == 'F' ){
                return $pdf->Output('pdf',\Mpdf\Output\Destination::FILE);
            }
            if($path ){
                return $pdf->Output($path,\Mpdf\Output\Destination::FILE);
            }
            if($filename){
                return $pdf->Output($filename,\Mpdf\Output\Destination::INLINE);
            }
            return $pdf->Output();
        }

	}
