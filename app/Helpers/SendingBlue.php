<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Modules\Student\Entities\Student;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkLocation\Entities\WorkLocation;
use SendinBlue\Client\Configuration;
use SendinBlue\Client\Api\AccountApi;
use SendinBlue\Client\Api\AttributesApi;
use SendinBlue\Client\Api\ContactsApi;
use SendinBlue\Client\API\SMTPApi;

class SendingBlue
{
        private $config;

        private $apiClient;
        private $attributeClient;
        private $listId=[3];

        private $availableAttribute=[
          'EMAIL'=>'',
          'LASTNAME'=>'',
          'FIRSTNAME'=>'',
          'SMS'=>'',
          'NEM'=>'',
          'MILYEN_MUNKA_ERDEKLI'=>'',
          'HOL_SZERETNE_DOLGOZNI'=>'',
          'VEGZETTSEG'=> '',
          'NYELV'=> '',
          'FEOR'=> '',
          'SZULETESI_DATUM'=>'',
          'SZAKIRANY'=>'',
          'TAG'=>'',
          'JOGOSITVANY'=>''
        ];

        public function __construct()
        {
            $this->config = Configuration::getDefaultConfiguration()->setApiKey('api-key', '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');
            $this->apiClient = new AccountApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
               new  Client(),
                $this->config
            );

        }
        public function createOrUpdateStudent(Student $student){
            try {
                $apiInstance = new ContactsApi(
                // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
                // This is optional, `GuzzleHttp\Client` will be used as default.
                    new Client(),
                    $this->config
                );
                $newsletter = $student->sendingBlue;
                if ($student->newsletter) {
                    $this->availableAttribute['MILYEN_MUNKA_ERDEKLI'] = '';
                    if ($newsletter->work_categories) {

                        $work_categories = WorkCategory::whereIn('id', $newsletter->work_categories)->select('name')->get()->pluck('name', 'name')->toArray();

                        $this->availableAttribute['MILYEN_MUNKA_ERDEKLI'] = implode(",", $work_categories);
                    }
                    $this->availableAttribute['HOL_SZERETNE_DOLGOZNI'] = '';
                   /* if ($newsletter->prefered_work_location) {
                        $workLocation = WorkLocation::whereIn("id",$newsletter->prefered_work_location)->first();
                        $this->availableAttribute['HOL_SZERETNE_DOLGOZNI'] = $workLocation->name;
                    }*/
                    $this->availableAttribute['EMAIL'] = trim($newsletter->email);
                    $this->availableAttribute['LASTNAME'] = explode(" ", $newsletter->name)[0];
                    $this->availableAttribute['FIRSTNAME'] = str_replace($this->availableAttribute['LASTNAME'], "", $newsletter->name);
                    $this->availableAttribute['SMS'] = trim(str_replace(['+','-','(',')'],['','',''],$newsletter->phone));
                    $this->availableAttribute['NEM'] = Arr::get(Student::$gender, '' . $newsletter->gender, '');
                    $this->availableAttribute['SZULETESI_DATUM'] = $newsletter->birth_date;
                    $this->availableAttribute['TAG'] = ConstansHelper::bool($newsletter->membership_agreement);
                    $this->availableAttribute['FEOR'] = $newsletter->feors;
                    $this->availableAttribute['JOGOSITVANY'] = ConstansHelper::bool($newsletter->driver_licence);
                    $this->availableAttribute['VEGZETTSEG'] = $newsletter->levels;
                    $this->availableAttribute['SZAKIRANY'] = $newsletter->areas;
                    $this->availableAttribute['NYELV'] = $newsletter->languages;

                    $createContact = new \SendinBlue\Client\Model\CreateContact(['email' => trim($student->email), 'updateEnabled' => true, 'listIds' => $this->listId, 'attributes' => json_decode(\GuzzleHttp\json_encode($this->availableAttribute))]); // \SendinBlue\Client\Model\CreateContact | Values to create a contact
                    try {
                        $result = $apiInstance->createContact($createContact);
                        $result = $apiInstance->updateContact(trim($newsletter->email), $createContact);

                    } catch (\Exception $e) {

                    }
                } else {
                    try {
                        $apiInstance->deleteContact($student->email);
                    } catch (\Exception $e) {

                    }
                }
            }catch (\Exception $e){

            }
        }
        public function createAllAttribute(){
            $this->attributeClient = new AttributesApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
                new  Client(),
                $this->config
            );
            foreach ($this->attributes as  $attr) {
                $createAttribute = new \SendinBlue\Client\Model\CreateAttribute();
                $createAttribute->setType('text');
                try {
                    $this->attributeClient->createAttribute('normal', $attr, $createAttribute);
                }catch (\Exception $e){

                }
            }
        }

        public  function handleUnsubscribed(){
            $apiInstance = new SMTPApi(
            // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
            // This is optional, `GuzzleHttp\Client` will be used as default.
                new Client(),
                $this->config
            );
            $result =$apiInstance->getTransacBlockedContacts(date('Y-m-d'),date('Y-m-d'),10000);
            print_r($result);
        }
}
