<?php

if (!function_exists('price_format')) {
    function price_format($price, $currencyId = 1, $prefix = null, $suffix = null )
    {


        return trim($prefix . ' ' . number_format($price, 0, ',', ' ') . ' ' . 'Ft');
    }
}
    if (!function_exists('generateHexa')) {
        function generateHexa()
        {
            $characters = '0123456789ABCDEF';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < 6; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            return '#' . $randomString;
        }
    }
if (!function_exists('price_rounding')) {
    function price_rounding($price, $currencyId)
    {
        $currency = \Modules\Webshop\Entities\Currency::find($currencyId);
        return round($price, $currency->rounding);
    }
}

if (!function_exists('format_date')) {
    function format_date($date, $format)
    {
        $d = strtotime($date);

        if ($d && !is_null($d)) {
            return date($format, strtotime($date));
        }

        return null;
    }
}
