<?php

	namespace App\Helpers;
	use Illuminate\Foundation\Http\FormRequest;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    class ConstansHelper
	{
	    public static function getUnits(){
	        return [
	            0=> 'Kg',
                1=> 'Liter',
                2=> 'Méter',
                3=> 'Db',
                4=> 'Szelet',
                5=> 'Doboz',
                6=> 'Palack',
                7=> 'Tasak',
            ];
        }
        public static $moths = [
            1 => 'Január',
            2 => 'Február',
            3 => 'Március',
            4 => 'Április',
            5 => 'Május',
            6 => 'Június',
            7 => 'Július',
            8 => 'Augusztus',
            9 => 'Szeptember',
            10 => 'Október',
            11=> 'November',
            12 => 'December'
        ];
        public static $mothsEn = [
            1 => 'January',
            2 => 'February',
            3 => 'March',
            4 => 'April',
            5 => 'May',
            6 => 'June',
            7 => 'July',
            8 => 'August',
            9 => 'September',
            10 => 'October',
            11=> 'November',
            12 => 'December'
        ];
        public static $days = [
            1 => 'Hétfő',
            2 => 'Kedd',
            3 => 'Szerda',
            4 => 'Csütörtök',
            5 => 'Péntek',
            6 => 'Szombat',
            7 => 'Vasárnap',
        ];
        public static $daysEn = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ];
        public static function formatPrice($price,$dec='.'){
	        return number_format($price,0,$dec, ' '). ' Ft';
        }
        public static function convertCheckboxCast(FormRequest $request,$key){
                $data = $request->input($key,[]);
                $new =[];
                foreach ($data as $k =>$v){
                    if($v){
                        $new[$k]=$k;
                    }
                }
                return $request->merge([$key=>$new]);
        }
        public static function  formatDate($date){
            $timestamp = strtotime($date);
	        return date('Y',$timestamp).'. '.Arr::get(self::$moths,(int)date('m',$timestamp)).' '. ((int)date('d',$timestamp)).'. '.Arr::get(self::$days,date('N',$timestamp));
        }
        public static function  formatShortDate($date){
            if(empty($date)){
                return null;
            }
            $timestamp = strtotime($date);
            return date('Y.m.d',$timestamp);
        }
        public static function  formatShortDateTime($date){
            if(empty($date)){
                return null;
            }
            $timestamp = strtotime($date);
            return date('Y.m.d H:i',$timestamp);
        }
	    public static function  formatTime($time){
		    $timestamp = strtotime($time);
		    return date('H:i',$timestamp);
	    }
	    public static function  formatDateShort($date,$lang="hu"){
            if(empty($date)){
                return null;
            }
		    $timestamp = strtotime($date);
            if($lang=="hu") {
                return Arr::get(self::$days, date('N', $timestamp)) . "<br>" . Arr::get(self::$moths, (int)date('m', $timestamp)) . ' ' . ((int)date('d', $timestamp)) . '.';
            }
            return Arr::get(self::$daysEn, date('N', $timestamp)) . "<br>" . Arr::get(self::$mothsEn, (int)date('m', $timestamp)) . ' ' . ((int)date('d', $timestamp)) . '.';
	    }
        public static function  formatDay($date){
            $timestamp = strtotime($date);
            return ((int)date('d',$timestamp)).'. '.  Arr::get(self::$days,date('N',$timestamp));
        }
	    public static function  formatMonth($date){
		    $timestamp = strtotime($date);
		    return date('Y',$timestamp).'. '.Arr::get(self::$moths,(int)date('m',$timestamp));
	    }
        public static function  bool($data){

            return $data?'Igen':'Nem';
        }
	}
