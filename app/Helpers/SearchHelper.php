<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

/**
 * Class SearchHelper
 * @package App\Helpers
 */
class SearchHelper
{

    /**
     * Implements search in Eloquent model.
     * @param $model
     * @param $filter
     * @return mixed
     */
    public static function searchInModel($model, $filter)
    {
        $class = get_class($model);
        $cl = new $class;
        $list = $class::whereNotNull(($cl->getTable()) . '.id');
        $searchColumns = $model->getFilters();

        $search = Arr::get($filter, 'search', null);
        unset($filter['search']);

        foreach ($filter as $key => $value) {
            foreach ($searchColumns as $sc) {
                if (Str::startsWith($key, $sc['name']) || Str::startsWith($key, str_replace(".","_",$sc['name']))) {
                    if ((empty($value) && $value !== 0 && $value !=='0') || $value == null || $value == 'null') {
                        continue;
                    }
                    $whereKey = ((isset($sc['db']) && !is_null($sc['db'])) ? $sc['db'] : $key);
                    $related = explode(".", $sc['name']);
                    switch ($sc['type']) {
                        case 'json':
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($value, $whereKey) {
                                    $query->whereJsonContains($whereKey, $value);
                                });
                            } else {
                                $list = $list->whereJsonContains($whereKey, $value);
                            }
                            break;
                        case 'select':
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($value, $whereKey) {
                                    $query->where($whereKey, '=', $value);
                                });
                            } else {
                                $list = $list->where($whereKey, '=', $value);
                            }
                            break;
                        case 'date_interval':
                            $date = explode(" - ", $value);
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($date, $whereKey) {
                                    $query->where($whereKey, '>=', $date[0])->where($whereKey, '<=', $date[1]);
                                });
                            } else {
                                $list->where($whereKey, '>=', $date[0])->where($whereKey, '<=', $date[1]);
                            }
                            break;
                        case 'date';
                            if (str_contains($whereKey, 'from')) {
                                $act = '>=';
                            } else {
                                $act = '<=';
                            }
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($value, $act, $whereKey) {
                                    $query->where($whereKey, $act, $value);
                                });
                            } else {
                                $list->where($whereKey, $act, $value);
                            }
                            break;
                        case 'interval':
                            if (Str::endsWith($key, '_from')) {
                                $action = '>=';
                                $whereKey = str_replace('_from', '', $whereKey);
                            } else {
                                $action = '<=';
                                $whereKey = str_replace('_to', '', $whereKey);
                            }
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0],
                                    function ($query) use ($value, $whereKey, $action) {
                                        $query->where($whereKey, $action, $value);
                                    });
                            } else {
                                $list->where($whereKey, $action, $value);
                            }
                            break;
                        case 'raw':
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($value, $sc) {
                                    $query->where(DB::raw('UPPER('.$sc['db_where'].')'), 'LIKE', '%' . strtoupper($value) . '%');
                                });
                            } else {
                                $list = $list->where(DB::raw('UPPER('.$sc['db_where'].')'), 'LIKE', '%' . strtoupper($value) . '%');
                            }
                            break;
                        case 'text':
                        default:
                            if (Str::contains($sc['name'], '.')) {
                                $list = $list->whereHas($related[0], function ($query) use ($value, $whereKey) {
                                    $query->where(DB::raw('UPPER('.$whereKey.')'), 'LIKE', '%' . strtoupper($value) . '%');
                                });
                            } else {
                                $list = $list->where(DB::raw('UPPER('.$whereKey.')'), 'LIKE', '%' . strtoupper($value) . '%');
                            }
                            break;
                    }
                    break;
                }
            }
        }

        if (!is_null($search)) {
            $columns = Schema::getColumnListing($cl->getTable());
            $list = $list->where(function ($query) use ($columns, $search, $cl) {
                foreach ($columns as $k => $v) {
                    if ($v == 'created_at' || $v == 'updated_at' || $v == 'deleted_at') {
                        continue;
                    }
                    $query = $query->orWhere($cl->getTable() . ".$v", 'LIKE', '%' . $search . '%');
                }
            });
        }

        $list = self::setOrderBy($filter, $cl->getTable(), $list);

        return $list;
    }

    /**
     * Setting up order by in Eloquent ORM query.
     * @param $filter
     * @param $tableName
     * @param $list
     * @return mixed
     */
    private static function setOrderBy($filter, $tableName, $list)
    {
        if (!empty($filter['sort'])) {

            $sort = explode("|", $filter['sort']);

            if (Str::contains($sort[0], '.')) {
                $helper = explode(".", $sort[0]);

                $list = $list->whereHas($helper[0], function ($query) use ($sort, $helper) {
                    $query->orderBy($helper[1], $sort[1]);
                });
            } else {
                $list = $list->orderBy($tableName . '.' . $sort[0], $sort[1]);
            }
        } else {
            $list = $list->orderBy($tableName . '.id', 'DESC');
        }

        return $list;
    }
}
