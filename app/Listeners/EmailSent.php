<?php


namespace App\Listeners;


use jdavidbakr\MailTracker\Events\ViewEmailEvent;
use Modules\Email\Entities\Email;

class EmailSent
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ViewEmailEvent  $event
     * @return void
     */
    public function handle(ViewEmailEvent $event)
    {
        $model=$event->sent_email->getHeader('X-Model-ID');
        $email = Email::where('id',$model)->first();
        if(!is_null($email)){
            $email->view_date = date('Y-m-d H:i:s');
            $email->save();
        }
        // Access the model using $event->sent_email...
    }
}
