<?php


namespace App\Listeners;


use jdavidbakr\MailTracker\Events\EmailDeliveredEvent;
use Modules\Email\Entities\Email;

class EmailDelivered
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailDeliveredEvent  $event
     * @return void
     */
    public function handle(EmailDeliveredEvent $event)
    {
        $model=$event->sent_email->getHeader('X-MODEL-ID');
        $email = Email::where('id',$model)->first();
        if(!is_null($email)){
            $email->sent_date = date('Y-m-d H:i:s');
            $email->save();
        }
        // Access the model using $event->sent_email...
    }
}
