<?php

namespace App;

use App\Contracts\BaseModelInterface;
use App\Helpers\SearchHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Modules\System\Entities\Settings;

/**
 * Class BaseModel
 * Extendable for System models.
 * @package App
 */
abstract class BaseModel extends Model implements BaseModelInterface
{
    protected $dateFormat = 'Y-m-d H:i';

    public function getDateFormat()
    {
        return $this->dateFormat;
    }
    /**
     * Cache prefix for Model Cache.
     * @var string
     */
    protected $cachePrefix = "";

    /**
     * Defines base dates.
     * @var array
     */
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    /**
     * Eloquent Eager loading relation loader.
     * @var array
     */
    protected $with = [];

    /**
     * Search columns for list views.
     * @var array
     */
    public $searchColumns = [];

    /**
     * BaseModel constructor.
     * Setting default cachePrefix.
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->cachePrefix = Str::slug(config('app.name').' '.config('app.env'));

        parent::__construct($attributes);
    }

    /**
     * Getter for searchColumns.
     * @return array|mixed
     */
    public function getFilters()
    {
        return $this->searchColumns;
    }

    /**
     * Base Eloquent model search implementation.
     * Using SearchHelper class searchInModel method.
     * @param array $filter
     * @param $query
     * @return mixed
     */
    public function searchInModel(array $filter)
    {
        return SearchHelper::searchInModel($this, $filter);
    }

    /**
     * Base Eloquent ORM fill and save method;
     * @param array $request
     * @return $this|mixed
     */
    public function fillAndSave(array $request)
    {
        $this->fill($request);

        $this->save();

        return $this;
    }

    public function saveWithoutEvents(array $options=[])
    {
        return static::withoutEvents(function() use ($options) {
            return $this->save($options);
        });
    }

    /**
     * Slugger method for generating compatible slug texts.
     * @param $text
     * @return string|string[]|null
     */
    protected function slugify($text)
    {
        return preg_replace("/[^a-zA-Z0-9-]+/", "",Str::ascii(Str::slug($text)));
    }

    protected function serializeDate(\DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i');
    }

    public function getFillables()
    {
        return $this->fillable;
    }

    protected function asJson($value)
    {
        return json_encode($value, JSON_UNESCAPED_UNICODE);
    }
}
