<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendDownPaymentResponseEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;


    public function __construct($user)
    {
        $this->user=$user;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from =[];
        $from['name'] ='MADS Csapata';
        $from['email'] = 'mads@mads.hu';
        return $this->markdown('mail.mads_down_payment_approved')->from([$from])->subject('MADS előlegkérés értesítés');

    }
}
