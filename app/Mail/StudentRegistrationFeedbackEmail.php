<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentRegistrationFeedbackEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $student;
    public $name;


    public function __construct($student)
    {
        $this->student=$student;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = \Modules\User\Entities\User::where('id',$this->student->register_user_id)->first();
        $this->name = $user->firstname.' '.$user->lastname;
        $from = [];
        $from['name'] ='MADS';
        $from['email'] = 'mads@mads.hu';
        $email =$this->markdown('mail.mads_registration_feedback')->from([$from])->subject('MADS - '.$this->student->name.' kitöltötte a kiküldött regisztrációt');

        return $email;
    }
}
