<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\PostalIncome\Entities\PostalIncome;
use Modules\Project\Entities\ProjectStudent;

class SendPostalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $subject;
    public $student;
    public $project;


    public function __construct($subject,$student,$project)
    {
        $this->subject=$subject;
        $this->student=$student;
        $this->project=$project;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from =[];
        $from['name'] ='Mads Postai iktató';
        $from['email'] = 'mads@mads.hu';

        return $this->markdown('mail.mads_postal_email',[])->from([$from])->subject('MADS- A postai iktatóba új '.$this->subject.' érkezett!');

    }
}
