<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendIntroductionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $sender;
    public $name;
    public $link;
    public $content;
    public $subject;

    public function __construct($sender,$name,$link,$content,$subject)
    {
        $this->sender = $sender;

        $this->name = $name;
        $this->content = $content;
        $this->link = $link;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from =[];
        $from['name'] =$this->sender->lastname." ".$this->sender->firstname;
        $from['email'] = $this->sender->mads_email;
        $email =$this->markdown('mail.mads_introduction')->from([$from])->subject($this->subject);
        if($this->sender->introduction) {
            $email = $email->attachFromStorageDisk('s3', $this->sender->introduction, 'MADS-Bemutatkozo.pdf');
        }

        return $email;
    }
}
