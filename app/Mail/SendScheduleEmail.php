<?php

namespace App\Mail;


use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Project\Entities\Project;

class SendScheduleEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $base64;
    public $user;
    public $json;
    public $student;
    public $project;


    public function __construct($base64,$student)
    {
        $this->base64=$base64;
        $this->student = $student;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $d = json_decode(base64_decode($this->base64));
        $this->json =$d;
        $project = Project::where('id',$d->project_id)->first();
        $this->project = $project;
        $user = \Modules\User\Entities\User::where('id','=',$project->user_id)->first();
        $this->user= $user;
        $from =[];
        $from['name'] =$user->lastname." ".$user->firstname;
        $from['email'] = $user->mads_email;
        $email =$this->markdown('mail.mads_schedules_email')->from([$from])->subject("Beosztás tervezés - ". $d->interval." ".$project->company->name);

        return $email;
    }
}
