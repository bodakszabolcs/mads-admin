<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendEmailWithLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $to_name;
    public $subject;
    public $content;
    public $link;
    public $link_title;
    public $att;
    public $user;

    public function __construct($to_name,$subject,$content,$link,$link_title,$attachments=[])
    {
        $this->subject=$subject;
        $this->content=$content;
        $this->link=$link;
        $this->link_title=$link_title;
        $this->att=$attachments;

        $this->to_name=$to_name;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from =[];
        $from['name'] ='MADS';
        $from['email'] = 'mads@mads.hu';
        $email =$this->markdown('mail.mads_email_with_link')->from([$from])->subject($this->subject);
        if($this->att){
            foreach ($this->att as $attachment) {
             $email =  $email->attachFromStorageDisk('s3', $attachment['path'], $attachment['name']);

            }
        }

        return $email;
    }
}
