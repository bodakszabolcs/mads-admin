<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Student\Entities\Student;

class RegistrationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $student;

    public function __construct(Student $student)
    {
        $this->student=$student;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from['name'] ='MADS';
        $from['email'] = 'mads@mads.hu';
        $email =$this->markdown('mail.mads_registration')->from([$from])->subject('MADS - Tagsági előregisztráció');

        return $email;
    }
}
