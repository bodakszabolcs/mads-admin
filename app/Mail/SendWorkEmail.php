<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Modules\Work\Entities\Work;

class SendWorkEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $name;
    public $comment;
    public $phone;
    public $file;
    public $workId;
    public $work;

    public function __construct($email,$name,$comment,$phone,$file,$workId)
    {
        $this->email=$email;
        $this->name=$name;
        $this->comment=$comment;
        $this->phone=$phone;
        $this->file=$file;
        $this->workId = $workId;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->work = Work::where("id",$this->workId)->first();
        $from['name'] =$this->name;
        $from['email'] = $this->email;
        $email =$this->markdown('mail.mads_work_apply')->from([$from])->subject("Mads munkajelentkezés - ".$this->work->name);
        if($this->file){

           $email= $email->attachFromStorageDisk("s3",$this->file,
                'cv.pdf',[
               'mime' => 'application/pdf',
           ]);
        }
        return $email;
    }
}
