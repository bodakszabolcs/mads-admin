<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendRealizationEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $data =null;

    public function __construct($data)
    {
        $this->data = $data;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from =[];
        $from['name'] =$this->data['sender'];
        $from['email'] = $this->data['sender_email'];
        $email =$this->markdown('mail.mads_realization')->from([$from])->subject($this->data['company_name'].', '.$this->data['date'].'. heti ráérés');


        return $email;
    }
}
