<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Company\Entities\Company;
use Modules\User\Entities\User;

class CompanyQuestionEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $companyQuestion;
    public $user;
    public $company;



    public function __construct(CampaignCompanyQuestion  $companyQuestion)
    {
        $this->companyQuestion=$companyQuestion;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $target = CampaignCompany::where('campaign_id',$this->companyQuestion->campaign_id)->where('company_id',$this->companyQuestion->company_id)->first();
        if($this->companyQuestion->answered) {
           $c = CampaignCompany::where('campaign_id', $this->companyQuestion->campaign_id)->where('company_id', $this->companyQuestion->company_id)->first();
            $this->company = Company::where('id',$this->companyQuestion->company_id)->first();
            $this->user = User::where('id', $c->user_id)->first();
            $from['name'] = "MADS";
            $from['email'] = "mads@mads.hu";
            $email = $this->markdown('mail.company.question_answer')->from([$from])->subject('MADS - Válaszok érkeztek az egyik árajánlathoz!');
        }
        else {
            $user = User::where('id', $target->user_id)->first();
            $from = [];
            $this->user = $user;
            $from['name'] = $user->firstname . ' ' . $user->lastname;
            $from['email'] = $user->mads_email;
            $email = $this->markdown('mail.company.question')->from([$from])->subject('MADS - Kérdések érkeztek az árajanlat elkészítésehez!');
        }
        return $email;
    }
}
