<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCalendarEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $calendar;
    public $from;
    public $sender;
    public $name;

    public function __construct($calendar,$from,$sender,$name)
    {
        $this->calendar=$calendar;
        $this->sender = $sender;
        $this->from = $from;
        $this->name = $name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $email =$this->markdown('mail.mads_calendar')->from([$this->from])->subject('MADS - Eseménymeghívás');

        return $email;
    }
}
