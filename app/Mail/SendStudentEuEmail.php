<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendStudentEuEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $student;


    public function __construct($student)
    {
        $this->student=$student;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from =[];
        $from['name'] ='MADS';
        $from['email'] = 'mads@mads.hu';
        $email =$this->markdown('mail.mads_student_eu_email')->from([$from])->subject("Értesítés lejáró Egészségügyi kiskönyvről");

    }
}
