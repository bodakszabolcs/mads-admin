<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\OnlineStudentCard\Entities\OnlineStudentCard;

class SendOnlineCardEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $user;
    public $card;
    public $name;

    public function __construct($card,$name)
    {
        $this->card = $card;
        $this->name = $name;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $this->user= \Modules\User\Entities\User::where('id',$this->card->user_id)->first();
        $from =[];
        $from['name'] =$this->user->firstname.' '.$this->user->lastname;
        $from['email'] = $this->user->email;
        $email =$this->markdown('mail.mads_online_card')->from([$from])->subject('MADS - Online diák/jogviszony igazolás');

        return $email;
    }
}
