<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class SendEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $user;

    public function __construct($email)
    {
        $this->email=$email;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = \Modules\User\Entities\User::where('id','=',$this->email->user_id)->first();
        $this->user= $user;
        $from =[];
        $from['name'] =$user->lastname." ".$user->firstname;
        $from['email'] = $user->mads_email;
        $email =$this->markdown('mail.mads_email')->from([$from])->subject($this->email->subject);
        if($this->email->attachments){
            foreach ($this->email->attachments as $attachment)
                Log::error($attachment['path']);
                Log::error($attachment['name']);
           $email= $email->attachFromStorageDisk('s3',$attachment['path'],$attachment['name']);
        }
        return $email;
    }
}
