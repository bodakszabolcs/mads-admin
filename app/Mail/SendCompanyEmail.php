<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendCompanyEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $email;
    public $from;

    public function __construct($email,$from)
    {
        $this->email=$email;
        $this->from = $from;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {


        $email =$this->markdown('mail.mads_email_company')->from([$this->from])->subject($this->email->subject);
        if($this->email->attachments){
            foreach ($this->email->attachments as $attachment)
           $email= $email->attachFromStorageDisk('s3',$attachment['path'],$attachment['name']);
        }
        return $email;
    }
}
