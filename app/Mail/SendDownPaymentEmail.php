<?php

namespace App\Mail;

use http\Client\Curl\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendDownPaymentEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $user;
    public $email;
    public $name='Ember Ágnes';

    public function __construct($user,$email)
    {
        $this->user=$user;
        $this->email=$email;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from =[];
        $from['name'] =$this->user;
        $from['email'] = $this->email;
        return $this->markdown('mail.mads_down_payment')->from([$from])->subject('MADS előlegkérés');

    }
}
