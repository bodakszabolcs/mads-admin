<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Campaig\Entities\CampaignCompanyRequest;
use Modules\Company\Entities\Company;
use Modules\User\Entities\User;

class CompanyReuqestEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $companyRequest;
    public $email;
    public $user;
    public $company;




    public function __construct(CampaignCompanyRequest  $companyRequest,$email)
    {
        $this->companyRequest=$companyRequest;
        $this->email = $email;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $target = CampaignCompany::where('campaign_id',$this->companyRequest->campaign_id)->where('company_id',$this->companyRequest->company_id)->first();
        $user = User::where('id',$target->user_id)->first();
        $from = [];
        $this->user = $user;
        $from['name'] =$user->firstname.' '.$user->lastname;
        $from['email'] = $user->email;
        if( $this->companyRequest->approved){
            $this->company =Company::where('id',$target->company_id)->first();
            $subject=$this->company->name.' - elfodta az árajánlatot';
            $email =$this->markdown('mail.company.request_approve')->from([$from])->subject($subject)->attachFromStorageDisk('s3',$this->companyRequest->documentum,
               'MADS-arajanlat.pdf'
                );;
        }else {
            $subject = 'MADS - Árajánlat érkezett!';
            $email =$this->markdown('mail.company.request')->from([$from])->subject($subject)->attachFromStorageDisk('s3',$this->companyRequest->documentum,
                'MADS-arajanlat.pdf'
            );
        }
        return $email;
    }
}
