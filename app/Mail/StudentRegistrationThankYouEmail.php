<?php

namespace App\Mail;


use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StudentRegistrationThankYouEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $student;
    public $name;


    public function __construct($student)
    {
        $this->student=$student;


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $from = [];
        $from['name'] ='MADS';
        $from['email'] = 'mads@mads.hu';
        $email =$this->markdown('mail.mads_registration_thank_you')->from([$from])->subject('MADS - Köszönjük a regisztrációt!');

        return $email;
    }
}
