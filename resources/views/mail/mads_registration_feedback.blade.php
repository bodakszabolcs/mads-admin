@component('mail::message')
    <h1> {{__('Kedves ').$name }},</h1>
    Ez egy automatikus értesítés!<br>
    {{$student->name}} kitöltötte az általad kiküldött regisztrációs linket. Kérlek vedd fel vele a kapcsolatot!

@component('mail::button',['url'=>'https://mads.hu/admin/student/edit/'.$student->id]) Ugrás a diákhoz @endcomponent

    {{__('Üdvözlettel')}}
    {!!   'MADS' !!}
@endcomponent
