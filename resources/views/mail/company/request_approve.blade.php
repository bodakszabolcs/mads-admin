@component('mail::message')
    <h1>Kedves {{ $user->firstname }} {{ $user->lastname }},</h1>
    <p>A(z) {{$company->name}} elfogadta a következő árajánlatokat:
    @foreach($companyRequest->request as $k=>$data)
    <p>{{$data['name']}} - {{\App\Helpers\ConstansHelper::bool(isset($data['approved'])?$data['approved']:false)}} </p>
    @endforeach
    <p>Az ajánlat letölthető a MADS felületén is :</p>
    @component('mail::button',['url'=>env('APP_URL').'/admin/campaign/question-list']) Ugrás az árajánlathoz @endcomponent
    <br>
    <br>
    <br>
    {{__('Üdvözlettel')}},<br>
    MADS Csapat
@endcomponent
