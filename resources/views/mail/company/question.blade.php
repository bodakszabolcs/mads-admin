@component('mail::message')
    <h1>Kedves {{ $companyQuestion->name }},</h1>
    <p>Mielőtt árajnátot készítünk, feltennénk néhány kérdést, hogy minnél pontossabban fel tudjuk mérni az igényeit.<br>
        Az alábbi kérdéseket gyüjtöttük össze:
    </p>
@foreach($companyQuestion->questions as $question)
    <p> <b>{{$loop->iteration}}. {{$question['question']}}</b></p>
    @if(isset($question['answer']) && !empty($question['answer']))
    <p> <b> {{$question['answer']}}</b></p>
    @endif
@endforeach
<p>Amennyiben lehetősége van kérjük a MADS felületén válaszolja meg az alábbi kérdéseket.<br>
        Előkészítettünk az Ön számára egy felhasználót a rendszerünkbe.<br>
        Amennyiben még nem használta a rendszerünket, az első belépéshez szükséges jelszót az "Elfelejtett jelszó" linken tudja beállítani.<br>
    <p>Az Ön felhasználó neve az email címe lesz: <b>{{$companyQuestion->email}}</b></p>
    <p>A felületet az alábbi linken éri el:</p>
    @component('mail::button',['url'=>env('APP_URL').'/admin/forgot-password']) Felhasználó létrehozása! @endcomponent
<br>
<br>
<br>
{{__('Üdvözlettel')}},<br>
{!!   $user->signature !!}
@endcomponent
