@component('mail::message')
    <h1>Kedves {{ $companyRequest->name }},</h1>
    {!! $email !!}
    <p>A MADS megküldte az Ön részére az árajánlatot, amit csatolva küldünk
    <p>Az ajánlat letölthető a MADS céges felületén, belépés után :</p>
    @component('mail::button',['url'=>env('APP_URL').'/admin/my-requests']) Ugrás az árajánlathoz @endcomponent
<br>
<br>
<br>
{{__('Üdvözlettel')}},<br>
{!!   $user->signature !!}
@endcomponent
