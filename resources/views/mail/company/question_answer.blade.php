@component('mail::message')
    <h1>Kedves {{ $user->firstname }} {{ $user->lastname }},</h1>
    {{ $company->name }} visszaküldte a válaszokat a kérdéseidre.
@foreach($companyQuestion->questions as $question)
    <p> <b>{{$loop->iteration}}. {{$question['question']}}</b></p>
    @if(isset($question['answer']) && !empty($question['answer']))
    <p> <b> {{$question['answer']}}</b></p>
    @endif
@endforeach
<br>
{{__('Üdvözlettel')}},<br>
MADS csapat
@endcomponent
