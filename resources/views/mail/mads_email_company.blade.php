@component('mail::message')
    <h1> {{__('Kedves ').$email->recipient_name }},</h1>
    {!! $email->content !!}
@endcomponent
