@component('mail::message')
    <h1> {{__('Kedves ').$name }},</h1>
    {!! $content !!}
@component('mail::button',['url'=>env('APP_URL').$link]) Ajánlatot kérek! @endcomponent{{__('Üdvözlettel')}},<br>{!! $sender->signature !!}
@endcomponent
