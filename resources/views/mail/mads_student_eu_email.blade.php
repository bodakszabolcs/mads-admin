@component('mail::message')
    <h1> {{__('Kedves ').$student->name }},</h1>
    <p>Nyilvántartásunk szerint az orvosi alkalmassági vizsgálatod vagy EÜ kis könyved érvényessége hamarosan megszűnik ({{\Modules\Student\Entities\StudentWorkSafety::where('student_id',$student->id)->orderBy('expire','desc')->where('type',1)->first()->expire}}).
        Amennyiben a jövőben is szeretnél ugyanebben a munkakörben dolgozni, el kell menned egy újabb vizsgálatra. Kérlek jelezd a Projektmenedzsered felé és intézi az orvosi beutalót és kér számodra időpontot a vizsgálatra.
        Az együttműködésedet előre is köszönjük!</p>

    {{__('Üdvözlettel')}},
    MADS csapata
@endcomponent
