@component('mail::message')
    <h1> {{__('Kedves ').$student->name }},</h1><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span
                            style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#2d2d2d">Nyilv&aacute;ntart&aacute;sunk szerint a di&aacute;kigazolv&aacute;nyodon l&eacute;vő matrica vagy a leadott hallgat&oacute;i jogviszony igazol&aacute;sod &eacute;rv&eacute;nyess&eacute;ge hamarosan megszűnik ({{$student->certificate()->orderBy('end','desc')->first()->end}}).</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">Amennyiben a k&eacute;sőbbiekben is szeretn&eacute;l munk&aacute;t v&aacute;llalni a MADS szervez&eacute;s&eacute;ben, juttasd el hozz&aacute;nk a k&ouml;vetkező időszakra &eacute;rv&eacute;nyes di&aacute;kigazolv&aacute;nyodat vagy a hallgat&oacute;i jogviszony igazol&aacute;sodat! </span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">A k&ouml;vetkező lehetős&eacute;gek k&ouml;z&uuml;l v&aacute;laszthatsz:</span></span></span></span></span></p>
    <ol>
        <li style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">FELT&Ouml;LT&Ouml;M a MADS fi&oacute;komba,</span></span></strong></span></span>
        </li>
        <li style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">elk&uuml;ld&ouml;m el e-mailben,</span></span></span></span>
        </li>
        <li style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">beviszem szem&eacute;lyesen:</span></span></span></span>
        </li>
    </ol><p style="margin-left:48px; text-align:justify">&nbsp;</p>
    <ul>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">az &eacute;rv&eacute;nyes&iacute;tett matric&aacute;val ell&aacute;tott (k&ouml;vetkező f&eacute;l&eacute;vre) nappali tagozatos di&aacute;kigazolv&aacute;nyod mindk&eacute;t oldal&aacute;r&oacute;l k&eacute;sz&iacute;tett j&oacute; minős&eacute;gű fot&oacute;t PDF form&aacute;tumban, vagy</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">az &eacute;rv&eacute;nyes nappali tagozatos hallgat&oacute;i jogviszony igazol&aacute;sod szkennelt v&aacute;ltozat&aacute;t</span></span></span></span>
        </li>
    </ul><p style="text-align:center">&nbsp;</p><p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span
                                style="font-family:&quot;Times New Roman&quot;,serif">Ha m&aacute;r v&aacute;lasztott&aacute;l a lehetős&eacute;gek k&ouml;z&uuml;l, hogyan tudod megtenni:</span></span></strong></span></span>
    </p>
    <ol>
        <li><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Felt&ouml;lt&eacute;s a MADS fi&oacute;kba:</span></span></strong></span></span>
        </li>
    </ol><p style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><strong>Ha </strong>bővebb inform&aacute;ci&oacute;ra lenne sz&uuml;ks&eacute;ged, n&eacute;zd meg &uacute;j youtube vide&oacute;nkat</span></span>
    </p><p style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="font-family:&quot;Segoe UI Symbol&quot;,sans-serif">➡</span> <a
                        href="https://youtu.be/VwTZXwSUNB0" style="color:blue; text-decoration:underline" target="_blank" title="https://youtu.be/VwTZXwSUNB0"><span
                            style="color:#0092ff">https://youtu.be/VwTZXwSUNB0</span></a></span></span></p><p style="text-align:justify">&nbsp;</p><p style="text-align:justify">&nbsp;</p>
    <ol start="2">
        <li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><strong>E-mailben k&uuml;ld&ouml;m:</strong></span></span></li>
    </ol><p style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">K<span style="color:#2d2d2d">&uuml;ld el e-mailben Ember &Aacute;gnes r&eacute;sz&eacute;re az </span><a
                        href="https://mads.hu/email/n?l=mailto%3Aember.agnes%40mads.hu&amp;h=Xcm4lyOjTmtwrapyNnGxtQWF6zpv1fW3" style="color:blue; text-decoration:underline" target="_blank"><span style="color:#3869d4">ember.agnes@mads.hu</span></a><span
                        style="color:#2d2d2d"> e-mail c&iacute;mre. A lev&eacute;l t&aacute;rgya a k&ouml;vetkező legyen: <strong>SAJ&Aacute;TN&Eacute;V_Di&aacute;kigazolv&aacute;ny </strong>vagy<strong> SAJ&Aacute;TN&Eacute;V_Jogviszony igazol&aacute;s</strong></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></p>
    <ol start="3">
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                        style="color:#2d2d2d">Szem&eacute;lyesen beviszem:</span></span></span></strong></span></span></li>
    </ol><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Nyitvatart&aacute;si időben leadhatod k&ouml;zponti irod&aacute;nkban.</span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></p><p style="text-align:justify"><span style="font-size:11pt"><span
                    style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                    style="color:#2d2d2d">Mi meddig &eacute;rv&eacute;nyes:</span></span></span></strong></span></span></p><p style="text-align:justify"><span style="font-size:11pt"><span
                    style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">A nappali tagozatos hallgat&oacute;i st&aacute;tuszodat az al&aacute;bbiak szerint igazolhatod:</span></span></strong></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">a) &nbsp; &nbsp; &nbsp; a k&ouml;znevel&eacute;si int&eacute;zm&eacute;ny &aacute;ltal ki&aacute;ll&iacute;tott tanul&oacute;i jogviszony-igazol&aacute;s, vagy</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">b) &nbsp; &nbsp; &nbsp; a felsőoktat&aacute;si int&eacute;zm&eacute;ny &aacute;ltal ki&aacute;ll&iacute;tott hallgat&oacute;i jogviszony-igazol&aacute;s, vagy</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">c) &nbsp; &nbsp;az oktat&aacute;si igazolv&aacute;nyokr&oacute;l sz&oacute;l&oacute; 362/2011. (XII. 30.) Korm. rendelet szerint a k&ouml;zreműk&ouml;dő int&eacute;zm&eacute;ny &aacute;ltal a t&aacute;rgyidőszakra kiadott, &eacute;rv&eacute;nyes&iacute;tő matric&aacute;val ell&aacute;tott, nappali t&iacute;pus&uacute; di&aacute;kigazolv&aacute;ny, vagy</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">d) &nbsp; &nbsp; &nbsp; az oktat&aacute;si igazolv&aacute;nyokr&oacute;l sz&oacute;l&oacute; 362/2011. (XII. 30.) Korm. rendelet szerint a tank&ouml;teles kor felső hat&aacute;r&aacute;t az adott tan&eacute;vben bet&ouml;ltő tanul&oacute; eset&eacute;n az &eacute;rv&eacute;nyes&iacute;tő matrica n&eacute;lk&uuml;li di&aacute;kigazolv&aacute;ny, vagy</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">e) &nbsp; a k&ouml;zreműk&ouml;dő int&eacute;zm&eacute;ny &aacute;ltal kiadott, a c) pont szerinti di&aacute;kigazolv&aacute;nyra val&oacute; jogosults&aacute;gr&oacute;l sz&oacute;l&oacute; igazol&aacute;s tan&uacute;s&iacute;tja.</span></span></span></span></span>
    </p><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></p><p style="text-align:justify"><span style="font-size:11pt"><span
                    style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">A nappali tagozatos hallgat&oacute;i st&aacute;tusz igazol&aacute;sok &eacute;rv&eacute;nyess&eacute;g&eacute;nek ideje:</span></span></strong></span></span>
    </p>
    <ul>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&Eacute;rv&eacute;nyes matric&aacute;val ell&aacute;tott nappali tagozatos di&aacute;kigazolv&aacute;ny őszi f&eacute;l&eacute;v: m&aacute;rcius 31.</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&Eacute;rv&eacute;nyes akt&iacute;v nappali tagozatos hallgat&oacute;i jogviszony-igazol&aacute;s őszi f&eacute;l&eacute;v: janu&aacute;r 31.</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&Eacute;rv&eacute;nyes matric&aacute;val ell&aacute;tott nappali tagozatos di&aacute;kigazolv&aacute;ny tavaszi f&eacute;l&eacute;v: okt&oacute;ber 31.</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&Eacute;rv&eacute;nyes akt&iacute;v nappali tagozatos hallgat&oacute;i jogviszony-igazol&aacute;s tavaszi f&eacute;l&eacute;v: augusztus 31.</span></span></span></span>
        </li>
    </ul><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></p><p style="text-align:center"><span style="font-size:11pt"><span
                    style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#2d2d2d">Az egy&uuml;ttműk&ouml;d&eacute;sedet előre is k&ouml;sz&ouml;nj&uuml;k!</span></span></span></span></span>
    </p><p>&nbsp;</p><h1> {{__('Dear ').$student->name }},</h1><p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span
                            style="font-family:&quot;Times New Roman&quot;,serif"><span style="color:#2d2d2d">According to our system the sticker on your student ID or the certificate of your student status is going to expire soon (({{date('d.m.Y',strtotime($student->certificate()->orderBy('end','desc')->first()->end))}}))</span></span></span></span></span>
    </p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">If you would like to work via MADS for the future please make sure to send us/bring us your student ID with the new sticker or the new certificate of your student status.</span></span></span></span></span>
    </p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">You can choose from the options below:</span></span></span></span></span></p>
    <ol>
        <li style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span
                                style="font-family:&quot;Times New Roman&quot;,serif">send it in e-mail</span></span></span></span></li>
        <li style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">bring it in to our office during opening hours</span></span></span></span>
        </li>
    </ol>
    <p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Find the information below regarding the chosen option:</span></span></strong></span></span>
    </p>
    <p style="text-align:justify">&nbsp;</p>
    <ol>
        <li style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><strong>Send it in e-mail:</strong></span></span></li>
    </ol>
    <p style="text-align:justify">&nbsp;</p>
    <p style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Take a (good quality) photo of both sides of your student ID or certificate of student status and send it <strong><em><span
                                style="color:red">in PDF format</span></em></strong> to Ember &Aacute;gnes (<a href="mailto:ember.agnes@mads.hu" style="color:blue; text-decoration:underline">ember.agnes@mads.hu</a>). The subject of the e-mail should be: <strong>YOURNAME_STUDENT ID</strong> or <strong>YOURNAME_CERTIFICATE OF STUDENT STATUS</strong></span></span>
    </p>
    <p style="text-align:justify"><span style="font-size:12pt"><span style="font-family:&quot;Times New Roman&quot;,serif">&nbsp;</span></span></p>
    <ol start="2">
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Bring it in to our office<span
                                        style="color:#2d2d2d">:</span></span></span></strong></span></span></li>
    </ol>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">You can bring it in to our office any time during opening hours.</span></span></span></span>
    </p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Monday-Thursday: 8:00-17:30</span></span></span></span>
    </p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span
                            style="font-family:&quot;Times New Roman&quot;,serif">Friday: 8:00-16:30</span></span></span></span></p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><u><span style="font-size:14.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                        style="color:#2d2d2d">Validity:</span></span></span></u></strong><u>&nbsp;</u></span></span></p>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                    style="color:#2d2d2d">Sticker on the student ID:</span></span></span></u></span></span></p>
    <ul>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">{{date('Y')-1}}/{{date('Y')}} I. semester (autumn semester): 31.03.{{date('Y')}}</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">{{date('Y')-1}}/{{date('Y')}} II. semester (spring semester): 31.10.{{date('Y')}}.</span></span></span></span>
        </li>
    </ul>
    <p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><u><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">Certificate of student status:</span></span></u></span></span>
    </p>
    <ul>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">{{date('Y')-1}}/{{date('Y')}} I. semester (autumn semester): 31.01.{{date('Y')}}.</span></span></span></span>
        </li>
        <li style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif">{{date('Y')-1}}/{{date('Y')}} II. semester (spring semester): 31.08.{{date('Y')}}.</span></span></span></span>
        </li>
    </ul>
    <p style="margin-left:48px; text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">&nbsp;</span></span></p>
    <p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:&quot;Times New Roman&quot;,serif"><span
                                style="color:#2d2d2d">Thank you for cooperating!</span></span></span></span></span></p>
    <p>&nbsp;</p>
    </p>{!!  \Modules\User\Entities\User::where('id',3)->first()->signature!!}
@endcomponent
