@component('mail::message')
    <h1> {{__('Kedves ').$student->name }},</h1>
    Köszönjük a regissztrációt!<br>
    A MADS azonosítód: <b>{{$student->student_number}}</b>
    <p><a href="https://mads.hu/storage/files/partners/Dokumentumok%20honlap%20VIKTOR/munka_baleset_es_tuzvedelmi_tajekoztato_MADS_Cegcsoport_230803.pdf" target="_blank">Itt olvashatod el a Munka-, baleset és tűzvédelmi tájékoztatónkat.</a></p>
    {{__('Üdvözlettel')}}
    {!!   'MADS' !!}
@endcomponent
