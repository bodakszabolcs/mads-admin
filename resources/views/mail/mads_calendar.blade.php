@component('mail::message')
    <h1> {{__('Kedves ').$name }},</h1>
    {{$sender}} meghívott, hogy csatlakozz egy eseményhez:
    <h2>{{$calendar->info}}</h2>

@component('mail::button',['url'=>'https://calendar.google.com/calendar/render?action=TEMPLATE&text='.$calendar->info.'&dates='.date('Ymd',strtotime($calendar->date.' '.$calendar->start)).'T'.date('Hi',strtotime($calendar->date.' '.$calendar->start)).'00/'.date('Ymd',strtotime($calendar->date.' '.$calendar->end)).'T'.date('Hi',strtotime($calendar->date.' '.$calendar->end)).'00&details='.$calendar->info.'&location=1092 Budapest Erkel u 3.']) Hozzáadás a naptárhoz @endcomponent

    {{__('Üdvözlettel')}}
    {!!   $sender !!}
@endcomponent
