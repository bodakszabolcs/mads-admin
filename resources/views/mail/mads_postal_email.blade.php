@component('mail::message')
    <h1> {{$student->name }} ({{$student->student_number}}),</h1>
    <p>A Postai iktatóba új dokumentum érkezett: {{$subject}}<p>
    <p>{{$project->company->name}}/{{$project->industry->name}}</p>
@endcomponent
