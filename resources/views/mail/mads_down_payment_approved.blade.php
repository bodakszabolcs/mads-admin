@component('mail::message')
    <h1> {{__('Kedves ').$user }},</h1>
    Előleget minden hónapban 20-25 között utalunk és csak a 15.-ig ledolgozott munkabéred 80%-át áll módunkban kifizetni, amit a Munkahelyed is leigazol jelenléti íven. Az elutalt előleg összegével csökken hó végén az utalandó béred.

    {{__('Üdvözlettel')}},
    MADS csapata
@endcomponent
