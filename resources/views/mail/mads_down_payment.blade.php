@component('mail::message')
    <h1> {{__('Kedves ').$name }},</h1>
    {!! $user !!} online kért előleget! A részleteket az adminban találod!
@endcomponent
