@component('mail::message')
    <h1> {{__('Munkajelentkezés ')}}</h1>
    <p>Név: {{$name}}<p/>
    <p>Email: {{$email}}<p/>
    <p>Telefon: {{$phone}}<p/>
    <p>Megjegyzés: {{$comment}}<p/>
    <p>Munka: {{$work->name}} <p/>
@endcomponent
