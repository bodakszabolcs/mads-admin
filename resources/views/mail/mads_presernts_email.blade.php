@component('mail::message')
    <h1> {{__('Kedves ').$student->name }},</h1><p>Szeretnénk felhívni a figyelmedet, hogy a MADS-es diákok online jelenléti íveinek ellenőrzése/jóváhagyása tárgyhónapot követő hónap 3-án 12 óráig érhető el. Ezt követően már csak a MADS belső munkatársai tudják módosítani az óraszámokat. Amennyiben nem sikerülne az adott időpontig leellenőrizni, átnézni vagy jóváhagyni az adatokat és azokban eltérést tapasztalsz, kérlek e-mailben az ember.agnes@mads.hu címre küld el a módosítási kéréseidet</p>{{__('Üdvözlettel')}},<br>MADS csapata
@endcomponent
