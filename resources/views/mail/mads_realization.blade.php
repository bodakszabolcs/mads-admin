@component('mail::message')
    <h1> {{__('Kedves ').$data['name'] }},</h1>
    <p>Kérlek jelezz vissza a {{$data['date']}}.heti, {{$data['company_name']}} ráéréssel kapcsolatban.</p>
    <p>Fontos, hogy akkor is jelezz vissza, ha nem érsz rá!</p>
    <p>Az alábbi linken, belépés után tudsz visszajelezni</p>
    <p>@component('mail::button',['url'=>$data['link']]) Visszajelzés küldése @endcomponent</p>
    {!! $data['signature'] !!}
@endcomponent
