@component('mail::message')
    <h1> {{__('Kedves ').$message->name }},</h1>

 <h2>{{ __('Előzmény:')  }}</h2>
 {!! $message->message !!}<br>
    <h2>{{ __('Válasz:') }} </h2>
 {!! $message->content !!}<br>


{{__('Üdvözlettel')}},<br>
{{__('Magyar Numzmatikai társulat ')}}
@endcomponent
