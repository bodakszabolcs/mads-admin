@component('mail::message')
    <h1> {{__('Kedves ').$to_name }},</h1>
    {!! $content !!}
@if(isset($link) && $link)
@component('mail::button',['url'=>env('APP_URL').$link]) {{$link_title}} @endcomponent
@endif
    {{__('Üdvözlettel')}},<br>
   MADS csapata!
@endcomponent
