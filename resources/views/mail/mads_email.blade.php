@component('mail::message')
    <h1> {{__('Kedves ').$email->recipient_name }},</h1>
    {!! $email->content !!}
@if(isset($email->link_button_enable) && $email->link_button_enable)
@component('mail::button',['url'=>env('APP_URL')]) Irány a mads.hu! @endcomponent
@endif
    {{__('Üdvözlettel')}},<br>
    {!!   $user->signature !!}
@endcomponent
