@component('mail::message')<h1> {{__('Kedves ').$name }},</h1>
    A {{date('Y.m.d.',strtotime($card->created_at))}} napon feltöltött Diák/jogviszony igazolásod @if($card->card_status==1)<b style="color:green">elfogadásra került!</b> @else <b style="color:red">Elutasitásra került!</b> @endif <br/>
    @if($card->card_status==2)Elutasítás indoka: {{$card->comment}}<br>
    @endif<br>
    Amennyiben kérdésed van, keress minket bátran!<br>
    {{__('Üdvözlettel')}},<br>
    {!!   $user->signature !!}
@endcomponent
