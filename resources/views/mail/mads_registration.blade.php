@component('mail::message')<h1> {{__('Kedves ').$student->name }},</h1>
    <p>A MADS- Márton Áron Iskolaszövetkezet kéri,hogy csatlakozz hozzá. </p>
    <p>Kattints az alábbi gombra és töltsd ki az adataidat!</p>
    <p>Készítsd elő a diákigazolványod, mert szükséges a regisztrációhoz!</p>
    @component('mail::button',['url'=>env('APP_URL').'/mads/register/'.$student->registration_token]) Tag regisztráció @endcomponent
<p><a href="https://mads.hu/storage/files/partners/Dokumentumok%20honlap%20VIKTOR/munka_baleset_es_tuzvedelmi_tajekoztato_MADS_Cegcsoport_230803.pdf" target="_blank">Itt olvashatod el a Munka-, baleset és tűzvédelmi tájékoztatónkat.</a></p>
<p>A regisztrációs link lejár: {{$student->registration_token_expire}}</p>
    <br/>
    <br/>
    <br/>
    {{__('Üdvözlettel')}},<br>
    A MADS csapata
@endcomponent
