@component('mail::message')
    <h1> {{__('Kedves ').$student->name }},</h1>
    {{$user->lastname}} {{$user->firstname}} szeretné ha kitöltenéd a beosztásodat!<br/>
    Cég: {{$project->company->name}}<br/>
    Tevékenység: {{$project->industry->name}}<br/>
    Beosztás: {{$json->interval}}<br/>
    <br>
@component('mail::button',['url'=>'https://mads.hu/profile/schedules-editor/'.$base64]) Kitöltöm a beosztást @endcomponent
    {{__('Üdvözlettel')}},<br>
    {!!   $user->signature !!}
@endcomponent
