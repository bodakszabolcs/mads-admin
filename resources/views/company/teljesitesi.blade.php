﻿
<table style="width:100%;margin-bottom:30px;margin-top:50px">

    <tr>

        <th style="text-align: left;vertical-align: top;font-size: 16px;padding-top:10px;width:200px">


            <b>Teljesítési Igazolás</b>
        </th>


    </tr>
</table>
<div class="paper" id="belepesi" style="width: 210mm; height: 327mm; font-size:15px;">
    <style>
        .paper table tr td
        {
            margin: 0px;
            padding: 5px;
            vertical-align: top;
            font-family: Arial;
            font-size: 12px;
            line-height: normal;
        }
        .paper{


            font-family: arial;
            font-size: 12px;
            text-align: justify;

        }
        .parag, .para10 {
            text-indent: 30pt;
            text-align: justify;
            line-height: initial;
            font-family: arial;
        }
        .paper div, .paper span
        {
            font-size:12px;
        }
        .paper table.data td{
            border:solid 1px #ccccc6;
            white-space: nowrap;
        }

    </style>

    <table class="data" style="width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;">
        <tbody>
        <tr>
            <td style="width:50%"><span class="bold" style="width:12%;font-size:12px;text-align:center"><b>Megbízó</b></span></td>
            <td style="width:50%"><span class="bold" style="width:12%;font-size:12px;text-align:center"><b>Megbízott</b></span></td>
        </tr>
        <tr>
            <td ><span style="font-size:12px" class="silver"><b>{{$company->name}}</b><br>
                {{$company->postal_address}}<br>
                Kijelölt képviselő: {{$company->delegate}}
                </span></td>
            <td  colspan="3"><span style="font-size:12px" class="silver"> <b>MADS Márton Áron Iskolaszövetkezet</b><br>
1092 Budapest, Erkel utca 3. fsz.<br>
Képviseli: Virág Viktor, igazgatósági tag

                </span></td>
        </tr>
        </tbody>
    </table>
    <p  style="font-size:12px">
        {{$company->completition_text}}, <b>{{\App\Helpers\ConstansHelper::$moths[(int)explode('-',$month)[1]]}}</b> hónapban a következő tevékenységekben:<br></p>
    <ul style="font-size:12px">
        @php $price = 0; @endphp
        @php $sum = \DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->where("company_id",$company->id)
                ->select(DB::raw("sum(sum_net+night_sum_net+festive_sum_net+sunday_sum_net+saturday_sum_net) as sum_net, sum(fix_company) as sum_fix,teaor, sum(hour+night_hour+festive_hour+sunday_hour+saturday_hour) as sum_hour"))->groupBy(DB::raw('LOWER(teaor)'))->get();
        @endphp
        @foreach($sum as $industry)




                @php $price+=$industry->sum_net+$industry->sum_fix; @endphp
        <li><b>{{$industry->sum_hour}} óra</b> {{$industry->teaor}}</li>

        @endforeach
    </ul>
    <p  style="font-size:12px">A Megbízó igénye szerint. <br>
        A Megbízott részére a <b>{{\App\Helpers\ConstansHelper::formatPrice($price)}} + ÁFA</b>  kifizethető.
    </p>
    @php
        $list = DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->groupBy('company_id')
             ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,

              sum(saturday_hour) as saturday_hour,
              sum(sunday_hour) as sunday_hour,
              sum(festive_hour) as festive_hour,
              sum(fix_company) as sum_fix,


              sum(hour) as sum_hour,
              CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
              CEILING(SUM(sum_net)) as sum_net,
              CEILING(SUM(sum_net)*1.27) as sum_gross,

              sum(night_hour) as night_hour,
              CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
              CEILING(Sum( night_sum_net))as night_sum_net,
              CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,

              sum(festive_hour) as festive_hour,
              CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
              CEILING(Sum( festive_sum_net))as festive_sum_net,
              CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,

              sum(sunday_hour) as sunday_hour,
              CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
              CEILING(Sum( sunday_sum_net))as sunday_sum_net,
              CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,

              sum(saturday_hour) as saturday_hour,
             CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
              CEILING(Sum( saturday_sum_net))as saturday_sum_net,
               CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
              payment_deadline,
              invoice_status,
                student_name,
	         teaor,
              CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross"))->orderBy('company_name', 'asc')->where('company_id',$company->id);
        @endphp
    @if($type=='student')
        @php $list =$list->groupBy('student_id')->groupBy(DB::raw('LOwER(teaor)'))->get();
        @endphp
        <table class="data" style="width: 100%; margin:0 0 0 0; font-size:12px;margin-top:60px">
            <thead>
            <tr style="background: silver;font-weight: bold">
                <td>Név</td>
                <td>Tevékenység</td>
                <td>Óra nappali</td>
                <td>Óra éjszaka</td>
                <td>Óra szombat</td>
                <td>Óra vasárnap</td>
                <td>Óra ünnep</td>
                <td>Össz óra</td>
                <td>Bér</td>
                <td>Fix bér</td>
                <td>Összesen</td>
            </tr>
            </thead>
            <tbody>
            @php
                $sum1=0;
                $sum2=0;
                $sum3=0;
                $sum4=0;
                $sum5=0;
                $sum6=0;
                $sum7=0;
                $sum8=0;
                $sum9=0;
            @endphp
        @foreach($list as $student)
            @if($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix >0)
            <tr>
                <td>{{$student->student_name}}</td>
                <td>{{$student->teaor}}</td>
                <td align="right">{{$student->sum_hour}}</td>
                <td align="right">{{$student->night_hour}}</td>
                <td align="right">{{$student->saturday_hour}}</td>
                <td align="right">{{$student->sunday_hour}}</td>
                <td align="right">{{$student->festive_hour}}</td>
                <td align="right">{{$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour}}</td>
                <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net)}}</td>
                <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($student->sum_fix)}}</td>
                <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix)}}</b></td>
            </tr>

            @php
                $sum1+=$student->sum_hour;
                $sum2+=$student->night_hour;
                $sum3+=$student->saturday_hour;
                $sum4+=$student->sunday_hour;
                $sum5+=$student->festive_hour;
                $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                $sum8+=$student->sum_fix;
                $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;
            @endphp
            @endif;
        @endforeach
            <tr style="background: silver;font-weight: bold">
                <td>Mindösszesen:</td>
                <td align="right"></td>
                <td align="right">{{$sum1}}</td>
                <td align="right">{{$sum2}}</td>
                <td align="right">{{$sum3}}</td>
                <td align="right">{{$sum4}}</td>
                <td align="right">{{$sum5}}</td>
                <td align="right">{{$sum6}}</td>
                <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($sum7)}}</td>
                <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($sum8)}}</td>
                <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($sum9)}}</b></td>
            </tr>
            </tbody>
        </table>
    @endif
    @if($type=='industry')
        @php $list =$list->groupBy(DB::raw('LOWER(teaor)'))->get();
        @endphp
        <table class="data" style="width: 100%; margin:0 0 0 0; font-size:12px;margin-top:60px">
            <thead>
            <tr style="background: silver;font-weight: bold">
                <td>Tevékenység</td>
                <td>Óra nappali</td>
                <td>Óra éjszaka</td>
                <td>Óra szombat</td>
                <td>Óra vasárnap</td>
                <td>Óra ünnep</td>
                <td>Össz óra</td>
                <td>Bér</td>
                <td>Fix bér</td>
                <td>Összesen</td>
            </tr>
            </thead>
            <tbody>
            @php
                $sum1=0;
                $sum2=0;
                $sum3=0;
                $sum4=0;
                $sum5=0;
                $sum6=0;
                $sum7=0;
                $sum8=0;
                $sum9=0;
            @endphp
            @foreach($list as $student)
                @if($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix >0)
                    <tr>
                        <td>{{$student->teaor}}</td>
                        <td align="right">{{$student->sum_hour}}</td>
                        <td align="right">{{$student->night_hour}}</td>
                        <td align="right">{{$student->saturday_hour}}</td>
                        <td align="right">{{$student->sunday_hour}}</td>
                        <td align="right">{{$student->festive_hour}}</td>
                        <td align="right">{{$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour}}</td>
                        <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net)}}</td>
                        <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($student->sum_fix)}}</td>
                        <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix)}}</b></td>
                    </tr>

                    @php
                        $sum1+=$student->sum_hour;
                        $sum2+=$student->night_hour;
                        $sum3+=$student->saturday_hour;
                        $sum4+=$student->sunday_hour;
                        $sum5+=$student->festive_hour;
                        $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                        $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                        $sum8+=$student->sum_fix;
                        $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;
                    @endphp
                @endif;
            @endforeach
            <tr style="background: silver;font-weight: bold">
                <td>Mindösszesen:</td>
                <td align="right">{{$sum1}}</td>
                <td align="right">{{$sum2}}</td>
                <td align="right">{{$sum3}}</td>
                <td align="right">{{$sum4}}</td>
                <td align="right">{{$sum5}}</td>
                <td align="right">{{$sum6}}</td>
                <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($sum7)}}</td>
                <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($sum8)}}</td>
                <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($sum9)}}</b></td>
            </tr>
            </tbody>
        </table>
    @endif
    @if($type=='site')
        @php $list =$list->groupBy('site_id')->groupBy(DB::raw('LOWER(teaor)'))->get();
        @endphp
        <table class="data" style="width: 100%; margin:0 0 0 0; font-size:12px;margin-top:60px">
            <thead>
            <tr style="background: silver;font-weight: bold">
                <td>Telephely</td>
                <td>Tevékenység</td>
                <td>Óra nappali</td>
                <td>Óra éjszaka</td>
                <td>Óra szombat</td>
                <td>Óra vasárnap</td>
                <td>Óra ünnep</td>
                <td>Össz óra</td>
                <td>Bér</td>
                <td>Fix bér</td>
                <td>Összesen</td>
            </tr>
            </thead>
            <tbody>
            @php
                $sum1=0;
                $sum2=0;
                $sum3=0;
                $sum4=0;
                $sum5=0;
                $sum6=0;
                $sum7=0;
                $sum8=0;
                $sum9=0;
            @endphp
            @foreach($list as $student)
                @if($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix >0)
                    <tr>
                        <td>{{$student->site_name}}</td>
                        <td>{{$student->teaor}}</td>
                        <td align="right">{{$student->sum_hour}}</td>
                        <td align="right">{{$student->night_hour}}</td>
                        <td align="right">{{$student->saturday_hour}}</td>
                        <td align="right">{{$student->sunday_hour}}</td>
                        <td align="right">{{$student->festive_hour}}</td>
                        <td align="right">{{$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour}}</td>
                        <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net)}}</td>
                        <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($student->sum_fix)}}</td>
                        <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix)}}</b></td>
                    </tr>

                    @php
                        $sum1+=$student->sum_hour;
                        $sum2+=$student->night_hour;
                        $sum3+=$student->saturday_hour;
                        $sum4+=$student->sunday_hour;
                        $sum5+=$student->festive_hour;
                        $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                        $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                        $sum8+=$student->sum_fix;
                        $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;
                    @endphp
                @endif;
            @endforeach
            <tr style="background: silver;font-weight: bold">
                <td>Mindösszesen:</td>
                <td></td>
                <td align="right">{{$sum1}}</td>
                <td align="right">{{$sum2}}</td>
                <td align="right">{{$sum3}}</td>
                <td align="right">{{$sum4}}</td>
                <td align="right">{{$sum5}}</td>
                <td align="right">{{$sum6}}</td>
                <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($sum7)}}</td>
                <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($sum8)}}</td>
                <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($sum9)}}</b></td>
            </tr>
            </tbody>
        </table>
    @endif

    @if($type=='uuid')
        @php $list =$list->groupBy('industry_id')->get();
        @endphp
        <table class="data" style="width: 100%; margin:0 0 0 0; font-size:12px;margin-top:60px">
            <thead>
            <tr style="background: silver;font-weight: bold">
                <td>Megnevezés</td>
                <td>Tevékenység</td>
                <td>Óra nappali</td>
                <td>Óra éjszaka</td>
                <td>Óra szombat</td>
                <td>Óra vasárnap</td>
                <td>Óra ünnep</td>
                <td>Össz óra</td>
                <td>Bér</td>
                <td>Fix bér</td>
                <td>Összesen</td>
            </tr>
            </thead>
            <tbody>
            @php
                $sum1=0;
                $sum2=0;
                $sum3=0;
                $sum4=0;
                $sum5=0;
                $sum6=0;
                $sum7=0;
                $sum8=0;
                $sum9=0;
            @endphp
            @foreach($list as $student)
                @if($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix >0)
                    <tr>
                        <td>{{$student->industry_name}}</td>
                        <td>{{$student->teaor}}</td>
                        <td align="right">{{$student->sum_hour}}</td>
                        <td align="right">{{$student->night_hour}}</td>
                        <td align="right">{{$student->saturday_hour}}</td>
                        <td align="right">{{$student->sunday_hour}}</td>
                        <td align="right">{{$student->festive_hour}}</td>
                        <td align="right">{{$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour}}</td>
                        <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net)}}</td>
                        <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($student->sum_fix)}}</td>
                        <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix)}}</b></td>
                    </tr>

                    @php
                        $sum1+=$student->sum_hour;
                        $sum2+=$student->night_hour;
                        $sum3+=$student->saturday_hour;
                        $sum4+=$student->sunday_hour;
                        $sum5+=$student->festive_hour;
                        $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                        $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                        $sum8+=$student->sum_fix;
                        $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;
                    @endphp
                @endif;
            @endforeach
            <tr style="background: silver;font-weight: bold">
                <td>Mindösszesen:</td>
                <td></td>
                <td align="right">{{$sum1}}</td>
                <td align="right">{{$sum2}}</td>
                <td align="right">{{$sum3}}</td>
                <td align="right">{{$sum4}}</td>
                <td align="right">{{$sum5}}</td>
                <td align="right">{{$sum6}}</td>
                <td align="right">{{ \App\Helpers\ConstansHelper::formatPrice($sum7)}}</td>
                <td align="right">{{\App\Helpers\ConstansHelper::formatPrice($sum8)}}</td>
                <td align="right"><b>{{\App\Helpers\ConstansHelper::formatPrice($sum9)}}</b></td>
            </tr>
            </tbody>
        </table>
    @endif
    <table style="width: 100%; margin:0 0 0 0; font-size:12px;margin-top:60px">
        <tbody>
        <tr style="padding-bottom:20px">
            <td align="right" style="padding-bottom:20px">Budapest,{{date('Y.m.d')}}</td>
            <td style="width:33%">.....................................</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            @php
                $imgData = base64_encode(\Illuminate\Support\Facades\Storage::disk('s3')->get("belyegzo.png"));
                $src = 'data: '.\Illuminate\Support\Facades\Storage::disk('s3')->getMimetype("belyegzo.png").';base64,'.$imgData;
                $imgSign= base64_encode(\Illuminate\Support\Facades\Storage::disk('s3')->get("alairas-viktor.png"));
                $srcSign = 'data: '.\Illuminate\Support\Facades\Storage::disk('s3')->getMimetype("alairas-viktor.png").';base64,'.$imgSign;
                @endphp
            <td colspan="2" style="text-align: center">.........................................................................</td>
            <td colspan="2" style="text-align: center">.........................................................................</td>
        </tr>
        <tr>

            <td colspan="2" style="text-align: center"><br>Megbízó Részéről</td>

            <td colspan="2" style="text-align: center"><img style="float:right;width:200px;rotation: 20deg;margin-top:-100px;opacity:0.8" src="{{$srcSign}}">
                <br><span style="margin-left:100px">MADS - Márton Áron Iskolaszövetkezet részéről<br><img style="float:right;width:200px;" src="{{$src}}">

</span></td>

        </tr>
        </tbody>
    </table>
</div>

