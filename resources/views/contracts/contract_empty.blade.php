<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;

    }
    ol {
        margin-left:12px;
        padding-left:5px;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:14px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px;;
    }
</style>
{!! $data !!}
