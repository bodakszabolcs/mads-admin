<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;

    }
    ol {
       margin-left:12px;
       padding-left:5px;
    }
    .paper{

        font-size: 11px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:12px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    .italic ol li,.italic ul li,.italic  td, .italic p{
        font-style: italic; !important;
        color: #575555;
    }
    i{
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:12px;;
    }
</style>
<div class="paper" style="font-size:12px">

    <p class="head"><strong><u>MEGB&Iacute;Z&Aacute;SI SZERZŐD&Eacute;S / <i>AGENCY CONTRACT</i></u></strong></p>
    <div style="position: relative">
<div style="width:49%;float:left" >
<p>
   <strong><u>I. BEVEZETŐ RENDELKEZ&Eacute;SEK</u></strong>
</p>
<p>A jelen szerződ&eacute;s az al&aacute;bbi felek k&ouml;z&ouml;tt j&ouml;tt l&eacute;tre:</p>
     <table>
         <tr>

             <td>egyrészről a<br/>
                 <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                 <strong>székhelye:</strong> {{$data->data['site']}}<br/>
                 <strong>adószáma:</strong> {{$data->data['tax']}}<br/>
                 <strong>képviseli:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position']}} @if(!$loop->last), @endif  @endforeach <br/>
                 mint Megbízó (a továbbiakban: <strong>megbízó</strong> vagy <strong>szolgáltatás fogadója</strong>),

             </td>
         </tr>
         <tr>

             <td>
                 másrészről a<br/>
                 <strong>MADS - GOLD Közérdekű Nyugdíjas Szövetkezet</strong><br/>
                 <strong>székhelye:</strong> 1092 Budapest, Erkel utca 3. földszint<br/>
                 <strong>adószáma:</strong>26248376-2-43.<br/>
                     <strong>képviseli:</strong> Lator Sándor az Igazgatóság tagja, önállóan<br/>
                 és Virág Viktor az Igazgatóság tagja, önállóan,<br/>
                 valamint Szántó Zsolt Sándor az Igazgatóság tagja, önállóan,<br/>
                 mint Megbízott (a továbbiakban: <strong>megbízott</strong> vagy <strong>iskolaszövetkezet</strong>),<br/>

             </td>
         </tr>
     </table>
<p>k&ouml;z&ouml;sen egy&uuml;tt: a <strong>felek</strong>, vagy <strong>szerződő felek</strong>.</p>
</div>
        <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
            <p>
                <strong><u>I. PREAMBLE</u></strong>
            </p>
            <p>This contract is made between the following parties: :</p>
            <table>
                <tr>

                    <td>on the one hand <br/>
                        <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                        <strong>seat:</strong> {{$data->data['site']}}<br/>
                        <strong>tax number:</strong> {{$data->data['tax']}}<br/>
                        <strong>represented by:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position']}} @if(!$loop->last), @endif  @endforeach <br/>
                        as Client (hereinafter referred to as <strong>client</strong> or <strong>receiver of service</strong>)
                        <br/>
                        <br/>

                    </td>
                </tr>
                <tr>

                    <td>
                        <br/>
                        and<br/>
                        <strong>MADS - GOLD Közérdekű Nyugdíjas Szövetkezet</strong><br/>
                        <strong>seat:</strong> ground floor, 3. Erkel street, Budapest, H-1092 Hungary<br/>
                        <strong>tax number:</strong> 26248376-2-43.<br/>
                        <strong>represented by:</strong> Sándor LATOR, Chairman of the Board,<br/>
                        independently;  Viktor VIRÁG, Member of the Board, independently; <br/>
                        and Zsolt SZÁNTÓ, Member of the Board, independently<br/>
                        as Agent (hereinafter referred to as the <strong>agent</strong> or <strong>students’ cooperation</strong>)
                        <br/>


                    </td>
                </tr>
            </table>
            <p>together jointly referred to as the <strong>parties</strong> or <strong>contracting parties</strong>.</p>
        </div>
        @php $sablon = \Modules\Campaig\Entities\ContractSablon::where('id',2)->first();
 $replaceHu=[
    '[industries]'=>'',
    '[e_szamla]'=>$data->data['e_szamla'],
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'<strong>'.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;t&oacute;l, hat&aacute;rozatlan </strong>':'<strong>'.date('Y. m. d.',strtotime($data->data['date'])).' napj&aacute;t&oacute;l '.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;ig, hat&aacute;rozott</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),

];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceHu['[industries]'].= $p['industry'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
 $replaceEn=[
    '[industries]'=>'',
    '[e_szamla]'=>$data->data['e_szamla'],
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'The present contract shall be effective from <strong>'.date('d',strtotime($data->data['effective_date'])).' of' .date('m. Y.',strtotime($data->data['effective_date'])).'</strong> for an <strong>indefinite</strong> period of time': 'The present contract shall be effective from <strong>'.date('d',strtotime($data->data['date'])).' of ' .date('m. Y.',strtotime($data->data['date'])).' for a definite period of time until ' .date('d',strtotime($data->data['effective_date'])).' of ' .date('m. Y.',strtotime($data->data['effective_date'])).'</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),

];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceEn['[industries]'].= optional($p)['industry_en'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
function replaceHu($text,$replace){

     return str_replace(array_keys($replace),array_values($replace),$text);
}
        @endphp
        @foreach($sablon['hu']['sections'] as $section)
            <div style="width:49%;float:left" >
                @php $actSection =$loop->iteration @endphp
                    <p><strong><u>{{$section['name']}}</u></strong></p>
                    @if($actSection ==4)

                    @else
                        <ol>
                    @endif
                    @foreach($section['items'] as $item)
                       @php $actItem =$loop->iteration @endphp
                       @if(($actSection ==2 && ($actItem ==1 || $actItem==12)) || ($actSection ==10 && $actItem==4) || ($actSection ==10 && $actItem==20) )
                         </ol>
                         <p><strong>{!! $item['content'] !!}</strong></p>
                         <ol>
                       @else
                          @if($actSection ==4 && $actItem ==1 )
                           <ol >
                              <li style="padding-left: 1px">A II. pontban le&iacute;rt &eacute;s teljes&iacute;tett feladatok&eacute;rt a megb&iacute;zottat:</li>
                             <ul>
                                 @foreach($data->data['industries'] as $ind)
                                     @if($ind['type'] == 1)<li><strong>{{$ind['industry']}} - {{$ind['price']}}</strong><strong>, - Ft / óra / fő + Áfa, azaz óránként, valamint fejenként </strong>{{$ind['price_text']}}<strong> magyar forint + Áfa megbízási díj illeti meg;</strong></li>@endif
                                     @if($ind['type'] == 2)<li><strong>{{$ind['industry']}} </strong><strong> -a feladatteljesítésre kijelölt iskolaszövetkezeti tagokat megillető <u>bruttó díj x (szorozva)</u>  {{$ind['price']}} ({{$ind['multiplier']}})Áfa összegű megbízási díj illeti meg;</strong></li>@endif
                                     @if($ind['type'] == 3)<li><strong>{{$ind['industry']}} </strong><strong> -a feladatteljesítésre kijelölt iskolaszövetkezeti tagokat megillető <u>bruttó díj / (osztva)</u>  {{$ind['price']}} ({{$ind['multiplier']}})Áfa összegű megbízási díj illeti meg;</strong></li>@endif
                                 @endforeach

                             </ul><p><u>Szerződő felek r&ouml;gz&iacute;tik, hogy a k&uuml;lső szolg&aacute;ltat&aacute;s teljes&iacute;t&eacute;s&eacute;ben r&eacute;szt vevő iskolasz&ouml;vetkezeti tagokat megillető megb&iacute;z&aacute;si d&iacute;j mindenkori &ouml;sszeg&eacute;ről megb&iacute;z&oacute; legk&eacute;sőbb a t&aacute;rgyh&oacute; első napj&aacute;ig &iacute;r&aacute;sban &eacute;rtes&iacute;ti a megb&iacute;zottat</u>.</p>

                          @endif
                          @if($actSection ==5 && $actItem ==6)
                                    <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                          @elseif($actSection ==4 && $actItem ==6)
                                    @if(optional($data->data)['e_szamla_approve'])
                                        <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                    @endif
                          @elseif($actSection ==13 && $actItem ==4)
                                    @if(optional($data->data)['previous_contract'] ==1)
                                        <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                    @endif
                          @elseif($actSection ==13 && $actItem == sizeof($section['items']))
                            </ol>
                            <p>{!!  replaceHu($item['content'],$replaceHu) !!}</p>
                            <ol>

                            @else
                                    @if(!empty($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content']))
                                <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                        @endif
                            @endif
                       @endif
                    @endforeach

               </ol>
            </div>
            <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
            <p><strong><u>{{$sablon['en']['sections'][$actSection-1]['name']}}</u></strong></p>
            @if($actSection ==4)

            @else
                <ol>
            @endif
                    @foreach($sablon['en']['sections'][$actSection-1]['items'] as $item)
                        @php $actItem =$loop->iteration @endphp
                        @if(($actSection ==2 && ($actItem ==1 || $actItem==12)) || ($actSection ==10 && $actItem==4) || ($actSection ==10 && $actItem==20)  )

                        </ol>
                        <p><strong>{!! $sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'] !!}</strong></p>
                        <ol>
                            @else
                                @if($actSection ==4 && $actItem ==1 )
                                    <ol style="padding-left: 1px">
                                        <li style="padding-left: 1px">For the performance of duties defined in Section II herein, agent shall be entitled to</li>

                                    <ul >
                                        @foreach($data->data['industries'] as $ind)

                                            @if($ind['type'] == 1)<li><strong>{{optional($ind)['industry_en']}} - HUF {{$ind['price']}} / hour/ person + VAT, namely {{$ind['price']}} Hungarian Forints per hour and per person + VAT as commission fee;</strong></li>@endif
                                            @if($ind['type'] == 2)<li><strong>{{optional($ind)['industry_en']}} - a commission fee equal to the gross fee cooperation appointed for the performance of duties hereunder x (multiplied by) {{$ind['price']}} ({{$ind['multiplier']}}) + (plus) VAT;</strong>@endif
                                            @if($ind['type'] == 3)<li><strong>{{optional($ind)['industry_en']}} - a commission fee equal to the gross fee due to the members of students’ cooperation appointed for the performance of duties hereunder x (divisor by) {{$ind['price']}} ({{$ind['divisor']}}) + (plus) VAT;@endif

                                        @endforeach

                                    </ul>

                                    <p><u>Contracting parties hereby agree that client shall notify agent of the prevailing amount of commission fee due for the members of students’ cooperation involved in the performance of external services in writing, latest by the first day of the given month</u>.</p>
                                @endif
                                @if($actSection ==5 && $actItem ==6)
                                    <li>{!!  replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                @elseif($actSection ==4 && $actItem ==6)
                                    @if(optional($data->data)['e_szamla_approve'])
                                        <li>{!!  replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                    @endif
                                @elseif($actSection ==13 && $actItem ==4)
                                    @if(optional($data->data)['previous_contract'] ==1)
                                        <li>{!!  replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                    @endif
                                @elseif($actSection ==13 && $actItem == sizeof($section['items']))
                        </ol>
                        <p>{!!  replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</p>
                        <ol>

                            @else
                                @if(!empty($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content']))
                                <li>{!!  replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                @endif
                            @endif
                            @endif
                            @endforeach
                        </ol>
                </div>
            <div style="width:100%;position:relative"></div>
                    @endforeach

        </div>


            <div style="width:49%;float:left" >
        Felek megállapodnak, hogy bármilyen jogi, vagy értelmezési kétség, eltérés esetén a magyar nyelvű verziót tekintik irányadónak.
            </div>
            <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
   The Parties agree that in case of any legal or interpretation dispute regarding this document the Hungarian language version shall prevail.
            </div>



    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>&nbsp;{{$data->data['name']}}</p>
                <p><strong>Megb&iacute;z&oacute; /<i>Client</i></strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>MADS - GOLD Közérdekű Nyugdíjas Szövetkezet</p>
                <p><strong>Megb&iacute;zott/ <i>Agent</i></strong></p>
            </td>
        </tr>
    </table>
</div>
