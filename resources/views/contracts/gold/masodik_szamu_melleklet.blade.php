<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{



        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:13px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
</style>
<div class="paper" style="font-size:12px">
    <div style="text-align: right;color:#3c4043">
        <strong>szerződés azonosítója:<br>
            {{$data->data['number']}}
        </strong>
    </div>
    <div style="text-align: right">
        <strong>2. sz&aacute;m&uacute; Mell&eacute;klet</strong>
    </div>
                <p class="head"><strong>Felek meg&aacute;llapod&aacute;sa alapj&aacute;n, a k&ouml;z&ouml;s adatkezel&eacute;s k&ouml;r&eacute;ben az al&aacute;bbi szem&eacute;lyes adatokat kezelik a felek:</strong></p>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <p><strong>&Eacute;rintettek:</strong></p>
                        </td>
                        <td>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sre jelentkező term&eacute;szetes szem&eacute;lyek, iskolasz&ouml;vetkezeti tagok</p>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sben r&eacute;sztvevő iskolasz&ouml;vetkezeti tagok</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>K&ouml;z&ouml;s adatkezel&eacute;ssel &eacute;rintett szem&eacute;lyes adatok lehets&eacute;ges k&ouml;re</strong></p>
                        </td>
                        <td>
                            @if($data->data['second']['name'])<p><strong>- </strong><strong>n&eacute;v</strong></p>@endif
                            @if($data->data['second']['address'])<p><strong>- </strong><strong>lakc&iacute;m</strong></p>@endif
                            @if($data->data['second']['birthdate'])<p><strong>- </strong><strong>sz&uuml;let&eacute;si hely, idő</strong></p>@endif
                            @if($data->data['second']['phone'])<p><strong>- </strong><strong>telefonsz&aacute;m</strong></p>@endif
                            @if($data->data['second']['email'])<p><strong>- </strong><strong>e-mail c&iacute;m</strong></p>@endif
                            @if($data->data['second']['nationality'])<p><strong>- </strong><strong>&aacute;llampolg&aacute;rs&aacute;g</strong></p>@endif
                            @if($data->data['second']['gender'])<p><strong>- </strong><strong>nem</strong></p>@endif
                            @if($data->data['second']['cv'])<p><strong>- </strong><strong>&ouml;n&eacute;letrajz</strong></p>@endif
                            @if($data->data['second']['picture'])<p><strong>- </strong><strong>f&eacute;nyk&eacute;p</strong></p>@endif
                            @if($data->data['second']['school'])<p><strong>- </strong><strong>jelenlegi, &eacute;s volt oktat&aacute;si int&eacute;zm&eacute;nyekre vonatkoz&oacute; adatok</strong></p>@endif
                            @if($data->data['second']['language'])<p><strong>- </strong><strong>nyelvismeret</strong></p>@endif
                            @if($data->data['second']['trainingInfo'])<p><strong>- </strong><strong>k&eacute;pz&eacute;si inform&aacute;ci&oacute;k</strong></p>@endif
                            @if($data->data['second']['driverLicense'])<p><strong>- </strong><strong>jogos&iacute;tv&aacute;ny t&iacute;pusa</strong></p>@endif
                            @if($data->data['second']['reference'])<p><strong>- </strong><strong>referenci&aacute;k</strong></p>@endif
                                @if($data->data['second']['reference'])<p><strong>- </strong><strong>jelenl&eacute;ti &iacute;v</strong></p>@endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s jogalapja:</strong></p>
                        </td>
                        <td>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sre jelentkező iskolasz&ouml;vetkezeti tagok eset&eacute;n a GDPR 6. cikk (1) bekezd&eacute;s a) pontja szerint az &eacute;rintett megfelelő t&aacute;j&eacute;koztat&aacute;son alapul&oacute; &ouml;nk&eacute;ntes hozz&aacute;j&aacute;rul&aacute;sa az adatkezel&eacute;shez</p>
                            <p><strong>- </strong>megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sben r&eacute;sztvevő iskolasz&ouml;vetkezeti tagok eset&eacute;ben a megb&iacute;z&oacute; vonatkoz&aacute;s&aacute;ban a GDPR 6. cikk (1) bekezd&eacute;s f) pontja szerint az adatkezel&eacute;s az adatkezelő vagy egy harmadik f&eacute;l jogos &eacute;rdekeinek &eacute;rv&eacute;nyes&iacute;t&eacute;s&eacute;hez sz&uuml;ks&eacute;ges, m&iacute;g a megb&iacute;zott vonatkoz&aacute;s&aacute;ban a GDPR 6. cikk (1) bekezd&eacute;s c) pontja szerint az adatkezel&eacute;s az adatkezelőre vonatkoz&oacute; jogi k&ouml;telezetts&eacute;g teljes&iacute;t&eacute;s&eacute;hez sz&uuml;ks&eacute;ges</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s c&eacute;ljai:</strong></p>
                        </td>
                        <td>
                            <p>- k&uuml;lső szolg&aacute;ltat&aacute;s teljes&iacute;t&eacute;se</p>
                            <p>- foglalkoztat&aacute;sra ir&aacute;nyul&oacute; jogviszony l&eacute;trej&ouml;tt&eacute;re vonatkoz&oacute; aj&aacute;nlat k&uuml;ld&eacute;se</p>
                            <p>- megb&iacute;z&oacute; r&eacute;sz&eacute;re megfelelő szakmai tud&aacute;ssal rendelkező jel&ouml;lt aj&aacute;nl&aacute;sa</p>
                            <p>- profilalkot&aacute;s</p>
                            <p>- kapcsolattart&aacute;s</p>
                            <p><strong>- </strong>azonos&iacute;t&aacute;s</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatt&aacute;rol&aacute;s időtartama: </strong></p>
                        </td>
                        <td>
                            <p>- sikeresen pályázó nyugdíjas szövetkezeti tagok esetében a
                                megbízott a vonatkozó jogszabályokban előírt határidőig tárolja
                                a vele tagsági viszonyban álló nyugdíjas szövetkezeti tagok
                                adatait</p>
                            <p>- amennyiben az öregségi nyugdíjban vagy átmeneti
                                bányászjáradékban részesülő nyugdíjas szövetkezeti tagja egy
                                adott, a megbízó által tett foglalkoztatásra irányuló jogviszonyra
                                vonatozó ajánlattal kapcsolatos adatkezeléshez járult hozzá, az
                                adatkezelés időtartama a pályázó visszautasítását követő 15
                                napig tart.</p>
                            <p>- amennyiben az öregségi nyugdíjban vagy átmeneti
                                bányászjáradékban részesülő nyugdíjas szövetkezeti tag a
                                megbízó, vagy a megbízott adatbázisban történő készletező
                                adatkezeléshez is hozzájárult, az adatkezelés időtartama a
                                hozzájárulás visszavonásáig, de maximum 5 évig tart.</p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
    <p></p>
    <p></p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>{{optional($data)->data['name']}}</p>
                <p><strong>Megb&iacute;z&oacute;</strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>MADS - GOLD Közérdekű Nyugdíjas Szövetkezet</p>
                <p><strong>Megb&iacute;zott</strong></p>
            </td>
        </tr>
    </table>


</div>
