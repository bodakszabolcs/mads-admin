<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:14px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
    </style>
<div class="paper" style="font-size:12px">
        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója:<br>
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            <strong>1. sz&aacute;m&uacute; Mell&eacute;klet</strong>
        </div>
        <p class="head" ><strong>ESETI MEGBÍZÁS</strong></p>
    <div class="">
        <strong>Sorszám: {{$data->data['number']}}</strong>
    </div>
    <p>
        Amely a {{$data->data['name']}} (képviseli: {{$data->data['persons'][0]['name']}}) mint
        @if($data->data['is_private'] == 1)
            <strong>Megbízó</strong>
        @else
            <strong>Kölcsönvevő</strong>
        @endif

        , és a <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság</strong> (képviseli: Ember Ágnes ügyvezető, önállóan) mint
        @if($data->data['is_private'] == 1)
            <strong>Megbízott</strong>
        @else
            <strong>Kölcsönbeadó</strong>
        @endif
        között a <strong>{{explode('-',$data->data['date'])[0]}}. év {{explode('-',$data->data['date'])[1]}} hónap {{explode('-',$data->data['date'])[2]}}.</strong> napján létrejött munkaerő-kölcsönzési Szerződés
        elválaszthatatlan része, és kizárólag azzal együtt hatályos.
    </p>

        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>1. A Kölcsönvevő által foglalkoztatni kívánt munkavállalók létszáma:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['person_count']}} fő</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>2. A munkavégzés helye:</strong></p>
                </td>
                <td>
                    <p>{{$data->data['zeroth']['location']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>3.Az elvégzendő munka leírása:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['description']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>4. Az elvégzendő munkára vonatkozó alkalmassági feltételek:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['preference']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>5. A munkaerő-kölcsönzés időtartama:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['time']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>6. A munkavégzésnél irányadó munkarend és a munkaidő-beosztás jellege:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['work_time']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>7.A munkáltatói jogkör gyakorlójának megnevezése:</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['company']}}</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>8. A kölcsönzött munkavállalók munkába állásának előfeltételeként meghatározott további elvárások (pl.
                            erkölcsi bizonyítvány bemutatása):</strong></p>
                </td>
                <td >
                    <p>{{$data->data['zeroth']['conditions']}}</p>
                </td>
            </tr>
            </tbody>
        </table>
         <p>Kelt, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
        <br/>
        <br/>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">

                    <p>____________________________________________________</p>
                    <p><strong>MADS WORK Kft.</strong></p>
                    <p>képviseli: Ember Ágnes ügyvezető, önállóan</p>
                    <p>  @if($data->data['is_private'] == 1)
                            <strong>Megbízott</strong>
                        @else
                            <strong>Kölcsönbeadó</strong>
                        @endif</p>

            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p><strong>{{$data->data['name']}}</strong></p>
                <p>képviseli: {{$data->data['persons'][0]['name']}} {{$data->data['persons'][0]['position']}}, önállóan</p>
                <p> @if($data->data['is_private'] == 1)
                        <strong>Megbízó</strong>
                    @else
                        <strong>Kölcsönbevevő</strong>
                    @endif</p>
            </td>
        </tr>
    </table>
    @if($data->data['is_private'] != 1)
    <br/>
    <hr/>
    <br/>
    <br/>
    <div style="text-align: right">
        <strong>2. sz&aacute;m&uacute; Mell&eacute;klet</strong>
    </div>
    <p>A 118/2001. (VI. 30.) Korm. rendelet szerint a munkaerő-kölcsönzési tevékenység ellátását a nyilvántartásba vételével a
        Kölcsönbeadó részére engedélyező illetékes kormányhivatali határozat másolata.</p>

    @endif

</div>
