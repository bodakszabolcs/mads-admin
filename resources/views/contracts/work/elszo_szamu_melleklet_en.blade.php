<style>
    table tr td
    {
        margin: 0px;
        padding: 3px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:11px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    i {
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
    </style>
<div class="paper" style="font-size:12px">
        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója / <i>contract No.</i>:
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            @if($data->data['is_private'] == 1) <strong>2. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 2</i></strong>@else <strong>3. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 3</i></strong> @endif
        </div>
        <p class="head" style="margin-top:25px" ><strong>Jogi személy szerződő partnerek természetes személy képviselőinek, kapcsolattartóinak elérhetőségi adatai a

                @if($data->data['is_private'] == 1) MEGBÍZÓ @else KÖLCSÖNVEVŐ @endif kapcsolattartója részéről</strong><br/>
            <strong><i>Managing contact details of people’s representatives and contacts of Legal entity contracting partners by the

                    @if($data->data['is_private'] == 1) CLIENT&#39;s @else  USER UNDERTAKING&#39;s @endif contact person</i> </strong></p>

        @for($i =0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/>
                            <i>Name of contact person:</i>
                        </strong></p>
                </td>
                <td>
                    <p></p>
                    <p><strong>{{$data->data['first']['contacts'][$i]['name']}}</strong></p>
                    <p></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/>
                            <i>Name of contact person:</i>
                        </strong></p>
                </td>
                <td>
                    <p></p>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['name']}}</strong></p>
                    <p></p>
                </td>
                @endif
            </tr>
            <tr>
                <td>
                    <p></p>
                    <p><strong>telefon-, fax sz&aacute;ma:<br/>
                            <i>Phone/fax number:</i></strong></p>
                    <p></p>
                </td>
                <td>
                    <p>@if(isset($data->data['first']['contacts'][$i]['fax']) && !empty($data->data['first']['contacts'][$i]['fax']))<strong>{{$data->data['first']['contacts'][$i]['fax']}}</strong>@endif</p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p></p>
                    <p><strong>telefon-, fax sz&aacute;ma:<br/>
                            <i>Phone/fax number:</i></strong></p>
                    <p></p>
                </td>
                <td>
                    <p><strong>@if(isset($data->data['first']['contacts'][$i+1]['fax']) && !empty($data->data['first']['contacts'][$i+1]['fax'])){{$data->data['first']['contacts'][$i+1]['fax']}}@endif</strong></p>
                </td>
                @endif
            </tr>

            <tr>
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma: <br/>
                            <i>Mobile number:</i>
                        </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['phone']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma:<br/>
                            <i>Mobile number:</i> </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['phone']}}</strong></p>
                </td>
                @endif
            </tr>
            <tr>
                <td>

                    <p><strong>e-mail c&iacute;me:<br/>
                            <i> E-mail address:</i></strong></p>

                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['email']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>

                    <p><strong>e-mail c&iacute;me:<br/>
                            <i>E-mail address:</i></strong></p>

                </td>

                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['email']}}</strong></p>
                </td>
                @endif
            </tr>
            </tbody>
        </table>
        @endfor
        <p class="head"><strong>Információk / <i>Information</i>:</strong></p>

        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>adatkezelő:<br/><i>Data controller</i></strong></p>
                </td>
                <td >
                    <p>MADS - WORK Szolgáltató Korlátolt Felelősségű Társaság</p>

                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezelő honlapja:<br/><i>The data controller’s website:</i></strong></p>
                </td>
                <td >
                    <p>www.madsgold.hu</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:<br/><i>Purpose of data processing:</i></strong></p>
                </td>
                <td >
                    <p>szerződés teljesítése, üzleti kapcsolattartás.<br/><i>contract fulfilment, business communications.</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                </td>
                <td >
                    <p>adatkezelő jogos érdeke<br/><i>legitimate interest of the Data controller</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:<br/><i>Recipients of the personal data:</i></strong></p>
                </td>
                <td >
                    <p>a   @if($data->data['is_private'] == 1) Megbízott  @else Kölcsönbeadó @endif ügyfélkiszolgálással kapcsolatos
                        feladatokat ellátó munkavállalói; a  @if($data->data['is_private'] == 1) Megbízónál @else Kölcsönvevőnél @endif
                        foglalkoztatott kölcsönzött munkavállalók<br/><i> @if($data->data['is_private'] == 1) Agent&#39;s  @else Staffing Agency&#39;s @endif employees providing customer service
                            functions; temporary agency workers employed by User
                            Undertaker</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:<br/><i>duration of storage of personal data:</i></strong></p>
                </td>
                <td >
                    <p>Az üzleti kapcsolat, illetve az érintett képviselői
                        minőségének fennállását követő 5 évig.<br/><i>For a period of 5 years after the business contact or the
                            person&#39;s capacity as a representative has been fulfilled. </i></p>
                </td>
            </tr>
            </tbody>
        </table>

    <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l: / <i> Information on the rights of the data subject:</i></strong></p>
    <table class="">
        <tr>
            @if($data->data['is_private'] == 1)
                <td style="width: 50%">Önnek joga van kérelmezni az adatkezelőtől az Önre vonatkozó személyes adatokhoz való hozzáférést, azok helyesbítését, törlését vagy kezelésének korlátozását, és tiltakozhat az ilyen személyes adatok kezelése ellen, valamint a joga van az adathordozhatósághoz.
                    <br/>Joga van megbízóhoz vagy bírósághoz fordulna, a felügyeleti hatósághoz (Nemzeti Adatvédelmi és Információszabadság Hatóság) panaszt benyújtani.
                    <br/>További információk az adatkezelő honlapján elérhető Adatkezelési tájékoztatóban olvashatók.
                    <br/><br/>
                    <strong>A fenti információkat és adatvédelmi tájékoztatást, valamint a személyes adataim fenti célú kezelését tudomásul vettem.</strong>
                </td>
                <td style="width: 50%">
                    <i>
                        You have the right to request from the controller access to, rectification, erasure or restriction of processing of personal data relating to you and to object to the processing of such personal data and you have the right to data portability.
                        <br/>You have the right to contact to the Client or to the court, or file a complaint with the Supervisory Authority (National Authority for Data Protection and Freedom of Information)
                        <br/>Further information can be found in the Privacy Policy available on the Controller’s website.
                        <br/><br/>
                        <strong>I have noted the above information and the privacy statements and the processing of my personal data for such purposes.</strong>
                    </i>

                </td>
            @else
            <td style="width:50%">
                Önnek joga van kérelmezni az adatkezelőtől az Önre
                vonatkozó személyes adatokhoz való hozzáférést, azok
                helyesbítését, törlését vagy kezelésének korlátozását, és
                tiltakozhat az ilyen személyes adatok kezelése ellen,
                valamint a joga van az adathordozhatósághoz.
                <br/> Joga van a Kölcsönbeadóhoz, a bírósághoz fordulni vagy
                a felügyeleti hatósághoz (Nemzeti Adatvédelmi és
                <br/> Információszabadság Hatóság) panaszt benyújtani.
                További információk az adatkezelő honlapján elérhető
                <br> Adatkezelési tájékoztatóban olvashatók.

                <br/><br/>
                <strong>A fenti információkat és adatvédelmi tájékoztatást, valamint a személyes adataim fenti célú kezelését tudomásul vettem.</strong>
            </td>
            <td style="width:50%">
                <i>
                    You have the right to request from the controller access to,
                    rectification, erasure or restriction of processing of
                    personal data relating to you and to object to the
                    processing of such personal data and you have the right to
                    data portability.
                    <br/>You have the right to contact to the Staffing Agency or to
                    the court, or file a complaint with the Supervisory Authority
                    (National Authority for Data Protection and Freedom of
                    Information)
                    <br/>Further information can be found in the Privacy Policy
                    available on the Controller’s website.
                    <br/><br/>
                    <strong>I have noted the above information and the privacy
                        statements and the processing of my personal data for
                        such purposes.</strong>

                </i>
            </td>
                @endif
        </tr>
    </table>

    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    @for($i=0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                <p>____________________________________________________</p>
                <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i+1]['name']}}</p>
                @else
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
        </tr>
    </table>
    @endfor
        <pagebreak></pagebreak>




        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója / <i>contract No</i>:{{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            @if($data->data['is_private'] != 1) <strong>3. sz&aacute;m&uacute; Mell&eacute;klet/ <i>Annex No. 3</i></strong> @else <strong>2. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 2</i></strong> @endif
        </div>

        <p class="head" style="margin-top:25px"><strong>Jogi személy szerződő partnerek természetes személy képviselőinek, kapcsolattartóinak elérhetőségi adatai

                @if($data->data['is_private'] == 1) MEGBÍZOTT  @else KÖLCSÖNBEADÓ @endif kapcsolattartója részéről<br/>
            <i>Managing contact details of people’s representatives and contacts of Legal entity contracting partners by the

                @if($data->data['is_private'] == 1) Agent&#39;s  @else STAFFING AGENCY&#39;s @endif contact person</i></strong>
        </p>
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>Vir&aacute;g Viktor</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                </td>
                <td >
                    <p>+36 (1) 700 1773</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->fax}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                </td>
                <td >
                    <p>+36 (20) 979-7927</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                </td>
                <td >
                    <p>virag.viktor@mads.hu</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2; $i++)
        @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                <td >
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                </td>
                @endif
            </tr>
            <td >
                <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
            </td>
            <td >
                <p>{{optional($user)->fax}}</p>
            </td>
            @if(isset($data->data['first']['madsContact'][$i+1]))
                @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp

                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->fax}}</p>
                </td>
            @endif
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->phone}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->email}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
      @endfor

    <p class="head"><strong>Inform&aacute;ci&oacute;k / <i>Information</i>:</strong></p>

        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>adatkezelő:<br/><i>Data controller:</i></strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['controller']}}<br/><i>{{optional($data->data['first'])['controller_en']}}</i></strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezelő honlapja:<br/><i>The data controller’s website:</i></strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['website']}}</strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:<br/><i>Purpose of data processing:</i></strong></p>
                </td>
                <td >
                    <p>szerződés teljesítése, üzleti kapcsolattartás.<br/><i>contract fulfilment, business communications.</i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                </td>
                <td >
                    <p>törvényes képviselő tekintetében szerződés teljesítése, míg
                        kijelölt kapcsolattartó tekintetében az adatkezelő jogos
                        érdeke<br/><i>the performance of a contract in case of a legal
                            representative and the legitimate interest of the controller in
                            case of a designated contact person</i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:<br/><i>Recipients of the personal data:</i></strong></p>
                </td>
                <td >
                    <p>a Társaság ügyfélkiszolgálással kapcsolatos feladatokat
                        ellátó munkavállalói;<br/><i>Company&#39;s employees providing customer service
                            functions; </i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:<br/><i>duration of storage of personal data:</i></strong></p>
                </td>
                <td >
                    <p>Az üzleti kapcsolat, illetve az érintett képviselői
                        minőségének fennállását követő 5 évig.<br/><i>For 5 years after the existence of the business relationship
                            or the status of the contact person concerned.</i></p>
                </td>
            </tr>
            </tbody>
        </table>

    <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l / <i>Information on the rights of the data subject:</i></strong></p>
    <table>
        <tr>
            <td style="width: 50%">Önnek joga van kérelmezni az adatkezelőtől az Önre
                vonatkozó személyes adatokhoz való hozzáférést, azok
                helyesbítését, törlését vagy kezelésének korlátozását, és
                tiltakozhat az ilyen személyes adatok kezelése ellen,
                valamint a joga van az adathordozhatósághoz.
                <br/>Joga van Kölcsönvevőhöz vagy bírósághoz fordulna, a
                felügyeleti hatósághoz (Nemzeti Adatvédelmi és
                Információszabadság Hatóság) panaszt benyújtani.
                <br/> További információk az adatkezelő honlapján elérhető
              Adatkezelési tájékoztatóban olvashatók.
                   <br/><br/>
                <strong>A fenti információkat és adatvédelmi tájékoztatást, valamint a személyes adataim fenti célú kezelését tudomásul vettem.</strong>
            </td>
            <td style="width: 50%">
                <i>
                    You have the right to request from the controller access to,
                    rectification, erasure or restriction of processing of
                    personal data relating to you and to object to the
                    processing of such personal data and you have the right to
                    data portability.
                    <br/>You have the right to contact to the User Undertaking or to
                    the court, or file a complaint with the Supervisory Authority
                    (National Authority for Data Protection and Freedom of
                    Information)
                    <br/> Further information can be found in the Privacy Policy
                    available on the Controller’s website.
                  <br/><br/>
                <strong>I have noted the above information and the privacy statements and the processing of my personal data for such purposes.</strong>
                </i>

            </td>
        </tr>
    </table>
    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
        <table class="signature" style="border:none">
            <tr>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        <p>____________________________________________________</p>
                        <p>Ember Ágnes</p>
                    @endif
                </td>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                        <p>____________________________________________________</p>
                        <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                    @else
                        <p>____________________________________________________</p>
                        <p>Ember Ágnes</p>
                    @endif
                </td>
            </tr>
        </table>
        @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2;$i++)
            <table class="signature" style="border:none">
                <tr>
                    <td style="text-align: center;border:none;width:50%">
                        @if(isset($data->data['first']['madsContact'][$i]))
                            @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
                            <p>____________________________________________________</p>

                            <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                        @endif
                    </td>
                    <td style="text-align: center;border:none;width:50%">
                        @if(isset($data->data['first']['madsContact'][$i+1]))
                            @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                            <p>____________________________________________________</p>
                            <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                        @endif
                    </td>
                </tr>
            </table>
        @endfor

</div>
