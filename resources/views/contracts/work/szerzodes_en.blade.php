<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;

    }
    ol {
       margin-left:12px;
       padding-left:5px;
    }
    .paper{

        font-size: 11px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:12px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    .italic ol li,.italic ul li,.italic  td, .italic p{
        font-style: italic; !important;
        color: #575555;
    }
    i{
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:12px;;
    }
</style>
<div class="paper" style="font-size:12px">

    <p class="head"><strong><u>MUNKAERŐ-KÖLCSÖNZÉSI SZERZŐDÉS /  <i>TEMPORARY EMPLOYEE STAFFING AGREEMENT</i></u></strong></p>
    <div style="position: relative">
<div style="width:49%;float:left" >
<p>
   <strong><u>I. BEVEZETŐ RENDELKEZ&Eacute;SEK</u></strong>
</p>
<p>A jelen szerződ&eacute;s az al&aacute;bbi felek k&ouml;z&ouml;tt j&ouml;tt l&eacute;tre:</p>
     <table>
         <tr>

             <td>egyrészről a<br/>
                 <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                 <strong>székhelye:</strong> {{$data->data['site']}}<br/>
                 <strong>adószáma:</strong> {{$data->data['tax']}}<br/>
                 <strong>képviseli:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position']}} @if(!$loop->last), @endif  @endforeach <br/>
                 mint Kölcsönvevő (a továbbiakban:<strong> Kölcsönvevő</strong>),

             </td>
         </tr>
         <tr>
             <td>másrészről a <br/>
                 <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság </strong><br/>
                 <strong>székhelye:</strong> 1092 Budapest, Erkel Ferenc utca 3. földszint <br/>
                 <strong>cégjegyzékszáma:</strong> Cg.01-09-949538 <br/>
                 <strong>adószáma:</strong> 23026517-2-43. <br/>
                 <strong>pénzforgalmi jelzőszáma:</strong> 10404089-50526752-84561008
                 Kereskedelmi és Hitelbank Zrt.<br/>
                 <strong>nyilvántartási száma:</strong> BP/0701/16797-1/2016-1641 <br/>
                 <strong> képviseli:</strong> Ember Ágnes ügyvezető, önállóan,
                 mint Kölcsönbeadó (a továbbiakban:  <strong>Kölcsönbeadó</strong>),

                 <br/>

             </td>
         </tr>
     </table>
<p>k&ouml;z&ouml;sen egy&uuml;tt: a <strong>felek</strong>, vagy <strong>szerződő felek</strong>.</p>
</div>
        <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
            <p>
                <strong><u>I. PREAMBLE</u></strong>
            </p>
            <p>This contract is made between the following parties: :</p>
            <table>
                <tr>

                    <td>on the one hand <br/>
                        <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                        <strong>seat:</strong> {{$data->data['site']}}<br/>
                        <strong>tax number:</strong> {{$data->data['tax']}}<br/>
                        <strong>represented by:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position_en']}} @if(!$loop->last), @endif  @endforeach <br/>
                        as User Undertaking (hereinafter as <strong>User Undertaking</strong>),
                        <br/>
                        <br/>

                    </td>
                </tr>
                <tr>
                    <td>and  <br/>
                        <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság </strong><br/>
                        <strong>seat:</strong> ground floor, 3. Erkel street, Budapest, H-1092 Hungary<br/>
                        <strong>registered number:</strong> Cg.01-09-949538 <br/>
                        <strong>tax number:</strong>23026517-2-43. <br/>
                        <strong>bank account number:</strong> 10404089-50526752-84561008
                        Kereskedelmi és Hitelbank Zrt.<br/>
                        <strong>registration number:</strong> BP/0701/16797-1/2016-1641 <br/>
                        <strong> represented by:</strong>Ágnes Ember, managing director, independently;
                        as <strong>Staffing Agency</strong>),

                        <br/>

                    </td>
                </tr>
            </table>
            <p>together jointly referred to as the <strong>parties</strong> or <strong>contracting parties</strong>.</p>
        </div>
        <div style="width:100%;position:relative"></div>
        @php $sablon = \Modules\Campaig\Entities\ContractSablon::where('id',3)->first();
 $replaceHu=[
    '[industries]'=>'',
    '[e_szamla]'=>'<strong>'.$data->data['e_szamla'].'</strong>',
    '[price]'=>$data->data['price'],
    '[price_name]'=>$data->data['price_name'],
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'<strong>'.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;t&oacute;l, hat&aacute;rozatlan </strong>':'<strong>'.date('Y. m. d.',strtotime($data->data['date'])).' napj&aacute;t&oacute;l '.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;ig, hat&aacute;rozott</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),
     '[contact_info]'=>"<p class='contact_info'><strong>Ember Ágnes</strong>
    <br/>Tel/Fax: <strong>+36 (1) 700 1773</strong>
    <br/>Mobil: <strong>+36 (20) 959 8997</strong>
    <br/>E-mail: <strong>ember.agnes@mads.hu</strong>
    </p>",
    '[contact_info_company]'=>"<p class='contact_info'><strong>".$data->data['persons'][0]['name']."</strong>
<br/>Tel/Fax: <strong>".optional($data->data['persons'][0])['fax']."</strong>
<br/>Mobil: <strong>".optional($data->data['persons'][0])['phone']."</strong>
<br/>E-mail: <strong>".optional($data->data['persons'][0])['email']."</strong>
</p>"
];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceHu['[industries]'].= $p['industry'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
 $replaceEn=[
    '[industries]'=>'',
    '[e_szamla]'=>'<strong>'.$data->data['e_szamla'].'</strong>',
    '[price]'=>$data->data['price'],
    '[price_name]'=>$data->data['price_name_en'],
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'The present contract shall be effective from <strong>'.date('d',strtotime($data->data['effective_date'])).' of' .date('m. Y.',strtotime($data->data['effective_date'])).'</strong> for an <strong>indefinite</strong> period of time': 'The present contract shall be effective from <strong>'.date('d',strtotime($data->data['date'])).' of ' .date('m. Y.',strtotime($data->data['date'])).' for a definite period of time until ' .date('d',strtotime($data->data['effective_date'])).' of ' .date('m. Y.',strtotime($data->data['effective_date'])).'</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),
     '[contact_info]'=>"<p class='contact_info'><strong>Ember Ágnes</strong>
<br/>Tel/Fax: <strong>+36 (1) 700 1773</strong>
<br/>Mobil: <strong>+36 (20) 959 8997</strong>
<br/>E-mail: <strong>ember.agnes@mads.hu</strong>
</p>",
'[contact_info_company]'=>"<p class='contact_info'><strong>".$data->data['persons'][0]['name']."</strong>
<br/>Tel/Fax: <strong>".optional($data->data['persons'][0])['fax']."</strong>
<br/>Mobil: <strong>".optional($data->data['persons'][0])['phone']."</strong>
<br/>E-mail: <strong>".optional($data->data['persons'][0])['email']."</strong>
</p>"

];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceEn['[industries]'].= optional($p)['industry_en'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
function replaceHu($text,$replace){

     return str_replace(array_keys($replace),array_values($replace),$text);
}
        @endphp
        @foreach($sablon['hu']['sections'] as $section)
            <div style="width:49%;float:left" >
                @php $actSection =$loop->iteration @endphp
                    <p><strong><u>{{$section['name']}}</u></strong></p>
                <ol>
                    @foreach($section['items'] as $item)
                       @php $actItem =$loop->iteration @endphp
                                @if($actSection ==11 && ($actItem ==4 || $actItem ==20))
                                    </ol>
                                    <p><strong>{!!  replaceHu($item['content'],$replaceHu) !!}</strong></p>
                                    <ol>
                                        @elseif($actSection ==6 && $actItem ==8)
                                            @if(optional($data->data)['e_szamla_approve'])
                                                <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                            @endif

                                        @elseif($actSection == 6 && ($actItem ==1))
                                            @if($data->data['is_private'] == 3)
                                                <li>
                                                    A Felek megállapodnak abban, hogy a Kölcsönbeadót az
                                                    általa a Kölcsönvevő részére kölcsönzött munkavállalók
                                                    munkavégzése és rendelkezésre állása alapján –
                                                    kölcsönzött munkavállalóként – a következő kölcsönzési díj
                                                    illeti meg:
                                                    <ul>
                                                        <li> naptári hónaponként a kölcsönzött munkavállaló
                                                            részére megállapított bruttó munkabér havi
                                                            összege x (szorozva) {{$data->data['price']}}
                                                            ({{$data->data['multiplier']}}) + Áfa, valamint</li>
                                                        <li> naptári hónaponként a kölcsönzött munkavállaló
                                                            részére megállapított béren kívüli juttatás bruttó
                                                            bekerülési költsége x (szorozva) {{$data->data['price_other']}}
                                                            ({{$data->data['multiplier']}}) + Áfa.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    Szerződő felek rögzítik, hogy a bruttó munkabér tartalmazza
                                                    a munkavállalót megillető alapbért, a munkavállalót
                                                    esetlegesen megillető bérpótlékok összegét (Mt. 66. fejezet:
                                                    139. § - 145 §.), és a munkavállalót az állásidőre megillető
                                                    munkabér összegét, melyek a Kölcsönvevőt terhelik és
                                                    melyeket a Kölcsönvevő köteles megfizetni a Kölcsönbeadó
                                                    részére.
                                                </li>
                                                <li>
                                                    Szerződő felek rögzítik, hogy a Kölcsönbeadót terhelik
                                                    továbbá az alábbi költségek:
                                                    <p>
                                                        - Szabadságra, fizetett ünnepre és egyéb törvényben
                                                        meghatározott esetekre járó távolléti díj;
                                                        <br/>- Szabadságmegváltás;
                                                        <br/>- Betegszabadságra járó járandóság, valamint a
                                                        munkáltatói táppénz hozzájárulás (táppénz 1/3-a)
                                                        <br/>- Rehabilitációs hozzájárulási adó
                                                        <br/>- Minden egyéb, a munkáltatót terhelő adó és járulék
                                                        (foglalkoztatási költségekre eső 2%-os iparűzési
                                                        adó)</p>
                                                </li>
                                            @else
                                                <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                            @endif
                                        @elseif($actSection == 6 && ($actItem ==2))
                                            @if($data->data['is_private'] == 2)
                                                <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                            @endif
                                        @else
                                    <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                @endif
                    @endforeach

               </ol>
            </div>
            <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
            <p><strong><u>{{$sablon['en']['sections'][$actSection-1]['name']}}</u></strong></p>
                <ol>

                    @foreach($sablon['en']['sections'][$actSection-1]['items'] as $item)
                        @php $actItem =$loop->iteration @endphp
                        @if($actSection ==11 && ($actItem ==4 || $actItem ==20))
                           </ol>
                            <p><strong>{!!   replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</strong></p>
                            <ol>
                                @elseif($actSection ==6 && $actItem ==8)
                                    @if(optional($data->data)['e_szamla_approve'])
                                        <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                                    @endif

                                @elseif($actSection == 6 && ($actItem ==1))
                                    @if($data->data['is_private'] == 3)
                                        <li>
                                            Parties agree that Staffing Agency is entitled to the following
                                            staffing fee based on the work and standby of the temporary
                                            agency workers provided to User Undertaking for each
                                            temporary agency worker:
                                            <ul>
                                                <li>for every calendar month, the sum of the
                                                    monthly gross wage calculated for the
                                                    temporary agency worker x (multiplied by)
                                                    {{$data->data['price']}} ({{$data->data['multiplier']}}) + VAT, and</li>
                                                <li>for every calendar month, the gross cost of the
                                                    fringe benefits calculated for the temporary
                                                    agency worker x (multiplied by) {{$data->data['price_other']}}
                                                    ({{$data->data['multiplier']}}) + VAT.
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            Contracting parties agree that the gross wage includes the
                                            basic wage the employee is entitled to and the sum of any
                                            remuneration supplement the employee may be entitled to
                                            (Sections 139-145 of Chapter 66 of the Labor Code) and the
                                            wage the employee is entitled to for downtime, which shall
                                            be borne by the User Undertaking and which the User
                                            Undertaking shall pay to the Staffing Agency.
                                        </li>
                                        <li>
                                            Contracting parties agree that the following costs shall be
                                            paid by the Staffing Agency:
                                            <p>
                                                - The absence pay due for days off, paid holidays
                                                and other other periods provided by law;
                                                <br/>- Holiday pay;
                                                <br/>- Sick leave allowances and employer’s sick pay
                                                contribution (1/3 of the sick pay)
                                                <br/>- Rehabilitation contribution tax
                                                <br/>-Any other taxes and contributions on the employer
                                                (2% local business tax on employment costs)</p>
                                        </li>
                                    @else
                                        <li>{!!   replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                    @endif
                                @elseif($actSection == 6 && ($actItem ==2))
                                    @if($data->data['is_private'] == 2)
                                        <li>{!!   replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                                    @endif
                                @else
                            <li>{!!   replaceHu($sablon['en']['sections'][$actSection-1]['items'][$actItem-1]['content'],$replaceEn) !!}</li>
                        @endif

                            @endforeach
                        </ol>
                </div>
            <div style="width:100%;position:relative"></div>
                    @endforeach

        </div>


            <div style="width:49%;float:left" >
        Felek megállapodnak, hogy bármilyen jogi, vagy értelmezési kétség, eltérés esetén a magyar nyelvű verziót tekintik irányadónak.
            </div>
            <div style="width: 49%;float:right;font-style: italic !important;" class="italic">
   The Parties agree that in case of any legal or interpretation dispute regarding this document the Hungarian language version shall prevail.
            </div>



    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>MADS WORK Kft.</p>
                <p><strong>Kölcsönbeadó / <i>Staffing Agency</i></strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>{{$data->data['name']}}</p>

                <p><strong>Kölcsönvevő / <i>User Undertaking</i></strong></p>
            </td>

        </tr>
    </table>
</div>
