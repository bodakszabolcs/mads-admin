<style>
    table tr td
    {
        margin: 0px;
        padding: 3px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:11px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    i {
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
</style>
<div class="paper" style="font-size:12px">
    <div style="text-align: right;color:#3c4043">
        <strong>szerződés azonosítója:/ <i>contract No.</i>
            {{$data->data['number']}}
        </strong>
    </div>
    <div style="text-align: right">
        @if($data->data['is_private'] != 1) <strong>4. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 4</i></strong> @else<strong>3. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 3</i></strong> @endif

    </div>
                <p class="head" style="margin-top:25px;"><strong>Felek megállapodása alapján, a közös adatkezelés körében az alábbi személyes adatokat kezelik a felek:</strong><br/>
                    <strong><i>On the basis of an agreement between the Parties, the following personal data will be processed by the Parties in
                            the context of joint data processing:</i></strong>
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td style="width: 30%">
                            <p><strong>&Eacute;rintettek:<br/><i>Data subjects:</i></strong></p>
                        </td>
                        <td>
                            @if($data->data['is_private'] == 1)  <p>- Megbízónál történő munkavégzésre jelentkező pályázók<br/>
                                <i>- Megbízónál történő munkavégzésre jelentkező pályázók</i></p> @else
                            <p>- kölcsönvevőnél, munkaerő-kölcsönzés keretében történő munkavégzésre
                                jelentkező természetes személyek, kölcsönzött munkavállalók<br/>
                                <i>- people applying to work at the User Undertaking, in the framework of
                                    temporary employment, temporary employees</i>
                            </p>
                            <p>- kölcsönvevőnél, munkaerő-kölcsönzés keretében munkát végző kölcsönzött
                                munkavállalók
                                <br/>
                                <i>- temporary employees working for the User Undertaking, within the framework
                                    of temporary employment</i>
                            </p>
                                @endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>K&ouml;z&ouml;s adatkezel&eacute;ssel &eacute;rintett szem&eacute;lyes adatok lehets&eacute;ges k&ouml;re<br/><i>Possible scope of personal data affected by joint data processing:</i></strong></p>
                        </td>
                        <td>
                            @if($data->data['second']['name'])<p><strong>- </strong><strong>n&eacute;v / <i>name</i></strong></p>@endif
                            @if(!$data->data['second']['address'])<p><strong>- </strong><strong>lakc&iacute;m / <i>address</i></strong></p>@endif
                            @if(!$data->data['second']['birthdate'])<p><strong>- </strong><strong>sz&uuml;let&eacute;si hely, idő / <i>place and date of birth</i></strong></p>@endif
                            @if(!$data->data['second']['phone'])<p><strong>- </strong><strong>telefonsz&aacute;m / <i> phone number</i></strong></p>@endif
                            @if(!$data->data['second']['email'])<p><strong>- </strong><strong>e-mail c&iacute;m / <i>e-mail address</i></strong></p>@endif
                            @if(!$data->data['second']['nationality'])<p><strong>- </strong><strong>&aacute;llampolg&aacute;rs&aacute;g / <i>nationality</i></strong></p>@endif
                            @if(!$data->data['second']['gender'])<p><strong>- </strong><strong>nem / <i>sex</i></strong></p>@endif
                            @if(!$data->data['second']['cv'])<p><strong>- </strong><strong>&ouml;n&eacute;letrajz / <i>CV (Curriculum Vitae)</i></strong></p>@endif
                            @if(!$data->data['second']['picture'])<p><strong>- </strong><strong>f&eacute;nyk&eacute;p / <i>photo</i></strong></p>@endif
                            @if(!$data->data['second']['school'])<p><strong>- </strong><strong>jelenlegi, &eacute;s volt oktat&aacute;si int&eacute;zm&eacute;nyekre vonatkoz&oacute; adatok / <i>data regarding current and former educational institutions</i></strong></p>@endif
                            @if(!$data->data['second']['language'])<p><strong>- </strong><strong>nyelvismeret / <i>language skills</i></strong></p>@endif
                            @if(!$data->data['second']['trainingInfo'])<p><strong>- </strong><strong>k&eacute;pz&eacute;si inform&aacute;ci&oacute;k / <i>educational information</i></strong></p>@endif
                            @if(!$data->data['second']['driverLicense'])<p><strong>- </strong><strong>jogos&iacute;tv&aacute;ny t&iacute;pusa / <i>type of eligibility</i></strong></p>@endif
                            @if(!$data->data['second']['reference'])<p><strong>- </strong><strong>referenci&aacute;k / <i>references</i></strong></p>@endif
                                @if(!$data->data['second']['reference'])<p><strong>- </strong><strong>jelenl&eacute;ti &iacute;v / <i>attendance sheet</i></strong></p>@endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                        </td>
                        <td>
                            @if($data->data['is_private'] == 1)  <p>- a GDPR 6. cikk (1) bekezdés a) pontja szerint az érintett
                                megfelelő tájékoztatáson alapuló önkéntes hozzájárulása az
                                adatkezeléshez<br/>
                                <i>- a GDPR 6. cikk (1) bekezdés a) pontja szerint az érintett
                                    megfelelő tájékoztatáson alapuló önkéntes hozzájárulása az
                                    adatkezeléshez</i></p> @else
                            <p>- kölcsönvevőnél, munkaerő-kölcsönzés keretében történő munkavégzésre
                                jelentkező természetes személyek esetén a GDPR 6. cikk (1) bekezdés a)
                                pontja szerint az érintett megfelelő tájékoztatáson alapuló önkéntes
                                hozzájárulása az adatkezeléshez<br/>
                            <i>- people applying to work at the User Undertaking, in the framework of
                                temporary employment, the data subject&#39;s voluntary consent to data
                                processing based on adequate information, according to Article 6 (1) point a)
                                of the GDPR</i>
                            </p>
                            <p>kölcsönvevőnél, munkaerő-kölcsönzés keretében munkát végző kölcsönzött
                                munkavállalók esetében a kölcsönvevő vonatkozásában a GDPR 6. cikk (1)
                                bekezdés f) pontja szerint az adatkezelés az adatkezelő vagy egy harmadik
                                fél jogos érdekeinek érvényesítéséhez szükséges, míg a Kölcsönbeadó
                                vonatkozásában a GDPR 6. cikk (1) bekezdés c) pontja szerint az
                                adatkezelés az adatkezelőre vonatkozó jogi kötelezettség teljesítéséhez
                                szükséges
                            <br/><i>- temporary employees working in the framework of temporary employment, in
                                    relation to the User Undertaking, according to Article 6 (1) point f) of the
                                    GDPR, data processing is necessary to enforce the legitimate interests of the
                                    data controller or a third party, while in relation to the Staffing Agency, Article
                                    6 of the GDPR According to point c) of paragraph (1), data management is
                                    necessary to fulfill the legal obligation of the data controller</i>
                                @endif
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s c&eacute;ljai:<br/><i>Purposes of data processing:</i></strong></p>
                        </td>
                        <td>
                            <p>- foglalkoztatás<br/><i>- employment</i></p>
                            <p>-- foglalkoztatásra irányuló jogviszony létrejöttére vonatkozó ajánlat küldése<br/><i>- sending an offer pertaining to the establishment of an employment
                                    relationship</i></p>
                            <p>- Kölcsönvevő részére megfelelő szakmai tudással rendelkező jelölt ajánlása<br/><i>- recommendation of a candidate with appropriate professional knowledge to
                                    the User Undertaking</i></p>
                            <p>- profilalkot&aacute;s<br/><i>- profiling</i></p>
                            <p>- kapcsolattart&aacute;s<br/><i>- contact maintenance</i></p>
                            <p>- azonos&iacute;t&aacute;s<br/><i>- identification</i></p>
                        </td>
                    </tr>
                    @if($data->data['is_private'] !=1)
                    </tbody>
                </table>
    <pagebreak></pagebreak>
    <div style="text-align: right;color:#3c4043">
        <strong>szerződés azonosítója:/ <i>contract No.</i>
            {{$data->data['number']}}
        </strong>
    </div>
    <br/>
    <br/>

    <br/>
                <table>
                    <tbody>
                    @endif
                    <tr>
                        <td style="width:30%">
                            <p><strong>Adatt&aacute;rol&aacute;s időtartama:<br/><i>Duration of data retention: </i> </strong></p>
                        </td>
                        <td>
                            @if($data->data['is_private'] == 1)
                                <p>- amennyiben a pályázó egy adott, a Megbízó által tett
                                    foglalkoztatásra irányuló jogviszonyra vonatozó ajánlattal
                                    kapcsolatos adatkezeléshez járult hozzá, az adatkezelés
                                    időtartama a pályázó visszautasítását követő 15 napig tart.<br/><i>- amennyiben a pályázó egy adott, a Megbízó által tett
                                        foglalkoztatásra irányuló jogviszonyra vonatozó ajánlattal
                                        kapcsolatos adatkezeléshez járult hozzá, az adatkezelés
                                        időtartama a pályázó visszautasítását követő 15 napig tart.</i>
                                <p>- amennyiben a pályázó a Megbízó, vagy a Megbízott
                                    adatbázisban történő készletező adatkezeléshez is hozzájárult,
                                    az adatkezelés időtartama a hozzájárulás visszavonásáig, de
                                    maximum 5 évig tart.<br/><i>- amennyiben a pályázó a Megbízó, vagy a Megbízott
                                        adatbázisban történő készletező adatkezeléshez is hozzájárult,
                                        az adatkezelés időtartama a hozzájárulás visszavonásáig, de
                                        maximum 5 évig tart.</i></p>
                            @else
                            <p>- sikeresen pályázó kölcsönzött munkavállalók esetében a Kölcsönbeadó a
                                vonatkozó jogszabályokban előírt határidőig tárolja a vele jogviszonyban álló
                                kölcsönzött munkavállalók adatait
                            <br/><i>- in case of temporary workers who apply successfully, the Staffing Agency
                                    stores the data of the temporary workers who have a legal relationship with it
                                    until the deadline prescribed in the relevant legislation</i>
                            </p>
                            <p>- amennyiben pályázó egy adott, a Kölcsönvevő által tett foglalkoztatásra
                                irányuló jogviszonyra vonatkozó ajánlattal kapcsolatos adatkezeléshez járult
                                hozzá, az adatkezelés időtartama a pályázó visszautasítását követő 15 napig
                                tart.
                            <br/><i>-	- if the applicant has contributed to the processing of data relating to a specific
                                    offer aimed at an employment relationship made by the User Undertaking, the
                                    duration of the data processing lasts 15 days after the applicant’s rejection.</i>
                            </p>
                            <p>- amennyiben a pályázó a Kölcsönvevő, vagy a Kölcsönbeadó adatbázisban
                                történő készletező adatkezeléshez is hozzájárult, az adatkezelés időtartama a
                                hozzájárulás visszavonásáig, de maximum 5 évig tart.
                                <br/><i>- if the applicant has also agreed to a reserve data processing in the User
                                    Undertaking’s or Staffing Agency’s database, the duration of the data
                                    processing lasts until the withdrawal of the consent, but not more than up to 5
                                    years.</i>
                            </p>
                                @endif
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    <p></p>
    <p></p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>{{optional($data)->data['name']}}</p>
                @if($data->data['is_private'] != 1) <p><strong>Kölcsönvevő / <i> User Undertaking</i></strong></p> @else
                    <p><strong>Megbízó/ <i> Client</i></strong></p>
                @endif
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>MADS WORK Kft.</p>
                @if($data->data['is_private'] != 1)  <p><strong>Kölcsönbeadó /<i> Staffing Agency</i></strong></p> @else
                    <p><strong>Megbízott /<i> Agent</i></strong></p>
                @endif
            </td>
        </tr>
    </table>


</div>
