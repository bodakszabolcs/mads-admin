<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;

    }
    ol {
       margin-left:12px;
       padding-left:5px;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:14px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px;;
    }
    .contact_info{
        margin:0 !important;;
        padding: 0 !important;
    }
</style>
<div class="paper" style="font-size:12px">

<p class="head"><strong><u>MUNKAERŐ-KÖLCSÖNZÉSI SZERZŐDÉS </u></strong></p>
<p><strong></strong></p>
<p>
   <strong><u>I. BEVEZETŐ RENDELKEZ&Eacute;SEK</u></strong>
</p>
<p>A jelen szerződ&eacute;s az al&aacute;bbi felek k&ouml;z&ouml;tt j&ouml;tt l&eacute;tre:</p>
     <table>
         <tr>
             <td>egyrészről a </td>
             <td>
                 <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                 <strong>székhelye:</strong> {{$data->data['site']}}<br/>
                 <strong>adószáma:</strong> {{$data->data['tax']}}<br/>
                 <strong>képviseli:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position']}} @if(!$loop->last), @endif  @endforeach <br/>
                 mint Megbízó (a továbbiakban: <strong>Kölcsönvevő</strong>),

             </td>
         </tr>
         <tr>
             <td>másrészről a</td>
             <td>
                 <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság </strong><br/>
                     <strong>székhelye:</strong> 1092 Budapest, Erkel Ferenc utca 3. földszint <br/>
                         <strong>cégjegyzékszáma:</strong> Cg.01-09-949538 <br/>
                             <strong>adószáma:</strong> 23026517-2-43. <br/>
                                 <strong>pénzforgalmi jelzőszáma:</strong> 10404089-50526752-84561008
                     Kereskedelmi és Hitelbank Zrt.<br/>
                 <strong>nyilvántartási száma:</strong> BP/0701/16797-1/2016-1641 <br/>
                                     <strong> képviseli:</strong> Ember Ágnes ügyvezető, önállóan,
                     mint Kölcsönbeadó (a továbbiakban:  <strong>Kölcsönbeadó</strong>),

                <br/>

             </td>
         </tr>
     </table>
<p>k&ouml;z&ouml;sen egy&uuml;tt: a <strong>felek</strong>, vagy <strong>szerződő felek</strong>.</p>

@php $sablon = \Modules\Campaig\Entities\ContractSablon::where('id',3)->first();
 $replaceHu=[
    '[industries]'=>'',
    '[e_szamla]'=>'<strong>'.$data->data['e_szamla'].'</strong>',
    '[price]'=>$data->data['price'],
    '[price_name]'=>$data->data['price_name'],
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'<strong>'.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;t&oacute;l, hat&aacute;rozatlan </strong>':'<strong>'.date('Y. m. d.',strtotime($data->data['date'])).' napj&aacute;t&oacute;l '.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;ig, hat&aacute;rozott</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),
     '[contact_info]'=>"<p class='contact_info'><strong>Ember Ágnes</strong>
    <br/>Tel/Fax: <strong>+36 (1) 700 1773</strong>
    <br/>Mobil: <strong>+36 (20) 959 8997</strong>
    <br/>E-mail: <strong>ember.agnes@mads.hu</strong>
    </p>",
    '[contact_info_company]'=>"<p class='contact_info'><strong>".$data->data['persons'][0]['name']."</strong>
<br/>Tel/Fax: <strong>".optional($data->data['persons'][0])['fax']."</strong>
<br/>Mobil: <strong>".optional($data->data['persons'][0])['phone']."</strong>
<br/>E-mail: <strong>".optional($data->data['persons'][0])['email']."</strong>
</p>"

];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceHu['[industries]'].= $p['industry'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
function replaceHu($text,$replace){

     return str_replace(array_keys($replace),array_values($replace),$text);
}
@endphp
@foreach($sablon['hu']['sections'] as $section)
        @php $actSection =$loop->iteration @endphp
        <p><strong><u>{{$section['name']}}</u></strong></p>

        <ol>


        @foreach($section['items'] as $item)
                        @php $actItem =$loop->iteration @endphp


                @if($actSection ==11 && ($actItem ==4 || $actItem ==20))
                        </ol>
        <p><strong>{!!  replaceHu($item['content'],$replaceHu) !!}</strong></p>
                        <ol>
                @elseif($actSection ==6 && $actItem ==8)
                    @if(optional($data->data)['e_szamla_approve'])
                        <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                    @endif

                @elseif($actSection == 6 && ($actItem ==1))
                    @if($data->data['is_private'] == 3)
                    <li>
                        A Felek megállapodnak abban, hogy a Kölcsönbeadót az
                        általa a Kölcsönvevő részére kölcsönzött munkavállalók
                        munkavégzése és rendelkezésre állása alapján –
                        kölcsönzött munkavállalóként – a következő kölcsönzési díj
                        illeti meg:
                        <ul>
                        <li> naptári hónaponként a kölcsönzött munkavállaló
                        részére megállapított bruttó munkabér havi
                        összege x (szorozva) {{$data->data['price']}}
                            ({{$data->data['multiplier']}}) + Áfa, valamint</li>
                        <li> naptári hónaponként a kölcsönzött munkavállaló
                        részére megállapított béren kívüli juttatás bruttó
                        bekerülési költsége x (szorozva) {{$data->data['price_other']}}
                            ({{$data->data['multiplier']}}) + Áfa.
                        </li>
                        </ul>
                    </li>
                    <li>
                        Szerződő felek rögzítik, hogy a bruttó munkabér tartalmazza
                        a munkavállalót megillető alapbért, a munkavállalót
                        esetlegesen megillető bérpótlékok összegét (Mt. 66. fejezet:
                        139. § - 145 §.), és a munkavállalót az állásidőre megillető
                        munkabér összegét, melyek a Kölcsönvevőt terhelik és
                        melyeket a Kölcsönvevő köteles megfizetni a Kölcsönbeadó
                        részére.
                    </li>
                    <li>
                        Szerződő felek rögzítik, hogy a Kölcsönbeadót terhelik
                        továbbá az alábbi költségek:
                        <p>
                        - Szabadságra, fizetett ünnepre és egyéb törvényben
                        meghatározott esetekre járó távolléti díj;
                            <br/>- Szabadságmegváltás;
                            <br/>- Betegszabadságra járó járandóság, valamint a
                        munkáltatói táppénz hozzájárulás (táppénz 1/3-a)
                            <br/>- Rehabilitációs hozzájárulási adó
                            <br/>- Minden egyéb, a munkáltatót terhelő adó és járulék
                        (foglalkoztatási költségekre eső 2%-os iparűzési
                            adó)</p>
                    </li>
                @else
                    <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                   @endif
                @elseif($actSection == 6 && ($actItem ==2))
                    @if($data->data['is_private'] == 2)
                        <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                    @endif
                @else
                            <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
            @endif


        @endforeach
        </ol>
@endforeach
<p>Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>MADS WORK Kft.</p>

                <p><strong>Kölcsönbeadó</strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>{{$data->data['name']}}</p>
                <p><strong>Kölcsönvevő</strong></p>
            </td>

        </tr>
    </table>
</div>
