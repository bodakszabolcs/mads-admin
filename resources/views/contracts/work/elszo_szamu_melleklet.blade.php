<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:14px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
    </style>
<div class="paper" style="font-size:12px">
        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója:<br>
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            @if($data->data['is_private'] != 1) <strong>3. sz&aacute;m&uacute; Mell&eacute;klet</strong> @else <strong>2. sz&aacute;m&uacute; Mell&eacute;klet</strong> @endif
        </div>
        <p class="head" ><strong>Jogi személy szerződő partnerek természetes személy képviselőinek, kapcsolattartóinak elérhetőségi adatai a  @if($data->data['is_private'] == 1) MEGBÍZÓ  @else KÖLCSÖNVEVŐ @endif kapcsolattartója részéről</strong></p>

        @for($i =0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td>

                    <p><strong>{{$data->data['first']['contacts'][$i]['name']}}</strong></p>

                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td>

                    <p><strong>{{$data->data['first']['contacts'][$i+1]['name']}}</strong></p>

                </td>
                @endif
            </tr>
            <tr>
                <td>

                    <p><strong>telefon-, fax sz&aacute;ma:</strong></p>

                </td>
                <td>
                    <p>@if(isset($data->data['first']['contacts'][$i]['fax']) && !empty($data->data['first']['contacts'][$i]['fax']))<strong>{{$data->data['first']['contacts'][$i]['fax']}}</strong>@endif</p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>

                    <p><strong>telefon-, fax sz&aacute;ma:</strong></p>

                </td>
                <td>
                    <p><strong>@if(isset($data->data['first']['contacts'][$i+1]['fax']) && !empty($data->data['first']['contacts'][$i+1]['fax'])){{$data->data['first']['contacts'][$i+1]['fax']}}@endif</strong></p>
                </td>
                @endif
            </tr>

            <tr>
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma: </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['phone']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma: </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['phone']}}</strong></p>
                </td>
                @endif
            </tr>
            <tr>
                <td>

                    <p><strong>e-mail c&iacute;me:</strong></p>

                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['email']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>

                    <p><strong>e-mail c&iacute;me:</strong></p>

                </td>

                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['email']}}</strong></p>
                </td>
                @endif
            </tr>
            </tbody>
        </table>
        @endfor
        <p class="head"><strong>Inform&aacute;ci&oacute;k:</strong></p>

        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>adatkezelő:</strong></p>
                </td>
                <td >
                    <p>MADS - WORK Szolgáltató Korlátolt Felelősségű Társaság</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezelő honlapja:</strong></p>
                </td>
                <td >
                    <p>www.madswork.hu</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:</strong></p>
                </td>
                <td >
                    <p>szerződés teljesítése, üzleti kapcsolattartás.</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s jogalapja:</strong></p>
                </td>
                <td >
                    <p>adatkezelő jogos érdeke</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:</strong></p>
                </td>
                <td >
                    <p>a  @if($data->data['is_private'] == 1) Megbízó @else Kölcsönbeadó @endif ügyfélkiszolgálással kapcsolatos
                        feladatokat ellátó munkavállalói; a  @if($data->data['is_private'] == 1) Megbízónál @else Kölcsönvevőnél @endif
                        foglalkoztatott kölcsönzött munkavállalók</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:</strong></p>
                </td>
                <td >
                    <p>Az üzleti kapcsolat, illetve az érintett képviselői
                        minőségének fennállását követő 5 évig.</p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l:</strong></p>
    @if($data->data['is_private'] == 1)
        <p style="margin-bottom: 0px;padding-bottom:0px">&Ouml;nnek joga van k&eacute;relmezni az adatkezelőtől az &Ouml;nre vonatkoz&oacute; szem&eacute;lyes adatokhoz val&oacute; hozz&aacute;f&eacute;r&eacute;st, azok helyesb&iacute;t&eacute;s&eacute;t, t&ouml;rl&eacute;s&eacute;t vagy kezel&eacute;s&eacute;nek korl&aacute;toz&aacute;s&aacute;t, &eacute;s tiltakozhat az ilyen szem&eacute;lyes adatok kezel&eacute;se ellen, valamint a joga van az adathordozhat&oacute;s&aacute;ghoz.
            <br/>Joga van panasza eset&eacute;n:</p>
        <ul style="margin-top: 0px;padding-top: 0px;margin-bottom: 0px;padding-bottom: 0px">
            <li>a Megb&iacute;z&oacute;hoz fordulni,</li>
            <li>b&iacute;r&oacute;s&aacute;ghoz is fordulhat (A pert - v&aacute;laszt&aacute;sa szerint - a lak&oacute;helye vagy tart&oacute;zkod&aacute;si helye szerint illet&eacute;kes t&ouml;rv&eacute;nysz&eacute;k előtt is megind&iacute;thatja.)</li>
            <li>a fel&uuml;gyeleti hat&oacute;s&aacute;ghoz (Nemzeti Adatv&eacute;delmi &eacute;s Inform&aacute;ci&oacute;szabads&aacute;g Hat&oacute;s&aacute;g [c&iacute;m: 1055 Budapest, Falk Miksa utca 9-11. levelez&eacute;si c&iacute;m: 1363 Budapest, Pf.: 9.; tel.: +36 (1) 391-1400; honlap: http://www.naih.hu; e-mail: ugyfelszolgalat@naih.hu]) panaszt beny&uacute;jtani</li>
        </ul>
        <p style="margin-top: 0px;padding-top:0px">&nbsp;Tov&aacute;bbi inform&aacute;ci&oacute;k az adatkezelő (Megb&iacute;z&oacute;) honlapj&aacute;n/sz&eacute;khely&eacute;n el&eacute;rhető Adatkezel&eacute;si t&aacute;j&eacute;koztat&oacute;ban olvashat&oacute;k.</p>
    @else
        <p style="margin-bottom: 0px;padding-bottom:0px">Önnek joga van kérelmezni az adatkezelőtől az Önre
            vonatkozó személyes adatokhoz való hozzáférést, azok
            helyesbítését, törlését vagy kezelésének korlátozását, és
            tiltakozhat az ilyen személyes adatok kezelése ellen,
            valamint a joga van az adathordozhatósághoz.
        <br/>Joga van a Kölcsönbeadóhoz, a bírósághoz fordulni vagy
            a felügyeleti hatósághoz (Nemzeti Adatvédelmi és
            Információszabadság Hatóság) panaszt benyújtani.
        <br/>
        </p>

        <p style="margin-top: 0px;padding-top:0px">&nbsp;További információk az adatkezelő honlapján elérhető Adatkezelési tájékoztatóban olvashatók.&nbsp;</p>
    @endif
        <p class="head">****</p>
        <p><strong>A fenti információkat és adatvédelmi tájékoztatást,
                valamint a személyes adataim fenti célú kezelését
                tudomásul vettem. </strong></p>
        <p>Kelt, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
    @for($i=0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                <p>____________________________________________________</p>
                <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i+1]['name']}}</p>
                @else
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
        </tr>
    </table>
    @endfor
        <pagebreak></pagebreak>




        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója:<br>
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            @if($data->data['is_private'] != 1) <strong>3. sz&aacute;m&uacute; Mell&eacute;klet</strong> @else <strong>2. sz&aacute;m&uacute; Mell&eacute;klet</strong> @endif
        </div>

        <p class="head"><strong>Jogi személy szerződő partnerek természetes személy képviselőinek, kapcsolattartóinak elérhetőségi adatai @if($data->data['is_private']==1) MEGBÍZOTT @else KÖLCSÖNBEADÓ @endif kapcsolattartója részéről</strong></p>
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td >
                    <p>Ember Ágnes</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                <td>
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:</strong></p>
                </td>
                <td >
                    <p>+36 (1) 700 1773</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>telefon-, fax sz&aacute;ma:</strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->fax}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:</strong></p>
                </td>
                <td >
                    <p>+36 (20) 959-8997</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:</strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:</strong></p>
                </td>
                <td >
                    <p>ember.agnes@mads.hu</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:</strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2; $i++)
        @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                <td >
                    <p><strong>kapcsolattart&oacute; neve:</strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                </td>
                @endif
            </tr>
            @if(isset($data->data['first']['madsContact'][$i+1]))
                @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp

                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:</strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->fax}}</p>
                </td>
            @endif
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:</strong></p>
                </td>
                <td >
                    <p>{{optional($user)->phone}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:</strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:</strong></p>
                </td>
                <td >
                    <p>{{optional($user)->email}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:</strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
      @endfor

        <p class="head"><strong>Inform&aacute;ci&oacute;k:</strong></p>

        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>adatkezelő:</strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['controller']}}</strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezelő honlapja:</strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['website']}}</strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:</strong></p>
                </td>
                <td >
                    <p>szerződés teljesítése, üzleti kapcsolattartás.</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s jogalapja:</strong></p>
                </td>
                <td >
                    <p>törvényes képviselő tekintetében szerződés teljesítése, míg
                        kijelölt kapcsolattartó tekintetében az adatkezelő jogos
                        érdeke</p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:</strong></p>
                </td>
                <td >@if($data->data['is_private']==1)
                        <p>    a Megbízó ügyfélkiszolgálással kapcsolatos feladatokat
                        ellátó munkavállalói; Megbízónál történő munkavégzésre
                            jelentkező pályázók</p>
                         @else
                    <p>a Társaság ügyfélkiszolgálással kapcsolatos feladatokat
                        ellátó munkavállalói;</p>
                         @endif
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:</strong></p>
                </td>
                <td >
                    <p>Az üzleti kapcsolat, illetve az érintett képviselői
                        minőségének fennállását követő 5 évig.</p>
                </td>
            </tr>
            </tbody>
        </table>

        <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l:</strong></p>
    @if($data->data['is_private'] == 1)
        <p style="margin-bottom: 0px;padding-bottom:0px">&Ouml;nnek joga van k&eacute;relmezni az adatkezelőtől az &Ouml;nre vonatkoz&oacute; szem&eacute;lyes adatokhoz val&oacute; hozz&aacute;f&eacute;r&eacute;st, azok helyesb&iacute;t&eacute;s&eacute;t, t&ouml;rl&eacute;s&eacute;t vagy kezel&eacute;s&eacute;nek korl&aacute;toz&aacute;s&aacute;t, &eacute;s tiltakozhat az ilyen szem&eacute;lyes adatok kezel&eacute;se ellen, valamint a joga van az adathordozhat&oacute;s&aacute;ghoz.
            <br/>Joga van panasza eset&eacute;n:</p>
        <ul style="margin-top: 0px;padding-top: 0px;margin-bottom: 0px;padding-bottom: 0px">
            <li>a Megb&iacute;zotthoz fordulni,</li>
            <li>b&iacute;r&oacute;s&aacute;ghoz is fordulhat (A pert - v&aacute;laszt&aacute;sa szerint - a lak&oacute;helye vagy tart&oacute;zkod&aacute;si helye szerint illet&eacute;kes t&ouml;rv&eacute;nysz&eacute;k előtt is megind&iacute;thatja.)</li>
            <li>a fel&uuml;gyeleti hat&oacute;s&aacute;ghoz (Nemzeti Adatv&eacute;delmi &eacute;s Inform&aacute;ci&oacute;szabads&aacute;g Hat&oacute;s&aacute;g [c&iacute;m: 1055 Budapest, Falk Miksa utca 9-11. levelez&eacute;si c&iacute;m: 1363 Budapest, Pf.: 9.; tel.: +36 (1) 391-1400; honlap: http://www.naih.hu; e-mail: ugyfelszolgalat@naih.hu]) panaszt beny&uacute;jtani</li>
        </ul>
        <p style="margin-top: 0px;padding-top:0px">&nbsp;Tov&aacute;bbi inform&aacute;ci&oacute;k az adatkezelő (Megb&iacute;z&oacute;) honlapj&aacute;n/sz&eacute;khely&eacute;n el&eacute;rhető Adatkezel&eacute;si t&aacute;j&eacute;koztat&oacute;ban olvashat&oacute;k.</p>
    @else
        <p style="margin-bottom: 0px;padding-bottom:0px">Önnek joga van kérelmezni az adatkezelőtől az Önre
            vonatkozó személyes adatokhoz való hozzáférést, azok
            helyesbítését, törlését vagy kezelésének korlátozását, és
            tiltakozhat az ilyen személyes adatok kezelése ellen,
            valamint a joga van az adathordozhatósághoz.
            <br/>Joga van Kölcsönvevőhöz vagy bírósághoz fordulna, a
            felügyeleti hatósághoz (Nemzeti Adatvédelmi és
            Információszabadság Hatóság) panaszt benyújtani.
      </p>  <p style="margin-top: 0px;padding-top:0px">Tov&aacute;bbi inform&aacute;ci&oacute;k az adatkezelő (Megb&iacute;z&oacute;) honlapj&aacute;n/sz&eacute;khely&eacute;n el&eacute;rhető Adatkezel&eacute;si t&aacute;j&eacute;koztat&oacute;ban olvashat&oacute;k.</p>
    @endif


        <p class="head">****</p>
        <p><strong>A fenti információkat és adatvédelmi tájékoztatást,
                valamint a személyes adataim fenti célú kezelését
                tudomásul vettem.</strong></p>
        <p>Kelt, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
        <table class="signature" style="border:none">
            <tr>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        <p>____________________________________________________</p>
                        <p>Ember Ágnes</p>
                    @endif
                </td>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                        <p>____________________________________________________</p>
                        <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                    @else
                        <p>____________________________________________________</p>
                        <p>Ember Ágnes</p>
                    @endif
                </td>
            </tr>
            @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2;$i++)

                    <tr>
                        <td style="text-align: center;border:none;width:50%">
                            @if(isset($data->data['first']['madsContact'][$i]))
                                @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
                                <p>____________________________________________________</p>

                                <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                            @endif
                        </td>
                        <td style="text-align: center;border:none;width:50%">
                            @if(isset($data->data['first']['madsContact'][$i+1]))
                                @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                                <p>____________________________________________________</p>
                                <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                            @endif
                        </td>
                    </tr>
            @endfor
        </table>


</div>
