<style>
    table tr td
    {
        margin: 0px;
        padding: 2px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;

    }
    ol {
        margin-left:12px;
        padding-left:5px;
    }
    .paper{

        font-size: 11px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:12px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    .italic ol li,.italic ul li,.italic  td, .italic p{
        font-style: italic; !important;
        color: #575555;
    }
    i{
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:12px;;
    }
    </style>
<div class="paper" style="font-size:12px">
        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója / <i>contract No.</i>:
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            <strong>1. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 1</i></strong>
        </div>
    <p class="head" style="margin-top:25px;"><strong>ESETI MEGBÍZÁS /<i>INDIVIDUAL AGREEMENT</i></strong></p>
    <div style="position: relative">
        <div style="width:49%;float:left" >
            <div class="">
                <strong>Sorszám:  {{$data->data['number']}}</strong>
            </div>
            <p>
                Amely a <strong>{{$data->data['name']}}</strong> (képviseli: {{$data->data['persons'][0]['name']}}) mint  @if($data->data['is_private'] == 1) <strong>Megbízó</strong> @else <strong>Kölcsönvevő</strong>@endif,
                és a <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság</strong> (képviseli: Ember Ágnes ügyvezető, önállóan) mint
                @if($data->data['is_private'] == 1)<strong>Megbízott</strong>@else<strong>Kölcsönbeadó</strong>@endif között a <strong>{{explode('-',$data->data['date'])[0]}}. év {{explode('-',$data->data['date'])[1]}} hónap {{explode('-',$data->data['date'])[2]}}.</strong> napján létrejött munkaerő-kölcsönzési Szerződés
                elválaszthatatlan része, és kizárólag azzal együtt hatályos.
                <br/>
            </p>
            <p></p>

            <table>
                <tbody>
                <tr>
                    <td>
                        <p><strong>1. A Kölcsönvevő által foglalkoztatni kívánt munkavállalók létszáma:</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['person_count']}} fő</td></tr>
                <tr>
                    <td>
                        <p><strong>2. A munkavégzés helye:</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['location']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>3. Az elvégzendő munka leírása:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['description']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>4. Az elvégzendő munkára vonatkozó alkalmassági feltételek:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['preference']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>5. A munkaerő-kölcsönzés időtartama:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['time']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>6. A munkavégzésnél irányadó munkarend és a munkaidő-beosztás jellege:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['work_time']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>7.A munkáltatói jogkör gyakorlójának megnevezése:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['company']}}</td></tr>
                <tr>
                    <td>
                        <p><strong>8. A kölcsönzött munkavállalók munkába állásának előfeltételeként meghatározott további elvárások (pl.
                                erkölcsi bizonyítvány bemutatása):</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['conditions']}}</td></tr>
                </tbody>
            </table></div>
        <div style="width: 49%;float:right;font-style: italic !important;" class="italic"> <div class="">
                <strong>Number:  {{$data->data['number']}}</strong>
            </div>
            <p>
                Which is an integral part of the temporary employee staffing
                agreement executed between
                <strong>{{$data->data['name']}}</strong> (represented by:
                {{$data->data['persons'][0]['name']}})  @if($data->data['is_private'] == 1)<strong>as Client</strong> @else<strong>as User Undertaking</strong>@endif,
                and <strong>MADS WORK Szolgáltató Korlátolt Felelősségű Társaság</strong>
                (represented by: Ágnes Ember, managing director, independently)
                @if($data->data['is_private'] == 1)<strong>as Agent</strong> @else <strong>as Staffing Agency</strong> @endif on {{explode('-',$data->data['date'])[2]}} day {{explode('-',$data->data['date'])[1]}} month {{explode('-',$data->data['date'])[0]}}.
                year, and it will have legal force only therewith.

            </p>

            <table>
                <tbody>
                <tr>
                    <td>
                        <p><strong>1. The number of employees intended to engage by
                                the User Undertaking:</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['person_count']}} person/people</td></tr>
                <tr>
                    <td>
                        <p><strong>2. Place of the work:</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['location']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>3. Description of the work to be carried out:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['description_en']}} </td></tr>
                <tr>
                    <td >
                        <p><strong>4. Suitability criteria relating to the work to be carried
                                out:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['preference_en']}} fő</td></tr>
                <tr>
                    <td >
                        <p><strong>5. The duration of temporary staffing :</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['time_en']}} </td></tr>
                <tr>
                    <td >
                        <p><strong>6. Work schedule and work arrangements applicable
                                to the work:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['work_time_en']}}</td></tr>
                <tr>
                    <td >
                        <p><strong>7. Name of the person exercising the employer’s
                                rights:</strong></p>
                    </td>

                </tr>
                <tr><td>{{$data->data['zeroth']['company_en']}}</td></tr>
                <tr>
                    <td>
                        <p><strong>8. Additional requirements specified as necessary for
                                temporary agency workers to start work (e.g.
                                presentation of a certificate of good conduct):</strong></p>
                    </td>
                </tr>
                <tr><td>{{$data->data['zeroth']['conditions_en']}} </td></tr>
                </tbody>
            </table></div>
    </div>


    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    <br/>
    <br/>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">

                <p>____________________________________________________</p>
                <p><strong>MADS WORK Kft.</strong></p>
                @if($data->data['is_private'] == 1)
                    <p><strong>Megbízott / <i> Agent</i></strong></p>
                @else
                    <p><strong>Kölcsönbeadó / <i> Staffing Agency</i></strong></p>
                @endif

            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p><strong>{{$data->data['name']}}</strong></p>
                @if($data->data['is_private'] == 1)
                    <p><strong>Megbízó / <i>Client</i> </strong></p>
                @else
                    <p><strong>Kölcsönvevő / <i>User Undertaking</i> </strong></p>
                @endif
            </td>
        </tr>
    </table>
    @if($data->data['is_private'] != 1)
    <br/>
    <hr/>
    <br/>
    <br/>
    <div style="text-align: right">
        <strong>2. sz&aacute;m&uacute; Mell&eacute;klet/ <i>Annex No. 2</i></strong>
    </div>
    <p>A 118/2001. (VI. 30.) Korm. rendelet szerint a munkaerő-kölcsönzési tevékenység ellátását a nyilvántartásba vételével a
        Kölcsönbeadó részére engedélyező illetékes kormányhivatali határozat másolata. / <i>A copy of the government office decision
            permitting the Staffing Agency to provide temporary employee staffing activities specified in Government Decision No.
            118/2001. (VI. 30.) by registering the Staffing Agency.</i></p>
    @endif
</div>
