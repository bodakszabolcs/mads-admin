<style>
    table tr td
    {
        margin: 0px;
        padding: 5px;
        vertical-align: top;

        font-size: 12px;
        line-height: normal;

    }
    ol {
       margin-left:12px;
       padding-left:5px;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:12px;
    }
    .head{
        font-size:14px;
        text-align: center
    }
    .paper table.data td{

        white-space: nowrap;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px;;
    }
</style>
<div class="paper" style="font-size:12px">

<p class="head"><strong><u>MEGB&Iacute;Z&Aacute;SI SZERZŐD&Eacute;S</u></strong></p>
<p><strong></strong></p>
<p>
   <strong><u>I. BEVEZETŐ RENDELKEZ&Eacute;SEK</u></strong>
</p>
<p>A jelen szerződ&eacute;s az al&aacute;bbi felek k&ouml;z&ouml;tt j&ouml;tt l&eacute;tre:</p>
     <table>
         <tr>
             <td>egyrészről a </td>
             <td>
                 <strong>{{$data->data['name']}}</strong>&nbsp; <br/>
                 <strong>székhelye:</strong> {{$data->data['site']}}<br/>
                 <strong>adószáma:</strong> {{$data->data['tax']}}<br/>
                 <strong>képviseli:</strong> @foreach($data->data['persons'] as $p) {{$p['name']}}, mint {{$p['position']}} @if(!$loop->last), @endif  @endforeach <br/>
                 mint Megbízó (a továbbiakban: <strong>megbízó</strong> vagy <strong>szolgáltatás fogadója</strong>),

             </td>
         </tr>
         <tr>
             <td>másrészről a</td>
             <td><strong>MADS - Márton Áron Szolgáltató Iskolaszövetkezet</strong><br/>
                 <strong>székhelye:</strong> 1092 Budapest, Erkel utca 3. földszint<br/>
                 <strong>adószáma:</strong> 11931335-2-43.<br/>
                     <strong>képviseli:</strong> Lator Sándor az Igazgatóság tagja, önállóan<br/>
                 és Virág Viktor az Igazgatóság tagja, önállóan,<br/>
                 valamint Szántó Zsolt Sándor az Igazgatóság tagja, önállóan,<br/>
                 mint Megbízott (a továbbiakban: <strong>megbízott</strong> vagy <strong>iskolaszövetkezet</strong>),<br/>

             </td>
         </tr>
     </table>
<p>k&ouml;z&ouml;sen egy&uuml;tt: a <strong>felek</strong>, vagy <strong>szerződő felek</strong>.</p>

@php $sablon = \Modules\Campaig\Entities\ContractSablon::where('id',1)->first();
 $replaceHu=[
    '[industries]'=>'',
    '[e_szamla]'=>'<strong>'.$data->data['e_szamla'].'</strong>',
    '[payment_deadline]'=>$data->data['payment_deadline'],
    '[contract_type]'=>($data->data['contract_type']==2)?'<strong>'.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;t&oacute;l, hat&aacute;rozatlan </strong>':'<strong>'.date('Y. m. d.',strtotime($data->data['date'])).' napj&aacute;t&oacute;l '.date('Y. m. d.',strtotime($data->data['effective_date'])).' napj&aacute;ig, hat&aacute;rozott</strong>',
    '[previous_contract_date]'=>($data->data['previous_contract']==1?$data->data['previous_contract_date']:''),
    '[previous_contract_number]'=>($data->data['previous_contract']==1?$data->data['previous_contract_number']:''),

];
 foreach($data->data['industries'] as$k=> $p) {
     $replaceHu['[industries]'].= $p['industry'] .((!$k+1 !=sizeof($data->data['industries'])?', ':''));
     }
function replaceHu($text,$replace){

     return str_replace(array_keys($replace),array_values($replace),$text);
}
@endphp
@foreach($sablon['hu']['sections'] as $section)
        @php $actSection =$loop->iteration @endphp
        <p><strong><u>{{$section['name']}}</u></strong></p>
        @if($actSection ==4)
        <ol start="2">
        @else
        <ol>
        @endif
        @foreach($section['items'] as $item)
                        @php $actItem =$loop->iteration @endphp
                @if(($actSection ==2 && ($actItem ==1 || $actItem==12)) || ($actSection ==10 && $actItem==4) || ($actSection ==10 && $actItem==19) )
                </ol>
                     <p><strong>{!! $item['content'] !!}</strong></p>
                <ol>
                @else
                    @if($actSection ==4 && $actItem ==1 )
                        <ol>
                            <li>A II. pontban le&iacute;rt &eacute;s teljes&iacute;tett feladatok&eacute;rt a megb&iacute;zottat:</li>
                        </ol>
                        <ul>
                            @foreach($data->data['industries'] as $ind)
                                @if($ind['type'] == 1)<li><strong>{{$ind['industry']}} - {{$ind['price']}}</strong><strong>, - Ft / óra / fő + Áfa, azaz óránként, valamint fejenként </strong>{{$ind['price_text']}}<strong> magyar forint + Áfa megbízási díj illeti meg;</strong></li>@endif
                                @if($ind['type'] == 2)<li><strong>{{$ind['industry']}} </strong><strong> -a feladatteljesítésre kijelölt iskolaszövetkezeti tagokat megillető <u>bruttó díj x (szorozva)</u>  {{$ind['price']}} ({{$ind['multiplier']}})Áfa összegű megbízási díj illeti meg;</strong></li>@endif
                                @if($ind['type'] == 3)<li><strong>{{$ind['industry']}} </strong><strong> -a feladatteljesítésre kijelölt iskolaszövetkezeti tagokat megillető <u>bruttó díj / (osztva)</u>  {{$ind['price']}} ({{$ind['multiplier']}})Áfa összegű megbízási díj illeti meg;</strong></li>@endif
                            @endforeach

                        </ul>
                        <p><u>Szerződő felek rögzítik, hogy a külső szolgáltatás teljesítésében részt vevő iskolaszövetkezeti tagokat megillető megbízási díj mindenkori összegéről megbízó legkésőbb a tárgyhó első napjáig írásban értesíti a megbízottat.</u>.</p>
                    @endif
                    @if($actSection ==5 && $actItem ==6)
                            <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                    @elseif($actSection ==4 && $actItem ==6)
                        @if(optional($data->data)['e_szamla_approve'])
                            <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                        @endif
                    @elseif($actSection ==13 && $actItem ==4)
                            @if(optional($data->data)['previous_contract'] ==1)
                                <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                            @endif
                    @elseif(($actSection ==13 && $actItem == sizeof($section['items'])) )
                        </ol>
                <p>{!!  replaceHu($item['content'],$replaceHu) !!}</p>
                        <ol>

                    @else
                            <li>{!!  replaceHu($item['content'],$replaceHu) !!}</li>
                    @endif
                @endif
        @endforeach
        </ol>
@endforeach
<p>Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n</p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>&nbsp;{{$data->data['name']}}</p>
                <p><strong>Megb&iacute;z&oacute;</strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>__________________________</p>
                <p>MADS - M&aacute;rton &Aacute;ron Szolg&aacute;ltat&oacute; Iskolasz&ouml;vetkezet</p>
                <p><strong>Megb&iacute;zott</strong></p>
            </td>
        </tr>
    </table>
</div>
