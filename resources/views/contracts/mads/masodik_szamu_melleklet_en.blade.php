<style>
    table tr td
    {
        margin: 0px;
        padding: 3px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:11px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    i {
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
</style>
<div class="paper" style="font-size:12px">
    <div style="text-align: right;color:#3c4043">
        <strong>szerződés azonosítója:/ <i>contract No.</i>
            {{$data->data['number']}}
        </strong>
    </div>
    <div style="text-align: right">
        <strong>2. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 2</i></strong>
    </div>
                <p class="head"><strong>Felek meg&aacute;llapod&aacute;sa alapj&aacute;n, a k&ouml;z&ouml;s adatkezel&eacute;s k&ouml;r&eacute;ben az al&aacute;bbi szem&eacute;lyes adatokat kezelik a felek:</strong><br/>
                    <strong><i>On the basis of an agreement between the Parties, the following personal data will be processed by the Parties in the context of joint data processing</i></strong>
                </p>
                <table>
                    <tbody>
                    <tr>
                        <td style="width: 30%">
                            <p><strong>&Eacute;rintettek:<br/><i>Data subjects:</i></strong></p>
                        </td>
                        <td>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sre jelentkező term&eacute;szetes szem&eacute;lyek, iskolasz&ouml;vetkezeti tagok<br/>
                                <i>-	natural persons, members of the Students’ Cooperation applying for the performance of tasks within the scope of external services, at the Client as a recipient of external services</i>
                            </p>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sben r&eacute;sztvevő iskolasz&ouml;vetkezeti tagok
                                <br/>
                                <i>-	members of the Students’ Cooperation participating in the performance of tasks within the scope of external services, at the Client as a recipient of external services</i>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>K&ouml;z&ouml;s adatkezel&eacute;ssel &eacute;rintett szem&eacute;lyes adatok lehets&eacute;ges k&ouml;re<br/><i>Possible scope of personal data affected by joint data processing:</i></strong></p>
                        </td>
                        <td>
                            @if($data->data['second']['name'])<p><strong>- </strong><strong>n&eacute;v / <i>name</i></strong></p>@endif
                            @if($data->data['second']['address'])<p><strong>- </strong><strong>lakc&iacute;m / <i>address</i></strong></p>@endif
                            @if($data->data['second']['birthdate'])<p><strong>- </strong><strong>sz&uuml;let&eacute;si hely, idő / <i>place and date of birth</i></strong></p>@endif
                            @if($data->data['second']['phone'])<p><strong>- </strong><strong>telefonsz&aacute;m / <i> phone number</i></strong></p>@endif
                            @if($data->data['second']['email'])<p><strong>- </strong><strong>e-mail c&iacute;m / <i>e-mail address</i></strong></p>@endif
                            @if($data->data['second']['nationality'])<p><strong>- </strong><strong>&aacute;llampolg&aacute;rs&aacute;g / <i>nationality</i></strong></p>@endif
                            @if($data->data['second']['gender'])<p><strong>- </strong><strong>nem / <i>sex</i></strong></p>@endif
                            @if($data->data['second']['cv'])<p><strong>- </strong><strong>&ouml;n&eacute;letrajz / <i>CV (Curriculum Vitae)</i></strong></p>@endif
                            @if($data->data['second']['picture'])<p><strong>- </strong><strong>f&eacute;nyk&eacute;p / <i>photo</i></strong></p>@endif
                            @if($data->data['second']['school'])<p><strong>- </strong><strong>jelenlegi, &eacute;s volt oktat&aacute;si int&eacute;zm&eacute;nyekre vonatkoz&oacute; adatok / <i>data regarding current and former educational institutions</i></strong></p>@endif
                            @if($data->data['second']['language'])<p><strong>- </strong><strong>nyelvismeret / <i>language skills</i></strong></p>@endif
                            @if($data->data['second']['trainingInfo'])<p><strong>- </strong><strong>k&eacute;pz&eacute;si inform&aacute;ci&oacute;k / <i>educational information</i></strong></p>@endif
                            @if($data->data['second']['driverLicense'])<p><strong>- </strong><strong>jogos&iacute;tv&aacute;ny t&iacute;pusa / <i>type of eligibility</i></strong></p>@endif
                            @if($data->data['second']['reference'])<p><strong>- </strong><strong>referenci&aacute;k / <i>references</i></strong></p>@endif
                            @if($data->data['second']['reference'])<p><strong>- </strong><strong>jelenl&eacute;ti &iacute;v / <i>attendance sheet</i></strong></p>@endif
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                        </td>
                        <td>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sre jelentkező iskolasz&ouml;vetkezeti tagok eset&eacute;n a GDPR 6. cikk (1) bekezd&eacute;s a) pontja szerint az &eacute;rintett megfelelő t&aacute;j&eacute;koztat&aacute;son alapul&oacute; &ouml;nk&eacute;ntes hozz&aacute;j&aacute;rul&aacute;sa az adatkezel&eacute;shez<br/>
                            <i>-	in the case of members of the Students’ Cooperation applying for the performance of tasks within the scope of external services, at the Client as a recipient of external services, according to Article 6 (1) a) of the GDPR, the data subject’s informed consent to the processing of his or her personal data</i>
                            </p>
                            <p>- megb&iacute;z&oacute;n&aacute;l, mint k&uuml;lső szolg&aacute;ltat&aacute;s fogad&oacute;j&aacute;n&aacute;l, k&uuml;lső szolg&aacute;ltat&aacute;s keret&eacute;ben t&ouml;rt&eacute;nő feladatteljes&iacute;t&eacute;sben r&eacute;sztvevő iskolasz&ouml;vetkezeti tagok eset&eacute;ben a megb&iacute;z&oacute; vonatkoz&aacute;s&aacute;ban a GDPR 6. cikk (1) bekezd&eacute;s f) pontja szerint az adatkezel&eacute;s az adatkezelő vagy egy harmadik f&eacute;l jogos &eacute;rdekeinek &eacute;rv&eacute;nyes&iacute;t&eacute;s&eacute;hez sz&uuml;ks&eacute;ges, m&iacute;g a megb&iacute;zott vonatkoz&aacute;s&aacute;ban a GDPR 6. cikk (1) bekezd&eacute;s c) pontja szerint az adatkezel&eacute;s az adatkezelőre vonatkoz&oacute; jogi k&ouml;telezetts&eacute;g teljes&iacute;t&eacute;s&eacute;hez sz&uuml;ks&eacute;ges
                            <br/><i>-	in the case of members of the Students’ Cooperation participating in the performance of tasks within the scope of external services, at the Client as a recipient of external services, for the Client, according to Article 6 (1) f) of the GDPR, processing is necessary for the purposes of the legitimate interests pursued by the controller or by a third party, and for the Agent, according to Article 6 (1) c) of the GDPR, processing is necessary for compliance with a legal obligation to which the controller is subject </i>
                            </p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><strong>Adatkezel&eacute;s c&eacute;ljai:<br/><i>Purposes of data processing:</i></strong></p>
                        </td>
                        <td>
                            <p>- k&uuml;lső szolg&aacute;ltat&aacute;s teljes&iacute;t&eacute;se<br/><i>- performance of external service</i></p>
                            <p>- foglalkoztat&aacute;sra ir&aacute;nyul&oacute; jogviszony l&eacute;trej&ouml;tt&eacute;re vonatkoz&oacute; aj&aacute;nlat k&uuml;ld&eacute;se<br/><i>- sending an offer pertaining to the establishment of an employment relationship</i></p>
                            <p>- megb&iacute;z&oacute; r&eacute;sz&eacute;re megfelelő szakmai tud&aacute;ssal rendelkező jel&ouml;lt aj&aacute;nl&aacute;sa<br/><i>- recommendation of a candidate with appropriate professional knowledge to the Client</i></p>
                            <p>- profilalkot&aacute;s<br/><i>- profiling</i></p>
                            <p>- kapcsolattart&aacute;s<br/><i>- contact maintenance</i></p>
                            <p>- azonos&iacute;t&aacute;s<br/><i>- identification</i></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
    <pagebreak></pagebreak>
    <div style="text-align: right;color:#3c4043">
        <strong>szerződés azonosítója:/ <i>contract No.</i>
            {{$data->data['number']}}
        </strong>
    </div>
    <br/>
    <br/>

    <br/>
                <table>
                    <tbody>
                    <tr>
                        <td style="width:30%">
                            <p><strong>Adatt&aacute;rol&aacute;s időtartama:<br/><i>Duration of data retention: </i> </strong></p>
                        </td>
                        <td>
                            <p>- sikeresen p&aacute;ly&aacute;z&oacute; iskolasz&ouml;vetkezeti tagok eset&eacute;ben a megb&iacute;zott a vonatkoz&oacute; jogszab&aacute;lyokban elő&iacute;rt hat&aacute;ridőig t&aacute;rolja a vele tags&aacute;gi viszonyban &aacute;ll&oacute; iskolasz&ouml;vetkezeti tagok adatait
                            <br/><i>-	in the case of successfully applying Students’ Cooperation members, the Agent shall store the data of the Students’ Cooperation members in a membership relation with the Agent for the term prescribed by the relevant legislation</i>
                            </p>
                            <p>- amennyiben az iskolasz&ouml;vetkezeti tag, vagy tags&aacute;ggal nem rendelkező di&aacute;k/hallgat&oacute; egy adott, a megb&iacute;z&oacute; &aacute;ltal tett foglalkoztat&aacute;sra ir&aacute;nyul&oacute; jogviszonyra vonatoz&oacute; aj&aacute;nlattal kapcsolatos adatkezel&eacute;shez j&aacute;rult hozz&aacute;, az adatkezel&eacute;s időtartama a p&aacute;ly&aacute;z&oacute; visszautas&iacute;t&aacute;s&aacute;t k&ouml;vető 15 napig tart.
                            <br/><i>-	if the Students’ Cooperation member or non-member student/alumni has contributed to the processing of data relating to a specific offer aimed at an employment relationship made by the Client, the duration of the data processing lasts 15 days after the applicant’s rejection.</i>
                            </p>
                            <p>- amennyiben az iskolasz&ouml;vetkezeti tag, vagy tags&aacute;ggal nem rendelkező di&aacute;k/hallgat&oacute; a megb&iacute;z&oacute;, vagy a megb&iacute;zott adatb&aacute;zisban t&ouml;rt&eacute;nő k&eacute;szletező adatkezel&eacute;shez is hozz&aacute;j&aacute;rult, az adatkezel&eacute;s időtartama a hozz&aacute;j&aacute;rul&aacute;s visszavon&aacute;s&aacute;ig, de maximum 5 &eacute;vig tart.
                                <br/><i>-	if the Students’ Cooperation member or non-member student/alumni has also agreed to a reserve data processing in the Client’s or Agent’s database, the duration of the data processing lasts until the withdrawal of the consent, but not more than up to 5 years.</i>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    <p></p>
    <p></p>
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>{{optional($data)->data['name']}}</p>
                <p><strong>Megb&iacute;z&oacute;/ <i> Client</i></strong></p>
            </td>
            <td style="text-align: center;border:none;width:50%">
                <p>____________________________________________________</p>
                <p>MADS - M&aacute;rton &Aacute;ron Szolg&aacute;ltat&oacute; Iskolasz&ouml;vetkezet</p>
                <p><strong>Megb&iacute;zott /<i> Agent</i></strong></p>
            </td>
        </tr>
    </table>


</div>
