<style>
    table tr td
    {
        margin: 0px;
        padding: 3px;
        vertical-align: top;

        font-size: 11px;
        line-height: normal;
        border:solid 1px #ccccc6;
    }
    .paper{

        font-size: 12px;
        text-align: justify;

    }
    .signiture {

        text-align: right;
    }
    .paper div, .paper span
    {
        font-size:11px;
    }
    .head{
        font-size:11px;
        text-align: center
    }
    .paper table.data td{
        border:solid 1px #ccccc6;
        white-space: nowrap;
    }
    i {
        color: #575555;
    }
    table {
        width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;
    }
    </style>
<div class="paper" style="font-size:12px">
        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója / <i>contract No.</i>:
                {{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            <strong>1. sz&aacute;m&uacute; Mell&eacute;klet / <i>Annex No. 1</i></strong>
        </div>
        <p class="head" ><strong>Jogi szem&eacute;ly szerződő partnerek term&eacute;szetes szem&eacute;ly k&eacute;pviselőinek, kapcsolattart&oacute;inak el&eacute;rhetős&eacute;gi adatai a MEGB&Iacute;Z&Oacute; kapcsolattart&oacute;ja r&eacute;sz&eacute;ről</strong><br/>
            <strong><i>Managing contact details of natural persons' representatives and contacts of Legal entity contracting partners by the CLIENT's contact person</i> </strong></p>

        @for($i =0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/>
                            <i>Name of contact person:</i>
                        </strong></p>
                </td>
                <td>
                    <p></p>
                    <p><strong>{{$data->data['first']['contacts'][$i]['name']}}</strong></p>
                    <p></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/>
                            <i>Name of contact person:</i>
                        </strong></p>
                </td>
                <td>
                    <p></p>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['name']}}</strong></p>
                    <p></p>
                </td>
                @endif
            </tr>
            <tr>
                <td>
                    <p></p>
                    <p><strong>telefon-, fax sz&aacute;ma:<br/>
                            <i>Phone/fax number:</i></strong></p>
                    <p></p>
                </td>
                <td>
                    <p>@if(isset($data->data['first']['contacts'][$i]['fax']) && !empty($data->data['first']['contacts'][$i]['fax']))<strong>{{$data->data['first']['contacts'][$i]['fax']}}</strong>@endif</p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p></p>
                    <p><strong>telefon-, fax sz&aacute;ma:<br/>
                            <i>Phone/fax number:</i></strong></p>
                    <p></p>
                </td>
                <td>
                    <p><strong>@if(isset($data->data['first']['contacts'][$i+1]['fax']) && !empty($data->data['first']['contacts'][$i+1]['fax'])){{$data->data['first']['contacts'][$i+1]['fax']}}@endif</strong></p>
                </td>
                @endif
            </tr>

            <tr>
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma: <br/>
                            <i>Mobile number:</i>
                        </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['phone']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>
                    <p><strong>mobiltelefon sz&aacute;ma:<br/>
                            <i>Mobile number:</i> </strong></p>
                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['phone']}}</strong></p>
                </td>
                @endif
            </tr>
            <tr>
                <td>

                    <p><strong>e-mail c&iacute;me:<br/>
                            <i> E-mail address:</i></strong></p>

                </td>
                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i]['email']}}</strong></p>
                </td>
                @if(isset($data->data['first']['contacts'][$i+1]))
                <td>

                    <p><strong>e-mail c&iacute;me:<br/>
                            <i>E-mail address:</i></strong></p>

                </td>

                <td>
                    <p><strong>{{$data->data['first']['contacts'][$i+1]['email']}}</strong></p>
                </td>
                @endif
            </tr>
            </tbody>
        </table>
        @endfor
        <p class="head"><strong>Információk / <i>Information</i>:</strong></p>

        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>adatkezelő:<br/><i>Data controller</i></strong></p>
                </td>
                <td >
                    <p>MADS - M&aacute;rton &Aacute;ron Szolg&aacute;ltat&oacute; Iskolasz&ouml;vetkezet</p>
                    <p><i>MADS - Márton Áron Service Provider Students’ Cooperation</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezelő honlapja:<br/><i>The data controller’s website:</i></strong></p>
                </td>
                <td >
                    <p>www.mads.hu</p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:<br/><i>Purpose of data processing:</i></strong></p>
                </td>
                <td >
                    <p>szerződ&eacute;s teljes&iacute;t&eacute;se, &uuml;zleti c&eacute;l&uacute; kapcsolattart&aacute;s a felek k&ouml;z&ouml;tt<br/><i>performance of the contract, maintenance of business contact between the parties.</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>az adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                </td>
                <td >
                    <p>t&ouml;rv&eacute;nyes k&eacute;pviselő tekintet&eacute;ben szerződ&eacute;s teljes&iacute;t&eacute;se, m&iacute;g kijel&ouml;lt kapcsolattart&oacute; tekintet&eacute;ben az adatkezelő jogos &eacute;rdeke<br/><i>the performance of a contract in the case of a legal representative and the legitimate interest of the controller in the case of a designated contact person</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:<br/><i>Recipients of the personal data:</i></strong></p>
                </td>
                <td >
                    <p>a megb&iacute;zott &uuml;gyf&eacute;lkiszolg&aacute;l&aacute;ssal kapcsolatos feladatokat ell&aacute;t&oacute; munkav&aacute;llal&oacute;i; a megb&iacute;z&oacute;n&aacute;l k&uuml;lső szolg&aacute;ltat&aacute;s teljes&iacute;t&eacute;s&eacute;ben r&eacute;sztvevő iskolasz&ouml;vetkezeti tagok<br/><i>employees of the Company performing customer service tasks; members of the Students’ Cooperation involved in the provision of external services at the Client</i></p>
                </td>
            </tr>
            <tr>
                <td >
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:<br/><i>duration of storage of personal data:</i></strong></p>
                </td>
                <td >
                    <p>Az &uuml;zleti kapcsolat, illetve az &eacute;rintett kapcsolattart&oacute;i minős&eacute;g&eacute;nek fenn&aacute;ll&aacute;s&aacute;t k&ouml;vető 5 &eacute;vig.<br/><i>For 5 years after the existence of the business relationship or the status of the contact person concerned. </i></p>
                </td>
            </tr>
            </tbody>
        </table>

    <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l: / <i> Information on the rights of the data subject:</i></strong></p>
    <table class="">
        <tr>
            <td style="width:50%">Önnek joga van kérelmezni az adatkezelőtől az Önre vonatkozó személyes adatokhoz való hozzáférést, azok helyesbítését, törlését vagy kezelésének korlátozását, és tiltakozhat az ilyen személyes adatok kezelése ellen, valamint a joga van az adathordozhatósághoz.
                <br/>Joga van megbízotthoz vagy bírósághoz fordulna, a felügyeleti hatósághoz (Nemzeti Adatvédelmi és Információszabadság Hatóság) panaszt benyújtani.
                <br/>További információk az adatkezelő honlapján elérhető Adatkezelési tájékoztatóban olvashatók.
                <br/><br/>
                <strong>A fenti információkat és adatvédelmi tájékoztatást, valamint a személyes adataim fenti célú kezelését tudomásul vettem.</strong>
            </td>
            <td style="width:50%">
                <i>
                    You have the right to request from the controller access to, rectification, erasure or restriction of processing of personal data relating to you and to object to the processing of such personal data and you have the right to data portability.
                    <br/>You have the right to contact to the Agent or to the court, or file a complaint with the Supervisory Authority (National Authority for Data Protection and Freedom of Information)
                    <br/> Further information can be found in the Privacy Policy available on the Controller’s website.
                    <br/><br/>
                    <strong>I have noted the above information and the privacy statements and the processing of my personal data for such purposes.</strong>

                </i>
            </td>
        </tr>
    </table>

    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
    @for($i=0; $i< sizeof($data->data['first']['contacts'])/2;$i++)
    <table class="signature" style="border:none">
        <tr>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                <p>____________________________________________________</p>
                <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
            <td style="text-align: center;border:none;width:50%">
                @if(isset($data->data['first']['contacts'][$i+1]))
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i+1]['name']}}</p>
                @else
                    <p>____________________________________________________</p>
                    <p>{{$data->data['first']['contacts'][$i]['name']}}</p>
                @endif
            </td>
        </tr>
    </table>
    @endfor
        <pagebreak></pagebreak>




        <div style="text-align: right;color:#3c4043">
            <strong>szerződés azonosítója / <i>contract No</i>:{{$data->data['number']}}
            </strong>
        </div>
        <div style="text-align: right">
            <strong>1. sz&aacute;m&uacute; Mell&eacute;klet/ <i>Annex No. 1</i></strong>
        </div>

        <p class="head"><strong>Jogi szem&eacute;ly szerződő partnerek term&eacute;szetes szem&eacute;ly k&eacute;pviselőinek, kapcsolattart&oacute;inak el&eacute;rhetős&eacute;gi adatai a MEGB&Iacute;ZOTT kapcsolattart&oacute;ja r&eacute;sz&eacute;ről<br/>
            <i>Managing contact details of natural persons' representatives and contacts of Legal entity contracting partners by the AGENT's contact person </i></strong>
        </p>
        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>Vir&aacute;g Viktor</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                <td>
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                </td>
                <td >
                    <p>+36 (1) 700 1773</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->fax}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                </td>
                <td >
                    <p>+36 (20) 979-7927</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                </td>
                <td >
                    <p>virag.viktor@mads.hu</p>
                </td>
                @if(sizeof($data->data['first']['madsContact'])>0)
                    @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
    @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2; $i++)
        @php $user = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
        <table>
            <tbody>
            <tr>
                <td >
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->lastname}} {{optional($user)->firstname}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                <td >
                    <p><strong>kapcsolattart&oacute; neve:<br/><i>Name of contact person:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                </td>
                @endif
            </tr>
            @if(isset($data->data['first']['madsContact'][$i+1]))
                @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp

                <td >
                    <p><strong>telefon-, fax sz&aacute;ma:<br/><i>Phone/fax number:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user2)->fax}}</p>
                </td>
            @endif
            <tr>
                <td >
                    <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->phone}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>mobiltelefon sz&aacute;ma:<br/><i>Mobile number:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->phone}}</p>
                    </td>
                @endif
            </tr>
            <tr>
                <td >
                    <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                </td>
                <td >
                    <p>{{optional($user)->email}}</p>
                </td>
                @if(isset($data->data['first']['madsContact'][$i+1]))
                    @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                    <td >
                        <p><strong>e-mail c&iacute;me:<br/><i>E-mail address:</i></strong></p>
                    </td>
                    <td >
                        <p>{{optional($user2)->email}}</p>
                    </td>
                @endif
            </tr>
            </tbody>
        </table>
      @endfor

    <p class="head"><strong>Inform&aacute;ci&oacute;k / <i>Information</i>:</strong></p>

        <table>
            <tbody>
            <tr>
                <td>
                    <p><strong>adatkezelő:<br/><i>Data controller:</i></strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['controller']}}<br/><i>{{optional($data->data['first'])['controller_en']}}</i></strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezelő honlapja:<br/><i>The data controller’s website:</i></strong></p>
                </td>
                <td >
                    <p><strong>{{$data->data['first']['website']}}</strong></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s c&eacute;lja:<br/><i>Purpose of data processing:</i></strong></p>
                </td>
                <td >
                    <p>szerződ&eacute;s teljes&iacute;t&eacute;se, &uuml;zleti c&eacute;l&uacute; kapcsolattart&aacute;s a felek k&ouml;z&ouml;tt<br/><i>performance of the contract, maintenance of business contact between the parties.</i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>az adatkezel&eacute;s jogalapja:<br/><i>Legal basis of data processing:</i></strong></p>
                </td>
                <td >
                    <p>t&ouml;rv&eacute;nyes k&eacute;pviselő tekintet&eacute;ben szerződ&eacute;s teljes&iacute;t&eacute;se, m&iacute;g kijel&ouml;lt kapcsolattart&oacute; tekintet&eacute;ben az adatkezelő jogos &eacute;rdeke<br/><i>the performance of a contract in the case of a legal representative and the legitimate interest of the controller in the case of a designated contact person</i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok c&iacute;mzettjei:<br/><i>Recipients of the personal data:</i></strong></p>
                </td>
                <td >
                    <p>a megb&iacute;z&oacute; &uuml;gyf&eacute;lkiszolg&aacute;l&aacute;ssal kapcsolatos feladatokat ell&aacute;t&oacute; munkav&aacute;llal&oacute;i;<br/><i>employees of the Company performing customer service tasks; </i></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p><strong>a szem&eacute;lyes adatok t&aacute;rol&aacute;s&aacute;nak időtartama:<br/><i>duration of storage of personal data:</i></strong></p>
                </td>
                <td >
                    <p>Az &uuml;zleti kapcsolat, illetve az &eacute;rintett kapcsolattart&oacute;i minős&eacute;g&eacute;nek fenn&aacute;ll&aacute;s&aacute;t k&ouml;vető 5 &eacute;vig.<br/><i>For 5 years after the existence of the business relationship or the status of the contact person concerned</i></p>
                </td>
            </tr>
            </tbody>
        </table>

    <p class="head"><strong>T&aacute;j&eacute;koztat&aacute;s az &eacute;rintett jogair&oacute;l / <i>Information on the rights of the data subject:</i></strong></p>
    <table>
        <tr>
            <td style="width: 50%">Önnek joga van kérelmezni az adatkezelőtől az Önre vonatkozó személyes adatokhoz való hozzáférést, azok helyesbítését, törlését vagy kezelésének korlátozását, és tiltakozhat az ilyen személyes adatok kezelése ellen, valamint a joga van az adathordozhatósághoz.
                <br/>Joga van megbízóhoz vagy bírósághoz fordulna, a felügyeleti hatósághoz (Nemzeti Adatvédelmi és Információszabadság Hatóság) panaszt benyújtani.
                <br/>További információk az adatkezelő honlapján elérhető Adatkezelési tájékoztatóban olvashatók.
                <br/><br/>
                <strong>A fenti információkat és adatvédelmi tájékoztatást, valamint a személyes adataim fenti célú kezelését tudomásul vettem.</strong>
            </td>
            <td style="width: 50%">
                <i>
                You have the right to request from the controller access to, rectification, erasure or restriction of processing of personal data relating to you and to object to the processing of such personal data and you have the right to data portability.
                <br/>You have the right to contact to the Client or to the court, or file a complaint with the Supervisory Authority (National Authority for Data Protection and Freedom of Information)
                <br/>Further information can be found in the Privacy Policy available on the Controller’s website.
                <br/><br/>
                <strong>I have noted the above information and the privacy statements and the processing of my personal data for such purposes.</strong>
                </i>

            </td>
        </tr>
    </table>
    @if(sizeof($data->data['first']['madsContact'])>1)
        <p><br/></p>
        <p><br/></p>
        <p><br/></p>

    @endif
    <p>Kelt /<i>Dated</i>, Budapest, {{date('Y. m. d.',strtotime($data->data['date']))}} napj&aacute;n / <i>{{date('d',strtotime($data->data['date']))}}. of {{date('m. Y',strtotime($data->data['date']))}}</i></p>
        <table class="signature" style="border:none">
            <tr>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        <p>____________________________________________________</p>
                        <p>Virág Viktor</p>
                    @endif
                </td>
                <td style="text-align: center;border:none;width:50%">
                    @if(isset($data->data['first']['madsContact'][0]))
                        @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][0]['user'])->first(); @endphp
                        <p>____________________________________________________</p>
                        <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                    @else
                        <p>____________________________________________________</p>
                        <p>Virág Viktor</p>
                    @endif
                </td>
            </tr>
        </table>
        @for($i=1; $i<= sizeof($data->data['first']['madsContact'])/2;$i++)
            <table class="signature" style="border:none">
                <tr>
                    <td style="text-align: center;border:none;width:50%">
                        @if(isset($data->data['first']['madsContact'][$i]))
                            @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i]['user'])->first(); @endphp
                            <p>____________________________________________________</p>

                            <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                        @endif
                    </td>
                    <td style="text-align: center;border:none;width:50%">
                        @if(isset($data->data['first']['madsContact'][$i+1]))
                            @php $user2 = \Modules\User\Entities\User::where('id',$data->data['first']['madsContact'][$i+1]['user'])->first(); @endphp
                            <p>____________________________________________________</p>
                            <p>{{optional($user2)->lastname}} {{optional($user2)->firstname}}</p>
                        @endif
                    </td>
                </tr>
            </table>
        @endfor

</div>
