<?php
$date = date('Y-m-d');
if(isset($params['document_date']) && !empty($params['document_date']) && $params['document_date'] != 'undefined'){
    $date = date('Y-m-d',strtotime($params['document_date']));
}
if(isset($membership)){
    $date = date('Y-m-d');
}
?>
<table style="width: 100%; margin: 0 0 0 0;font-size: 12px;">
    <tbody>
    <tr>
        <td align="left">Kelt: </td>
        <td style="width:61%">{{date('Y-m-d',strtotime($date))}}</td>
        <td></td>
        <td>.........................................................................</td>

    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: center">{{$student->name}}</td>

    </tr>
    </tbody>
</table>
<div class="bordr" style="font-size:12px; margin: 14pt auto 0 auto;border:solid 1px silver;  padding:10px;padding-right:10px;line-height:normal">
					<span><span id="tag_ala_b2">{{$student->name}}</span> .....................................................................................T Tagfelvétele jogi és egyéb akadályokba nem ütközik, a MADS -Márton Áron Szolgáltató Iskolaszövetkezet Igazgatósága az  ................................-ei ülésen felvette a Szövetkezet tagjai közé.

					</span>
    <table style="width: 100%; margin: 6pt 0 0 0;font-size:12px">
        <tbody>
        <tr>
            <td align="right">Budapest, </td>
            <td style="width:33%">.....................................</td>
            <td></td>
            <td>...............................................................................................</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: center"><span style="margin-left:100px">Igazgatósági tag</span></td>

        </tr>
        </tbody>
    </table>
</div>
<div class="bordr" style="border:solid 1px silver;margin-top:10px; padding:10px;line-height:normal;font-size:12px">
    <table style="width: 100%; margin: 10pt 0 10pt 0;font-size:12px">
        <tbody>
        <tr>
            <td align="left" style="font-size: 12pt;">Záradék</td>
            <td align="right" style="font-size: 12pt;">!kitöltés kilépéskor!</td>
        </tr>
        </tbody>
    </table>
    <div style="font-size:12px">Tagsági viszony megszüntetése:
    Vagyoni hozzájárulásom /részjegy/ összegét visszakaptam, a Szövetkezettel szemben semmiféle követelésem nincs.
    </div>
    <table style="width: 100%; margin: 16pt 0 0 0;font-size:12px">
        <tbody>
        <tr>
            <td align="right">Budapest, </td>
            <td style="width:33%">.....................................</td>
            <td></td>
            <td>...............................................................................................</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: center"><span style="margin-left:100px">{{$student->name}}</span></td>

        </tr>
        </tbody>
    </table>
</div>
