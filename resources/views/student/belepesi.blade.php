﻿
<div class="paper" id="belepesi" style="width: 210mm; height: 327mm; font-size:15px;">
    <style>
        .paper table tr td
        {
            margin: 0px;
            padding: 1px;
            vertical-align: top;
            font-family: Arial;
            font-size: 12px;
            line-height: normal;
        }
        .paper{


            font-family: arial;
            font-size: 12px;
            text-align: justify;

        }
        .parag, .para10 {
            text-indent: 30pt;
            text-align: justify;
            line-height: initial;
            font-family: arial;
        }
        .paper div, .paper span
        {
            font-size:12px;
        }
        .paper table.data td{
            border:solid 1px #ccccc6;
            white-space: nowrap;
        }

    </style>

    <table class="data" style="width: 100%; margin:0 0 0 0; font-size:14px; border:solid 1px #ccccc6;">
        <tbody>
        <tr>
            <td><span class="bold" style="width:12%;font-size:12px">Diákigazolvány:</span></td>
            <td  colspan="3"><span style="font-size:12px" class="silver">{{optional($student->card()->orderBy('temporary_student_card_expire','desc')->first())->student_card}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:12px" class="bold" >Név:</span></td>
            <td style="width:36%;font-size:12px" ><span class="silver">{{$student->name}}</span></td>
            <td><span style="font-size:12px" class="bold">Adóazonosó jele:</span></td>
            <td style="width:31%;font-size:12px"><span class="silver">{{$student->tax_number}}</span> </td>
        </tr>
        <tr>
            <td ><span style="font-size:12px" class="bold">Anyja neve:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->mother_name}}</span></td>
            <td><span style="font-size:12px" class="bold">TAJ. száma:</span></td>
            <td><span  style="font-size:12px"class="silver">{{$student->taj}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:12px"  class="bold">Születési helye:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->birth_location}}</span></td>
            <td><span style="font-size:12px" class="bold">Születési ideje:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->birth_date}}</span></td>
        </tr>


        <tr>
            <td><span class="bold" style="width:12%;font-size:12px">Bankszámlaszáma:</span></td>
            <td  colspan="3"><span style="font-size:12px" class="silver">{{$student->bank_account_number}}</span></td>

        </tr>

        <tr>
            <td><span style="font-size:12px" class="bold">Állandó lakcíme:</span></td>
            <td colspan="3"><span  style="font-size:12px"class="silver">
                    {{$student->getAddress}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:12px" class="bold">Ideiglenes lakcíme:</span></td>
            <td colspan="3"><span style="font-size:12px" class="silver">{{$student->notification_address}}</span></td>
        </tr>

        <tr>
            <td><span style="font-size:12px" class="bold">Telefon:</span></td>
            <td ><span style="font-size:12px" class="silver">{{$student->phone}}</span></td>
            <td><span style="font-size:12px" class="bold">E-mail:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->email}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:12px" class="bold">Személyi ig. száma:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->identity_card}}</span></td>
            <td><span style="font-size:12px" class="bold">Útlevél száma(külföldieknél):</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->passport}}</span></td>
        </tr>

        <tr>
            <td><span style="font-size:12px" class="bold">Oktatási intézmény neve,címe:</span></td>
            <td colspan="3"  class="silver" style="font-size:12px">{{optional($student->school()->orderBy('end','desc')->first())->name}} {{optional($student->school()->orderBy('end','desc')->first())->address}}</td>
        </tr>

        <tr>
            <td><span style="font-size:12px" class="bold">Hallgatói jogviszony kezdete:</span></td>
            <td><span style="font-size:12px" class="silver">{{optional($student->certificate()->orderBy('end','desc')->first())->start}}</span></td>
            <td><span style="font-size:12px" class="bold">Állampolgársága:</span></td>
            <td><span style="font-size:12px" class="silver">{{$student->natioanligy}}</span></td>
        </tr>

        <tr>
            <td style="font-size:12px">Megváltozott munkaképességű:</td>
            <td colspan="3"><span style="font-size:12px" class="silver"></span></td>
        </tr>
        </tbody>
    </table>
    <p class="parag" style="font-size:12px">
        Alulírott, jelen nyilatkozat aláírásával elismerem, hogy a tagfelvétellel egyidejűleg 2000.- Ft, azaz kettőezer magyar forint vagyoni hozzájárulási kötelezettségem keletkezik, egyben visszavonhatatlan kötelezettséget vállalok ezen, vagyoni hozzájárulás /részjegy/ teljesítésére. Tudomásul veszem, hogy ezt az összeget a Szövetkezet az első esedékes jövedelmemből levonja. Tudomásul veszem, hogy, jelen nyilatkozat aláírásától számított 1. /egy/ éven belül a teljes vagyoni hozzájárulásomat köteles vagyok teljesíteni. Tudomásul veszem továbbá, hogy tagságom megszűnése esetén a Szövetkezet egy hónapon belül elszámol velem.

        Aláírásommal elismerem, hogy az általam közölt adatok a valóságnak megfelelnek. A belépési nyilatkozatot az Igazgatóság tagfelvételről szólódöntése után tagsági megállapodásként, magamra nézve kötelező érvényűnek ismerem el.
    </p>



</div>

