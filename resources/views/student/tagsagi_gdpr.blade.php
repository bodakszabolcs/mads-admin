﻿
<div class="paper" id="belepesi" style="width: 210mm; height: 560mm; margin: 10pt auto 10pt auto;font-size:15px;">
    <style>
        .paper table tr td
        {
            margin: 0px;
            padding: 1px;
            vertical-align: top;
            font-family: Arial;
            font-size: 11px;
            line-height: normal;
        }
        .parag, .para10 {
            text-indent: 40pt;
            text-align: justify;
            line-height: initial;
            font-family: arial;
        }
        .paper div, .paper span
        {
            font-size:12px;
        }
        .paper table.data td{
            border:solid 1px #ccccc6;
            white-space: nowrap;
        }

    </style>
    <p class="parag" style="font-size:11px">
        Tagsági jogviszonyon alapuló, az Iskolaszövetkezet és Tagja közötti gazdasági együttműködésre, a Tag személyes közreműködésére irányuló jogviszony létesítéséről, amely létrejött egyrészről, a MADS - Márton Áron Szolgáltató Iskolaszövetkezet, /székhelye: 1092 Budapest, Erkel utca 3. földszint, adószáma: 11931335-2-43., képviseli: Lator Sándor, az Igazgatósági elnöke, önállóan, valamint Virág Viktor, az Igazgatóság tagja, önállóan, továbbá Szántó Zsolt Sándor, az Igazgatóság tagja, önállóan - továbbiakban Szövetkezet vagy Iskolaszövetkezet -/ másrészről::
    </p>

    {!! view('student.tagsagiStudent',['student'=>$student,'params'=>$params]) !!}



    <p class="parag" style="font-size:11px">
        -	továbbiakban: Tag - között a mai napon az alábbi feltételekkel:<br>
        1.	Szerződő felek rögzítik, hogy a szövetkezetekről szóló 2006. évi X. törvény / továbbiakban: Sztv. / 10/A. § (1) bekezdése kimondja, hogy az iskolaszövetkezet és a nappali tagozatos tanuló, hallgató tagja közötti gazdasági együttműködést, a tag személyes közreműködésének módját – az alapszabály keretei közt – tagsági megállapodásban kell meghatározni, továbbá az Sztv. 10/B. § (1) bekezdése rögzíti, hogy az iskolaszövetkezet nappali tagozatos tanuló, hallgató tagja a személyes közreműködését az iskolaszövetkezet által harmadik személy részére nyújtott szolgáltatás / továbbiakban: külső szolgáltatás / keretében is teljesítheti.
        <br>
        2.	A Tag jelen megállapodás aláírásával kijelenti, hogy az Iskolaszövetkezetbe tagként be kíván lépni, valamint, hogy az Iskolaszövetkezet Alapszabályát, illetve az abban foglaltakat megismerte, és azokat magára nézve kötelezőnek ismeri el. A Tag jelen okirat aláírásával kijelenti továbbá, hogy az Iskolaszövetkezet feladatainak megvalósításában személyes közreműködésével kíván részt venni. A Tag kijelenti, hogy az Iskolaszövetkezet tartozásaiért vagyoni hozzájárulása erejéig felelősséget vállal.
        <br>
        3.	A Tag jelen nyilatkozat aláírásával elismeri, hogy a tagfelvétellel egyidejűleg 2000.- Ft, azaz kettőezer magyar forint vagyoni hozzájárulási kötelezettsége keletkezik, egyben visszavonhatatlan kötelezettséget vállal ezen vagyoni hozzájárulás /részjegy/ teljesítésére. Tudomásul veszi, hogy ezt az összeget az Iskolaszövetkezet az első esedékes díjából levonja. Tudomásul veszi, hogy jelen tagsági megállapodás aláírásától számított 1 /egy/ éven belül a teljes vagyoni hozzájárulást köteles teljesíteni. Felek tudomásul veszik továbbá, hogy a Tag tagsági jogviszonyának megszűnése esetén egy hónapon belül jogosultak és kötelesek elszámolni egymással.
        <br>
        4.	A Tag jelen nyilatkozat aláírásával elismeri, hogy az általa közölt adatok a valóságnak megfelelnek, a tagsági megállapodást magára nézve kötelező érvényűnek ismeri el.
        <br>
        5.	Az Iskolaszövetkezet a Tagot, amíg annak tagsági jogviszonya a MADS - Márton Áron Szolgáltató Iskolaszövetkezettel fennáll, külső szolgáltatás keretében történő feladatteljesítéssel bízza meg, a Szövetkezet megbízóinál / szolgáltatás fogadója /, változó helyszíneken, változó díjazás és időbeosztás szerint, mely megbízás kereteit jelen tagsági megállapodás tartalmazza.
        <br>
        6.	Az Iskolaszövetkezet jelen okirat aláírásával kijelenti, egyben kötelezettséget vállal arra, hogy a tizennyolcadik életévét be nem töltött Tagnak az Iskolaszövetkezet üzletszerű gazdasági tevékenységében történő személyes közreműködésére az Mt.-ben foglalt, a fiatal munkavállalók munkajogi védelmére vonatkozó rendelkezések figyelembevételével kerül sor.
        <br>
        7.	A felek rögzítik, hogy a tizennyolcadik életévét be nem töltött Tag jognyilatkozatának érvényességhez a törvényes képviselőjének az írásbeli hozzájárulása szükséges, mely hozzájárulás – szükség esetén - jelen tagsági megállapodás mellékletét képezi.
        <br>
        8.	A Tag személyes közreműködésének külső szolgáltatás keretében történő teljesítése megkezdését megelőzően a Felek írásban eseti megállapodásokat kötnek, mely eseti megállapodások a Felek jóváhagyó aláírásával jelen tagsági megállapodás kiegészítésévé válnak. Az eseti megállapodásokban a Felek rögzítik a Tag által vállalt tényleges feladatok körét, a szolgáltatás fogadójának személyét, a Tag díjazásának összegét, valamint a díjazás kifizetésének időpontját, a feladat teljesítésének helyét, továbbá a feladat teljesítésének időtartamát.
        <br>
        9.	A Tag kötelezettséget vállal arra, hogy legkésőbb a külső szolgáltatás keretében történő feladatteljesítés megkezdését megelőzően az Iskolaszövetkezet számára a tárgyi félévre vonatkozó iskolalátogatási igazolását /hallgatói jogviszony igazolását leadja, az érvényes matricával ellátott diákigazolványát bemutatja. A Tag tudomásul veszi, hogy az Iskolaszövetkezeti tagsági jogviszonya is megszűnik a tanulói vagy a hallgatói jogviszonya megszűnésével.
        <br>
        10.	A Tagot az egyes tényleges feladatok teljesítéséért az eseti megállapodásban külön meghatározott díj illeti meg, amelynek mértéke – nem teljesítményarányos díjazású feladatok esetén – eléri az Mt. 153. §-ában foglalt felhatalmazás alapján kiadott jogszabályban meghatározott minimális összeget. A Tagot megillető díj a szolgáltatás fogadója által leigazolt feladatteljesítéssel járó időtartammal arányosan, a feladatteljesítést követő hónap esedékes fizetési időpontjaiban kerül kifizetésre, a Tag által megadott pénzintézeti számlára történő átutalással, illetve különös méltánylást érdemlő esetben – előzetes időpontegyeztetést követően - készpénzben.
        <br>
        11.	Jelen Megállapodást a Felek 5 /öt/ éves határozott időtartamra kötik. A Tag és a Szövetkezet megállapodnak abban, hogy jelen Megállapodást közös megegyezéssel szüneteltetik, amennyiben az Iskolaszövetkezet Megbízói a Tag feladatteljesítésére nem tartanak igényt, illetve a Tag nem vállal a Szövetkezetnél egyetlen eseti megbízást sem. Ebben az esetben a Tag számára az addig ténylegesen feladatteljesítéssel járó időtartammal arányosan számított díjazás jár, valamint a tanulói, hallgatói jogviszonnyal nem rendelkező Tag vonatkozásában a munka törvénykönyvéről szóló 2012. évi I. törvény / továbbiakban: Mt. / szerint szabadság.
        <br>
        12.	A tanulói, hallgatói jogviszonnyal nem rendelkező Tagot megillető szabadság mértéke az Sztv. 10/B § (5) bekezdése szerint minden tizenhárom feladatteljesítéssel töltött nap után egy nap. A felek rögzítik, hogy a szabadság pénzbeli megváltására csak a tagsági jogviszony megszűnése esetén kerülhet sor.
        <br>
        13.	Az Iskolaszövetkezet kötelezettséget vállal arra, hogy az Sztv. 10/B § (3) bekezdésének megfelelően a Tag részére, ha a feladatteljesítés tartama a napi hat órát meghaladja, napi húsz, ha a napi kilenc órát meghaladja, további napi huszonöt perc munkaközi szünetet biztosít, továbbá a Tag részére, ha két egymást követő napon végzi feladatát, a napi munka befejezése és a következő napi munka megkezdése között legalább tizenegy óra pihenőidőt biztosít.
        <br>
        14.	A Felek rögzítik, hogy a tizennyolcadik életévét be nem töltött Tag naponta legfeljebb nyolc, hetente legfeljebb negyven órát tölthet el feladatteljesítéssel, a több szolgáltatást fogadó harmadik személynél teljesített feladatok időtartamát össze kell számítani. Az Iskolaszövetkezet kötelezettséget vállal arra, hogy a tizennyolcadik életévét be nem töltött Tagot este tíz és reggel hat óra között nem kötelezi feladatteljesítésre, az őt megillető heti kettő pihenőnapot nem osztja be egyenlőtlenül, számára négy és fél óra után harminc perc, hat órát meghaladóan negyvenöt perc szünetet, két egymást követő feladatteljesítéssel töltött nap között tizenkettő óra pihenőidőt biztosít.
        <br>
        15. A Tag az egyes szolgáltatások fogadóinál – az eseti megállapodásokban – külön meghatározott műszakbeosztásban és munkaidőben köteles a feladatát teljesíteni, az Iskolaszövetkezet által adott utasítások alapján, amihez a szolgáltatás fogadója szakmai, technikai, gyakorlati, szellemi segítséget nyújt. Szerződő felek megállapodnak, hogy a tizennyolcadik életévét be nem töltött Tag számára utasítást csak az Iskolaszövetkezettel munkaviszonyban álló személy adhat. Az előbbi körbe nem tartozó esetben, valamint ha az utasítás adására baleset, elemi csapás vagy súlyos kár, továbbá az életet, egészséget, testi épséget fenyegető közvetlen és súlyos veszély megelőzése, illetőleg elhárítása érdekében van szükség, a Tag részére utasítást adhat a szolgáltatás fogadójának képviselője is.
        <br>
        16.	A Tag köteles a feladatteljesítés időtartamáról jelenléti ívet vezetni, azt a szolgáltatás fogadójával leigazoltatni, és a feladat teljesítését követő hónap 3. napjáig az Iskolaszövetkezet számára eljuttatni. Amennyiben ezen kötelezettségének határidőben nem tesz eleget, tudomásul veszi, hogy az Iskolaszövetkezet jogosult a havi díját a jelenléti ív leadását követő hónapban esedékes fizetési időpontban kifizetni.
        <br>
        17.	A Tag jelen okirat aláírásával nyilatkozik, hogy Munka- és Balesetvédelmi alapoktatásban részesült. A Tag a munka- és balesetvédelmi, valamint tűzvédelmi szabályokat megismerte, jelen szerződés aláírásával kötelezettséget vállal a szabályok mindenkori betartására. A Tag tudomásul veszi, hogy a külső szolgáltatás keretében történő feladatteljesítés során a Munka- és Balesetvédelmi oktatást a szolgáltatás fogadója szervezi meg számára, és jelen szerződés aláírásával kötelezettséget vállal a szabályok mindenkori betartására.
        <br>
        18.	A Tag kötelezettséget vállal arra, hogy saját elhatározásából a külső szolgáltatás helyszínét feladatteljesítés közben nem hagyja el. Abban az esetben, ha a szolgáltatás fogadójának képviselője bármilyen okból erre felszólítja, a Tag köteles azonnal az Iskolaszövetkezetet értesíteni. A Tag a vállalt feladatokat a meghatározott időtartamban pontosan, a képességeihez, képzettségéhez mért legmagasabb színvonalon teljesíti, a feladatteljesítés helyszínén késlekedés nélkül megjelenik. A Tag kötelezettséget vállal arra, hogy igazolatlanul nem hiányzik a feladatteljesítés helyszínéről. Igazolatlan hiányzásnak minősül a feladat ellátásának az eseti megállapodás lejárta előtti, önhibából történő befejezése is. Igazolt hiányzásnak minősül az Iskolaszövetkezet vezetői által igazolt hiányzás és indokolt esetben az orvosi igazolás által igazolt hiányzás. A Tag kötelezettséget vállal arra, hogy az Iskolaszövetkezetet legkevesebb huszonnégy órával előbb értesíti, ha az általa vállalt feladatokat a vállalt időpontban nem tudja ellátni.
        <br>
        19.	A külső szolgáltatás teljesítésében résztvevő Tag károkozása vagy személyiségi jog megsértése esetén az Iskolaszövetkezet és a szolgáltatás fogadójának megállapodása alapján az alkalmazott károkozásáért való felelősség polgári jogi szabályait kell alkalmazni.
        <br>
        20.	A szolgáltatás fogadója részére történő feladatteljesítés során a külső szolgáltatás teljesítésében résztvevő Tagnak okozott kárért vagy személyiségi jogai megsértéséért az Iskolaszövetkezet és a szolgáltatás fogadója egyetemlegesen felel.
        <br>
        21.	A Tag köteles ingyen kijavítani az általa hibásan ellátott feladatot, amennyiben ezt nem teszi meg, tudomásul veszi, hogy a kijavítás költsége a díjából levonásra kerül. Amennyiben a Tag a rábízott eszközöket, termékeket, anyagi javakat nem a tőle elvárható gondossággal kezeli, illetve azokban kárt tesz, illetve a visszaszolgáltatási kötelezettséggel átvett eszközöket az Iskolaszövetkezet által meghatározott határidőig nem szolgáltatja vissza, tette anyagi és büntetőjogi felelősségre vonást eredményez.
        <br>
        22.	A Tag jelen okirat aláírásával tudomásul veszi, hogy abban az esetben, ha a feladatteljesítést az általa vállalt időpontban és helyszínen nem kezdi meg, és a feladatteljesítés tervezett kezdetét megelőző minimum 24 /huszonnégy/ órán belül a megbízó, illetve az iskolaszövetkezet kijelölt képviselőjét nem értesíti távolmaradásáról, úgy az iskolaszövetkezet az elvállalt napnak megfelelő megbízási díjból 30%, de legalább 5.000, - Ft/nem bejelentett távolmaradás, azaz előre be nem jelentett távolmaradásonként ötezer magyar forint visszatartására jogosult. A Tag jelen lényegi kikötést kifejezetten elismeri és nyugtázza.
        <br>

        23.	Az Iskolaszövetkezet rendkívüli felmondási oknak tekinti a Tag igazolatlan hiányzását, továbbá a több alkalommal észlelt hanyag, vagy mennyiségében, illetőleg minőségében nem megfelelő feladatteljesítést.
        <br>
        24.	Amíg a jelen megállapodás hatályban van, a Tag az Iskolaszövetkezet megbízóinál csak az Iskolaszövetkezet által nyújtott külső szolgáltatás keretében, az Iskolaszövetkezet tagjaként láthat el feladatokat. Amennyiben jelen pontot megszegi, a Tag kötelezi magát, hogy azonnal kilép a Szövetkezetből, vállalva annak minden következményét.
        <br>
        25.	A Tag a tagsági jogviszonyból származó kötelezettségének megszegésével okozott kárt köteles megtéríteni, ha nem úgy járt el, ahogy az adott helyzetben általában elvárható.
        <br>
        26.	A Tagot a feladatteljesítése során tudomására jutott minősített, vagy más titkot képező, illetőleg nem nyilvános, bizalmas adatok és információk, továbbá a személyes adatok, információk tekintetében, az ezekre vonatkozó jogszabályi előírásoknak megfelelő titoktartási kötelezettség terheli, mely kötelezettsége megszegése esetén teljes kártérítési felelősség terheli mind az Iskolaszövetkezet, mind a külső szolgáltatás fogadói felé. A Tag kötelezettséget vállal arra, hogy a külső szolgáltatás teljesítése során tudomására jutott személyes adatokat kizárólag a feladatai teljesítése céljából kezeli és továbbítja, más célra nem használja, azokat  illetéktelen személlyel nem közli és részükre nem adja át, a személyes adatokhoz  jogosulatlan hozzáférést nem enged, a személyes adatokat nyilvánosságra nem hozza. A Tag tudomásul veszi, hogy ezen titoktartási kötelezettsége a munkavégzésre irányuló egyéb jogviszonya fennállását követően is terheli.
        <br>
        27.	Az Iskolaszövetkezet tájékoztatja a Tagot arról, hogy személyes adatait kizárólag a tagsági jogviszony teljesítésével, valamint foglalkoztatásra irányuló egyéb jogviszony létrehozásával kapcsolatban kezeli, a 2011. évi CXII. törvény és a GDPR rendelet előírásai szerint.
        <br>
        28.	A Tag jelen megállapodás aláírásával nyilatkozza, hogy az Iskolaszövetkezet által közzétett Adatkezelési Tájékoztató tartalmát megismerte.
        <br>
        29.	Az Iskolaszövetkezet tájékoztatja a Tagot arról, hogy a feladatteljesítésre vonatkozó adatvédelmi szabályokkal, rendelkezésekkel, valamint a Tag adatainak kezelésével kapcsolatos kérdéseivel az Iskolaszövetkezet adatvédelmi felelőséhez fordulhat.
        <br>
        30.	A Tag jelen megállapodás aláírásával kifejezetten hozzájárulását adja személyes adatainak - azonosításához szükséges személyes adatok, a kapcsolattartáshoz szükséges adatok, valamint a meghirdetett feladatkörre való alkalmasság megítéléséhez esetlegesen szükséges további személyes adatok - az Iskolaszövetkezet azon megbízói, mint külső szolgáltatás fogadói, részére történő továbbításához, amely külső szolgáltatás fogadói által biztosított álláspályázatra - a külső szolgáltatás fogadója személyének és adatkezelésre vonatkozó tájékoztatásának ismeretében - a Tag jelezte jelentkezési/pályázási szándékát az Iskolaszövetkezet felé.
        <br>
        31.	A Felek rögzítik, hogy a feladatteljesítéssel nem járó időtartam alatt köztük a kapcsolattartás e-mail, illetve telefon útján történik, elsődlegesen az eseti megállapodásban rögzített, az Iskolaszövetkezet által kijelölt Kapcsolattartó útján, annak hiányában az Iskolaszövetkezet internetes weblapján /www.mads.hu/ található elérhetőségek bármelyikén.
        <br>
        32.	Jelen megállapodás által nem szabályozott egyéb kérdésekben az Ptk. és az Sztv. vonatkozó rendelkezései, illetve az Sztv. kifejezett ilyen irányú rendelkezése esetén az Mt. rendelkezései az irányadóak.
        <br>
        33.	Jelen megállapodás 2 /kettő/ példányban készült, melyből egy az Iskolaszövetkezetet, egy a Tagot illeti meg.


    </p>

    <p>&nbsp;</p>

    {!! view('student.belepesiFooter',['student'=>$student,'params'=>$params]) !!}

</div>
