<div id="esetiPage">

    <div class="paper" id="eseti" >
        <style>
            .paper div, .paper span, .paper p
            {
                font-size:11px;
            }
            .paper table tr td
            {
                margin: 0px;
                padding: 1px;
                vertical-align: top;
                font-size:15px;
                line-height: normal;
            }
            .paper{
                background: #ffffff;
                font-size: 11px;
                text-align: justify;
                font-size:15px;
            }
            .parag, .para10 {
                text-indent: 30pt;
                text-align: justify;
                line-height: initial;
                font-size:15px;
            }
        </style>
        <p>As an amendment to the membership agreement concluded by and between student employment agency named MADS - Márton Áron Szolgáltató Iskolaszövetkezet /registered office: H-1092 Budapest, Erkel utca 3. földszint, tax registration number: 11931335-2-43., represented by: LATOR Sándor, chairman of the Board of Directors, individually, hereinafter “Student Employment Agency” or the “Agency”/ on the one hand, and:</p>
        <p><b>NAME: </b><span id="diak_nev">{{$student->name}} </span></p>
        <p><b>MADS ID: </b><span id="diak_ado">{{$student->student_number}}</span></p>
        <p><b>MOTHER’s MAIDEN NAME: </b><span id="diak_ideig">{{$student->mothers_name}} </span></p>
        <p><b>PLACE AND DATE OF BIRTH: </b><span id="diak_allando">{{$student->birth_location}}, {{$student->birth_date}} </span></p>
        <p>/hereinafter: “Member”/, the Student Employment Agency appoints on this day the Member to perform tasks in the framework of external services subject to the following terms and conditions:</p>
        <p><b>TASKS TO BE PERFORMED: </b><span class="diak_munkakor">{{$params['job']}}</span></p>

        <p><b>SCOPE OF TASKS AGREED BY THE MEMBER: </b><span class="diak_munkakor_munkaltato">{{$params['task']}}</span></p>
        <p><b>CHARACTERISTICS OF TASKS AGREED BY THE MEMBER: </b><span class="diak_jellemzo">{{$params['task_details']}}</span></p>
        <p><b>RECIPIENT OF THE SERVICE: </b><span class="diak_megrendelo">{{$params['company']}}</span></p>
        <p><b>PLACE OF PERFORMANCE : </b><span class="diak_hely">{{$params['work_location']}}</span></p>
        <p><b>COMMENCEMENT DATE OF PERFORMANCE: </b><span class="diak_ido">{{$params['date']}}</span></p>
        <p><b>DURATION OF PERFORMANCE: </b><span class="diak_ido_veg">{{$params['work_time']}}</span></p>
        <p><b>DAILY SCHEDULE: </b><span class="diak_munkarend">{{$params['schedule']}}</span></p>
        <p><b>FEE: </b><span class="diak_alapber">{{$params['gross']}}</span></p>
        <p><b>DATE OF PAYMENT: </b><span class="diak_bernap">{{$params['day']}}</span></p>
        <p><b>CONTACT PERSON/HR MANAGER APPOINTED BY THE STUDENT EMPLOYMENT AGENCY: </b><span class="diak_kapcsolat">{{$params['contact']}}</span></p>
        <p><b>CONTACT DETAILS OF THE CONTACT PERSON: </b><span class="diak_kapcsolat_elerhetoseg">{{$params['contact_data']}}</span></p>
        <p><b>CACONTACT PERSON APPOINTED BY THE RECIPIENT OF THE SERVICE : </b><span class="diak_utasito">{{$params['delegate']}}</span></p>
        <p><b>DATA OF THE MEMBER TO BE TRANSFERRED TO THE RECIPIENT OF EXTERNAL SERVICES: name, mother’s name at birth; place and date of birth; telephone number; e-mail address  </b></p>
        <p><b>PURPOSE OF DATA TRANSFER: the performance of this membership agreement, identification, facilitating contact   </b></p>
        <p><b>ACCESSIBILITY OF INFORMATION ON THE PROCESSING OF DATA BY THE RECIPIENT OF THE SERVICE, OR THE MANNER/CIRCUMSTANCES IN WHICH THE INFORMATION ON THE PROCESSING IS PROVIDED </b><span class="tev_aszf">{{isset($params['gdpr'])?$params['gdpr']:''}}</span></p>
        <p><b>FURTHER MATTERS OF IMPORTANCE: </b><span class="diak_kerdes">{{$params['question']}}</span></p>

        <p><b>By signing this individual agreement, the Member undertakes to perform the tasks set out herein by accepting the above conditions.
            </b></p>
        <p>The Member declares that he/she has been informed about the data processing activities of the recipient of the external service and gives his/her explicit and voluntary consent to the transfer of his/her personal data to the recipient of the external service for the stipulated purposes.</p>

        <?php
        $date = date('Y-m-d');
        if(isset($params['document_date']) && !empty($params['document_date'])  && $params['document_date'] != 'undefined'){
            $date = date('Y-m-d',strtotime($params['document_date']));
        }
        ?>

        <p>Budapest,<span id="nap">{{date('d',strtotime($date))}}</span> day. <span id="honap">{{date('m',strtotime($date))}}</span> month  <span id="ev"> {{date('Y',strtotime($date))}}</span> year  </p>

        <table style="width:100%" border="0">
            <tr>
                <td style="text-align:center">........................................................
                </td>
                <td style="text-align:center">........................................................
                </td>
            </tr>
            <tr>
                <td style="text-align:center">Student Employment Agency
                </td>
                <td style="text-align:center">Member
                </td>
            </tr>
        </table>
        @if(isset($params['attachments']) && strlen($params['attachments'])>0)
            <pagebreak></pagebreak>
            <div>{!! $params['attachments'] !!}</div>
            <p>Budapest,<span id="nap">{{date('d',strtotime($date))}}</span> day. <span id="honap">{{date('m',strtotime($date))}}</span> month  <span id="ev"> {{date('Y',strtotime($date))}}</span> year  </p>

            <table style="width:100%" border="0">
                <tr>
                    <td style="text-align:center">........................................................
                    </td>
                    <td style="text-align:center">........................................................
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center">Student Employment Agency
                    </td>
                    <td style="text-align:center">Member
                    </td>
                </tr>
            </table>
        @endif

    </div>
</div>
