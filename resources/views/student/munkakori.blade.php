<div id="munkakorPage">
    <div class="paper" id="munkakori" style="width: 210mm; margin: 10pt auto;">
        <style>
            .paper div, .paper span,p
            {
                font-size:13px;
            }
            .paper table tr td
            {
                margin: 0px;
                padding: 1px;
                vertical-align: top;
                font-size:13px;
                line-height: normal;
            }
            .paper{
                background: #ffffff;
                font-size: 13px;
                text-align: justify;
                padding: 30pt 30pt 30pt 30pt;
                font-size:15px;
            }
            .parag, .para10 {
                text-indent: 30pt;
                text-align: justify;
                line-height: initial;
                font-size:13px;
            }
        </style>

        <p><b>Név: </b><span id="diak_nev">{{$student->name}} </span></p>
        <p><b>Adóazonosító jel: </b><span id="diak_ado">{{$student->tax_number}}</span></p>
        <p><b>Ideiglenes lakcím: </b><span id="diak_ideig">{{$student->notification_address}} </span></p>
        <p><b>Állandó lakcím: </b><span id="diak_allando">{{$student->address()}} </span></p>
        <p><b>Munkakör: </b><span id="diak_munkakor">{{$params['job']}}</span></p>
        <p><b>A munkavállaló által vállalt feladatok köre: </b><span id="diak_munkakor_munkaltato">{{$params['task']}}</span></p>
        <p><b>A munkáltató által vállalt feladatok jellemzői: </b><span id="diak_jellemzo">{{$params['task_details']}}</span></p>
        <p><b>Megrendelő/harmadik szemely: </b><span id="diak_megrendelo">{{$params['company']}}</span></p>
        <p><b>Munkavégzés helye: </b><span id="diak_hely">{{$params['work_location']}}</span></p>
        <p><b>Munkaidő: </b><span id="diak_ido">{{$params['work_time']}}</span></p>
        <p><b>Személyi alapbér: </b><span id="diak_alapber">{{$params['gross']}}</span></p>
        <p><b>Bérfizetés napja: </b><span id="diak_bernap">{{$params['day']}}</span></p>
        <p><b>A munkáltatói utasítást adó személy: </b><span id="diak_utasito">{{$params['delegate']}}</span></p>
        <p><b>Irányadó munkarend: </b><span id="diak_munkarend">{{$params['schedule']}}</span></p>
        <p><b>További szükségesnek tartott kérdés: </b><span id="diak_kerdes">{{$params['question']}}</span></p>
        <br>
        <p><span>Abban az időszakban, amikor a Munkavállaló munkavégzési kötelessége szünetel a Felek kapcsolattartására e-mailben vagy telefonon történik.
								/ Telefon: +36 1 951 0754, Email: mads@mads.hu/
								Jelen Munkaköri leírás a felek jóváhagyó aláírásával a korábban megkötött általános munkaszerződés kiegészítésévé válik és annak elválaszthatatlan részét képezi.
							  </span></p>

        <br>
        <p>Budapest, <span id="ev"> {{date('Y')}}</span> év <span id="honap">{{date('m')}}</span> hónap <span id="nap">{{date('d')}}</span> nap.</p>
        <br>
        <br>
        <table style="width:100%" border="0">
            <tr>
                <td style="text-align:center">........................................................
                </td>
                <td style="text-align:center">........................................................
                </td>
            </tr>
            <tr>
                <td style="text-align:center">Szövetkezet/Munkáltató
                </td>
                <td style="text-align:center">Tag/Munkavállaló
                </td>
            </tr>
        </table>
    </div>
</div>
