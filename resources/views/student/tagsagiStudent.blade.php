 <table class="data" style="width: 100%; margin:0 0 0 0; font-size:15px; border:solid 1px #ccccc6">
        <tbody>
        <tr>
            <td><span style="font-size:10px" class="silver" >Név:</span></td>
            <td colspan="1" style="width:36%;font-size:10px" >{{$student->name}}</td>
            <td><span style="font-size:10px" class="silver" >MADS azonosító:</span></td>
            <td colspan="1" style="width:36%;font-size:10px" >{{$student->student_number}}</td>

        </tr>
        <tr>
            <td ><span style="font-size:10px" class="silver">Anyja leánykori neve:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->mothers_name}}</span></td>
            <td><span style="font-size:10px" class="silver">Adóazonosó jele:</span></td>
            <td style="width:31%;font-size:10px"><span style="font-size:10px" class="silver">{{$student->tax_number}}</span> </td>

        </tr>
        <tr>
            <td><span style="font-size:10px"  class="silver">Születési helye:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->birth_location}}</span></td>
            <td><span style="font-size:10px" class="silver">Születési ideje:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->birth_date}}</span></td>
        </tr>
        <tr>
            <td><span class="bold" style="width:12%;font-size:10px">Bankszámlaszáma:</span></td>
            <td colspan="3"  width="33%"><span style="font-size:10px" class="silver">{{$student->bank_account_number}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">TAJ. száma:</span></td>
            <td><span  style="font-size:10px" class="silver">{{$student->taj}}</span></td>
            <td><span style="font-size:10px" class="silver">Diákigazolvány száma:</span></td>
            <td><span  style="font-size:10px" class="silver">{{optional($student->card()->orderBy('temporary_student_card_expire','desc')->first())->student_card}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Állandó lakcíme:</span></td>
            <td colspan="3"><span  style="font-size:10px" class="silver">{{ $student->address()  }}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Ideiglenes lakcíme:</span></td>
            <td colspan="3" ><span style="font-size:10px" class="silver">{{$student->notification_address}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Tel:</span></td>
            <td ><span style="font-size:10px" class="silver">{{$student->phone}}</span></td>
            <td><span style="font-size:10px" class="silver">Személyi ig. száma:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->identity_card}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">E-mail:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->email}}</span></td>
            <td><span style="font-size:10px" class="silver">Útlevél száma(külföldieknél):</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->passport}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="bold">Oktatási intézmény neve:</span></td>
            <td colspan="3" class="silver" style="font-size:10px">{{optional($student->school()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->name}}</td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Oktatási intézmény címe:</span></td>
            <td colspan="3" class="silver" style="font-size:10px">{{optional($student->school()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->address}}</td>
        </tr>
        <tr>
            <td colspan="2"><span style="font-size:10px" class="bold">{{ ((optional($student->schools()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->education_type)==1)?'Nappali':'25 éven aluli passzív nappali' }}</span></td>

            <td><span style="font-size:10px" class="silver">Hallgatói jogviszony kezdete:</span></td>
            <td><span style="font-size:10px" class="silver">{{optional($student->certificate()->orderBy('end','desc')->first())->start}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Állampolgársága:</span></td>
            <td><span style="font-size:10px" class="silver">{{optional($student->nationalities)->name}}</span></td>
            <td><span style="font-size:10px" class="silver">Megváltozott munkaképpességű</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->ability_to_work==1 ?'Igen':'Nem'}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Egyedi MADS azonosítója:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->student_number}}</span></td>
            <td></td>
            <td></td>

        </tr>
        </tbody>
    </table>
