﻿
<div class="paper" id="belepesi" style="width: 210mm; height: 560mm; margin: 10pt auto 10pt auto;font-size:15px;">
    <style>
        .paper table tr td
        {
            margin: 0px;
            padding: 1px;
            vertical-align: top;
            font-family: Arial;
            font-size: 11px;
            line-height: normal;
        }
        .parag, .para10 {
            text-indent: 40pt;
            text-align: justify;
            line-height: initial;
            font-family: arial;
        }
        .paper div, .paper span
        {
            font-size:12px;
        }
        .paper table.data td{
            border:solid 1px #ccccc6;
            white-space: nowrap;
        }

    </style>
    <p class="parag" style="font-size:11px">
        On the establishment of a membership-based economic cooperation between the Student Employment Agency and a member thereof, aimed at the personal participation of the Member, concluded by and between student employment agency named MADS - Márton Áron Szolgáltató Iskolaszövetkezet, /registered office: H-1092 Budapest, Erkel utca 3. földszint, Tax registration number: 11931335-2-43., represented by: Lator Sándor, chairman of the Board of Directors, individually, Virág Viktor, member of the Board of Directors, individually, and Szántó Zsolt Sándor, individually, hereinafter referred to as “Student Employment Agency” or “Agency”/ on the one hand, and on the other hand:
    </p>

    {!! view('student.tagsagiStudentEn',['student'=>$student,'params'=>$params]) !!}



    <p class="parag" style="font-size:11px;">
        -	hereinafter referred to as: “Member”, on this day and subject to the following terms and conditions:
        1.	The Contracting Parties confirm that Paragraph (1) Section 10/A of Act X of 2006 on Cooperatives /hereinafter referred to as “Act on Cooperatives”/ sets forth that the economic cooperation between the student employment agency and full-time students, as well as the method of the member’s participation, shall be laid down in a membership agreement by the rules of the articles of association, while Paragraph (1) Section 10/B of the Act on Cooperatives sets forth that the full-time student member of the student employment agency may also perform his/her participation as a part of the services provided by the student employment agency to a third party /hereinafter referred to as “External Services”/.
       <br>
        2.	By signing the present document, the Member declares that he/she wishes to join the Student Employment Agency as a member, and that he/she has read the terms of the Articles of Association of the Student Employment Agency and agrees to be obligatory. By signing the present document, the Member also declares that he/she wishes to take part in the fulfilment of the Student Employment Agency’s tasks via his/her participation. The Member declares that he/she shall be liable for any debts of the Student Employment Agency up to his/her financial contribution.
        <br>
        3.	By signing the present document, the Member acknowledges that simultaneously to becoming a Member, he/she shall make a financial contribution of HUF 2000, say two thousand Hungarian Forints and at the same time undertake an irrevocable obligation to fulfil this financial contribution /share/. The Member acknowledges that the Student Employment Agency will deduct this amount from the first fee that is due to him/her. The Member acknowledges that he/she shall fulfil the full financial contribution within 1 /one/ year from the signing of this Membership Agreement. The Parties further acknowledge that in the event the member status of the Member terminates, the Parties may and shall settle accounts with each other within one month.
        <br>
        4.	By signing the present document, the Member represents that the information provided by him/her is true and correct, and accepts the Membership Agreement as obligatory.
        <br>
        5.	During the Member's membership with MADS - Márton Áron Szolgáltató Iskolaszövetkezet, the Student Employment Agency shall assign the Member to perform tasks as a part of External Services at the Agency's clients (service recipients), at varying locations, according to varying remuneration and time schedules, the framework of which is set out in this Membership Agreement.
        <br>
        6.	By signing the present document, the Student Employment Agency agrees and represents that Members under the age of eighteen may be involved in the business activity of the Student Employment Agency via personal participation by the provisions of the Labor Code about the labor protection of young workers.
        <br>
        7.	The Parties confirm that representations of a Member under the age of eighteen shall only be valid together with the written consent of the Member’s legal representative, which consent form is incorporated herein by reference as an annex where necessary.
        <br>
        8.	Before the commencement of the Member’s performance of his/her participation as a part of External Services, the Parties shall conclude an individual agreement, which shall constitute the amendment hereof with the Parties’ signature. In the individual agreement, the Parties lay down the exact scope of the tasks agreed by the Member, the recipient of the services, the remuneration of the Member, the date of payment of the remuneration, the place of performance of the tasks as well as the duration of the performance of the task.
        <br>
        9.	The Member undertakes to submit to the Student Employment Agency at the latest before the commencement of the performance of the task in the framework of External Services his/her certificate of school attendance/certificate of student status for the relevant semester and present his/her student certificate with a valid vignette. The Member acknowledges that upon the termination of his/her student status, his/her membership with the Student Employment Agency will also terminate.
        <br>
        10.	For the performance of each task, the Member shall be entitled to the fee specified in the individual agreement, the amount of which, in the case of tasks not remunerated on a pro-rata basis, shall reach the threshold value set out in the legislation issued based on the authorization under Section 153 of the Labor Code. The fee payable to the Member will be paid at the scheduled payment date of the month following the month during which the task is performed pro rata to the time requirements of the task as certified by the service recipient, via bank transfer to the bank account provided by the Member, or, in cases requiring special consideration, and after agreeing on date in advance, in cash.
        <br>
        11.	This Agreement is concluded for a fixed term of 5 /five/years. The Member and the Agency agree that they will suspend this Agreement with mutual consent if the Clients of the Agency do not require the Member to perform any tasks or if the Member does not accept any individual orders from the Agency. In this case, the Member shall receive a fee pro-rata to the time required to perform the tasks performed, and Members without student status shall be entitled to leave as per Act I of 2012 on the Labor Code /hereinafter: “Labor Code”/.
        <br>
        12.	Members without student status shall be entitled to one day's leave for every thirteen days spent in the performance of their tasks by Paragraph (5) Section 10/B of the Act on Cooperatives. The Parties confirm that financial compensation for any unused leave may only be paid after the membership has been terminated.
        <br>
        13.	The Student Employment Agency agrees to provide by Paragraph (3) Section 10/B of the Act on Cooperatives to Members a break of twenty minutes per day if the duration of the performance of his/her tasks exceeds six hours per day, and an additional break of twenty-five minutes per day if the duration of the performance of his/her tasks exceeds nine hours per day, and a rest period of at least eleven hours between the end of the day's work and the beginning of the next day's work if the Member performs his/her tasks on two consecutive days.

        <br>
        14.	The Parties agree that Members under eighteen years of age may only perform tasks for eight hours per day, amounting to a total of forty hours a week. The times of tasks performed for third parties ordering several services shall be added together. The Student Employment Agency agrees not to require a Member under the age of eighteen to perform tasks between 10.00 PM and 6.00 AM, not to schedule the two days of rest due to such member unequally, and to ensure a break of thirty minutes after four and a half hours, a break of forty-five minutes after six hours, and at least twelve hours between the end of the day’s work and the beginning of the next day’s work if the Member performs his/her tasks on two consecutive days.
        <br>
        15.	The Member is required to perform his/her tasks at the recipient of each service in the shifts and according to the working hours specified in the individual agreements, in line with the instructions provided by the Student Employment Agency, to which the recipient of the service shall provide professional, technical, practical, and intellectual assistance. The contracting parties agree that Members under the age of eighteen may only receive instructions from persons employed by the Student Employment Agency. In cases beyond the scope of the foregoing, and if the instruction is necessary to prevent or avert an accident, natural disaster, severe damage, or an imminent and grave threat to life, health, or physical integrity, the representative of the service recipient may also give instructions to the Member.
        <br>
        16.	The Member is required to keep an attendance sheet of the duration of his/her performance and shall have it certified by the recipient of the service and deliver it to the Student Employment Agency on or before the 3rd day of the month following the month during which the task is performed. If the Member fails to fulfil his/her abovementioned obligation in due time, he/she acknowledges that the Student Employment Agency is entitled to pay his/her monthly fee on the payment date of the month following the month during which the attendance sheet has been delivered.
        <br>
        17.	By signing the present document, the Member represents that he/she has attended basic Health and Safety training. The Member confirms that he/she is familiar with all occupational health and safety and fire protection rules and agrees to always adhere to them by signing this Agreement. The Member acknowledges that in the event the task is performed in the framework of an external service, the Occupational Health and Safety training will be organized by the recipient of the service, and upon the signing of this Agreement, the Member agrees to always comply with such rules.
        <br>
        18.	The Member agrees that he/she will not leave the place of performance during his/her performance at his/her discretion. If the representative of the service recipient instructs the Member to do so for any reason whatsoever, the Member is required to notify the Student Employment Agency. The Member is required to perform the agreed tasks during the specified duration accurately, to the best of his abilities and qualifications, and agrees to arrive at the place of performance on time, without delay. The Member agrees not to be absent from the place of performance of the task without justification. Unjustified absence shall include completion of the task before the termination of the individual agreement due to the Member’s fault. Justified absence shall include absence certified by the managers of the Student Employment Agency and in justified cases absence certified by a medical practitioner. The Member undertakes to inform the Student Employment Agency at the latest twenty-four hours in advance if he/she is unable to perform the agreed tasks at the agreed date.
        <br>
        19.	In the event a Member involved in the provision of an external service causes damage or infringes personal rights of a third party, the civil liability rules for damage caused by employees shall apply, as agreed between the Student Employment Agency and the recipient of the service.
        <br>
        20.	While performing tasks for the recipient of the service, the Student Employment Agency and the recipient of the service shall be held liable jointly and severally for any damages caused to the Member or for the infringement of his/her rights.
        <br>
        21.	The Member is required to correct any tasks performed faultily by him/her free of charge. If the Member fails to do so, he/she shall acknowledge that the cost of correction will be deducted from his/her free. If the Member fails to handle the tools, products, and assets entrusted to him/her with due care if he/she damages such items, or if he/she fails to return to the Student Employment Agency any items subject to a return obligation by the deadline specified by the Student Employment Agency, he/she shall be held liable under civil and criminal law.
        <br>
        22.	By signing this document, the Member acknowledges if the Member fails to start the tasks at the time and place agreed by The Member and does not notify the Student Employment Agency or the designated representative of the Student Employment Agency cooperative of the absence within at least 24 /twenty-four/ hours before the scheduled start of the task, the Student Employment Agency shall be paid 30% of the assignment fee for the day of the assignment, but not less than 5. 000, - HUF per unannounced absence, say five thousand Hungarian Forints per unannounced absence. The Member expressly acknowledges and accepts this material clause.
        <br>
        23.	The Student Employment Agency considers the Member’s unjustified absence as well as repeated negligent or improper performance in quality or quantity a cause for termination for breach.
        <br>
        24.	During the term of this Agreement, the Member may only perform tasks for the Clients of the Student Employment Agency in the framework of external service provided by the Student Employment Agency, as a member of the Student Employment Agency. If the Member fails to comply with this obligation, the Member agrees to quit the Agency immediately, with all consequences thereof.
        <br>
        25.	The Member shall compensate the damage caused by the breach of his/her obligations arising from his/her membership if the Member has not acted in a way that could normally be expected in the circumstances.
        <br>
        26.	The Member shall be bound by a confidentiality obligation in respect of classified or other confidential or non-public confidential data and information, as well as personal data and information, which come to his/her knowledge in the course of the performance of his/her tasks, by the statutory provisions applicable to such data and information, and shall be liable to pay full compensation to the Student Employment Agency and the recipients of the external service in the event of a breach of this obligation. The Member undertakes to handle and transfer personal data obtained while providing the external service solely to perform his/her tasks, not to use it for any other purpose, not to disclose it to unauthorized persons or to share it with them, not to allow unauthorized access to personal data, and not to disclose personal data to the public. The Member acknowledges that this confidentiality obligation shall survive the termination of his/her contractual working relationship.
        <br>
        27.	The Student Employment Agency hereby informs the Member that it will only process his/her data about the performance of his/her membership, as well as establish a contractual working relationship by Act CXII of 2011 and the provisions of the GDPR.
    <br>
        28.	Upon the signing hereof, the Member represents that he/she is familiar with the content of the Student Employment Agency’s Privacy Policy.
    <br>
        29.	The Student Employment Agency informs the Member that he/she may contact the Student Employment Agency's data protection officer with any questions concerning the data protection rules and regulations applicable to the performance of the tasks and the processing of the Member's data.
    <br>
        30.	By signing this Agreement, the Member expressly gives his/her consent to the transfer of his/her data – personal data necessary for his/her identification, data necessary for contacting him/her, and any additional personal data necessary to assess his/her suitability for the advertised position – to the Student Employment Agency's clients, as external service recipients, for which the Member has indicated his/her intention to apply for a job offered by such external service recipient, in the knowledge of the external service recipient's identity and information on data processing.
    <br>
        31.	The parties agree that during the period where no tasks are performed by the Member, they shall contact each other by email or telephone, primarily through the contact person designated by the Student Employment Agency in the individual agreement or, in the absence of such contact person, through any of the contact details indicated on the Student Employment Agency's website (www.mads.hu).
        <br>
        32.	Matters not regulated herein shall be governed by the relevant provisions of the Civil Code and the Act on Cooperatives, as well as the provisions of the Labor Code if specifically, so stipulated by the Act on Cooperatives.
        <br>
        33.	This Agreement has been made in two (2) counterparts, of which each Party shall receive one.
    </p >







    <p>&nbsp;</p>
    {!! view('student.belepesiFooterEn',['student'=>$student,'params'=>$params]) !!}

</div>
