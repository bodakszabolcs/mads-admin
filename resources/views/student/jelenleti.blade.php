﻿
<div class="paper" id="belepesi" style="width: 210mm; height: 337mm; font-size:15px;">
    <style>
        .paper table tr td
        {
            margin: 0px;
            padding: 1px;
            vertical-align: top;
            font-family: Arial;
            font-size: 12px;
            line-height: normal;
        }
        .paper{


            font-family: arial;
            font-size: 12px;
            text-align: justify;

        }
        .parag, .para10 {
            text-indent: 30pt;
            text-align: justify;
            line-height: initial;
            font-family: arial;
        }
        .paper div, .paper span
        {
            font-size:12px;
        }
        .paper table.data td{
            border:solid 1px #c7a97a;
            white-space: nowrap;
        }

    </style>
    <table style="width:100%;border-color: #c7a97a">
        <tr>
            <th style="text-align: right;vertical-align: top;font-size: 20px;width:600px">

                <b>Jelenléti ív</b><br>
                <span style="font-size: 14px"> {{$projectManager->name}}<br> <a href="tel:{{$projectManager->phone}}">{{$projectManager->phone}}</a><br> <a href="mailto:{{$projectManager->email}}">{{$projectManager->email}}</a></span>
            </th>


        </tr>
    </table>
    <table class="data"  cellspacing="0" cellpadding="0" style="width: 100%; margin:0 0 0 0; font-size:12px; border:solid 1px #c7a97a;">
        <tbody>
        <tr>
            <td style=" padding: 4px;"><span style="font-size:12px" class="bold" >Név:</span></td>
            <td style=" padding: 4px;" ><span class="silver"><b>{{$student->name}}</b></span></td>
            <td style=" padding: 4px;"><span style="font-size:12px" class="bold">Egyedi azonosító:</span></td>
            <td  style="width:31%;font-size:12px;padding: 4px;"><span class="silver"><b>{{'MADS'. str_pad($student->id.'',6,'0',STR_PAD_LEFT)}}</b></span> </td>
        </tr>
        <tr>
            <td style=" padding: 4px;" ><span style="font-size:12px" class="bold">Munkahely:</span></td>
            <td style=" padding: 4px;" ><span style="font-size:12px" class="silver"><b>{{$project}}</b></span></td>
            <td style=" padding: 4px;"><span style="font-size:12px" class="bold">Év-Hónap:</span></td>
            <td style=" padding: 4px;"><span  style="font-size:12px"class="silver"><b>{{$month}}</b></span></td>
        </tr>
        </tbody>
    </table>
    <p class="parag" style="font-size:9px">
        Alulírott tudomásul veszem, hogyha nem a valóságnak megfelelő adatokat írok a jelenléti ívre, azzal
        érvénytelenítem azt. Alulírott kijelentem, hogy a munkavédelmi oktatáson részt vettem.
        A szövetkezetbe való munkavégzés feltétele egy 2000 forintos részjegy a jegyzése (amit az első
        munkabéredből vonunk le), a 16 életév betöltése, belépési nyilatkozat, általános munkaszerződés,
        munkaismertető, illetve a mindenkor hatályos (értsd: adott félévi) hallgatói jogviszony igazolás
        (iskolalátogatási) megléte.
        A fiatalkorúak (18. év alattiak) törvényes képviselőjük (általában szülő) írásbeli hozzájárulásával
        vállalhatnak munkát. A 15. évet betöltött személyek is vállalhatnak munkát nyári szünetben, azonban itt
        is feltétel a törvényes képviselő hozzájárulása. A béredet csak abban az esetben tudjuk kifizetni, ha a
        hallgatói jogviszony igazolásodat ügyfélszolgálatunkra (1092 Budapest Erkel u 3) személyesen
        behozod. További feltétel az adóazonosító megléte (illetékes adóhivatalnál igényelhető), Taj kártya,
        érvényes diákigazolvány.
        A munkavégzést az adott munkahelyen rögzített jelenléti ív alapján ellenőrizzük, ami szintén feltétele a
        bérfizetésnek. A jelenléti íveket a kijelölt munkahelyi vezetők gyűjtik be, azt semmiképp ne vidd haza,
        illetve ne tartsd magadnál.
        Természetesen a biztonság érdekében készíthetsz róla fénymásolatot.
        Megkérünk, hogy a jelenléti ívre mindig a saját, „ eredeti neved”írd, ahogy a anyakönyvben szerepel, és
        ne a beceneved.
        Kérünk szépen, hogy z elvállalt munkát felelősségteljesen végezd.
        A munkahelyeden megfelelő állapotban jelenj meg (megfelelő öltözet, megfelelő tudatállapot).
        Ha egy munkát elvállaltál, de tudod, hogy nem mész el, azt legalább egy nappal előtte jelezd felénk,
        hogy még időben gondoskodjunk a helyettesítésedről. Amennyiben nem szólsz időben, ami legalább a
        munka megkezdése előtt 24 óra, jogunk van az elvállalt napnak megfelelő munkabéred 30%-át
        visszatartani, továbbá a szövetkezetnek okozott kárért felelősséggel tartozol a Polgári Törvénykönyv
        rendelkezése szerint.
        Ha nem vagy megelégedve velünk, akkor elsősorban felénk jelezd és ne a munkatársaid felé, mert ők
        nem tudnak változtatni a hibákon, csak mi, és hidd el, hogy megpróbáljuk a legjobban végezni a
        dolgunkat, mert érted (és a többiekért) vagyunk
    </p>
    <table class="data" cellspacing="0" cellpadding="1"  style="width: 100%; margin:0 0 0 0; font-size:12px; border:solid 1px #ccccc6;">
        <tbody>
        <tr>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Dátum</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Feladatkör</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Érkezés</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Távozás</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Szünet</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Óraszám</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Munkavállaló<br> aláírása</span></td>
            <td style=" padding: 2px;"><span style="font-size:12px" class="bold" >Munkáltató<br> aláírása</span></td>
        </tr>

        @foreach($presents as $k=>$p)
        <tr>
            <td style=" padding: 2px;">{{\App\Helpers\ConstansHelper::formatDay($k)}}</td>
            <td style=" padding: 2px;">{{$task}}</td>
            <td style=" padding: 2px;">{{$p['start']}}</td>
            <td style=" padding: 2px;">{{$p['end']}}</td>
            <td style=" padding: 2px;">@if($p['break_from']){{$p['break_from']}}-{{$p['break_to']}}@endif</td>
            <td style=" padding: 2px;">{{$p['hour']>0?$p['hour']:''}}</td>
            <td style=" padding: 2px;"></td>
            <td style=" padding: 2px;"></td>
        </tr>
        @endforeach
        </tbody>
    </table>


</div>

