<?php
$date = date('Y-m-d');
if(isset($params['document_date']) && !empty($params['document_date'])  && $params['document_date'] != 'undefined'){
$date = date('Y-m-d',strtotime($params['document_date']));
}
if(isset($membership)){
$date = date('Y-m-d',strtotime(isset($membership)));
}
?>
<table style="width: 100%; margin: 0 0 0 0;font-size: 12px;">
    <tbody>
    <tr>
        <td align="left">Dated: </td>
        <td style="width:61%">{{date('Y-m-d',strtotime($date))}}</td>
        <td></td>

        <td>.........................................................................</td>

    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td style="text-align: center">{{$student->name}}</td>

    </tr>
    </tbody>
</table>
<div class="bordr" style="font-size:12px; margin: 14pt auto 0 auto;border:solid 1px silver;  padding:10px;padding-right:10px;line-height:normal">
					<span><span id="tag_ala_b2">{{$student->name}}</span> .....................................................................................There are no legal or other obstacles to the Member’s admission, and the Board of Directors of MADS - Márton Áron Szolgáltató Iskolaszövetkezet admitted him/her as a member of the Student Employment Agency at its meeting held at  .................................

					</span>
    <table style="width: 100%; margin: 6pt 0 0 0;font-size:12px">
        <tbody>
        <tr>
            <td align="right">Budapest, </td>
            <td style="width:33%">.....................................</td>
            <td></td>
            <td>...............................................................................................</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: center"><span style="margin-left:100px">Member of the Board

</span></td>

        </tr>
        </tbody>
    </table>
</div>
<div class="bordr" style="border:solid 1px silver;margin-top:10px; padding:10px;line-height:normafont-size:12pxl">
    <table style="width: 100%; margin: 10pt 0 10pt 0;font-size:12px">
        <tbody>
        <tr>
            <td align="left" style="font-size: 12pt;">Clause</td>
            <td align="right" style="font-size: 12pt;">!to be filled out upon quitting!</td>
        </tr>
        </tbody>
    </table>
    <div style="font-size:12px">Termination of membership:
        The amount of my financial contribution (share) has been returned to me, and I have no claims against the Student Employment Agency.
    </div>
    <table style="width: 100%; margin: 16pt 0 0 0;font-size:12px">
        <tbody>
        <tr>
            <td align="right">Budapest, </td>
            <td style="width:33%">.....................................</td>
            <td></td>
            <td>...............................................................................................</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td style="text-align: center"><span style="margin-left:100px">{{$student->name}}</span></td>

        </tr>
        </tbody>
    </table>
</div>
