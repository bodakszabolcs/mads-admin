<div id="esetiPage">

    <div class="paper" id="eseti" >
        <style>
            .paper div, .paper span, .paper p
            {
                font-size:11px;
            }
            .paper table tr td
            {
                margin: 0px;
                padding: 1px;
                vertical-align: top;
                font-size:15px;
                line-height: normal;
            }
            .paper{
                background: #ffffff;
                font-size: 11px;
                text-align: justify;
                font-size:15px;
            }
            .parag, .para10 {
                text-indent: 30pt;
                text-align: justify;
                line-height: initial;
                font-size:15px;
            }
        </style>
        <p>A MADS-Márton Áron Szolgáltató Iskolaszövetkezet /székhelye: 1092 Budapest, Erkel utca 3. földszint, adószáma: 11931335-2-43., képviseli: Lator Sándor, az igazgatóság elnöke, önállóan, - továbbiakban Szövetkezet vagy Iskolaszövetkezet -/ másrészről:</p>
        <p><b>Név: </b><span id="diak_nev">{{$student->name}} </span></p>
        <p><b>MADS azonosító: </b><span id="diak_ado">{{$student->student_number}}</span></p>
        <p><b>Anyja születéskori neve: </b><span id="diak_ideig">{{$student->mothers_name}} </span></p>
        <p><b>Születési hely, idő: </b><span id="diak_allando">{{$student->birth_location}}, {{$student->birth_date}} </span></p>
        <p>/a továbbiakban: Tag/ között létrejött Tagsági Megállapodás kiegészítéseként az Iskolaszövetkezet a Tagot külső szolgáltatás keretében történő feladatteljesítéssel bízza meg, a mai napon az alábbi feltételekkel:</p>
        <p><b>Ellátandó feladatkör: </b><span class="diak_munkakor">{{$params['job']}}</span></p>

        <p><b>A Tag által vállalt feladatok köre: </b><span class="diak_munkakor_munkaltato">{{$params['task']}}</span></p>
        <p><b>A Tag általt vállalt feladatok jellemzői: </b><span class="diak_jellemzo">{{$params['task_details']}}</span></p>
        <p><b>A Szolgáltatás fogadójának személye: </b><span class="diak_megrendelo">{{$params['company']}}</span></p>
        <p><b>A feladat teljesítésének helye : </b><span class="diak_hely">{{$params['work_location']}}</span></p>
        <p><b>A feladat teljesítésének kezdőnapja: </b><span class="diak_ido">{{($params['date'])?date('Y-m-d',strtotime($params['date'])):'' }}</span></p>
        <p><b>A feladat teljesítésének tartama: </b><span class="diak_ido_veg">{{$params['work_time']}}</span></p>
        <p><b>Napi időbeosztás: </b><span class="diak_munkarend">{{$params['schedule']}}</span></p>
        <p><b>Megbízási díj: </b><span class="diak_alapber">{{$params['gross']}}</span></p>
        <p><b>A díjfizetés napja: </b><span class="diak_bernap">{{$params['day']}}</span></p>
        <p><b>Az iskolaszövetkezet által kijelölt kapcsolattartó/manager: </b><span class="diak_kapcsolat">{{$params['contact']}}</span></p>
        <p><b>Kapcsolattartó elérhetősége: </b><span class="diak_kapcsolat_elerhetoseg">{{$params['contact_data']}}</span></p>
        <p><b>A szolgáltatás fogadója által kijelölt kapcsolattartó : </b><span class="diak_utasito">{{$params['delegate']}}</span></p>
        <p><b>A tag külső szolgáltatás fogadója részére továbbításra kerülő személyes adatai: név, anyja születési neve; születési hely és idő; telefonszám; e-mail cím  </b></p>
        <p><b>Adattovábbitás célja: 	 jelen tagsági megállapodás teljesítése, azonosítás, kapcsolattartás elősegítése  </b></p>
        <p><b>A szolgáltatás fogadója általi adatkezelésre vonatkozó tájékoztató elérhetősége, vagy az adatkezelésre vonatkozó tájékoztatás módja / körülményei </b><span class="tev_aszf">{{isset($params['gdpr'])?$params['gdpr']:''}}</span></p>
        <p><b>További szükségesnek tartott kérdés: </b><span class="diak_kerdes">{{$params['question']}}</span></p>

        <p><b>A Tag jelen eseti megállapodás aláírásával a benne foglat feladatok ellátására, a fenti feltételek elfogadásával kötelezettséget vállal..
            </b></p>
        <p>A Tag nyilatkozza, hogy a külső szolgáltatás fogadójának adatkezelési tevékenységére vonatkozó tájékoztatást megismerte, a személyes adatainak a külső szolgáltatás fogadója részére a rögzített célból történő továbbításához kifejezett és önkéntes hozzájárulását adja.</p>

        <?php
         $date = date('Y-m-d');
         if(isset($params['document_date']) && !empty($params['document_date'])  && $params['document_date'] != 'undefined'){
             $date = date('Y-m-d',strtotime($params['document_date']));
         }
        ?>
        <p>Budapest, <span id="ev"> {{date('Y',strtotime($date))}}</span> év <span id="honap">{{date('m',strtotime($date))}}</span> hónap <span id="nap">{{date('d',strtotime($date))}}</span> nap.</p>
        <br>

        <table style="width:100%" border="0">
            <tr>
                <td style="text-align:center">........................................................
                </td>
                <td style="text-align:center">........................................................
                </td>
            </tr>
            <tr>
                <td style="text-align:center">Iskolaszövetkezet
                </td>
                <td style="text-align:center">Tag
                </td>
            </tr>
        </table>
        @if(isset($params['attachments']) && strlen($params['attachments'])>0)
            <pagebreak></pagebreak>
            <div>{!! $params['attachments'] !!}</div>
            <p>Budapest, <span id="ev"> {{date('Y',strtotime($date))}}</span> év <span id="honap">{{date('m',strtotime($date))}}</span> hónap <span id="nap">{{date('d',strtotime($date))}}</span> nap.</p>
            <br>

            <table style="width:100%" border="0">
                <tr>
                    <td style="text-align:center">........................................................
                    </td>
                    <td style="text-align:center">........................................................
                    </td>
                </tr>
                <tr>
                    <td style="text-align:center">Iskolaszövetkezet
                    </td>
                    <td style="text-align:center">Tag
                    </td>
                </tr>
            </table>
        @endif

    </div>
</div>
