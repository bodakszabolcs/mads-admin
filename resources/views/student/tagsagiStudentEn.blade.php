 <table class="data" style="width: 100%; margin:0 0 0 0; font-size:15px; border:solid 1px #ccccc6">
        <tbody>
        <tr>
            <td><span style="font-size:10px" class="silver" >Name:</span></td>
            <td colspan="1" style="width:36%;font-size:10px" >{{$student->name}}</td>
            <td><span style="font-size:10px" class="silver" >MADS ID:</span></td>
            <td colspan="1" style="width:36%;font-size:10px" >{{$student->student_number}}</td>

        </tr>
        <tr>
            <td ><span style="font-size:10px" class="silver">Mother’s maiden name:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->mothers_name}}</span></td>
            <td><span style="font-size:10px" class="silver">Tax identification number:</span></td>
            <td style="width:31%;font-size:10px"><span style="font-size:10px" class="silver">{{$student->tax_number}}</span> </td>

        </tr>
        <tr>
            <td><span style="font-size:10px"  class="silver">Place of birth:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->birth_location}}</span></td>
            <td><span style="font-size:10px" class="silver">Date of birth:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->birth_date}}</span></td>
        </tr>
        <tr>
            <td><span class="bold" style="width:12%;font-size:10px">Bank account number:</span></td>
            <td colspan="3"  width="33%"><span style="font-size:10px" class="silver">{{$student->bank_account_number}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Social security number:</span></td>
            <td><span  style="font-size:10px" class="silver">{{$student->taj}}</span></td>
            <td><span style="font-size:10px" class="silver">Student ID number /card number:</span></td>
            <td><span  style="font-size:10px" class="silver">{{optional($student->card()->orderBy('temporary_student_card_expire','desc')->first())->student_card}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Permanent address:</span></td>
            <td colspan="3"><span  style="font-size:10px" class="silver">{{ $student->address()  }}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">ITemporary address:</span></td>
            <td colspan="3" ><span style="font-size:10px" class="silver">{{$student->notification_address}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Telephone number:</span></td>
            <td ><span style="font-size:10px" class="silver">{{$student->phone}}</span></td>
            <td><span style="font-size:10px" class="silver">Personal ID card number (for foreigners if it entitles the holder to stay in Hungary): </span></td>
            <td><span style="font-size:10px" class="silver">{{$student->identity_card}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">E-mail address:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->email}}</span></td>
            <td><span style="font-size:10px" class="silver">Passport number or number of document certifying right of residence (for foreigners):</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->passport}}</span></td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="bold">Name of the educational institution:</span></td>
            <td colspan="3" class="silver" style="font-size:10px">{{optional($student->school()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->name}}</td>
        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Address of educational institution:</span></td>
            <td colspan="3" class="silver" style="font-size:10px">{{optional($student->school()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->address}}</td>
        </tr>
        <tr>
            <td colspan="2"><span style="font-size:10px" class="bold">{{ (optional($student->school()->whereNull('student_schools.deleted_at')->orderBy('end','desc')->first())->education_type)==1?'Full-time student ':'Non full-time student' }}</span></td>

            <td><span style="font-size:10px" class="silver">Commencement of student status:</span></td>
            <td><span style="font-size:10px" class="silver">{{optional($student->certificate()->orderBy('end','desc')->first())->start}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">Nationality:</span></td>
            <td><span style="font-size:10px" class="silver">{{optional($student->nationalities)->name}}</span></td>
            <td><span style="font-size:10px" class="silver"> Person with reduced working capacity</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->ability_to_work==1 ?'Igen':'Nem'}}</span></td>

        </tr>
        <tr>
            <td><span style="font-size:10px" class="silver">MADS ID:</span></td>
            <td><span style="font-size:10px" class="silver">{{$student->student_number}}</span></td>
            <td></td>
            <td></td>

        </tr>
        </tbody>
    </table>
