﻿
<center  style="padding:8px; margin-top:12px;background-color:white;">
    <table style="font-size:13px;">
        <tr style="border-bottom:solid 1px black;">
            <td style="border-bottom:solid 2px black;text-align:left;font-size: 10px;width:200px">
                <?php
                $arr = str_split($payroll->month,4);
                ?>
                {{ $arr[0]}} <?= \Illuminate\Support\Arr::get(\App\Helpers\ConstansHelper::$moths,(int)$arr[1]); ?> havi bérelszámolási lap</td>
            <td colspan="2" style="border-bottom:solid 2px black;white-space: nowrap;width:300px">MADS - MÁRTON ÁRON ISKOLSZÖVETKEZET</td>
            <td style="border-bottom:solid 2px black;text-align:right;font-size: 10px;width:200px">Készült: <?=date('Y-m-d',strtotime($arr[0].'-'.$arr[1]."-12".'+1 month'))?></td>
        </tr>
        <tr>
            <td colspan="4" style="height:10px">
            </td>
        </tr>
        <tr style="border-bottom:solid 2px black;">
            <td style="border-bottom:solid 2px black;font-size:18px;font-weight:bold"><?= $payroll->name ?></td>
            <td colspan="2" style="border-bottom:solid 2px black;"></td>
            <td style="border-bottom:solid 2px black;"></td>
        </tr>
        <tr style="">
            <td style="text-align:left;padding-top:10px;white-space: nowrap">Adóazonosító jel: <?=$payroll->tax?> <br> Telephely:</td>
            <td colspan="2" style="text-align:center;padding-top:10px"></td>
            <td style="text-align:right;padding-top:10px"></td>
        </tr>
        <tr>
            <td colspan="4" style="height:15px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black; font-weight:bold;text-align:left ;"><b>Bérköltség</b></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right">óra &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; nap &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Összeg</td>
        </tr>
        <tr>
            <td style="text-align:left;">Havidíjas bér</td>
            <td style=""></td>
            <td style=""></td>
            <td style="text-align: right"><?=  number_format($payroll->gross,0,'',' ')?> Ft</td>
        </tr>
        <tr>
            <td colspan="4" style="height:15px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black; font-weight:bold;text-align:left"><b>Egyéni adók és járulékok</b></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right">Összeg</td>
        </tr>
        <tr>
            <td style="text-align:left;">Összevonandó jövedelem adója</td>
            <td style=""></td>
            <td style=""></td>
            <td style="text-align: right"><?= number_format($payroll->szja,0,'',' '); ?> Ft</td>
        </tr>
        <tr>
            <td colspan="4" style="height:15px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black; font-weight:bold;text-align:left"><b>Levonások</b></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right">Összeg</td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black;text-align:left;">Részjegy<br/>Hóközi kifizetés<br/><?php if($payroll->deduction): ?><?= 'Egyéb levonás' ?> <?php endif; ?></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right"><?= number_format($payroll->part_ticket,0,'',' '); ?> Ft<br/><?= number_format($payroll->monthly,0,'',' '); ?> Ft

                <?php if($payroll->deduction): ?><br/><?= number_format($payroll->deduction,0,'',' '); ?> Ft <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td style="text-align:left;">Összesen</td>
            <td style=""></td>
            <td style=""></td>
            <td style="text-align: right"><?= number_format($payroll->part_ticket+$payroll->monthly+$payroll->deduction,0,'',' '); ?> Ft</td>
        </tr>
        <tr>
            <td colspan="4" style="height:15px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black; font-weight:bold;text-align:left"><b>Kifizetés</b></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right">Összeg</td>
        </tr>
        <tr>
            <td style="text-align:left;">Kifizetendő</td>
            <td style=""></td>
            <td style=""></td>
            <td style="text-align: right"><?= number_format($payroll->payable,0,'',' '); ?> Ft</td>
        </tr>
        <tr>
            <td colspan="4" style="height:15px">
            </td>
        </tr>
        <tr>
            <td style="border-bottom:solid 1px black; font-weight:bold;text-align:left"><b>Tájékoztató adatok</b></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;"></td>
            <td style="border-bottom:solid 1px black;text-align: right">Összeg</td>
        </tr>
        <tr>
            <td style="text-align:left;">Rendszeres jövedelem</td>
            <td style=""></td>
            <td style=""></td>
            <td style="text-align: right"><?= number_format($payroll->gross,0,'',' '); ?> Ft</td>
        </tr>
        <tr>

            <td colspan=4 >

            </td>

        </tr>



    </table>
    <img width="1500" style="width:900px;max-width: 100%" src="{{config('app.app_url')}}/assets/admin/alairas.png"/>

</center>
