<WebAdverts>
    <?php
    $server = 'https://'.$_SERVER['HTTP_HOST'];
    $data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->whereNotNull('cv_online_kat_1')->get()->toArray();

    foreach($data as $row): ?>
    <?php

    $c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
    $cvOnlineCategory= \Modules\Work\Entities\Work::$cvonlineCategory;
    $head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
    $url=route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
    $url = str_replace("api/v1/work",'munkak',$url);
    $projekt =\Modules\User\Entities\User::find($row['user_id']);
    $projekt_text = $projekt->name."<br/>".
        "Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
        "Telefon: <b>".$projekt->phone."</b>";
    ?>
    <advert ID="<?=$row['id']?>">
        <company>mads@mads.hu</company>
        <title><![CDATA[<?= $row['name'] ?>]]></title>
        <description><![CDATA[<h3> <?=$row['name']?></h3><br/>Leírás: <?=$row['lead']?><br/> Feladat: <?=$row['task']?>  <br/>Óradíj: <?=$row['gross']?> ]]></description>
        <locations>
            <?php $city=\Modules\City\Entities\City::find($row['city_id']); ?>
            <location><?= str_contains(optional($city)->name,'Budapest')?'Budapest':optional($city)->name?></location>
        </locations>
        <categories>
            <category><?= $row['cv_online_kat_1']; ?></category>
            <?php if($row['cv_online_kat_2']):?>
            <category><?= $row['cv_online_kat_2'] ?></category>
            <?php endif; ?>
        </categories>
        <requirements><![CDATA[ Feltétel: <?=$row['condition']?>  ]]></requirements>
        @if($row['preference'])
            <qualities><![CDATA[ <?=$row['preference']?>  ]]></qualities>
        @endif
        @if($row['company_description'])
            <benefits><![CDATA[ <?=$row['company_description']?>  ]]></benefits>
        @endif

        <email><?= $projekt->email ?></email>
    </advert>
    <?php endforeach; ?>
    <CategoryList>

       <?php
       $index =1;
        foreach($cvOnlineCategory as $key=>$value): ?>

        <CategoryItem>
            <id><?= ($index) ?></id>
            <name><![CDATA[<?= $key ?>]]></name>
            <parent></parent>
        </CategoryItem>
        <?php foreach($value as $k=>$v): ?>
        <CategoryItem>
            <id><?= ($index).($k+1) ?></id>
            <name><![CDATA[<?= $v ?>]]></name>
            <parent><?= $index ?></parent>
        </CategoryItem>
    <?php endforeach;
           $index++;
    ?>
        <?php endforeach; ?>
    </CategoryList>
        <LocationList>
            <?php $cities=\Modules\City\Entities\City::all();
            foreach($cities as $row): ?>
            <LocationItem>
                <id><?= $row->id ?></id>
                <name><![CDATA[<?= $row->name ?>]]></name>
            </LocationItem>
            <?php endforeach; ?>
        </LocationList>
</WebAdverts>

