<rss version="2.0" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:sy="http://purl.org/rss/1.0/modules/syndication/" xmlns:admin="http://webns.net/mvcb/" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <version>3.0</version>
        <title><![CDATA[ MADS]]></title>
        <link>https://mads.hu</link>



        <?php
        $server = 'https://'.$_SERVER['HTTP_HOST'];
        $data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

        foreach($data as $row): ?>
        <?php
        $c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
        $head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
        $url=route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
        $url = str_replace("api/v1/work",'munkak',$url);
        $projekt =\Modules\User\Entities\User::find($row['user_id']);
        $projekt_text = $projekt->name."<br/>".
            "Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
            "Telefon: <b>".$projekt->phone."</b>";
        ?>

        <item>
            <id><?=$row['id']?></id>

            <title>
                <![CDATA[ <?= $row['name'] ?> ]]>
            </title>
            <short_title>
                <![CDATA[ <?= $row['lead'] ?> ]]>
            </short_title>
            <companyname><![CDATA[ Márton Áron Iskolaszövetkezet]]></companyname >
            <short_companyname><![CDATA[ MADS]]></short_companyname >
            <companyinfo><![CDATA[ Diákmunka]]></companyinfo>
            <location><![CDATA[ <?= $city= optional(\Modules\City\Entities\City::find($row['city_id']))->name; ?>]]></location>
            <tasks><![CDATA[ <?= $row['task'] ?>]]></tasks>
            <?php if($row['condition']): ?>
            <requirements><![CDATA[ <?= $row['condition'] ?>]]></requirements>
            <?php endif; ?>
            <?php if($row['preference']): ?>
            <advantages><![CDATA[ <?= $row['preference']; ?>]]></advantages>
            <?php endif; ?>
            <phone><![CDATA[ <?= $projekt->phone ?>]]></phone>
            <contact><![CDATA[  <?= $projekt->vezeteknev." ".$projekt->keresztnev."<br/>"."Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a>" ?>]]></contact>
            <?php if($row['apply']): ?>
            <registration><![CDATA[ <?= $row['apply'] ?> ]]></registration>
            <?php endif; ?>
            <clickurl><![CDATA[ <?= $url ?>]]></clickurl>
            <description><![CDATA[ <?= $row['lead'] ?> <?= $row['task'] ?>]]></description>
            <?php if($row['comment']): ?>
            <other><![CDATA[ <?= $row['comment'] ?>]]></other>
            <?php endif; ?>
            <email mailtype="jobline" email="<?= $projekt->email ?>" />
            <settings anonim="false" onlineregistration="false" />
            <dutytime><![CDATA[<?= $row['work_time']?$row['work_time']:'Minden nap' ?>]]></dutytime>
            <date start="<?= date('Y.m.d') ?>" lastmodification="<?= date('Y.m.d H:i',strtotime($row['updated_at'])) ?>" />
            <?php $price =preg_replace("/[^0-9,]/", "", $row['gross']);
            if($price > 5000)
            {
                $oraber=explode(" ",$row['gross']);
                if(isset($oraber[1]))
                {
                    $price =preg_replace("/[^0-9,]/", "", $oraber[1]);
                }
                else
                {
                    $price =preg_replace("/[^0-9,]/", "", $oraber[0]);
                }
            }
            ?>
            <salary gross="true" salary="<?= $price ?>" currency="FT" hourlyrate="<?= $price>5000?'false':'true' ?>"/>
        </item>


        <?php endforeach; ?>
    </channel>
</rss>
