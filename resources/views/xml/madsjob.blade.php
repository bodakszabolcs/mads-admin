<jobs>
<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

		foreach($data as $row):

			$c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
			$head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
			$url= route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
			$url = str_replace("api/v1/work",'munkak',$url);
			$projekt =\Modules\User\Entities\User::find($row['user_id']);
			$projekt_text = $projekt->name."<br/>".
				"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
				"Telefon: <b>".$projekt->phone."</b>";
			?>

			<job>
				<link><?=$url?></link>
				<name><![CDATA[<?=$row['name']?>]]></name>
				<?php $city=\Modules\City\Entities\City::find($row['city_id']); ?>
				<region><?= optional($city)->name ?></region>

				<solary><?=$row['gross']?></solary>
				<company>MADS Work</company>
				<company_url>https://www.mad.hu</company_url>
				<description><![CDATA[<h3> <?=$row['name']?></h3></br><?=$row['lead']?></br> Feladat: <?=$row['task']?></br>Feltétel: <?=$row['condition']?> </br> Kapcsolattartó: <?=$projekt_text?> ]]></description>
				<categories>

				<category><?= $head_category->name ?>/<?= $c->name ?></category>

				<category>Munka</category>

				</categories>
				<?php $endOfCycle = date("d.m.y", strtotime("+1 month")); ?>
				<expire><?=$endOfCycle?></expire>
				<update><?=date("d.m.y")?></update>
			</job>

	<?php endforeach; ?>
</jobs>
