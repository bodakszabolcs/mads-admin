<ads>


    <?php
    $server = 'https://'.$_SERVER['HTTP_HOST'];
    $data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->whereNotNull('jobhunter_category')->get()->toArray();

    foreach($data as $row): ?>
    <?php
    $c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
    $head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
    $url=route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
    $url = str_replace("api/v1/work",'munkak',$url);
    $projekt =\Modules\User\Entities\User::find($row['user_id']);
    $projekt_text = $projekt->name."<br/>".
        "Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
        "Telefon: <b>".$projekt->phone."</b>";

    ?>
        <ad>
            <id><?=$row['id']?></id>
            <title>
                <![CDATA[<?=$row['name']?>]]>
            </title>
            <tasks>
                <![CDATA[<?=$row['task']?>]]>
            </tasks>
            <requirements>
                <![CDATA[<?=$row['condition']?>]]>
            </requirements>
            <preferences>
                <![CDATA[<?=$row['preference']?>]]>
            </preferences>
            <offers>
                <![CDATA[<?=$row['lead']?>]]>
            </offers>
            <category><?=$row['jobhunter_category']?></category>
            <?php $city=\Modules\City\Entities\City::find($row['city_id']); ?>
            <location><?= optional($city)->name ?></location>
            <experience>-1</experience>

            <job_type>resz</job_type>
            <documents/>
            <salary><?=$row['gross']?>.00</salary>
            <contact_email><?= $projekt->email?></contact_email>
            <expires><?= date('Y-m-d',strtotime($row['updated_at'].' +7 day'))?></expires>
            <last_updated><?= date('Y-m-d H:i:s',strtotime($row['updated_at']))?></last_updated>
            <status>1</status>
            <created_at><![CDATA[<?= date('Y-m-d H:i:s',strtotime($row['created_at']))?>]]></created_at>
            <updated_at><![CDATA[<?= date('Y-m-d H:i:s',strtotime($row['updated_at']))?>]]></updated_at>

            <slug><![CDATA[<?= $url ?>]]></slug>
        </ad>


    <?php endforeach; ?>
</ads>
