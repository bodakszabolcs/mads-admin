<madsWork>


		<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

		foreach($data as $row): ?>
				<?php
				$c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
				$head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
				$url= route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
				$url = str_replace("api/v1/work",'munkak',$url);
				$projekt =\Modules\User\Entities\User::find($row['user_id']);
				$projekt_text = $projekt->name."<br/>".
					"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
					"Telefon: <b>".$projekt->phone."</b>";
			    ?>
			<work>
				<id><?=$row['id']?></id>
				<url><![CDATA[<?= $url ?>]]></url>


				<description><![CDATA[<?=$row['lead']?>]]></description>
				<category><?=$head_category->name?>/<?=$c->name?></category>
				<place><?=$row['work_location']?></place>
				<project><![CDATA[<?=$row['task']?>]]></project>
				<condition><![CDATA[<?=$row['condition']?>]]></condition>
				<advantage><![CDATA[<?=$row['preference']?>]]></advantage>
				<workingHours><![CDATA[<?=$row['work_time']?>]]></workingHours>
				<duration><![CDATA[<?=$row['work_time']?>]]></duration>
				<beginning><![CDATA[<?=$row['start']?>]]></beginning>
				<grossHourlyWage><![CDATA[<?=$row['gross']?>]]></grossHourlyWage>
				<signin><![CDATA[<?=$row['apply']?>]]></signin>

				<projektmanagger><?=$projekt_text?></projektmanagger>
				<work_time><?=$row['updated_at']?></work_time>
			</work>

	<?php endforeach; ?>
</madsWork>
