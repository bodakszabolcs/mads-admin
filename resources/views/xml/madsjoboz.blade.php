<jobs>


		<?php
	$server = 'http://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

		foreach($data as $row): ?>
				<?php
	$c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
	$head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
	$url= route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
	$url = str_replace("api/v1/work",'munkak',$url);
	$projekt =\Modules\User\Entities\User::find($row['user_id']);
	$projekt_text = $projekt->name."<br/>".
		"Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
		"Telefon: <b>".$projekt->phone."</b>";
	?>

			<job>
				<id><?=$row['id']?></id>
				<pubDate><?= $row['updated_at'] ?></pubDate>
				<?php $city=\Modules\City\Entities\City::find($row['city_id']); ?>
				<city><?= optional($city)->name?></city>
				<title><![CDATA[<?= $row['name'] ?>]]></title>
				<description><![CDATA[<h3> <?=$row['name']?></h3><br>Leírás: <?=$row['lead']?></br> Feladat: <?=$row['task']?></br>Feltétel: <?=$row['condition']?> </br> Kapcsolattartó: <?=$projekt_text?> ]]></description>
				<sal><?=$row['gross']?></sal>
				<worktime><?=$row['work_time']?></worktime>
				<category><?=$head_category->name?>/<?=$c->name?></category>
				<link><![CDATA[<?= $url ?>]]></link>
			</job>

	<?php endforeach; ?>
</jobs>
