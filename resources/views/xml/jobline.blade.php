<Advs>


		<?php
		$server = 'https://'.$_SERVER['HTTP_HOST'];
	$data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

		foreach($data as $row): ?>
				<?php

				$c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
				$head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
				$url=route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
				$url = str_replace("api/v1/work",'munkak',$url);
			$projekt =\Modules\User\Entities\User::find($row['user_id']);
			$projekt_text = $projekt->name."<br/>".
			 "Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
			 "Telefon: <b>".$projekt->phone."</b>";
			    ?>

				<Adv>
					<WebId>
						<![CDATA[ <?=$row['id']?> ]]>
					</WebId>

					<PositionName>
						 <![CDATA[ <?= $row['lead'] ?> ]]>
					</PositionName>
					<Link>
						<![CDATA[ <?= $server."/munkak"; ?> ]]>
					</Link>
					<CompanyLink>
						<![CDATA[ <?= $url ?> ]]>
					</CompanyLink>
					<PeriodEnd>
					<![CDATA[ <?= date('Y.m.d H:i:s',strtotime('+20 day')) ?> ]]>
					</PeriodEnd>
					<PeriodStart>
					<![CDATA[ <?= date('Y.m.d H:i:s') ?> ]]>
					</PeriodStart>
					<PositionShortName>
					<![CDATA[ <?= $row['name'] ?> ]]>
					</PositionShortName>
					<CompanyName>
					<![CDATA[ <?= "MADS Work Kft"; ?> ]]>
					</CompanyName>
					<CompanyShortName>
					<![CDATA[ <?= "MADS Work"; ?> ]]>
					</CompanyShortName>
					<ShortDescription>
						<![CDATA[ <?= $row['lead'] ?> <?= strip_tags($row['task']) ?> ]]>
					</ShortDescription>
				</Adv>


	<?php endforeach; ?>
</Advs>
