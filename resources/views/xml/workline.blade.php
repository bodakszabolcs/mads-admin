<Munkak>


    <?php
    $server = 'https://'.$_SERVER['HTTP_HOST'];
    $data = \Modules\Work\Entities\Work::where('is_full','=',0)->where('archive','=',0)->get()->toArray();

    foreach($data as $row): ?>
    <?php
    $c=\Modules\WorkCategory\Entities\WorkCategory::find($row['work_category_id']);
    $head_category = \Modules\WorkCategory\Entities\WorkCategory::find($c->parent_id);
    $url=route('Frontend get work by slug',[$head_category->slug,$c->slug,$row['slug']]);
    $url = str_replace("api/v1/work",'munkak',$url);
    $projekt =\Modules\User\Entities\User::find($row['user_id']);
    $projekt_text = $projekt->name."<br/>".
        "Email: <a href='mailto:".$projekt->email."'><b>".$projekt->email."</b></a><br/>".
        "Telefon: <b>".$projekt->phone."</b>";

    ?>
    <Munka ID='<?=$row['id']?>'>

        <Cim><![CDATA[<?=$row['name']?>]]></Cim>
        <Feladas><![CDATA[<?= date('Y-m-d H:i:s',strtotime($row['updated_at']))?>]]></Feladas>
        <Lejarat><![CDATA[<?= date('Y-m-d H:i:s',strtotime($row['updated_at'].' +7 day'))?>]]></Lejarat>
        <Rovidleiras><![CDATA[<?=$row['lead']?>]]></Rovidleiras>
        <Feladatok><![CDATA[<?=$row['task']?>]]></Feladatok>
        <Elvarasok><![CDATA[<?=$row['condition']?>]]></Elvarasok>
        <Amitkinalunk><![CDATA[ Bér: <?= $row['gross']?>]]></Amitkinalunk>
        <Egyebb><![CDATA[Jelentkezés: <?=$row['apply']?>]]></Egyebb>
        <Leiras><![CDATA[<?= $row['description']?>]]></Leiras>
        <Tagek><![CDATA[<?= $head_category->name ?>,<?= $c->name ?>]]></Tagek>
        <Regio><![CDATA[<?= $city= optional(\Modules\City\Entities\City::find($row['city_id']))->name; ?>)]]></Regio>
        <Telepules><![CDATA[<?=$row['work_location']?>]]></Telepules>
        <Munkaido><![CDATA[<![CDATA[<?=$row['work_time']?>]]></Munkaido>
        <Url><![CDATA[<?= $url ?>]]></Url>
        <Logo><![CDATA[https://www.mads.hu/assets/frontend/img/mads_logo.jpg]]></Logo>
        <Kapcsolattarto><![CDATA[<?= $projekt->vezeteknev." ".$projekt->keresztnev?> ]]></Kapcsolattarto>
        <Telefon><![CDATA[<?= $projekt->phone ?>]]></Telefon>
        <Email><![CDATA[<?= $projekt->email ?>]]></Email>
    </Munka>

    <?php endforeach; ?>
</Munkak>
