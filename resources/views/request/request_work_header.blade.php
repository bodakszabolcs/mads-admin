<style>
    p{
        padding:3px;
        margin:3px;

    }
    body{
        font-family: Calibri,sans-serif;
        font-size: 14px;
    }
    .blue{
        color:#e86902
    }
    .justify{
        text-align: justify;
    }
    .title{
        font-size:16px
    }
    ul.custum-list{
     list-style: none;
    }
        ul.custum-list li:before {
            content: '✓';
        }

</style>
<p class="blue title"><strong>Árajánlat munkaerő-kölcsönzési és közvetítési szolgáltatásra.</strong></p>
<p class="blue title"><strong> Munkaerő-kölcsönzési szolgáltatás díja és tartalma.</strong></p>
<p class="blue title"><strong>Költségalapú kölcsönzési díj</strong></p>
<p class="justify">A havi kölcsönzési díj két részből tevődik össze, az egyik a munkavállalók foglalkoztatásával kapcsolatos költségek összessége (továbbiakban: <strong>foglalkoztatási költség</strong>), a másik pedig a
    munkaerő-kölcsönzési szolgáltatási díj (továbbiakban: <strong>szolgáltatási díj</strong>).</p>
<p class="justify">A költségalapú kölcsönzési díj esetén a havi kölcsönzési díj összegét befolyásolja az esetlegesen
    változó foglalkoztatási költség.</p>
<ul  class="custum-list">
    <li ><strong class="blue">✓ Foglalkoztatási költség</strong></li>
</ul>
<p>Az alábbi jogcímeken felmerülő kifizetéseket tartalmazza:</p>
<ul>
    <li>Munkabér (alapbér, műszakpótlék, bérpótlékok, egyéb pótlékok stb.)</li>
    <li>Egyéb bér jellegű juttatások (teljesítménybér, jutalék, jutalom, mozgó bér, prémium, bónusz stb.)</li>
    <li>Rendkívüli munkavégzésért járó bér</li>
    <li>Szabadságra, fizetett ünnepre és egyéb a törvényben meghatározott esetekre járó távolléti díj.</li>
    <li>Szabadságmegváltás.</li>
    <li>Betegszabadságra járó járandóság, valamint a munkáltatói táppénz hozzájárulás (táppénz 1/3-a)</li>
    <li>Állásidőre járó bér.</li>
    <li>Minden egyéb a foglalkoztatással kapcsolatban a Kölcsönvevő és a Kölcsönbeadó által
        előzetesen egyeztetett kifizetés (munkába járás költsége, stb.)</li>
    <li>Minden egyéb a munkáltatót terhelő adó és járulék (foglalkoztatási költségekre eső 2%-
        os iparűzési adó)</li>
</ul>
<ul class="custum-list">
    <li><strong class="blue">✓ Szolg&aacute;ltat&aacute;si d&iacute;j</strong></li>
</ul>
<p>A havi szolgáltatási díj a Kölcsönvevőnél foglalkoztatott munkavállalónként a táblázat szerint:</p>
<ul  class="custum-list">
    <li><strong class="blue">✓ szolgáltatási szorzó</strong>.</li>
</ul>
