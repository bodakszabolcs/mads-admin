<style>
    p{
        padding:3px;
        margin:3px;

    }
    body{
        font-family: Calibri,sans-serif;
        font-size: 14px;
    }
    .blue{
        color:#e86902
    }
    .justify{
        text-align: justify;
    }
    .title{
        font-size:16px
    }
    ul.custum-list{
        list-style: none;
    }
    ul.custum-list li:before {
        content: '✓';
    }
    table {
        width: 100%;
        padding:3px;
        margin:3px;
        border-collapse:collapse; border:none;

    }
    .title-work{
        background: #fff2cc;
        padding:5px;
        font-size: 16px;
        text-align: center;
        margin-bottom: 10px;
    }
    tr th {
        font-weight: 700;

    }
    tr th, tr td {
        padding: 5px;

    }
    .bg-white td{
        background: rgba(0, 0, 0, 0.93) !important;
        color: white;
    }
    .text-center{
        text-align: center;
    }
    .small{
        font-size: 12px;
    }
    tr:nth-child(even) {background-color: #ffd966;}
    tr:nth-child(even) {background-color: #fff2cc;}

</style>
<p class="blue title"><strong>Árajánlat diákmunkaerő szervezésére, foglalkoztatására.</strong></p>
<p class="blue title-work"><strong>Munkakör: {!! nl2br($ccr['name'])!!} (FEOR: {!!nl2br($ccr['feor'])!!})</strong></p>

<table cellspacing="0">
    <tbody>
    <tr  class="bg-white">
        <td colspan="2">
            <p><strong>Elvárások:</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p> {!!nl2br($ccr['requirements'])!!}</p>
        </td>
    </tr>

    <tr  class="bg-white">
        <td colspan="2">
            <p><strong>Feladat rövid bemutatása:</strong></p>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <p> {!!nl2br($ccr['task'])!!}</p>
        </td>
    </tr>

    <tr class="bg-white">
        <td colspan="2">
            <p ><strong>Javasolt juttatási csomag:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>Javasolt bér:</strong></p>
        </td>
        <td>
            <p class="blue"> <strong>Bruttó {!!($ccr['price'])!!} Ft/óra</strong></p>
        </td>
    </tr>
    @if(isset($ccr['night']) && $ccr['night'])
        <tr>
            <td>
                <p><strong>Éjszakai pótlék ({{$ccr['night']}}):</strong></p>
            </td>
            <td>
                <p> {!!nl2br($ccr['night_text'])!!}</p>
            </td>
        </tr>
    @endif
    @if(isset($ccr['weekend']) &&$ccr['weekend'] )
        <tr>
            <td>
                <p><strong>Hétvégi pótlék ({{$ccr['weekend']}}):</strong></p>
            </td>
            <td>
                <p> {!!nl2br($ccr['weekend_text'])!!}</p>
            </td>
        </tr>
    @endif
    @if(isset($ccr['freeday']) &&$ccr['freeday'] )
        <tr>
            <td>
                <p><strong>Ünnepnapi pótlék:</strong></p>
            </td>
            <td>
                <p> {!!nl2br($ccr['freeday'])!!}</p>
            </td>
        </tr>
    @endif
    @if(isset($ccr['other']) &&$ccr['other'] )
        <tr>
            <td>
                <p><strong>Egyedi bónusz:</strong></p>
            </td>
            <td>
                <p> {!!nl2br($ccr['other'])!!}</p>
            </td>
        </tr>
    @endif
    </tbody>
</table>
<p></p>
<p ><strong>Szolgáltatási díj, mely minden költséget tartalmaz, a munkavállaló bérét is:</strong></p>
<p></p>
<table cellspacing="0">
    <tbody>
    <tr class="bg-white">
        <td>
            <p><strong>Megnevezés:</strong></p>
        </td>
        <td>
            <p><strong>Megbízási díj:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>MADS </strong> által delegált diákok esetében:</p>
        </td>
        <td class="text-center">
            <p class="text-center"><strong>Bruttó bér * <span class="blue">{{$ccr['multiplier']}}</span> + ÁFA </strong><br/>
                <span class="small">
                    ({{$ccr['price']}} Ft/óra * {{$ccr['multiplier']}} = <strong>{{(int)ceil($ccr['price']*str_replace(",",".",$ccr['multiplier']))}} Ft/óra</strong> +ÁFA)
                </span>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>Megrendelő </strong>által delegált diákok esetében:</p>
        </td>
        <td class="text-center">
            <p class="text-center"><strong>Brutt&oacute; b&eacute;r * <span class="blue">{!!nl2br($ccr['multiplier_company'])!!}</span> + &Aacute;FA</strong><br />
                <span class="small">
                (de minimum a minimális szolgáltatási díj,{{$ccr['price']}} Ft/óra + @if(isset($ccr['min_type']) && $ccr['min_type'] ==1) garantált bérminimum @else minimálbér @endif
                    0,178 szorosa + ÁFA azaz {{$ccr['price']}} Ft/óra + {{$ccr['min']}} Ft/óra =   <strong>{{(int)ceil($ccr['price']+str_replace(",",".",$ccr['min']))}} Ft/óra</strong> +ÁFA)</span></p>
        </td>
    </tr>
    </tbody>
</table>
<p></p>
<p><strong class="blue title">Csere díjmentes!</strong></p>
<p></p>
<table cellspacing="0">
    <tbody>
    <tr class="bg-white">
        <td colspan="2">
            <p><strong>Amit nem kell megfizetni diákok esetén:</strong></p>
        </td>

    </tr>
    <tr>
        <td><span class="blue">✓ szabadság</span></td>
        <td><span class="blue">✓ betegszabadság</span></td>
    </tr>
    <tr>
        <td><span class="blue">✓ szochó</span></td>
        <td><span class="blue">✓ társadalombiztosítási járulék</span></td>
    </tr>
    <tr>
        <td><span class="blue">✓ REHAB</span></td>
        <td><span class="blue">✓ szakképzési hozzájárulás</span></td>
    </tr>
    </tbody>
</table>
<p></p>

<p>Számla kiállításától számított 8 naptári nap</p>
