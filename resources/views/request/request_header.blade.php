<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:22.0pt"><span style="font-family:'Times New Roman',serif">MADS &ndash; A Munka Erő!</span></span></strong></span></span>
</p>


<p style="text-align:center">&nbsp;</p>

<p style="text-align:center">
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:18.0pt"><span style="font-family:'Times New Roman',serif">
                    Ajánlattétel
                    @if($ccr->request[1]["selected"])– munkaerő-kölcsönzésére, -közvetítésére @endif
                    @if($ccr->request[1]["selected"] && ($ccr->request[0]["selected"] || $ccr->request[2]["selected"] )) és @endif
                    @if($ccr->request[0]["selected"]) diák @endif
                    @if($ccr->request[0]["selected"] && $ccr->request[2]["selected"]) - és @endif
                    @if($ccr->request[2]["selected"]) nyugdíjas munkaerő @endif
                    foglalkoztatására </span></p>


<p style="text-align:center">&nbsp;</p>

<p style="text-align:center">&nbsp;</p>

@php
    $company = \Modules\Company\Entities\Company::where('id',$ccr->company_id)->first();
    $cc = \Modules\Campaig\Entities\CampaignCompany::where("campaign_id",$ccr->campaign_id)->where("company_id",$ccr->company_id)->first();
    $user = \Modules\User\Entities\User::where('id',$cc->user_id)->first();
    $contact = $company->contacts()->first();
@endphp
<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><strong><span style="font-size:20.0pt"><span><span
                                style="font-family:'Times New Roman',serif"><span style="color:black">{!!$company->name!!}</span></span></span></span></strong></span></span></p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span><span style="font-family:'Times New Roman',serif">{!!$contact->name!!}</span></span><span
                    style="font-family:'Times New Roman',serif"> r&eacute;sz&eacute;re</span></span></span></p>

<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span>
                <span style="font-family:'Times New Roman',serif">{!!$contact->phone!!}</span><br>
                <span style="font-family:'Times New Roman',serif"> <a href="mailto:{!!$contact->email!!}" style="color:#0563c1; text-decoration:underline">{!!$contact->email!!}</a></span>
            </span></span></span>
</p>


<p style="text-align:center">&nbsp;</p>

<p>&nbsp;</p>

<p style="text-align:center">&nbsp;</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">&nbsp;</p>

<p style="text-align:right">
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:'Times New Roman',serif">MADS Csoport</span></span></span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:'Times New Roman',serif">1092 Budapest, Erkel utca 3. fsz.</span></span></span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:'Times New Roman',serif">tel: +36 (1) 951 0754</span></span></span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:'Times New Roman',serif">fax: +36 (1) 700 1773</span></span></span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span><span style="font-family:'Times New Roman',serif">mobil: {!!$user->phone!!}</span></span></span></span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span ><span style="font-family:'Times New Roman',serif">e-mail: </span></span></span><a href="mailto:{!!$user->email!!}" style="color:#0563c1; text-decoration:underline"><span style="font-size:12.0pt"><span><span style="font-family:'Times New Roman',serif">{!!$user->email!!}</span></span></span></a>&nbsp; </span></span><br/>
    <span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><span style="font-size:12.0pt"><span style="font-family:'Times New Roman',serif"><span style="color:#2d2d2d"><span style="font-size:12.0pt"><span ><span style="font-family:'Times New Roman',serif">{!!$user->firstnamne!!} {!!$user->lastname!!}</span></span></span></span></span></span></span></span>
</p>

