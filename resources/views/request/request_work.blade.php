<style>
    p{
        padding:3px;
        margin:3px;

    }
    body{
        font-family: Calibri,sans-serif;
        font-size: 14px;
    }
    .blue{
        color:#e86902
    }
    .justify{
        text-align: justify;
    }
    .title{
        font-size:16px
    }
    ul.custum-list{
        list-style: none;
    }
    ul.custum-list li:before {
        content: '✓';
    }
    table {
        width: 100%;
        padding:3px;
        margin:3px;
        border-collapse:collapse; border:none;

    }
    .title-work{
         background: #fff2cc;
        padding:5px;
        font-size: 16px;
        text-align: center;
        margin-bottom: 10px;
    }
    tr th {
        font-weight: 700;

    }
    tr th, tr td {
        padding: 5px;

    }
    .bg-white td{
        background: rgba(0, 0, 0, 0.93) !important;
        color: white;
    }
    tr:nth-child(even) {background-color: #ffd966;}
    tr:nth-child(even) {background-color: #fff2cc;}

</style>
@if(isset($type) && $type ==1)
<p class="blue title"><strong>&Aacute;raj&aacute;nlat mag&aacute;n munkaerő-k&ouml;zvet&iacute;t&eacute;s&eacute;re</strong></p>
@endif
<p class="blue title-work"><strong>Munkakör: {!!nl2br($ccr['name'])!!} (FEOR: {!!nl2br($ccr['feor'])!!})</strong></p>
<table cellspacing="0">
    <tbody>
    <tr class="bg-white">
        <td>
            <p ><strong>Elvárások:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p> {!!nl2br($ccr['requirements'])!!}</p>
        </td>
    </tr>

    <tr class="bg-white">
        <td>
            <p  ><strong>Feladat rövid bemutatása:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p> {!!nl2br($ccr['task'])!!}</p>
        </td>
    </tr>

    <tr class="bg-white">
        <td>
            <p  ><strong>Javasolt juttatási csomag:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p class="blue"><strong> {!!nl2br($ccr['price'])!!}</strong> </p>
            @if(isset($ccr['other_price']) && $ccr['other_price'])
                <p> {!!nl2br($ccr['other_price'])!!} </p>
            @endif
        </td>
    </tr>
    </tbody>
</table>
<p></p>
<p><strong>Szolgáltatási díj, mely minden költséget tartalmaz, a munkavállaló bérét is:</strong></p>
<p></p>
<table cellspacing="0">
    <tbody>
    <tr class="bg-white">
        <td >
            <p  ><strong>Megnevez&eacute;s:</strong></p>
        </td>
        <td>
            <p ><strong>Sikerd&iacute;j:</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>MADS </strong>szolg&aacute;ltat&aacute;si d&iacute;j:</p>
        </td>
        <td>
            @if(isset($type) && $type ==1)
                <p><strong>Éves bruttó alapbér  <span class="blue">{!!nl2br($ccr['multiplier'])!!}</span>-a + &Aacute;FA</strong></p>
            @else
            <p><strong>Foglalkoztat&aacute;si k&ouml;lts&eacute;g * <span class="blue">{!!nl2br($ccr['multiplier'])!!}</span> + &Aacute;FA</strong></p>
            @endif
        </td>
    </tr>
    @if(isset($type) && $type ==0)
    <tr>
        <td>
            <p>K&ouml;lcs&ouml;nz&ouml;tt &aacute;llom&aacute;ny cser&eacute;je<br /> a k&ouml;lcs&ouml;nz&eacute;si időszak alatt:</p>
        </td>
        <td>
            <p><strong>d&iacute;jmentes</strong></p>
        </td>
    </tr>
    @endif
    </tbody>
</table>
@if(isset($type) && $type ==1)
    <p></p>
    <p>A közvetítés sikeresnek minősül és kiszámlázásra kerül, ha a MADS által bemutatott jelölt részére a Megrendelő munka-, vagy munkára irányuló szerződést ajánl és azt a jelölt elfogadja.</p>
    <p></p>
@endif
@if(isset($ccr['name1']) && $ccr['name1'])
<p></p>
<p><strong>MADS</strong> &aacute;ltal k&ouml;lcs&ouml;nz&ouml;tt munkav&aacute;llal&oacute; saj&aacute;t &aacute;llom&aacute;nyba v&eacute;tele (munkaerő-k&ouml;zvet&iacute;t&eacute;s):</p>
<p></p>
<table cellspacing="0">
    <tbody>
    <tr class="bg-white">
        <td>
            <p ><strong>Megnevez&eacute;s:</strong></p>
        </td>
        <td>
            <p ><strong>@if(isset($type) && $type ==1) Sikerd&iacute;j: @else Megbízási díj @endif</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>{!!nl2br($ccr['name1'])!!}</strong><strong> h&oacute;nap k&ouml;z&ouml;tt</strong></p>
        </td>
        <td>

            <p><strong>&eacute;ves brutt&oacute; b&eacute;r  <span class="blue">{!!nl2br($ccr['name1_price'])!!}</span> + &Aacute;FA</strong></p>

        </td>
    </tr>
    <tr>
        <td>
            <p><strong>{!!nl2br($ccr['name2'])!!}</strong><strong> h&oacute;nap k&ouml;z&ouml;tt</strong></p>
        </td>
        <td>
            <p><strong>&eacute;ves brutt&oacute; b&eacute;r  <span class="blue">{!!nl2br($ccr['name2_price'])!!}</span> + &Aacute;FA</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>{!!nl2br($ccr['name3'])!!}</strong><strong> h&oacute;nap k&ouml;z&ouml;tt</strong></p>
        </td>
        <td>
            <p><strong>&eacute;ves brutt&oacute; b&eacute;r  <span class="blue">{!!nl2br($ccr['name3_price'])!!}</span> + &Aacute;FA</strong></p>
        </td>
    </tr>
    <tr>
        <td>
            <p><strong>{!!nl2br($ccr['name4'])!!}</strong><strong> h&oacute;napt&oacute;l</strong></p>
        </td>
        <td>
            <p><strong>d&iacute;jmentes</strong></p>
        </td>
    </tr>
    </tbody>
</table>
@endif
<p></p>
<p>Számla kiállításától számított 8 naptári nap.</p>
