import axios from 'axios'
import store from './store'
import i18n from './translate'

axios.get(`${process.env.MIX_APP_URL}/api/${process.env.MIX_API_VERSION}/system/languages`).then((response) => {
    console.log(response.data)
    localStorage.setItem('langs', JSON.stringify(response.data))
    for (const item in response.data) {
        console.log(item)
        axios.get(`${process.env.MIX_APP_URL}/api/${process.env.MIX_API_VERSION}/translation/${item}`).then((resp) => {
            store.commit('defaultStore/setLanguage', { lang: item, payload: resp.data })
            i18n.setLocaleMessage(item, resp.data)
        })
    }
})
