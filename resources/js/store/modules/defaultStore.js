export default ({
    namespaced: true,
    state: {
        loading: true,
        errors: [],
        pageLanguage: process.env.MIX_APP_DEFAULT_LOCALE,
        languages: {
            hu: {},
            en: {}
        },
        pageCurrency: '1',
        model: {},
        recruit: {},
        prefixes: {},
        loggedIn: false,
        studentLoggedIn: false,
        user: {},
        student: {}
    },
    mutations: {
        change (state, payload) {
            // mutate state
            state.loading = payload
        },
        setLanguage (state, payload) {
            state.languages[payload.lang] = payload.payload
        },
        setRecruitment (state, payload) {
            state.recruit = payload
        },
        setErrors (state, payload) {
            state.errors = payload
        },
        setModel (state, payload) {
            state.model = payload
        },
        setLoggedIn (state, payload) {
            state.loggedIn = payload
        },
        setUser (state, payload) {
            state.user = payload
        },
        setStudentLoggedIn (state, payload) {
            state.studentLoggedIn = payload
        },
        setStudent (state, payload) {
            state.student = payload
        },
        setPageLanguage (state, payload) {
            state.pageLanguage = payload
        },
        setPageCurrency (state, payload) {
            state.pageCurrency = payload
        },
        setPrefixes (state, payload) {
            state.prefixes = payload
        }
    },
    getters: {}
})
