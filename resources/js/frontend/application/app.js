/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import '@babel/polyfill'
import Es6Promise from 'es6-promise'
import 'es6-promise/auto'
import VueAxios from 'vue-axios'
import axios from 'axios'
import router from '../router'
import VueI18n from 'vue-i18n'
import store from '../../store'
import App from '../components/App.vue'
import VueCookies from 'vue-cookies'
import i18n from '../../translate'
import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

// Global 3rd party plugins

import 'select2'
import 'moment'
import 'bootstrap-datepicker'
import 'bootstrap-datetime-picker'
import 'vue-the-mask'
Es6Promise.polyfill()

require('./bootstrap')
require('./axios-frontend')
require('../../frontend/modules-frontend')
require('../../translation')

window.Vue = require('vue')
window.io = require('socket.io-client')

Vue.use(VueAnalytics, {
    id: 'G-E4R5YSW3ET',
    router
})
Vue.use(VueAxios, axios, VueI18n, router, store, VueCookies)

const mixin = require('../../mixins')
Vue.mixin(mixin.default)

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

const filesPages = require.context('../components/pages/', true, /\.vue$/i)
filesPages.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], filesPages(key).default))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.config.productionTip = false
Vue.prototype.$translate = i18n
Vue.prototype.$eventHub = new Vue()

new Vue({
    el: '#app',
    router,
    VueAxios,
    axios,
    VueCookies,
    store,
    i18n,
    render: (h) => h(App)
})
