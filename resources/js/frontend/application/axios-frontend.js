import axios from 'axios'
import store from '../../store/index'
import VueCookie from 'vue-cookies'
import i18n from '../../translate'
import Vue from 'vue'
import VueAxios from 'vue-axios'
import VueSocialauth from 'vue-social-auth'

Vue.use(VueAxios, axios)
Vue.use(VueSocialauth, {

    providers: {
        facebook: {
            clientId: `${process.env.MIX_FACEBOOK_ID}`,
            redirectUri: `${process.env.MIX_APP_URL}` + '/auth/facebook/callback' // Your client app URL
        },
        google: {
            clientId: `${process.env.MIX_GOOGLE_ID}`,
            redirectUri: `${process.env.MIX_APP_URL}` + '/auth/google/callback' // Your client app URL
        },
        twitter: {
            clientId: `${process.env.MIX_TWITTER_ID}`,
            redirectUri: `${process.env.MIX_APP_URL}` + '/auth/twitter/callback' // Your client app URL
        }

    }
})
// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if (!config.url.startsWith(process.env.MIX_APP_URL)) {
        config.headers.common = {}
    } else {
        if (localStorage.getItem('language')) {
            config.headers.common.Language = localStorage.getItem('language')
        } else {
            localStorage.setItem('language', process.env.MIX_APP_DEFAULT_LOCALE)
            config.headers.common.Language = process.env.MIX_APP_DEFAULT_LOCALE
        }
        if (localStorage.getItem('currency')) {
            config.headers.common.Currency = localStorage.getItem('currency')
        } else {
            localStorage.setItem('currency', process.env.MIX_APP_DEFAULT_CURRENCY)
            config.headers.common.Currency = process.env.MIX_APP_DEFAULT_CURRENCY
        }
        if (VueCookie.get('student-token')) {
            config.headers.common.Authorization = 'Bearer ' + VueCookie.get('student-token')
        }
    }

    return config
}, function (error) {
    // Do something with request error
    return Promise.reject(error)
})
const Toption = {
    closeButton: true,
    debug: false,
    newestOnTop: false,
    progressBar: false,
    positionClass: 'toast-bottom-right',
    preventDuplicates: true,
    onclick: null,
    showDuration: '300',
    hideDuration: '1000',
    timeOut: '5000',
    extendedTimeOut: '1000',
    showEasing: 'swing',
    hideEasing: 'linear',
    showMethod: 'fadeIn',
    hideMethod: 'fadeOut'
}

axios.interceptors.response.use(
    function (response) {
        if (response.config.method !== 'get') {
            toastr.options = Toption
            if (!response.headers.hide_alert && response.headers.hide_alert !== '1') {
                toastr.success(i18n.t('Success!'), i18n.t('Action successfully done.'))
            }
        }
        if (VueCookie.get('student-token')) {
            VueCookie.set('student-token', VueCookie.get('student-token'), 60 * 60)
        }

        store.commit('defaultStore/setErrors', [])

        return response
    },
    function (error) {
        toastr.options = Toption
        if (error.response.data && error.response.data.errors) {
            store.commit('defaultStore/setErrors', error.response.data.errors)
        }
        if (error.response.status && error.response.status === 419) {
            window.location.reload()
        }
        if (error.response.status && error.response.status === 401) {
            console.log('access denied')
            // toastr.error(i18n.t('Access Denied!'), i18n.t('You have insufficent permission for this action.'))
        } else if (error.response.status && error.response.status === 422) {
            var errorText = ''
            $.each(error.response.data.errors, function (key, value) {
                errorText += value + '<br/>'
            })
            toastr.error(i18n.t('Error!') + '<br/>' + errorText, i18n.t('Please fix the errors!'))
        } else {
            if (!error.response.config.url.startsWith('/storage/')) {
                toastr.error(i18n.t('Error!'), i18n.t('Something went wrong.'))
            }
        }
        return Promise.reject(error)
    }
)
