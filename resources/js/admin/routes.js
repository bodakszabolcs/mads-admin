import Admin from '../../../Modules/Admin/Resources/assets/js/components/Admin'
import NotFound from '../../../Modules/Admin/Resources/assets/js/components/NotFound'

const mixedroutes = []

/* ROUTE CONFIG START */
const routesAdmin = require('./../../../Modules/Admin/Resources/assets/js/router/index')
const routesSlider = require('./../../../Modules/Slider/Resources/assets/js/router/index')
const routesTranslation = require('./../../../Modules/Translation/Resources/assets/js/router/index')
const routesSystem = require('./../../../Modules/System/Resources/assets/js/router/index')
const routesUsers = require('./../../../Modules/User/Resources/assets/js/router/index')
const routesRoles = require('./../../../Modules/Role/Resources/assets/js/router/index')
const routesModuleBuilder = require('./../../../Modules/ModuleBuilder/Resources/assets/js/router/index')
const routesPage = require('./../../../Modules/Page/Resources/assets/js/router/index')
const routesMenu = require('./../../../Modules/Menu/Resources/assets/js/router/index')
const routesBlog = require('./../../../Modules/Blog/Resources/assets/js/router/index')
const routesCategory = require('./../../../Modules/Category/Resources/assets/js/router/index')
const routesDownloadableDocument = require('./../../../Modules/DownloadableDocument/Resources/assets/js/router/index')
const routesMessage = require('./../../../Modules/Message/Resources/assets/js/router/index')
const routesWorkCategory = require('./../../../Modules/WorkCategory/Resources/assets/js/router/index')
const routesWork = require('./../../../Modules/Work/Resources/assets/js/router/index')
const routesFeor = require('./../../../Modules/Feor/Resources/assets/js/router/index')
const routesLanguage = require('./../../../Modules/Language/Resources/assets/js/router/index')
const routesCity = require('./../../../Modules/City/Resources/assets/js/router/index')
const routesPostSubject = require('./../../../Modules/PostSubject/Resources/assets/js/router/index')
const routesPostDelivery = require('./../../../Modules/PostDelivery/Resources/assets/js/router/index')
const routesPostDeliveryType = require('./../../../Modules/PostDeliveryType/Resources/assets/js/router/index')
const routesContent = require('./../../../Modules/Content/Resources/assets/js/router/index')
const routesPartnerLogo = require('./../../../Modules/PartnerLogo/Resources/assets/js/router/index')
const routesStudent = require('./../../../Modules/Student/Resources/assets/js/router/index')
const routesNationality = require('./../../../Modules/Nationality/Resources/assets/js/router/index')
const routesAreaType = require('./../../../Modules/AreaType/Resources/assets/js/router/index')
const routesEducationLevel = require('./../../../Modules/EducationLevel/Resources/assets/js/router/index')
const routesEducationArea = require('./../../../Modules/EducationArea/Resources/assets/js/router/index')
const routesSchool = require('./../../../Modules/School/Resources/assets/js/router/index')
const routesAttribute = require('./../../../Modules/Attribute/Resources/assets/js/router/index')
const routesCountry = require('./../../../Modules/Country/Resources/assets/js/router/index')
const routesCompany = require('./../../../Modules/Company/Resources/assets/js/router/index')
const routesProjectStatus = require('./../../../Modules/ProjectStatus/Resources/assets/js/router/index')
const routesProject = require('../../../Modules/Project/Resources/assets/js/router/index')
const routesFixing = require('./../../../Modules/Fixing/Resources/assets/js/router/index')
const routesMonthlyPayment = require('./../../../Modules/MonthlyPayment/Resources/assets/js/router/index')
const routesEmail = require('./../../../Modules/Email/Resources/assets/js/router/index')
const routesWorkLocation = require('./../../../Modules/WorkLocation/Resources/assets/js/router/index')
const routesStudentPresent = require('./../../../Modules/StudentPresent/Resources/assets/js/router/index')
const routesPayroll = require('./../../../Modules/Payroll/Resources/assets/js/router/index')
const routesPostalIncome = require('./../../../Modules/PostalIncome/Resources/assets/js/router/index')
const routesPostalOutgoings = require('./../../../Modules/PostalOutgoings/Resources/assets/js/router/index')
const routesInvoiceTransporter = require('./../../../Modules/InvoiceTransporter/Resources/assets/js/router/index')
const routesInvoice = require('./../../../Modules/Invoice/Resources/assets/js/router/index')
const routesMonthlyOrders = require('./../../../Modules/MonthlyOrders/Resources/assets/js/router/index')
const routesProjectEmailSablon = require('./../../../Modules/ProjectEmailSablon/Resources/assets/js/router/index')
const routesImportantInfo = require('./../../../Modules/ImportantInfo/Resources/assets/js/router/index')
const routesAutomaticEmail = require('./../../../Modules/AutomaticEmail/Resources/assets/js/router/index')
const routesDownPayment = require('./../../../Modules/DownPayment/Resources/assets/js/router/index')
const routesOnlineStudentCard = require('./../../../Modules/OnlineStudentCard/Resources/assets/js/router/index')
const routesRealization = require('./../../../Modules/Realization/Resources/assets/js/router/index')
const routesCampaig = require('./../../../Modules/Campaig/Resources/assets/js/router/index')
const routesCampaignStatus = require('./../../../Modules/CampaignStatus/Resources/assets/js/router/index')
const routesRequest = require('./../../../Modules/Request/Resources/assets/js/router/index')
const routesQuestions = require('./../../../Modules/Questions/Resources/assets/js/router/index')
const routesSemester = require('./../../../Modules/Semester/Resources/assets/js/router/index')
const routesProjectQuestion = require('./../../../Modules/ProjectQuestion/Resources/assets/js/router/index')
/* ROUTE CONFIG END */

/* ROUTE ARRAY START */
const routes = [
    routesSystem.default, routesTranslation.default, routesAdmin.default, routesUsers.default, routesRoles.default, routesModuleBuilder.default, routesSlider.default,
    routesPage.default, routesMenu.default, routesBlog.default, routesCategory.default, routesDownloadableDocument.default, routesMessage.default, routesWorkCategory.default, routesWork.default, routesFeor.default, routesLanguage.default, routesCity.default, routesPostSubject.default, routesPostDelivery.default, routesPostDeliveryType.default, routesContent.default, routesPartnerLogo.default, routesStudent.default, routesNationality.default, routesAreaType.default, routesEducationLevel.default, routesEducationArea.default, routesSchool.default, routesAttribute.default, routesCountry.default, routesCompany.default, routesProjectStatus.default, routesProject.default, routesFixing.default, routesMonthlyPayment.default, routesEmail.default, routesWorkLocation.default, routesStudentPresent.default, routesPayroll.default, routesPostalIncome.default, routesPostalOutgoings.default, routesInvoiceTransporter.default, routesInvoice.default, routesMonthlyOrders.default, routesProjectEmailSablon.default, routesImportantInfo.default, routesAutomaticEmail.default, routesDownPayment.default, routesOnlineStudentCard.default, routesRealization.default, routesCampaig.default, routesCampaignStatus.default, routesRequest.default, routesQuestions.default, routesSemester.default, routesProjectQuestion.default] // routesEnd
/* ROUTE ARRAY END */
for (const it in routes) {
    for (const elem in routes[it]) {
        let have = false
        for (const find in mixedroutes) {
            if (mixedroutes[find].path === routes[it][elem].path) {
                have = true
                mixedroutes[find].children = mixedroutes[find].children.concat(routes[it][elem].children)
            }
        }
        if (!have) {
            mixedroutes.push(routes[it][elem])
        }
    }
}
mixedroutes.push(
    {
        path: '/admin',
        component: Admin,
        meta: {
            title: 'Admin',
            requiresAuth: true
        },
        children: [{
            path: '*',
            component: NotFound,
            meta: {
                title: 'A keresett oldal nem található'
            }
        }
        ]
    })
export default mixedroutes
