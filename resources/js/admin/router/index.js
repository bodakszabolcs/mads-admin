import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import store from '../../store'

Vue.use(VueRouter)

const routes = require('./../routes')

const router = new VueRouter({
    mode: 'history',
    routes: routes.default
})

axios.get(`${process.env.MIX_APP_URL}/prefixes.json`).then(response => {
    store.commit('defaultStore/setPrefixes', response.data)
})
function historyPush (route) {
    let history = localStorage.getItem('urlHistoryList')
    if (!history || !history.length) {
        history = {}
    } else {
        history = JSON.parse(history)
    }
    if (!route.fullPath.includes('api_token=')) {
        history[route.path] = route.fullPath
    }
    localStorage.setItem('urlHistoryList', JSON.stringify(history))
}
router.beforeEach((to, from, next) => {
    historyPush(to)
    if (to.matched.some(record => record.meta.requiresAuth)) {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        if ($cookies.get('token') === null) {
            next({
                path: `/${process.env.MIX_ADMIN_URL}/login`,
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        next() // make sure to always call next()!
    }
})

export default router
