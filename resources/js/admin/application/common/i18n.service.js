import i18n from '../../../translate'

const i18nService = {
    defaultLanguage: 'hu',

    languages: [
        {
            lang: 'hu',
            name: i18n.t('Hungarian'),
            flag: process.env.MIX_APP_URL + '/assets/media/flags/115-hungary.svg'
        }, {
            lang: 'en',
            name: i18n.t('English'),
            flag: process.env.MIX_APP_URL + '/assets/media/flags/226-united-states.svg'
        }
    ],

    /**
   * Keep the active language in the localStorage
   * @param lang
   */
    setActiveLanguage (lang) {
        localStorage.setItem('language', lang)
    },

    /**
   * Get the current active language
   * @returns {string | string}
   */
    getActiveLanguage () {
        return localStorage.getItem('language') || this.defaultLanguage
    }
}

export default i18nService
