import axios from 'axios'
import store from '../../store/index'
import VueCookie from 'vue-cookies'
import i18n from '../../translate'
import toastr from 'toastr'

const timeOut = null

// Add a request interceptor
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    if (localStorage.getItem('language')) {
        config.headers.common.Language = localStorage.getItem('language')
    } else {
        localStorage.setItem('language', process.env.MIX_APP_DEFAULT_LOCALE)
        config.headers.common.Language = process.env.MIX_APP_DEFAULT_LOCALE
    }

    if (VueCookie.get('token')) {
        config.headers.common.Authorization = 'Bearer ' + VueCookie.get('token')
    }

    return config
}, function (error) {
    // Do something with request error
    clearTimeout(timeOut)
    return Promise.reject(error)
})

axios.interceptors.response.use(
    function (response) {
        if (response.config.method !== 'get') {
            toastr.options = {
                closeButton: true,
                debug: false,
                newestOnTop: false,
                progressBar: false,
                positionClass: 'toast-bottom-center',
                preventDuplicates: true,
                onclick: null,
                showDuration: '300',
                hideDuration: '1000',
                timeOut: '5000',
                extendedTimeOut: '1000',
                showEasing: 'swing',
                hideEasing: 'linear',
                showMethod: 'fadeIn',
                hideMethod: 'fadeOut'
            }

            if (!response.headers.hide_alert && response.headers.hide_alert !== '1') {
                toastr.success(i18n.t('Success!'), i18n.t('Action successfully done.'))
            }
        }
        if (VueCookie.get('token')) {
            VueCookie.set('token', VueCookie.get('token'), 60 * 60)
        }

        store.commit('defaultStore/setErrors', [])
        clearTimeout(timeOut)

        return response
    },
    function (error) {
        toastr.options = {
            closeButton: true,
            debug: false,
            newestOnTop: false,
            progressBar: false,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true,
            onclick: null,
            showDuration: '100',
            hideDuration: '500',
            timeOut: '1000',
            extendedTimeOut: '1000',
            showEasing: 'swing',
            hideEasing: 'linear',
            showMethod: 'fadeIn',
            hideMethod: 'fadeOut'
        }

        if (error.response.data && error.response.data.errors) {
            store.commit('defaultStore/setErrors', error.response.data.errors)
        }
        if (error.response.status && error.response.status === 419) {
            window.location.reload()
        }
        if (error.response.status && (error.response.status === 401 || error.response.status === 403)) {
            toastr.error(i18n.t('Hozzéférés megtagadva!'), i18n.t('You have insufficent permission for this action.'))
        } else if (error.response.status && error.response.status === 422) {
            var errorText = ''
            $.each(error.response.data.errors, function (key, value) {
                errorText += value + '<br/>'
            })
            if (error.response.data.data && error.response.data.data.message) {
                errorText += error.response.data.data.message + '<br/>'
            }
            toastr.error(i18n.t('Error!') + '<br/>' + errorText, i18n.t('Please fix the errors!'))
        } else {
            if (!error.response.config.url.startsWith('/storage/')) {
                toastr.error(i18n.t('Error!'), i18n.t('Valami hiba történt'))
            }
        }

        clearTimeout(timeOut)
        return Promise.reject(error)
    }
)
