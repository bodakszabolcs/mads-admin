const mix = require('laravel-mix')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/*
 |--------------------------------------------------------------------------
 | Frontend Asset Management
 |--------------------------------------------------------------------------
 */

mix.js(['resources/js/frontend/application/app.js'], 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .options({ processCssUrls: false })

mix.styles([
    'public/frontend/css/animate.css',
    'public/frontend/css/boostrap.min.css',
    'public/frontend/css/device.min.css',
    'public/frontend/css/font-awesome.min.css',
    'public/frontend/css/idangeroues.swiper.css',
    'public/frontend/css/simple-line-icons.css',
    'public/frontend/css/style.css',
    'public/frontend/css/float-label.css',
    'public/frontend/css/main.css',
    'public/assets/vendors/general/select2/dist/css/select2.min.css',
    'public/assets/vendors/general/bootstrap-datepicker/dist/css/bootstrap-datepicker.css'

], 'public/css/frontend.css').options({ processCssUrls: false })
mix.scripts([
    'public/frontend/js/controls.min.js',
    'public/assets/vendors/general/toastr/build/toastr.min.js',
    'public/assets/vendors/general/sweetalert2/dist/sweetalert2.all.min.js',
    'public/assets/vendors/general/bootstrap-timepicker/js/bootstrap-timepicker.js',
    'public/frontend/js/global.js'
], 'public/js/mixed.js')

mix.styles([
    'node_modules/ContentTools/build/content-tools.min.css',
    'resources/sass/extra.css'
], 'public/css/vendor.css')
    .options({ processCssUrls: false })

/*
 |--------------------------------------------------------------------------
 | Admin Asset Management
 |--------------------------------------------------------------------------
 */

mix.js(['resources/js/admin/application/app-admin.js'], 'public/js')
    .sass('resources/sass/app-admin.scss', 'public/css')
    .options({ processCssUrls: false })

if (mix.inProduction()) {
    mix.version()

    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.jsx?$/,
                    exclude: /node_modules/,
                    include: [
                        path.resolve('node_modules/laravel-file-manager'),
                        path.resolve('node_modules/engine.io-client')
                    ],
                    use: [{
                        loader: 'babel-loader',
                        options: Config.babel()
                    }]
                }
            ]
        }
    })
} else {
    mix.webpackConfig({
        module: {
            rules: [
                {
                    enforce: 'pre',
                    test: /\.(js|vue)$/,
                    loader: 'eslint-loader',
                    exclude: /node_modules/
                }
            ]
        }
    })
}
