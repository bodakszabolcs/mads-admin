import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import MonthlyOrders from '../components/MonthlyOrders'
import MonthlyOrdersList from '../components/MonthlyOrdersList'
import MonthlyOrdersCreate from '../components/MonthlyOrdersCreate'
import MonthlyOrdersEdit from '../components/MonthlyOrdersEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'monthly-orders',
                component: MonthlyOrders,
                meta: {
                    title: 'MonthlyOrders'
                },
                children: [
                    {
                        path: 'index',
                        name: 'MonthlyOrdersList',
                        component: MonthlyOrdersList,
                        meta: {
                            title: 'MonthlyOrders',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly-orders/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly-orders/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'MonthlyOrdersCreate',
                        component: MonthlyOrdersCreate,
                        meta: {
                            title: 'Create MonthlyOrders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly-orders/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'MonthlyOrdersEdit',
                        component: MonthlyOrdersEdit,
                        meta: {
                            title: 'Edit MonthlyOrders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly-orders/index'
                        }
                    }
                ]
            }
        ]
    }
]
