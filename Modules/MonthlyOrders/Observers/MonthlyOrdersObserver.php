<?php

namespace Modules\MonthlyOrders\Observers;

use Modules\MonthlyOrders\Entities\MonthlyOrders;

class MonthlyOrdersObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function saved(MonthlyOrders $model)
    {
        MonthlyOrders::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function created(MonthlyOrders $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function updated(MonthlyOrders $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function deleted(MonthlyOrders $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function restored(MonthlyOrders $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\MonthlyOrders\Entities\MonthlyOrders  $model
     * @return void
     */
    public function forceDeleted(MonthlyOrders $model)
    {
        //
    }
}
