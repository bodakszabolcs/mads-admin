<?php

namespace Modules\MonthlyOrders\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\MonthlyOrders\Entities\MonthlyOrders;


class MonthlyOrdersListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "company_name" => $this->company_name,
            "company_id" => $this->company_id,
            "student_count" => $this->student_count,
            "type" => ($this->type==1) ? 'Alkalmi' : 'Folyamatos',
            "month" => $this->month,
            "user_name" => $this->user_name,
            "status" => Arr::get(MonthlyOrders::$status, $this->status, 0),
        ];
    }
}
