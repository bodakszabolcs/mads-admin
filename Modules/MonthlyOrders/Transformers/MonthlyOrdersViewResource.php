<?php

namespace Modules\MonthlyOrders\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Company;
use Modules\MonthlyOrders\Entities\MonthlyOrders;
use Modules\User\Entities\User;


class MonthlyOrdersViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "company_id" => $this->company_id,
            "student_count" => $this->student_count,
            "type" => $this->type,
            "month" => $this->month,
            "month2" => $this->month,
            "status" => $this->status,
            "selectables" => [
                "companies" => Company::pluck("name", "id"),
                "status" => MonthlyOrders::$status,
            ]
        ];
    }
}
