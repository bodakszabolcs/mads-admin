<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('monthly_orders');
        Schema::create('monthly_orders', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("company_id")->unsigned()->nullable();
$table->integer("student_count")->unsigned()->nullable();
$table->tinyInteger("type")->unsigned()->nullable();
$table->integer("month")->unsigned()->nullable();
$table->tinyInteger("status")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_orders');
    }
}
