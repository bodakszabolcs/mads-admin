<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\MonthlyOrders\Entities\MonthlyOrders;

$factory->define(MonthlyOrders::class, function (Faker $faker) {
    return [
        "company_id" => rand(1000,5000),
"student_count" => rand(1000,5000),
"type" => rand(1,10),
"month" => rand(1000,5000),
"status" => rand(1,10),

    ];
});
