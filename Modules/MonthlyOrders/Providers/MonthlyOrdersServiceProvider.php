<?php

namespace Modules\MonthlyOrders\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\MonthlyOrders\Entities\MonthlyOrders;
use Modules\MonthlyOrders\Observers\MonthlyOrdersObserver;

class MonthlyOrdersServiceProvider extends ModuleServiceProvider
{
    protected $module = 'monthlyorders';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        MonthlyOrders::observe(MonthlyOrdersObserver::class);
    }
}
