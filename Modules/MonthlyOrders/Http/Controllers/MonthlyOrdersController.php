<?php

namespace Modules\MonthlyOrders\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\MonthlyOrders\Entities\MonthlyOrders;
use Illuminate\Http\Request;
use Modules\MonthlyOrders\Http\Requests\MonthlyOrdersCreateRequest;
use Modules\MonthlyOrders\Http\Requests\MonthlyOrdersUpdateRequest;
use Modules\MonthlyOrders\Transformers\MonthlyOrdersViewResource;
use Modules\MonthlyOrders\Transformers\MonthlyOrdersListResource;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Modules\User\Entities\User;
use Modules\Project\Entities\ProjectStudent;
use Modules\Project\Entities\Project;

class MonthlyOrdersController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new MonthlyOrders();
        $this->viewResource = MonthlyOrdersViewResource::class;
        $this->listResource = MonthlyOrdersListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = MonthlyOrdersCreateRequest::class;
        $this->updateRequest = MonthlyOrdersUpdateRequest::class;
    }
    public function index(Request $request, $auth = null)
    {

        $list = MonthlyOrders::leftJoin("companies",'companies.id','=','company_id')->leftJoin('users','users.id','=','monthly_orders.user_id')->select(DB::raw('monthly_orders.*,companies.name as company_name,concat(lastname,\' \',firstname) as user_name '));
        $filter = $this->model->getFilters();

        if($request->has('month') && !empty($request->input('month'))){
            $list = $list->where("month",$request->input("month"));
        }else{
            $list = $list->where("month",date('Y-m',strtotime('-1 month')));
        }
        if($request->has('user_id') && !empty($request->input('user_id'))){
            $list = $list->where("monthly_orders.user_id",$request->input("user_id"));
        }
        if($request->has('status')&& !empty($request->input('status'))){
            $list = $list->where("status",$request->input("status"));
        }
        if($request->has('search') && !empty($request->input('search'))){
            $list=$list->where("companies.name",'Like','%'.$request->input('search').'%');
        }
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {

            $list = $list->where('monthly_orders.user_id', '=', \Auth::id());
        }else{
               $filter[]= [

                       'name' => 'user_id',
                       'title' => 'Felelős',
                       'type' => 'select',
                       'data' => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id")

               ];
        }
        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('companies.name','asc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy('monthly_orders.'.$sort[0],$sort[1]);
        }

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', 100);
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }


        return $this->listResource::collection($list)->additional(['filters' => $filter]);
    }
    public function fillProject(Request $request,$month=null){

        if(!$month){
            $month =date('Y-m');
        }

        //az összeset hozza ne csak akinél van diák
        $project = Project::leftJoin(DB::raw("(select project_students.project_id, count(*) as diak from project_students where project_students.status_id = 2 group by project_students.project_id) as ps"),'ps.project_id','=','projects.id')
            ->where('user_id','=',Auth::id())->select(DB::raw('company_id, count(diak) as diak '))->groupBy('company_id')->get();
        foreach ($project as $p){
            $mo = MonthlyOrders::where('company_id',$p->company_id)->where('month',$month)->first();
            if(!$mo){
                $mo = new MonthlyOrders();
                $mo->company_id = $p->company_id;
                $mo->user_id = Auth::id();
                $mo->month = $month;
                $mo->status = 2;
                $mo->student_count = $p->diak;
                $mo->type=2;
                $mo->save();

            }
        }
        return response()->json($this->successMessage);
    }
    public function setStatus(Request $request,$id){
        $mo =MonthlyOrders::where('id',$id)->first();
        $mo->status = $request->input('status');
        $mo->save();
        return response()->json($this->successMessage);
    }

}
