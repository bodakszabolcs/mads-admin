<?php

namespace Modules\MonthlyOrders\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MonthlyOrdersCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
			'type' => 'required',
			'month' => 'required',
			'status' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'company_id' => __('Cég'),
'student_count' => __('Diákok'),
'type' => __('Típus'),
'month' => __('Hónap'),
'status' => __('Állapot'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
