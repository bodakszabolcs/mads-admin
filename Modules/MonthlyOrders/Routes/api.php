<?php

Route::group(['namespace' => 'Modules\MonthlyOrders\Http\Controllers',
    'prefix' => '/monthlyorders',
    'middleware' => ['auth:sanctum', 'role']
], function () {
    Route::match(['post'], 'change-status/{id}', 'MonthlyOrdersController@setStatus')->name('Change status');
    Route::match(['post'], 'fill-project/{month?}', 'MonthlyOrdersController@fillProject')->name('Fill from project');
});
