<?php

namespace Modules\MonthlyOrders\Entities;

use Modules\MonthlyOrders\Entities\Base\BaseMonthlyOrders;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class MonthlyOrders extends BaseMonthlyOrders
{
    use SoftDeletes, Cachable;


    static $status=[
        1=> 'Nem volt munkavégzés',
        2=> 'Adatok bekérése folymatban',
        3=> 'Adatok bekérve',
        7=>'Adatok bekérve, rögzítés után számlázható',
        8=>' Adatok bekérve, rögzítés után elszámolást kell küldeni',
        9=>' Rögzítve, elszámolást kell küldeni',
        4=> 'Számlázható',
        5=> 'Számlázva',
        6=> 'Lezárva',
    ];




    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
