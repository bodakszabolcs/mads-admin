<?php

namespace Modules\PostSubject\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\PostSubject\Entities\PostSubject;
use Illuminate\Http\Request;
use Modules\PostSubject\Http\Requests\PostSubjectCreateRequest;
use Modules\PostSubject\Http\Requests\PostSubjectUpdateRequest;
use Modules\PostSubject\Transformers\PostSubjectViewResource;
use Modules\PostSubject\Transformers\PostSubjectListResource;

class PostSubjectController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PostSubject();
        $this->viewResource = PostSubjectViewResource::class;
        $this->listResource = PostSubjectListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PostSubjectCreateRequest::class;
        $this->updateRequest = PostSubjectUpdateRequest::class;
    }

}
