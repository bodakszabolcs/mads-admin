<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\PostSubject\Entities\PostSubject;

$factory->define(PostSubject::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
