<?php

namespace Modules\PostSubject\Observers;

use Modules\PostSubject\Entities\PostSubject;

class PostSubjectObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function saved(PostSubject $model)
    {
        PostSubject::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function created(PostSubject $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function updated(PostSubject $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function deleted(PostSubject $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function restored(PostSubject $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PostSubject\Entities\PostSubject  $model
     * @return void
     */
    public function forceDeleted(PostSubject $model)
    {
        //
    }
}
