import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PostSubject from '../components/PostSubject'
import PostSubjectList from '../components/PostSubjectList'
import PostSubjectCreate from '../components/PostSubjectCreate'
import PostSubjectEdit from '../components/PostSubjectEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'post-subject',
                component: PostSubject,
                meta: {
                    title: 'PostSubjects'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PostSubjectList',
                        component: PostSubjectList,
                        meta: {
                            title: 'PostSubjects',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/post-subject/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-subject/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PostSubjectCreate',
                        component: PostSubjectCreate,
                        meta: {
                            title: 'Create PostSubjects',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-subject/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PostSubjectEdit',
                        component: PostSubjectEdit,
                        meta: {
                            title: 'Edit PostSubjects',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-subject/index'
                        }
                    }
                ]
            }
        ]
    }
]
