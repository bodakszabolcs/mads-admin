<?php

namespace Modules\PostSubject\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PostSubject\Entities\PostSubject;
use Modules\PostSubject\Observers\PostSubjectObserver;

class PostSubjectServiceProvider extends ModuleServiceProvider
{
    protected $module = 'postsubject';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PostSubject::observe(PostSubjectObserver::class);
    }
}
