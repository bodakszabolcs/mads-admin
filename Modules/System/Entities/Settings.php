<?php

namespace Modules\System\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\System\Entities\Base\BaseSettings;

class Settings extends BaseSettings
{
    use Cachable;
}
