<?php

namespace Modules\System\Http\Controllers;

use App\Events\TestEvent;
use App\Events\UpdateNotifications;
use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Kreait\Firebase\Messaging\CloudMessage;
use Modules\System\Emails\TestEmail;
use Modules\System\Entities\Settings;
use Modules\System\Notifications\SystemNotification;
use Modules\User\Entities\User;
use Nwidart\Modules\Facades\Module;

class SystemController extends AbstractLiquidController
{
    public function runCommand(Request $request)
    {
        $response = '';
        if ($request->has('command') && $request->input('command') != null) {
            Artisan::call($request->input('command'));
            $response = Artisan::output();
        }

        if ($request->has('predefined') && $request->input('predefined') != null && is_array($request->input('predefined'))) {
            foreach ($request->input('predefined') as $pre) {
                $response .= '<br/>';
                Artisan::call($pre);
                $response .= Artisan::output();
            }
        }

        return response()->json(['data' => $response], $this->successStatus);
    }
    public function pushNotification(Request $request){
        $firebase = (new \Kreait\Firebase\Factory())
            ->withServiceAccount(base_path('firebase_config.json'));

        $messaging = $firebase->createMessaging();

        $message = CloudMessage::fromArray([
            'notification' => [
                'title' => $request->input("title"),
                'body' => $request->input("body"),
            ],
            'data'=>["link"=>$request->input("link")],
            'topic' => 'MADS'.$request->input("channel")
        ]);


        $messaging->send($message);
        return response()->json(['message' => 'Push notification sent successfully']);
    }
    public function getLanguages(Request $request)
    {
        $languages = Settings::where('settings_key', '=', 'language')->first();

        return response()->json(json_decode($languages->settings_value), $this->successStatus);
    }

    public function setLanguages(Request $request)
    {
        if (sizeof($request->input('languages')) < 1) {
            abort(422);
        }

        $langArr = [];
        $liveJsons = [];
        foreach ($request->input('languages') as $lang) {
            $langArr[$lang['key']] = $lang['value'];
            $liveJsons[] = 'lang/' . $lang['key'] . '.json';
            if (!File::exists(resource_path('lang/' . $lang['key'] . '.json')) && !File::exists(resource_path('lang/' . $lang['key'] . '.json.old'))) {
                Storage::disk('resources')->put('lang/' . $lang['key'] . '.json', '{}');
            }

            if (File::exists(resource_path('lang/' . $lang['key'] . '.json.old'))) {
                File::move(resource_path('lang/' . $lang['key'] . '.json.old'),
                    resource_path('lang/' . $lang['key'] . '.json'));
            }
        }

        foreach (Storage::disk('resources')->allFiles('lang/') as $files) {
            if (strpos($files, '.json') == false) {
                continue;
            }
            if (!in_array($files, $liveJsons)) {
                File::move(resource_path($files), resource_path($files . '.old'));
            }
        }

        $languages = Settings::where('settings_key', '=', 'language')->first();
        $languages->settings_value = json_encode($langArr);
        $languages->save();

        return response()->json(json_decode($languages->settings_value), $this->successStatus);
    }

    public function sendNotifications(Request $request)
    {
        $users = User::all();

        Notification::locale(App::getLocale())->send($users,
            new SystemNotification($request->input('title', __('System Notification')),
                $request->input('icon', 'flaticon-danger'), $request->input('action'),
                $request->input('url', null)));

        return response()->json(['data' => 'OK'], $this->successStatus);
    }

    public function testEmail(Request $request)
    {
        try {
            Mail::to(Auth::user())->sendNow(new TestEmail(Auth::user()));
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], $this->errorStatus);
        }
    }

    public function getNotifications(Request $request, $unread = false)
    {
        $user = Auth::user();

        if (!$unread) {
            if ($request->has('limit')) {
                return $user->notifications()->take($request->input('limit'))->get();
            }
            return $user->notifications;
        }

        if ($request->has('limit')) {
            return $user->unreadNotifications()->take($request->input('limit'))->get();
        }
        return $user->unreadNotifications;
    }

    public function readNotifications(Request $request, $id = 0)
    {
        $user = Auth::user();
        if ($id == 0) {
            $user->unreadNotifications->markAsRead();
        } else {
            foreach ($user->unreadNotifications as $notification) {
                if ($notification->id == $id) {
                    $notification->markAsRead();
                }
            }
        }

        return $user->notifications;
    }

    public function getSettings(Request $request)
    {
        $settingsConfig = config('default-settings');
        foreach ($settingsConfig as $sf) {
            Settings::firstOrCreate([
                'settings_key' => $sf
            ]);
        }

        $settings = Settings::whereIn('settings_key', $settingsConfig)->get();

        return response()->json($settings, $this->successStatus);
    }

    public function setSettings(Request $request)
    {
        foreach ($request->input('settings') as $setting) {
            $settings = Settings::where('settings_key', '=', $setting['settings_key'])->first();
            $settings->settings_value = $setting['settings_value'];
            $settings->save();
        }

        return \response()->json(['data' => 'OK'], $this->successStatus);
    }

    public function getMaintenanceMode(Request $request)
    {
        $down = file_exists(storage_path('framework/down'));

        return \response()->json(['data' => $down], $this->successStatus);
    }

    public function setMaintenanceMode(Request $request)
    {
        $down = file_exists(storage_path('framework/down'));
        $users = User::all();

        if ($down) {
            Artisan::call('up');
            $response = Artisan::output();
            Notification::locale(App::getLocale())->send($users,
                new SystemNotification($request->input('title', __('Maintenance mode turned off!')),
                    $request->input('icon', 'flaticon-danger'), $request->input('action'),
                    $request->input('url', null)));
        } else {
            Artisan::call('down', ['--message' => $request->input('message'), '--retry' => 60]);
            $response = Artisan::output();
            Notification::locale(App::getLocale())->send($users,
                new SystemNotification($request->input('title', __('Maintenance mode turned on!')),
                    $request->input('icon', 'flaticon-danger'), $request->input('action'),
                    $request->input('url', null)));
        }

        return response()->json(['data' => $response], $this->successStatus);
    }

    public function getPhpModules(Request $request)
    {
        $modules = [];

        $modules['php_version'] = phpversion();
        $modules['post_max_size'] = ini_get('post_max_size');
        $modules['memory_limit'] = ini_get('memory_limit');
        $modules['upload_max_filesize'] = ini_get('upload_max_filesize');
        $modules['opcache'] = ini_get('opcache.enable');
        $modules['memcached'] = extension_loaded('memcached');
        $modules['redis'] = extension_loaded('redis');
        $modules['environment'] = config('app.env');
        $modules['debug'] = config('app.debug');
        $modules['cache_driver'] = config('cache.default');
        $modules['queue_driver'] = config('queue.default');
        $modules['session_driver'] = config('session.driver');
        $modules['laravel_version'] = App::version();
        $modules['email_config'] = config('mail');
        $modules['database_config'] = config('database');
        $modules['sql_mode'] = DB::select(DB::raw('SELECT @@sql_mode;'))[0]->{'@@sql_mode'};

        return response()->json($modules, $this->successStatus);
    }

    public function getSystemModules(Request $request)
    {
        $modules = [];
        $modules['enabled'] = Module::allEnabled();
        $modules['disabled'] = Module::allDisabled();

        return response()->json($modules, $this->successStatus);
    }
}
