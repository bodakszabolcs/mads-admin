<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Modules\System\Http\Controllers', 'prefix' => 'system', 'middleware' => ['auth:sanctum', 'accesslog', 'role']], function () {
    Route::post('/run-command', 'SystemController@runCommand')->name('Run Artisan command');

    Route::get('/application-data', 'SystemController@getApplicationData')->name('Get application data');
    Route::post('/application-data', 'SystemController@setApplicationData')->name('Set application data');

    Route::post('/languages', 'SystemController@setLanguages')->name('Set system languages');

    Route::post('/send-notifications', 'SystemController@sendNotifications')->name('Send system notifications');

    Route::get('/settings', 'SystemController@getSettings')->name('Get system settings');
    Route::post('/settings', 'SystemController@setSettings')->name('Set system settings');

    Route::get('/maintenance-mode', 'SystemController@getMaintenanceMode')->name('Get maintenance mode');
    Route::post('/maintenance-mode', 'SystemController@setMaintenanceMode')->name('Set maintenance mode');

    Route::get('/php-modules', 'SystemController@getPhpModules')->name('Get php modules');
    Route::get('/system-modules', 'SystemController@getSystemModules')->name('Get system modules');

    Route::post('/test-email', 'SystemController@testEmail')->name('Test email function');
    Route::post('/push-notification', 'SystemController@pushNotification')->name('Push notification test');
});

Route::group(['namespace' => 'Modules\System\Http\Controllers', 'prefix' => 'system', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/get-notifications/{unread?}', 'SystemController@getNotifications');
    Route::get('/read-notification/{id?}', 'SystemController@readNotifications');
});

Route::group(['namespace' => 'Modules\System\Http\Controllers', 'prefix' => 'system'], function () {
    Route::get('/languages', 'SystemController@getLanguages')->name('Get system languages');
});
