<?php

namespace Modules\Category\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Category\Entities\Base\BaseCategory;
use Spatie\Translatable\HasTranslations;

class Category extends BaseCategory
{
    use SoftDeletes, Cachable;
}
