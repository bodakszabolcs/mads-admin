<?php

namespace Modules\Category\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Category\Entities\Base\BaseTag;


class Tag extends BaseTag
{
    use SoftDeletes, Cachable;

}
