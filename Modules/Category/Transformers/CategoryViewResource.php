<?php

namespace Modules\Category\Transformers;

use App\Http\Resources\BaseResource;

class CategoryViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'parent_name' => optional($this->parent)->name,
            'parent_id' => $this->parent_id,
            'parent' => $this->parent,
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'og_image' => $this->og_image
        ];
    }
}
