<?php

namespace Modules\Category\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\Category\Entities\Category;
use Modules\Category\Http\Requests\CategoryCreateRequest;
use Modules\Category\Transformers\CategoryListResource;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Category\Transformers\CategoryViewResource;

class CategoryController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Category();
        $this->viewResource = CategoryViewResource::class;
        $this->listResource = CategoryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CategoryCreateRequest::class;
        $this->updateRequest = CategoryCreateRequest::class;
    }

    public function getCategories(Request $request)
    {
        $category = Category::where('id','>',0);

        if ($request->input('id',0) != 0) {
            $category = $category->where('id','!=',$request->input('id'));
        }

        return response()->json(['data' => $category->pluck('name','id')], $this->successStatus);
    }

    public function getCategoryTree(Request $request)
    {
        return CategoryTreeResource::collection(Category::whereNull('parent_id')->get());
    }

    public function destroy($id, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrfail();
            }

            $this->model->where('parent_id','=',$this->model->id)->update(['parent_id' => $this->model->parent_id]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }
}
