<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;

$factory->define(\Modules\Category\Entities\Tag::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->realText(10),
        'slug' => $faker->unique()->slug
    ];
});
