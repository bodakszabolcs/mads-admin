<?php

namespace Modules\ModuleBuilder\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     *
     *
     * moduleName:ArticleModule
    collection:articles
    route:/article
    middlewares[]:api
    middlewares[]:auth:sanctum
    validation[CREATE][title][]:required
    validation[CREATE][slug][]:required
    validation[UPDATE][title][]:numeric
    validation[UPDATE][slug][]:required
    options[softdeletes]:1
    options[fillable][]:title
    options[fillable][]:slug
    options[fillable][]:date
    options[searchColumns][title]:text
    options[searchColumnLabels][title]:Cím
    options[connections][0][type]:belongsto
    options[connections][0][name]:owner
    options[connections][0][model]:Modules\User\Entities\User
    options[connections][0][key1]:user_id
    options[connections][0][key2]:id
    options[resource][]:id
    inputs[panel1][0][row][0][col-md-6][0][name]:name
    inputs[panel1][0][row][0][col-md-6][0][id]:form-name
    inputs[panel1][0][row][0][col-md-6][0][class]:form-control form-input
    inputs[panel1][0][row][0][col-md-6][0][type]:text
    inputs[panel1][0][row][0][col-md-6][0][placeholder]:Placeholder name
    inputs[panel1][0][row][0][col-md-6][0][label]:Label
    inputs[panel1][0][row][0][col-md-6][0][helpText]:name helpText
    inputs[panel1][0][row][1][col-md-6][1][name]:email
    inputs[panel1][0][row][1][col-md-6][1][id]:form-email
    inputs[panel1][0][row][1][col-md-6][1][class]:form-control form-input
    inputs[panel1][0][row][1][col-md-6][1][type]:email
    inputs[panel1][0][row][1][col-md-6][1][placeholder]:Placeholder email
    inputs[panel1][0][row][1][col-md-6][1][label]:email Label
    inputs[panel1][0][row][1][col-md-6][1][helpText]:email helpText
    inputs[panel1][1][break]: 1
    inputs[panel1][2][row][0][col-md-6][0][name]:user_id
    inputs[panel1][2][row][0][col-md-6][0][id]:form-user_id
    inputs[panel1][2][row][0][col-md-6][0][class]:form-control form-input select2
    inputs[panel1][2][row][0][col-md-6][0][type]:select
    inputs[panel1][2][row][0][col-md-6][0][label]:userid Label
    inputs[panel1][2][row][0][col-md-6][0][helpText]:userid helpText
    inputs[panel1][2][row][0][col-md-6][0][options][0]:Option 1
    inputs[panel1][2][row][0][col-md-6][0][options][1]:Option 2
    inputs[panel1][2][row][1][col-md-6][1][name]:description
    inputs[panel1][2][row][1][col-md-6][1][id]:form-description
    inputs[panel1][2][row][1][col-md-6][1][class]:form-control form-input wyswyg
    inputs[panel1][2][row][1][col-md-6][1][type]:textarea
    inputs[panel1][2][row][1][col-md-6][1][label]:desc Label
    inputs[panel1][2][row][1][col-md-6][1][helpText]:desc helpText
    form:m-form m-form--state
     *
     * @return array
     */
    public function rules()
    {
        return [
            'moduleName' => 'required',
            'collection' => 'required',
            'route' => 'required',
            'middlewares' => 'required|array',
            'validation.*' => 'array',
            'validation.CREATE.*' => 'array',
            'validation.UPDATE.*' => 'array',
            'options' => 'array',
            'options.fillable' => 'array',
            'options.searchColumns' => 'array',
            'options.searchColumnLabels' => 'array',
            'fields' => 'array',
            'form' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
