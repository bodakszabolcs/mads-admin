<?php

namespace Modules\ModuleBuilder\Providers;

use App\Providers\ModuleServiceProvider;

class ModuleBuilderServiceProvider extends ModuleServiceProvider
{
    protected $module = 'modulebuilder';
    protected $directory = __DIR__;

}
