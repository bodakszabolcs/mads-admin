<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\ModuleBuilder\Http\Controllers',
    'prefix' => '/module-builder',
    'middleware' => ['auth:sanctum', 'role', 'accesslog']
], function () {
    Route::post('/create', 'ModuleBuilderController@create')->name('adminModuleCreate');
});

Route::group([
    'namespace' => 'Modules\ModuleBuilder\Http\Controllers',
    'prefix' => '/module-builder',
], function () {
    Route::match(['get'], '/credentials', 'ModuleBuilderController@credentials')->name('ModuleBuilder Credentials');

});
Route::group(['namespace' => 'Modules\ModuleBuilder\Http\Controllers', 'prefix' => 'module-builder/settings', 'middleware' => ['api']], function ()
{
    Route::get('/get-middlewares', 'ModuleBuilderController@getMiddlewares')->name('adminGetMiddlewares');
    Route::get('/get-models', 'ModuleBuilderController@getModels')->name('adminGetModels');
});
