<?php

namespace Modules\Payroll\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PayrollViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "name" => $this->name,
		    "tax" => $this->tax,
		    "month" => $this->month,
		    "gross" => $this->gross,
		    "szja" => $this->szja,
		    "deduction" => $this->deduction,
		    "part_ticket" => $this->part_ticket,
		    "monthly" => $this->monthly,
		    "net" => $this->net,
		    "payable" => $this->payable,
		"selectables" => [
		]
		     ];
    }
}
