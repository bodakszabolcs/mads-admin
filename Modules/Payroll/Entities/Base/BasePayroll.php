<?php

namespace Modules\Payroll\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BasePayroll extends BaseModel
{


    protected $table = 'payrolls';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [

           [
                'name' => 'month',
                'title' => 'Év/Hónap',
                'type' => 'text',
            ],
            [
                'name' => 'search',
                'title' => 'Keresés',
                'type' => 'text',
            ],
        ];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['student_id','name','tax','month','gross','szja','deduction','part_ticket','monthly','net','payable'];

    protected $casts = [];


}
