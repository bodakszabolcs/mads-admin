<?php

namespace Modules\Payroll\Entities;

use Modules\Payroll\Entities\Base\BasePayroll;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Payroll extends BasePayroll
{
    use SoftDeletes, Cachable;


    public static $underAgeLimit=656785;




    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
