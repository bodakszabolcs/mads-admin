<?php

return [
    'name' => 'Payroll',

                 'menu_order' => 41,

                 'menu' => [
                     [

                      'icon' =>'fa fa-file-word',

                      'title' =>'Bérelszámolási lapok',

                      'route' =>'/'.env('ADMIN_URL').'/payroll/index',

                     ]

                 ]
];
