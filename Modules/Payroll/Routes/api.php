<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Modules\Payroll\Http\Controllers',
    'prefix' => '/payroll',
    'middleware' => ['auth:sanctum','role']
], function () {
    Route::match(['post'], 'import', 'PayrollController@import')->name('Import payroll');
});
