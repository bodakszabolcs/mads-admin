<?php

namespace Modules\Payroll\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayrollCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
			'name' => 'required',
			'tax' => 'required',
			'month' => 'required',
			'gross' => 'required',
			'szja' => 'required',
			'deduction' => 'required',
			'part_ticket' => 'required',
			'monthly' => 'required',
			'net' => 'required',
			'payable' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'student_id' => __('Student'),
'name' => __('Név'),
'tax' => __('Adószám'),
'month' => __('Hónap'),
'gross' => __('Bruttó'),
'szja' => __('SZJA'),
'deduction' => __('Levonás'),
'part_ticket' => __('Részjegy'),
'monthly' => __('Hóközi kifizetés'),
'net' => __('Nettó'),
'payable' => __('Fizetendő'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
