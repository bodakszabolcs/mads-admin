<?php

namespace Modules\Payroll\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Imports\MaterialImport;
use App\Imports\PayrollImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Material\Entities\Product;
use Modules\Payroll\Entities\Payroll;
use Illuminate\Http\Request;
use Modules\Payroll\Http\Requests\PayrollCreateRequest;
use Modules\Payroll\Http\Requests\PayrollUpdateRequest;
use Modules\Payroll\Transformers\PayrollViewResource;
use Modules\Payroll\Transformers\PayrollListResource;

class PayrollController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Payroll();
        $this->viewResource = PayrollViewResource::class;
        $this->listResource = PayrollListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PayrollCreateRequest::class;
        $this->updateRequest = PayrollUpdateRequest::class;
    }

    public function import(Request  $request){
        try{



            // /storage/public/files/public/files/shares/hDhRBnx8Tl8cRmcBdpo3cRftlCg5c3eW8WJCrXBH.xlsx
            // /storage/app/public/public/files/shares/hDhRBnx8Tl8cRmcBdpo3cRftlCg5c3eW8WJCrXBH.xlsx
            Excel::import(new PayrollImport(),$request->input('file'),'s3');


            return  response()->json(['data'=> 'Sikeres importálás']);
        }
        catch (\Exception $e){

            return response()->json(['data'=> 'Hiba az importálás során: '.json_encode($e->getMessage())]);
        }
    }

}
