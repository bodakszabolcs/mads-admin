<?php

namespace Modules\Payroll\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Payroll\Entities\Payroll;
use Modules\Payroll\Observers\PayrollObserver;

class PayrollServiceProvider extends ModuleServiceProvider
{
    protected $module = 'payroll';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Payroll::observe(PayrollObserver::class);
    }
}
