import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Payroll from '../components/Payroll'
import PayrollList from '../components/PayrollList'
import PayrollCreate from '../components/PayrollCreate'
import PayrollEdit from '../components/PayrollEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'payroll',
                component: Payroll,
                meta: {
                    title: 'Payrolls'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PayrollList',
                        component: PayrollList,
                        meta: {
                            title: 'Payrolls',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/payroll/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/payroll/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PayrollCreate',
                        component: PayrollCreate,
                        meta: {
                            title: 'Create Payrolls',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/payroll/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PayrollEdit',
                        component: PayrollEdit,
                        meta: {
                            title: 'Edit Payrolls',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/payroll/index'
                        }
                    }
                ]
            }
        ]
    }
]
