<?php

namespace Modules\Payroll\Observers;

use Modules\Payroll\Entities\Payroll;

class PayrollObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function saved(Payroll $model)
    {
        Payroll::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function created(Payroll $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function updated(Payroll $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function deleted(Payroll $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function restored(Payroll $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Payroll\Entities\Payroll  $model
     * @return void
     */
    public function forceDeleted(Payroll $model)
    {
        //
    }
}
