<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('payrolls');
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("student_id")->unsigned()->nullable();
$table->text("name")->nullable();
$table->text("tax")->nullable();
$table->integer("month")->unsigned()->nullable();
$table->integer("gross")->unsigned()->nullable();
$table->integer("szja")->unsigned()->nullable();
$table->integer("deduction")->unsigned()->nullable();
$table->integer("part_ticket")->unsigned()->nullable();
$table->text("monthly")->nullable();
$table->integer("net")->unsigned()->nullable();
$table->text("payable")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
