<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Payroll\Entities\Payroll;

$factory->define(Payroll::class, function (Faker $faker) {
    return [
        "student_id" => rand(1000,5000),
"name" => $faker->realText(),
"tax" => $faker->realText(),
"month" => rand(1000,5000),
"gross" => rand(1000,5000),
"szja" => rand(1000,5000),
"deduction" => rand(1000,5000),
"part_ticket" => rand(1000,5000),
"monthly" => $faker->realText(),
"net" => rand(1000,5000),
"payable" => $faker->realText(),

    ];
});
