<?php

namespace Modules\School\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\School\Entities\School;
use Illuminate\Http\Request;
use Modules\School\Http\Requests\SchoolCreateRequest;
use Modules\School\Http\Requests\SchoolUpdateRequest;
use Modules\School\Transformers\SchoolViewResource;
use Modules\School\Transformers\SchoolListResource;

class SchoolController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new School();
        $this->viewResource = SchoolViewResource::class;
        $this->listResource = SchoolListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = SchoolCreateRequest::class;
        $this->updateRequest = SchoolUpdateRequest::class;
    }

}
