import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import School from '../components/School'
import SchoolList from '../components/SchoolList'
import SchoolCreate from '../components/SchoolCreate'
import SchoolEdit from '../components/SchoolEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'school',
                component: School,
                meta: {
                    title: 'Schools'
                },
                children: [
                    {
                        path: 'index',
                        name: 'SchoolList',
                        component: SchoolList,
                        meta: {
                            title: 'Schools',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/school/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/school/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'SchoolCreate',
                        component: SchoolCreate,
                        meta: {
                            title: 'Create Schools',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/school/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'SchoolEdit',
                        component: SchoolEdit,
                        meta: {
                            title: 'Edit Schools',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/school/index'
                        }
                    }
                ]
            }
        ]
    }
]
