<?php

namespace Modules\School\Observers;

use Modules\School\Entities\School;

class SchoolObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function saved(School $model)
    {
        School::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function created(School $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function updated(School $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function deleted(School $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function restored(School $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\School\Entities\School  $model
     * @return void
     */
    public function forceDeleted(School $model)
    {
        //
    }
}
