<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\School\Entities\School;

$factory->define(School::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"address" => $faker->realText(),

    ];
});
