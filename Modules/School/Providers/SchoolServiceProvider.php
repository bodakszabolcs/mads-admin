<?php

namespace Modules\School\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\School\Entities\School;
use Modules\School\Observers\SchoolObserver;

class SchoolServiceProvider extends ModuleServiceProvider
{
    protected $module = 'school';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        School::observe(SchoolObserver::class);
    }
}
