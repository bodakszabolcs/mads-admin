<?php

	namespace Modules\Project\Http\Controllers;
	use App\Exports\BaseArrayExport;
    use App\Helpers\ConstansHelper;
	use App\Http\Controllers\AbstractLiquidController;
	use Illuminate\Console\Scheduling\Schedule;
	use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Str;
    use Modules\Project\Entities\Project;
	use Illuminate\Http\Request;
	use Modules\Project\Entities\ProjectSchedule;
    use Modules\Project\Entities\ProjectScheduleInfo;
    use Modules\Project\Http\Requests\ProjectCreateRequest;
	use Modules\Project\Http\Requests\ProjectUpdateRequest;
	use Modules\Project\Transformers\ProjectStudentResource;
	use Modules\Project\Transformers\ProjectViewResource;
	use Modules\Project\Transformers\ProjectListResource;
    use Modules\Student\Entities\Student;
    use Modules\Student\Http\Requests\ScheduleCreateRequest;
	class ProjectScheduleController extends AbstractLiquidController
	{
		public function __construct(Request $request)
		{
			parent::__construct($request);
			$this->model = new Project();
			$this->viewResource = ProjectViewResource::class;
			$this->listResource = ProjectListResource::class;
			$this->useResourceAsCollection = true;
			$this->createRequest = ProjectCreateRequest::class;
			$this->updateRequest = ProjectUpdateRequest::class;
		}
        public function exportShedule(Request $request,$id,$dep){
            $date = $request->input('interval');
            $date = explode(' - ',$date);
            $start =  date('Y-m-d',strtotime($date[0]));
            $start2 =  date('Y-m-d',strtotime($date[0]));
            $end = date('Y-m-d',strtotime($date[1]));

            $days =[];
            $tableArray=[];
            $schedulesCount = ProjectSchedule::where('schedule_date','>=',$start)
                ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone'))
                ->where('schedule_date','<=',$end)->where('project_id',$id)->where("department",$dep)
               ->orderBy('student_name')->groupBy('student_id')->get();
            foreach ($schedulesCount as $c){
                $tableArray[$c->student_id]= [];
                $tableArray[$c->student_id]['name']=$c->student_name;
                $tableArray[$c->student_id]['phone']=$c->student_phone;
                $i =0;
                $start =  date('Y-m-d',strtotime($date[0]));
                while($i<7) {
                    $day = date('Y-m-d', strtotime($start));
                    $tableArray[$c->student_id][$day]='';
                    $start = date('Y-m-d',strtotime($start.' +1 day'));
                    $i++;
                }
            }
            $i=0;
            $start =  date('Y-m-d',strtotime($date[0]));
            while($i<7){

                $day =date('Y-m-d',strtotime($start));
                $days[str_replace("<br>",", ",ConstansHelper::formatDateShort($day))] =str_replace("<br>",", ",ConstansHelper::formatDateShort($day));

                $schedules = ProjectSchedule::where('schedule_date','=',$day)
                    ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone'))
                    ->where('project_id',$id)->where("department",$dep)
                    ->orderBy('student_name')->orderBy('schedule_date')->get();
                foreach ($schedules as $s){

                    $tableArray[$s->student_id][$day]= ConstansHelper::formatTime($s->start).'-'.ConstansHelper::formatTime($s->end);
                }
                $start = date('Y-m-d',strtotime($start.' +1 day'));

                $i++;
            }

            $export = new BaseArrayExport(['Diák','Telefon']+$days, "beosztás", $tableArray);

            return $export->download(Str::slug('Beosztas') . '.xlsx');


        }
        public function exportScheduleGroup(Request $request,$id){
            $date = $request->input('interval');
            $date = explode(' - ',$date);;
            $response =[];

            $project = Project::where('id',$id)->first();
            $days =[];
            $tableArray=[];
            $departments = explode(";",$project->departments);
            $empty=[];
            $empty['department']='';
            foreach ($departments as $d){
                $tableArray[str_slug($d)]=[];

                $i =0;
                $start =  date('Y-m-d',strtotime($date[0]));

                while($i<7) {
                    $empty[$start]='';
                    $day = date('Y-m-d', strtotime($start));
                    $days[str_replace("<br>",", ",ConstansHelper::formatDateShort($day))] =str_replace("<br>",PHP_EOL,ConstansHelper::formatDateShort($day));
                    $schedules = ProjectSchedule::where('schedule_date','=',$day)
                        ->where('project_id',$id)->where("department",$d) ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone'))
                       ->orderBy('start')->get();
                    foreach ($schedules as $s){
                        $tableArray[str_slug($d)][$s->schedule_date][]=$s->student_name.PHP_EOL.implode(':',array_slice(explode(':',$s->start),0,2)).'-'.implode(':',array_slice(explode(':',$s->end),0,2));;
                    }
                    $start = date('Y-m-d',strtotime($start.' +1 day'));
                    $i++;
                }

            }

            $j =0;
            foreach ($tableArray as $k=>$v){
                $response[$j]=$empty;
                $response[$j]['department']=$this->findDepartment($project->departments,$k);
                foreach ($v as $responseKey =>$b){

                    foreach ($b as $c =>$value){
                        if(!isset($response[$j+$c])){
                            $response[$j+$c]=$empty;

                        }
                        $response[$j+$c][$responseKey]=$value;

                    }


                }
                $j=sizeof($response);


            }



            $export = new BaseArrayExport(['Részleg']+$days, "beosztás", $response);

            return $export->download(Str::slug('Beosztas') . '.xlsx');


        }
        private function findDepartment($deps,$slug){
            $d = explode(';',$deps);
            foreach ($d as $a=>$b){
                if(str_slug($b)==$slug){
                    return $b;
                }
            }
            return "";
        }
        public function createScheduleLink(Request $request,$id){
            $project = Project::where('id',$id)->first();
            $data=[];
            $data['project_id'] = $id;
            $data['interval'] =$request->input('interval');;
            $data['industry_id'] = $project->industry_id;
            $data['departmen'] = $request->input('departmen');
            $data['expire'] = $request->input('expire');
            $data['students'] = $project->projectStudent()->wherePivot('status_id', 2)->pluck('student_id','student_id')->toArray();
            $link = base64_encode(json_encode($data));
            $students = Student::whereIn('id',$data['students'])->get();
            foreach ($students as $s){
                try {

                    Mail::to($s->email)->send(new \App\Mail\SendScheduleEmail($link,$s));
                }catch (\Exception $e){
                        dd($e);
                }
            }
            return response()->json(['data'=>$link]);
        }
		public function getSchedule(Request $request,$id){
			$date = $request->input('interval');
			$date = explode(' - ',$date);
			$start =  date('Y-m-d',strtotime($date[0]));
			$end = date('Y-m-d',strtotime($date[1]));
            $project = Project::where('id',$id)->first();
            $deps = explode(";",$project->departments);
			$days =[];
            $schedulesArrayInfo=[];
			$i=0;
			while($i<7){
				$day =date('Y-m-d',strtotime($start));
				$days[$day]=[
					'value'=>$day,
					'formatted' => ConstansHelper::formatDateShort($day),
					'name'=>ConstansHelper::formatDateShort($day)
				];
                foreach ( $deps as $d){
                    $schInfo= ProjectScheduleInfo::where("project_id",$project->id)->where("department",($d)?Str::slug(trim($d),'-'):null)->where("schedule_date",$day)->first();
                    $schedulesArrayInfo[Str::slug(trim($d),'-')][$day] =[
                      'id'=>$schInfo?$schInfo->id:null,
                      'project_id'=>$project->id,
                      'industry_id'=>$project->industry_id,
                      'schedule_date'=>$day,
                      'department'=>Str::slug(trim($d),'-'),
                      'start'=>$schInfo?$schInfo->start:'08:00',
                      'end'=>$schInfo?$schInfo->end:'16:00',
                      'student_count'=>  $schInfo?$schInfo->student_count:null,
                      'info'=>  $schInfo?$schInfo->info:null,

                    ];
                }
				$start = date('Y-m-d',strtotime($start.' +1 day'));
				$i++;
			}

			$schedules = ProjectSchedule::where('schedule_date','>=',$date[0])
				->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone'))
				->where('schedule_date','<=',$end)->where('project_id',$id)
				->orderBy('department')->orderBy('student_id')->orderBy('schedule_date')->get();
			$schedulesArray=[];

			foreach ($schedules as $sc){

				$schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id][$sc->schedule_date]=[
					"end"=> ConstansHelper::formatTime($sc->end),
					"have_time"=> true,
					"schedule_date"=> $sc->schedule_date,
					"start"=> ConstansHelper::formatTime($sc->start),
				];
				$schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['name']=$sc->student_name;
				$schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['student_id']=$sc->student_id;
				$schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['phone']=$sc->student_phone;

			}
			return response()->json([
				'title' => str_replace('<br>',' ',ConstansHelper::formatDateShort($date[0]).'  -  '.ConstansHelper::formatDateShort($date[1])),
				'days' => $days,
				'schedules' => $schedulesArray,
				'schedulesInfo' => $schedulesArrayInfo,
				'students' => ProjectStudentResource::collection($project->projectStudent()->wherePivot('status_id', 2)->get())
			],$this->successStatus);

		}
        public function saveInfo(Request $request){
            $pi = new ProjectScheduleInfo();
            if($request->has("id") && $request->input("id")!= null){
                $pi =ProjectScheduleInfo::where("id",$request->input("id"))->first();
            }
            $pi->fill($request->all());
            $pi->save();
            return response()->json("ok");
        }
		public function saveSchedule(ScheduleCreateRequest $request){
			try
			{
				$project = Project::where('id','=',$request->input('project_id'))->firstOrFail();
				foreach ($request->input('schedules',[]) as $sch ){

					$hasSchedule = ProjectSchedule::where('project_id',$project->id)
						->where('schedule_date',$sch['schedule_date'])
                        ->where('department',$request->input('department',''))
						->where('student_id',$request->input('student_id'))->first();
					if($hasSchedule && $sch['have_time'] == false){
						$hasSchedule->delete();
					}
					if(!$hasSchedule){
						$hasSchedule = new ProjectSchedule();
					}
					if($sch['have_time']){
						$hasSchedule->project_id = $project->id;
						$hasSchedule->student_id = $request->input('student_id');
						$hasSchedule->industry_id = $project->industry_id;
						$hasSchedule->department = $request->input('department','');
						$hasSchedule->schedule_date =$sch['schedule_date'];
						$hasSchedule->start =$sch['start'];
						$hasSchedule->end =$sch['end'];
						$hasSchedule->save();

					}
				}
				return  response()->json($this->successMessage,$this->successStatus);
			}
			catch (ModelNotFoundException $e){
				return response()->json(['message'=>'Ismeretlen biba'],422);
			}
		}

	}

