<?php

	namespace Modules\Project\Http\Controllers;
	use App\Exports\BaseArrayExport;
    use App\Helpers\PdfHelper;
    use App\Http\Controllers\AbstractLiquidController;
    use App\Jobs\SaveStatistic;
    use Google\Service\BackupforGKE\Schedule;
    use Google\Type\Date;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
	use Illuminate\Support\Arr;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Response;
    use Illuminate\Support\Str;
    use Modules\Category\Entities\Category;
    use Modules\Category\Transformers\CategoryListResource;
    use Modules\Company\Entities\Company;
    use Modules\Company\Entities\Forms;
    use Modules\Company\Entities\Industry;
    use Modules\Fixing\Entities\Fixing;
    use Modules\Project\Entities\Project;
	use Illuminate\Http\Request;
    use Modules\Project\Entities\ProjectContact;
    use Modules\Project\Entities\ProjectRealization;
    use Modules\Project\Entities\ProjectSchedule;
    use Modules\Project\Entities\ProjectStudent;
    use Modules\Project\Entities\ProjectStudentAnswer;
    use Modules\Project\Entities\ProjectTask;
	use Modules\Project\Http\Requests\ProjectCreateRequest;
    use Modules\Project\Http\Requests\ProjectRealizationRequest;
    use Modules\Project\Http\Requests\ProjectUpdateRequest;
	use Modules\Project\Transformers\ProjectDetailsResource;
	use Modules\Project\Transformers\ProjectStudentResource;
	use Modules\Project\Transformers\ProjectTaskResource;
	use Modules\Project\Transformers\ProjectViewResource;
	use Modules\Project\Transformers\ProjectListResource;
    use Modules\Student\Entities\Student;
    use Modules\User\Entities\User;
    use Modules\Work\Entities\Work;
    use Modules\Work\Transformers\WorkListResource;
    use Modules\WorkCategory\Entities\WorkCategory;
    use Modules\WorkCategory\Transformers\WorkCategoryListResource;
    use Mpdf\Mpdf;

    class ProjectController extends AbstractLiquidController
	{
		public function __construct(Request $request)
		{
			parent::__construct($request);
			$this->model = new Project();
			$this->viewResource = ProjectViewResource::class;
			$this->listResource = ProjectListResource::class;
			$this->useResourceAsCollection = true;
			$this->createRequest = ProjectCreateRequest::class;
			$this->updateRequest = ProjectUpdateRequest::class;
		}
        public function create(Request $request)
        {

            if ($this->createRequest) {
                app()->make($this->createRequest);
            }
            $this->model->employees_change = date('Y-m-d H:i:s');
            $this->model->fillAndSave($request->all());
            SaveStatistic::dispatch($request->input("user_id"),'new_project');
            SaveStatistic::dispatch($request->input("user_id"),'new_order');
            return new $this->viewResource($this->model);
        }
        public function update(Request $request, $id, $auth = null)
        {
            if ($this->updateRequest) {
                app()->make($this->updateRequest);
            }

            $this->model = $this->model->where('id', '=', $id);

            try {
                if ($auth == null) {
                    $this->model = $this->model->firstOrFail();
                } else {
                    $this->model = $this->model->where('user_id', '=', $auth)->firstOrFail();
                }
                if($this->model->user_id != $request->input("user_id")){
                    SaveStatistic::dispatch($request->input("user_id"),'new_project');
                }
                if($this->model->number_of_employees <= $request->input("number_of_employees")){
                    SaveStatistic::dispatch($request->input("user_id"),'new_order');
                }
                if($this->model->number_of_employes !== $request->input('number_of_employees')){
                    $this->model->employees_change = date('Y-m-d H:i:s');
                }

            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $p = Project::where('id',$id)->first();
            $ps = ProjectStudent::where('status_id',2)->where('project_id',$id)->get();
            if($p->number_of_employees <= sizeof($ps)){
                $p->completion_date = date('Y-m-d H:i:s');
            }else{
                $p->completion_date = null;
            }
            $this->model->fillAndSave($request->all());

            return new $this->viewResource($this->model);
        }
        public function changeNumberOfEmloyees(Request $request,$id){
            $this->model = $this->model->where('id', '=', $id);
            try {

                $this->model = $this->model->firstOrFail();

                if($this->model->number_of_employees <= $request->input("number_of_employees")){
                    SaveStatistic::dispatch($request->input("user_id"),'new_order');
                }
                if($this->model->number_of_employes !== $request->input('number_of_employees')){
                    $this->model->employees_change = date('Y-m-d H:i:s');
                }
                $this->model->number_of_employees= $request->input("number_of_employees");


                $ps = ProjectStudent::where('status_id',2)->where('project_id',$id)->get();
                if($this->model->number_of_employees <= sizeof($ps)){
                    $this->model->completion_date = date('Y-m-d H:i:s');
                }else{
                    $this->model->completion_date = null;
                }
                $this->model->save();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
        }

        public function getCompanyProjects(Request $request,$company){
            $list = Project::leftJoin('companies','companies.id','=','company_id')
                ->leftJoin('company_industries','company_industries.id','=','industry_id')
                ->leftJoin('users','users.id','=','projects.user_id')
                ->leftJoin(DB::raw('(select project_id,count(distinct student_id) as worker from project_students left join students on students.id = student_id where status_id=2 and students.deleted_at is null and students.id is not null group by project_id ) as work'),'work.project_id','=','projects.id')
                ->select(DB::raw('projects.*,companies.name as company_name,company_industries.name as industry_name, concat(users.firstname,\' \',users.lastname) as user_name,worker,if(number_of_employees>0,worker/number_of_employees,1) as completion'))
                ->where('companies.id','=',$company);
            $listActive = clone $list;
            $listActive2 = clone $list;
            $listInactive = clone $list;
            $listCompleted = clone $list;
            $listActive = $listActive->where(function($query) {
                $query->where("number_of_employees", ">", DB::raw("worker"))
                    ->orWhereNull("number_of_employees")
                    ->orWhereNull("worker");
            })->get();;
            $listActive2=$listActive2->where("number_of_employees","<=",DB::raw("worker"))->get();
            $listInactive = $listInactive->where('status',1)->get();
            $listCompleted = $listCompleted->where('status',2)->get();

            return \response()->json([
                'active' => $this->listResource::collection($listActive),
                'active2' => $this->listResource::collection($listActive2),
                'inactive' => $this->listResource::collection($listInactive),
                'completed' => $this->listResource::collection($listCompleted)
            ]);
        }
        public function index(Request $request, $auth = null)
		{

			$list = Project::leftJoin('companies','companies.id','=','company_id')
                ->leftJoin('company_industries','company_industries.id','=','industry_id')
                ->leftJoin('users','users.id','=','projects.user_id')
                ->leftJoin(DB::raw('(select project_id,count(distinct student_id) as worker from project_students left join students on students.id = student_id where status_id=2 and students.deleted_at is null and students.id is not null group by project_id ) as work'),'work.project_id','=','projects.id')
                ->select(DB::raw('projects.*,companies.name as company_name,company_industries.name as industry_name, concat(users.firstname,\' \',users.lastname) as user_name,worker,if(number_of_employees>0,worker/number_of_employees,1) as completion'))
                ;
            if($request->input('search')){
                $list = $list->where( function($query) use ($request){
                    $query->where('companies.name','LIKE','%'.$request->input('search').'%')->orWhere('company_industries.name','LIKE','%'.$request->input('search').'%');
                });
            }
            if($request->has('student-diff')){
                if($request->input('student-diff')==1){
                    $list = $list->where(function($query) {
                        $query->where("number_of_employees", ">", DB::raw("worker"))
                            ->orWhereNull("number_of_employees")
                            ->orWhereNull("worker");
                    });
                }else{
                    $list = $list->where("number_of_employees","<=",DB::raw("worker"));
                }

            }
            if($request->has('status')){
                $list = $list->where('status',$request->input('status'));
            }
            if($request->has('completion')){
                if($request->input('completion')==2) {
                    $list = $list->where(DB::raw('if(number_of_employees>0,worker/number_of_employees,1)'),'<',1);
                }
                if($request->input('completion')==1) {
                    $list = $list->where(DB::raw('if(number_of_employees>0,worker/number_of_employees,1)'),'>=',1);
                }
            }
            if($request->has('user_id') && !empty($request->input('user_id'))){
                $list = $list->where('projects.user_id',$request->input('user_id'));
            }
            if($request->has('sort') && !empty($request->input('sort'))){
                $sort = explode('|',$request->input('sort'));
                $list = $list->orderBy($sort[0],$sort[1]);
            }else{
                $list = $list->orderBy('id','desc');
            }

            if (!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {

                $list = $list->where(function ($query) {
                    $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
                });
            }

			if ($this->pagination) {
				$pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
				$list = $list->paginate($pagination)->withPath($request->path());

			} else {
				$list = $list->get();
			}



			if (!$this->useResourceAsCollection) {
				return new $this->collection($list);
			}
            $filters =[
                [
                    'name' => 'search',
                    'title' => 'Keresés',
                    'type' => 'text',
                ],
                [
                    'name' => 'completion',
                    'title' => 'Állapot',
                    'type' => 'select',
                    'data' => ['1'=>'Teljesítve','2'=>'Nincs teljesítve'],
                ],
                [
                    'name' => 'priority',
                    'title' => 'Fontosság',
                    'type' => 'select',
                    'data' => ['0'=>'Normál', '1'=>'Kevésbé fontos','2'=>'Fontos'],
                ]];
            if (!(!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3)){
                $filters=  [
                    [
                        'name' => 'search',
                        'title' => 'Keresés',
                        'type' => 'text',
                    ],
                    [
                        'name' => 'completion',
                        'title' => 'Állapot',
                        'type' => 'select',
                        'data' => ['1'=>'Teljesítve','2'=>'Nincs teljesítve'],
                    ],
                    [
                        'name' => 'priority',
                        'title' => 'Fontosság',
                        'type' => 'select',
                        'data' => ['0'=>'Normál', '1'=>'Kevésbé fontos','2'=>'Fontos'],
                    ],
                    [
                        'name' => 'user_id',
                        'title' => 'Felelős',
                        'type' => 'select',
                        'data' => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id")
                    ]
                    ];

            }
            else{

            }
                return $this->listResource::collection($list)->additional(['filters' => $filters]);
		}

		public function getIndustryByCompany(Request $request, $id)
		{
			return response()->json(Industry::where('company_id', $id)->pluck('name', 'id'), $this->successStatus);
		}
        public function getIndustryByCompanyAndWorker(Request $request, $id)
        {
            $resp =ProjectStudent::leftJoin('projects','projects.id','project_id')->leftJoin('company_industries','company_industries.id','industry_id')->where('projects.company_id','=',$id)->where('status_id','=',2)
                ->whereNull('projects.deleted_at')->groupBy('industry_id')->select(DB::raw("concat(company_industries.name,' -  ', count(*),' diák dolgozik') as name,company_industries.id as id"))->get()->pluck('name', 'id');
            return response()->json($resp, $this->successStatus);
        }
        public function getRealization(Request $request,$id){
              $end = $request->input('date');
              $start = date('Y-m-d',strtotime($end.' - 2 month'));

            $list=ProjectRealization::leftJoin('students','students.id','=','project_realizations.student_id')
                ->select(DB::raw("students.name as name, students.student_number as student_number,project_realizations.*,'' as schedule,sch.prev_department as prev_department"))
                ->leftJoin(DB::raw("(select group_concat(concat(department,' - ',schedule_date),'<br>') as prev_department, student_id from project_schedules where project_id = $id and schedule_date < '$end' and schedule_date > '$start'   group by student_id order by schedule_date) as sch"),'sch.student_id','project_realizations.student_id')
                ->where('date','=',$request->input('date'))->where('project_id','=',$id)->get();

            foreach ($list as $k=>$v){
                $list[$k]['schedule'] = $this->buildSchedule($v);
                foreach ($list[$k]['schedule'] as $s){
                    if($s['start']){
                        $list[$k]['scheduled']=1;
                        break;
                    }
                }
            }
            return response()->json(['data'=>$list]);
        }
        public function getRealizationExcel(Request $request,$id){
            $end = $request->input('date');
            $start = date('Y-m-d',strtotime($end.' - 2 month'));

            $list=ProjectRealization::leftJoin('students','students.id','=','project_realizations.student_id')
                ->select(DB::raw("students.name as name, students.student_number as student_number,project_realizations.*,'' as schedule,sch.prev_department as prev_department"))
                ->leftJoin(DB::raw("(select group_concat(concat(department,' - ',schedule_date),'<br>') as prev_department, student_id from project_schedules where project_id = $id and schedule_date < '$end' and schedule_date > '$start'   group by student_id order by schedule_date) as sch"),'sch.student_id','project_realizations.student_id')
                ->where('date','=',$request->input('date'))->where('project_id','=',$id)->get();
            $data = [];
            $week=['monday','tuesday','wednesday','thursday','friday','saturday','sunday'];
            $weekHeader=['Hétfő','Kedd','Szerda','Csütörtök','Péntek','Szombat','Vasárnap'];

            $start = Project::where('id',$id)->first()->week_start;
            $week = array_merge(array_slice($week,$start),array_slice($week,0,$start));
            $weekHeader = array_merge(array_slice($weekHeader,$start),array_slice($weekHeader,0,$start));
            foreach ($list as $k=>$v){
                $data[$k]['name'] = $v['name'];
                $data[$k]['student_number'] = $v['student_number'];
                $data[$k]['preferred_departments'] =$v['preferred_departments'];
                $data[$k]['comment'] = $v['comment'];
                $data[$k]['prev_department'] = $v['prev_department'];
                foreach ($week as $k1=>$v1){
                    $data[$k][$v1] = $v[$v1]=='F'?'Egész nap':$v[$v1];
                }


            }
            $export = new BaseArrayExport(array_merge(['Diák','Azonosító','Preferált részleg','Megjegyzés','Előző beosztás'],$weekHeader), "Ráérés", $data);

            return $export->download(Str::slug('Ráérés') . '.xlsx');
        }
        public function saveScheduleRealization(Request $request,$id,$psId){
            $p = Project::where('id',$id)->first();

            $pr =ProjectRealization::where('id',$psId)->first();
            $pr->scheduled=1;
            $pr->save();

            foreach ($request->input('schedule',[]) as $k=>$v){
                if($v['start']) {

                    $s = ProjectSchedule::where('project_id', $id)->where('student_id', $v['student_id'])->where('schedule_date', $v['schedule_date'])->first();
                    if (!$s) {
                        $s = new ProjectSchedule();
                    }
                    $s->fill($v);
                    $s->industry_id = $p->industry_id;
                    $s->department= $this->findDepartment($p->departments,$v['department']);
                    $s->save();
                }
            }
            return response()->json("Ok");

        }
        private function findDepartment($deps,$slug){
            $d = explode(';',$deps);
            foreach ($d as $a=>$b){
                if(str_slug($b)==$slug){
                    return $b;
                }
            }
            return "";
        }
        public function buildSchedule($v){
            $resp = [];
            $start = date('Y-m-d',strtotime($v->date));
            $end = date('Y-m-d',strtotime($v->date.' +7 day'));
            $schedules = ProjectSchedule::where('schedule_date','>=',$start)->where('schedule_date','<=',$end)->where('student_id',$v->student_id)->where('project_id',$v->project_id)->get();
            while($start <=$end){
                $resp[$start]=[
                    'start'=>'',
                    'end'=>'',
                    'schedule_date'=>$start,
                    'department'=>'',
                    'student_id'=>$v->student_id,
                    'project_id'=>$v->project_id
                ];
                $start = date('Y-m-d',strtotime($start.' +1 day'));
            }
            foreach ($schedules as $s){
                $resp[$s->schedule_date]['start']=$s->start;
                $resp[$s->schedule_date]['end']=$s->end;
                $resp[$s->schedule_date]['schedule_date']=$s->schedule_date;
                $resp[$s->schedule_date]['student_id']=$s->student_id;
                $resp[$s->schedule_date]['project_id']=$s->project_id;
                $resp[$s->schedule_date]['department']=str_slug($s->department);
            }

            return $resp;
        }
        public function saveRealizition(ProjectRealizationRequest  $request){
            $realization = ProjectRealization::where('id',$request->input('id'))->first();
            $realization->fill($request->all());
            $realization->save();
            $request->merge(['date'=>$realization->date]);
            return response()->json("ok");
        }
        public function sendRealization(Request $request,$id){
            $projectStudents = ProjectStudent::where('project_id',$id)->where('status_id',2)->get();
            $project = Project::where('id',$id)->first();
            $company = Company::where('id',$project->company_id)->first();
            $user = User::where('id',$project->user_id)->first();
            foreach ($projectStudents as $p){
                $pr = ProjectRealization::where('student_id',$p->student_id)->where('project_id','=',$id)->where('date',$request->input('date'))->first();
                if(!$pr){
                    $pr = new ProjectRealization();
                }
                $pr->date=$request->input('date');
                $pr->project_id=$id;
                $pr->student_id=$p->student_id;
                $pr->save();

                if(!$pr->monday){
                    $s=Student::where('id',$p->student_id)->first();
                    if($s) {
                        $data = [];
                        $data['company_name'] = $company->name;
                        $data['name'] = $s->name;
                        $data['date'] = $pr->date;
                        $data['sender'] = $user->lastname . ' ' . $user->firstname;
                        $data['sender_email'] = $user->email;
                        $data['to'] = $s->email;
                        $data['signature'] = $user->signature;
                        $data['link'] = 'https://mads.hu/profile/realization/' . base64_encode(json_encode(['id' => $pr->id, 'open_modal' => 'true', 'date' => $pr->date]));

                        \App\Jobs\SendRealization::dispatch($data);
                    }

                }
            }
            return $this->getRealization($request,$id);

        }
        public function getStatusList(Request $request){
            $filter=[];
            $list = ProjectStudent::leftJoin('project_status','project_status.id','=','status_id')
                ->leftJoin('projects','project_students.project_id','=','projects.id')
                ->leftJoin('students','students.id','=','project_students.student_id')->whereNull('students.deleted_at')->whereNull('projects.deleted_at')->whereNotNull('students.id')
                ->select(DB::raw("project_status.name as status_name, count(*) as student_count, status_id"))->groupBy("status_id");
            if (!\Auth::user()->isSuperAdmin() &&  \Auth::user()->position!=2 && \Auth::user()->position !=3) {
                $list = $list->where(function ($query) {
                    $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
                });
            }else{
                $filter =['users'=>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name", "id")];
                if($request->has('user_id') && $request->input('user_id')){
                    $user = $request->input('user_id');
                    $list = $list->where(function ($query) use ($user) {
                        $query->where('projects.user_id', '=',$user)->orWhere('projects.replacement_user_id', '=',$user);
                    });
                }
            }
            $list=$list->get()->toArray();
            foreach ($list as $k=>$v) {
                $list[$k]['opened']=false;
                $list[$k]['search']='';
                $l =ProjectStudent::leftJoin('projects','project_students.project_id','=','projects.id')->where('status_id',$v['status_id'])->groupBy("project_id")
                        ->leftJoin('companies','companies.id','=','company_id')
                        ->leftJoin('students','students.id','=','project_students.student_id')->whereNull('students.deleted_at')->whereNull('projects.deleted_at')
                        ->leftJoin('company_industries','company_industries.id','=','industry_id')
                        ->leftJoin('users','users.id','=','projects.user_id')->orderBy("companies.name")->orderby('company_industries.name')
                        ->select(DB::raw('companies.name as company_name,company_industries.name as industry_name,projects.name as name, concat(users.firstname,\' \',users.lastname) as user_name, count(*) as student_count,project_id'));
                if (!\Auth::user()->isSuperAdmin() &&  \Auth::user()->position!=2 && \Auth::user()->position !=3) {
                    $l = $l->where(function ($query) {
                        $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
                    });
                }
                 $l=$l->get()->toArray();
                $list[$k]['list']  =$l;
            }
            return response()->json(['data'=>$list,'filter'=>$filter]);
        }
        public function destroy($id, $auth = null)
        {
            $this->model = $this->model->where('id', '=', $id);
            $ps =ProjectStudent::where("project_id",$id)->where("status_id",2)->get();
            if(sizeof($ps)>0){
                return response()->json(['data' => ['message' => "A Project nem törölhető, mert vannak dolgozó diákok"]], $this->errorStatus);
            }

            try {
                if ($auth == null) {
                    $this->model = $this->model->firstOrFail();
                } else {
                    $this->model = $this->model->where('user_id', '=', $auth)->firstOrfail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $this->model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }
		public function details(Request $request, $id = 0, $auth = null)
		{
			$model = $this->model;
			$this->model = $this->model->where('id', '=', $id);
			try {

				if (!\Auth::user()->isSuperAdmin()) {
					$model = $model->where(function ($query) {
						$query->where('user_id', '=', \Auth::id())->orWhere('replacement_user_id', '=', \Auth::id());
					});
				}
				$this->model = $this->model->firstOrFail();

			} catch (ModelNotFoundException $e) {
				$this->model = new $model();
			}

			return new ProjectDetailsResource($this->model);
		}
        public function printForm(Request $request,$form, $company,$industry,$student,$date){

            $form = Forms::where('id',$form)->first();
            $content = $form->replaceData($industry,$student);
            $content = str_replace('[date]',$date,$content);
            $config= PdfHelper::$config;
            if($form->title){
                $config['margin_top'] = 55;


            }else {
                $config['margin_top'] = 15;
            }

            $pdf = new Mpdf($config);
            if($form->title) {
                $header = view('student.templateHeader', ['title' => $form->title]);
                $pdf->SetHTMLHeader($header->render());
                $pdf->showWatermarkImage=true;

                $pdf->SetDefaultBodyCSS('background', 'url('.base_path('storage/doc_bg.png').')');
                $pdf->SetDefaultBodyCSS('background-image-resize', 6);
                $pdf->SetDefaultBodyCSS('background-repeat', 'no-repeat');
            }
            $pdf->curlAllowUnsafeSslRequests = true;
            $pdf->debug=true;
            $pdf->WriteHTML($content);
            return Response::make($pdf->Output('pdf',\Mpdf\Output\Destination::STRING_RETURN), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$form->name.'"'
            ]);

        }
        public function addContact(Request $request){
            $p = new ProjectContact();
            $p->fill($request->all());
            $p->save();
            return $this->details($request,$p->project_id);

        }
        public function deleteContact(Request $request,$id){
            $p = ProjectContact::where('id',$id)->first();
            $project = $p->project_id;
            $p->delete();
            return $this->details($request,$project);
        }
        public function setStartDate(Request $request,$id){
           $ps = ProjectStudent::where('id',$id)->first();
           $ps->start_date = $request->input('start_date');
           $ps->save();
           return response()->json("OK");
        }
		public function getProjectStudents(Request $request, $id)
		{
			try {

				$project = Project::where('id', $id)->firstOrFail();
                $company = Company::where('id',$project->company_id)->first();
                $industry = Industry::where('id',$project->industry_id)->first();
				$search = $request->input('search');
				$students = $project->projectStudent()->wherePivot('status_id', $request->input('status'))->where(function ($query) use ($search) {
					$query = $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('phone', 'LIKE', '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%');
				})->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates  where deleted_at is null group by student_id ) as cert'), 'cert.student_id', '=', 'students.id')
                ->select("project_students.*","end","cv","answer");
               $type =$company->medical_certificate;
               if($industry->medical_certificate>0){
                   $type = $industry->medical_certificate;
               }

                if($type == 2){
                    $students = $students->leftJoin(DB::raw("(select student_id, max(expire) as expire from student_work_safety where type=1 group by student_id) as works"),'works.student_id','=','students.id')->select("project_students.*","answer","end","works.expire",DB::raw("1 as eu_certificate"));
                } if($type == 1){
                    $students = $students->leftJoin(DB::raw("(select student_id, max(expire) as expire_medical from student_work_safety where type=3 group by student_id) as works"),'works.student_id','=','students.id')->select("project_students.*","answer","end","works.expire_medical",DB::raw("1 as medical_certificate"));
                }
				if ($request->input('sort')) {
					$sort = explode('|', $request->input('sort'));
                    if($sort[0]=='comment'){
                        $students->orderBy('project_students.'.$sort[0], $sort[1]);
                    }else {
                        $students->orderBy($sort[0], $sort[1]);
                    }
				}
                $students->leftJoin(DB::raw('(select count(*) as answer,student_id,project_id from project_student_answer group by project_id,student_id ) as test'),function($join){
                   $join->on('test.student_id','=','students.id');
                   $join->on('test.project_id','=','project_students.project_id');
                });
				$pagination = 200;

               $students= $students->paginate($pagination)->withPath($request->path());


				return ProjectStudentResource::collection($students);
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
		}
        public function getProjectAnswer(Request $request,$project,$student){
            $res = ProjectStudentAnswer::where('project_id',$project)->where('student_id',$student)->first();
            return response()->json($res);
        }

		public function changeProjectStatus(Request $request, $id)
		{
			try {
				$project = Project::where('id', $id)->firstOrFail();
				$project->status = $request->input('status');
				$project->save();

				return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
		}
        public function changeProjectPriority(Request $request, $id)
        {
            try {
                $project = Project::where('id', $id)->firstOrFail();
                $project->priority = $request->input('priority');
                $project->save();
                return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
        }
		public function changeProjectStudentStatus(Request $request, $id)
		{
			try {
				$project = ProjectStudent::where('id', $id)->firstOrFail();
				$project->status_id = $request->input('status');
                if($project->status_id == 1){
                    SaveStatistic::dispatch(\Auth::user()->id,'all_apply');
                }
                if($project->status_id == 22){
                    SaveStatistic::dispatch(\Auth::user()->id,'cv_request');
                }
                if($project->status_id == 3){
                    SaveStatistic::dispatch(\Auth::user()->id,'cv_send');
                }

                if($project->status_id == 2){


                    $fixing = Fixing::where('month','<',date('Ym'))->where("student_id",$project->student_id)->first();
                    if($fixing){
                        SaveStatistic::dispatch(\Auth::user()->id,'start_work_old');
                    }else{
                        SaveStatistic::dispatch(\Auth::user()->id,'start_work');
                    }

                }
                $project->save();
                $p = Project::where('id',$project->project_id)->first();
                $ps = ProjectStudent::where('status_id',2)->where('project_id',$p->id)->get();
                if($p->number_of_employees <= sizeof($ps)){
                    $p->completion_date = date('Y-m-d H:i:s');
                }else{
                    $p->completion_date = null;
                }
                $p->save();
				return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
		}
		public function addStudentComment(Request $request, $id)
		{
			try {
				$project = ProjectStudent::where('id', $id)->firstOrFail();
				$project->comment = $request->input('comment');
				$project->save();
                SaveStatistic::dispatch(Auth::user()->id,'visited_students');
				return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
		}
		public function deleteStudent($id)
		{
			$model = ProjectStudent::where('id', '=', $id);

			try {
				$model = $model->firstOrFail();

			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();

			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}
		public function deleteRecruitedStudent(Request $request,$id){

			try {
					$model = ProjectStudent::where('id',$id)->firstOrFail();

			} catch (ModelNotFoundException $e) {
					return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();

			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}
		public function recruitStudent(Request $request){
			try {
				$model = ProjectStudent::where('project_id',$request->input('project_id'))->where('student_id',$request->input('student_id'))->first();
				if($model){
					throw new ModelNotFoundException();
				}
				$model = new ProjectStudent();
				$model->project_id =$request->input('project_id');
				$model->student_id =$request->input('student_id');
				$model->status_id = 1;
				$model->save();
                SaveStatistic::dispatch(\Auth::user()->id,'all_apply');

				$student = Project::where('id',$model->project_id)->first();
				$student = $student->projectStudent()->where('project_students.id',$model->id)->first();
				return new ProjectStudentResource($student);

			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => 'Az adtott diák már toborozva van az adott projekthez']], $this->errorStatus);
			}
		}
		public function getTaskList(Request $request,$id){
			$prTask = ProjectTask::where('project_id',$id)->whereNull('status')->orderBy('deadline','asc');
			return ProjectTaskResource::collection($prTask->get());
		}
		public function editTask(Request $request){

			$task = new ProjectTask();
			if($request->input('id')){
				$task= ProjectTask::where('id',$request->input('id'))->first();
			}
			$task->fill($request->all());
			$task->save();
			return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
		}
		public function deleteTask(Request $request, $id){

			$model = ProjectTask::where('id', '=', $id);
			try {
				$model = $model->firstOrFail();
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();
			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}
        public function getWorkByProject(Request $request,$id){
            return WorkListResource::collection(Work::where('project_id',$id)->get())->additional(['categories'=>WorkCategoryListResource::collection(WorkCategory::all())]);
        }
	}

