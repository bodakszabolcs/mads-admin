<?php

	namespace Modules\Project\Http\Controllers;
	use App\Exports\BaseArrayExport;
    use App\Helpers\ConstansHelper;
    use App\Http\Controllers\Controller;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Str;
    use Modules\Project\Entities\ProjectCalendar;
    use Modules\Project\Entities\ProjectSchedule;
    use Modules\Project\Entities\ProjectStudent;
    use Modules\Project\Http\Requests\ProjectCalendarRequest;

    class ProjectCalendarController extends Controller
	{
		public function __construct(Request $request)
		{
			$this->model = new ProjectCalendar();
		}
        public function exportCalendar(Request $request,$project,$year,$month){

            $date = $year."-".str_pad($month+1,2,'0',STR_PAD_LEFT).'-01';
            $start =  date('Y-m-01',strtotime($date));
            $end = date('Y-m-t',strtotime($date));
            $tableArray=[];
            $schedulesCount = ProjectCalendar::where('date','>=',$start)->where("date",'<=',$end)
                ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_calendar.*, students.name as student_name, students.phone as student_phone'))
                ->where('project_id',$project)
               ->orderBy('date')->orderBy("start")->get();
            foreach ($schedulesCount as $c){
                $tableArray[]= [
                    'data'=>$c->date,
                    'student_name'=>$c->student_name,
                    'student_phone'=>$c->student_phone,
                    'info'=>$c->info,
                    'start'=>$c->start,
                    'end'=>$c->end
                ];
            }
            $export = new BaseArrayExport(['Dátum','Diák','Telefon','Infó','Kezdete','Vége'], "Naptár", $tableArray);

            return $export->download(Str::slug('Naptár') . '.xlsx');


        }

        public function editEvent(ProjectCalendarRequest  $request){
            $projectCalendar = new ProjectCalendar();
            if($request->has("id") && $request->input("id")>0 ){
                $projectCalendar = ProjectCalendar::where("id",$request->input("id"))->first();
            }
            $projectCalendar->fill($request->all());
            $projectCalendar->save();
            return response()->json("ok");
        }
        public function deleteEvent(Request $request,$id){
            $this->model = $this->model->where('id', '=', $id);

            try {

                    $this->model = $this->model->firstOrFail();

            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => 'ERROR']], 422);
            }
            $this->model->delete();

            return response()->json(['data' => ['message' => 'ok']], 200);
        }
        public function getCalendar(Request $request,$project,$year,$month){
            $m = date("Y-m",strtotime($year.'-'.$month));
            $calendarData = ProjectCalendar::where("date",'LIKE','%'.$m.'%')->leftJoin("students",'students.id','=','student_id')->select(DB::raw("project_calendar.*,students.name as student_name,students.student_number as student_number,phone, email"))
                ->where("project_id",$project)
                ->orderBy("date",'asc')->orderBy('start')->get();
            $response =[];
            foreach ($calendarData as $c){
                if(!isset($response[$c->date])){
                    $response[$c->date] =[];
                }
                $response[$c->date][]=$c->toArray();
            }
            $students = ProjectStudent::where("project_id",$project)->leftJoin("students",'students.id','=','student_id')->select(DB::raw("students.id, concat(students.student_number,'-',students.name) as name "))->get()->pluck("name",'id')->toArray();
            return response()->json(['calendar'=>$response,'students'=>$students]);
        }

	}

