<?php

namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCalendarRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
			'student_id' => 'required',
			'date' => 'required',
			'start' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'project_id' => __('Projekt'),
                'student_id' => __('Diák'),
                'date' => __('Dátum'),
                'start' => __('Esemény kezdete'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
