<?php

namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRealizationRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'monday' => 'required',
            'tuesday' => 'required',
            'wednesday' => 'required',
            //'status_list' => 'required',

            'thursday' => 'required',
            'friday' => 'required',
            'saturday' => 'required',
            'sunday' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'monday' => 'Hétfő',
            'tuesday' => 'Kedd',
            'wednesday' => 'Szerda',
            //'status_list' => 'required',

            'thursday' => 'Csütörtök',
            'friday' => 'Péntek',
            'saturday' => 'Szombat',
            'sunday' => 'Vasárnap',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
