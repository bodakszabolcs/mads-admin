<?php

namespace Modules\Project\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
			'industry_id' => 'required',
			'user_id' => 'required',
			//'status_list' => 'required',

			'number_of_employees' => 'required',
			'deadline' => 'required',
			'status' => 'required',
			'schedule' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'company_id' => __('Cég'),
'industry_id' => __('Tevékenység'),
'user_id' => __('Projekt menedzser'),
'status_list' => __('Állapotok'),
'comment' => __('Megjegyzés'),
'number_of_employees' => __('Létszám'),
'deadline' => __('Határidő'),
'status' => __('Állapot'),
'schedule' => __('Kell beosztás?'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
