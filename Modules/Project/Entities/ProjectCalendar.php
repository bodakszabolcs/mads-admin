<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProjectCalendar extends BaseModel
{
    use  Cachable;
	protected $table = "project_calendar";
	protected $fillable =['project_id','student_id','date','start','end','info'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
