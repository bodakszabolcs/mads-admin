<?php

	namespace Modules\Project\Entities\Base;
	use App\BaseModel;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;
	use Spatie\Translatable\HasTranslations;
	use Modules\Company\Entities\Company;
	use Modules\Company\Entities\Industry;
	use Modules\User\Entities\User;

	abstract class BaseProject extends BaseModel
	{
		static $status=[
			1=>'Aktív',
			2=>'Felfüggesztve',
			3=>'Teljesítve'
		];
		protected $table = 'projects';
		protected $dates = ['created_at', 'updated_at'];

		public function __construct(array $attributes = [])
		{
			parent::__construct($attributes);
		}

		public function getFilters()
		{
			$this->searchColumns = [
				[
					'name' => 'companies.name', 'title' => 'Cég', 'type' => 'text',
				], [
					'name' => 'company_industries.name', 'title' => 'Tevékenység', 'type' => 'text',
				], [
					'name' => 'number_of_employees', 'title' => 'Létszám', 'type' => 'text',
				], [
					'name' => 'deadline', 'title' => 'Határidő', 'type' => 'text',
				], [
					'name' => 'status', 'title' => 'Állapot', 'type' => 'text',
				],
			];

			return parent::getFilters();
		}

		protected $with = ["company", "industry"];
		protected $fillable = ['company_id', 'industry_id', 'user_id','replacement_user_id', 'status_list', 'comment', 'number_of_employees', 'deadline', 'status', 'schedule','departments','position','week_start','priority','name','test_list'];
		protected $casts = ['status_list'=>'array','test_list'=>'array'];

		public function company()
		{
			return $this->hasOne('Modules\Company\Entities\Company', 'id', 'company_id');
		}

		public function industry()
		{
			return $this->hasOne('Modules\Company\Entities\Industry', 'id', 'industry_id');
		}

		public function user()
		{
			return $this->hasMany('Modules\User\Entities\User', 'id', 'user_id');
		}
        public function contacts()
        {
            return $this->hasMany('Modules\Project\Entities\ProjectContact', 'project_id', 'id');
        }

		public function replacementUser()
		{
			return $this->hasOne('Modules\User\Entities\User', 'id', 'replacement_user_id');
		}
		public function projectStudent(){
			return $this->belongsToMany('Modules\Student\Entities\Student','project_students',"project_id",'student_id')->withPivot(['viewed','comment','id','project_id','status_id','start_date','created_at']);
		}
	}
