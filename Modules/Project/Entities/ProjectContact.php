<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;

class ProjectContact extends Model
{
    use  Cachable;
	protected $table = "project_contacts";
	protected $fillable =['name','email','phone','position','project_id'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
