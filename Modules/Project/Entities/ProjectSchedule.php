<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProjectSchedule extends BaseModel
{
    use  Cachable;
	protected $table = "project_schedules";
	protected $fillable =['project_id','industry_id','student_id','schedule_date','start','end','department'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
