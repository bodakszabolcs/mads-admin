<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectStudentAnswer extends Model
{
    use  Cachable;
    use SoftDeletes;
	protected $table = "project_student_answer";
	protected $fillable =['student_id','project_id','answers','percent'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    protected $casts=['answers'=>'json'];








}
