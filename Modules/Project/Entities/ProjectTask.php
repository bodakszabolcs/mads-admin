<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProjectTask extends BaseModel
{
    use  Cachable;
	protected $table = "project_tasks";
	protected $fillable =['deadline','status','description','project_id'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
