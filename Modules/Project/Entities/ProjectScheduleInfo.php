<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProjectScheduleInfo extends BaseModel
{
    use  Cachable;
	protected $table = "project_schedule_info";
	protected $fillable =['project_id','industry_id','student_count','schedule_date','info','department','start','end'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
