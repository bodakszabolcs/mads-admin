<?php

namespace Modules\Project\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ProjectRealization extends BaseModel
{
    use  Cachable;
	protected $table = "project_realizations";
	protected $fillable =['project_id','student_id','date','preferred_departments','comment','monday','tuesday','wednesday','thursday','friday','saturday','sunday','scheduled'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }


    protected $casts = [
        'preferred_departments' => 'array',
    ];





}
