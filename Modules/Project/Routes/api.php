<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
	Route::group([
		'namespace' => 'Modules\Project\Http\Controllers',
		'prefix' => '/project',
		'middleware' => ['auth:sanctum','role','accesslog']
	], function () {
		Route::match(['get'], '/get-industry/{id}', 'ProjectController@getIndustryByCompany')->name('Get industry by id');
		Route::match(['get'], '/get-industry-worker/{id}', 'ProjectController@getIndustryByCompanyAndWorker')->name('Get industry by id');
		Route::match(['get'], '/get-realization/{id}', 'ProjectController@getRealization')->name('Project - Rérés lekérdezése');
		Route::match(['get'], '/get-realization-excel/{id}', 'ProjectController@getRealizationExcel')->name('Project - Rérés letöltése excelben');
		Route::match(['post'], '/save-realization/{id}', 'ProjectController@saveRealizition')->name('Project - Rérés módosítása');
		Route::match(['post'], '/save-realization-schedule/{id}/{psId}', 'ProjectController@saveScheduleRealization')->name('Project - Rérés beosztás mentés');
		Route::match(['post'], '/create-realization-link/{id}', 'ProjectController@sendRealization')->name('Project - Ráérés küldése a diákoknak');
		Route::match(['get'], '/get-company-project/{company}', 'ProjectController@getCompanyProjects')->name('Get company projects by id');
		Route::match(['get'], '/get-work/{id}', 'ProjectController@getWorkByProject')->name('Projekthez tartozó munkák (felugró)');
		Route::match(['get'], '/status-list', 'ProjectController@getStatusList')->name('Get project status table');
		Route::match(['get'], '/task/list/{id}', 'ProjectController@getTaskList')->name('Get project task');
		Route::match(['get'], '/details/{id}', 'ProjectController@details')->name('Get project details');

		Route::match(['post'], '/status/{id}', 'ProjectController@changeProjectStatus')->name('Change project status');
		Route::match(['post'], '/add-contact', 'ProjectController@addContact')->name('Add contact');
		Route::match(['delete'], '/delete-contact/{id}', 'ProjectController@deleteContact')->name('Delete contact');

		Route::match(['put'], '/update-employees/{id}', 'ProjectController@changeNumberOfEmloyees')->name('Change project number of employees');
		Route::match(['put'], '/set-start-date/{id}', 'ProjectController@setStartDate')->name('Set start date');
		Route::match(['post'], '/priority/{id}', 'ProjectController@changeProjectPriority')->name('Change project priority');
		Route::match(['post'], '/recruit', 'ProjectController@recruitStudent')->name('Project recruiting');
		Route::match(['post'], '/project-student-status/{id}', 'ProjectController@changeProjectStudentStatus')->name('Change project student status');
		Route::match(['get'], '{id}/project-students', 'ProjectController@getProjectStudents')->name('Get project students');
		Route::match(['delete'], '/delete-student/{id}', 'ProjectController@deleteStudent')->name('Delete student actual project');
		Route::match(['delete'], '/recruit/delete/{id}', 'ProjectController@deleteRecruitedStudent')->name('Delete recruited student');
		Route::match(['delete'], '/task/delete/{id}', 'ProjectController@deleteTask')->name('Delete project task');
		Route::match(['post'], '/student-add-comment/{id}', 'ProjectController@addStudentComment')->name('Comment student');
		Route::match(['post'], '/task/save', 'ProjectController@editTask')->name('Edit project task');
		Route::match(['get'], '/test-answer/{project}/{student}', 'ProjectController@getProjectAnswer')->name('Show test answer');

	});
	Route::group([
		'namespace' => 'Modules\Project\Http\Controllers',
		'prefix' => '/project/schedule',
		'middleware' => ['auth:sanctum','role']
	], function () {
		Route::match(['get'], '/get-schedule/{id}', 'ProjectScheduleController@getSchedule')->name('Get schedule by project');
        Route::match(['post'], '/save-info', 'ProjectScheduleController@saveInfo')->name('Project schedule save info');
		Route::match(['post'], '/save-schedule', 'ProjectScheduleController@saveSchedule')->name('Save schedule');
		Route::match(['post'], '/send-schedule-link/{id}', 'ProjectScheduleController@createScheduleLink')->name('Send schedule editor to student');
		Route::match(['get'], '/get-schedule-excel/{id}/{dep}', 'ProjectScheduleController@exportShedule')->name('Schedule export');
		Route::match(['get'], '/get-schedule-excel-all/{id}', 'ProjectScheduleController@exportScheduleGroup')->name('Schedule export all department');
	});

Route::group([
    'namespace' => 'Modules\Project\Http\Controllers',
    'prefix' => '/project',
    'middleware' => []
], function () {
    Route::match(['get'], '/print-company-form/{form}/{company}/{industry}/{student}/{date}', 'ProjectController@printForm')->name('Céges dokumentumok');
});
Route::group([
    'namespace' => 'Modules\Project\Http\Controllers',
    'prefix' => '/project/calendar',
    'middleware' => ['auth:sanctum','role']
], function () {
    Route::match(['get'], '/get-calendar/{project}/{year}/{month}', 'ProjectCalendarController@getCalendar')->name('Get project calendar');
    Route::match(['post'], '/edit', 'ProjectCalendarController@editEvent')->name('Project calendar edit event');
    Route::match(['delete'], '/delete/{id}', 'ProjectCalendarController@deleteEvent')->name('Project calendar delete event');
    Route::match(['get'], '/get-calendar-excel/{project}/{year}/{month}', 'ProjectCalendarController@exportCalendar')->name('Calendar export');
});
