<?php

namespace Modules\Project\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Project\Entities\Project;
use Modules\Project\Observers\ProjectObserver;

class ProjectServiceProvider extends ModuleServiceProvider
{
    protected $module = 'project';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Project::observe(ProjectObserver::class);
    }
}
