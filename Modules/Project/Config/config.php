<?php

return [
    'name' => 'Project',

                 'menu_order' => 35,

                 'menu' => [
                     [

                      'icon' =>'flaticon2-chart',

                      'title' =>'Projektek',

                      'route' =>'#Projekt lista',
                         'submenu' => [

                             [

                                 'icon' => 'fa fa-list',

                                 'title' => 'Lista',

                                 'route' =>'/'.env('ADMIN_URL').'/projects/index'

                             ],
                             [

                                 'icon' => 'fa fa-sort-alpha-down',

                                 'title' => 'Státusz',

                                 'route' => '/' . env('ADMIN_URL') . '/projects/status',

                             ]

                         ],
                     ]

                 ]
];
