import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Project from '../components/Project'
import ProjectList from '../components/ProjectList'
import ProjectCreate from '../components/ProjectCreate'
import ProjectEdit from '../components/ProjectEdit'
import ProjectDetails from '../components/ProjectDetails'
import ProjectStatusList from '../components/ProjectStatusList'
import ProjectRealizations from '../components/ProjectRealizations.vue'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'projects',
                component: Project,
                meta: {
                    title: 'Projects'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProjectList',
                        component: ProjectList,
                        meta: {
                            title: 'Projects',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    },
                    {
                        path: 'status',
                        name: 'ProjectList',
                        component: ProjectStatusList,
                        meta: {
                            title: 'Projekt stástuszok',
                            subheader: false

                        }
                    },
                    {
                        path: 'create',
                        name: 'ProjectCreate',
                        component: ProjectCreate,
                        meta: {
                            title: 'Create Projects',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProjectEdit',
                        component: ProjectEdit,
                        meta: {
                            title: 'Edit Projects',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    },
                    {
                        path: 'details/:id',
                        name: 'ProjectDetails',
                        component: ProjectDetails,
                        meta: {
                            title: 'Megrendelés részletek',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    },
                    {
                        path: 'schedule/:id',
                        name: 'ProjectSchedule',
                        component: ProjectDetails,
                        meta: {
                            title: 'Megrendelés beosztás',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    },
                    {
                        path: 'realization/:id',
                        name: 'ProjectRealization',
                        component: ProjectRealizations,
                        meta: {
                            title: 'Ráérés küldése',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/projects/index'
                        }
                    }
                ]
            }
        ]
    }
]
