<?php
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	class CreateProjectTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::dropIfExists('projects');
			Schema::create('projects', function (Blueprint $table) {
				$table->bigIncrements("id");
				$table->integer("company_id")->unsigned()->nullable();
				$table->integer("industry_id")->unsigned()->nullable();
				$table->text("user_id")->nullable();
				$table->json("status_list")->nullable();
				$table->text("comment")->nullable();
				$table->integer("number_of_employees")->unsigned()->nullable();
				$table->date("deadline")->nullable();
				$table->integer("status")->unsigned()->nullable();
				$table->integer("schedule")->unsigned()->nullable();
				$table->timestamps();
				$table->softDeletes();
				$table->index(["deleted_at"]);

			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('projects');
		}
	}
