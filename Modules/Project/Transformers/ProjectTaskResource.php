<?php

	namespace Modules\Project\Transformers;
	use App\Helpers\ConstansHelper;
	use Illuminate\Http\Resources\Json\Resource;
	use App\Http\Resources\BaseResource;

	class ProjectTaskResource extends BaseResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request
		 * @return array
		 */
		public function toArray($request)
		{
			return [
				"id" => $this->id,
				"description" => $this->description,
				"project_id" =>$this->project_id,
				"deadline" => $this->deadline,
				"deadlineFormatted" =>ConstansHelper::formatDate($this->deadline),

			];
		}
	}
