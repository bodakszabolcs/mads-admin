<?php

	namespace Modules\Project\Transformers;
	use App\Helpers\ConstansHelper;
    use Illuminate\Http\Resources\Json\Resource;
	use App\Http\Resources\BaseResource;
    use Illuminate\Support\Facades\DB;
    use Modules\Company\Entities\Company;
	use Modules\Company\Entities\Industry;
	use Modules\Project\Entities\Project;
    use Modules\ProjectQuestion\Entities\ProjectQuestion;
    use Modules\ProjectStatus\Entities\ProjectStatus;
	use Modules\User\Entities\User;
	class ProjectViewResource extends BaseResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request
		 * @return array
		 */
		public function toArray($request)
		{
			return [
				"id" => $this->id,
				"company_id" => $this->company_id,
				"name" => $this->name,
				"industry_id" => $this->industry_id,
				"user_id" => ($this->user_id)?$this->user_id:\Auth::user()->id,
				"replacement_user_id" => $this->replacement_user_id,
				"status_list" => ($this->status_list)?$this->status_list:[],
				"test_list" => ($this->test_list)?$this->test_list:[],
				"comment" => $this->comment,
				"number_of_employees" => $this->number_of_employees,
				"deadline" => $this->deadline,
				"status" => $this->status,
				"schedule" => $this->schedule,
				"departments" => $this->departments,
				"position" => $this->position,
				"week_start" => $this->week_start,
				"selectables" => [
					"status"=> Project::$status,
					"days"=> ConstansHelper::$days,
					"statusList"=> ProjectStatus::select('id','name','o')->orderBy('o')->get(),
					"questionList"=> ProjectQuestion::orderBy('question')->get(),
					"company" => Company::pluck("name", "id"),
                    "user" =>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
					"replacement_user" => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
				]
			];
		}
	}
