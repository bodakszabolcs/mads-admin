<?php

	namespace Modules\Project\Transformers;
	use App\Helpers\ConstansHelper;
    use Illuminate\Http\Resources\Json\Resource;
	use App\Http\Resources\BaseResource;
    use Modules\Project\Entities\Project;

    class ProjectStudentResource extends BaseResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request
		 * @return array
		 */
		public function toArray($request)
		{
            $project = Project::find($this->pivot->project_id);
			return [
				"id" => $this->pivot->id,
				"student_id" => $this->id,
				"company_id" => $project->company_id,
				"industry_id" => $project->industry_id,
				"status_id" => $this->pivot->status_id,
				"name" => $this->name,
				"cv" => $this->cv,
				"cv_name" => $this->cv_name,
				"email" =>$this->email,
				"exit_date" =>$this->exit_date,
				"phone"=>$this->phone,
				"comment" => $this->pivot->comment,
				"newsletter" => $this->newsletter,
				"medical_certificate" =>  optional($this)->medical_certificate,
				"eu_certificate" =>  optional($this)->eu_certificate,
                "certificate"=> $this->end,
                "expire"=> optional($this)->expire,
                "expire_medical"=> optional($this)->expire_medical,
				"created_at" => ConstansHelper::formatShortDate($this->pivot->created_at),
				"start_date" =>  ConstansHelper::formatShortDate($this->pivot->start_date),
				"answer" =>  $this->answer,
				"project_id" =>  $this->project_id,

			];
		}
	}
