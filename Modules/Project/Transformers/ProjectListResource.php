<?php

namespace Modules\Project\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProjectListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company" => $this->company_name,
		    "industry" => $this->industry_name,
		    "name" => $this->name,
		    "user_id" => $this->user_id,
		    "status_list" => $this->status_list,
		    "user" => $this->user_name,
		    "comment" => $this->comment,
		    "number_of_employees" => $this->number_of_employees,
		    "worker" => $this->worker,
		    "deadline" => ConstansHelper::formatShortDate($this->deadline),
		    "status" => $this->status,
		    "completion" => $this->completion,
		    "priority" => $this->priority,
		    "schedule" => ($this->schedule)?'Igen':'Nem',
		     ];
    }
}
