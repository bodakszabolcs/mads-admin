<?php

	namespace Modules\Project\Transformers;
	use App\Helpers\ConstansHelper;
	use Illuminate\Http\Resources\Json\Resource;
	use App\Http\Resources\BaseResource;
	use Modules\Company\Entities\Company;
    use Modules\Company\Entities\Forms;
    use Modules\Company\Entities\Industry;
	use Modules\Company\Transformers\CompanyViewResource;
	use Modules\Company\Transformers\ContactViewResource;
	use Modules\Company\Transformers\IndustryViewResource;
	use Modules\Company\Transformers\SiteViewResource;
	use Modules\Project\Entities\Project;
    use Modules\Project\Entities\ProjectContact;
    use Modules\ProjectStatus\Entities\ProjectStatus;
	use Modules\User\Entities\User;
	class ProjectDetailsResource extends BaseResource
	{
		/**
		 * Transform the resource into an array.
		 *
		 * @param \Illuminate\Http\Request
		 * @return array
		 */
		public function toArray($request)
		{

			return [
				"id" => $this->id,
				"company" => new CompanyViewResource($this->company),
                "name" => $this->name,
				"industry" => new IndustryViewResource($this->industry),
				"industry_id" => $this->indusrty_id,
				"contacts"=>  ($this->contacts?ContactViewResource::collection($this->contacts):ContactViewResource::collection(optional(optional($this->industry)->instructor))),
				"site"=>  new SiteViewResource(optional($this->industry)->site),
				"contact_counts"=> $this->contacts->count(),
				"replacement_user_id" => $this->replacement_user_id,
				"user_id" => $this->user_id,
				"status_list" => ($this->status_list)?$this->status_list:[],
				"comment" => $this->comment,
				"number_of_employees" => $this->number_of_employees,
				"deadline" => ConstansHelper::formatDate($this->deadline),
				"schedule" => $this->schedule,
				"departments" => $this->departments,
                "position" => $this->position,
                "week_start" => $this->week_start,
                "forms"=>Forms::where('company_id',$this->company_id)->get()

			];
		}
	}
