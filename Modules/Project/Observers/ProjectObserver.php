<?php

namespace Modules\Project\Observers;

use Modules\Project\Entities\Project;

class ProjectObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function saved(Project $model)
    {
        Project::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function created(Project $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function updated(Project $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function deleted(Project $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function restored(Project $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Project\Entities\Project  $model
     * @return void
     */
    public function forceDeleted(Project $model)
    {
        //
    }
}
