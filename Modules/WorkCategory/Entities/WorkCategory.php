<?php

namespace Modules\WorkCategory\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\WorkCategory\Entities\Base\BaseWorkCategory;
use Spatie\Translatable\HasTranslations;

class WorkCategory extends BaseWorkCategory
{
    use SoftDeletes, Cachable;
}
