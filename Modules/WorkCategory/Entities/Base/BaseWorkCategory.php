<?php

namespace Modules\WorkCategory\Entities\Base;

use App\BaseModel;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Storage;
use Modules\System\Entities\Settings;
use Modules\WorkCategory\Entities\WorkCategory;

abstract class BaseWorkCategory extends BaseModel
{
    protected $dates = ['deleted_at'];

    protected $table = 'work_categories';

    protected $fillable = [
        'name',
        'slug',
        'meta_title',
        'meta_description',
        'og_image',
        'parent_id',
        'o'
    ];

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->save();

        if (Arr::get($request, 'slug', null) == null) {
            $this->slug = $this->slugify($this->name.' '.$this->id);
        }
        $this->save();
        return $this;

    }

    public function parent()
    {
        return $this->belongsTo('Modules\WorkCategory\Entities\WorkCategory', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('Modules\WorkCategory\Entities\WorkCategory', 'parent_id', 'id');
    }



}
