<?php

return [
    'name' => 'WorkCategory',
    'menu_order' => 19,
    'menu' => [
        [
            'icon' => 'fa fa-folder-open',
            'title' => 'Törzsadatok',
            'route' => '#data',
            'submenu' => [

                [
                    'icon' => 'fa fa-sitemap',
                    'title' => 'Munka kategóriák',
                    'route' => '/'.env('ADMIN_URL').'/workcategory/index'
                ],
                [

                    'icon' =>'fa fa-archive',

                    'title' =>'Postai iktató küldemény',

                    'route' =>'/'.env('ADMIN_URL').'/post-delivery/index',

                ],
                [

                    'icon' =>'fa fa-truck',

                    'title' =>'Postázás módja',

                    'route' =>'/'.env('ADMIN_URL').'/post-delivery-type/index',

                ],
                [

                    'icon' =>'fa fa-comment',

                    'title' =>'Postai iktató tárgy',

                    'route' =>'/'.env('ADMIN_URL').'/post-subject/index',

                ],
                [

                    'icon' =>'fa fa-building',

                    'title' =>'Város',

                    'route' =>'/'.env('ADMIN_URL').'/city/index',

                ],
                [

                    'icon' =>'fa fa-flag',

                    'title' =>'Nyelvek',

                    'route' =>'/'.env('ADMIN_URL').'/language/index',

                ],
                [

                    'icon' =>'fa fa-bars',

                    'title' =>'Feor',

                    'route' =>'/'.env('ADMIN_URL').'/feor/index',

                ],
                [

                    'icon' =>'fa fa-flag',

                    'title' =>'Nemzetiség',

                    'route' =>'/'.env('ADMIN_URL').'/nationality/index',

                ],
                [

                    'icon' =>'fa fa-map-marker',

                    'title' =>'Közterület jellege',

                    'route' =>'/'.env('ADMIN_URL').'/area-type/index',

                ],
                [

                    'icon' =>'fa fa-briefcase',

                    'title' =>'Képzési szint',

                    'route' =>'/'.env('ADMIN_URL').'/education-level/index',

                ],
                [

                    'icon' =>'fa fa-graduation-cap',

                    'title' =>'Iskolák',

                    'route' =>'/'.env('ADMIN_URL').'/school/index',

                ],
                [

                    'icon' =>'fa fa-book',

                    'title' =>'Képzési terület',

                    'route' =>'/'.env('ADMIN_URL').'/education-area/index',

                ],
                [

	                'icon' =>'flaticon-squares',

	                'title' =>'Diák tulajdonságok',

	                'route' =>'/'.env('ADMIN_URL').'/attributes/index',

                ],
                [

	                'icon' =>'fa fa-globe',

	                'title' =>'Ország',

	                'route' =>'/'.env('ADMIN_URL').'/country/index',

                ],
                [

	                'icon' =>'fa fa-step-forward',

	                'title' =>'Megrendelés állapotok',

	                'route' =>'/'.env('ADMIN_URL').'/project-status/index',

                ],
                [

                    'icon' =>'flaticon-map-location',

                    'title' =>'Hol szeretne dolgozni?',

                    'route' =>'/'.env('ADMIN_URL').'/worklocation/index',

                ],
                [

                    'icon' =>'flaticon-calendar',

                    'title' =>'Tanév/Félév',

                    'route' =>'/'.env('ADMIN_URL').'/semester/index',

                ],
                [
                    'icon' =>'flaticon-squares',

                    'title' =>'Project tesztek',

                    'route' =>'/'.env('ADMIN_URL').'/project-question/index',
                ]

            ]
        ]
    ]
];
