<?php

namespace Modules\WorkCategory\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\WorkCategory\Entities\WorkCategory;

class WorkCategoryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'workcategory';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();
    }

}
