<?php

namespace Modules\WorkCategory\Transformers;

use App\Http\Resources\BaseResource;

class WorkCategoryTreeResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'og_image' => $this->og_image,
            'children' => WorkCategoryTreeResource::collection($this->children)
        ];
    }
}
