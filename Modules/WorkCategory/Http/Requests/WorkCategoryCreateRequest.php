<?php

namespace Modules\WorkCategory\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkCategoryCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique_translation:categories,name,'.$this->id.',id,deleted_at,NULL',
            'slug' => 'unique:categories,slug,'.$this->id.',id,deleted_at,NULL',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'slug' => __('URL')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
