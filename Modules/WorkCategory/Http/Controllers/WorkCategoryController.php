<?php

namespace Modules\WorkCategory\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkCategory\Http\Requests\WorkCategoryCreateRequest;
use Modules\WorkCategory\Transformers\WorkCategoryListResource;
use Modules\WorkCategory\Transformers\WorkCategoryTreeResource;
use Modules\WorkCategory\Transformers\WorkCategoryViewResource;

class WorkCategoryController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new WorkCategory();
        $this->viewResource = WorkCategoryViewResource::class;
        $this->listResource = WorkCategoryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = WorkCategoryCreateRequest::class;
        $this->updateRequest = WorkCategoryCreateRequest::class;
    }

    public function getCategories(Request $request)
    {
        $workcategory = WorkCategory::whereNull('parent_id');

        if ($request->input('id',0) != 0) {
            $workcategory = $workcategory->where('id','!=',$request->input('id'));
        }

        return response()->json(['data' => $workcategory->pluck('name','id')], $this->successStatus);
    }

    public function getWorkCategoryTree(Request $request)
    {
        return WorkCategoryTreeResource::collection(WorkCategory::whereNull('parent_id')->get());
    }

    public function destroy($id, $auth = null)
    {
        try {
            if ($auth == null) {
                $this->model = $this->model->where('id', '=', $id)->firstOrFail();
            } else {
                $this->model = $this->model->where([
                    ['id', '=', $id],
                    ['user_id', '=', $auth]
                ])->firstOrfail();
            }

            $this->model->where('parent_id','=',$this->model->id)->update(['parent_id' => $this->model->parent_id]);
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }
}
