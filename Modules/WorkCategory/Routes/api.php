<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group([
    'namespace' => 'Modules\WorkCategory\Http\Controllers',
    'prefix' => '/workcategory',
    'middleware' => ['auth:sanctum']
], function () {
    Route::match(['get'], '/all', 'WorkCategoryController@getCategories')->name('Get all work categories');
});

Route::group([
    'namespace' => 'Modules\WorkCategory\Http\Controllers',
    'prefix' => '/workcategory',
    'middleware' => ['auth:sanctum', 'role', 'accesslog']
], function () {
    Route::match(['get'], '/tree', 'WorkCategoryController@getWorkCategoryTree')->name('Get work categories tree');
});

