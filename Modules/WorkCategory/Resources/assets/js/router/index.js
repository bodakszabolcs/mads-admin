import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import WorkCategory from '../components/WorkCategory/WorkCategory'
import WorkCategoryList from '../components/WorkCategory/WorkCategoryList'
import WorkCategoryCreate from '../components/WorkCategory/WorkCategoryCreate'
import WorkCategoryEdit from '../components/WorkCategory/WorkCategoryEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'workcategory',
                component: WorkCategory,
                meta: {
                    title: 'WorkCategories'
                },
                children: [
                    {
                        path: 'index',
                        name: 'WorkCategoryList',
                        component: WorkCategoryList,
                        meta: {
                            title: 'WorkCategories',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/workcategory/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/workcategory/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'WorkCategoryCreate',
                        component: WorkCategoryCreate,
                        meta: {
                            title: 'Create WorkCategory',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/workcategory/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'WorkCategoryEdit',
                        component: WorkCategoryEdit,
                        meta: {
                            title: 'Edit WorkCategory',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/workcategory/index'
                        }
                    }
                ]
            }
        ]
    }
]
