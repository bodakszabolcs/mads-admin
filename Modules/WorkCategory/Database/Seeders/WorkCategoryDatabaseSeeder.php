<?php

namespace Modules\WorkCategory\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\System\Entities\Settings;
use Modules\Blog\Jobs\GeneratePrefixJson;

class WorkCategoryDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        Settings::firstOrCreate(['settings_key' => 'workcategory_prefix', 'settings_value' => 'blog']);

        GeneratePrefixJson::dispatch();
    }
}
