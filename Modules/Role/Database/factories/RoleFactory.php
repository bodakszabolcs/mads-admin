<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use \Modules\Role\Entities\Role;

$factory->define(Role::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->realText(10),
        'description' => $faker->unique()->realText(30),
        'menu' => [],
        'access' => []
    ];
});
