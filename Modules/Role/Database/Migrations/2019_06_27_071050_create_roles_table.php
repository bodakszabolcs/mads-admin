<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::defaultStringLength(191);

        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->longText('menu');
            $table->longText('access');
            $table->timestamps();
            $table->softDeletes();
            $table->index(['deleted_at', 'name']);
        });

        Schema::create('roles_users', function (Blueprint $table) {
            $table->bigInteger('roles_id')->unsigned();
            $table->bigInteger('users_id')->unsigned();
            $table->index(['roles_id', 'users_id']);
            $table->foreign('roles_id')->references('id')->on('roles');
            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('roles');
        Schema::drop('roles_users');
    }
}
