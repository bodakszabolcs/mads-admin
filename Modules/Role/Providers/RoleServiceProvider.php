<?php

namespace Modules\Role\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Role\Entities\Role;
use Modules\Role\Observers\RoleObserver;

class RoleServiceProvider extends ModuleServiceProvider
{
    protected $module = 'role';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Role::observe(RoleObserver::class);
    }
}
