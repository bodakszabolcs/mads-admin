<?php

namespace Modules\Role\Transformers;

use App\Http\Resources\BaseResource;

class RoleResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'menu' => $this->fixResponse($this->menu),
            'access' => $this->fixResponse($this->access),
        ];
    }

    private function fixResponse($data) {
        if(!$data) return new \stdClass();
        return $data;
    }
}
