import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Role from '../components/Role'
import RoleList from '../components/RoleList'
import RoleCreate from '../components/RoleCreate'
import RoleEdit from '../components/RoleEdit'

export default [
    {
        path: '/admin',
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'role',
                component: Role,
                meta: {
                    title: 'Roles'
                },
                children: [
                    {
                        path: 'index',
                        name: 'RoleList',
                        component: RoleList,
                        meta: {
                            title: 'Jogosultág lista',
                            subheader: true,
                            add_new_link: '/admin/role/create',
                            list_link: '/admin/role/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'RoleCreate',
                        component: RoleCreate,
                        meta: {
                            title: 'Jogosultág létrehozása',
                            subheader: true,
                            list_link: '/admin/role/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'RoleEdit',
                        component: RoleEdit,
                        meta: {
                            title: 'Jogosultág szekesztése',
                            subheader: true,
                            list_link: '/admin/role/index'
                        }
                    }
                ]
            }
        ]
    }
]
