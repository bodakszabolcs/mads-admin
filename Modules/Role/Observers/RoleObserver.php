<?php

namespace Modules\Role\Observers;

use Modules\Role\Entities\Role;

class RoleObserver
{
    /**
     * Handle the blog "created" event.
     *
     * @param  \Modules\Role\Entities\Role  $role
     * @return void
     */
    public function created(Role $role)
    {

    }

    /**
     * Handle the blog "updated" event.
     *
     * @param  \Modules\Role\Entities\Role  $role
     * @return void
     */
    public function updated(Role $role)
    {
        //
    }

    /**
     * Handle the blog "deleted" event.
     *
     * @param  \Modules\Role\Entities\Role  $role
     * @return void
     */
    public function deleted(Role $role)
    {
        //
    }

    /**
     * Handle the blog "restored" event.
     *
     * @param  \Modules\Role\Entities\Role  $role
     * @return void
     */
    public function restored(Role $role)
    {
        //
    }

    /**
     * Handle the blog "force deleted" event.
     *
     * @param  \Modules\Role\Entities\Role  $role
     * @return void
     */
    public function forceDeleted(Role $role)
    {
        //
    }
}
