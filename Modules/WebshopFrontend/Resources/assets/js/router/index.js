import Profile from '../components/profile/Profile'
import LoginWeb from '../components/profile/LoginWeb'
import RegistrationWeb from '../components/profile/RegistrationWeb'
import ForgetPasswordWeb from '../components/profile/ForgetPasswordWeb'
import Logout from '../../../../../../resources/js/components/Logout'
import ResetPasswordWeb from '../components/profile/ResetPasswordWeb'
import Contact from '../../../../../../resources/js/frontend/components/Contact'
import WorkTemplate from '../components/work/WorkTemplate'
import Downloadable from '../components/downloadable/Downloadable'
import Presents from '../components/profile/Profile/Presents'
import VerifyPassword from '../components/profile/VerifyPassword'
import PasswordChange from '../components/profile/Profile/PasswordChange'
import PersonalData from '../components/profile/Profile/PersonalData'
import Apply from '../components/profile/Profile/Apply'
import Finance from '../components/profile/Profile/Finance'
import Payroll from '../components/profile/Profile/Payroll'
import Documents from '../components/profile/Profile/Documents'
import Schedules from '../components/profile/Profile/Schedules'
import Notification from '../components/profile/Profile/Notification'
import Register from '../components/profile/Profile/Register'
import SchedulesEditor from '../components/profile/Profile/SchedulesEditor'
import StudentCards from '../components/profile/Profile/StudentCards'
import WorkSafety from '../components/profile/Profile/WorkSafety.vue'
import Realization from '../components/profile/Profile/Realization.vue'
import Request from '../components/request/Request.vue'
import Test from '../components/work/Test.vue'

export default [
    {
        path: '/auth/twitter',
        component: {
            template: '<div class="auth-component"></div>'
        }
    },
    {
        path: '/auth/:provider/:callback?',
        component: {
            template: '<div class="auth-component"></div>'
        }
    },
    {
        path: '/mads/register/:hash',
        component: Register,
        name: 'Mads regisztráció',
        meta: {
            title: 'Mads regisztráció'
        }
    },
    {
        path: '/profile/schedules-editor/:hash',
        component: SchedulesEditor,
        name: 'Beosztás tervező',
        meta: {
            title: 'Beosztás tervező'
        }
    },
    {
        path: '/profile/realization/:hash',
        component: Realization,
        name: 'Ráérésem',
        meta: {
            title: 'Ráérésem'
        }
    },
    {
        path: '/profile',
        component: Profile,
        name: 'Profile',
        meta: {
            requiresAuth: true,
            title: 'Profile'
        }
    },
    {
        path: '/profile/presents',
        component: Presents,
        name: 'Jelenléti',
        meta: {
            requiresAuth: true,
            title: 'Jelenléti'
        }
    },
    {
        path: '/profile/student-card',
        component: StudentCards,
        name: 'Online jogviszony vagy diákigazolvány',
        meta: {
            requiresAuth: true,
            title: 'Online jogviszony vagy diákigazolvány'
        }
    },
    {
        path: '/profile/realization',
        component: Realization,
        name: 'Ráérésem',
        meta: {
            requiresAuth: true,
            title: 'Ráérésem'
        }
    },

    {
        path: '/profile/personal-data',
        component: PersonalData,
        name: 'Személyes adatok',
        meta: {
            requiresAuth: true,
            title: 'Személyes adatok'
        }
    },
    {
        path: '/profile/work-safety',
        component: WorkSafety,
        name: 'Munkaeszközök, igazolások',
        meta: {
            requiresAuth: true,
            title: 'Munkaeszközök, igazolások'
        }
    },
    {
        path: '/profile/notification',
        component: Notification,
        name: 'Értesítések',
        meta: {
            requiresAuth: true,
            title: 'Értesítések'
        }
    },
    {
        path: '/profile/apply',
        component: Apply,
        name: 'Munka jelentkezések',
        meta: {
            requiresAuth: true,
            title: 'Munka jelentkezések'
        }
    },
    {
        path: '/profile/finance',
        component: Finance,
        name: 'Pénzügyek',
        meta: {
            requiresAuth: true,
            title: 'Pénzügyek'
        }
    },
    {
        path: '/profile/password',
        component: PasswordChange,
        name: 'Jelszó',
        meta: {
            requiresAuth: true,
            title: 'Jelszó módosítás'
        }
    },
    {
        path: '/profile/payroll',
        component: Payroll,
        name: 'Bérelszámolási lapok',
        meta: {
            requiresAuth: true,
            title: 'Bérelszámolási lapok'
        }
    },
    {
        path: '/profile/documents',
        component: Documents,
        name: 'Dokumentumok',
        meta: {
            requiresAuth: true,
            title: 'Dokumentumok'
        }
    },
    {
        path: '/profile/schedules',
        component: Schedules,
        name: 'Beosztások',
        meta: {
            requiresAuth: true,
            title: 'Beosztások'
        }
    },

    {
        path: '/munkak/:cat?/:subCat?/:slug?',
        component: WorkTemplate,
        name: 'WorkTemplate',
        meta: {
            title: 'Munkák'
        }
    },
    {
        path: '/munkak/:cat?/:subCat?/:slug?/sikeres-regisztracio',
        component: WorkTemplate,
        name: 'WorkTemplate',
        meta: {
            title: 'Munkák'
        }
    },
    {
        path: '/letoltheto-dokumentumok',
        component: Downloadable,
        name: 'Downloadable',
        meta: {
            title: 'Letölthető dokumentumok'
        }
    },
    {
        path: '/ajanlat-keres',
        component: Request,
        name: 'Request',
        meta: {
            title: 'Ajánlat kérés'
        }
    },
    {
        path: '/login',
        component: LoginWeb,
        name: 'Login',
        meta: {
            title: 'Login'
        }
    },
    {
        path: '/test',
        component: Test,
        name: 'Test',
        meta: {
            title: 'Test'
        }
    },
    {
        path: '/logout',
        component: Logout,
        name: 'Logout',
        meta: {
            title: 'Logout'
        }
    },
    {
        path: '/register',
        component: RegistrationWeb,
        name: 'Register',
        meta: {
            title: 'Register'
        }
    },
    {
        path: '/forget',
        component: ForgetPasswordWeb,
        name: 'Forget',
        meta: {
            title: 'Forget'
        }
    },
    {
        path: '/student/email/verify/:id/:hash',
        component: VerifyPassword,
        name: 'Verify password',
        meta: {
            title: 'Email megerősítés'
        }
    },
    {
        path: '/kapcsolat',
        component: Contact,
        name: 'Forget',
        meta: {
            title: 'Forget'
        }
    },
    {
        path: '/reset-password',
        component: ResetPasswordWeb,
        name: 'Reset',
        meta: {
            title: 'Reset password'
        }
    }
]
