<?php

	namespace Modules\WebshopFrontend\Entities;
    use Illuminate\Http\Request;
    use Modules\Order\Entities\Order;
    use Modules\Order\Entities\ShippingMethod;
    use Modules\ProductVariation\Entities\ProductVariation;
    class Cart extends  Order
    {
	    public static function uuID(Request $request){
	        $uuID = $request->header('User-Agent').$request->ip();
	        return md5($uuID);
        }

        public static function getMyCart(Request $request,$uuID = null,$userID = null){
	        $cartByUUID = null;
	        if(!$uuID){
	            $cartByUUID = Cart::where('uuid','=',self::uuID($request))->where('status', '<', 2)->first();
            }else{
                $cartByUUID = Cart::where('uuid','=',$uuID)->where('status', '<', 2)->first();
            }
	        if($userID){
	            if(!$cartByUUID) {
                    $cartByUUID = Cart::where('user_id', '=', $userID)->where('status', '<', 2)->first();
                }else{
                    $cart =Cart::where('user_id', '=', $userID)->where('status', '<', 2)->where('uuid','<>',$cartByUUID->uuid)->get();
                    foreach ($cart->items as $i){
                        $i->forceDelete();
                    }
                    Cart::where('user_id', '=', $userID)->where('status', '<', 2)->where('uuid','<>',$cartByUUID->uuid)->forceDelete();

                }
            }
	        if(!$cartByUUID){
	            $cartByUUID = new Cart();
	            $cartByUUID->status = 0;
	            $cartByUUID->uuid = self::uuID($request);
	            $cartByUUID->order_total = 0;
	            $cartByUUID->shipping_price = 0;
	            $cartByUUID->discount = 0;
	            $cartByUUID->save();
            }
	        return $cartByUUID;
        }
        public function calculateShipping(){
	        if($this->coupons() && $this->coupons()->type==3){
	            return 0;

            }
	            return optional(ShippingMethod::where('id','=',$this->shipping_method_id)->first())->price;
        }
	}
