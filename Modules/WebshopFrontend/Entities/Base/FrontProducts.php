<?php

	namespace Modules\WebshopFrontend\Entities\Base;
	use Illuminate\Support\Facades\DB;
    use Modules\Color\Entities\Color;
    use Modules\Color\Transformers\ColorListResource;
    use Modules\Pack\Entities\Pack;
    use Modules\Pack\Transformers\PackListResource;
    use Modules\Product\Entities\Product;
    use Modules\Type\Entities\Type;
    use Modules\Type\Transformers\TypeListResource;
    class FrontProducts extends Product
	{

	    public function ProductCollection(){
	        $product = Product::select(DB::raw('name,products.id as id,slug, rating.rating_count as rating_count, rating.avg_rate as avg_rate,description'))
                ->leftJoin(DB::raw('(select product_id, count(*) as rating_count, avg(rate) avg_rate from product_ratings group by product_id) as rating'),'products.id','=','rating.product_id')
                ->leftJoin(DB::raw('(select product_id,sum(in_stock) as ist from product_variations  where product_variations.active = 1 group by product_id) as variation'),'variation.product_id','=','products.id')
                ->where('variation.ist','>',0)->with('variations');
	        return $product;

        }
        public function additional(){
	        return [
	            "colors" => ColorListResource::collection(Color::all()),
                "packs"  => PackListResource::collection(Pack::all()),
                "types"  => TypeListResource::collection(Type::all())
            ];
        }
	}
