<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkSafetyCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'date' => 'required',
            'expire' => 'required',
            'file' => 'required',


        ];
    }

    public function attributes()
    {
        return [
            'type' => 'Típus',
            'date' => 'Kiállítás dátuma',
            'expire' => 'Lejárat dátuma',
            'file' => 'Fájl',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
