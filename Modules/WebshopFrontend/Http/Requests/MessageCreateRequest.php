<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
            'message' => 'required',
            

        ];
    }

    public function attributes()
    {
        return [
            'email' => __('Email'),
            'phone' => __('Phone'),
            'name' => __('Name'),
            'message' => __('Message'),
            'terms' => __('Term and condition'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
