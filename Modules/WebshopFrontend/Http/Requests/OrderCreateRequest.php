<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'billing_name' => 'required_without:isCompany',
            'billing_company_name' => 'required_if:isCompany,true',
            'vat_number' => 'required_if:isCompany,true',
            'billing_country' => 'required',
            'billing_zip' => 'required',
            'billing_city' => 'required',
            'billing_address' => 'required',
            'billing_email' => 'required|email',
            'billing_phone' => 'required',
            'shipping_name' => 'required',
            'shipping_email' => 'required|email',
            'shipping_country_id' => 'required',
            'shipping_city' => 'required',
            'shipping_address' => 'required',
            'shipping_zip' => 'required',
            'shipping_phone' => 'required',
            'payment_method_id' => 'required',
            'shipping_method_id' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'billing_name' => __('Billing name'),
            'billing_country' => __('Billing country'),
            'billing_zip' => __('Billing zip'),
            'email' => __('Billing email'),
            'billing_city' => __('Billing city'),
            'billing_address' => __('Billing address'),
            'vat_number' => __('Vat number'),
            'shipping_name' => __('Shipping name'),
            'shipping_email' => __('Shipping email'),
            'shipping_phone' => __('Shipping phone'),
            'billing_email' => __('Billing email'),
            'billing_phone' => __('Billing phone'),
            'shipping_country_id' => __('Shipping country'),
            'shipping_city' => __('Shipping city'),
            'shipping_address' => __('Shipping address'),
            'shipping_zip' => __('Shipping name'),
            'phone' => __('Phone'),
            'payment_method_id' => __('Payment method'),
            'shipping_method_id' => __('shipping method'),
            'isCompany'=> __('is company')

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
