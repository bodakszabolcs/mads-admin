<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkApplyRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'phone' => 'required',
            'name' => 'required',
            'aszf' => ['required','accepted'],

        ];
    }

    public function attributes()
    {
        return [
            'email' => __('Email'),
            'phone' => __('Telefon'),
            'name' => __('Név'),
            'comment' => __('Megjegyzés'),
            'aszf' => __('Adatvédelmi szabályzat'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
