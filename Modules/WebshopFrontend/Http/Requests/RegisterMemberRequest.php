<?php

namespace Modules\WebshopFrontend\Http\Requests;

use App\Rules\Tax;
use Illuminate\Foundation\Http\FormRequest;

class RegisterMemberRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'tax_number' => ['required',new Tax()],
            'email' => 'required',
            'newsletter' => 'required',
            'work_categories' => 'required',
            'name' => 'required',
            'identity_card' => 'required',
            'birth_location' => 'required',
            'birth_date' => 'required',
            'mothers_name' => 'required',
            'nationality' => 'required',
            'gender' => 'required',
            'bank_account_number' => 'required',
            'taj' => 'required',
            'om_code' => 'required',
            'phone' => 'required',
            'notification_address' => 'required',
            'country_id' => 'required',
            'zip' => 'required',
            'city_id' => 'required',
            'street' => 'required',
            'area_type_id' => 'required',
            'house_number' => 'required',
            'student_card' => 'required',
            //'building' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'tax_number' => 'Adószám',
            'prefered_work_location' => 'Hol szeretne dolgozni?',
            'om_code' => 'Diákigazolvány/OM azonosító',
            'newsletter' => 'Hírlevelet kér?',
            'work_categories' => 'Milyen munka érdekli',
            'name' => 'Név',
            'identity_card' => 'Személyi igazolvány szám',
            'birth_location' => 'Születési hely',
            'birth_date' => 'Születési dátum',
            'mothers_name' => 'Anyja leánykori neve',
            'nationality' => 'Állampolgársága',
            'gender' => 'Neme',
            'bank_account_number' => 'Bankszámlaszám',
            'taj' => 'Tajszám',
            'phone' => 'Mobil',

            'notification_address' => 'Értesítési cím',
            'country_id' => 'Ország',
            'zip' => 'Irányítószám',
            'city_id' => 'Város',
            'street' => 'Utca',
            'area_type_id' => 'Közterület jellege',
            'house_number' => 'Házszám',
            'building' => 'Épület',
            'student_card' => 'Fájl:Diákigazolvány',

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
