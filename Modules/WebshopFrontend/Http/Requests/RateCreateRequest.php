<?php

namespace Modules\WebshopFrontend\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RateCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email',
            'name' => 'required',
            'rate' => 'required',
            'title'=> 'required',
            'comment'=> 'required'

        ];
    }

    public function attributes()
    {
        return [
            'email' => __('Email'),
            'name' => __('Név'),
            'comment' => __('Üzenet'),
            'rate' => __('Értékelés'),
            'title' => __('Cím'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
