<?php

    namespace Modules\WebshopFrontend\Http\Controllers;
    use App\Http\Controllers\StudentAuth\ForgotPasswordController;
    use App\Jobs\SaveStatistic;
    use App\Jobs\SendEmailWithLink;
    use Google\Service\Dns\Project;
    use Illuminate\Auth\Notifications\ResetPassword;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;
    use Illuminate\Support\Arr;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Password;
    use Mockery\Exception\InvalidOrderException;
    use Modules\Blog\Entities\Blog;
    use Modules\Blog\Transformers\BlogListResource;
    use Modules\Campaig\Entities\CampaignCompany;
    use Modules\Category\Transformers\CategoryListResource;
    use Modules\Category\Transformers\CategoryTreeResource;
    use Modules\Category\Transformers\CategoryViewResource;
    use Modules\City\Entities\City;
    use Modules\DownloadableDocument\Entities\DownloadableDocument;
    use Modules\DownloadableDocument\Transformers\DownloadableDocumentListResource;
    use Modules\Message\Entities\Message;
    use Modules\Page\Entities\Page;
    use Modules\Page\Transformers\PageListResource;
    use Modules\Page\Transformers\PageViewResource;
    use Modules\PartnerLogo\Entities\PartnerLogo;
    use Modules\PartnerLogo\Transformers\PartnerLogoViewResource;
    use Modules\Project\Entities\ProjectApply;
    use Modules\Project\Entities\ProjectStudent;
    use Modules\Project\Entities\ProjectStudentAnswer;
    use Modules\Request\Http\Requests\RequestCreateRequest;
    use Modules\Request\Entities\Request as WebshopRequest;
    use Modules\Student\Entities\Student;
    use Modules\User\Entities\User;
    use Modules\WebshopFrontend\Entities\Base\FrontProducts;
    use Modules\WebshopFrontend\Http\Controllers\Base\BaseWebshopFrontendController;
    use Modules\WebshopFrontend\Http\Requests\MessageCreateRequest;
    use Modules\WebshopFrontend\Http\Requests\QuestionCreateRequest;
    use Modules\WebshopFrontend\Http\Requests\RateCreateRequest;
    use Modules\WebshopFrontend\Http\Requests\WorkApplyRequest;
    use Modules\WebshopFrontend\Transformers\MembersResource;
    use Modules\WebshopFrontend\Transformers\ProductListResource;
    use Modules\WebshopFrontend\Transformers\RegistrationResource;
    use Modules\WebshopFrontend\Transformers\SearchResource;
    use Modules\WebshopFrontend\Transformers\WorkListResource;
    use Modules\Work\Entities\Work;
    use Modules\Work\Transformers\WorkViewResource;
    use Modules\WorkCategory\Entities\WorkCategory;
    use \Illuminate\Foundation\Auth\SendsPasswordResetEmails;
    class WebshopFrontendController extends BaseWebshopFrontendController
    {
        public function saveRequest(RequestCreateRequest $request){
            $rs =new WebshopRequest();
            $rs->web =1;
            $rs->fill($request->all());
            $rs->subject = array_keys($request->input('subject'));
            $rs->save();
            $id = $request->input('id',8);
            $cid = $request->input('cid',0);
            $user= User::where('id',$id)->first();
            if($cid>0){
                $cc =CampaignCompany::where('id',$cid)->first();
                if($cc) {
                    $cc->status_id = 4;
                    $cc->save();
                }
            }
            SendEmailWithLink::dispatch($user->mads_email,'MADS- Új ajánlatkérés érkezett!',$user->firstname. ' '.$user->lastname,"<p>Új ajánlatod érkezett: ".$rs->company_name."<br/>Az ajánlat megtekintéséhez kattints az alábbi linkre</p>",'/admin/request/edit/'.$rs->id,'Irány az admin',[]);

            return response()->json(['data'=>'ok']);

        }
        public function checkEmail(Request $request,$email){
            $student = Student::where("email",$email)->first();
            if($student){
                return response()->json(['data'=>["student_id"=>$student->id,'name'=>$student->name,'phone'=>$student->phone]]);
            }
            return response()->json(['data'=>["student_id"=>null,'name'=>null,'phone'=>null]]);
        }
        public function getContactRequest(Request $request){
            $id = $request->input('id',8);
            $user= User::where('id',$id)->first();
            return response()->json(['name'=>$user->lastname.' '.$user->firstname,'phone'=>$user->phone,'email'=>$user->mads_email,'avatar'=>$user->avatar]);
        }

        public function getHighlightedWork(Request $request){

            return WorkViewResource::collection(Work::where('highlighted','=',1)->where('archive',0)->orderBy('updated_at','desc')->limit(2)->get());

        }
        public function searchCity(Request $request){
            return SearchResource::collection(City::leftJoin('works','works.city_id','=','cities.id')->whereNotNull('works.id')
                ->groupBy('cities.id')->select(DB::raw('cities.id as id, cities.name as name'))->where(DB::raw('LOWER(cities.name)'),'LIKE','%'.strtolower($request->input('search')).'%')->limit(10)->get());
        }
        public function searchCategory(Request $request){
         return  SearchResource::collection(
                WorkCategory::whereNull('parent_id')->leftJoin(DB::raw('(select count(*) as cnt, work_categories.parent_id as cat_id from works
                 left join work_categories on work_category_id = work_categories.id group by cat_id) as w'),'w.cat_id','=','id')->whereNotNull('cat_id')
                    ->where(function($query) use ($request){
                        $query->where(DB::raw("Lower(work_categories.name)"),'LIKE','%'.strtolower($request->input('search')).'%');
                    })
                    ->select(DB::raw("work_categories.name as name, work_categories.id"))->limit(10)->orderBy('o')->get()
            );
        }
        public function getLastWork(Request $request){

            return WorkViewResource::collection(Work::orderBy('updated_at','desc')->where('archive',0)->where('is_full',0)->limit(4)->get());

        }
        public function getRelatedWork(Request $request,$id,$category_id){

            return WorkViewResource::collection(Work::where('id','<>',$id)->where('work_category_id','=',$category_id)->where('archive',0)->where('is_full',0)->limit(2)->inRandomOrder()->get());

        }
        public function getWorkCategories(Request  $request){
            return CategoryViewResource::collection(WorkCategory::whereNull('parent_id')->where('cnt','>',0)->leftJoin(DB::raw('(select count(*) as cnt, work_categories.parent_id as cat_id from works left join work_categories on  work_category_id = work_categories.id  where is_full = 0 and archive=0 and works.deleted_at is null  group by cat_id) as w'),'w.cat_id','=','id')->whereNotNull('cat_id')->orderBy('o')->get());
        }
        public function getMembers(){
            $users= User::whereNotNull('position')->orderBy('position','asc')->orderBy('lastname','asc')->get();
            return MembersResource::collection($users)->additional(['positions'=>User::$positions]);

        }
        public function getWorkCategoryThreeWithNumbers(Request  $request){
            $headCategories = WorkCategory::whereNull('parent_id')->orderBy('name')->get();
            $subCategories = WorkCategory::where('parent_id','>',0)->select('work_categories.*','cnt')
                ->leftJoin(DB::raw('(select count(*) as cnt,work_category_id from works where is_full = 0 and archive=0  and works.deleted_at is null group by work_category_id) as work' ),'work.work_category_id','=','work_categories.id')->orderBy('name')->get();
            $response =[];
            foreach ($headCategories as $h){
                $response[$h->id] = [
                    'slug'=>$h->slug,
                    'name'=>$h->name,
                    'cnt' =>0,
                    'children' =>[],
                ];
            }
            foreach ($subCategories as $s){
                if($s->cnt >0) {
                    $response[$s->parent_id]['children'][$s->id] = [
                        'slug' => $s->slug, 'name' => $s->name, 'cnt' => $s->cnt,
                    ];
                   if(isset($response[$s->parent_id]['cnt'])) {
                       $response[$s->parent_id]['cnt'] += $s->cnt;
                   }
                }
            }

            foreach ($response as $k=>$r){

                if(!isset($r['cnt']) || $r['cnt'] == 0){
                    unset($response[$k]);
                }
            }
            return response()->json($response,200);


        }
        public function getWorkBySlug(Request  $request,$cat,$subCat,$slug){
            try
            {
                $work =Work::where('slug', '=', $slug)->firstOrFail();
                if($work->getCategorySlug() != '/'.$cat.'/'.$subCat){
                    throw new ModelNotFoundException();
                }
                return new WorkViewResource($work);
            }
            catch (ModelNotFoundException $e){
                abort(404);
            }
        }
        public function getWorkList(Request $request,$cat=null,$sub=null){

            $cats =[];
            $work = Work::where('id','>',0);
            if($sub){
                $cats =WorkCategory::where('slug','=',$sub)->get()->pluck('id','id')->toArray();
            }
            elseif($cat){
                $c =WorkCategory::where('slug','=',$cat)->first();
                $cats =WorkCategory::where('parent_id','=',optional($c)->id)->get()->pluck('id','id')->toArray();
            }
            if(!empty($cats)) {
                $work = Work::whereIn('work_category_id', $cats)->orderBy('updated_at', 'desc');
            }

            if($request->input('search')){
               $work = $work->where('name','LIKE','%'.$request->input('search').'%');
            }
            if($request->has('city_id')){
                $work = $work->where('city_id','=',$request->input('city_id'));
            }
          if($request->has('category_id')){
                $catss = WorkCategory::where('parent_id',$request->input('category_id'))->get()->pluck('id','id')->toArray();
                $work = $work->whereIn('work_category_id',$catss);
            }
            if($request->has('gross')){
                $work = $work->where('gross','>=',$request->input('gross'));
            }
            if($request->has('type')){
                if($request->input('type') == 'Hosszú távú') {
                    $work =  $work->where('work_duration', '=', 'Hosszú távú');
                }else{
                    $work = $work->where('work_duration', '<>', $request->input('type'));
                }
            }

            $work = $work->where('archive',0)->orderBy('updated_at', 'desc');
            $pagination = (int)Arr::get($request, 'per_page', 12);

            return WorkListResource::collection($work->paginate($pagination)->withPath($request->path()));
        }

        public function index(Request $request, $slug = null)
        {
            $prods = new FrontProducts();
            $list = $prods->ProductCollection();
            if ($slug) {
                // $list = $list->join('product_categories','product_categories.product_id','=','products.id')->where('slug','=',$slug);
            }
            if ($request->has('search')) {
                $search = explode(' ', $request->input('search'));
                $list = $list->where(function ($query) use ($search) {
                    foreach ($search as $s) {
                        $query = $query->where(function ($query) use ($s) {
                            $query = $query->where('name', 'LIKE', '%' . $s . '%');
                        });
                        $query = $query->orWhere(function ($query) use ($s) {
                            $query = $query->where('description', 'LIKE', '%' . $s . '%');
                        });
                    }
                });
            }
            $pagination = 12;
            $list = $list->paginate($pagination)->withPath($request->path());

            return ProductListResource::collection($list);
        }
        public function getPartnerLogos(){
            return PartnerLogoViewResource::collection(PartnerLogo::inRandomOrder()->get());
        }



        public function getDownloadableContent(Request $request){
            $documents = DownloadableDocument::orderBy('is_main','desc');
            if($request->input('search')){
                $documents->where('name','LIKE','%'.$request->input('search').'%');
            }
            $pagination = (int)Arr::get($request, 'per_page', 12);
            return DownloadableDocumentListResource::collection($documents->paginate($pagination)->withPath($request->path()));
        }

        public function Message(MessageCreateRequest $request){
            $message = new Message();
            $message->fill($request->all());
            $message->status = 0;
            $message->save();
            return response()->json('OK');

        }
        public function search(Request $request){
            $search = $request->input('search');
            $prod = new FrontProducts();
            $product = $prod->ProductCollection()->groupBy('products.id')
                ->where(function($query) use($search){
                    $query->where('name','LIKE','%'.$search.'%')
                        ->orWhere('description','LIKE','%'.$search.'%');
                })
                ->limit(4)->get();
            $documents = DownloadableDocument::where('name','LIKE','%'.$search.'%')->limit(4)->get();
            $events = Event::where(function($query) use($search){
                $query->where('title','LIKE','%'.$search.'%')
                    ->orWhere('location','LIKE','%'.$search.'%')
                    ->orWhere('description','LIKE','%'.$search.'%');
            })->limit(4)->get();
            $page = Page::where(function($query) use($search){
                $query->where('name','LIKE','%'.$search.'%')
                    ->orWhere('content','LIKE','%'.$search.'%');
            })->limit(4)->get();
            $blog = Blog::where(function($query) use($search){
                $query->where('title','LIKE','%'.$search.'%')
                    ->orWhere('content','LIKE','%'.$search.'%');
            })->limit(4)->get();
            return response()->json([
                'products'=> ProductListResource::collection($product),
                'documents'=>DownloadableDocumentListResource::collection($documents),
                'events' => EventListResource::collection($events),
                'pages' => PageListResource::collection($page),
                'blog' => BlogListResource::collection($blog),
            ]);
        }
        public function getSelectables(Request $request){
            return new RegistrationResource($request);
        }
        public function getTests(Request $request,$id){
            $work = Work::where("id", $id)->first();
            $project = \Modules\Project\Entities\Project::where("id", $work->project_id)->first();
            if($project && $project->test_list) {
                return response()->json(['data' => ['message' => "Sikeres jelentkezés", "sentEmail" =>false],'hasTest'=>true,'tests'=>['student_id'=>null,'project_id'=>$project->id,'json'=>$project->test_list]], 200);
            }
            return response()->json(['data' => ['message' => "Sikeres jelentkezés", "sentEmail" =>false],'hasTest'=>false,'tests'=>['student_id'=>null,'project_id'=>$project->id,'json'=>[]]], 200);
        }
        public function workApply(WorkApplyRequest  $request){

                $sentEmail = false;
                $work = Work::where("id", $request->input("work_id"))->first();
                $pm = User::where("id", $work->user_id)->first();
                $student2 = Student::where('email', $request->input("email"))->first();
                    if(!$student2) {
                        $c = new ForgotPasswordController();
                        $student = new Student();
                        $student->name = $request->input("name");
                        $student->phone = $request->input("phone");
                        $student->email = $request->input("email");
                        $student->cv = $request->input("file");

                        $student->save();

                        $sentEmail = true;
                        $c->sendResetLinkEmail($request);
                    }else{
                        $student= $student2;
                    }


                $student->cv = $request->input("file");
                $student->cv_name = 'cv.pdf';
                $student->cv_date = date('Y-m-d H:i:s');
                $student->save();
                if($student->exit_date){
                    return response()->json(['errors' => ['errors' => "Kilépett diákok nem jelentkezhetnek munkára!"]],422);
                }
                $project = \Modules\Project\Entities\Project::where("id", $work->project_id)->first();
                $ps = ProjectStudent::where("project_id", $project->id)->where("student_id", $student->id)->first();
                if ($ps) {
                    return response()->json(['errors' => ['errors' => "Már jelentkeztél erre a munkára"]], 422);
                }
                $ps = new ProjectStudent();
                $ps->student_id = $student->id;
                $ps->project_id = $project->id;
                $ps->status_id = 1;
                $ps->save();
                SaveStatistic::dispatch($work->user_id,'new_apply_email');
                SaveStatistic::dispatch($work->user_id,'all_apply');
                Mail::to($pm->email)->send(new \App\Mail\SendWorkEmail(
                    $request->input("email"),
                    $request->input("name"),
                    $request->input("comment"),
                    $request->input("phone"),
                    $request->input("file"),
                    $work->id

                ));
               // $project= \Modules\Project\Entities\Project::where('id', 2602)->first();
                if($request->has('tests') && $request->input('tests')){
                    $answer = new ProjectStudentAnswer();
                    $answer->fill($request->input('tests'));
                    $answer->student_id = $student->id;
                    $answer->save();
                    $status=0;
                    if($answer->percent <=0.4){
                        $status=5;
                    }
                    else if($answer->percent > 0.9){
                        $status=28;
                    }else{
                        $status=27;
                    }
                    $ps = ProjectStudent::where('project_id',$answer->project_id)->where('student_id',$answer->student_id)->first();
                    if(!$ps){
                        $ps = new ProjectStudent();
                        $ps->project_id = $answer->project_id;
                        $ps->student_id = $answer->student_id;
                    }
                    $ps->status_id = $status;
                    $ps->save();
                }
                if($project->test_list) {
                    return response()->json(['data' => ['message' => "Sikeres jelentkezés", "sentEmail" => $sentEmail],'hasTest'=>true,'tests'=>['student_id'=>$student->id,'project_id'=>$project->id,'json'=>$project->test_list]], 200);
                }
                return response()->json(['data' => ['message' => "Sikeres jelentkezés", "sentEmail" => $sentEmail]], 200);

        }
    }
