<?php

namespace Modules\WebshopFrontend\Http\Controllers;

use App\Helpers\ConstansHelper;
use App\Helpers\PdfHelper;
use App\Http\Controllers\Controller;
use App\Jobs\SaveStatistic;
use App\Jobs\StudentRegistrationFeedback;
use App\Jobs\StudentRegistrationThankYou;
use App\Mail\StudentRegistrationThankYouEmail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Laravel\Sanctum\PersonalAccessToken;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Industry;
use Modules\DownPayment\Entities\DownPayment;
use Modules\EducationArea\Entities\EducationArea;
use Modules\EducationLevel\Entities\EducationLevel;
use Modules\Fixing\Entities\Fixing;
use Modules\Fixing\Transformers\FixingViewResource;
use Modules\Language\Entities\Language;
use Modules\MonthlyPayment\Entities\MonthlyPayment;
use Modules\Nationality\Entities\Nationality;
use Modules\OnlineStudentCard\Entities\OnlineStudentCard;
use Modules\OnlineStudentCard\Http\Requests\OnlineStudentCardCreateRequest;
use Modules\Payroll\Entities\Payroll;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ProjectRealization;
use Modules\Project\Entities\ProjectSchedule;
use Modules\Project\Entities\ProjectScheduleInfo;
use Modules\Project\Entities\ProjectStudent;
use Modules\Project\Entities\ProjectStudentAnswer;
use Modules\Project\Http\Requests\ProjectRealizationRequest;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Modules\Realization\Entities\Realization;
use Modules\Realization\Http\Requests\RealizationCreateFrontendRequest;
use Modules\Realization\Http\Requests\RealizationCreateRequest;
use Modules\Realization\Transformers\RealizationListResource;
use Modules\School\Entities\School;
use Modules\Semester\Entities\Semester;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentCertificate;
use Modules\Student\Entities\StudentDocuments;
use Modules\Student\Entities\StudentLanguages;
use Modules\Student\Entities\StudentNotification;
use Modules\Student\Entities\StudentSchool;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\Student\Http\Requests\LanguageCreateRequest;
use Modules\Student\Http\Requests\SchoolCreateRequest;
use Modules\Student\Http\Requests\StudentUpdateRequest;
use Modules\Student\Transformers\StudentDocumentListResource;
use Modules\Student\Transformers\StudentLanguageListResource;
use Modules\Student\Transformers\StudentSchoolViewResource;
use Modules\Student\Transformers\StudentWorkSafetyListResource;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\User\Entities\User;
use Modules\WebshopFrontend\Http\Requests\RegisterMemberRequest;
use Modules\WebshopFrontend\Http\Requests\WorkSafetyCreateRequest;
use Modules\WebshopFrontend\Transformers\WorkStudentListResource;
use Modules\Work\Entities\Work;
use TheSeer\Tokenizer\Token;

class StudentController extends Controller
{
    private $successMessage='OK';
    private $successStatus=200;

    public function getApiDashboard(Request $request){


            $resp =[
                'prev_month'=>price_format(optional(Payroll::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('month','=',date('Ym',strtotime(' -1 month')))->first())->net),
                'prev_hour'=> Fixing::where('month',date('Ym',strtotime(' -1 month')))->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->select(DB::raw('sum(hour+bonus_hour) as hour'))->get()->toArray(),
                'documents'=> StudentDocuments::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('type',1)->count(),
                'schedule'=>ProjectSchedule::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('schedule_date',date('Y-m-d'))->first(),
                'chart'=>Payroll::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('month','LIKE',date('Y').'%')->orderBy('month')->get()->pluck('net','month')
            ];
            return \response()->json($resp);
    }
    public function getMyPresents(Request $request,$month=null){
        if(!$month){
            $month = date('Y-m');
        }
       $projectIds = ProjectStudent::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('status_id',2)->groupBy('project_id')->get()->pluck('project_id','project_id');
       if(!$projectIds){
           return response()->json([],200);
       }
       $projects = Project::whereIn('projects.id',$projectIds)
           ->select(DB::raw("projects.*,companies.name as company_name, company_industries.name as industry_name,online_presence"))
           ->leftJoin('companies','companies.id','=','company_id')->leftJoin('company_industries','company_industries.id','=','industry_id')->get();
        if(!$projects){
            return response()->json([],200);
        }
        $student = Student::where("id",$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        $nationality = Nationality::where('id',$student->nationality)->first();
       $response = [];
       foreach ($projects as $p){
           $industry = Industry::where('id',$p->industry_id)->first();
           $fixings= DB::table('fixings')->where('industry_id','=',$industry->id)->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('month',date('Ym',strtotime($month.'-01')))
               ->select(DB::raw("sum(student_price) as sum_price"))->first();

           $response[]=[
             'company'=>$p->company_name,
             'industry'=>$p->industry_name,
             'company_use'=>$p->online_presence>0,
             'company_id'=>$p->company_id,
             'industry_id'=>$p->industry_id,
             'industry_has_break'=>$industry->break,
             'price_hourly'=>$industry->price_hour,
             'mads_all'=>$this->calcNet(optional($nationality)->discount,$student,date('Ym',strtotime($month.'-01')),$fixings->sum_price),
             'presents'=> $this->generatePresents($p->company_id,$p->industry_id,$month,$request->user()?$request->user()->id:\Auth::guard('student')->id())
           ];
       }
       return response()->json($response,200);

    }
    public function checkRegistrationLink(Request $request){

        $student= Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('registration_token','=',$request->input('token'))->first();
        if(!$student){
            return response()->json(['status'=>false,'error'=>'Az adott link nem érvényes'],422);
        }
        if($student && $student->registration_token_expire < date('Y-m-d H:i:s')){
            return response()->json(['status'=>false,'error'=>'Az adott link lejárt'],422);
        }
        return response()->json(['status'=>true,'error'=>null],200);
    }
    public function registerMemeber(RegisterMemberRequest $request){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $student = Student::where('id','=',$id)->firstOrFail();
        $student->fillAndSave($request->all());
        $student->fill($request->all());
        if($student->newsletter===null){
            $student->newsletter=1;
        }
        $student->registration_created= date('Y-m-d H:i:s');
        $student->registration_token = null;
        $student->save();
        $doc= new StudentDocuments();
        $doc->student_id = $id;
        $doc->document_type_id =0;
        $doc->file =$request->input('student_card');
        $doc->name ="Regisztrációs diákigazolvány";
        $doc->save();
        StudentRegistrationFeedback::dispatch($student);
        StudentRegistrationThankYou::dispatch($student);
    }
    public function getProjectRealization(Request $request){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $realization =ProjectRealization::where('student_id',$id)->leftJoin('projects','projects.id','=','project_id')->leftJoin('companies','companies.id','=','company_id')->select(DB::raw('project_realizations.*, companies.name as company_name,project_realizations.monday as apply,projects.week_start,projects.departments'))->orderBy('date','desc')
            ->limit(10)->get();

        return response()->json($realization);
    }
    public function getProjectRealizationByHash(Request $request,$hash){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $data = json_decode(base64_decode($hash),true);
        $pr = ProjectRealization::where('project_realizations.id',$data['id'])->leftJoin('projects','projects.id','=','project_id')->leftJoin('companies','companies.id','=','company_id')->select(DB::raw('project_realizations.*, companies.name as company_name,project_realizations.monday as apply,projects.week_start,projects.departments'))->first();
        if(!$pr){
            return  response()->json(['error'=>'A link nem található'],422);
        }
        if($pr->student_id !== $id){
            return  response()->json(['error'=>'A link nem ehhez a felhasználóhoz tartozik'],422);
        }
        return  response()->json($pr);
    }
    public function saveProjectRealization(ProjectRealizationRequest $request){
        $p = ProjectRealization::where('id',$request->input('id'))->first();
        $p->fill($request->all());
        $p->save();
        return \response()->json("ok");
    }
    public function updateProfile(StudentUpdateRequest $request){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $student = Student::where('id','=',$id)->firstOrFail();
        if( $student->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $student->fill($request->all());
        if($student->newsletter===null){
            $student->newsletter=1;
        }

        $student->save();
        $student->updateSendingBlue();
    }
    public function getSchools(Request $request){
         $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        try{
            $student = Student::where('id','=',$id)->firstOrFail();

            return StudentSchoolViewResource::collection($student->schools()->orderBy('id','desc')->get())->additional([
                'education_types'=>Student::$EDUCATIONTYPES,
                'education_levels'=>EducationLevel::all()->pluck('name','id'),
                'education_areas'=>EducationArea::all()->pluck('name','id'),
                'school_list'=>School::all()->pluck('name','id'),
            ]);
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }

    }
    public function addSchools(SchoolCreateRequest $request){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        if( Student::where("id",$id)->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }


        $school =new StudentSchool();
        $school->fill($request->all());
        $school->student_id = $id;
        $school->save();

        return response()->json($this->successMessage,$this->successStatus);
    }
    public function deleteSchools(Request $request,$id){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        try {

            $school = StudentSchool::where('id','=',$id)->where('student_id',  $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail();

            $school->delete();
            return response()->json($this->successMessage ,$this->successStatus);

        }	catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function listLanguage(Request $request){
        $id =$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $studentLanguages = StudentLanguages::where('student_id',$id)->leftJoin('languages','language_id','languages.id')->select(DB::raw('student_languages.*,languages.name as language_name'))->get();
        return StudentLanguageListResource::collection($studentLanguages)->additional(['languages'=>Language::all()->pluck('name','id')]);
    }
    public function addLanguage(LanguageCreateRequest $request){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $id=$request->user()?$request->user()->id:\Auth::guard('student')->id();
        $studentLanguage = new StudentLanguages();
        $studentLanguage->fill($request->all());
        $studentLanguage->student_id = $id;
        $studentLanguage->save();
        return $this->listLanguage($request);
    }
    public function deleteLanguage(Request  $request, $id){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        try {
            $studentLanguage = StudentLanguages::where('id',$id)->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail();
            $studentLanguage->delete();
            return $this->listLanguage($request);
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }

    public function generatePresents($company_id,$industry_id,$month,$user_id){
        $start = date('Y-m-d',strtotime($month.'-01'));
        $end = date('Y-m-t',strtotime($month.'-01'));
        $days = [];
            $presents = StudentPresent::where('company_id',$company_id)->where('industry_id',$industry_id)->where('student_id',$user_id)->where('date','>=',$start)->where('date','<=',$end)->get();
            while ($start <= $end){
                $days[$start] = [
                    'name'=> ConstansHelper::formatDateShort($start),
                    'date'=> $start,
                    'start'=>'',
                    'weekend'=> (date('N',strtotime($start)) >5)?true:false,
                    'end'=>'',
                    'break_from'=>'',
                    'break_to'=>'',
                    'hour'=>0,
                    'approved'=> (date('Y-m-d') > date('Y-m-d',strtotime($end.' +1 day'))),
                    'updated'=>'',
                ];
                $start = date('Y-m-d',strtotime($start.' +1 day'));
            }
            foreach ($presents as $pres){
                $days[$pres->date]['start']=$pres->start;
                $days[$pres->date]['end']=$pres->end;
                $days[$pres->date]['break_from']=$pres->break_from;
                $days[$pres->date]['break_to']=$pres->break_to;
                $days[$pres->date]['hour']=$pres->hour;
                $days[$pres->date]['updated']=$pres->updated_at;
                $days[$pres->date]['approved']=(date('Y-m-d') > date('Y-m-d',strtotime($end.' +1 day')))?1:$pres->approved;
            }

            return $days;
    }
    public function savePresents(Request $request){

        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $presents = $request->input('presents',[]);
        $month = null;
        $temp = null;
        DB::beginTransaction();

        foreach ($presents as $p){

            $av =StudentPresent::where('company_id',$request->input('company_id'))->where('industry_id',$request->input('industry_id'))->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())
                ->where('date','=',$p['date'])->first();
            if($av  && (!$p['start'] || !$p['end'])){
                $av->forceDelete();
            }
            if(!$av){
                $av = new StudentPresent();
            }
            $month = date('Y-m',strtotime($p['date']));
            if(!$av->approved && $p['start'] && $p['end'] && $p['hour']>0){
                if($p['start'] !='0:')
                $av->fill($p);
                $av->company_id = $request->input('company_id');
                $av->industry_id = $request->input('industry_id');
                $av->student_id = $request->user()?$request->user()->id:\Auth::guard('student')->id();
                $av->save();
            }
            $temp =$av;

        }
        DB::commit();

        return response()->json(
             $this->generatePresents($request->input('company_id'),$request->input('industry_id'),$month,$request->user()?$request->user()->id:\Auth::guard('student')->id())
        ,200);
    }
    public function getPresentPDF(Request $request,$industry_id,$month){
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        if(!$student){
            return abort(419);
        }
        $industry = Industry::where('id',$industry_id)->first();
        $company = Company::where('id',$industry->company_id)->first();
        $project= Project::where('industry_id',$industry_id)->first();
        $projectManager = User::where('id',$project->user_id)->first();
        $presents = $this->generatePresents($industry->company_id,$industry_id,$month,$request->user()?$request->user()->id:\Auth::guard('student')->id());
        $date = date('Y',strtotime($month.'-01')).'. '.Arr::get(ConstansHelper::$moths,(int)explode('-',$month)[1]);

        return PdfHelper::createPdf(view('student.jelenleti',['student'=>$student,'projectManager'=>$projectManager,'month'=>$date,'presents'=>$presents,'task'=>$industry->name,'project'=>$company->name]),null,null,null,['margin_top'=>10,'margin_bottom'=>10]);

    }
    public function personalData(Request $request){

    }
    public function getMyNotification(Request $request){
        $list=StudentNotification::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->whereNull('viewed');
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return WorkStudentListResource::collection($list);
    }
    public function getMyApply(Request $request){
        $list=ProjectStudent::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->leftJoin('works','works.project_id','=','project_students.project_id')->groupBy('project_students.project_id')->select(DB::raw('works.id as id,works.name as name,work_category_id'),'slug','lead','image');
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return WorkStudentListResource::collection($list);
    }
    public function getMyFinance(Request $request,$month){
        $month = date('Y-m',strtotime($month));
      $fixingsCompany =   Fixing::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('month','=',str_replace('-','',$month))->groupBy('industry_id')->orderBy('date','asc')->get();
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        $nationality = Nationality::where('id',$student->nationality)->first();
      $response =['fixings'=>[],'days'=>[],'summation'=>[]];
      if(!$fixingsCompany){
          return response()->json($response,'200');
      }
      $start = date('Y-m-01',strtotime($month));
      $end = date('Y-m-t',strtotime($month));
      while ($start<=$end){
            $response['days'][$start]=[
                'date'=>$start,
                'name'=>Arr::get(ConstansHelper::$days,date('N',strtotime($start))),
                'work'=>false
            ];
          $start = date('Y-m-d',strtotime($start.' +1 day'));
      }
      $sum_hour =0;
      $sum_price=0;
      foreach ($fixingsCompany as $fc) {
          $fixings = Fixing::where('student_id', $request->user()?$request->user()->id:\Auth::guard('student')->id())
              ->where('month', '=', str_replace('-', '', $month))->where('industry_id','=',$fc->industry_id)->orderBy('date', 'asc')->get();
          $company = Company::where('id',$fc->company_id)->first();
          $industry = Industry::where('id',$fc->industry_id)->first();
          $response['fixings'][$fc->industry_id]['company_name']=$company->name.' - '.$industry->name;
          $sum= [
              'date' => 'Összesen',
              'start' => '',
              'end' => '',
              'hour' => 0,
              'fix' => 0,
              'price' => 0,
              'szja' => 0,
              'price_net' => 0,
          ];

          foreach ($fixings as $f) {
              $response['fixings'][$fc->industry_id]['data'][$f->date] = [
                  'date' => $f->date,
                  'start' => $f->start,
                  'end' => $f->end,
                  'hour' => $f->hour + $f->bonus_hour,
                  'fix' => $f->fix,
                  'price' => $f->student_price,
                  'szja' => $f->student_price-$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$f->student_price),
                  'price_net' => $this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$f->student_price),
              ];
              $response['days'][$f->date]['work']=true;
              $sum['hour']+=$f->hour + $f->bonus_hour;
              $sum['fix']+=$f->fix;
              $sum['price']+= $f->student_price;
              $sum['szja']+=$f->student_price-$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$f->student_price);
              $sum['price_net']+=$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$f->student_price);
              $sum_price +=$f->student_price;
              $sum_hour += $f->hour + $f->bonus_hour;
          }
          $response['fixings'][$fc->industry_id]['data']['Összesen']=$sum;
      }
        $response['days'] = array_chunk($response['days'],7);
      $monthly = MonthlyPayment::where('month',str_replace('-','',$month))->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->get();
      if($monthly){
          $response['monthly']=[
              'price'=>0,
              'szja'=>0,
              'price_net'=>0
          ];
          foreach ($monthly as $m){
              $response['monthly']['price']+=$m->price;
              $response['monthly']['szja']+=$m->price-$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$m->price);
              $response['monthly']['price_net']+=$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$m->price);;
          }
      }
      $response['summation']['Összesen']= [
          'hour'=>$sum_hour,
          'price'=>$sum_price,
          'szja'=>$sum_price-$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$sum_price),
          'price_net'=>$this->calcNet(optional($nationality)->discount,$student,str_replace('-', '', $month),$sum_price)
      ];
        $response['summation']['Hóközi kifizetés']= [
            'hour'=>'',
            'price'=>$response['monthly']['price'],
            'szja'=>$response['monthly']['szja'],
            'price_net'=>$response['monthly']['price_net'],
        ];
        $response['summation']['Részjegy']= [
            'hour'=>'',
            'price'=>'',
            'szja'=>'',
            'price_net'=>0,
        ];
      if(!$student->part_ticket){
          $response['summation']['Részjegy']['price_net']=2000;
      }
        $response['summation']['Mindösszesen']= [
            'hour'=>$sum_hour,
            'price'=> $response['summation']['Összesen']['price']-$response['summation']['Hóközi kifizetés']['price'],
            'szja'=>$response['summation']['Összesen']['szja']+$response['summation']['Hóközi kifizetés']['szja'],
            'price_net'=>$response['summation']['Összesen']['price_net']-$response['summation']['Hóközi kifizetés']['price_net'] - $response['summation']['Részjegy']['price_net'],
        ];
      return response()->json($response,200);

    }
    public function getMyPayroll(Request $request,$year){

        $fixings=Payroll::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('month','LIKE','%'.$year.'%')->get();
        $response=[];
        for($i=1;$i<=12;$i++){
            $response[$year.str_pad($i,2,'0',STR_PAD_LEFT)]=[
              'month' => ConstansHelper::$moths[$i],
              'data' => null

            ];
        }
        foreach ($fixings as $f){
            $m =$f->month;
            if(strlen($f->month) <6){
                $arr = str_split($f->month,4);

                $m = ($arr[0].'0').(str_replace($arr[0],"",$f->month));

            }
            $response[$m]['data']=$f->toArray();
        }

        return response()->json($response,200);
    }
    public function printPayroll(Request $request,$month){

        $t =isset($_COOKIE['student-token'])?$_COOKIE['student-token']:$request->bearerToken();
        $token = PersonalAccessToken::findToken($t);
        if ($token) {

            $user = Student::find($token->tokenable_id);


            $month = str_replace('-', '', $month);
            $payroll = Payroll::where('student_id', $user->id)->where(function($query)use($month){
                $arr = str_split($month,4);
             $query->where('month', $month)->orWhere('month',$arr[0].((int)($arr[1])))   ;
            })->first();
            if ($payroll) {
                if(strlen($payroll->month) <6){
                    $arr = str_split($payroll->month,4);

                    $m = ($arr[0].'0').(str_replace($arr[0],"",$payroll->month));
                    $payroll->month=$m;


                }
                return PdfHelper::createPdf(view('student.berelszamolas', ['payroll' => $payroll]),
                    view('student.templateHeader', ['title' => 'Bérelszámolási lap'])
                );
            }
        }
        abort(404);

    }
    public function getMyDocuments(Request $request){
        $list = StudentDocuments::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->orderBy('id','desc');

        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return StudentDocumentListResource::collection($list);
    }
    public function getMyWorkSafety(Request $request){
        $list = StudentWorkSafety::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->orderBy('id','desc');
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return StudentWorkSafetyListResource::collection($list);
    }
    public function saveWorkSafety(WorkSafetyCreateRequest $request){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $safety =  new StudentWorkSafety();
        $safety->fill($request->all());
        $safety->student_id= $request->user()?$request->user()->id:\Auth::guard('student')->id();
        $safety->approve_is_required = 1;
        $safety->save();
        return response()->json("ok");
    }

    public function deleteWorkSafety(Request $request,$id){

        try {
            $workSafety = StudentWorkSafety::where('id',$id)->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail();
            $workSafety->delete();
            return response()->json('ok');
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getMyDocument(Request $request,$id){
        $t =isset($_COOKIE['student-token'])?$_COOKIE['student-token']:$request->bearerToken();
        try {
            if(!$t){
                abort(404);
            }
            $token = PersonalAccessToken::findToken($t);
            if ($token) {
                $user = Student::find($token->tokenable_id);
                $document = StudentDocuments::where('student_id', $user->id)->where('id',$id)->firstOrFail();
                return Storage::disk('s3')->download($document->file,Str::slug($document->name).'.pdf');

            }
            abort(404);
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getMyCv(Request $request){
        $t =isset($_COOKIE['student-token'])?$_COOKIE['student-token']:$request->bearerToken();
        try {
            if(!$t){
                abort(404);
            }
            $token = PersonalAccessToken::findToken($t);
            if ($token) {
                $user = Student::find($token->tokenable_id);
                return Storage::disk('s3')->download($user->cv,Str::slug($user->cv_name).'.pdf');

            }
            abort(404);
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }
    }

    public function uploadDocument(Request $request){
        $file = $request->file->store('private/student_documents/'.$request->user()?$request->user()->id:\Auth::guard('student')->id(), 's3');
        $name = str_slug($request->file->getClientOriginalName());
        return \response()->json(['path' => $file, 'url' => 'https://mads.eu-central-1.linodeobjects.com/'.$file,'name'=>$name],
            200);
    }
    public function uploadAnyDocument(Request $request){
        $file = $request->file->store('private/apply', 's3');
        $name = str_slug($request->file->getClientOriginalName());
        return \response()->json(['path' => $file, 'url' => 'https://mads.eu-central-1.linodeobjects.com/'.$file,'name'=>$name],
            200);
    }
    public function uploadCV(Request $request){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        if($student->cv){
            try {
                Storage::disk('s3')->delete($student->cv);
            }catch(\Exception $ex){
                \Log::error($ex->getMessage());
            }
        }

        \Log::info("request:",$request->all());
        $file = $request->file->store('private/student_documents/'.$request->user()?$request->user()->id:\Auth::guard('student')->id(), 's3');
        $student->cv=$file;
        $student->cv_name = str_slug($student->name."_cv_".date('Ymd')).'.pdf';
        $student->cv_date = date('Y-m-d H:i:s');
        $student->save();
        $name = $request->file->getClientOriginalName();

        return \response()->json(['path' => $file, 'url' => 'https://mads.eu-central-1.linodeobjects.com/'.$file,'name'=>$student->cv_name],
            200);
    }
    public function deleteCV(Request $request){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem módosíthatják a profiljukat!"]],422);
        }
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        if($student->cv){
            try {
                Storage::delete($student->cv);
            }catch(\Exception $ex){
                \Log::error($ex->getMessage());
            }
        }
        $student->cv=null;
        $student->cv_name=null;
        $student->cv_date=null;
        $student->save();
        return \response()->json(['message' => 'Sikeres törlés']);
    }
    public function saveMyDocument(Request $request)
    {
        try {
            $document = StudentDocuments::where('student_id', $request->user()?$request->user()->id:\Auth::guard('student')->id())->where('id', $request->input('id'))->firstOrFail();
            $document->file=$request->input('new_file');
            $document->document_type_id=2;
            $document->name = $document->name."(Aláírt)";
            $document->signed_date = date('Y-m-d H:i:s');
            $document->save();
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function m30(Request $request)
    {
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail();
        $directories = Storage::disk('s3')->allDirectories('m30');
        $response = [];
        foreach ($directories as $d){
            if (Storage::disk('s3')->exists($d.'/'.$student->tax_number.'.pdf')) {
                $response[$d]=$d.'/'.$student->tax_number.'.pdf';
            }
        }
        return response()->json($response,200);

    }
    public function getM30(Request $request,$file){

        return Response::make(Storage::disk('s3')->get($file), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="m30.pdf"'
        ]);

    }
    public function getCerticicateHTML(Request $request){
        $resp ="";

       $cert = StudentCertificate::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->orderBy('end','desc')->first();
       if(!$cert){
           $resp="<div class='alert alert-danger h4 '>Nyilvántartásunk szerint nincs érvényes jogviszonyod</div>";
           return \response()->json(['data'=>$resp]);
       }
       if($cert->end < date('Y-m-d')){
            $resp="<div class='alert alert-danger h4'>Nyilvántartásunk szerint lejárt a jogviszonyod</div>";
       }else
        if($cert->end < date('Y-m-d',strtotime('+30 day')) || $cert->end <date('Y-m-d',strtotime('+60 day'))){
            $resp="<div class='alert alert-warning h4'>A jogviszonyod  hamarosan lejár: ".ConstansHelper::formatDate($cert->end)."</div>";
        }else
        if($cert->end >= date('Y-m-d',strtotime('+60 day'))){
            $resp="<div class='alert alert-success h4'>A jogviszonyod érvényes: ".ConstansHelper::formatDate($cert->end)."</div>";
        }
        return \response()->json(['data'=>$resp]);
    }
    public function getMySchedules(Request $request){
            $date = $request->input('interval');
            $date = explode(' - ',$date);
            $start =  date('Y-m-d',strtotime($date[0]));
            $end = date('Y-m-d',strtotime($date[1]));
            $days =[];
            $i=0;
            while($i<7){
                $day =date('Y-m-d',strtotime($start));
                $days[$day]=[
                    'value'=>$day,
                    'formatted' => ConstansHelper::formatDateShort($day),
                    'name'=>ConstansHelper::formatDateShort($day)
                ];
                $start = date('Y-m-d',strtotime($start.' +1 day'));

                $i++;
            }
            $schedules = ProjectSchedule::where('schedule_date','>=',$date[0])
                ->leftJoin('projects','projects.id','=','project_id')
                ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone,companies.name as company_name, company_industries.name as industry_name'))
                ->leftJoin('company_industries','company_industries.id','=','projects.industry_id')
                ->leftJoin('companies','companies.id','=','company_industries.company_id')

                ->where('schedule_date','<=',$end)->where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())
                ->orderBy('department')->orderBy('schedule_date')->orderBy('start')->get();
            $schedulesArray=[];
            foreach ($schedules as $sc){
                $schedulesArray[$sc->schedule_date][]=[
                    "end"=> ConstansHelper::formatTime($sc->end),
                    "have_time"=> true,
                    "name"=>$sc->company_name."<br>".$sc->industry_name.(($sc->department)?"<br>".$sc->department:''),
                    "schedule_date"=> $sc->schedule_date,
                    "start"=> ConstansHelper::formatTime($sc->start),
                ];
                $schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['name']=$sc->student_name;
                $schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['student_id']=$sc->student_id;
                $schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id]['phone']=$sc->student_phone;

            }
            return response()->json([
                'title' => str_replace('<br>',' ',ConstansHelper::formatDateShort($date[0]).'  -  '.ConstansHelper::formatDateShort($date[1])),
                'days' => $days,
                'schedules' => $schedulesArray,
            ],200);


    }
    public function subscribeSchedulesEditor(Request  $request,$base64){
        $d =json_decode(base64_decode($base64));
        $project = Project::where("id",$d->project_id)->first();
        $sch = new ProjectSchedule();
        $sch->student_id = $request->user()?$request->user()->id:\Auth::guard('student')->id();
        $sch->project_id = $d->project_id;
        $sch->industry_id = $project->industry_id;
        $sch->schedule_date = $request->input("date");
        $sch->start = $request->input("start");
        $sch->end = $request->input("end");
        $sch->department = $d->departmen;
        $sch->save();
        return $this->getMySchedulesEditor($request,$base64);
    }
    public function unsubscribeSchedulesEditor(Request  $request,$base64){
        $d =json_decode(base64_decode($base64));
        ProjectSchedule::where('schedule_date',$request->input("date"))->where('department',$d->departmen)->where('student_id', $request->user()?$request->user()->id:\Auth::guard('student')->id())->where('project_id',$d->project_id)->delete();
        return $this->getMySchedulesEditor($request,$base64);
    }
    public function saveStudentCard(OnlineStudentCardCreateRequest $request){
        if(Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem tölthetnek fel dokumentumot!"]],422);
        }
        $onlineCard = new OnlineStudentCard();
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();

        $onlineCard->fill($request->all());
        $onlineCard->student_id = $student->id;
        if($onlineCard->school_year){
            $onlineCard->start= $this->getStudentYear()[$onlineCard->school_year]['start'];
            $onlineCard->end= $this->getStudentYear()[$onlineCard->school_year]['end'];
        }
        if($onlineCard->semester){
            if($onlineCard->document_type == 1) {
                $onlineCard->start = $this->getSemesterCard()[$onlineCard->semester]['start'];
                $onlineCard->end = $this->getSemesterCard()[$onlineCard->semester]['end'];
            }
            if($onlineCard->document_type == 2) {
                $onlineCard->start = $this->getSemesterCertificate()[$onlineCard->semester]['start'];
                $onlineCard->end = $this->getSemesterCertificate()[$onlineCard->semester]['end'];
            }
        }
        $onlineCard->card_status=0;
        $onlineCard->save();
        $doc = new StudentDocuments();
        $doc->student_id = $student->id;
        $doc->document_type_id = 2;
        $doc->file = $onlineCard->new_file;
        $doc->type="card";
        $doc->name="Online diák/jogviszony igazolás";
        $doc->signed_date= date('Y-m-d H:i:s');
        $doc->created= date('Y-m-d H:i:s');
        $doc->save();
        return response()->json(['data'=>'OK'],200);
    }
    public function getMySchedulesEditor(Request $request,$base64){

        $base64 =json_decode(base64_decode($base64));
        $date = explode(' - ',$base64->interval);
        $start =  date('Y-m-d',strtotime($date[0]));
        $end = date('Y-m-d',strtotime($date[1]));
        $days =[];
        $schedulesArrayInfo=[];
        $i=0;
        while($i<7){
            $day =date('Y-m-d',strtotime($start));
            $days[$day]=[
                'value'=>$day,
                'formatted' => ConstansHelper::formatDateShort($day),
                'name'=>ConstansHelper::formatDateShort($day)
            ];
            $start = date('Y-m-d',strtotime($start.' +1 day'));
            $schInfo= ProjectScheduleInfo::where("project_id",$base64->project_id)->where("department",$base64->departmen)->where("schedule_date",$day)->first();
            $schedulesArrayInfo[$base64->departmen][$day] =[
                'id'=>$schInfo?$schInfo->id:null,
                'project_id'=>$base64->project_id,
                'industry_id'=>$base64->industry_id,
                'schedule_date'=>$day,
                'department'=>$base64->departmen,
                'start'=>$schInfo?$schInfo->start:'08:00',
                'end'=>$schInfo?$schInfo->end:'16:00',
                'student_count'=>  $schInfo?$schInfo->student_count:null,
                'info'=>  $schInfo?$schInfo->info:null,

            ];
            $i++;
        }
        $schedules = ProjectSchedule::where('schedule_date','>=',$date[0])
            ->leftJoin('projects','projects.id','=','project_id')
            ->leftJoin('students','student_id','=','students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone,companies.name as company_name, company_industries.name as industry_name'))
            ->leftJoin('company_industries','company_industries.id','=','projects.industry_id')
            ->leftJoin('companies','companies.id','=','company_industries.company_id')

            ->where('schedule_date','<=',$end)->where('department',$base64->departmen)
            ->orderBy('department')->orderBy('schedule_date')->orderBy('start')->get();
        $schedulesArray=[];
        foreach ($schedules as $sc){
            $schedulesArray[Str::slug(trim($sc->department),'-')][$sc->student_id][$sc->schedule_date]=[
                "end"=> ConstansHelper::formatTime($sc->end),
                "have_time"=> true,
                "schedule_date"=> $sc->schedule_date,
                "start"=> ConstansHelper::formatTime($sc->start),
                "exist" =>($sc->student_id==($request->user()?$request->user()->id:\Auth::guard('student')->id()))
            ];
        }
        return response()->json([
            'title' => str_replace('<br>',' ',ConstansHelper::formatDateShort($date[0]).'  -  '.ConstansHelper::formatDateShort($date[1])),
            'days' => $days,
            'schedules' => $schedulesArray,
            'schedulesInfo' => $schedulesArrayInfo,
            'expire'=>$base64->expire,
            'editable'=>$base64->expire>= date('Y-m-d'),
            'departmen'=>$base64->departmen,
            'student_exist'=>array_search($request->user()?$request->user()->id:\Auth::guard('student')->id(),(array)$base64->students)
        ],200);


    }
    public function applyWork(Request $request,$workId){

        if(Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nem jelentkezhetnek munkára!"]],422);
        }
        try {
            $work = Work::where('id', $workId)->first();
            $project = Project::where("id", $work->project_id)->first();
            $pm = User::where("id", $work->user_id)->first();
            $id = $request->user()?$request->user()->id:\Auth::guard('student')->id();
            $ps = ProjectStudent::where("project_id", $project->id)->where("student_id", $id)->first();
            $student =Student::where('id',$id)->first();
            if ($ps) {
                return response()->json(['errors' => ['errors' => "Már jelentkeztél erre a munkára"]], 422);
            }
            $ps = new ProjectStudent();
            $ps->student_id = $id;
            $ps->project_id = $project->id;
            $ps->status_id = 1;
            $ps->save();
            SaveStatistic::dispatch($work->user_id,'new_apply');
            SaveStatistic::dispatch($work->user_id,'all_apply');
            Mail::to($pm->email)->send(new \App\Mail\SendWorkEmail(
                $student->email,
                $student->name,
               "",
                $student->phone,
                null,
                $work->id

            ));

            return response()->json("Sikeres regisztráció", 200);

        }catch (\Exception $e){
            return response()->json(['errors' => ['errors' => "Ismeretlen hiba"]], 500);
        }

    }
    public function saveApplyTest(Request $request){
        $id = $request->user()?$request->user()->id:\Auth::guard('student')->id();
        $work = Work::where("id", $request->input("work_id"))->first();
        $pm = User::where("id", $work->user_id)->first();
        $student = Student::where('id', $id)->first();
        $answer = new ProjectStudentAnswer();
        $answer->fill($request->all());
        $answer->student_id = $id;
        $answer->save();
        $status=0;
        if($answer->percent <=0.4){
            $status=5;
        }
        else if($answer->percent > 0.9){
            $status=28;
        }else{
            $status=27;
        }
        $ps = ProjectStudent::where('project_id',$answer->project_id)->where('student_id',$answer->student_id)->first();
        if(!$ps){
            $ps = new ProjectStudent();
            $ps->project_id = $answer->project_id;
            $ps->student_id = $answer->student_id;
        }
        $ps->status_id = $status;
        $ps->save();
        SaveStatistic::dispatch($work->user_id,'new_apply');
        SaveStatistic::dispatch($work->user_id,'all_apply');
        Mail::to($pm->email)->send(new \App\Mail\SendWorkEmail(
            $student->email,
            $student->name,
            "",
            $student->phone,
            null,
            $work->id

        ));
        return response()->json($this->successMessage ,$this->successStatus);
    }
    function calcNet($discount,$student,$month,$price){
        if(!$discount || date("Y-m-d",strtotime($student->birth_date."+ 25 year"))<date("Y-m-d",strtotime($month.'01')) || $student->afa)
        {
            return $price*0.85;
        }
        if($price> Payroll::$underAgeLimit){
            return Payroll::$underAgeLimit+(($price-Payroll::$underAgeLimit)*0.85);
        }
        return  $price;
    }
    public function downPaymentRequest(Request $request){
        if( Student::where("id", $request->user()?$request->user()->id:\Auth::guard('student')->id())->firstOrFail()->exit_date){
            return response()->json(['errors' => ['errors' => "Kilépett diákok nemKérhetnek előleget!"]],422);
        }
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        $month = date('Y-m-d',strtotime($request->input('month').'-01'));
        $presens = StudentPresent::where('student_id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('date','LIKE','%'.date('Y-m',strtotime($month)).'%')->get();
        if(sizeof($presens)<1){
            return response()->json(["message"=>'Az adott hónapra nincs óraszámod rögzítve! Nem kérhetsz előleget!','error'=>true]);
        }
        if(date('Y-m-d',strtotime($request->input('month').'-01')) > date('Y-m-15',strtotime($request->input('month').'-01'))){
            return response()->json(["message"=>'Csak 15.-ig kérhetsz előleget','error'=>true]);
        }
        $existDownPaymen = DownPayment::where('student_id',$student->id)->where('month',date('Y-m',strtotime($request->input('month').'-01')))->first();
        if($existDownPaymen){
            return response()->json(["message"=>'Erre a hónapra már kértél előleget!','error'=>true]);
        }
        $down_payment= new DownPayment();
        $down_payment->student_id = $student->id;
        $down_payment->approved = false;
        $down_payment->month=date('Y-m',strtotime($request->input('month').'-01'));
        $down_payment->save();
        try {
            Mail::to("ember.agnes@mads.hu")->send(new \App\Mail\SendDownPaymentEmail($student->name,$student->email));
        }catch (\Exception $e){

        }
        return response()->json(["message"=>'A kérésed továbbítottuk a pénzügy felé! Amint a pénzügy jóváhagyta a kérésed emailben értesítünk! Várhatóan 20-25-e között utaljuk.','error'=>false]);
    }
    public function getSemesterCard(){
        $semester = Semester::where('type',1)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($semester as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }
        return $response;
    }
    public function getStudentYear(){
        $tanev = Semester::where('type',2)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($tanev as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }

        return $response;
    }
    public function getSemesterCertificate(){
        $semester = Semester::where('type',3)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($semester as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }
        return $response;
    }
    public function getRealization(Request $request){
        $realization = Realization::where('student_id','=',$request->user()?$request->user()->id:\Auth::guard('student')->id())->where('end','>=',date('Y-m-d'))->orderBy('end','desc')->get();
        return RealizationListResource::collection($realization);
    }
    public function addRealization(RealizationCreateFrontendRequest $request){

        $school =new Realization();
        $school->fill($request->all());
        $school->student_id = $request->user()?$request->user()->id:\Auth::guard('student')->id();
        $school->save();
        return response()->json($this->successMessage,$this->successStatus);
    }
    public function deleteRealization(Request $request,$id){
        try {
            $school = Realization::where('id','=',$id)->firstOrFail();
            $school->delete();
            return response()->json($this->successMessage ,$this->successStatus);

        }	catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getTetsByWork(Request $request,$work,$student)
    {
        $project= Project::where('id',2602)->first();

        return response()->json(['project_id'=>$project->id,'student_id'=>85717,'tests'=>$project->test_list]);
    }
    public function saveTest(Request $request)
    {
        $answer = new ProjectStudentAnswer();
        $answer->fill($request->all());
        $answer->save();
        $status=0;
        if($answer->percent <=0.4){
            $status=5;
        }
        else if($answer->percent > 0.9){
            $status=28;
        }else{
            $status=27;
        }
        $ps = ProjectStudent::where('project_id',$answer->project_id)->where('student_id',$answer->student_id)->first();
        if(!$ps){
            $ps = new ProjectStudent();
            $ps->project_id = $answer->project_id;
            $ps->student_id = $answer->student_id;
        }
        $ps->status_id = $status;
        $ps->save();
        return response()->json($this->successMessage ,$this->successStatus);
    }
}
