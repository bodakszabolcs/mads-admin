<?php

namespace Modules\WebshopFrontend\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \DB::table('menus')->delete();

        \DB::table('menus')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'Webshop menu',
                    'available_languages' => NULL,
                    'created_at' => '2020-03-12 15:41:00',
                    'updated_at' => '2020-03-12 15:41:00',
                    'deleted_at' => NULL,
                ),
        ));
    }
}
