<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/','middleware' => ['auth:sanctum']], function () {
    Route::get('/checkout/index/auth', 'CheckoutController@index')->name('Checkout with auth');
    Route::get('/orders/my-orders', 'OrderController@getMyOrders')->name('Get my orders');
});
Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/','middleware' => []], function () {
    Route::post('/work-apply', 'WebshopFrontendController@workApply')->name('apply work');
    Route::get('/get-test/{id}', 'WebshopFrontendController@getTests')->name('getTests');
    Route::get('/check-email/{email}', 'WebshopFrontendController@checkEmail')->name('check');
    Route::post('/request/save', 'WebshopFrontendController@saveRequest')->name('save request');
    Route::get('/request/user', 'WebshopFrontendController@getContactRequest')->name('get contact request');
    Route::get('/search-city', 'WebshopFrontendController@searchCity')->name('search city');
    Route::get('/search-category', 'WebshopFrontendController@searchCategory')->name('search category');
    Route::get('/highlighted-work', 'WebshopFrontendController@getHighlightedWork')->name('Frontend get highlighted work');
    Route::get('/last-work', 'WebshopFrontendController@getLastWork')->name('Frontend get last work');
    Route::get('/related-work/{id}/{category_id}', 'WebshopFrontendController@getRelatedWork')->name('Frontend get related work');
    Route::get('/work-categories', 'WebshopFrontendController@getWorkCategories')->name('Frontend get WorkCategories');
    Route::get('/work-categories-with-number', 'WebshopFrontendController@getWorkCategoryThreeWithNumbers')->name('Frontend get WorkCategories with number');
    Route::get('/work-list/{cat?}/{sub?}', 'WebshopFrontendController@getWorkList')->name('Frontend get WorkList');
    Route::get('/work/{cat}/{sub}/{slug}', 'WebshopFrontendController@getWorkBySlug')->name('Frontend get work by slug');
    Route::get('/members', 'WebshopFrontendController@getMembers')->name('Frontend get members');
    Route::get('/downloadable-content', 'WebshopFrontendController@getDownloadableContent')->name('Frontend get downloadable content');
    Route::get('/get-slides', 'WebshopFrontendController@getSlides')->name('Frontend get slides');
    Route::get('/get-partner-logos', 'WebshopFrontendController@getPartnerLogos')->name('Frontend get partner logos');
    Route::get('/menu-category', 'WebshopFrontendController@getMenuCategory')->name('Frontend get menu category');
    Route::get('/get-home-pages', 'WebshopFrontendController@getHomePages')->name('Get HomePages');
    Route::get('/home', 'WebshopFrontendController@getHomeProducts')->name('Frontend home page products');
    Route::get('/members', 'WebshopFrontendController@getMembers')->name('Frontend get members');
    Route::post('/message', 'WebshopFrontendController@Message')->name('Frontend send message');
    Route::get('/student/register/get-selectables', 'WebshopFrontendController@getSelectables')->name('Frontend get selectables');
    Route::post('/upload-any-document', 'StudentController@uploadAnyDocument')->name('uploadDocument');
    Route::post('/student/save-test', 'StudentController@saveTest')->name('saveTest');
    Route::get('/student/get-test/{work}/{student}', 'StudentController@getTetsByWork')->name('getTetsByWork');
});

Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/cart','middleware' => []], function () {
    Route::post('/add-to-cart/{uuid?}', 'CartController@addToCart')->name('Product add to cart');
    Route::get('/my-cart/{uuid?}', 'CartController@getMyCart')->name('Get my cart');

    Route::delete('/remove/{uuid}/{id}', 'CartController@removeFromCart')->name('Remove product from cart');
    //Route::post('/coupon/{uuid}', 'CartController@applyCoupon')->name('Apply coupon');
    //Route::delete('/remove-coupon/{uuid}', 'CartController@removeCoupon')->name('Remove coupon');

});
Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '/student','middleware' => ['auth:sanctum','student','api']], function () {

    Route::get('/get-schools', 'StudentController@getSchools')->name('Get schools');
    Route::post('/save-apply-test', 'StudentController@saveApplyTest')->name('Get schools');
    Route::get('/get-project-realization', 'StudentController@getProjectRealization')->name('Get schools');
    Route::get('/get-project-realization/{hash}', 'StudentController@getProjectRealizationByHash')->name('Get schools');
    Route::post('/save-porject-realization', 'StudentController@saveProjectRealization')->name('Save online student_card');
    Route::post('/save-online-student-card', 'StudentController@saveStudentCard')->name('Save online student_card');
    Route::get('/api-dashboard', 'StudentController@getApiDashboard')->name('Api dashboard');
    Route::post('/work-apply/{workId}', 'StudentController@applyWork')->name('apply work');
    Route::delete('/delete-school/{id}', 'StudentController@deleteSchools')->name('delete schools');
    Route::post('/add-student-school/{id}', 'StudentController@addSchools')->name('add schools');
    Route::match(['post'], '/add-realization', 'StudentController@addRealization')->name('Diák ráérés rögzítése');
    Route::match(['delete'], '/delete-realization/{id}', 'StudentController@deleteRealization')->name('Diák ráérés törlése');
    Route::match(['get'], '/get-realization', 'StudentController@getRealization')->name('Diák ráérések lekérdezése');
    Route::get('/get-languages', 'StudentController@listLanguage')->name('Get languages');
    Route::delete('/delete-language/{id}', 'StudentController@deleteLanguage')->name('delete languages');
    Route::post('/add-languages', 'StudentController@addLanguage')->name('add languages');
    Route::post('/save-registration', 'StudentController@registerMemeber')->name('save registration');
    Route::put('/update-profile', 'StudentController@updateProfile')->name('save registration');
    Route::get('/my-m30', 'StudentController@m30')->name('get my m30');
    Route::post('/down-payment-request', 'StudentController@downPaymentRequest')->name('down payment request');
    Route::get('/get-my-schedules-editor/{hash}', 'StudentController@getMySchedulesEditor')->name('payment schedule editor');
    Route::post('/subscribe-schedules-editor/{hash}', 'StudentController@subscribeSchedulesEditor')->name('payment schedule editor');
    Route::post('/unsubscribe-schedules-editor/{hash}', 'StudentController@unsubscribeSchedulesEditor')->name('payment schedule editor');

    Route::get('/get-my-presents/{month?}', 'StudentController@getMyPresents')->name('Get my presents');
    Route::get('/get-my-work-safety', 'StudentController@getMyWorkSafety')->name('Get my work safety');
    Route::delete('/delete-work-safety/{id}', 'StudentController@deleteWorkSafety')->name('delete work safety');
        Route::post('/create-work-safety', 'StudentController@saveWorkSafety')->name('create work safety');
    Route::get('/get-my-cert', 'StudentController@getCerticicateHTML')->name('Get my cert');
    Route::get('/get-my-finance/{month?}', 'StudentController@getMyFinance')->name('Get my finance');
    Route::post('/save-my-presents', 'StudentController@savePresents')->name('save my presents');
    Route::get('/get-my-schedule', 'StudentController@getMySchedules')->name('getMySchedule');
    Route::get('/get-my-payroll/{year}', 'StudentController@getMyPayroll')->name('getMyPayroll');
    Route::get('/get-my-documents', 'StudentController@getMyDocuments')->name('getMyDocuments');
    Route::get('/get-my-apply', 'StudentController@getMyApply')->name('getMyApply');
    Route::get('/get-my-notification', 'StudentController@getMyNotification')->name('getMyNotification');

    Route::post('/upload-document', 'StudentController@uploadDocument')->name('uploadDocument');
    Route::post('/upload-cv', 'StudentController@uploadCv')->name('uploadCv');
    Route::delete('/delete-cv', 'StudentController@deleteCv')->name('deleteCv');
    Route::post('/save-my-document', 'StudentController@saveMyDocument')->name('saveMyDocument');
    Route::post('/check-registration-link', 'StudentController@checkRegistrationLink')->name('checkRegistrationLink');



});
Route::group(['namespace' => 'Modules\WebshopFrontend\Http\Controllers','prefix' => '','middleware' => ['student']], function () {
    Route::get('/print-payroll/{month}', 'StudentController@printPayroll')->name('print payroll');
    Route::get('/student/get-my-document/{id}', 'StudentController@getMyDocument')->name('getMyDocument');
    Route::get('/student/get-my-cv', 'StudentController@getMyCv')->name('getMyCv');
    Route::get('/student/presents-pdf/{industry}/{month}', 'StudentController@getPresentPDF')->name('Frontend get presents');
    Route::get('/student/get-my-m30/{file}', 'StudentController@getM30')->name('get my m30')->where('file', '.*');
});
