<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class SearchResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
