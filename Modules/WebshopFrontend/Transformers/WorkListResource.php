<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class WorkListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'category'=>$this->getCategorySlug(),
            'slug'=> $this->slug,
            'lead'=>$this->lead
        ];
    }
}
