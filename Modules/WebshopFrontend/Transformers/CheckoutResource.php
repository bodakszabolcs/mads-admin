<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Modules\Order\Entities\Cities;
use Modules\Order\Entities\PaymentMethod;
use Modules\Order\Entities\ShippingMethod;
use Modules\Order\Transformers\PaymentMethodListResource;
use Modules\Order\Transformers\PaymentMethodViewResource;
use Modules\Order\Transformers\ShippingMethodListResource;
use Modules\Order\Transformers\ShippingMethodViewResource;
use Modules\Webshop\Entities\Payment;
use Modules\Webshop\Entities\Shipment;


class CheckoutResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'guestCheckout' => config('webshopfrontend.quest_checkout'),
            'loggedIn' => \Auth::user() ? true: false,
            'user_id' =>optional(\Auth::user())->id,
            'billing_name'=> optional(optional(\Auth::user())->billing)->name,
            'billing_company_name'=> optional(optional(\Auth::user())->billing)->company_name,
            'billing_country'=> optional(optional(\Auth::user())->billing)->country,
            'billing_zip'=> optional(optional(\Auth::user())->billing)->zip,
            'billing_city'=> optional(optional(\Auth::user())->billing)->city,
            'billing_address'=> optional(optional(\Auth::user())->billing)->address,
            'billing_email'=> optional(optional(\Auth::user())->billing)->email,
            'billing_phone'=> optional(optional(\Auth::user())->billing)->phone,
            'vat_number'=> optional(optional(\Auth::user())->billing)->vat_number,

            'shipping_name'=> optional(optional(\Auth::user())->shipping)->name,
            'shipping_email'=> optional(optional(\Auth::user())->shipping)->email,
            'shipping_country_id'=> (optional(optional(\Auth::user())->shipping)->country_id)?\Auth::user()->shipping->country_id:1,
            'shipping_address'=> optional(optional(\Auth::user())->shipping)->address,
            'shipping_city'=> optional(optional(\Auth::user())->shipping)->city,
            'shipping_zip'=> optional(optional(\Auth::user())->shipping)->zip,
            'shipping_phone'=> optional(optional(\Auth::user())->shipping)->phone,
            'shipping_note'=> optional(optional(\Auth::user())->shipping)->note,
            'shipping_is_same' => false,
            'selectables'=>[
                'countries' => json_decode(json_encode(config('countries'))),
                'shipping_methods'=> ShippingMethodListResource::collection(ShippingMethod::all()),
                'payment_methods'=> PaymentMethodListResource::collection(PaymentMethod::all()),
            ]
        ];
    }
}
