<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;

class ProductQuestionResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'useful' => $this->useful,
            'not_useful' => $this->not_useful,
            'name' => $this->name,
            'title' =>$this->title,
            'comment'=>$this->comment,
            'answer'=>$this->answer,
            'publish'=> date('Y/m/d',strtotime($this->created_at))
        ];
    }

}
