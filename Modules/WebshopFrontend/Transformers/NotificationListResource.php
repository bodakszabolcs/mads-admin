<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Helpers\ConstansHelper;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class NotificationListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'link' => $this->link,
            'description' => $this->description,
            'created_at' =>ConstansHelper::formatDate($this->created_at)
        ];
    }
}
