<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\EducationArea\Entities\EducationArea;
use Modules\EducationLevel\Entities\EducationLevel;
use Modules\Language\Entities\Language;
use Modules\Student\Entities\Student;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkLocation\Entities\WorkLocation;

class RegistrationResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {
        return [
               'education_areas'=> EducationArea::all()->pluck('name','id'),
               'education_levels'=> EducationLevel::all()->pluck('name','id'),
               'work_categories'=> WorkCategory::whereNull('parent_id')->pluck('name','id'),
               'work_locations'=> WorkLocation::all()->pluck('name','id'),
               'languages'=> Language::orderBy('name')->get()->pluck('name','id'),
               'genders'=> json_decode(json_encode(Student::$gender))
            ];
    }


}
