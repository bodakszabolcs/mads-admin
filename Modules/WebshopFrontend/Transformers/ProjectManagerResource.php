<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class ProjectManagerResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->lastname.' '.$this->firstname,
            'email' => $this->email,
            'phone' => $this->phone,

        ];
    }


}
