<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\Webshop\Entities\Product;
use Modules\Webshop\Entities\Variation;
use Modules\WebshopFrontend\Entities\ProductVariationList;


class EventDealsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */


    public function toArray($request)
    {
        return [
            "id"=> $this->id,
            "slug" => $this->slug,
            "title" => $this->title,
            "image" => $this->image,
            "date" =>$this->date,
            "discount_to" => strtotime($this->date)

        ];
    }

}
