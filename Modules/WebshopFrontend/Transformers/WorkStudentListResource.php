<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\ProductVariation\Entities\ProductVariation;
use Modules\WorkCategory\Entities\WorkCategory;

class WorkStudentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $category = WorkCategory::where("id",$this->work_category_id)->first();
        $parent = WorkCategory::where("id",optional($category)->parent_id)->first();
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => '/munkak/'.optional($parent)->slug.'/'.optional($category)->slug.'/'.$this->slug,
            'lead' => $this->lead,
            'image' => $this->image,
        ];
    }

}
