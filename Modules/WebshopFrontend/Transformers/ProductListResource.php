<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Modules\Webshop\Entities\Product;
use Modules\Webshop\Entities\Variation;
use Modules\WebshopFrontend\Entities\ProductVariationList;


class ProductListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */


    public function toArray($request)
    {


        return [
            "id"=> $this->id,
            "name"=> $this->name,
            "slug"=> $this->slug,
            "rating_count" => $this->rating_count,
            "rating_avg" => $this->avg_rate,
            "variations" => ProductVariationListResource::collection($this->variations()->where('active','=',1)->where('in_stock','>',0)->orderBy('price','asc')->get()),

        ];
    }

}
