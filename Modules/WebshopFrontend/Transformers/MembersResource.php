<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\ProductVariation\Entities\ProductVariation;
class MembersResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [

            'name' => $this->lastname.' '.$this->firstname,
            'phone' =>$this->phone,
            'email' => $this->email,
            'position' => $this->position,
            'avatar' =>$this->avatar

        ];
    }

}
