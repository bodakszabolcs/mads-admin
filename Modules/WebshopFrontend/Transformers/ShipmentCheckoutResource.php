<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;


class ShipmentCheckoutResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->getTranslatable('name'),
            "description" => $this->getTranslatable('description'),
            "price_net" => $this->price_net,
            "price_gross" => $this->price_gross,
            "price_net_formatted" => $this->generateFormattedPrices($this->price_net),
            "price_gross_formatted" => $this->generateFormattedPrices($this->price_gross),
        ];
    }

    private function generateFormattedPrices($array) {
        $index = 0;
        foreach ($array as $arr) {
            $array[$index] = price_format($arr, $index);
            $index++;
        }

        return $array;
    }
}
