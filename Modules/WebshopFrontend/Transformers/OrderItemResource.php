<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;


class OrderItemResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "product_name" => $this->product_name,
            "quantity" => $this->quantity,
            "net_value" => price_format($this->net_value, optional($this->order)->currency_id),
            "gross_value" => price_format($this->gross_value, optional($this->order)->currency_id),
            "tax_rate" => price_format($this->tax_rate, -1, null, '%'),
            "total" => price_format($this->gross_value * $this->quantity, optional($this->order)->currency_id),
            "variation" => new VariationDetailsResource($this->variation,null),
        ];
    }



}
