<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Modules\Category\Transformers\CategoryTreeResource;
use Modules\Webshop\Entities\Coupon;
use Modules\Webshop\Entities\ProductCategory;
use Modules\Webshop\Entities\ProductStock;
use Modules\Webshop\Entities\Stock;
use Modules\Webshop\Entities\VariationPrice;
use Modules\Webshop\Transformers\Product\ProductViewResource;


class VariationDetailsResource extends VariationCollectionDetailsResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function __construct($resource, Coupon $coupon = null)
    {
        $this->coupon = $coupon;
        parent::__construct($resource);
    }

}
