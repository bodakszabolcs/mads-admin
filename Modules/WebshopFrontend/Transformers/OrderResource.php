<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class OrderResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            'discount' => $this->discount,
            'shipping_price' => $this->shipping_price,
            'total_with_shipping' => $this->order_total,
            'status'=>$this->orderStatus->name,
            "details" =>[
                'billing_name'=> optional($this->details)->billing_name,
                'billing_company_name'=> optional($this->details)->billing_company_name,
                'billing_country'=> optional($this->details)->billing_country,
                'billing_zip'=> optional($this->details)->billing_zip,
                'billing_city'=> optional($this->details)->billing_city,
                'billing_address'=>optional( $this->details)->billing_address,
                'billing_email'=> optional($this->details)->billing_email,
                'billing_phone'=> optional($this->details)->billing_phone,
                'vat_number'=> optional($this->details)->vat_number,
                'payment_method' => optional($this->paymentMethod)->name,
                'payment_method_id' => optional($this->paymentMethod)->id,
                'shipping_method' => optional($this->shippingMethod)->name,

                'shipping_name'=> optional($this->details)->shipping_name,
                'shipping_email'=> optional(optional(\Auth::user())->shipping)->email,
                'shipping_country'=> Arr::get(config('countries'),optional($this->details)->shipping_country_id),
                'shipping_country_id'=> optional($this->details)->shipping_country_id,
                'shipping_address'=>optional($this->details)->shipping_address,
                'shipping_city'=> optional($this->details)->shipping_city,
                'shipping_zip'=> optional($this->details)->shipping_zip,
                'shipping_phone'=>optional($this->details)->shipping_phone,
                'shipping_note'=> optional($this->details)->shipping_note,
                'shipping_is_same' => false,

            ],
            "items"=> WorkStudentListResource::collection($this->items)
        ];
    }
}
