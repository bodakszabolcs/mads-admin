<?php

namespace Modules\WebshopFrontend\Transformers;

use App\Http\Resources\BaseResource;
use http\Client\Curl\User;
use Illuminate\Support\Arr;
use Modules\Webshop\Entities\Attribute;
use Modules\Webshop\Transformers\Attribute\AttributeListResource;
class ProductRatingResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'rate' => $this->rate,
            'useful' => $this->useful,
            'not_useful' => $this->not_useful,
            'name' => $this->name,
            'title' =>$this->title,
            'comment'=>$this->comment,
            'publish'=> date('Y/m/d',strtotime($this->created_at))
        ];
    }

}
