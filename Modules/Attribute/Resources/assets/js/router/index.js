import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Attribute from '../components/Attribute'
import AttributeList from '../components/AttributeList'
import AttributeCreate from '../components/AttributeCreate'
import AttributeEdit from '../components/AttributeEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'attributes',
                component: Attribute,
                meta: {
                    title: 'Attributes'
                },
                children: [
                    {
                        path: 'index',
                        name: 'AttributeList',
                        component: AttributeList,
                        meta: {
                            title: 'Attributes',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/attributes/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/attributes/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'AttributeCreate',
                        component: AttributeCreate,
                        meta: {
                            title: 'Create Attributes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/attributes/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'AttributeEdit',
                        component: AttributeEdit,
                        meta: {
                            title: 'Edit Attributes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/attributes/index'
                        }
                    }
                ]
            }
        ]
    }
]
