<?php

namespace Modules\Attribute\Observers;

use Modules\Attribute\Entities\Attribute;

class AttributeObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function saved(Attribute $model)
    {
        Attribute::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function created(Attribute $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function updated(Attribute $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function deleted(Attribute $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function restored(Attribute $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Attribute\Entities\Attribute  $model
     * @return void
     */
    public function forceDeleted(Attribute $model)
    {
        //
    }
}
