<?php

namespace Modules\Attribute\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Attribute\Entities\Attribute;
use Illuminate\Http\Request;
use Modules\Attribute\Http\Requests\AttributeCreateRequest;
use Modules\Attribute\Http\Requests\AttributeUpdateRequest;
use Modules\Attribute\Transformers\AttributeViewResource;
use Modules\Attribute\Transformers\AttributeListResource;

class AttributeController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Attribute();
        $this->viewResource = AttributeViewResource::class;
        $this->listResource = AttributeListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = AttributeCreateRequest::class;
        $this->updateRequest = AttributeUpdateRequest::class;
    }

}
