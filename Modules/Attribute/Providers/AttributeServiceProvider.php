<?php

namespace Modules\Attribute\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Attribute\Entities\Attribute;
use Modules\Attribute\Observers\AttributeObserver;

class AttributeServiceProvider extends ModuleServiceProvider
{
    protected $module = 'attribute';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Attribute::observe(AttributeObserver::class);
    }
}
