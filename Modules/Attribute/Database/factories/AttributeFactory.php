<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Attribute\Entities\Attribute;

$factory->define(Attribute::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
