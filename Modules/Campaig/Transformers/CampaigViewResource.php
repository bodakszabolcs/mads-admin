<?php

namespace Modules\Campaig\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\CampaignStatus\Entities\CampaignStatus;


class CampaigViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            "id" => $this->id,
            "name" => $this->name,
            "deadline" => $this->deadline,
            "status_list" => $this->status_list?$this->status_list: array(),
            "selectables" => array(
                'statusList' => CampaignStatus::select('id','name','o')->orderBy('o')->get()
            )
        );
    }
}
