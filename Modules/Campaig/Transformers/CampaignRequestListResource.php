<?php

namespace Modules\Campaig\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CampaignRequestListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "campaign_id" => $this->campaign_id,
            "company_name" => optional($this)->company_name,
		    "name" => $this->name,
		    "request" => $this->request,
		    "approve_details" => $this->approve_details,
		    "approved" => $this->approved,
		    "approved_bool" =>($this->approved ==2) ? 'Elutasítva': ConstansHelper::bool($this->approved).$this->getDetails(),
		    "email" => $this->email,
		    "documentum" => $this->documentum,
		    "updated_at" => ConstansHelper::formatShortDateTime($this->updated_at),
		     ];
    }
    private function getDetails(){
        $resp =[];
        foreach ($this->request as $k =>$v){
            if(isset($v['approved']) && $v['approved']){
                $resp[] =$v['name'];
            }
        }
        if(!sizeof($resp)){
            return "";
        }
        return " (".implode(",",$resp).")";
    }
}
