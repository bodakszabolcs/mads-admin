<?php

namespace Modules\Campaig\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\CampaignStatus\Entities\CampaignStatus;


class ContractSablonResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return array(
            "id" => $this->id,
            "sablon" => $this->sablon,
            "hu" => $this->hu,
            "en" => $this->en
        );
    }
}
