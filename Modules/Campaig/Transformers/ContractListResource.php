<?php

namespace Modules\Campaig\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ContractListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->data['name'],
		    "number" => $this->data['number'],
		    "type" => $this->type,
            "mads_approved"=>$this->mads_approved,
            "mads_approved_bool"=>ConstansHelper::bool($this->mads_approved),
            "mads_updated"=>ConstansHelper::formatShortDateTime($this->mads_updated),
            "company_approved"=>$this->company_approved,
            "company_approved_bool"=>ConstansHelper::bool($this->company_approved),
            "company_updated"=>ConstansHelper::formatShortDateTime($this->company_updated),
		     ];
    }
}
