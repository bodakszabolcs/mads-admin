<?php

namespace Modules\Campaig\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CampaigQuestionListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "campaign_id" => $this->campaign_id,
            "company_id" => $this->company_id,
            "company_name" => optional($this)->company_name,
		    "name" => $this->name,
		    "questions" => $this->questions,
		    "answered" => $this->answered,
		    "answered_bool" => ConstansHelper::bool($this->answered),
		    "email" => $this->email,
		    "updated_at" => ConstansHelper::formatShortDateTime($this->updated_at),
		     ];
    }
}
