<?php

namespace Modules\Campaig\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CampaigListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "deadline" => $this->deadline,
		    "companies_count" => $this->companies_count,
		    "state" => $this->companies_count.'/'.$this->finished,
		    "state_percent" => (ceil(($this->companies_count>0)?($this->finished/$this->companies_count)*100:0)).' %',
		     ];
    }
}
