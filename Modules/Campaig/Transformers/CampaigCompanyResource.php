<?php

namespace Modules\Campaig\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Campaig\Entities\CampaignCompanyContract;
use Modules\CampaignStatus\Entities\CampaignStatus;
use Modules\Company\Transformers\ContactViewResource;


class CampaigCompanyResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    private $statusList;
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $contracts = CampaignCompanyContract::where('company_id',$this->company_id)->where('campaign_id',$this->campaign_id)->get();
        return array(
            "id" => $this->id,
            "company_id" => $this->company_id,
            "campaign_id" => $this->campaign_id,
            "company_name" => $this->company_name,
            "contact_name"=>$this->contact_name,
            "phone"=>$this->phone,
            "contact_email"=>$this->contact_email,
            "contracts"=>$contracts?$contracts:[],
            "services"=>$this->services,
            "comment"=>$this->comment,
            "status_name" => $this->status_name,
            "status_id" => $this->status_id,
            "updated_at" => ConstansHelper::formatShortDateTime($this->updated_at)


        );
    }
}
