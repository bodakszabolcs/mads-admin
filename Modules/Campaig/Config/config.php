<?php

return [
    'name' => 'Értékesítés',

    'menu_order' => 6,

    'menu' => [
        [

            'icon' => 'flaticon-whatsapp',

            'title' => 'Értékesítés',

            'route' => '#',
            'submenu' =>
                [
                    [

                        'icon' => 'flaticon-earth-globe',

                        'title' => 'Webes árajánlatok',

                        'route' => '/' . env('ADMIN_URL') . '/request/index-web?web=1',

                    ],
                    [

                        'icon' => 'flaticon-email-black-circular-button',

                        'title' => 'Telefonos árajánlatok',

                        'route' => '/' . env('ADMIN_URL') . '/request/index?web=0',

                    ],
                    [
                        'icon' => 'flaticon-graph', 'title' => 'Értékesítési kampányok', 'route' => '/' . env('ADMIN_URL') . '/campaign/index'
                    ],
                    [

                        'icon' =>'fa fa-comments',

                        'title' =>'Árajánlatok válaszok',

                        'route' =>'/'.env('ADMIN_URL').'/campaign/question-list',

                    ],
                    [

                        'icon' =>'fa fa-envelope-open',

                        'title' =>'Kiküldött árajánlatok',

                        'route' =>'/'.env('ADMIN_URL').'/campaign/request-list',

                    ],
                    [

                        'icon' =>'fa fa-handshake',

                        'title' =>'Elküldött szerződések',

                        'route' =>'/'.env('ADMIN_URL').'/campaign/contract-list',

                    ],
                    [

                        'icon' =>'flaticon-list-1',

                        'title' =>'Kampány státuszok',

                        'route' =>'/'.env('ADMIN_URL').'/campaign-status/index',

                    ],

                    [

                        'icon' =>'flaticon-questions-circular-button',

                        'title' =>'Árajánlat kérdések',

                        'route' =>'/'.env('ADMIN_URL').'/question/index',

                    ],
                    [

                        'icon' =>'fa fa-file-pdf',

                        'title' =>'Szerződés sablonok',

                        'route' =>'/'.env('ADMIN_URL').'/campaign/contract-template/list',

                    ]
                ]

        ]

    ]
];
