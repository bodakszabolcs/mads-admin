<?php

namespace Modules\Campaig\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CampaignTask extends BaseModel
{
    use  Cachable;
	protected $table = "campaign_tasks";
	protected $fillable =['deadline','status','description','campaign_id'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
