<?php

namespace Modules\Campaig\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class ContractSablon extends BaseModel
{
    use  Cachable;
	protected $table = "contract_sablon";
	protected $fillable =['sablon','hu','en'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    protected $casts =['hu'=>'json','en'=>'json'];







}
