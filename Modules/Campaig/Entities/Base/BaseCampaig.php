<?php

namespace Modules\Campaig\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\DB;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Transformers\CampaigCompanyResource;
use Spatie\Translatable\HasTranslations;


abstract class BaseCampaig extends BaseModel
{


    protected $table = 'campaigns';

    protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
            'name' => 'name',
            'title' => 'Név',
            'type' => 'text',
        ], [
            'name' => 'deadline',
            'title' => 'Határidő',
            'type' => 'text',
        ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name', 'deadline', 'status_list'];

    protected $casts = ['status_list' => 'array'];


    public function companies(){
        $list = $this->getCompanyQuery();
        return $list->get();
    }
    public function getCompanyQuery(){
        return CampaignCompany::leftJoin('companies','companies.id','=','campaign_companies.company_id')
            ->leftJoin('campaign_status','campaign_status.id','=','status_id')
            ->leftJoin('company_contacts','company_contacts.company_id','=','companies.id')
            ->groupBy('companies.id')
            ->where('campaign_id',$this->id)
            ->select(DB::raw('campaign_companies.id as id, companies.id as company_id,companies.name as company_name,company_contacts.name as contact_name,company_contacts.email as contact_email,company_contacts.phone as phone,campaign_status.name as status_name,campaign_status.id as status_id,campaign_companies.comment,campaign_companies.campaign_id as campaign_id, campaign_companies.updated_at,campaign_companies.services'));
    }
}
