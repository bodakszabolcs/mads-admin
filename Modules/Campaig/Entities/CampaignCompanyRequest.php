<?php

namespace Modules\Campaig\Entities;

use App\BaseModel;
use App\Helpers\PdfHelper;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\Storage;

class CampaignCompanyRequest extends BaseModel
{
    use  Cachable;
	protected $table = "campaign_company_request";
	protected $fillable =['campaign_id','company_id','name','email','request','documentum','other_recipients','approve_details'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    protected $casts =['request'=>'json','other_recipients'=>'array','approve_details'=>'array'];

    public function genereteRequestPDF($preview=false){

        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        $ccr = $this;

        PdfHelper::createContractPdf(view('request.request_header', ['ccr' => $ccr]),
            null, null, '/tmp/' .$ccr->id . 'header.pdf',
            ['margin_top' => 50, 'margin_bottom' => 10,'type'=>'request','type'=>'request']);
        $pdf->AddPage();
        $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id . 'header.pdf');
        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink('/tmp/' .  $ccr->id . 'header.pdf');

        if(isset($ccr['request'][3]['properties']) &&  sizeof($ccr['request'][3]['properties'])>0){
                PdfHelper::createContractPdf(view('request.request_work_header', ['ccr' => $ccr]),
                    null, null, '/tmp/' . $ccr->id . 'work_header.pdf',
                    ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
                $pdf->AddPage();
                $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id . 'work_header.pdf');
                for ($i = 1; $i <= $pageCount; $i++) {

                    $tplIdx = $pdf->importPage($i);
                    //$wh = $pdf->getTemplateSize($tplIdx);
                    if (($i == 1)) {
                        //$pdf->state = 0;
                        $pdf->UseTemplate($tplIdx);
                    } else {
                        //$pdf->state = 1;
                        $pdf->AddPage();
                        $pdf->UseTemplate($tplIdx);
                    }

                }
                unlink('/tmp/' . $ccr->id . 'work_header.pdf');


         }
        foreach ($ccr['request'][3]['properties'] as $key => $item) {

            PdfHelper::createContractPdf(view('request.request_work', ['ccr' => $item,'type'=>0]),
                null, null, '/tmp/' . $ccr->id . '-' . $key . 'work.pdf',
                ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id . '-' . $key . 'work.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $ccr->id . '-' . $key . 'work.pdf');
        }
        foreach ($ccr['request'][1]['properties'] as $key => $item) {

            PdfHelper::createContractPdf(view('request.request_work', ['ccr' => $item,'type'=>1]),
                null, null, '/tmp/' . $ccr->id . '-' . $key . 'work2.pdf',
                ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id . '-' . $key . 'work2.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $ccr->id . '-' . $key . 'work2.pdf');
        }
        foreach ($ccr['request'][0]['properties'] as $key=>$item) {

            PdfHelper::createContractPdf(view('request.request_mads', ['ccr' => $item]),
                null, null, '/tmp/' . $ccr->id .'-'.$key. 'mads.pdf',
                ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id .'-'.$key. 'mads.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $ccr->id .'-'.$key. 'mads.pdf');
        }
        foreach ($ccr['request'][2]['properties'] as $key=>$item) {
            PdfHelper::createContractPdf(view('request.request_gold', ['ccr' => $item]),
                null, null, '/tmp/' . $ccr->id .'-'.$key. 'gold.pdf',
                ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id .'-'.$key. 'gold.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $ccr->id .'-'.$key. 'gold.pdf');
        }

        PdfHelper::createContractPdf(view('request.request_footer', ['ccr' => $ccr]),
            null, null, '/tmp/' .$ccr->id . 'footer.pdf',
            ['margin_top' => 40, 'margin_bottom' => 10,'type'=>'request']);
        $pdf->AddPage();
        $pageCount = $pdf->setSourceFile('/tmp/' . $ccr->id . 'footer.pdf');
        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink('/tmp/' .  $ccr->id . 'footer.pdf');

        if($preview) {
            return $pdf->Output();
        }else{
            $company = \Modules\Company\Entities\Company::where('id',$ccr->company_id)->first();
            if(!file_exists(storage_path('requests'))) {

                Storage::makeDirectory('requests');
            }
             $pdf->Output(storage_path('app/requests/'.str_slug($ccr->id.'-MADS_'.$company->name.'_Árajánlat').'.pdf'),\Mpdf\Output\Destination::FILE);
            Storage::disk('s3')->put('app/requests/'.str_slug($ccr->id.'-MADS_'.$company->name.'_Árajánlat').'.pdf',file_get_contents(storage_path('app/requests/'.str_slug($ccr->id.'-MADS_'.$company->name.'_Árajánlat').'.pdf')));
            unlink(storage_path('app/requests/'.str_slug($ccr->id.'-MADS_'.$company->name.'_Árajánlat').'.pdf'));
            return 'app/requests/'.str_slug($ccr->id.'-MADS_'.$company->name.'_Árajánlat').'.pdf';

        }
    }





}
