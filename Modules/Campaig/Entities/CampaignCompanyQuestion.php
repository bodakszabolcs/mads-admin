<?php

namespace Modules\Campaig\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class CampaignCompanyQuestion extends BaseModel
{
    use  Cachable;
	protected $table = "campaign_company_questions";
	protected $fillable =['campaign_id','company_id','name','email','questions','other_recipients'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    protected $casts =['questions'=>'json','other_recipients'=>'array'];







}
