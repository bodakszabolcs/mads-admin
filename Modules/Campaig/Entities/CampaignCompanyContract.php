<?php

namespace Modules\Campaig\Entities;

use App\BaseModel;
use App\Helpers\PdfHelper;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\Storage;

class CampaignCompanyContract extends BaseModel
{
    use  Cachable;
	protected $table = "campaign_company_contract";
	protected $fillable =['campaign_id','company_id','name','email','data','type','other_recipients','contract_sign_type'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    protected $casts =['data'=>'json','other_recipients'=>'array'];
    public function genereteNullMADSPDF($preview=false){

        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        $ccr = $this;
        if($this->type == 'work') {
            if($this->data['language']=='hu') {
                return PdfHelper::createContractPdf(view('contracts.work.nulladik_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'work']);
            }else{
                return PdfHelper::createContractPdf(view('contracts.work.nulladik_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'work']);
            }
        }
        return null;
    }
    public function genereteFirstMADSPDF($preview=false){

        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        $ccr = $this;
        if($this->type == 'mads') {
            if($this->data['language']=='hu') {
                return PdfHelper::createContractPdf(view('contracts.mads.elszo_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'mads']);
            }else{
                return PdfHelper::createContractPdf(view('contracts.mads.elszo_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 15, 'margin_bottom' => 15,'type'=>'mads']);
            }
        }
        if($this->type == 'gold') {
            if($this->data['language']=='hu') {
                return PdfHelper::createContractPdf(view('contracts.gold.elszo_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer_gold', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'gold']);
            }else{
                return PdfHelper::createContractPdf(view('contracts.gold.elszo_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_gold_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 15, 'margin_bottom' => 15,'type'=>'gold']);
            }
        }
        if($this->type == 'work') {
            if($this->data['language']=='hu') {
                return PdfHelper::createContractPdf(view('contracts.work.elszo_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'work']);
            }else{
                return PdfHelper::createContractPdf(view('contracts.work.elszo_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 15, 'margin_bottom' => 15,'type'=>'work']);
            }
        }
        return null;

    }
    public function genereteSecondMADSPDF($preview=false)
    {

        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        $ccr = $this;
        if($this->type == 'mads') {
            if($this->data['language']=='hu') {
               return PdfHelper::createContractPdf(view('contracts.mads.masodik_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'mads']);
            }else{
               return PdfHelper::createContractPdf(view('contracts.mads.masodik_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'mads']);
            }
        }
        if($this->type == 'gold') {
            if($this->data['language']=='hu') {
                  return  PdfHelper::createContractPdf(view('contracts.gold.masodik_szamu_melleklet', ['data' => $ccr]),
                        null, view('contracts.contract_footer_gold', ['data' => null]), $preview?'S':null,
                        ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'gold']);
            }else{
                return  PdfHelper::createContractPdf(view('contracts.gold.masodik_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_gold_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'gold']);
            }
        }
        if($this->type == 'work') {
            if($this->data['language']=='hu') {
                return  PdfHelper::createContractPdf(view('contracts.work.masodik_szamu_melleklet', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'work']);
            }else{
                return  PdfHelper::createContractPdf(view('contracts.work.masodik_szamu_melleklet_en', ['data' => $ccr]),
                    null, view('contracts.contract_footer_work_en', ['data' => null]), $preview?'S':null,
                    ['margin_top' => 10, 'margin_bottom' => 15,'type'=>'work']);
            }
        }
        return null;

    }
    public function genereteContractMADSPDF($preview=false)
    {

        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        $ccr = $this;

        if($this->type == 'mads') {

            if($this->data['language']=='hu') {
                if ($this->actual_contract) {
                    return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                        view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 30, 'margin_bottom' => 15,'type'=>'mads']);
                } else {

                    return PdfHelper::createContractPdf(view('contracts.mads.szerzodes', ['data' => $ccr]),
                        view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 30, 'margin_bottom' => 15,'type'=>'mads']);
                }
            }else{
                if ($this->actual_contract) {
                    return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                        view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_en', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 25, 'margin_bottom' => 15,'type'=>'mads']);
                } else {
                    return PdfHelper::createContractPdf(view('contracts.mads.szerzodes_en', ['data' => $ccr]),
                        view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_en', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 25, 'margin_bottom' => 15,'type'=>'mads']);
                }
            }
        }
        if($this->type == 'gold') {
            if($this->data['language']=='hu') {
                if ($this->actual_contract) {
                    return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                        view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_gold', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 30, 'margin_bottom' => 15,'type'=>'gold']);
                } else {
                    return PdfHelper::createContractPdf(view('contracts.gold.szerzodes', ['data' => $ccr]),
                        view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_gold', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 30, 'margin_bottom' => 15,'type'=>'gold']);
                }
            }else{
                if ($this->actual_contract) {
                    return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                        view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_gold_en', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 25, 'margin_bottom' => 15,'type'=>'gold']);
                } else {
                    return PdfHelper::createContractPdf(view('contracts.gold.szerzodes_en', ['data' => $ccr]),
                        view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_gold_en', ['data' => $ccr]), $preview?'S':null,
                        ['margin_top' => 25, 'margin_bottom' => 15,'type'=>'gold']);
                }
            }
        }
        if($this->type == 'work') {
            if(isset($this->data['is_private']) && $this->data['is_private'] == 1) {
                if ($this->data['language'] == 'hu') {
                    if ($this->actual_contract) {
                        return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                            view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_work', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 25, 'margin_bottom' => 15, 'type' => 'work']);
                    } else {
                        return PdfHelper::createContractPdf(view('contracts.work.szerzodes_magan', ['data' => $ccr]),
                            view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_work', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 25, 'margin_bottom' => 15, 'type' => 'work']);
                    }
                } else {
                    if ($this->actual_contract) {
                        return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                            view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_work_en', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 25, 'margin_bottom' => 15, 'type' => 'work']);
                    } else {
                        return PdfHelper::createContractPdf(view('contracts.work.szerzodes_magan_en', ['data' => $ccr]),
                            view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_work_en', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 25, 'margin_bottom' => 15, 'type' => 'work']);
                    }
                }
            }else{
                if ($this->data['language'] == 'hu') {
                    if ($this->actual_contract) {
                        return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                            view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_work', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 26, 'margin_bottom' => 15, 'type' => 'work']);
                    } else {
                        return PdfHelper::createContractPdf(view('contracts.work.szerzodes', ['data' => $ccr]),
                            view('contracts.contract_header', ['data' => $ccr]), view('contracts.contract_footer_work', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 26, 'margin_bottom' => 15, 'type' => 'work']);
                    }
                } else {
                    if ($this->actual_contract) {
                        return PdfHelper::createContractPdf(view('contracts.contract_empty', ['data' => $this->actual_contract]),
                            view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_work_en', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 23, 'margin_bottom' => 15, 'type' => 'work']);
                    } else {
                        return PdfHelper::createContractPdf(view('contracts.work.szerzodes_en', ['data' => $ccr]),
                            view('contracts.contract_header_en', ['data' => $ccr]), view('contracts.contract_footer_work_en', ['data' => $ccr]), $preview?'S':null,
                            ['margin_top' => 23, 'margin_bottom' => 15, 'type' => 'work']);
                    }
                }
            }
        }

        return null;

    }





}
