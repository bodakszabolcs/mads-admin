<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\Campaig\Http\Controllers',
    'prefix' => '/campaig',
    'middleware' => ['auth:sanctum','role','accesslog']
], function () {
    Route::match(['get'], '/details/{id}', 'CampaigController@details')->name('Get campaign details');
    Route::match(['get'], '/search-company-by-name/{id}', 'CampaigController@searchCompanyByName')->name('Cég keresése a státuszokban');
    Route::match(['post'], '/import/{id}', 'CampaigController@import')->name('Campaign company import');
    Route::match(['get'], '/get-max-contract-number/{campaign}/{company}/{type}', 'CampaigController@getMaxNumber')->name('Get max contract number');
    Route::match(['get'], '/contract-list', 'CampaigController@contractList')->name('Contract list');
    Route::match(['get'], '/question-list', 'CampaigController@questionList')->name('Question list');
    Route::match(['get'], '/request-list', 'CampaigController@requestList')->name('Request list');
    Route::match(['get'], '/contract-list/show/{id}', 'CampaigController@contractShow')->name('Contract show');

    Route::match(['get'], '/contract-sablon/show/{id}', 'CampaigController@showSablon')->name('Contract template show');
    Route::match(['get'], '/contract-sablon/list', 'CampaigController@listSablon')->name('Contract template list');
    Route::match(['post'], '/contract-sablon/edit/{id}', 'CampaigController@editSablon')->name('Contract template edit');

    Route::match(['post'], '/contract-list/save-as-draft/{id}', 'CampaigController@saveAsDraft')->name('Contract save as draft');
    Route::match(['post'], '/contract-list/approve-contract/{id}', 'CampaigController@approveContract')->name('Contract approve');
    Route::match(['post'], '/contract-list/revert-last-version/{id}', 'CampaigController@revertLastVersion')->name('Contract revert last version');
    Route::match(['post'], '/contract-list/save-and-approve/{id}', 'CampaigController@saveAndApproveContract')->name('Contract save and approve');
    Route::match(['get'], '/contract-list/pdf-preview/{id}/0-szamu-melleklet', 'CampaigController@contractShowNull')->name('Contract show 0 számú melléklet');
    Route::match(['get'], '/contract-list/pdf-preview/{id}/1-szamu-melleklet', 'CampaigController@contractShowFirst')->name('Contract show 1 számú melléklet');
    Route::match(['get'], '/contract-list/pdf-preview/{id}/2-szamu-melleklet', 'CampaigController@contractShowSecond')->name('Contract show 2 számú melléklet');
    Route::match(['get'], '/contract/pdf-preview/{id}/0-szamu-melleklet', 'CampaigController@contractShowNull')->name('Contract show 0 számú melléklet');
    Route::match(['get'], '/contract-list/pdf-preview/{id}/szerzodes', 'CampaigController@contractShowContract')->name('Contract show');
    Route::match(['get'], '/contract/pdf-preview/{id}/1-szamu-melleklet', 'CampaigController@contractShowFirst')->name('Contract show');
    Route::match(['get'], '/contract/pdf-preview/{id}/2-szamu-melleklet', 'CampaigController@contractShowSecond')->name('Contract show');
    Route::match(['get'], '/contract/pdf-preview/{id}/szerzodes', 'CampaigController@contractShowContract')->name('Contract show');
    Route::match(['get'], '/get-questions', 'CampaigController@getQuestions')->name('Get questions');
    Route::match(['get'], '/company-details/{id}', 'CampaigController@companyDetails')->name('Get company details');
    Route::match(['post'], '/add-company', 'CampaigController@addCompany')->name('Add company');
    Route::match(['post'], '/create-company', 'CampaigController@createCompany')->name('Create company');
    Route::match(['get'], '/{id}/campaign-company', 'CampaigController@listCompany')->name('List company');
    Route::match(['get'], '/task/list/{id}', 'CampaigController@getTaskList')->name('Get campaign task');
    Route::match(['delete'], '/task/delete/{id}', 'CampaigController@deleteTask')->name('Delete campaign task');
    Route::match(['post'], '/task/save', 'CampaigController@editTask')->name('Edit campaign task');
    Route::match(['post'], '/company-add-comment/{id}', 'CampaigController@addCompanyComment')->name('Comment campaign company');
    Route::match(['post'], '/change-status', 'CampaigController@changeStatus')->name('Change status');
    Route::match(['post'], '/save-company-question', 'CampaigController@saveQuestions')->name('Save questions');
    Route::match(['post'], '/company-save-request', 'CampaigController@saveRequest')->name('Save request');
    Route::match(['post'], '/company-save-contract/{type}', 'CampaigController@saveContract')->name('Save contract');
    Route::match(['post'], '/company-pdf-preview', 'CampaigController@pdfPreviewRequest')->name('preview pdf file');
    Route::match(['post'], '/pdf-preview/mads/0-szamu-melleklet', 'CampaigController@pdfPreview1SzamuMellekletMADSRequest')->name('MADS 1. számú melléklet előnézet');
    Route::match(['post'], '/pdf-preview/mads/1-szamu-melleklet', 'CampaigController@pdfPreview1SzamuMellekletMADSRequest')->name('MADS 1. számú melléklet előnézet');
    Route::match(['post'], '/pdf-preview/mads/2-szamu-melleklet', 'CampaigController@pdfPreview2SzamuMellekletMADSRequest')->name('MADS 2. számú melléklet előnézet');
    Route::match(['post'], '/pdf-preview/mads/szerzodes', 'CampaigController@pdfPreviewContractMADSRequest')->name('MADS Szerződés előnézet');
});
