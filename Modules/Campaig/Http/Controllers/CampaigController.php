<?php

namespace Modules\Campaig\Http\Controllers;

use App\Helpers\ConstansHelper;
use App\Http\Controllers\AbstractLiquidController;
use App\Imports\CompanyImport;
use App\Imports\PayrollImport;
use App\Jobs\CompanyQuestion;
use App\Jobs\CompanyRequest;
use App\Jobs\SendEmailWithLink;
use App\Jobs\SendOnlineCardEmail;
use Caxy\HtmlDiff\HtmlDiff;
use Google\Service\Dfareporting\UserRole;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Campaig\Entities\Campaig;
use Illuminate\Http\Request;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyContract;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Campaig\Entities\CampaignCompanyRequest;
use Modules\Campaig\Entities\CampaignTask;
use Modules\Campaig\Entities\ContractSablon;
use Modules\Campaig\Http\Requests\ApproveRequest;
use Modules\Campaig\Http\Requests\CampaigCreateRequest;
use Modules\Campaig\Http\Requests\CampaignCompanyCreateRequest;
use Modules\Campaig\Http\Requests\CampaigUpdateRequest;
use Modules\Campaig\Http\Requests\CompanyAddRequest;
use Modules\Campaig\Http\Requests\ContractMADSRequest;
use Modules\Campaig\Http\Requests\SaveQuestionRequest;
use Modules\Campaig\Transformers\CampaigCompanyResource;
use Modules\Campaig\Transformers\CampaigDetailsResource;
use Modules\Campaig\Transformers\CampaignRequestListResource;
use Modules\Campaig\Transformers\CampaignTaskResource;
use Modules\Campaig\Transformers\CampaigQuestionListResource;
use Modules\Campaig\Transformers\CampaigViewResource;
use Modules\Campaig\Transformers\CampaigListResource;
use Modules\Campaig\Transformers\CompanyModalResource;
use Modules\Campaig\Transformers\ContractListResource;
use Modules\Campaig\Transformers\ContractSablonResource;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Contact;
use Modules\Company\Transformers\CompanyContactResource;
use Modules\Questions\Entities\Questions;
use Modules\Questions\Transformers\QuestionsListResource;
use Modules\Questions\Transformers\QuestionsViewResource;
use Modules\Request\Transformers\RequestListResource;
use Modules\User\Entities\User;

class CampaigController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Campaig();
        $this->viewResource = CampaigViewResource::class;
        $this->listResource = CampaigListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CampaigCreateRequest::class;
        $this->updateRequest = CampaigUpdateRequest::class;
    }
    public function searchCompanyByName(Request $request, $id){
        $list = CampaignCompany::leftJoin("companies",'companies.id','=','company_id')->select("status_id")->where('campaign_id','=',$id);
        $list = $list->where("companies.name",'LIKE','%'.$request->input("search",'').'%');

        return response()->json($list->get()->pluck('status_id','status_id')->toArray());
    }
    public function index(Request $request, $auth = null)
    {
        $list = $this->model->searchInModel($request->input());
        $list = $list->leftJoin(DB::raw("(select campaign_id,count(*) as company_count, sum(if(status_id =12,1,0))+sum(if(status_id =13,1,0))+sum(if(status_id =11,1,0)) as finished from campaign_companies group by campaign_id) as cnt"),'cnt.campaign_id','=','campaigns.id')
            ->select(DB::raw("campaigns.*,cnt.company_count as companies_count, cnt.finished as finished"));
        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('campaigns.id','desc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy($sort[0],$sort[1]);
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function contractShowFirst(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteFirstMADSPDF();
    }
    public function contractShowNull(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteNullMADSPDF();
    }
    public function contractShowSecond(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteSecondMADSPDF();
    }
    public function contractShowContract(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteContractMADSPDF();
    }
    public function contractList(Request $request){
        $list = CampaignCompanyContract::whereNotNull('actual_contract')->leftJoin('companies','companies.id','=','company_id')->select(DB::raw("campaign_company_contract.*, companies.name as company_name"));

        if($request->has("search") && !empty($request->input("search")))
        {
            $search =$request->input("search");
            $list = $list->where(function($query) use ($search){
                $query = $query->where('name','LIKE','%'.$search.'%')->orWhere('company_name','LIKE','%'.$search.'%');
            });
        }

        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('id','desc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy($sort[0],$sort[1]);
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }


        return ContractListResource::collection($list)->additional(['filters' => [[
            'name' => 'search',
            'title' => 'Keresés',
            'type' => 'text',
        ] ]]);

    }
    public function questionList(Request $request){
        $list = CampaignCompanyQuestion::leftJoin('companies','companies.id','=','company_id')->select(DB::raw("campaign_company_questions.*, companies.name as company_name"));
        if($request->has("search") && !empty($request->input("search")))
        {
            $search =$request->input("search");
            $list = $list->where(function($query) use ($search){
                $query = $query->where('name','LIKE','%'.$search.'%')->orWhere('company_name','LIKE','%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%');
            });
        }
        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('id','desc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy($sort[0],$sort[1]);
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }


        return CampaigQuestionListResource::collection($list)->additional(['filters' => [[
            'name' => 'search',
            'title' => 'Keresés',
            'type' => 'text',
        ] ]]);

    }
    public function requestList(Request $request){
        $list = CampaignCompanyRequest::leftJoin('companies','companies.id','=','company_id')->select(DB::raw("campaign_company_request.*, companies.name as company_name"));
        if($request->has("search") && !empty($request->input("search")))
        {
            $search =$request->input("search");
            $list = $list->where(function($query) use ($search){
                $query = $query->where('name','LIKE','%'.$search.'%')->orWhere('company_name','LIKE','%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%');
            });
        }
        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('id','desc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy($sort[0],$sort[1]);
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }


        return CampaignRequestListResource::collection($list)->additional(['filters' => [[
            'name' => 'search',
            'title' => 'Keresés',
            'type' => 'text',
        ] ]]);

    }
    public function saveAsDraft(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->mads_draft =  $request->input('actual_contract');
        $contract->save();
        return $this->contractShow($request,$id);
    }
    public function approveContract(ApproveRequest  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->mads_updated =  date('Y-m-d H:i:s');
        $contract->mads_approved =  true;
        $contract->name = $request->input('name');
        $contract->email = $request->input('email');
        $contract->other_recipients = $request->input('other_recipients');
        $contract->save();
        $att = $this->getAttachments($contract);
        SendEmailWithLink::dispatch($contract->email,'MADS-'.$contract->data['name'].'- szerződést a mads elfogadhatta!', $contract->name, "<p>A " . $contract->data['number'] . " sorszámú szerződést a MADS elfogadta:<br/>A részletekért kérem látogasson el az alábbi linkre, ahonnan letölthető a végleges szerződés</p>", '/admin/company/contract/show/' . $contract->id, 'Ugrás a szerződéshez', $att);
        if($contract->other_recipients) {
            foreach ($contract->other_recipients as $c) {
                SendEmailWithLink::dispatch($c['email'], 'MADS-'.$contract->data['name'].'- szerződést a mads elfogadhatta!', $c['name'], "<p>A " . $contract->data['number'] . " sorszámú szerződést a MADS elfogadta:<br/>A részletekért kérem látogasson el az alábbi linkre, ahonnan letölthető a végleges szerződés</p>", '/admin/company/contract/show/' . $contract->id, 'Ugrás a szerződéshez', $att);
            }
        }
        return $this->contractShow($request,$id);
    }
    public function revertLastVersion(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->mads_draft = null;
        $contract->save();

       return $this->contractShow($request,$id);
    }
    public function contractShow(Request $request,$id){
        ini_set("max_execution_time",-1);
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $htmlDiff = new HtmlDiff($contract->previous_contract, ($contract->mads_draft)?$contract->mads_draft:$contract->actual_contract);
        return response()->json([
            'data'=>[
                'id'=>$id,
                'type'=>$contract->type,
                'name'=>$contract->name,
                'email'=>$contract->email,
                'other_recipients'=>$contract->other_recipients,
                'previous_contract'=>$contract->previous_contract,
                'actual_contract'=>($contract->mads_draft)?$contract->mads_draft:$contract->actual_contract,
                'diff'=> $htmlDiff->build(),
                'mads_approved'=>ConstansHelper::bool($contract->mads_approved),
                'company_approved'=>ConstansHelper::bool($contract->company_approved),
                'mads_updated'=> ConstansHelper::formatShortDateTime($contract->mads_updated),
                'company_updated'=> ConstansHelper::formatShortDateTime($contract->company_updated),

            ]
        ]);
    }

    public function getAttachments($contract){

        if($contract->type=='work'){
            if(!file_exists(Storage::disk('s3')->exists('private/company/' . $contract->company_id ))) {

                Storage::makeDirectory('private/company/' . $contract->company_id );
            }
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . '1-2_melleklet.pdf', $contract->genereteNullMADSPDF(true));
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . '3_szamu_melleklet.pdf', $contract->genereteFirstMADSPDF(true));
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . '4_szamu_melleklet.pdf', $contract->genereteSecondMADSPDF(true));
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . 'MADS_szerzodes.pdf', $contract->genereteContractMADSPDF(true));
            $attachments=[
                [
                    'path'=>'private/company/' . $contract->company_id. '/' . '1-2_melleklet.pdf',
                    'name'=>'1-2-szamu-melleklet.pdf'
                ],
                [
                    'path'=>'private/company/' . $contract->company_id. '/' . '3_szamu_melleklet.pdf',
                    'name'=>'3_szamu_melleklet.pdf'
                ],
                [
                    'path'=>'private/company/' . $contract->company_id. '/' . '4_szamu_melleklet.pdf',
                    'name'=>'4_szamu_melleklet.pdf'
                ],
                [
                    'path'=>'private/company/' . $contract->company_id. '/' . 'MADS_szerzodes.pdf',
                    'name'=>'MADS_szerzodes.pdf'
                ]
            ];
        }else{
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . '1_szamu_melleklet.pdf', $contract->genereteFirstMADSPDF(true));
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . '2_szamu_melleklet.pdf', $contract->genereteSecondMADSPDF(true));
            Storage::disk('s3')->put('private/company/' . $contract->company_id . '/' . 'MADS_szerzodes.pdf', $contract->genereteContractMADSPDF(true));
            $attachments=[
                [
                    'path'=>'/private/company/' . $contract->company_id. '/' . '1_szamu_melleklet.pdf',
                    'name'=>'1_szamu_melleklet.pdf'
                ],
                [
                    'path'=>'/private/company/' . $contract->company_id. '/' . '2_szamu_melleklet.pdf',
                    'name'=>'2_szamu_melleklet.pdf'
                ],
                [
                    'path'=>'/private/company/' . $contract->company_id. '/' . 'MADS_szerzodes.pdf',
                    'name'=>'MADS_szerzodes.pdf'
                ]
            ];
        }

        return $attachments;
    }
    public function saveAndApproveContract(ApproveRequest $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->previous_contract = $contract->actual_contract;
        $contract->actual_contract = $request->input('actual_contract');
        $contract->name = $request->input('name');
        $contract->email = $request->input('email');
        $contract->other_recipients = $request->input('other_recipients');
        $contract->company_approved = false;
        $contract->mads_approved = true;
        $contract->mads_draft = null;
        $contract->mads_updated = date('Y-m-d H:i:s');
        $contract->save();
        $att = $this->getAttachments($contract);
        SendEmailWithLink::dispatch($contract->email, 'MADS-' . $contract->data['name'] . '- szerződést a mads módosította!', $contract->name, "<p>A " . $contract->data['number'] . " sorszámú szerződést a MADS módosította:<br/>A részletekért kérem látogasson el az alábbi linkre, ahol megtekintheti a szerződésben módosított pontokat</p>", '/admin/my-contracts/' . $contract->id, 'Ugrás a szerződéshez', $att);
        if($contract->other_recipients) {
            foreach ($contract->other_recipients as $c) {
                SendEmailWithLink::dispatch($c['email'], 'MADS-' . $contract->data['name'] . '- szerződést a mads módosította!', $c['name'], "<p>A " . $contract->data['number'] . " sorszámú szerződést a MADS módosította:<br/>A részletekért kérem látogasson el az alábbi linkre, ahol megtekintheti a szerződésben módosított pontokat</p>", '/admin/my-contracts/' . $contract->id, 'Ugrás a szerződéshez', $att);
            }
        }
        return $this->contractShow($request,$id);
    }
    public function details(Request $request, $id = 0, $auth = null)
    {
        $model = $this->model;
        $this->model = $this->model->where('id', '=', $id);
        try {

            if (!\Auth::user()->isSuperAdmin()) {
                $model = $model->where(function ($query) {
                    $query->where('user_id', '=', \Auth::id());
                });
            }
            $this->model = $this->model->firstOrFail();

        } catch (ModelNotFoundException $e) {
            $this->model = new $model();
        }

        return new CampaigDetailsResource($this->model);
    }
    public function addCompany(CompanyAddRequest $request){
         $exists=CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id',$request->input('company_id'))->first();
        if($exists){
          return  response()->json(['errors' => ['errors' =>'A cég már hozzá van rendeleve a kampányhoz' ]], 422);
        }
         $cc = new CampaignCompany();
         $cc->status_id=1;
         $cc->company_id = $request->input('company_id');
         $cc->campaign_id = $request->input('campaign_id');
         $cc->user_id = $request->input('user_id',\Auth::user()->id);
         $cc->services = $request->input('services');
         $cc->save();

         return response()->json("OK");

    }
    public function import(Request  $request, $id){
        try{

            Excel::import(new CompanyImport($id),$request->input('file'),'s3');
            return  response()->json(['data'=> 'Sikeres importálás']);
        }
        catch (\Exception $e){

            return response()->json(['data'=> 'Hiba az importálás során: '.json_encode($e->getMessage())]);
        }
    }
    public function listCompany(Request $request,$id){
        $campaign =  Campaig::where('id',$id)->first();
        $list=$campaign->getCompanyQuery();
        $list= $list->where('status_id',$request->input('status'));
        if (!\Auth::user()->isSuperAdmin()) {
            $list = $list->where(function ($query) {
                $query->where('campaign_companies.user_id', '=', \Auth::id());
            });
        }
        if(!$request->has('sort') || empty($request->input('sort'))){
            $list=$list->orderBy('company_name','asc');
        }else{
            $sort = explode('|',$request->input('sort'));
            $list=$list->orderBy($sort[0],$sort[1]);
        }
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return CampaigCompanyResource::collection($list);
    }
    public function listSablon(Request $request){

        $list=ContractSablon::whereNotNull('id');

        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return ContractSablonResource::collection($list);
    }
    public function editSablon(Request $request,$id){

        $task=ContractSablon::where('id',$id)->first();
        $task->fill($request->all());
        $task->save();
        return  $this->showSablon($request,$id);
    }
    public function showSablon(Request $request,$id){

        $task=ContractSablon::where('id',$id)->first();
        return new ContractSablonResource($task);
    }
    public function getTaskList(Request $request,$id){
        $prTask =CampaignTask::where('project_id',$id)->whereNull('status')->orderBy('deadline','asc');
        return CampaignTaskResource::collection($prTask->get());
    }
    public function editTask(Request $request){

        $task = new CampaignTask();
        if($request->input('id')){
            $task=CampaignTask::where('id',$request->input('id'))->first();
        }
        $task->fill($request->all());
        $task->save();
        return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
    }
    public function deleteTask(Request $request, $id){

        $model =CampaignTask::where('id', '=', $id);
        try {
            $model = $model->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $model->delete();
        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }
    public function addCompanyComment(Request $request, $id)
    {
        try {
            $project = CampaignCompany::where('id', $id)->firstOrFail();
            $project->comment = "\n".date('Y.n.d H:i').' - '.$request->input('comment');
            $project->save();
            return response()->json(['data' => ['message' => 'OK']], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
    }
    public function companyDetails(Request $request,$id){
        try {
        $company = Company::where("id",'=',$id)->firstOrFail();
        return new CompanyModalResource($company);
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
    }
    public function changeStatus(Request $request){
        $project = CampaignCompany::where('id', $request->input("id"))->firstOrFail();
        $project->status_id= $request->input("status");
        $project->save();
        return response()->json("ok");
    }
    public function createCompany(CampaignCompanyCreateRequest $request){
        $company = Company::where('tax',$request->input('tax'))->first();
        if($company)
        {
            return  response()->json(['errors' => ['errors' =>'A cég már létezik az adatbázisban' ]], 422);
        }
        $company = new Company();
        $company->fillAndSave($request->all());
        $contact = new  Contact();
        $contact->name = $request->input('contact_name');
        $contact->position  = $request->input('contact_position');
        $contact->email = $request->input('contact_email');
        $contact->phone = $request->input('contact_phone');
        $contact->company_id = $company->id;
        $contact->save();
        $cc = new CampaignCompany();
        $cc->status_id=1;
        $cc->company_id = $company->id;
        $cc->campaign_id = $request->input('campaign_id');
        $cc->user_id = \Auth::user()->id;
        $cc->services = $request->input('services');
        $cc->save();
        return  response()->json('ok');

    }
    public function getQuestions(Request $request){
        return QuestionsViewResource::collection(Questions::orderBy('o')->get());
    }
    public function saveQuestions(SaveQuestionRequest $request){
        $question = CampaignCompanyQuestion::where('campaign_id',$request->input('campaign_id'))->where('company_id','=',$request->input('company_id'))->first();
        if(!$question){
            $question = new CampaignCompanyQuestion();
        }
        $question->fill($request->all());
        $question->save();
        // Céges felhasználó
        $user = User::where('email',$request->input('email'))->first();
        if(!$user){
            $user = new User();
               $user->name = $request->input("name");
               $user->firstname = explode(' ',$request->input("name"))[0];
               $user->lastname = str_replace(explode(' ',$request->input("name"))[0].' ','',$request->input("name"));
               $user->email = $request->input("email");
               $user->email_verified_at = date('Y-m-d H:i:s');
               $user->is_company = 1;
               $user->company_id = $request->input("company_id");
               $user->save();
               $user->roles()->sync([11]);
        }
        $campaign = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id','=',$request->input('company_id'))->first();
        $campaign->status_id = 5;
        $campaign->save();
        CompanyQuestion::dispatch($question);

        return response()->json("OK");
    }
    public function saveRequest(Request $request){
        $rs = CampaignCompanyRequest::where('campaign_id',$request->input('campaign_id'))->where('company_id','=',$request->input('company_id'))->first();
        if(!$rs){
            $rs = new CampaignCompanyRequest();
        }
        $rs->company_id = $request->input('company_id');
        $rs->campaign_id = $request->input('campaign_id');
        $rs->name = $request->input('name');
        $rs->email = $request->input('email');
        $rs->request = $request->input("data");
        $rs->other_recipients = $request->input("other_recipients");
        $rs->approved = 0;
        $rs->save();
        $rs->documentum = $rs->genereteRequestPDF();
        $rs->save();
        $campaign = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id','=',$request->input('company_id'))->first();
        $campaign->status_id = 7;
        $campaign->save();

        CompanyRequest::dispatch($rs,$request->input('content'));
        return response()->json("OK");
    }
    public function pdfPreviewRequest(Request $request){
        $rs = CampaignCompanyRequest::where('campaign_id',$request->input('campaign_id'))->where('company_id','=',$request->input('company_id'))->first();
        if(!$rs){
            $rs = new CampaignCompanyRequest();
        }
        $rs->company_id = $request->input('company_id');
        $rs->campaign_id = $request->input('campaign_id');
        $rs->name = $request->input('name');
        $rs->email = $request->input('email');
        $rs->request = $request->input("data");

        return $rs->genereteRequestPDF(true);

    }
    public function pdfPreview1SzamuMellekletMADSRequest(Request $request){

        $rs = CampaignCompanyContract::where('campaign_id',$request->input('campaign_id'))
            ->where('company_id','=',$request->input('company_id'))->where('type',$request->input('type'))->first();
        if(!$rs){
            $rs = new CampaignCompanyContract();
        }
        $rs->company_id = $request->input('company_id',1);
        $rs->campaign_id = $request->input('campaign_id',1);
        $rs->name = $request->input('name');
        $rs->email = $request->input('email');
        $rs->data = $request->input("data");
        $rs->type= $request->input('type');
        $rs->save();
        return $rs->genereteFirstMADSPDF();

    }
    public function saveContract(ContractMADSRequest $request,$type){
        $c = CampaignCompanyRequest::where('company_id','=',$request->input('company_id'))->where('campaign_id',$request->input('campaign_id'))->first();
        if($c){
            $c = CampaignCompanyQuestion::where('company_id','=',$request->input('company_id'))->where('campaign_id',$request->input('campaign_id'))->first();
        }
        $rs = CampaignCompanyContract::where('campaign_id',$request->input('campaign_id'))
            ->where('company_id','=',$request->input('company_id'))->where('type',$type)->first();
        if(!$rs){
            $rs = new CampaignCompanyContract();
        }
        $rs->company_id = $request->input('company_id',1);
        $rs->campaign_id = $request->input('campaign_id',1);
        $rs->name = optional($c)->name;
        $rs->number =$request->input("data")['number'];
        $rs->email = optional($c)->email;
        $rs->other_recipients = optional($c)->other_recipients;
        $rs->data = $request->input("data");
        $rs->type= $type;
        $rs->actual_contract = view('contracts.mads.szerzodes', ['data' => $rs])->render();
        $rs->save();
        return response()->json(['link'=>'/admin/campaign/contract-list/show/'.$rs->id]);

    }
    public function pdfPreview2SzamuMellekletMADSRequest(Request $request){
        $rs = CampaignCompanyContract::where('campaign_id',$request->input('campaign_id'))
            ->where('company_id','=',$request->input('company_id'))->where('type',$request->input('type'))->first();
        if(!$rs){
            $rs = new CampaignCompanyContract();
        }
        $rs->company_id = $request->input('company_id');
        $rs->campaign_id = $request->input('campaign_id');
        $rs->name = $request->input('name');
        $rs->email = $request->input('email');
        $rs->data = $request->input("data");
        return $rs->genereteSecondMADSPDF();

    }
    public function pdfPreviewContractMADSRequest(Request $request){
        $rs = CampaignCompanyContract::where('campaign_id',$request->input('campaign_id'))
            ->where('company_id','=',$request->input('company_id'))->where('type',$request->input('type'))->first();
        if(!$rs){
            $rs = new CampaignCompanyContract();
        }
        $rs->company_id = $request->input('company_id');
        $rs->campaign_id = $request->input('campaign_id');
        $rs->name = $request->input('name');
        $rs->email = $request->input('email');
        $rs->data = $request->input("data");
        return $rs->genereteContractMADSPDF();

    }
    public function getMaxNumber(Request $request,$campaign,$company,$type){
        $rs = CampaignCompanyContract::where('campaign_id',$campaign)
            ->where('company_id','=',$company)->where('type',$type)->first();
        if($rs){
            return response()->json(['data'=>['number'=>'','exists'=>true,'data'=>$rs->data]]);
        }
        $number = CampaignCompanyContract::where('type',$type)->where('number','LIKE','%/'.date('Y').'%')->select(DB::raw("max(`number`) as n "))->first();
        $maxNumber = "1/".date('Y');
        if(!$number || !$number->n){
            return response()->json(['data'=>['number'=>$maxNumber,'exists'=>false,'data'=>null]]);
        }
        $d = explode("/",$number->n);
        $maxNumber = (((int)$d[0])+1)."/".$d[1];
        return response()->json(['data'=>['number'=>$maxNumber,'exists'=>false,'data'=>null]]);
    }
}
