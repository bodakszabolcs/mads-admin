<?php

namespace Modules\Campaig\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaveQuestionRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
			'campaign_id' => 'required',
			'name' => 'required',
			'email' => 'required',
			'questions' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'company_id' => __('Cég'),
                'campaign_id' => __('Kampány'),
                'name' => __('Címzett'),
                'email' => __('Email'),
                'questions' => __('Kérdések'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
