<?php

namespace Modules\Campaig\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CampaignCompanyCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'tax' => 'required',
			//'tax_eu' => 'required',
			//'company_number' => 'required',
          //  'delegate' => 'required',
          //  'delegate_position' => 'required',
          //  'contact_name'=>'required',
          //  'contact_position'=>'required',
          //  'contact_phone'=>'required',
            'contact_email'=>'required',
			/*'delegate' => 'required',
			'delegate_position' => 'required',
			'delegate_comment' => 'required',
			'contract_date' => 'required',
			'contract_expire' => 'required',
			'contract_type' => 'required',
			'agancy_contract_date' => 'required',
			'works_contarct_date' => 'required',
			'payment_deadline' => 'required',
			'paper_contract' => 'required',
			'mads_company' => 'required',
			'user_id' => 'required',
			'pob' => 'required',
			'zip' => 'required',
			'country_id' => 'required',
			'city_id' => 'required',
			'postal_address' => 'required',
			'work_address' => 'required',*/

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Cég neve'),
'tax' => __('Adószám'),
'tax_eu' => __('EU adószám'),
'company_number' => __('Cégjegyzékszám'),
'delegate' => __('Képviselő'),
'delegate_position' => __('Képviselő beosztása'),
'delegate_comment' => __('Megjegyzés'),
'contract_date' => __('Szerződéskötés dáuma'),
'contract_expire' => __('Szerződés érvényessége'),
'contract_type' => __('Határozattlan'),
'agancy_contract_date' => __('Megbízási szerződés dátuma'),
'works_contarct_date' => __('Vállalkozási szerződés dátuma'),
'payment_deadline' => __('Fizetési határidő'),
'paper_contract' => __('Papír alapú szerződés'),
'mads_company' => __('Kihez tartozik?'),
'user_id' => __('Felelős'),
'pob' => __('Postafiók'),
'zip' => __('Irányítószám'),
'country_id' => __('Ország'),
'city_id' => __('Város'),
'postal_address' => __('Postázási cím'),
'work_address' => __('Munkavégés helye'),
'region' => __('Régió'),
                'contact_name'=>'Kapcsolattartó neve',
                'contact_position'=>'Kapcsolattartó beosztása',
                'contact_phone'=>'Kapcsolat telefon',
                'contact_email'=>'Kapcsolat email',
            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
