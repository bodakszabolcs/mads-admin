<?php

namespace Modules\Campaig\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ContractMADSRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.number' => 'required',
			'data.date' => 'required',
			'data.first.controller' => 'required',
			'data.first.website' => 'required',
			'data.name' => 'required',
			'data.site' => 'required',
			'data.tax' => 'required',
			'data.payment_deadline' => 'required',
			'data.e_szamla' => 'required_if:e_szamla_approve,==,1',
			'data.language' => 'required',
			'data.effective_date' => 'required_if:data.contract_type,==,2',
			'data.previous_contract_date' => 'required_if:data.previous_contract,==,1',
			'data.previous_contract_number' => 'required_if:data.previous_contract,==,1',
            'data.persons'=>'required|array|min:1',
            'data.first.contacts'=>'required|array|min:1',
            'data.first.madsContact'=>'required|array|min:1',
            'data.e_szamla_approve'=>'required'

        ];
    }

    public function attributes()
        {
            return [
                'data.number' => __('Szerződés sorszáma'),
                'data.deadline' => __('Szerződés dátuma'),
                'data.first.controller' => 'Adatkezelő',
                'data.first.website' => 'Az adatkezelő honlapja',
                'data.name' => 'Cég neve',
                'data.site' => 'Cég székhelye',
                'data.tax' => 'Cég adószáma',
                'data.payment_deadline' => 'Fizetési határidő',
                'data.e_szamla' => 'E számla email',
                'data.e_szamla_approve' => 'E számla',
                'data.language' => 'Szerződés típusa',
                'data.effective_date' => 'Hatályba lépés dátuma',
                'previous_contract' => 'Előző szerződés',
                'data.previous_contract_date' => 'Előző szerződés dátuma',
                'data.previous_contract_number' => 'Előző szerződés sorszáma',
                'data.persons'=>'Képviselő',
                'data.first.contacts'=>'Kapcsolattartó',
                'data.first.madsContract'=>'MADS kapcsolattartó',
                'data.industries'=>'Tevékenység'

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
