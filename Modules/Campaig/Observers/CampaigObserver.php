<?php

namespace Modules\Campaig\Observers;

use Modules\Campaig\Entities\Campaig;

class CampaigObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function saved(Campaig $model)
    {
        Campaig::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function created(Campaig $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function updated(Campaig $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function deleted(Campaig $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function restored(Campaig $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Campaig\Entities\Campaig  $model
     * @return void
     */
    public function forceDeleted(Campaig $model)
    {
        //
    }
}
