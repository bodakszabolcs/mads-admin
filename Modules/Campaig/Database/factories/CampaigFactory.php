<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Campaig\Entities\Campaig;

$factory->define(Campaig::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"deadline" => $faker->date(),

    ];
});
