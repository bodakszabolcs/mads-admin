import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Campaig from '../components/Campaig'
import CampaigList from '../components/CampaigList'
import CampaigCreate from '../components/CampaigCreate'
import CampaigEdit from '../components/CampaigEdit'
import CampaignDetails from '../components/CampaignDetails.vue'
import CampaigContractList from '../components/CampaigContractList.vue'
import ContractShow from '../components/ContractShow.vue'
import ContractSablonList from '../components/ContractSablonList.vue'
import ContractSablonEdit from '../components/ContractSablonEdit.vue'
import QuestionList from '../components/QuestionList.vue'
import RequestList from '../components/RequestList.vue'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'campaign',
                component: Campaig,
                meta: {
                    title: 'Campaigs'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CampaigList',
                        component: CampaigList,
                        meta: {
                            title: 'Campaigs',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign/index'
                        }
                    },
                    {
                        path: 'contract-list',
                        name: 'ContractList',
                        component: CampaigContractList,
                        meta: {
                            title: 'Szerződések',
                            subheader: false
                        }
                    },
                    {
                        path: 'question-list',
                        name: 'Question list',
                        component: QuestionList,
                        meta: {
                            title: 'Árajánlat válaszok',
                            subheader: false
                        }
                    },
                    {
                        path: 'request-list',
                        name: 'Request list',
                        component: RequestList,
                        meta: {
                            title: 'Kiküldött árajánlatok',
                            subheader: false
                        }
                    },
                    {
                        path: 'contract-list/show/:id',
                        name: 'ContractShow',
                        component: ContractShow,
                        meta: {
                            title: 'Szerződések',
                            subheader: false
                        }
                    },
                    {
                        path: 'contract-template/list',
                        name: 'Contract Sablon List',
                        component: ContractSablonList,
                        meta: {
                            title: 'Szerződések sablonok',
                            subheader: false
                        }
                    },
                    {
                        path: 'contract-sablon/edit/:id',
                        name: 'Contract Sablon edit',
                        component: ContractSablonEdit,
                        meta: {
                            title: 'Szerződések sablonok',
                            subheader: false
                        }
                    },
                    {
                        path: 'create',
                        name: 'CampaigCreate',
                        component: CampaigCreate,
                        meta: {
                            title: 'Create Campaigs',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CampaigEdit',
                        component: CampaigEdit,
                        meta: {
                            title: 'Edit Campaigs',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign/index'
                        }
                    },
                    {
                        path: 'details/:id',
                        name: 'CampaigDetails',
                        component: CampaignDetails,
                        meta: {
                            title: 'Kampány részletek',
                            subheader: false
                        }
                    }
                ]
            }
        ]
    }
]
