<?php

namespace Modules\Campaig\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Campaig\Entities\Campaig;
use Modules\Campaig\Observers\CampaigObserver;

class CampaigServiceProvider extends ModuleServiceProvider
{
    protected $module = 'campaig';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Campaig::observe(CampaigObserver::class);
    }
}
