<?php

    namespace Modules\Message\Entities\Base;
    use App\BaseModel;
    use Illuminate\Database\Eloquent\SoftDeletes;
    use GeneaLabs\LaravelModelCaching\Traits\Cachable;
    abstract class BaseMessage extends BaseModel
    {
        protected $table = 'messages';
        public $statusMessage = [];
        protected $dates = ['created_at', 'updated_at'];

        public function __construct(array $attributes = [])
        {
            $this->statusMessage = [0 => __('New'), 1 => __('Answered')];
            parent::__construct($attributes);
        }

        public function getFilters()
        {
            $this->searchColumns = [
                [
                    'name' => 'name', 'title' => 'Name', 'type' => 'text',
                ], [
                    'name' => 'email', 'title' => 'Email', 'type' => 'text',
                ], [
                    'name' => 'phone', 'title' => 'Phone', 'type' => 'text',
                ], [
                    'name' => 'message', 'title' => 'Message', 'type' => 'text',
                ], [
                    'name' => 'status', 'title' => 'Status', 'type' => 'text',
                ],
            ];

            return parent::getFilters();
        }

        protected $with = [];
        protected $fillable = ['name', 'email', 'phone', 'message', 'status','subject','content'];
        protected $casts = [];
    }
