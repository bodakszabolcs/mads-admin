<?php

namespace Modules\Message\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MessageCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'email' => 'required',
			'phone' => 'required',
			'message' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Name'),
'email' => __('Email'),
'phone' => __('Phone'),
'message' => __('Message'),
'status' => __('Status'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
