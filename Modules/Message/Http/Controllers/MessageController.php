<?php

namespace Modules\Message\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Mail\StatusChangeEmail;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Mail;
use Modules\Message\Entities\Message;
use Illuminate\Http\Request;
use Modules\Message\Http\Requests\MessageCreateRequest;
use Modules\Message\Http\Requests\MessageUpdateRequest;
use Modules\Message\Http\Requests\SendReplyRequest;
use Modules\Message\Transformers\MessageViewResource;
use Modules\Message\Transformers\MessageListResource;

class MessageController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Message();
        $this->viewResource = MessageViewResource::class;
        $this->listResource = MessageListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = MessageCreateRequest::class;
        $this->updateRequest = MessageUpdateRequest::class;
    }
    public function Answered(Request $request,$id){
        $this->model = $this->model->where('id', '=', $id);
        try {
            $this->model = $this->model->firstOrFail();
            $this->model->status = 1;
            $this->model->save();

        } catch (ModelNotFoundException $e) {
           return response()->json($this->errorMessage,$this->errorStatus);
        }

        return response()->json($this->successMessage,$this->successStatus);
    }
    public function sendReply(SendReplyRequest $request){
        try {
            $msg = Message::where('id', '=', $request->input('id'))->firstOrFail();
            $msg->fill($request->all());
            $msg->save();
            Mail::to($msg->email)->send(new \App\Mail\Message($msg));
            $msg->status = 1;
            $msg->save();
            return response()->json($this->successMessage,$this->successStatus);
        }
        catch (ModelNotFoundException $e){
            return \response()->json($this->errorMessage,$this->errorStatus);
        }
    }

}
