import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Message from '../components/Message'
import MessageList from '../components/MessageList'
import MessageCreate from '../components/MessageCreate'
import MessageEdit from '../components/MessageEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'message',
                component: Message,
                meta: {
                    title: 'Messages'
                },
                children: [
                    {
                        path: 'index',
                        name: 'MessageList',
                        component: MessageList,
                        meta: {
                            title: 'Messages',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/message/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/message/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'MessageCreate',
                        component: MessageCreate,
                        meta: {
                            title: 'Create Messages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/message/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'MessageEdit',
                        component: MessageEdit,
                        meta: {
                            title: 'Edit Messages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/message/index'
                        }
                    }
                ]
            }
        ]
    }
]
