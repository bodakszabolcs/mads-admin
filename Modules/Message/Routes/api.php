<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
    Route::group(['namespace' => 'Modules\Message\Http\Controllers','prefix'=>'message', 'middleware' => ['auth:sanctum']], function () {
        Route::get('/answered/{id}', 'MessageController@Answered');
        Route::post('/send-reply', 'MessageController@sendReply');
    });
