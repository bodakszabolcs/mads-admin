import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Slider from '../components/Slider'
import SliderList from '../components/SliderList'
import SliderCreate from '../components/SliderCreate'
import SliderEdit from '../components/SliderEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'sliders',
                component: Slider,
                meta: {
                    title: 'Sliders'
                },
                children: [
                    {
                        path: 'index',
                        name: 'SliderList',
                        component: SliderList,
                        meta: {
                            title: 'Sliders',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/sliders/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/sliders/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'SliderCreate',
                        component: SliderCreate,
                        meta: {
                            title: 'Create Sliders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/sliders/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'SliderEdit',
                        component: SliderEdit,
                        meta: {
                            title: 'Edit Sliders',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/sliders/index'
                        }
                    }
                ]
            }
        ]
    }
]
