<?php

namespace Modules\Slider\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\Storage;
use Modules\Content\Entities\Content;
use Modules\Slider\Entities\Slider;


abstract class BaseSlider extends BaseModel
{


    protected $table = 'sliders';

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'name',
                'title' => 'Name',
                'type' => 'text',
            ],
            [
                'name' => 'link',
                'title' => 'Link',
                'type' => 'text',
            ]
        ];

        return parent::getFilters();
    }

   public function fillAndSave(array $request)
   {
       $this->fill($request);

       $this->save();
       $file=[];
       $data = Slider::orderBy('order')->where('active',1)->get();
       foreach ($data as $d){
           $file[$d->order] = [
               'title' => $d->title,
               'lead' => $d->lead,
               'button_text' => $d->button_text,
               'link' => $d->link,
               'image' => str_replace('//','/','https:///mads.eu-central-1.linodeobjects.com/'.$d->image_path),
               'sec_image' => str_replace('//','/','https:///mads.eu-central-1.linodeobjects.com/'.$d->second_image),
               'mobile_image' => str_replace('//','/','https:///mads.eu-central-1.linodeobjects.com/'.$d->image_mobile_path),
           ];
       }
       Storage::disk(config('filesystems.default_upload_filesystem'))->delete('slider.json');
       Storage::disk(config('filesystems.default_upload_filesystem'))->put('slider.json',json_encode($file));
       return $this;
   }

    protected $fillable = ['name','lead','button_text','link', 'order', 'image_path','second_image','title','active','image_mobile_path'];

    protected $casts = [];


}
