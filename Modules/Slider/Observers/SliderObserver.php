<?php

namespace Modules\Slider\Observers;

use Illuminate\Support\Facades\Storage;
use Modules\Slider\Entities\Slider;

class SliderObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function saved(Slider $model)
    {
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function created(Slider $model)
    {
        $lastSlider = Slider::orderBy('order','DESC')->first();
        $model->order = optional($lastSlider)->order + 1;
        $model->save();
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function updated(Slider $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function deleted(Slider $model)
    {
        $file=[];
        $data = Slider::orderBy('order')->where('active',1)->get();
        foreach ($data as $d){
            $file[$d->order] = [
                'title' => $d->title,
                'lead' => $d->lead,
                'button_text' => $d->button_text,
                'link' => $d->link,
                'image' => $d->image_path,
                'sec_image' => $d->second_image,
            ];
        }
        Storage::disk(config('filesystems.default_upload_filesystem'))->delete('slider.json');
        Storage::disk(config('filesystems.default_upload_filesystem'))->put('slider.json',json_encode($file));
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function restored(Slider $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Slider\Entities\Slider  $model
     * @return void
     */
    public function forceDeleted(Slider $model)
    {
        //
    }
}
