<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Slider\Http\Controllers',
    'prefix' => '/slider',
    'middleware' => ['auth:sanctum', 'accesslog', 'role']
], function () {
    Route::post('/move/{id}/{type}', 'SliderController@move')->name('Change slider order');
});
