<?php

namespace Modules\Slider\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class SliderViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "lead" => $this->lead,
		    "button_text" => $this->button_text,
		    "link" => $this->link,
		    "order" => $this->order,
		    "image_path" => str_replace('///','/','/'.$this->image_path),
		    "active" => $this->active,
		    "second_image" => str_replace('///','/','/'.$this->second_image),
		    "image_mobile_path" => str_replace('///','/','/'.$this->image_mobile_path),
		    "title" => $this->title,
                "selectables" => [
                ]
		     ];
    }
}
