<?php

namespace Modules\Semester\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class SemesterListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "type" => $this->type,
		    "type_string" => ($this->type==1)?'Félév(Diák)':(($this->type==3)?'Félév(Jogviszony)':'Tanév'),
		    "start" => $this->start,
		    "end" => $this->end,
		    "name" => $this->name,
		    "key" => $this->key,
		    "active" => ConstansHelper::bool($this->active),
		     ];
    }
}
