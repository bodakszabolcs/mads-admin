<?php

namespace Modules\Semester\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class SemesterViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "type" => $this->type,
		    "start" => $this->start,
		    "end" => $this->end,
		    "name" => $this->name,
		    "key" => $this->key,
		    "active" => $this->active,
		"selectables" => [
		]
		     ];
    }
}
