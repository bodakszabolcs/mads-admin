import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Semester from '../components/Semester'
import SemesterList from '../components/SemesterList'
import SemesterCreate from '../components/SemesterCreate'
import SemesterEdit from '../components/SemesterEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'semester',
                component: Semester,
                meta: {
                    title: 'Semesters'
                },
                children: [
                    {
                        path: 'index',
                        name: 'SemesterList',
                        component: SemesterList,
                        meta: {
                            title: 'Semesters',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/semester/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/semester/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'SemesterCreate',
                        component: SemesterCreate,
                        meta: {
                            title: 'Create Semesters',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/semester/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'SemesterEdit',
                        component: SemesterEdit,
                        meta: {
                            title: 'Edit Semesters',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/semester/index'
                        }
                    }
                ]
            }
        ]
    }
]
