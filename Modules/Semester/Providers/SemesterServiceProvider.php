<?php

namespace Modules\Semester\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Semester\Entities\Semester;
use Modules\Semester\Observers\SemesterObserver;

class SemesterServiceProvider extends ModuleServiceProvider
{
    protected $module = 'semester';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Semester::observe(SemesterObserver::class);
    }
}
