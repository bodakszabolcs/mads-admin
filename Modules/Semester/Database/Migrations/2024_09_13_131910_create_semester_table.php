<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSemesterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('semester');
        Schema::create('semester', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->tinyInteger("type")->unsigned()->nullable();
$table->date("start")->nullable();
$table->date("end")->nullable();
$table->text("name")->nullable();
$table->text("key")->nullable();
$table->tinyInteger("active")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('semester');
    }
}
