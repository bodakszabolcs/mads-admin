<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Semester\Entities\Semester;

$factory->define(Semester::class, function (Faker $faker) {
    return [
        "type" => rand(1,10),
"start" => $faker->date(),
"end" => $faker->date(),
"name" => $faker->realText(),
"key" => $faker->realText(),
"active" => rand(1,10),

    ];
});
