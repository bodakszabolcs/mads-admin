<?php

namespace Modules\Semester\Observers;

use Modules\Semester\Entities\Semester;

class SemesterObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function saved(Semester $model)
    {
        Semester::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function created(Semester $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function updated(Semester $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function deleted(Semester $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function restored(Semester $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Semester\Entities\Semester  $model
     * @return void
     */
    public function forceDeleted(Semester $model)
    {
        //
    }
}
