<?php

namespace Modules\Semester\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseSemester extends BaseModel
{
    

    protected $table = 'semester';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'start',
                    'title' => 'Kezdete',
                    'type' => 'text',
                    ],[
                    'name' => 'end',
                    'title' => 'Vége',
                    'type' => 'text',
                    ],[
                    'name' => 'name',
                    'title' => 'Megnevezés',
                    'type' => 'text',
                    ],[
                    'name' => 'key',
                    'title' => 'key',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['type','start','end','name','key','active'];

    protected $casts = [];

    
}
