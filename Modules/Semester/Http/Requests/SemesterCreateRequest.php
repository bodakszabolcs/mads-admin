<?php

namespace Modules\Semester\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SemesterCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
			'start' => 'required',
			'end' => 'required',
			'name' => 'required',
			'key' => 'required',
			'active' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'type' => __('Típus'),
'start' => __('Kezdete'),
'end' => __('Vége'),
'name' => __('Megnevezés'),
'key' => __('key'),
'active' => __('Aktív'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
