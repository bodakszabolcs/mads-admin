<?php

namespace Modules\Semester\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Semester\Entities\Semester;
use Illuminate\Http\Request;
use Modules\Semester\Http\Requests\SemesterCreateRequest;
use Modules\Semester\Http\Requests\SemesterUpdateRequest;
use Modules\Semester\Transformers\SemesterViewResource;
use Modules\Semester\Transformers\SemesterListResource;

class SemesterController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Semester();
        $this->viewResource = SemesterViewResource::class;
        $this->listResource = SemesterListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = SemesterCreateRequest::class;
        $this->updateRequest = SemesterUpdateRequest::class;
    }

}
