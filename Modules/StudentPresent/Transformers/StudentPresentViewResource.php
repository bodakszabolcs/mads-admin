<?php

namespace Modules\StudentPresent\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Student\Entities\Student;
		use Modules\Company\Entities\Company;
		use Modules\Company\Entities\Industry;
		

class StudentPresentViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "company_id" => $this->company_id,
		    "industry_id" => $this->industry_id,
		    "date" => $this->date,
		    "start" => $this->start,
		    "end" => $this->end,
		    "approved" => $this->approved,
		    "appruved_user_id" => $this->appruved_user_id,
		"selectables" => [
		"student" => Student::pluck("name","id"),
		"company" => Company::pluck("name","id"),
		"industry" => Industry::pluck("name","id"),
		]
		     ];
    }
}
