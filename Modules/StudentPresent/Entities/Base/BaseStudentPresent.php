<?php

namespace Modules\StudentPresent\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\Student\Entities\Student;
		use Modules\Company\Entities\Company;
		use Modules\Company\Entities\Industry;


abstract class BaseStudentPresent extends BaseModel
{


    protected $table = 'student_presents';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'student_id',
                    'title' => 'Student',
                    'type' => 'text',
                    ],[
                    'name' => 'company_id',
                    'title' => 'Cég',
                    'type' => 'text',
                    ],[
                    'name' => 'industry_id',
                    'title' => 'Tevékenység',
                    'type' => 'text',
                    ],[
                    'name' => 'date',
                    'title' => 'Dátum',
                    'type' => 'text',
                    ],[
                    'name' => 'start',
                    'title' => 'Start',
                    'type' => 'text',
                    ],[
                    'name' => 'end',
                    'title' => 'End',
                    'type' => 'text',
                    ],[
                    'name' => 'approved',
                    'title' => 'jóváhagyva',
                    'type' => 'text',
                    ],[
                    'name' => 'appruved_user_id',
                    'title' => 'jóváhagyó',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = ["student","company","industry"];

    protected $fillable = ['student_id','company_id','industry_id','date','start','end','hour','approved_user_id','break_from','break_to'];

    protected $casts = [];

     public function student()
    {
        return $this->hasOne('Modules\Student\Entities\Student','id','student_id');
    }


 public function company()
    {
        return $this->hasOne('Modules\Company\Entities\Company','id','company_id');
    }


 public function industry()
    {
        return $this->hasOne('Modules\Company\Entities\Industry','id','industry_id');
    }



}
