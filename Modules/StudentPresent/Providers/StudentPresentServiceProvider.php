<?php

namespace Modules\StudentPresent\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\StudentPresent\Observers\StudentPresentObserver;

class StudentPresentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'studentpresent';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        StudentPresent::observe(StudentPresentObserver::class);
    }
}
