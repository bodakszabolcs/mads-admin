import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import StudentPresent from '../components/StudentPresent'
import StudentPresentList from '../components/StudentPresentList'
import StudentPresentCreate from '../components/StudentPresentCreate'
import StudentPresentEdit from '../components/StudentPresentEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'studentpresent',
                component: StudentPresent,
                meta: {
                    title: 'StudentPresents'
                },
                children: [
                    {
                        path: 'index',
                        name: 'StudentPresentList',
                        component: StudentPresentList,
                        meta: {
                            title: 'StudentPresents',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/studentpresent/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/studentpresent/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'StudentPresentCreate',
                        component: StudentPresentCreate,
                        meta: {
                            title: 'Create StudentPresents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/studentpresent/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'StudentPresentEdit',
                        component: StudentPresentEdit,
                        meta: {
                            title: 'Edit StudentPresents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/studentpresent/index'
                        }
                    }
                ]
            }
        ]
    }
]
