<?php

namespace Modules\StudentPresent\Observers;

use Modules\StudentPresent\Entities\StudentPresent;

class StudentPresentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function saved(StudentPresent $model)
    {
        StudentPresent::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function created(StudentPresent $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function updated(StudentPresent $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function deleted(StudentPresent $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function restored(StudentPresent $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\StudentPresent\Entities\StudentPresent  $model
     * @return void
     */
    public function forceDeleted(StudentPresent $model)
    {
        //
    }
}
