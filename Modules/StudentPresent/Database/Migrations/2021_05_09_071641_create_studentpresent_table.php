<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentPresentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('student_presents');
        Schema::create('student_presents', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("student_id")->unsigned()->nullable();
$table->bigInteger("company_id")->unsigned()->nullable();
$table->bigInteger("industry_id")->unsigned()->nullable();
$table->date("date")->nullable();
$table->string("start")->nullable();
$table->text("end")->nullable();
$table->timestamp("approved")->nullable();
$table->bigInteger("appruved_user_id")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_presents');
    }
}
