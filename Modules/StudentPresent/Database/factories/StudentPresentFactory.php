<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\StudentPresent\Entities\StudentPresent;

$factory->define(StudentPresent::class, function (Faker $faker) {
    return [
        "student_id" => rand(1000,5000),
"company_id" => rand(1000,5000),
"industry_id" => rand(1000,5000),
"date" => $faker->date(),
"start" => $faker->realText(),
"end" => $faker->realText(),
"approved" => $faker->dateTime(),
"appruved_user_id" => rand(1000,5000),

    ];
});
