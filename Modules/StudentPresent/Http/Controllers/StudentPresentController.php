<?php

namespace Modules\StudentPresent\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\StudentPresent\Entities\StudentPresent;
use Illuminate\Http\Request;
use Modules\StudentPresent\Http\Requests\StudentPresentCreateRequest;
use Modules\StudentPresent\Http\Requests\StudentPresentUpdateRequest;
use Modules\StudentPresent\Transformers\StudentPresentViewResource;
use Modules\StudentPresent\Transformers\StudentPresentListResource;

class StudentPresentController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new StudentPresent();
        $this->viewResource = StudentPresentViewResource::class;
        $this->listResource = StudentPresentListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = StudentPresentCreateRequest::class;
        $this->updateRequest = StudentPresentUpdateRequest::class;
    }

}
