<?php

namespace Modules\StudentPresent\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentPresentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
			'company_id' => 'required',
			'industry_id' => 'required',
			'date' => 'required',
			'start' => 'required',
			'end' => 'required',
			'approved' => 'required',
			'appruved_user_id' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'student_id' => __('Student'),
'company_id' => __('Cég'),
'industry_id' => __('Tevékenység'),
'date' => __('Dátum'),
'start' => __('Start'),
'end' => __('End'),
'approved' => __('jóváhagyva'),
'appruved_user_id' => __('jóváhagyó'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
