import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Realization from '../components/Realization'
import RealizationList from '../components/RealizationList'
import RealizationCreate from '../components/RealizationCreate'
import RealizationEdit from '../components/RealizationEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'realization',
                component: Realization,
                meta: {
                    title: 'Realizations'
                },
                children: [
                    {
                        path: 'index',
                        name: 'RealizationList',
                        component: RealizationList,
                        meta: {
                            title: 'Realizations',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/realization/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/realization/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'RealizationCreate',
                        component: RealizationCreate,
                        meta: {
                            title: 'Create Realizations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/realization/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'RealizationEdit',
                        component: RealizationEdit,
                        meta: {
                            title: 'Edit Realizations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/realization/index'
                        }
                    }
                ]
            }
        ]
    }
]
