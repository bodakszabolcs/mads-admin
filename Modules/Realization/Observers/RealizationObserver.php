<?php

namespace Modules\Realization\Observers;

use Modules\Realization\Entities\Realization;

class RealizationObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function saved(Realization $model)
    {
        Realization::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function created(Realization $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function updated(Realization $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function deleted(Realization $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function restored(Realization $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Realization\Entities\Realization  $model
     * @return void
     */
    public function forceDeleted(Realization $model)
    {
        //
    }
}
