<?php

namespace Modules\Realization\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RealizationCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'required',
            'end' => 'required',
            'monday' => 'required',
            'tuesday' => 'required',
            'wednesday' => 'required',
            'thursday' => 'required',
            'friday' => 'required',
            'saturday' => 'required',
            'sunday' => 'required',
            'work' => 'required',
            'location' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'start' => __('Start'),
            'end' => __('End'),
            'monday' => __('monday'),
            'tuesday' => __('Tuesday'),
            'wednesday' => __('Wednesday'),
            'thursday' => __('Thursday'),
            'friday' => __('Friday'),
            'saturday' => __('Saturday'),
            'sunday' => __('Sunday'),
            'work' => __('Work'),
            'location' => __('Location'),
            'underage' => __('Underage'),
            'student_id' => __('Student'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
