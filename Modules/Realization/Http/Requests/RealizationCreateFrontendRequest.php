<?php

namespace Modules\Realization\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RealizationCreateFrontendRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'required',
            'underage' => 'required',
            'end' => 'required',
            'monday' => 'required',
            'tuesday' => 'required',
            'wednesday' => 'required',
            'thursday' => 'required',
            'friday' => 'required',
            'saturday' => 'required',
            'sunday' => 'required',
            'work' => 'required',
            'location' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'start' => __('Kezdez'),
            'end' => __('Vége'),
            'monday' => __('Hétfő'),
            'tuesday' => __('Kedd'),
            'wednesday' => __('Szerda'),
            'thursday' => __('Csütörtök'),
            'friday' => __('Péntek'),
            'saturday' => __('Szombat'),
            'sunday' => __('Vasárnap'),
            'work' => __('Milyen munka érdekel'),
            'location' => __('Hol szeretne dolgozni'),
            'underage' => __('Kiskorú'),


        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
