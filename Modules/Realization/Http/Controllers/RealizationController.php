<?php

namespace Modules\Realization\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Realization\Entities\Realization;
use Illuminate\Http\Request;
use Modules\Realization\Http\Requests\RealizationCreateRequest;
use Modules\Realization\Http\Requests\RealizationUpdateRequest;
use Modules\Realization\Transformers\RealizationViewResource;
use Modules\Realization\Transformers\RealizationListResource;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkLocation\Entities\WorkLocation;

class RealizationController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Realization();
        $this->viewResource = RealizationViewResource::class;
        $this->listResource = RealizationListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = RealizationCreateRequest::class;
        $this->updateRequest = RealizationUpdateRequest::class;
    }
    public function getSelect(Request $request){

        $work_categories= WorkCategory::whereNull('parent_id')->get()->pluck('name','id');
        $work_location = WorkLocation::all()->pluck('name','id');
        $start = date('Y-m-d',strtotime('next monday'));
        $end = date('Y-m-d',strtotime($start.' +7 day'));
        return response()->json(['works'=>$work_categories,'cities'=>$work_location,'start'=>$start,'end'=>$end]);

    }
}
