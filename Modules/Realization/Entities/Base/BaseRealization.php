<?php

namespace Modules\Realization\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseRealization extends BaseModel
{


    protected $table = 'realizations';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'start',
                    'title' => 'Start',
                    'type' => 'text',
                    ],[
                    'name' => 'end',
                    'title' => 'End',
                    'type' => 'text',
                    ],[
                    'name' => 'work',
                    'title' => 'Work',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['start','end','monday','tuesday','wednesday','thursday','friday','saturday','sunday','work','location','underage','student_id'];

    protected $casts = ['location'=>'array'];


}
