<?php

namespace Modules\Realization\Entities;

use Modules\Realization\Entities\Base\BaseRealization;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\WorkLocation\Entities\WorkLocation;
use Spatie\Translatable\HasTranslations;


class Realization extends BaseRealization
{
    use SoftDeletes, Cachable;



    public function workName()
    {
        return $this->hasOne('Modules\WorkCategory\Entities\WorkCategory', 'id', 'work');
    }
    public function locationName()
    {
       if(is_array($this->location)) {
           $list = WorkLocation::whereIn('id', $this->location)->get();
       }else{
           $list = WorkLocation::where('id', $this->location)->get();
       }
        $rs = [];
        foreach ($list as $w){
            $rs[]=$w->name;
        }
        return implode(',',$rs);
    }



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
