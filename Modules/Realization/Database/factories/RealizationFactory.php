<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Realization\Entities\Realization;

$factory->define(Realization::class, function (Faker $faker) {
    return [
        "start" => $faker->date(),
"end" => $faker->date(),
"monday" => rand(1,10),
"tuesday" => rand(1,10),
"wednesday" => rand(1,10),
"thursday" => rand(1,10),
"friday" => rand(1,10),
"saturday" => rand(1,10),
"sunday" => rand(1,10),
"work" => $faker->realText(),
"location" => rand(1000,5000),
"underage" => rand(1,10),
"student_id" => rand(1000,5000),

    ];
});
