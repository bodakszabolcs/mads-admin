<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealizationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('realizations');
        Schema::create('realizations', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->date("start")->nullable();
$table->date("end")->nullable();
$table->tinyInteger("monday")->unsigned()->nullable();
$table->tinyInteger("tuesday")->unsigned()->nullable();
$table->tinyInteger("wednesday")->unsigned()->nullable();
$table->tinyInteger("thursday")->unsigned()->nullable();
$table->tinyInteger("friday")->unsigned()->nullable();
$table->tinyInteger("saturday")->unsigned()->nullable();
$table->tinyInteger("sunday")->unsigned()->nullable();
$table->text("work")->nullable();
$table->bigInteger("location")->unsigned()->nullable();
$table->tinyInteger("underage")->unsigned()->nullable();
$table->bigInteger("student_id")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realizations');
    }
}
