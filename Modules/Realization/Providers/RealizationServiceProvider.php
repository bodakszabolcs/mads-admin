<?php

namespace Modules\Realization\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Realization\Entities\Realization;
use Modules\Realization\Observers\RealizationObserver;

class RealizationServiceProvider extends ModuleServiceProvider
{
    protected $module = 'realization';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Realization::observe(RealizationObserver::class);
    }
}
