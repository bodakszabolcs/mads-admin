<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\Realization\Http\Controllers',
    'prefix' => '/realization',
    'middleware' => []
], function () {
    Route::get( '/select', 'RealizationController@getSelect')->name('Get realization select');
});
