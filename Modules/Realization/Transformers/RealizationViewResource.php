<?php

namespace Modules\Realization\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class RealizationViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "start" => $this->start,
		    "end" => $this->end,
		    "monday" => $this->monday,
		    "tuesday" => $this->tuesday,
		    "wednesday" => $this->wednesday,
		    "thursday" => $this->thursday,
		    "friday" => $this->friday,
		    "saturday" => $this->saturday,
		    "sunday" => $this->sunday,
		    "work" => $this->work,
		    "location" => $this->location,
		    "underage" => $this->underage,
		    "student_id" => $this->student_id,
		"selectables" => [
		]
		     ];
    }
}
