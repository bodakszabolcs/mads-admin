<?php

namespace Modules\Realization\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class RealizationStudentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "start" => date('Y.m.d',strtotime($this->start)).'-'.date('Y.m.d',strtotime($this->end)),

            "monday" => $this->monday,
            "tuesday" => $this->tuesday,
            "wednesday" => $this->wednesday,
            "thursday" => $this->thursday,
            "friday" => $this->friday,
            "saturday" => $this->saturday,
            "sunday" => $this->sunday,
            "work" => $this->work_name,
            "location" => $this->locationName(),
            "underage" => $this->birth_date >= date('Y-m-d',strtotime('-18 year')),
            "student_id" => $this->student_id,
            "name"=>$this->name,
            "phone"=>$this->phone,
            "email"=>$this->email,
            "birth_date"=>$this->birth_date,
        ];
    }

    private function getRealizationString($id)
    {
        switch ($id) {
            case 1:
            {
                return 'Egész nap ráér';
            }
            case 2:
            {
                return 'Délelőtt ráér';
            }
            case 3:
            {
                return 'Délután ráér';
            }
            default:
                return '';
        }

    }
}
