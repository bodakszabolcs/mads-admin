<?php

namespace Modules\Realization\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class RealizationListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
            "start" => $this->start,
            "end" => $this->end,
            "monday" => $this->getRealizationString($this->monday),
            "monday_number" => $this->monday,
            "tuesday" => $this->getRealizationString($this->tuesday),
            "tuesday_number" => $this->tuesday,
            "wednesday" => $this->getRealizationString($this->wednesday),
            "wednesday_number" => $this->wednesday,
            "thursday" => $this->getRealizationString($this->thursday),
            "thursday_number" =>$this->thursday,
            "friday" => $this->getRealizationString($this->friday),
            "friday_number" => $this->friday,
            "saturday" => $this->getRealizationString($this->saturday),
            "saturday_number" => $this->saturday,
            "sunday" => $this->getRealizationString($this->sunday),
            "sunday_number" => $this->sunday,
            "work" => optional($this->workName)->name,
            "location" => $this->locationName(),
            "underage" => $this->underage,
            "student_id" => $this->student_id,
            "birth_date" => $this->birth_date,
        ];
    }

    private function getRealizationString($id)
    {
        switch ($id) {
            case 1:
            {
                return 'Egész nap ráér';
            }
            case 2:
            {
                return 'Délelőtt ráér';
            }
            case 3:
            {
                return 'Délután ráér';
            }
            default:
                return '';
        }

    }
}
