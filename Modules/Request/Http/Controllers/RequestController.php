<?php

namespace Modules\Request\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;

use Illuminate\Support\Arr;
use Modules\Request\Http\Requests\RequestCreateRequest;
use Modules\Request\Http\Requests\RequestUpdateRequest;
use Modules\Request\Transformers\RequestViewResource;
use Modules\Request\Transformers\RequestListResource;
use Illuminate\Http\Request;
class RequestController extends AbstractLiquidController
{

    public function __construct( Request $request)
    {
        parent::__construct($request);
        $this->model = new \Modules\Request\Entities\Request();
        $this->viewResource = RequestViewResource::class;
        $this->listResource = RequestListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = RequestCreateRequest::class;
        $this->updateRequest = RequestUpdateRequest::class;
    }
    public function create(Request $request)
    {
        if ($this->createRequest) {
            app()->make($this->createRequest);
        }
        $this->model->web = 0;
        $this->model->fillAndSave($request->all());

        return new $this->viewResource($this->model);
    }
    public function index(Request $request, $auth = null)
    {
        $list = $this->model->searchInModel($request->input());
        if($request->has('web')){
            $list =$list->where('web',$request->input('web'));
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
}
