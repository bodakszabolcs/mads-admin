<?php

namespace Modules\Request\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required',
            'company_name' => 'required',
            'city' => 'required',
            'zip' => 'required',
            'contact_name' => 'required',
            'contact_phone' => 'required',
            'contact_email' => 'required',
            'task' => 'required',
            'location' => 'required',
            'duration' => 'required',
            'program' => 'required',
            'type' => 'required',
            'description' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'subject' => __('Ajánlatkérés tárgy'),
            'company_name' => __('Cég neve'),
            'city' => __('Város'),
            'zip' => __('Irányítószám'),
            'contact_name' => __('Kapcsolattartó név'),
            'contact_phone' => __('Kapcsolattartó telefon'),
            'contact_email' => __('Kapcsolattartó email'),
            'task' => __('Elvégzendő feladat'),
            'location' => __('Munkavégzés helye'),
            'duration' => __('Megbizás időtartama'),
            'program' => __('Munkarend'),
            'type' => __('Típus'),
            'description' => __('Tevljenység rövid benutatása'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
