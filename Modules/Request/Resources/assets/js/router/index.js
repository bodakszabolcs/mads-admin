import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Request from '../components/Request'
import RequestList from '../components/RequestList'
import RequestCreate from '../components/RequestCreate'
import RequestEdit from '../components/RequestEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'request',
                component: Request,
                meta: {
                    title: 'Requests'
                },
                children: [
                    {
                        path: 'index',
                        name: 'RequestList',
                        component: RequestList,
                        meta: {
                            title: 'Telefonos árajánlatok',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/request/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/request/index'
                        }
                    },
                    {
                        path: 'index-web',
                        name: 'RequestListWeb',
                        component: RequestList,
                        meta: {
                            title: 'Webes árajánlatok',
                            subheader: true
                        }
                    },
                    {
                        path: 'create',
                        name: 'RequestCreate',
                        component: RequestCreate,
                        meta: {
                            title: 'Create Requests',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/request/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'RequestEdit',
                        component: RequestEdit,
                        meta: {
                            title: 'Edit Requests',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/request/index'
                        }
                    }
                ]
            }
        ]
    }
]
