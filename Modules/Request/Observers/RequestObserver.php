<?php

namespace Modules\Request\Observers;

use Modules\Request\Entities\Request;

class RequestObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function saved(Request $model)
    {
        Request::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function created(Request $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function updated(Request $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function deleted(Request $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function restored(Request $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Request\Entities\Request  $model
     * @return void
     */
    public function forceDeleted(Request $model)
    {
        //
    }
}
