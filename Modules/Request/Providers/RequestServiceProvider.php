<?php

namespace Modules\Request\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Request\Entities\Request;
use Modules\Request\Observers\RequestObserver;

class RequestServiceProvider extends ModuleServiceProvider
{
    protected $module = 'request';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Request::observe(RequestObserver::class);
    }
}
