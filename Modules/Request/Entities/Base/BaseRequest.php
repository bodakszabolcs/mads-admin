<?php

namespace Modules\Request\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseRequest extends BaseModel
{


    protected $table = 'requests';

    protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
            'name' => 'company_name',
            'title' => 'Cég neve',
            'type' => 'text',
        ], [
            'name' => 'contact_name',
            'title' => 'Kapcsolattartó név',
            'type' => 'text',
        ], [
            'name' => 'contact_email',
            'title' => 'Kapcsolattartó email',
            'type' => 'text',
        ], [
            'name' => 'task',
            'title' => 'Elvégzendő feladat',
            'type' => 'text',
        ], [
            'name' => 'subject',
            'title' => 'Tárgy',
            'type' => 'select',
            'data' => [0 => 'Diákmunka',
                1 => 'Munkaerő közvetítés',
                2 => 'Munkaerő kölcsönzés',
                3 => 'Nyugdíjasok foglalkoztatása',]
        ]];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['subject', 'company_name', 'city', 'zip', 'contact_name', 'contact_phone', 'contact_email', 'task', 'location', 'duration', 'program', 'type', 'description','address'];

    protected $casts = ['subject'=>'json'];


}
