<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('requests');
        Schema::create('requests', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("subject")->unsigned()->nullable();
$table->text("company_name")->nullable();
$table->text("city")->nullable();
$table->integer("zip")->unsigned()->nullable();
$table->text("contact_name")->nullable();
$table->text("contact_phone")->nullable();
$table->text("contact_email")->nullable();
$table->text("task")->nullable();
$table->text("location")->nullable();
$table->text("duration")->nullable();
$table->text("program")->nullable();
$table->text("type")->nullable();
$table->text("description")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('requests');
    }
}
