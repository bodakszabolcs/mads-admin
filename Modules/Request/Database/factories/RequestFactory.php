<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Request\Entities\Request;

$factory->define(Request::class, function (Faker $faker) {
    return [
        "subject" => rand(1000,5000),
"company_name" => $faker->realText(),
"city" => $faker->realText(),
"zip" => rand(1000,5000),
"contact_name" => $faker->realText(),
"contact_phone" => $faker->realText(),
"contact_email" => $faker->realText(),
"task" => $faker->realText(),
"location" => $faker->realText(),
"duration" => $faker->realText(),
"program" => $faker->realText(),
"type" => $faker->realText(),
"description" => $faker->realText(),

    ];
});
