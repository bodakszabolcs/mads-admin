<?php

namespace Modules\Request\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class RequestListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    private $types = [0 => 'Diákmunka',
        1 => 'Munkaerő közvetítés',
        2 => 'Munkaerő kölcsönzés',
        3 => 'Nyugdíjasok foglalkoztatása'];

    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "subject" => $this->getSubject(),
            "company_name" => $this->company_name,
            "city" => $this->city,
            "zip" => $this->zip,
            "contact_name" => $this->contact_name,
            "contact_phone" => $this->contact_phone,
            "contact_email" => $this->contact_email,
            "task" => $this->task,
            "location" => $this->location,
            "duration" => $this->duration,
            "program" => $this->program,
            "type" => $this->type,
            "description" => $this->description,
        ];
    }
    private function getSubject(){
        $rs=[];
        foreach ($this->subject as $k=> $item){

           $rs[]= array_get($this->types,$k,'');
        }
        return implode(", ",$rs);
    }
}
