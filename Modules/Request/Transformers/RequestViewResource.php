<?php

namespace Modules\Request\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class RequestViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "subject" => $this->subject,
            "company_name" => $this->company_name,
            "city" => $this->city,
            "zip" => $this->zip,
            "contact_name" => $this->contact_name,
            "contact_phone" => $this->contact_phone,
            "contact_email" => $this->contact_email,
            "task" => $this->task,
            "location" => $this->location,
            "duration" => $this->duration,
            "program" => $this->program,
            "type" => $this->type,
            "description" => $this->description,
            "selectables" => [
                'subjects' => [
                    0 => 'Diákmunka',
                    1 => 'Munkaerő közvetítés',
                    2 => 'Munkaerő kölcsönzés',
                    3 => 'Nyugdíjasok foglalkoztatása',
                ]
            ]
        ];
    }
}
