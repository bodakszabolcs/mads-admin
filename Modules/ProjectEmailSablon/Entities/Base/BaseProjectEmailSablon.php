<?php

namespace Modules\ProjectEmailSablon\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Spatie\Translatable\HasTranslations;
use Modules\Project\Entities\Project;


abstract class BaseProjectEmailSablon extends BaseModel
{


    protected $table = 'project_email_sablon';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                    'name' => 'name',
                    'title' => 'Sablon ',
                    'type' => 'text',
            ],[
            'name' => 'status_id',
            'title' => 'Project állapot',
            'type' => 'select',
            'data'=> ProjectStatus::all()->pluck("name",'id')
        ]];

        return parent::getFilters();
    }


    protected $fillable = ['status_id','name','content'];

    protected $casts = [];

     public function projectStatus()
    {
        return $this->hasOne('Modules\ProjectStatus\Entities\ProjectStatus','id','status_id');
    }



}
