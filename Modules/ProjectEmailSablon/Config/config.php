<?php

return [
    'name' => 'ProjectEmailSablon',

                 'menu_order' => 47,

                 'menu' => [
                     [

                      'icon' =>'fa fa-envelope',

                      'title' =>'Project email sablonok',

                      'route' =>'/'.env('ADMIN_URL').'/project-email-sablon/index',

                     ]

                 ]
];
