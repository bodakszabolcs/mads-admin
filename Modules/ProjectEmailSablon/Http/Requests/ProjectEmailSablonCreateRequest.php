<?php

namespace Modules\ProjectEmailSablon\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectEmailSablonCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status_id' => 'required',
            'name' => 'required',
            'content' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'status_id' => __('Projekt státusz'),
            'name' => __('Sablon neve'),
            'content' => __('Tartalom'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
