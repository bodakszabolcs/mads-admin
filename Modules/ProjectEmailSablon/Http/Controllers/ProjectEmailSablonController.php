<?php

namespace Modules\ProjectEmailSablon\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProjectEmailSablon\Entities\ProjectEmailSablon;
use Illuminate\Http\Request;
use Modules\ProjectEmailSablon\Http\Requests\ProjectEmailSablonCreateRequest;
use Modules\ProjectEmailSablon\Http\Requests\ProjectEmailSablonUpdateRequest;
use Modules\ProjectEmailSablon\Transformers\ProjectEmailSablonViewResource;
use Modules\ProjectEmailSablon\Transformers\ProjectEmailSablonListResource;

class ProjectEmailSablonController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProjectEmailSablon();
        $this->viewResource = ProjectEmailSablonViewResource::class;
        $this->listResource = ProjectEmailSablonListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProjectEmailSablonCreateRequest::class;
        $this->updateRequest = ProjectEmailSablonUpdateRequest::class;
    }

}
