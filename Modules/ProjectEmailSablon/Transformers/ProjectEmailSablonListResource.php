<?php

namespace Modules\ProjectEmailSablon\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
class ProjectEmailSablonListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "status_id" => $this->status_id,
		    "status_name" => optional($this->projectStatus)->name,
		    "name" => $this->name,
		    "content" => $this->content,
		     ];
    }
}
