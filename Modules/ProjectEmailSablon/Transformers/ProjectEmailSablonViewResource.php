<?php

namespace Modules\ProjectEmailSablon\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Project\Entities\Project;
use Modules\ProjectStatus\Entities\ProjectStatus;

class ProjectEmailSablonViewResource extends BaseResource
{function toArray($request)
    {
        return [
            "id" => $this->id,
		    "status_id" => $this->status_id,
		    "name" => $this->name,
		    "content" => $this->content,
            "selectables"=>[
                "status_list"=> ProjectStatus::all()->pluck("name",'id')
            ]
		];
    }
}
