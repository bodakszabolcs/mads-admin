import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProjectEmailSablon from '../components/ProjectEmailSablon'
import ProjectEmailSablonList from '../components/ProjectEmailSablonList'
import ProjectEmailSablonCreate from '../components/ProjectEmailSablonCreate'
import ProjectEmailSablonEdit from '../components/ProjectEmailSablonEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'project-email-sablon',
                component: ProjectEmailSablon,
                meta: {
                    title: 'ProjectEmailSablons'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProjectEmailSablonList',
                        component: ProjectEmailSablonList,
                        meta: {
                            title: 'ProjectEmailSablons',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/project-email-sablon/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-email-sablon/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProjectEmailSablonCreate',
                        component: ProjectEmailSablonCreate,
                        meta: {
                            title: 'Create ProjectEmailSablons',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-email-sablon/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProjectEmailSablonEdit',
                        component: ProjectEmailSablonEdit,
                        meta: {
                            title: 'Edit ProjectEmailSablons',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-email-sablon/index'
                        }
                    }
                ]
            }
        ]
    }
]
