<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\ProjectEmailSablon\Entities\ProjectEmailSablon;

$factory->define(ProjectEmailSablon::class, function (Faker $faker) {
    return [
        "project_id" => rand(1000,5000),
"name" => $faker->realText(),
"content" => $faker->realText(),

    ];
});
