<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectEmailSablonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('project_email_sablon');
        Schema::create('project_email_sablon', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("project_id")->unsigned()->nullable();
$table->text("name")->nullable();
$table->text("content")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_email_sablon');
    }
}
