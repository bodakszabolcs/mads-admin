<?php

namespace Modules\ProjectEmailSablon\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProjectEmailSablon\Entities\ProjectEmailSablon;
use Modules\ProjectEmailSablon\Observers\ProjectEmailSablonObserver;

class ProjectEmailSablonServiceProvider extends ModuleServiceProvider
{
    protected $module = 'projectemailsablon';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProjectEmailSablon::observe(ProjectEmailSablonObserver::class);
    }
}
