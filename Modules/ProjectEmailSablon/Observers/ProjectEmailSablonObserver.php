<?php

namespace Modules\ProjectEmailSablon\Observers;

use Modules\ProjectEmailSablon\Entities\ProjectEmailSablon;

class ProjectEmailSablonObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function saved(ProjectEmailSablon $model)
    {
        ProjectEmailSablon::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function created(ProjectEmailSablon $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function updated(ProjectEmailSablon $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function deleted(ProjectEmailSablon $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function restored(ProjectEmailSablon $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProjectEmailSablon\Entities\ProjectEmailSablon  $model
     * @return void
     */
    public function forceDeleted(ProjectEmailSablon $model)
    {
        //
    }
}
