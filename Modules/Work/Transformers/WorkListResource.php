<?php

namespace Modules\Work\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class WorkListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "image" => $this->image,
		    "name" => $this->name,
		    "zip" => $this->zip,
		    "archive" => $this->archive,
		    "work_type" => $this->work_type,
		    "user" => optional($this->user)->lastname." ".optional($this->user)->firstname,
		    "work_category_id" => $this->work_category_id,
		    "work_location" => $this->work_location,
		    "city" => optional($this->city)->name,
		    "work_time" => $this->work_time,
		    "gross" => $this->gross,
		    "gross_type" => $this->gross_type,
		    "work_duration" => $this->work_duration,
		    "start" => $this->start,
		    "apply" => $this->apply,
		    "highlighted" => $this->highlighted,
		    "is_full" => $this->is_full,

            "highlighted_text" => ConstansHelper::bool($this->highlighted),
            "is_full_text" => ConstansHelper::bool($this->is_full),
            "archive_text" => ConstansHelper::bool($this->archive),

		    "lead" => $this->lead,
		    "task" => $this->task,
		    "company_description" => $this->company_description,
		    "interview_date" => $this->interview_date,
		    "preference" => $this->preference,
		    "condition" => $this->condition,
		    "comment" => $this->comment,
		    "slug" => $this->slug,
            'gross_text'=>$this->gross_text,
		     ];
    }
}
