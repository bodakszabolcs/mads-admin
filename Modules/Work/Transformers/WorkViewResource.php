<?php

namespace Modules\Work\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\DB;
use Modules\City\Entities\City;
use Modules\User\Entities\User;
use Modules\Project\Entities\Project;
use Modules\WebshopFrontend\Transformers\ProjectManagerResource;
use Modules\Work\Entities\Work;
use Modules\WorkCategory\Entities\WorkCategory;


class WorkViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public $jobhunter = [
        0 => 'Nincs kiválasztva',
        1 => 'Alapítvány / Non-profit',
        2 => 'Adminisztráció / Irodai munka / Asszisztencia',
        3 => 'Bank / Biztosítás / Pénzintézet',
        4 => 'Beszerzés / Logisztika / Szállítás',
        5 => 'Betanított / Fizikai / Segédmunka',
        6 => 'Biztonság / Honvédelem / Biztonságtechnika',
        7 => 'Diákmunka / Szakmai gyakorlat',
        8 => 'Egészségügy / Szociális ellátás / Gyógyszeripar/ Vegyipar',
        9 => 'Egyéb',
        10 => 'Elektronika / Telekommunikáció',
        11 => 'Energetika / Villamosság',
        12 => 'Építés / Ingatlan',
        13 => 'Értékesítés / Kereskedelem',
        14 => 'Gyártás / Termelés',
        15 => 'Humán erőforrás',
        16 => 'Idegenforgalom / Szálloda / Vendéglátás',
        17 => 'IT / Informatika',
        18 => 'Jog / Jogi tanácsadás',
        19 => 'Közigazgatás / Államigazgatás',
        20 => 'Kultúra / Művészet / Szórakoztatás',
        21 => 'Külföldi munka',
        22 => 'Marketing / Reklám / Média / PR',
        23 => 'Mérnök / Műszaki',
        24 => 'Mezőgazdaság / Környezettudomány / Környezetvédelem',
        25 => 'Oktatás / Képzés',
        26 => 'Pénzügy / Számvitel / Kontrolling',
        27 => 'Szakmunka / fizikai munka',
        28 => 'Szervezés / Menedzsment / Cégvezetés',
        29 => 'Távmunka / Otthoni munka',
        30 => 'Tudomány / Kutatás, fejlesztés',
        31 => 'Ügyfélszolgálat / Vevőszolgálat / Szolgáltatás'
    ];

    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "image" => $this->image,
            "name" => $this->name,
            "zip" => $this->zip,
            "work_type" => $this->work_type,
            "user_id" => $this->user_id,
            "work_category_id" => $this->work_category_id,
            "work_location" => $this->work_location,
            "city_id" => $this->city_id,
            "work_time" => $this->work_time,
            "gross" => $this->gross,
            "gross_type" => $this->gross_type,
            "work_duration" => $this->work_duration,
            "start" => $this->start,
            "apply" => $this->apply,
            "highlighted" => $this->highlighted,
            "is_full" => $this->is_full,
            "archive" => $this->archive,
            "lead" => $this->lead,
            "description" => $this->description,
            "task" => $this->task,
            "company_description" => $this->company_description,
            "interview_date" => $this->interview_date,
            "preference" => $this->preference,
            "condition" => $this->condition,
            "comment" => $this->comment,
            "project_id" => $this->project_id,
            "category_slug" => $this->getCategorySlug(),
            "slug" => '/' . $this->slug,
            "user" => new ProjectManagerResource($this->user),
            "updated_at" => ConstansHelper::formatDate($this->updated_at),
            'cv_online_kat_1' => $this->cv_online_kat_1,
            'cv_online_kat_2' => $this->cv_online_kat_2,
            'jobhunter_category'=>$this->jobhunter_category,
            'gross_text'=>$this->gross_text,

            "selectables" => [
                "users" => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name", "id"),
                "projects" => Project::select(DB::raw("concat_ws('/',companies.name,company_industries.name,projects.name) as name, projects.id as id "))->where('projects.status',0)
                    ->leftJoin('companies', 'companies.id', '=', 'company_id')->leftJoin('company_industries', 'company_industries.id', '=', 'industry_id')->get()->pluck("name", "id"),
                "categories" => WorkCategory::leftJoin('work_categories as w2', 'w2.id', '=', 'work_categories.parent_id')
                    ->select(DB::raw("work_categories.id as id, Concat_WS('/',w2.name,work_categories.name) as name"))->where('work_categories.parent_id', '>', 0)->pluck("name", "id"),
                "cities" => City::pluck("name", "id"),
                "cv_online" => Work::$cvonlineCategory,
                'jobhunter'=>$this->jobhunter
            ]
        ];
    }
}
