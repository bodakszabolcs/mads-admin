<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('works');
        Schema::create('works', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->integer("zip")->unsigned()->nullable();
            $table->text("name")->nullable();
            $table->text("slug")->nullable();
            $table->text("lead")->nullable();
            $table->text("description")->nullable();
            $table->text("work_type")->nullable();
            $table->text("work_category_id")->nullable();
            $table->text("work_location")->nullable();
            $table->text("city_id")->nullable();
            $table->text("task")->nullable();
            $table->text("condition")->nullable();
            $table->text("preference")->nullable();
            $table->text("work_time")->nullable();
            $table->text("work_duration")->nullable();
            $table->text("start")->nullable();
            $table->text("interview_date")->nullable();
            $table->text("comment")->nullable();
            $table->integer("gross")->unsigned()->nullable();
            $table->integer("gross_type")->unsigned()->nullable();
            $table->text("apply")->nullable();
            $table->integer("user_id")->nullable();
            $table->tinyInteger("highlighted")->unsigned()->nullable();
            $table->text("image")->nullable();
            $table->tinyInteger("archive")->unsigned()->nullable();
            $table->text("company_description")->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works');
    }
}
