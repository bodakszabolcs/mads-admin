<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Work\Entities\Work;

$factory->define(Work::class, function (Faker $faker) {
    return [
        "image" => $faker->realText(),
"zip" => rand(1000,5000),
"work_type" => $faker->realText(),
"user_id" => $faker->realText(),
"work_category_id" => $faker->realText(),
"work_location" => $faker->realText(),
"city_id" => $faker->realText(),
"work_time" => $faker->realText(),
"gross" => rand(1000,5000),
"gross_type" => rand(1000,5000),
"work_duration" => $faker->realText(),
"start" => $faker->realText(),
"apply" => $faker->realText(),
"highlighted" => rand(1,10),
"is_full" => rand(1,10),
"lead" => $faker->realText(),
"task" => $faker->realText(),
"company_description" => $faker->realText(),
"interview_date" => $faker->realText(),
"preference" => $faker->realText(),
"condition" => $faker->realText(),
"comment" => $faker->realText(),

    ];
});
