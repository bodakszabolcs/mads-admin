<?php

namespace Modules\Work\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
			'zip' => 'required',
			'work_type' => 'required',
			'user_id' => 'required',
			'work_category_id' => 'required',
			'city_id' => 'required',
			'work_time' => 'required',
			'gross' => 'required',
			'gross_type' => 'required',
			'work_duration' => 'required',
			'start' => 'required',
			'apply' => 'required',
			'highlighted' => 'required',
			'is_full' => 'required',
			'lead' => 'required',
			'project_id' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'image' => __('Kép'),
'zip' => __('Irányítószám'),
'project_id' => __('Projekt'),
'work_type' => __('Munka típusa'),
'user_id' => __('Projektmenedzser'),
'work_category_id' => __('Kategória'),
'work_location' => __('Munkavégzés helye'),
'city_id' => __('Város'),
'work_time' => __('Munkaidő'),
'gross' => __('Bruttó bér'),
'gross_type' => __('Bér típusa'),
'work_duration' => __('Munka időtartama'),
'start' => __('Kezdés'),
'apply' => __('Jelentkezés'),
'highlighted' => __('Kiemelt'),
'is_full' => __('Betelt'),
'lead' => __('Rövid leírás'),
'task' => __('Feladat'),
'company_description' => __('Cég bemutatása'),
'interview_date' => __('Interjú időpontja'),
'preference' => __('Előny'),
'condition' => __('Feltétel'),
'comment' => __('Megjegyzés'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
