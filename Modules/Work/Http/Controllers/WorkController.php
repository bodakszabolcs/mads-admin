<?php

namespace Modules\Work\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\SaveStatistic;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Modules\Work\Entities\Work;
use Illuminate\Http\Request;
use Modules\Work\Http\Requests\WorkCreateRequest;
use Modules\Work\Http\Requests\WorkUpdateRequest;
use Modules\Work\Transformers\WorkViewResource;
use Modules\Work\Transformers\WorkListResource;

class WorkController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Work();
        $this->viewResource = WorkViewResource::class;
        $this->listResource = WorkListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = WorkCreateRequest::class;
        $this->updateRequest = WorkUpdateRequest::class;
    }
    public function index(Request $request, $auth = null)
    {
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {
            if (!$request->has('user_id')) {
                $request->merge(['user_id' => \Auth::user()->id]);
            }
        }
        $list = $this->model->searchInModel($request->input());

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function create(Request $request)
    {
        if ($this->createRequest) {
            app()->make($this->createRequest);
        }
        $this->model->fillAndSave($request->all());
        SaveStatistic::dispatch(\Auth::user()->id,'new_adversting');
        return new $this->viewResource($this->model);
    }
    public function update(Request $request, $id, $auth = null)
    {
        if ($this->updateRequest) {
            app()->make($this->updateRequest);
        }

        $this->model = $this->model->where('id', '=', $id);

        try {
            if ($auth == null) {
                $this->model = $this->model->firstOrFail();
            } else {
                $this->model = $this->model->where('user_id', '=', $auth)->firstOrFail();
            }
            SaveStatistic::dispatch(\Auth::user()->id,'refreshed_adversting');
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->fillAndSave($request->all());

        return new $this->viewResource($this->model);
    }
    public function changeArchive(Request $request ,$id){
        $work = Work::where('id',$id)->first();
        $work->archive = $request->input('status');
        $work->save();
        return response()->json($this->successMessage);
    }
}
