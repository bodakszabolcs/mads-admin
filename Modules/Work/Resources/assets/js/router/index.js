import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Work from '../components/Work'
import WorkList from '../components/WorkList'
import WorkCreate from '../components/WorkCreate'
import WorkEdit from '../components/WorkEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'work',
                component: Work,
                meta: {
                    title: 'Works'
                },
                children: [
                    {
                        path: 'index',
                        name: 'WorkList',
                        component: WorkList,
                        meta: {
                            title: 'Works',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/work/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/work/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'WorkCreate',
                        component: WorkCreate,
                        meta: {
                            title: 'Create Works',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/work/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'WorkEdit',
                        component: WorkEdit,
                        meta: {
                            title: 'Edit Works',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/work/index'
                        }
                    }
                ]
            }
        ]
    }
]
