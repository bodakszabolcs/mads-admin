<?php

namespace Modules\Work\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Translatable\HasTranslations;
use Modules\User\Entities\User;
		use Modules\WorkCategory\Entities\WorkCategory;


abstract class BaseWork extends BaseModel
{


    protected $table = 'works';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'search',
                'title' => 'Keresés',
                'type' => 'text',
            ],
            [
                'name' => 'user_id',
                'title' => 'Projekt',
                'type' => 'select',
                'data' =>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
            ],
            [
                'name' => 'archive',
                'title' => 'Archive',
                'type' => 'select',
                'data'=>[0=>'Nem',1=>'Igen'],
            ],
            [
                'name' => 'is_full',
                'title' => 'Betelt',
                'type' => 'select',
                'data'=>[0=>'Nem',1=>'Igen'],
            ],
            [
                'name' => 'gross',
                'title' => 'Bér',
                'type' => 'interval',

            ],
        ];

        return parent::getFilters();
    }


    protected $fillable = ['image','name','zip','work_type','user_id','work_category_id','city_id','work_time','gross','gross_type','work_location','work_duration','start','apply','highlighted','is_full','lead','task','company_description','interview_date','preference','condition','comment','project_id','cv_online_kat_1','cv_online_kat_2','jobhunter_category','gross_text'];

    protected $casts = [];

     public function user()
    {
        return $this->hasOne('Modules\User\Entities\User','id','user_id');
    }
    public function city()
    {
        return $this->hasOne('Modules\City\Entities\City','id','city_id');
    }

    public function workcategory()
    {
        return $this->hasOne('Modules\WorkCategory\Entities\WorkCategory','id','work_category_id');
    }
    public function fillAndSave(array $request)
    {

         parent::fillAndSave($request);
         if(empty($this->archive)){
             $this->archive =0;
             $this->save();
         }
         if(!$this->slug){
             $this->slug = Str::slug($this->name.'-'.$this->id);
             $this->save();
         }
    }


}
