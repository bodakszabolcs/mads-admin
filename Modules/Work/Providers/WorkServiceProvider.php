<?php

namespace Modules\Work\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Work\Entities\Work;
use Modules\Work\Observers\WorkObserver;

class WorkServiceProvider extends ModuleServiceProvider
{
    protected $module = 'work';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Work::observe(WorkObserver::class);
    }
}
