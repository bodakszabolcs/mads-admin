<?php

namespace Modules\Work\Observers;

use Modules\Work\Entities\Work;

class WorkObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function saved(Work $model)
    {
        Work::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function created(Work $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function updated(Work $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function deleted(Work $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function restored(Work $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Work\Entities\Work  $model
     * @return void
     */
    public function forceDeleted(Work $model)
    {
        //
    }
}
