import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import WorkLocation from '../components/WorkLocation'
import WorkLocationList from '../components/WorkLocationList'
import WorkLocationCreate from '../components/WorkLocationCreate'
import WorkLocationEdit from '../components/WorkLocationEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'worklocation',
                component: WorkLocation,
                meta: {
                    title: 'WorkLocations'
                },
                children: [
                    {
                        path: 'index',
                        name: 'WorkLocationList',
                        component: WorkLocationList,
                        meta: {
                            title: 'WorkLocations',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/worklocation/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/worklocation/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'WorkLocationCreate',
                        component: WorkLocationCreate,
                        meta: {
                            title: 'Create WorkLocations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/worklocation/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'WorkLocationEdit',
                        component: WorkLocationEdit,
                        meta: {
                            title: 'Edit WorkLocations',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/worklocation/index'
                        }
                    }
                ]
            }
        ]
    }
]
