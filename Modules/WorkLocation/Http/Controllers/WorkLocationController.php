<?php

namespace Modules\WorkLocation\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\WorkLocation\Entities\WorkLocation;
use Illuminate\Http\Request;
use Modules\WorkLocation\Http\Requests\WorkLocationCreateRequest;
use Modules\WorkLocation\Http\Requests\WorkLocationUpdateRequest;
use Modules\WorkLocation\Transformers\WorkLocationViewResource;
use Modules\WorkLocation\Transformers\WorkLocationListResource;

class WorkLocationController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new WorkLocation();
        $this->viewResource = WorkLocationViewResource::class;
        $this->listResource = WorkLocationListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = WorkLocationCreateRequest::class;
        $this->updateRequest = WorkLocationUpdateRequest::class;
    }

}
