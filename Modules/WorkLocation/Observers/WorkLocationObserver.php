<?php

namespace Modules\WorkLocation\Observers;

use Modules\WorkLocation\Entities\WorkLocation;

class WorkLocationObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function saved(WorkLocation $model)
    {
        WorkLocation::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function created(WorkLocation $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function updated(WorkLocation $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function deleted(WorkLocation $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function restored(WorkLocation $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\WorkLocation\Entities\WorkLocation  $model
     * @return void
     */
    public function forceDeleted(WorkLocation $model)
    {
        //
    }
}
