<?php

namespace Modules\WorkLocation\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\WorkLocation\Entities\WorkLocation;
use Modules\WorkLocation\Observers\WorkLocationObserver;

class WorkLocationServiceProvider extends ModuleServiceProvider
{
    protected $module = 'worklocation';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        WorkLocation::observe(WorkLocationObserver::class);
    }
}
