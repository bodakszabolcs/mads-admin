<?php

namespace Modules\ImportantInfo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportantInfoCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Megnevezés'),
'content' => __('Tartalom'),
'file' => __('Dokumentum'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
