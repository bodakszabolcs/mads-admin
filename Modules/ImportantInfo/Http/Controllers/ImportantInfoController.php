<?php

namespace Modules\ImportantInfo\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ImportantInfo\Entities\ImportantInfo;
use Illuminate\Http\Request;
use Modules\ImportantInfo\Http\Requests\ImportantInfoCreateRequest;
use Modules\ImportantInfo\Http\Requests\ImportantInfoUpdateRequest;
use Modules\ImportantInfo\Transformers\ImportantInfoViewResource;
use Modules\ImportantInfo\Transformers\ImportantInfoListResource;

class ImportantInfoController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ImportantInfo();
        $this->viewResource = ImportantInfoViewResource::class;
        $this->listResource = ImportantInfoListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ImportantInfoCreateRequest::class;
        $this->updateRequest = ImportantInfoUpdateRequest::class;
    }

}
