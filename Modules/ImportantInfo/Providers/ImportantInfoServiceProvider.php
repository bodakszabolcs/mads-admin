<?php

namespace Modules\ImportantInfo\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ImportantInfo\Entities\ImportantInfo;
use Modules\ImportantInfo\Observers\ImportantInfoObserver;

class ImportantInfoServiceProvider extends ModuleServiceProvider
{
    protected $module = 'importantinfo';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ImportantInfo::observe(ImportantInfoObserver::class);
    }
}
