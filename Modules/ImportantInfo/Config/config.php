<?php

return [
    'name' => 'Fontos információk',

    'menu_order' => 48,

    'menu' => [
        [

            'icon' => 'flaticon2-shield',

            'title' => 'Fontos információk',

            'route' => '/' . env('ADMIN_URL') . '/important-info/index',

        ]

    ]
];
