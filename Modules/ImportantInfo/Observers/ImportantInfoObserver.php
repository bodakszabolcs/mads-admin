<?php

namespace Modules\ImportantInfo\Observers;

use Modules\ImportantInfo\Entities\ImportantInfo;

class ImportantInfoObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function saved(ImportantInfo $model)
    {
        ImportantInfo::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function created(ImportantInfo $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function updated(ImportantInfo $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function deleted(ImportantInfo $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function restored(ImportantInfo $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ImportantInfo\Entities\ImportantInfo  $model
     * @return void
     */
    public function forceDeleted(ImportantInfo $model)
    {
        //
    }
}
