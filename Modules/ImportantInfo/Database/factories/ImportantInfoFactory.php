<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\ImportantInfo\Entities\ImportantInfo;

$factory->define(ImportantInfo::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"content" => $faker->realText(),
"file" => $faker->realText(),

    ];
});
