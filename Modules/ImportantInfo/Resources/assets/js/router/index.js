import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ImportantInfo from '../components/ImportantInfo'
import ImportantInfoList from '../components/ImportantInfoList'
import ImportantInfoCreate from '../components/ImportantInfoCreate'
import ImportantInfoEdit from '../components/ImportantInfoEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'important-info',
                component: ImportantInfo,
                meta: {
                    title: 'ImportantInfos'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ImportantInfoList',
                        component: ImportantInfoList,
                        meta: {
                            title: 'ImportantInfos',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/important-info/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/important-info/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ImportantInfoCreate',
                        component: ImportantInfoCreate,
                        meta: {
                            title: 'Create ImportantInfos',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/important-info/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ImportantInfoEdit',
                        component: ImportantInfoEdit,
                        meta: {
                            title: 'Edit ImportantInfos',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/important-info/index'
                        }
                    }
                ]
            }
        ]
    }
]
