<?php

namespace Modules\Page\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Page\Entities\Base\BasePage;

class Page extends BasePage
{
    use SoftDeletes, Cachable;
    
}
