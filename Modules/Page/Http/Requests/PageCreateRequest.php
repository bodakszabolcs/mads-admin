<?php

namespace Modules\Page\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique_translation:pages,name,'.$this->id.',id,deleted_at,NULL',
            'slug' => 'unique:pages,slug,'.$this->id.',id,deleted_at,NULL',
        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Name'),
            'slug' => __('URL')
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
