<?php

namespace Modules\Page\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Modules\Page\Entities\Page;

class PageDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $pg = new Page();
        $pg->name = 'HomePage';
        $pg->meta_title = 'HomePage';
        $pg->slug = '';
        $pg->content = 'Home Page content';
        $pg->meta_description = 'Home Page content';
        $pg->component_name = 'HomePage.vue';
        $pg->save();

    }
}
