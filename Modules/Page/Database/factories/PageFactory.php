<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Page\Entities\Page;

$factory->define(Page::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->realText(10),
        'slug' => $faker->unique()->slug,
        'content' => $faker->realText(),
        'meta_title' => $faker->unique()->realText(10),
        'meta_description' => $faker->unique()->realText(100),
        'og_image' => 'https://source.unsplash.com/1600x900/?webdesign,software'.rand(0,9999)
    ];
});
