import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Page from '../components/Page'
import PageList from '../components/PageList'
import PageCreate from '../components/PageCreate'
import PageEdit from '../components/PageEdit'
import ContactForm from '../components/ContactForm'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'page/contact',
                name: 'Contact',
                component: ContactForm,
                meta: {
                    title: 'Contact',
                    subheader: false
                }
            },
            {
                path: 'page',
                component: Page,
                meta: {
                    title: 'Pages'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PageList',
                        component: PageList,
                        meta: {
                            title: 'Pages',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/page/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/page/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PageCreate',
                        component: PageCreate,
                        meta: {
                            title: 'Create Page',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/page/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PageEdit',
                        component: PageEdit,
                        meta: {
                            title: 'Edit Page',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/page/index'
                        }
                    }
                ]
            }
        ]
    }
]
