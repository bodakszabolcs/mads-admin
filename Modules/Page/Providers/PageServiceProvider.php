<?php

namespace Modules\Page\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Page\Console\GenerateSiteMap;
use Modules\Page\Entities\Page;
use Modules\Page\Observers\PageObserver;

class PageServiceProvider extends ModuleServiceProvider
{
    protected $module = 'page';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        $this->commands([
            GenerateSiteMap::class
        ]);

        Page::observe(PageObserver::class);
    }
}
