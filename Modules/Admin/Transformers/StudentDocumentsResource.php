<?php

namespace Modules\Admin\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\Student\Entities\StudentWorkSafety;


class StudentDocumentsResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "student_name" => $this->student_name,
            "student_number" => $this->student_number,
            "phone" => $this->phone,
            "email" => $this->email,
            "student_id" => $this->student_id,
            "project_name" => $this->project_name,
            "user" => $this->user,
            "file" => $this->file,
            "updated_at" => ConstansHelper::formatShortDateTime($this->updated_at),
            "name" => $this->name,
            "signed_date" => ConstansHelper::formatShortDateTime($this->signed_date),
            "mads_signed" => ConstansHelper::formatShortDateTime($this->mads_signed),
        ];
    }
}
