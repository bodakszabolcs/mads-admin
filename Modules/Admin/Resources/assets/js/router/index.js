import Login from './../components/Login'
import Logout from './../components/Logout'
import Admin from './../components/Admin'
import Dashboard from './../components/Dashboard'
import Presents from '../components/company/Presents'
import Schedules from '../components/company/Schedules'
import Data from '../components/company/Data'
import Documents from '../components/company/Documents'
import Certificates from '../components/company/Certificates'
import Workers from '../components/company/Workers'
import RegioStatistic from '../components/RegioStatistic.vue'
import StaffingStatistic from '../components/StaffingStatistic.vue'
import MedicalCertificate from '../components/MedicalCertificate.vue'
import ContractStatistic from '../components/ContractStatistic.vue'
import ExpiringCertificates from '../components/ExpiringCertificates.vue'
import PresenceIndustrySummation from '../components/company/PresenceIndustrySummation.vue'
import ForgotPassword from '../components/ForgotPassword.vue'
import ResetPassword from '../components/ResetPassword.vue'
import Questions from '../components/company/Questions.vue'
import Reuqests from '../components/company/Reuqests.vue'
import Contracts from '../components/company/Contracts.vue'
import MyContractShow from '../components/company/MyContractShow.vue'
import MembershipDocuments from '../components/MembershipDocuments.vue'
import FixingStatistic from '../components/FixingStatistic.vue'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}/login`,
        name: 'Login',
        component: Login,
        meta: {
            title: 'Login Page'
        }
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}/forgot-password`,
        name: 'Forgot password',
        component: ForgotPassword,
        meta: {
            title: 'Forgot password Page'
        }
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}/reset-password`,
        name: 'Reset password',
        component: ResetPassword,
        meta: {
            title: 'Reset password Page'
        }
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}/logout`,
        name: 'Logout',
        component: Logout
    },
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                name: 'Dashboard',
                path: '',
                component: Dashboard,
                meta: {
                    title: 'Dashboard',
                    subheader: true
                }
            },
            {
                name: 'Presents summation ',
                path: 'company/presents-summation',
                component: PresenceIndustrySummation,
                meta: {
                    title: 'Jelenlét összesítő',
                    subheader: false
                }
            },
            {
                name: 'Medical approve',
                path: 'medical-certificate',
                component: MedicalCertificate,
                meta: {
                    title: 'Orvosi igazolások jóváhagyása',
                    subheader: false
                }
            },
            {
                name: 'Questions',
                path: 'my-questions',
                component: Questions,
                meta: {
                    title: 'Árajánlat kérdések',
                    subheader: false
                }
            },
            {
                name: 'Requests',
                path: 'my-requests',
                component: Reuqests,
                meta: {
                    title: 'Árajánlatok',
                    subheader: false
                }
            },
            {
                name: 'Contracts',
                path: 'my-contracts',
                component: Contracts,
                meta: {
                    title: 'Szerződések',
                    subheader: false
                }
            },
            {
                name: 'Contract edit',
                path: 'my-contracts/:id',
                component: MyContractShow,
                meta: {
                    title: 'Szerződés szerkesztése',
                    subheader: false
                }
            },
            {
                name: 'Medical approvee ',
                path: 'certificates/index',
                component: ExpiringCertificates,
                meta: {
                    title: 'Lejáró igazolások',
                    subheader: false
                }
            },
            {
                name: 'Online membership ',
                path: 'adhock-membership/index',
                component: MembershipDocuments,
                meta: {
                    title: 'Online eseti, tagsági',
                    subheader: false
                }
            },

            {
                name: 'Region Statistic',
                path: 'region/statistic',
                component: RegioStatistic,
                meta: {
                    title: 'Régió statistic',
                    subheader: false
                }
            },
            {
                name: 'Contract Statistic',
                path: 'contract/statistic',
                component: ContractStatistic,
                meta: {
                    title: 'Szerződés statisztika',
                    subheader: false
                }
            },
            {
                name: 'Fixing Statistic',
                path: 'fixing/statistic',
                component: FixingStatistic,
                meta: {
                    title: 'Bevétel statisztika',
                    subheader: false
                }
            },
            {
                name: 'Staffing requirement',
                path: 'staffing/statistic',
                component: StaffingStatistic,
                meta: {
                    title: 'Létszám igények',
                    subheader: false
                }
            },
            {
                name: 'Persents',
                path: 'company/presents',
                component: Presents,
                meta: {
                    title: 'Jelenléti ívek',
                    subheader: true
                }
            },
            {
                name: 'Schedules',
                path: 'company/schedules',
                component: Schedules,
                meta: {
                    title: 'Beosztások',
                    subheader: true
                }
            },
            {
                name: 'Data',
                path: 'company/data',
                component: Data,
                meta: {
                    title: 'Cég adatok',
                    subheader: true
                }
            },
            {
                name: 'Workres',
                path: 'company/workers-list',
                component: Workers,
                meta: {
                    title: 'Dolgozó diákok- a legutolsó lezárt hónapban',
                    subheader: true
                }
            },
            {
                name: 'Documents',
                path: 'company/documents',
                component: Documents,
                meta: {
                    title: 'Dokumentumok',
                    subheader: true
                }
            },
            {
                name: 'Certificates',
                path: 'company/certificates',
                component: Certificates,
                meta: {
                    title: 'Teljesítési igazolások',
                    subheader: true
                }
            }
        ]
    }
]
