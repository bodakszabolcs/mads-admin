<?php

namespace Modules\Admin\Http\Controllers;

use App\Exports\BaseArrayExport;
use App\Helpers\ConstansHelper;
use App\Helpers\PdfHelper;
use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\CompanyQuestion;
use App\Jobs\CompanyRequest;
use App\Jobs\SendEmailWithLink;
use Caxy\HtmlDiff\HtmlDiff;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Campaig\Entities\CampaignCompany;
use Modules\Campaig\Entities\CampaignCompanyContract;
use Modules\Campaig\Entities\CampaignCompanyQuestion;
use Modules\Campaig\Entities\CampaignCompanyRequest;
use Modules\Campaig\Transformers\CampaignRequestListResource;
use Modules\Campaig\Transformers\CampaigQuestionListResource;
use Modules\Campaig\Transformers\ContractListResource;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Contact;
use Modules\Company\Entities\Document;
use Modules\Company\Entities\Industry;
use Modules\Company\Entities\Site;
use Modules\Company\Transformers\CompanyViewResource;
use Modules\Company\Transformers\ContactListResource;
use Modules\Company\Transformers\DocumentListResource;
use Modules\Company\Transformers\DocumentViewResource;
use Modules\Company\Transformers\IndustryViewResource;
use Modules\Feor\Entities\Feor;
use Modules\Fixing\Entities\Fixing;
use Modules\Fixing\Transformers\FixingPresensStudentResource;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ProjectSchedule;
use Modules\Project\Entities\ProjectStudent;
use Modules\Project\Transformers\ProjectStudentResource;
use Modules\Student\Entities\Student;
use Modules\Student\Http\Requests\DocumentCreateRequest;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\User\Entities\User;
use Modules\WorkCategory\Entities\WorkCategory;


class AdminCompanyController extends AbstractLiquidController
{
    private $headings = [
        'Diák', 'E-mail', 'Adószám', 'Cég', 'Tevékenyslég/Telephely', 'Dátum', 'Kezdés', 'Végzés', 'Óraszám', 'Fix/Bónusz'
    ];
    public function getMyCampaign(Request $request){
       // $myCampagn = CampaignCompany::where('company_id',\Auth::user()->company_id)->leftJoin('campaign_status','campaign_status.id','=','status_id')
        $myCampagn = CampaignCompany::where('company_id',\Auth::user()->company_id)->leftJoin('campaign_status','campaign_status.id','=','status_id')
            ->select(DB::raw('campaign_companies.*,campaign_status.name as status_name'))->orderBy('id','desc')->whereNotIn('status_id',[1,11,12])->first();
        return response()->json(['data'=>[
            'status'=>optional($myCampagn)->status_name,
            'id'=>optional($myCampagn)->id,
            'status_id' => optional($myCampagn)->status_id,
        ]]);
    }
    public function getMyQuestions(Request $request){
       // return CampaigQuestionListResource::collection(CampaignCompanyQuestion::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
        return CampaigQuestionListResource::collection(CampaignCompanyQuestion::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
    }
    public function saveAnswer(Request $request){
        $ans = CampaignCompanyQuestion::where('id',$request->input('id'))->first();
        $ans->fill($request->all());
        $ans->answered = 1;
        $ans->save();
        $v = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id',\Auth::user()->company_id)->first();
        $v->status_id = 6;
        $v->save();
        CompanyQuestion::dispatch($ans);
        return $this->getMyQuestions($request);
    }
    public function getMyRequest(Request $request){
        // return CampaigQuestionListResource::collection(CampaignCompanyQuestion::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
        return CampaignRequestListResource::collection(CampaignCompanyRequest::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
    }
    public function approveRequest(Request $request){
        $ans = CampaignCompanyRequest::where('id',$request->input('id'))->first();
        $ans->request= $request->input('request');
        $ans->approved = 1;
        $ans->save();
        $v = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id',\Auth::user()->company_id)->first();
        $v->status_id = 8;
        $v->save();
        CompanyRequest::dispatch($ans,"");
        return $this->getMyRequest($request);
    }
    public function declineRequest(Request $request){
        $ans = CampaignCompanyRequest::where('id',$request->input('id'))->first();
        $ans->approved = 2;
        $ans->declined=$request->input('declined');
        $ans->save();
        $v = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id',\Auth::user()->company_id)->first();
        //todo email küldés árajánlat elfogadásáról
        $rs = Company::where('id',\Auth::user()->company_id)->first();
        $v->status_id =11;
        $v->save();
        $user = User::where('id',$v->user_id)->first();
        SendEmailWithLink::dispatch($user->mads_email,'MADS- Árajánlat elutasítva!',$user->firstname. ' '.$user->lastname,"<p>A(z) ".$rs->name. "cég elutasította az árajánlatot a következő indokkal:<br/>".$request->input('declined')."</p>",'/admin/request/edit/'.$ans->id,'Ugrás az ajánlathoz',[]);
       // SendEmailWithLink::dispatch("bodak.szabolcs@gmail.com",'MADS- Árajánlat elutasítva!',$user->firstname. ' '.$user->lastname,"<p>A(z) ".$rs->name. "cég elutasította az árajánlatot a következő indokkal:<br/>".$request->input('declined')."</p>",'/admin/request/edit/'.$ans->id,'Ugrás az ajánlathoz',[]);
        return $this->getMyRequest($request);
    }
    public function callMeBack(Request $request){
        $ans = CampaignCompanyRequest::where('id',$request->input('id'))->first();
        $v = CampaignCompany::where('campaign_id',$request->input('campaign_id'))->where('company_id',\Auth::user()->company_id)->first();
        $user = User::where('id',$v->user_id)->first();
        $rs = Company::where('id',\Auth::user()->company_id)->first();
        SendEmailWithLink::dispatch($user->mads_email,'MADS- Visszahavást kér!',$user->firstname. ' '.$user->lastname,"<p>A(z) ".$rs->name. " kérdésket szeretne feltenni. Kérlek mihamarabb vedd fel velük a kapcsolatot:</p>",'/admin/request/edit/'.$ans->id,'Ugrás az ajánlathoz',[]);
        //SendEmailWithLink::dispatch("bodak.szabolcs@gmail.com",'MADS- Visszahavást kér!',$user->firstname. ' '.$user->lastname,"<p>A(z) ".$rs->name. " kérdésket szeretne feltenni. Kérlek mihamarabb vedd fel velük a kapcsolatot:</p>",'/admin/request/edit/'.$ans->id,'Ugrás az ajánlathoz',[]);
        return $this->getMyRequest($request);
    }
    public function getMyContract(Request $request){
        // return CampaigQuestionListResource::collection(CampaignCompanyQuestion::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
        return ContractListResource::collection(CampaignCompanyContract::where('company_id',\Auth::user()->company_id)->orderBy('id','desc')->get());
    }
    public function contractShowFirst(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteFirstMADSPDF();
    }
    public function contractShowSecond(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteSecondMADSPDF();
    }
    public function contractShowContract(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        return $contract->genereteContractMADSPDF();
    }
    public function saveAsDraft(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->company_draft =  $request->input('actual_contract');
        $contract->save();
        return $this->contractShow($request,$id);
    }
    public function approveContract(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->company_updated =  date('Y-m-d H:i:s');
        $contract->company_approved =  true;
        $contract->company_draft =  null;
        $contract->save();

        SendEmailWithLink::dispatch("virag.viktor@mads.hu", 'MADS-' . $contract->data['name'] . '- szerződést a mads módosította!', "Virág Viktor", "<p>A " . $contract->data['number'] . " sorszámú szerződést az Ügyfél elfogadta:<br/>A részletekért kérem látogasson el az alábbi linkre, ahol megtekintheti a szerződésben módosított pontokat</p>", '/admin/campaign/contract-list/show/'. $contract->id, 'Ugrás a szerződéshez', []);
        return $this->contractShow($request,$id);
    }
    public function revertLastVersion(Request  $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->company_draft = null;
        $contract->save();

        return $this->contractShow($request,$id);
    }
    public function contractShow(Request $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $htmlDiff = new HtmlDiff($contract->previous_contract, ($contract->company_draft)?$contract->company_draft:$contract->actual_contract);
        return response()->json([
            'data'=>[
                'id'=>$id,
                'previous_contract'=>$contract->previous_contract,
                'actual_contract'=>($contract->company_draft)?$contract->company_draft:$contract->actual_contract,
                'diff'=> $htmlDiff->build(),
                'mads_approved'=>ConstansHelper::bool($contract->mads_approved),
                'company_approved'=>ConstansHelper::bool($contract->company_approved),
                'mads_updated'=> ConstansHelper::formatShortDateTime($contract->mads_updated),
                'company_updated'=> ConstansHelper::formatShortDateTime($contract->company_updated),

            ]
        ]);
    }
    public function saveAndApproveContract(Request $request,$id){
        $contract = CampaignCompanyContract::where('id',$id)->first();
        $contract->previous_contract = $contract->actual_contract;
        $contract->actual_contract = $request->input('actual_contract');
        $contract->company_approved = true;
        $contract->mads_approved = false;
        $contract->company_draft = null;
        $contract->company_updated = date('Y-m-d H:i:s');
        $contract->save();
        SendEmailWithLink::dispatch("virag.viktor@mads.hu", 'MADS-' . $contract->data['name'] . '- szerződést az ügyfél módosította!', "Virág Viktor", "<p>A " . $contract->data['number'] . " sorszámú szerződést a MADS módosította:<br/>A részletekért kérem látogasson el az alábbi linkre, ahol megtekintheti a szerződésben módosított pontokat</p>", '/admin/campaign/contract-list/show/' . $contract->id, 'Ugrás a szerződéshez', []);
        return $this->contractShow($request,$id);
    }
    public function getPresents(Request $request, $month)
    {
        if (!$month) {
            $month = date('Y-m');
        }
        $start = date('Y-m-d', strtotime($month . '-01'));
        $end = date('Y-m-t', strtotime($month . '-01'));
        $industries = Industry::whereIn('id', \Auth::user()->industries)->get();
        $response = [];
        foreach ($industries as $ind) {
            if(Auth::user()->students){
             $sIds   = ProjectStudent::where('industry_id', $ind->id)->leftJoin('projects', 'projects.id', '=', 'project_id')->where('status_id', 2)->disableCache()
                 ->leftJoin('students','students.id','=','student_id')->whereIn('students.student_number',explode(",",Auth::user()->students))
                 ->groupBy('student_id')->get()->pluck('student_id', 'student_id');
            }else {
                $sIds = ProjectStudent::where('industry_id', $ind->id)->leftJoin('projects', 'projects.id', '=', 'project_id')->where('status_id', 2)->groupBy('student_id')->get()->pluck('student_id', 'student_id');
            }
            if (!$sIds) {
                continue;
            }
            $students = Student::whereIn('id', $sIds)->get();

            if (!$students) {
                continue;
            }

            $response[$ind->id] = [
                'name' => $ind->name,
                'industry_id' => $ind->id,
                'industry_has_break' => $ind->break,
                'students' => []
            ];
            foreach ($students as $s) {
                $fixings= DB::table('fixings')->where('industry_id','=',$ind->id)->where('student_id', $s->id)->where('month',date('Ym',strtotime($month.'-01')))
                    ->select(DB::raw("sum(student_price) as sum_price"))->first();
                $response[$ind->id]['students'][$s->id] = [
                    'name' => $s->name,
                    'student_id' => $s->id,
                    'industry_id' => $ind->id,
                    'company_id' => $ind->company->id,
                    'email' => $s->email,
                    'phone' => $s->phone,
                    'price_hourly'=>$ind->price_hour,
                    'mads_all'=>$fixings->sum_price,
                    'all_approved' => StudentPresent::where('student_id', $s->id)->where('industry_id', $ind->id)->where('date', '>=', $start)->whereNull('approved')->where('date', '<=', $end)->count(),
                    'presents' => $this->generatePresents($ind->id, $s->id, $month)
                ];
            }
        }

        return response()->json($response, 200);

    }
    public function listWorker(Request $request)
    {
        $date= date('Ym',strtotime('-1 month'));
        if(date('d')<10){
            $date= date('Ym',strtotime('-2 month'));
        }

        $list = DB::table("fixing_view")->where("month",'=',$date)->groupBy('fixing_view.student_id')->where('company_id','=',Auth::user()->company_id)->leftJoin('students','students.id', 'fixing_view.student_id')->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates  where deleted_at is null group by student_id ) as cert'), 'cert.student_id', '=', 'fixing_view.student_id')
            ->select(DB::raw("students.name as name, students.student_number as student_number, students.phone as phone,students.email as email,GROUP_CONCAT(DISTINCT teaor SEPARATOR ', ') as teaor, ceiling(sum(hour+bonus_hour)) as hour, cert.end as certificate"));
        if(Auth::user()->industries && sizeof(Auth::user()->industries) >0 ){
            $list= $list->whereIn("fixing_view.industry_id",Auth::user()->industries);
        }

        return response()->json(["data"=>$list->get()]);
    }
    public function getPresentsPdf(Request $request,$month,$industry){
        //$month = date('Y').'-'.$month;
        $student = Student::where('id',$request->user()?$request->user()->id:\Auth::guard('student')->id())->first();
        $industry = Industry::where('id',$industry)->first();
        $company = Company::where('id',$industry->company_id)->first();
        $project= Project::where('industry_id',$industry->id)->first();
        $projectManager = User::where('id',$project->user_id)->first();
        $sIds = ProjectStudent::where('industry_id', $industry->id)->disableCache()->leftJoin('projects', 'projects.id', '=', 'project_id')->where('status_id', 2)->groupBy('student_id')->get()->pluck('student_id', 'student_id');


        $students = Student::whereIn('id', $sIds)->get();
        $date = date('Y', strtotime($month . '-01')) . '. ' . Arr::get(ConstansHelper::$moths, (int)explode('-', $month)[1]);
        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        foreach ($students as $s) {
           $presents = $this->generatePresents($industry->id, $s->id, $month);
           PdfHelper::createPdf(view('student.jelenleti', ['student' => $s, 'projectManager' => $projectManager, 'month' => $date, 'presents' => $presents, 'task' => $industry->name, 'project' => $company->name]),
               null, null, '/tmp/' .$s->id . 'package.pdf',
               ['margin_top' => 10, 'margin_bottom' => 10]);


            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $s->id . 'package.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $s->id . 'package.pdf');
        }
        return $pdf->Output();

    }

    public function approvePresents(Request $request)
    {
        $presents = $request->input('presents', []);
        $month = null;

        DB::beginTransaction();

        foreach ($presents as $p) {

            $av = StudentPresent::where('company_id', $request->input('company_id'))->where('industry_id', $request->input('industry_id'))->where('student_id', $request->input('student_id'))
                ->where('date', '=', $p['date'])->first();
            if ($av && (!$p['start'] || !$p['end'])) {
                $av->forceDelete();
            }
            if (!$av) {
                $av = new StudentPresent();
            }
            $month = date('Y-m', strtotime($p['date']));
            if ($p['start'] && $p['end']) {
                $av->fill($p);
                if (!$av->approved) {
                    $av->approved = date('Y-m-d H:i:s');
                    $av->appruved_user_id = Auth::id();

                }
                $av->company_id = $request->input('company_id');
                $av->industry_id = $request->input('industry_id');
                $av->student_id = $request->input('student_id');

                $av->hour = $p['hour'];
                $av->save();
            }
            $temp = $av;

        }
        DB::commit();

        return response()->json(
            $this->generatePresents($request->input('industry_id'), $request->input('student_id'), $month)
            , 200);
    }

    public function generatePresents($industry_id, $student_id, $month)
    {
        $start = date('Y-m-d', strtotime($month . '-01'));
        $end = date('Y-m-t', strtotime($month . '-01'));
        $days = [];
        $presents = StudentPresent::where('industry_id', $industry_id)->where('student_id', $student_id)->where('date', '>=', $start)->where('date', '<=', $end)->get();
        while ($start <= $end) {
            $days[$start] = [
                'name' => ConstansHelper::formatDateShort($start,\App::getLocale()),
                'date' => $start,
                'start' => '',
                'weekend' => (date('N', strtotime($start)) > 5) ? true : false,
                'end' => '',
                'approved' => '',
                'updated' => '',
                'hour' => '',
                'break_from' => '',
                'break_to' => '',
            ];
            $start = date('Y-m-d', strtotime($start . ' +1 day'));
        }
        foreach ($presents as $pres) {
            $days[$pres->date]['start'] = $pres->start;
            $days[$pres->date]['end'] = $pres->end;
            $days[$pres->date]['break_from'] = $pres->break_from;
            $days[$pres->date]['break_to'] = $pres->break_to;
            $days[$pres->date]['end'] = $pres->end;
            $days[$pres->date]['updated'] = $pres->updated_at;
            $days[$pres->date]['approved'] = $pres->approved;
            $days[$pres->date]['hour'] = $pres->hour;
        }

        return $days;
    }

    public function documents(Request $request)
    {
        $model = new Document();
        $list = $model->searchInModel($request->input())->where('company_id','=',Auth::user()->company_id);
        $list = $list->get();

        return DocumentListResource::collection($list);
    }
    public function documentEdit(DocumentCreateRequest $request){
        $model = new Document();
        $model->fill($request->all());
        $model->company_id = Auth::user()->company_id;
        $model->save();
        return new  DocumentViewResource($model);

    }
    public function getPresentsSummation(Request $request, $month){

        DB::enableQueryLog();
        $data['has_presence'] =
            StudentPresent::where('date', 'LIKE', '%' . $month . '%')->leftJoin('students', 'students.id', '=', 'student_presents.student_id')->whereNull('students.deleted_at')
            ->groupBy('student_id')->orderBy('students.name')->leftJoin('company_industries','company_industries.id','=','industry_id')
                ->leftJoin('users','users.id','=','appruved_user_id')
                ->where("student_presents.company_id",Auth::user()->company_id)
                ->leftJoin(DB::raw(Student::$studentCertificateQuery),'sCard.student_id','=','students.id')
                ->select(DB::raw("concat(users.lastname,' ',users.firstname) as appruved_user,(company_industries.price_hour+company_industries.price_company_hourly) as price_hourly,(company_industries.price_hour+company_industries.price_company_hourly)*sum(hour) as price, students.name as name,students.id as student_id,tax_number,company_industries.name as industry_name, expire, sum(hour) as sum_hour,students.student_number as student_number,industry_id,count(*) as count,sum(finalized) as finalized, sum(if(ISNULL(student_presents.approved), 1,0)) as appr "))->get();
        $ids=[];
        //dd(DB::getQueryLog());
        foreach ($data['has_presence'] as $pr){
            $ids[$pr["student_id"]] =$pr["student_id"];
        }
        $data['has_presence'] = FixingPresensStudentResource::collection($data['has_presence']);
        $data['not_has_presence'] =ProjectStudent::where("status_id","2")
            ->leftJoin('students', 'students.id', '=', 'student_id')->whereNotIn("students.id",$ids)
            ->leftJoin("projects","projects.id","=","project_id")->whereNull('students.deleted_at')->where("projects.company_id",Auth::user()->company_id)
            ->leftJoin('company_industries','company_industries.id','=','projects.industry_id')
            ->leftJoin(DB::raw(Student::$studentCertificateQuery),'sCard.student_id','=','students.id')
            ->select(DB::raw("students.name as name,students.id as student_id,tax_number,students.student_number,company_industries.name as industry_name, expire"))->get();
        return response()->json(["data"=>$data]);
    }
    public function completionForm(Request $request,$type,$month){
        $company = Company::where('id',Auth::user()->company_id)->first();

        return PdfHelper::createPdf(view('company.teljesitesi',['company'=>$company,'month'=>$month,'type'=>$type]));
    }
    public function completionFormExcel(Request $request, $type, $month)
    {
        $company = Company::where('id',Auth::user()->company_id)->first();
        $headers=[];
        $list = DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->groupBy('company_id')
            ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,

              sum(saturday_hour) as saturday_hour,
              sum(sunday_hour) as sunday_hour,
              sum(festive_hour) as festive_hour,
              sum(fix_company) as sum_fix,


              sum(hour) as sum_hour,
              CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
              CEILING(SUM(sum_net)) as sum_net,
              CEILING(SUM(sum_net)*1.27) as sum_gross,

              sum(night_hour) as night_hour,
              CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
              CEILING(Sum( night_sum_net))as night_sum_net,
              CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,

              sum(festive_hour) as festive_hour,
              CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
              CEILING(Sum( festive_sum_net))as festive_sum_net,
              CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,

              sum(sunday_hour) as sunday_hour,
              CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
              CEILING(Sum( sunday_sum_net))as sunday_sum_net,
              CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,

              sum(saturday_hour) as saturday_hour,
             CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
              CEILING(Sum( saturday_sum_net))as saturday_sum_net,
               CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
              payment_deadline,
              invoice_status,
                student_name,
	         teaor,
              CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross"))->orderBy('company_name', 'asc')->where('company_id',$company->id);
        if($type=='student'){
            $list =$list->groupBy('student_id')->groupBy(DB::raw('LOwER(teaor)'))->get();
            $headers =['Név','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->student_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];

        }
        if($type=='industry'){
            $list =$list->groupBy(DB::raw('LOWER(teaor)'))->get();
            $headers =['Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        if($type=='site'){
            $list =$list->groupBy('site_id')->groupBy(DB::raw('LOWER(teaor)'))->get();
            $headers =['Telephely','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->site_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        if($type=='uuid'){
            $list =$list->groupBy('industry_id')->get();
            $headers =['Megnevezés','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->industry_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        return Excel::download(new BaseArrayExport($headers,'MADS-teljesitesi-'.date('Y-m-d'),$data),'MADS-teljesitesi-'.date('Y-m-d').'.xlsx');
    }
    public function fixingSummationList(Request $request, $month)
    {
        $group = "industry";
        $company = Company::where('id',Auth::user()->company_id)->first();
        $list = Fixing::where('month', '=', str_replace('-', '', $month))->groupBy('fixings.company_id')->disableCache()->leftJoin('companies', 'companies.id', '=', 'fixings.company_id')->leftJoin('company_industries', 'company_industries.id', '=', 'fixings.industry_id')->leftJoin('company_sites', 'company_sites.id', '=', 'company_industries.site_id')
            ->select(DB::raw("companies.name as company_name,company_industries.name as  industry_name,fixings.company_id as company_id, fixings.industry_id as industry_id,company_sites.name as site_name,company_sites.id as site_id,
	         sum(IF(bonus_type=1,bonus_hour,0)) as night_hour,
	         sum(IF(bonus_type=2,bonus_hour,0)) as saturday_hour,
	         sum(IF(bonus_type=3,bonus_hour,0)) as sunday_hour,
	         sum(IF(bonus_type=4,bonus_hour,0)) as festive_hour,
	         sum(IF(bonus_type=1,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as night_hour_price,
	         sum(IF(bonus_type=2,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as saturday_hour_price,
	         sum(IF(bonus_type=3,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as sunday_hour_price,
	         sum(IF(bonus_type=4,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as festive_hour_price,
	         sum(bonus_hour) as sum_bonus_hour,
	         sum(hour+bonus_hour) as sum_hour,
	         payment_deadline,
	         sum(fix) as sum_fix,
	         AVG(fixings.price_hour) as avg_price_hour,
	         AVG(fixings.price_company_hourly) as avg_price_company,
	         sum(student_price) as  sum_price,
	         sum(IF(invoice_status is null || invoice_status=0,1,0)) as  invoice_status,
	         sum(company_price) as sum_company"))->orderBy('companies.name', 'asc')->where('fixings.company_id',Auth::user()->company_id);
        if ($request->input('group', 'industry') != 'industry' && $request->input('group', '') != null) {
            $list = $list->groupBy('site_id');
            $group = "site";
        } else {
            $list = $list->groupBy('industry_id');
        }

        $list = $list->get();

        $response = [];
        $invoice_status =0;
        foreach ($list as $item) {
            if (!isset($response[$item->company_id])) {
                $response[$item->company_id] = [
                    "company_name" => $item->company_name, "payment_deadline" => $item->payment_deadline, "company_id" => $item->company_id, "invoice_status" => $item->invoice_status, "avg_price_hour" => $item->avg_price_hour, "sum_hour" => 0, "night_hour" => 0, "saturday_hour" => 0, "sunday_hour" => 0, "festive_hour" => 0, "sum_student" => 0, "sum_company" => 0, "sum_fix" => 0,
                    "night_hour_price" => 0,"avg_price_company"=>$item->avg_price_company, "saturday_hour_price" => 0, "sunday_hour_price" => 0, "festive_hour_price" => 0, "items" => []
                ];

            }
            $invoice_status = $item->invoice_status;
            if ($group == "industry") {
                if (!isset($response[$item->company_id]['items'][$item->industry_id])) {
                    $response[$item->company_id]['items'][$item->industry_id] = [
                        "name" => $item->industry_name, "id" => $item->industry_id, "invoice_status" => $item->invoice_status, "avg_price_hour" => $item->avg_price_hour, "sum_hour" => 0, "night_hour" => 0, "saturday_hour" => 0, "sunday_hour" => 0, "festive_hour" => 0, "sum_student" => 0, "sum_company" => 0, "sum_fix" => 0, "night_hour_price" => 0, "saturday_hour_price" => 0,
                        "sunday_hour_price" => 0,"avg_price_company"=>$item->avg_price_company, "festive_hour_price" => 0,
                    ];
                }
            } else {
                if (!isset($response[$item->company_id]['items'][$item->site_id])) {
                    $response[$item->company_id]['items'][$item->site_id] = [
                        "name" => $item->site_name,"avg_price_company"=>$item->avg_price_company, "id" => $item->site_id, "invoice_status" => $item->invoice_status, "avg_price_hour" => $item->avg_price_hour, "sum_hour" => 0, "night_hour" => 0, "saturday_hour" => 0, "sunday_hour" => 0, "festive_hour" => 0, "sum_student" => 0, "sum_company" => 0, "sum_fix" => 0, "night_hour_price" => 0, "saturday_hour_price" => 0, "sunday_hour_price" => 0,
                        "festive_hour_price" => 0,
                    ];
                }
            }
            $response[$item->company_id]["sum_hour"] += $item->sum_hour;
            $response[$item->company_id]["sum_student"] += $item->sum_price;
            $response[$item->company_id]["sum_company"] += $item->sum_company;
            $response[$item->company_id]["night_hour"] += $item->night_hour;
            $response[$item->company_id]["saturday_hour"] += $item->saturday_hour;
            $response[$item->company_id]["sunday_hour"] += $item->sunday_hour;
            $response[$item->company_id]["festive_hour"] += $item->festive_hour;
            $response[$item->company_id]["sum_fix"] += $item->sum_fix;
            $response[$item->company_id]["night_hour_price"] += $item->night_hour_price;
            $response[$item->company_id]["saturday_hour_price"] += $item->saturday_hour_price;
            $response[$item->company_id]["sunday_hour_price"] += $item->sunday_hour_price;
            $response[$item->company_id]["festive_hour_price"] += $item->festive_hour_price;
            if ($group == 'industry') {
                $response[$item->company_id]['items'][$item->industry_id]["sum_hour"] += $item->sum_hour;
                $response[$item->company_id]['items'][$item->industry_id]["sum_student"] += $item->sum_price;
                $response[$item->company_id]['items'][$item->industry_id]["sum_company"] += $item->sum_company;
                $response[$item->company_id]['items'][$item->industry_id]["night_hour"] += $item->night_hour;
                $response[$item->company_id]['items'][$item->industry_id]["saturday_hour"] += $item->saturday_hour;
                $response[$item->company_id]['items'][$item->industry_id]["sunday_hour"] += $item->sunday_hour;
                $response[$item->company_id]['items'][$item->industry_id]["festive_hour"] += $item->festive_hour;
                $response[$item->company_id]['items'][$item->industry_id]["sum_fix"] += $item->sum_fix;
                $response[$item->company_id]['items'][$item->industry_id]["night_hour_price"] += $item->night_hour_price;
                $response[$item->company_id]['items'][$item->industry_id]["saturday_hour_price"] += $item->saturday_hour_price;
                $response[$item->company_id]['items'][$item->industry_id]["sunday_hour_price"] += $item->sunday_hour_price;
                $response[$item->company_id]['items'][$item->industry_id]["festive_hour_price"] += $item->festive_hour_price;
            } else {
                $response[$item->company_id]['items'][$item->site_id]["sum_hour"] += $item->sum_hour;
                $response[$item->company_id]['items'][$item->site_id]["sum_student"] += $item->sum_price;
                $response[$item->company_id]['items'][$item->site_id]["sum_company"] += $item->sum_company;
                $response[$item->company_id]['items'][$item->site_id]["night_hour"] += $item->night_hour;
                $response[$item->company_id]['items'][$item->site_id]["saturday_hour"] += $item->saturday_hour;
                $response[$item->company_id]['items'][$item->site_id]["sunday_hour"] += $item->sunday_hour;
                $response[$item->company_id]['items'][$item->site_id]["festive_hour"] += $item->festive_hour;
                $response[$item->company_id]['items'][$item->site_id]["sum_fix"] += $item->sum_fix;
                $response[$item->company_id]['items'][$item->site_id]["night_hour_price"] += $item->night_hour_price;
                $response[$item->company_id]['items'][$item->site_id]["saturday_hour_price"] += $item->saturday_hour_price;
                $response[$item->company_id]['items'][$item->site_id]["sunday_hour_price"] += $item->sunday_hour_price;
                $response[$item->company_id]['items'][$item->site_id]["festive_hour_price"] += $item->festive_hour_price;
            }

        }

        $response['completition'] = !$invoice_status?$company->completition_type:'';
        return response()->json($response, 200);
    }
    public function schedules(Request $request)
    {
    }

    public function excel(Request $request, $month, $industry)
    {
        $group = "industry";
        $list = Fixing::where('month', '=', str_replace('-', '', $month))->disableCache()->where('fixings.company_id', '=', Auth::user()->company_id)->leftJoin('companies', 'companies.id', '=', 'fixings.company_id')->leftJoin('company_industries', 'company_industries.id', '=', 'fixings.industry_id')->leftJoin('company_sites', 'company_sites.id', '=', 'company_industries.site_id')->leftJoin('students', 'students.id', 'student_id')->select(DB::raw('students.name as student_name,tax_number, email,date,start,end, fix,hour+bonus_hour as  hour ,companies.name as company_name,company_industries.name as industry_name,company_sites.name as site_name'));
        if ($request->input('group', 'industry') != 'industry' && $request->input('group', '') != null) {
            $group = "site";
            $list = $list->where('site_id', '=', $industry);
        } else {
            $list = $list->where('industry_id', '=', $industry);
        }
        $excel = [];
        $title = "";
        foreach ($list->orderBy('student_name')->get() as $row) {
            $title = $row->company_name;
            $excel[] = [
                $row->student_name, $row->email, $row->tax_number, $row->company_name, ($group == 'industry') ? $row->industry_name : $row->site_name, $row->date, $row->start, $row->end, $row->hour, $row->fix
            ];
        }
        $export = new BaseArrayExport($this->headings, "Óraszám összesítő", $excel);

        return $export->download(Str::slug($title . '-' . $month . '-Óraszám összesítő') . '.xlsx');

    }

    public function data(Request $request)
    {

        try {

            $company = Company::where('id', Auth::user()->company_id)->firstOrFail();
            return new CompanyViewResource($company);
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }

    public function getFixings(Request $request)
    {
        try {
            $fixings = Fixing::where('company_id', '=', Auth::user()->company_id)
                ->select(DB::raw("SUM(hour+bonus_hour) as sum_hour, month"))->groupBy('month')
                ->where('month', '>', date('Ym', strtotime('-5 year')))->orderBy('month', 'desc')->get();
            $fixingsStudent = Fixing::where('company_id', '=', Auth::user()->company_id)
                ->select(DB::raw('sum(hour+bonus_hour) as sum_hour,student_id,students.name as student_name'))->disableCache()
                ->leftJoin('students', 'students.id', '=', 'student_id')->groupBy('student_id')->orderBy('students.name')->get();
            $dateTable = [];
            foreach ($fixings as $fixing) {
                $year = substr($fixing->month, 0, 4);
                $month = substr($fixing->month, 4);
                if (!isset($dateTable[$year])) {
                    foreach (ConstansHelper::$moths as $k => $v)
                        $dateTable[$year][str_pad($k, 2, '0', STR_PAD_LEFT)] = [
                            'month' => $v,
                            'hours' => ''
                        ];
                }
                $dateTable[$year][$month]['hours'] = $fixing->sum_hour . ' óra';
            }
            return response()->json(['fixing' => $dateTable, 'students' => $fixingsStudent->toArray()]);
        } catch (\Exception $e) {
            abort(404);
        }
    }
    public function getIndustries(Request $request){
        $model = new Industry();
        $list = $model->searchInModel($request->input())->where('company_id','=',Auth::user()->company_id);
        $list = $list->get();

        return IndustryViewResource::collection($list)->additional(['selectables'=> [
            "users" =>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
            "site" => Site::where('company_id',Auth::user()->company_id)->pluck("name","id"),
            "feor" => Feor::select(DB::raw("concat(code,'-',name) as name, id"))->get()->pluck("name","id"),
            "work_category"=> WorkCategory::leftJoin('work_categories as w2','w2.id','=','work_categories.parent_id')->disableCache()
                ->select(DB::raw("work_categories.id as id, Concat_WS('/',w2.name,work_categories.name) as name"))->where('work_categories.parent_id','>',0)->pluck("name","id"),
        ]]);
    }
    public function newStudent(Request $request){

        $project =Project::where('industry_id',$request->industry_id)->first();
        $pm =User::where('id',$project->user_id)->first();
        $industry =Industry::where('id',$request->industry_id)->first();
        $company =Company::where('id',$industry->company_id)->first();
        $user = Auth::user();
        $email = new \Modules\Email\Entities\Email();
        $email->user_id = Auth::user()->id;
        $email->subject = 'MADS - új diák kérés cég részéről';
        $email->recipient_name = $pm->firstname.' '.$pm->lastname;
        //$email->recipient_email ='bodak.szabolcs@gmail.com';
        $email->recipient_email = $pm->email;

        $email->content = "$user->name<br>".$request->input('new_student')." új diákot kér ".$request->input('from')." határidővel<br>"."Cég: $company->name<br> Tevékenység: $industry->name";
        $from = [];
        $from['name']=$user->name;
        $from['email']=$user->email;
        $send = Mail::to($email->recipient_email)->send(new \App\Mail\SendCompanyEmail($email,$from));

    }
    public function getContacts(Request  $request){
        $model = new Contact();
        $list = $model->searchInModel($request->input())->where('company_id','=',Auth::user()->company_id);
        $list = $list->get();

        return ContactListResource::collection($list);
    }
    public function editContact(Request $request,$id = null){
        try {
            $contact = new Contact();
            if ($id) {
                $contact = Contact::where('company_id', Auth::user()->company_id)->where('id', $id)->firstOrFail();
            }
            $contact->fill($request->all());
            $contact->company_id = Auth::user()->company_id;
        $contact->save();
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getSchedule(Request $request){
        $date = $request->input('interval');
        $date = explode(' - ',$date);
        $start =  date('Y-m-d',strtotime($date[0]));
        $end = date('Y-m-d',strtotime($date[1]));
        $days =[];
        $i=0;
        while($i<7){
            $day =date('Y-m-d',strtotime($start));
            $days[$day]=[
                'value'=>$day,
                'formatted' => ConstansHelper::formatDateShort($day),
                'name'=>ConstansHelper::formatDateShort($day)
            ];
            $start = date('Y-m-d',strtotime($start.' +1 day'));
            $i++;
        }
        $schedulesArray = [];
        $industries =Auth::user()->industries;
        if(!$industries){
            $industries = Industry::where('company_id',Auth::user()->company_id)->get()->pluck('id','id');
        }
        foreach ($industries as $k =>$v) {
            $project = Project::where('industry_id', $v)->first();
            if($project) {

                $industry = Industry::where('id', $v)->first();
                $schedules = ProjectSchedule::where('schedule_date', '>=', $date[0])
                    ->leftJoin('students', 'student_id', '=', 'students.id')->select(DB::raw('project_schedules.*, students.name as student_name, students.phone as student_phone'))->disableCache()
                    ->where('schedule_date', '<=', $end)->where('project_id', $project->id)
                    ->orderBy('department')->orderBy('student_id')->orderBy('schedule_date')->get();

                foreach ($schedules as $sc) {
                    $schedulesArray[$industry->name][$sc->department][$sc->student_id][$sc->schedule_date] = [
                        "end" => ConstansHelper::formatTime($sc->end),
                        "have_time" => true,
                        "schedule_date" => $sc->schedule_date,
                        "start" => ConstansHelper::formatTime($sc->start),
                    ];
                    $schedulesArray[$industry->name][$sc->department][$sc->student_id]['name'] = $sc->student_name;
                    $schedulesArray[$industry->name][$sc->department][$sc->student_id]['student_id'] = $sc->student_id;
                    $schedulesArray[$industry->name][$sc->department][$sc->student_id]['phone'] = $sc->student_phone;

                }
            }
        }
        return response()->json([
            'title' => str_replace('<br>',' ',ConstansHelper::formatDateShort($date[0]).'  -  '.ConstansHelper::formatDateShort($date[1])),
            'days' => $days,
            'schedules' => $schedulesArray,
        ],$this->successStatus);

    }

}
