<?php

namespace Modules\Admin\Http\Controllers;

use App\Exports\BaseArrayExport;
use App\Helpers\ConstansHelper;
use App\Http\Controllers\AbstractLiquidController;
use http\Env\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Admin\Entities\AnalyticsData;
use Modules\Admin\Entities\AnalyticsGoogle;
use Modules\Admin\Entities\AnalyticsQuery;

use Modules\Admin\Http\Requests\UpdateDocumentRequest;
use Modules\Admin\Transformers\StudentDocumentsResource;
use Modules\Admin\Transformers\StudentWorkSafetyResource;
use Modules\Company\Entities\Company;
use Modules\Fixing\Entities\Fixing;

use Modules\Project\Entities\ProjectApply;
use Modules\Project\Entities\ProjectStudent;
use Modules\Student\Entities\Student;
use Modules\Project\Entities\Project;
use Modules\Student\Entities\StudentDocuments;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\User\Entities\User;
use phpDocumentor\Reflection\Types\Boolean;

class AdminController extends AbstractLiquidController
{
    public function notifications(Request $request)
    {
        $user = Auth::user();

        return response()->json($user->unreadNotifications, $this->successStatus);
    }

    public function search(Request $request)
    {
        $search = $request->input("search");
        $company = Company::leftJoin('users', 'users.id', '=', 'companies.user_id')->disableCache()->select(DB::raw("companies.id as id,companies.name as company_name, '' as industry_name,users.name as user_name,tax,delegate,contract_date, contract_expire,paper_contract,agancy_contract_date,works_contarct_date"));
        $company = $company->where(function ($query) use ($search) {
            $query->where('companies.name', 'LIKE', '%' . $search . '%')
                ->orWhere('users.name', 'LIKE', '%' . $search . '%')
                ->orWhere('tax', 'LIKE', '%' . $search . '%')
                ->orWhere('delegate', 'LIKE', '%' . $search . '%');
        })->limit(6)->get();
        $student = Student::where(function ($query) use ($search) {
            $query->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('tax_number', 'LIKE', '%' . $search . '%')
                ->orWhere('phone', 'LIKE', '%' . $search . '%')
                ->orWhere('email', 'LIKE', '%' . $search . '%')
                ->orWhere('student_number', 'LIKE', '%' . $search . '%');
        })->disableCache()->limit(6)->get();

        $project = Project::leftJoin('companies', 'companies.id', 'company_id')->disableCache()->leftJoin('company_industries', 'company_industries.id', 'industry_id')->select(DB::raw("projects.id as id,concat(companies.name,'/',company_industries.name) as name"))->where(DB::raw("concat(companies.name,'/',company_industries.name)"), 'LIKE', '%' . $search . '%')->limit(6)->get();
        $response = [];
        $response[] = ['text' => 'Diákok', 'type' => 0, 'link' => '/admin/student/index?search=' . $search];
        foreach ($student as $s) {
            $response[] = [
                'text' => $s->name,
                'desc' => $s->email . ', ' . $s->phone . ', ' . $s->tax,
                'type' => 1,
                'link' => '/admin/student/edit/' . $s->id,
            ];
        }
        $response[] = ['text' => 'Cégek', 'type' => 0];
        foreach ($company as $s) {
            $response[] = [
                'text' => $s->company_name,
                'type' => 1,
                'link' => '/admin/company/edit/' . $s->id,
            ];
        }
        $response[] = ['text' => 'Pojektek', 'type' => 0];
        foreach ($project as $s) {
            $response[] = [
                'text' => $s->name,
                'type' => 1,
                'link' => '/admin/project/details/' . $s->id,
            ];
        }
        return $response;


    }

    public function getDocuments(Request $request)
    {
        $student_doc = StudentDocuments::leftJoin('students', 'students.id', '=', 'student_id')->where('document_type_id', '=', 2)->disableCache()->whereNotNull('signed_date')->whereNull('mads_signed')
            ->select(DB::raw("students.name as student_name,student_documents.*"));
        if (Auth::user()->isSuperAdmin()) {
            $student_doc = $student_doc->where(function ($query) {
                $query->whereIn('student_documents.type', ['adhock','adhock_en','membership','membership_en','exit']);
            });
        } else {
            $student_doc = $student_doc->where(function ($query) {
                $query->whereNotIn('student_documents.type', ['adhock','adhock_en','membership','membership_en','exit'])->orWhereNull('student_documents.type');
            });
        }
        $student_doc = $student_doc->get();
        return response()->json([
            'documents' => $student_doc
        ]);
    }

    public function getMyDocuments(Request $request)
    {
        $student_doc = StudentDocuments::leftJoin('students', 'students.id', '=', 'student_id')->where('document_type_id', '=', 2)->disableCache()->whereNotNull('signed_date')->whereNull('mads_signed')
            ->select(DB::raw("students.name as student_name,student_documents.*"));
        if (Auth::user()->isSuperAdmin()) {
            $student_doc = $student_doc->whereIn('type', ['membership_en', 'membership', 'adhock_en', 'adhock','exit']);
        } else {
            $student_doc = $student_doc->whereNotIn('type', ['membership_en', 'membership', 'adhock_en', 'adhock','exit']);
        }
        $student_doc = $student_doc->get();
        return response()->json([
            'documents' => $student_doc
        ]);
    }

    public function noSign(Request $request, $id)
    {
        try {
            $student_doc = StudentDocuments::where('id', '=', $id)->firstOrFail();
            $student_doc->mads_signed = date('Y-m-d H:i:d');
            $student_doc->save();
            return response()->json("success");
        } catch (ModelNotFoundException $e) {
            abort(404);
        }

    }

    public function applyHandled(Request $request, $id)
    {
        try {
            $prApply = ProjectApply::where('id', '=', $id)->firstOrFail();
            $prApply->handled = date('Y-m-d H:i:d');
            $prApply->save();
            return response()->json("success");
        } catch (ModelNotFoundException $e) {
            abort(404);
        }

    }
    public function expiringCertificates(Request $request){
        $baseQuery =ProjectStudent::where('status_id','=',2)->leftJoin('projects','projects.id','=','project_id')
            ->leftJoin('companies','companies.id','=','projects.company_id')->leftJoin('company_industries','company_industries.id','=','projects.industry_id')->orderBy('expire','desc')
            ->leftJoin('students','students.id','=','project_students.student_id')->whereNull('projects.deleted_at')->select(DB::raw("students.id as student_id, students.name as student_name, expire,concat(companies.name,'/',company_industries.name) as project_name, projects.id as project_id"));
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) {
            $baseQuery = $baseQuery->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
        }
        if ($request->has('user_id')) {
            $baseQuery = $baseQuery->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
        }
        if($request->has('limit')){
            $baseQuery =$baseQuery->limit($request->input('limit'))->offset(0);
        }
        $eu= clone $baseQuery;
        $noEu= clone $baseQuery;
        $eu = $eu->leftJoin(DB::raw('(select student_id, max(expire) as expire from student_work_safety where type = 1 group by student_id) as eu'),'eu.student_id','project_students.student_id')->where('eu.expire','<=',date('Y-m-d',strtotime('+30 day')))->where('companies.medical_certificate','=',2);
        $noEu = $noEu->leftJoin(DB::raw('(select student_id, max(expire) as expire from student_work_safety where type = 1 group by student_id) as eu'),'eu.student_id','project_students.student_id')->whereNull('eu.expire')->where('companies.medical_certificate','=',2);
        $medical= clone $baseQuery;
        $noMedical= clone $baseQuery;
        $medical = $medical->leftJoin(DB::raw('(select student_id, max(expire) as expire from student_work_safety where type = 3 group by student_id) as eu'),'eu.student_id','project_students.student_id')->where('eu.expire','<=',date('Y-m-d',strtotime('+30 day')))->where('companies.medical_certificate','=',1);
        $noMedical = $noMedical->leftJoin(DB::raw('(select student_id, max(expire) as expire from student_work_safety where type = 3 group by student_id) as eu'),'eu.student_id','project_students.student_id')->whereNull('eu.expire')->where('companies.medical_certificate','=',1);

        $certificate= clone $baseQuery;
        $certificate = $certificate->leftJoin(DB::raw(Student::$studentCertificateQuery),'sCard.student_id','=','students.id')->where('sCard.expire','<=',date('Y-m-d',strtotime('+30 day')));

        $passive= clone $baseQuery;
        $passive = $passive->leftJoin(DB::raw('(select max(student_certificates.end) as expire,student_certificates.student_id as student_id from student_certificates left join student_schools on student_certificates.school_id = student_schools.id  where student_certificates.deleted_at is null and student_schools.education_type =2  group by student_certificates.student_id) as sCard'),'sCard.student_id','=','students.id')->where('sCard.expire','<=',date('Y-m-d',strtotime('+30 day')));

        return \response()->json([
            'eu'=>$eu->get(),
            'no_eu'=>$noEu->get(),
            'medical'=>$medical->get(),
            'no_medical'=>$noMedical->get(),
            'certificate'=>$certificate->get(),
            'passive'=>$passive->get(),
        ]);
    }
    public function updateDocument(UpdateDocumentRequest $request, $id)
    {
        try {
            $student_doc = StudentDocuments::where('id', '=', $id)->firstOrFail();
            $student_doc->file = $request->input("new_file");
            $student_doc->mads_signed = date('Y-m-d H:i:d');
            $student_doc->save();
            $student_doc->sendSignEmail();
            return response()->json("success");
        } catch (ModelNotFoundException $e) {
            abort(404);
        }

    }

    public function projectStudents(Request $request)
    {

        $perfect = ProjectStudent::leftJoin('projects', 'projects.id', '=', 'project_students.project_id')
            ->leftJoin('works', 'projects.id', '=', 'works.project_id')
            ->leftJoin('students', 'students.id', '=', 'project_students.student_id')
            ->leftJoin("project_student_answer", function($query){
                $query->on('students.id', '=', 'project_student_answer.student_id',)->on('projects.id','project_student_answer.project_id');
            })
            ->disableCache()
            ->leftJoin('companies', 'companies.id', '=', 'projects.company_id')
            ->whereNotNull('project_student_answer.student_id')
            ->leftJoin('company_industries', 'company_industries.id', '=', 'projects.industry_id')
            ->leftJoin('users', 'users.id', '=', 'projects.user_id')
            ->groupBy('projects.id')->groupBy('students.id')->limit('20')->orderBy('project_students.updated_at','desc')
            ->select(DB::raw("projects.*,works.image,companies.name as company_name,company_industries.name as industry_name, concat(users.lastname,' ',users.firstname) as user_name,students.name as reg_name,students.email as reg_email,students.phone as reg_phone,project_students.updated_at as udate"));


        $potencial = clone $perfect;
        $declined = clone $perfect;
        $perfect= $perfect->where('project_students.status_id',28);
        $potencial= $potencial->where('project_students.status_id',27);
        $declined= $declined->where('project_students.status_id',5);
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) {
            $perfect = $perfect->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
            $potencial = $potencial->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
            $declined = $declined->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
        }
        if ($request->has('user_id')) {
            $perfect = $perfect->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
            $potencial = $potencial->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
            $declined = $declined->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
        }

        $potencial = $potencial->get();
        $perfect = $perfect->get();
        $declined = $declined->get();


        $hour24 = ProjectStudent::where('project_students.created_at', '>=', date('Y-m-d H:i:s', strtotime('-24 hour')))->where('status_id', 1)->disableCache()->leftJoin('projects', 'projects.id', '=', 'project_id')
            ->leftJoin('companies', 'companies.id', '=', 'projects.company_id')
            ->leftJoin('company_industries', 'company_industries.id', '=', 'projects.industry_id')
            ->leftJoin('users', 'users.id', '=', 'projects.user_id')
            ->leftJoin(DB::raw('(select project_id, image from works group by project_id) as works'), 'works.project_id', '=', 'projects.id')
            ->select(DB::raw("projects.*,works.image,count(*) as new_student,companies.name as company_name,company_industries.name as industry_name, concat(users.lastname,' ',users.firstname) as user_name"))->groupBy('projects.id')->orderBy('new_student', 'desc');
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) {
            $hour24 = $hour24->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
        }
        if ($request->has('user_id')) {
            $hour24 = $hour24->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
        }
        $hour24 = $hour24->get();
        $dayAll = ProjectStudent::where('status_id', 1)->disableCache()->leftJoin('projects', 'projects.id', '=', 'project_id')
            ->leftJoin('companies', 'companies.id', '=', 'projects.company_id')
            ->leftJoin('company_industries', 'company_industries.id', '=', 'projects.industry_id')
            ->leftJoin('students', 'students.id', '=', 'project_students.student_id')->whereNull('students.deleted_at')
            ->leftJoin('users', 'users.id', '=', 'projects.user_id')
            ->leftJoin(DB::raw('(select project_id, image from works group by project_id) as works'), 'works.project_id', '=', 'projects.id')
            ->select(DB::raw("projects.*,works.image,count(*) as new_student,companies.name as company_name,company_industries.name as industry_name, concat(users.lastname,' ',users.firstname) as user_name"))->groupBy('projects.id')->orderBy('new_student', 'desc');
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) {
            $dayAll = $dayAll->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
        }
        if ($request->has('user_id')) {
            $dayAll = $dayAll->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
        }
        $dayAll = $dayAll->get();
        $day3 = ProjectStudent::where('project_students.created_at', '>=', date('Y-m-d H:i:s', strtotime('-3 day')))->where('status_id', 1)->disableCache()->leftJoin('projects', 'projects.id', '=', 'project_id')
            ->leftJoin('users', 'users.id', '=', 'projects.user_id')
            ->leftJoin('company_industries', 'company_industries.id', '=', 'projects.industry_id')
            ->leftJoin('companies', 'companies.id', '=', 'projects.company_id')
            ->leftJoin(DB::raw('(select project_id, image from works group by project_id) as works'), 'works.project_id', '=', 'projects.id')->select(DB::raw("projects.*,works.image,count(*) as new_student,companies.name as company_name,company_industries.name as industry_name, concat(users.lastname,' ',users.firstname) as user_name"))->orderBy('new_student', 'desc')->groupBy('projects.id');
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) {
            $day3 = $day3->where(function ($query) {
                $query->where('projects.user_id', '=', \Auth::id())->orWhere('projects.replacement_user_id', '=', \Auth::id());
            });
        }
        if ($request->has('user_id')) {
            $day3 = $day3->where(function ($query) use ($request) {
                $query->where('projects.user_id', '=', $request->input('user_id'));
            });
        }

        $students = StudentDocuments::leftJoin("students", 'students.id', '=', 'student_id')->where(function ($query) {
            $query = $query->where('student_documents.name', 'LIKE', '%tagsági%')
                ->orWhere('student_documents.name', 'LIKE', '%regisztráció%')
                ->orWhere('student_documents.name', 'LIKE', '%tagsagi%')
                ->orWhere('student_documents.name', 'LIKE', '%membership%')
                ->orWhere('student_documents.name', 'LIKE', '%registration%')
                ->orWhere('student_documents.name', 'LIKE', '%regisztracio%');
        })
            ->whereNull('nav_declaration')
            ->whereNull('students.deleted_at')
            ->select(DB::raw("students.name as student_name,email,phone,students.id as student_id"))->disableCache()->groupBy('students.id')->get();

        $day3 = $day3->get();
        return response()->json([
            'hour24' => $hour24->toArray(),
            'day3' => $day3->toArray(),
            'dayAll' => $dayAll->toArray(),
            'perfect' => $perfect->toArray(),
            'potencial' => $potencial->toArray(),
            'declined' => $declined->toArray(),
            'waiting_for_declaration' => $students,
            'users' => (!\Auth::user()->isSuperAdmin() && \Auth::user()->position != 2 && \Auth::user()->position != 3) ? [] : User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name", "id"),
        ], 200);

    }

    public function dashboard(Request $request)
    {
        $analytics = new AnalyticsGoogle();
        $analytics2 = new AnalyticsGoogle();
        $analyticsQuery1 = new AnalyticsQuery('totalUsers', 'deviceCategory');

        $analyticsData1 = new AnalyticsData($analyticsQuery1);
        $analytics->push($analyticsData1);
        $analyticsQuery2 = new AnalyticsQuery('screenPageViews', 'date');
        $analyticsData2 = new AnalyticsData($analyticsQuery2);
        $analytics2->push($analyticsData2);
        $dashboard = [
            'company' => Fixing::where('month', '>=', date('Ym', strtotime('-1 month')))->count(DB::raw('Distinct company_id')) . '/' . Company::count(),
            'project' => Project::where('status', 0)->count() . '/' . Project::whereIn('status', [0, 1])->count(),
            'student' => Fixing::where('month', '>=', date('Ym', strtotime('-1 month')))->count(DB::raw('Distinct student_id')) . '/' . Student::count(),
        ];

        $dashboardPresence = [
            'company' => StudentPresent::where('date', '>=', date('Y-m-01', strtotime('-1 month')))->count(DB::raw('Distinct company_id')),
            'industry' => StudentPresent::where('date', '>=', date('Y-m-01', strtotime('-1 month')))->count(DB::raw('Distinct industry_id')),
            'student' => StudentPresent::where('date', '>=', date('Y-m-01', strtotime('-1 month')))->count(DB::raw('Distinct student_id')),
            'hour' => (int)StudentPresent::where('date', '>=', date('Y-m-01', strtotime('-1 month')))->sum('hour'),
        ];

        return response()->json([
            'devicesDay' => $analytics->getAnalyticsData(date('Y-m-d',strtotime('-1 day')), date('Y-m-d')),
            'devicesLast7Day' => $analytics->getAnalyticsData(date('Y-m-d', strtotime('-7 day')), date('Y-m-d')),
            'deviceLastMonth' => $analytics->getAnalyticsData(date('Y-m-d', strtotime('-1 month')), date('Y-m-d')),
            'pageViews' => $analytics2->getAnalyticsData(date('Y-m-d', strtotime('-1 month')), date('Y-m-d'), 'bar'),
            'dashboard' => $dashboard,
            'dashboardPresence' => $dashboardPresence,
        ], $this->successStatus);
    }


    public function getMenu(Request $request)
    {
        $menus = parent::getMenu($request);
        $isSuperAdmin = Auth::user()->isSuperAdmin();

        $superAdminRoutes = ['/' . config('app.admin_url') . '/module-builder/index', '/telescope'];
        $productionRoutes = (config('app.env') === 'production') ? ['/' . config('app.admin_url') . '/module-builder/index'] : [];

        if ($isSuperAdmin) {
            return $menus;
        }

        $mergedRoles = [];
        $roles = Auth::user()->roles;

        foreach ($roles as $r) {
            try {
                $mergedRoles = array_merge($mergedRoles, json_decode($r->menu, true));
            } catch (\Exception $e) {
                $mergedRoles = array_merge($mergedRoles, $r->menu);
            }
        }

        $index = 0;
        foreach ($menus as $m) {
            if (!array_key_exists($m['route'], $mergedRoles)) {
                unset($menus[$index]);
            } else {
                if (in_array($m['route'], $superAdminRoutes) && in_array($m['route'],
                        $productionRoutes) && !$isSuperAdmin) {
                    unset($menus[$index]);
                }
            }

            if (isset($m['submenu'])) {
                $kndex = 0;
                foreach ($m['submenu'] as $k) {
                    if (!array_key_exists($k['route'], $mergedRoles)) {
                        unset($menus[$index]['submenu'][$kndex]);
                    } else {
                        if (in_array($k['route'], $superAdminRoutes) && in_array($k['route'],
                                $productionRoutes) && !$isSuperAdmin) {
                            unset($menus[$index]);
                        }
                    }
                    $kndex++;
                }
            }
            $index++;
        }

        return $menus;
    }

    public function uploadFile(Request $request)
    {

        $file = $request->file->store('public/files/shares' . $request->input('dir'), 's3');

        $fileExpl = explode("public/", $file);

        return \response()->json(['path' => $file, 'url' => 'https://mads.eu-central-1.linodeobjects.com/'.$file],
            $this->successStatus);
    }

    public function uploadPrivateFile(Request $request)
    {

        $file = $request->file->store('private/' . $request->input('dir'), 's3');
        $name = $request->file->getClientOriginalName();
        return \response()->json(['path' => $file, 'url' => 'https://mads.eu-central-1.linodeobjects.com/'.$file, 'name' => $name],
            $this->successStatus);
    }

    public function getDocument(Request $request, $any)
    {


        $file = Storage::disk('s3')->get(str_replace("//", "/", $any));
        $name = explode('/', $any);

        return response()->make($file, 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . end($name) . '.pdf"'
        ]);
    }

    public function getAnyDocument(Request $request, $any)
    {

        return Storage::disk('s3')->response($any);
    }

    public function regionStatistic(Request $request, $excel = null)
    {
        $list = DB::table("fixing_view")
            ->where('month', '>=', str_replace('-', '', $request->input('start')))
            ->where('month', '<=', str_replace('-', '', $request->input('end')))
            ->select(DB::raw(" company_name, industry_name, fixing_view.company_id , industry_id,region,
	        
	         sum(saturday_hour) as saturday_hour,
	         sum(sunday_hour) as sunday_hour,
	         sum(festive_hour) as festive_hour,
	         sum(fix_company) as sum_fix,
	       
	         sum(hour) as sum_hour,
	         CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
	         CEILING(SUM(sum_net)) as sum_net,
	         CEILING(SUM(sum_net)*1.27) as sum_gross,
	         
	         sum(night_hour) as night_hour,
	         CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
	         CEILING(Sum( night_sum_net))as night_sum_net,
	         CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,
	         
	         sum(festive_hour) as festive_hour,
	         CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
	         CEILING(Sum( festive_sum_net))as festive_sum_net,
	         CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,
	         
	         sum(sunday_hour) as sunday_hour,
	         CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
	         CEILING(Sum( sunday_sum_net))as sunday_sum_net,
	         CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,
	         
	         sum(saturday_hour) as saturday_hour,
	         CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
	         CEILING(Sum( saturday_sum_net))as saturday_sum_net,
	         CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
	         payment_deadline,
	         sum(IF(invoice_status is null or invoice_status=0,1,0)) as  invoice_status,
	         sum(company_price) as amrest_gross,
	         count(distinct(student_id)) as student_count,
	       
	         CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,status"))->orderBy('company_name', 'asc')->leftJoin('company_industries', 'company_industries.id', '=', 'industry_id');

        $list = $list->groupBy(DB::raw('company_industries.id'));

        $list = $list->get();
        $response = [];
        foreach ($list as $item) {

            if (!isset($response[$item->region][$item->company_name . $item->company_id])) {

                $response[$item->region][$item->company_name . $item->company_id] = [
                    "company_name" => $item->company_name,
                    "payment_deadline" => $item->payment_deadline,
                    "company_id" => $item->company_id,
                    "invoice_status" => $item->invoice_status,
                    "status" => $item->status,
                    "student_count" => $item->student_count,
                    "company_count" => 0,
                    "sum_hour" => 0, "night_hour" => 0, "saturday_hour" => 0, "sunday_hour" => 0, "festive_hour" => 0, "sum_student" => 0, "sum_company" => 0, "sum_fix" => 0, "amrest_gross" => 0, "hour" => 0,
                    "night_hour_price" => 0, "saturday_hour_price" => 0, "sunday_hour_price" => 0, "festive_hour_price" => 0];

            }
            $response[$item->region][$item->company_name . $item->company_id]["sum_hour"] += $item->festive_hour + $item->night_hour + $item->sunday_hour + $item->saturday_hour + $item->sum_hour;
            $response[$item->region][$item->company_name . $item->company_id]["sum_student"] += $item->festive_sum_net + $item->night_sum_net + $item->sunday_sum_net + $item->saturday_sum_net + $item->sum_net;
            $response[$item->region][$item->company_name . $item->company_id]["night_hour"] += $item->night_hour;
            $response[$item->region][$item->company_name . $item->company_id]["hour"] += $item->sum_hour;
            $response[$item->region][$item->company_name . $item->company_id]["student_count"] += $item->student_count;
            $response[$item->region][$item->company_name . $item->company_id]["saturday_hour"] += $item->saturday_hour;
            $response[$item->region][$item->company_name . $item->company_id]["sunday_hour"] += $item->sunday_hour;
            $response[$item->region][$item->company_name . $item->company_id]["festive_hour"] += $item->festive_hour;
            $response[$item->region][$item->company_name . $item->company_id]["sum_fix"] += $item->sum_fix;
            $response[$item->region][$item->company_name . $item->company_id]["student_count"] += $item->student_count;
            $response[$item->region][$item->company_name . $item->company_id]["amrest_gross"] += $item->amrest_gross;
            if ($item->company_id == 863 || $item->company_id == 1047) {
                $response[$item->region][$item->company_name . $item->company_id]["sum_fix"] += $item->amrest_gross;
            }
            $response[$item->region][$item->company_name . $item->company_id]["night_hour_price"] += $item->night_sum_net;
            $response[$item->region][$item->company_name . $item->company_id]["saturday_hour_price"] += $item->saturday_sum_net;
            $response[$item->region][$item->company_name . $item->company_id]["sunday_hour_price"] += $item->sunday_sum_net;
            $response[$item->region][$item->company_name . $item->company_id]["festive_hour_price"] += $item->festive_sum_net;

        }
        $students = Fixing::select(DB::raw('count(distinct student_id) as student_count,region'))->leftJoin('company_industries', 'company_industries.id', '=', 'industry_id')
            ->where('month', '>=', str_replace('-', '', $request->input('start')))
            ->where('month', '<=', str_replace('-', '', $request->input('end')))->groupBy('region')->get()->pluck('student_count', 'region')->toArray();
        $region = [1 => 'Közép', 2 => 'Nyugat', 3 => 'Kelet'];
        $regionList = [];
        foreach ($response as $k => $v) {
            if(isset($region[$k])) {
                if (!isset($regionList[$k])) {
                    $regionList[$k] = [
                        "region" => $region[$k],
                        "sum_hour" => 0,
                        "sum_student" => (isset($students[$k])) ? $students[$k] : 0,

                        "sum_company" => 0,
                        "sum_net" => 0,


                    ];
                }
                foreach ($v as $k1 => $v1) {
                    $regionList[$k]["sum_company"] += 1;
                    $regionList[$k]["sum_net"] += $v1['sum_student'] + $v1['sum_fix'];
                    $regionList[$k]["sum_hour"] += (int)$v1['sum_hour'];
                }
            }
        }
        if ($excel) {
            $export = new BaseArrayExport(['Régió', 'Ledolgozott munkaóra', 'Munkát végző tagok száma', 'Megbízók száma', 'Nettó árbevétel'], "Régió statisztika", $regionList);

            return $export->download(Str::slug('Regio-statisztika') . '.xlsx');
        }
        return response()->json($regionList, 200);
    }
    public function getFixingStatistic(Request $request,$type=1,$excel = null){
        $companies = Company::whereNull('companies.deleted_at')->leftJoin("users",'companies.user_id','=','users.id')
            ->select(DB::raw("companies.id as id,companies.invoice_type,online_presence,contract_date,importer, companies.name as name, companies.tax as tax, concat(users.lastname,' ',users.firstname) as pm,pr.w as worker,emp,docs"))->orderBy("companies.name")
        ->leftJoin(DB::raw('(select company_id, count(*) as docs from company_documents  group by company_id ) as  cdoc'),'cdoc.company_id','=','companies.id')
        ->leftJoin(DB::raw('(select company_id,id, sum(if( number_of_employees is null,0,number_of_employees) )as  emp,sum(if(worker is null,0,worker)) as w  from projects left join (select project_id,count(distinct student_id) as worker from project_students left join students on students.id = student_id where status_id=2 and students.deleted_at is null and students.id is not null group by project_id ) as work on projects.id = work.project_id group by company_id) as pr'),'pr.company_id','=','companies.id')



        ;
        $response =[];
        $year = $request->input('year', date('Y'));
        if($request->has('company_name')){
            $companies->where('companies.name','LIKE','%'.$request->input('company_name').'%');
        }
        if($request->has('sales')){
            $companies->where('companies.importer','LIKE','%'.$request->input('sales').'%');
        }
        if($request->has('user_id')){
            $companies->where('users.id','=',$request->input('user_id'));
        }
        if($request->has('contract_date')){
            $companies->where('companies.contract_date','LIKE',$request->input('contract_date').'%');
        }
        $companies = $companies->get();
        foreach ($companies as $c){
            $response[$c->id]=[
                "name"=>$c->name,
                "id"=>$c->id,
                "tax"=>$c->tax,
                "contract_date"=>$c->contract_date,
                "importer"=>$c->importer,
                "online_presence"=>ConstansHelper::bool($c->online_presence),
                "invoice_type"=>ConstansHelper::bool(!$c->invoice_type),
                "docs"=>$c->docs,
                "emp"=>$c->emp,
                "worker"=>$c->worker,
                "1"=>0,
                "2"=>0,
                "3"=>0,
                "4"=>0,
                "5"=>0,
                "6"=>0,
                "7"=>0,
                "8"=>0,
                "9"=>0,
                "10"=>0,
                "11"=>0,
                "12"=>0,
                "sum"=>0,
                "pm"=>$c->pm
            ];
        }
        if($type==1) {
            $list = DB::table("fixing_view")->where('month', 'LIKE', $year . '%')->groupBy('company_id')->groupBy('month')
                ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,month,

              sum(saturday_hour) as saturday_hour,
              sum(sunday_hour) as sunday_hour,
              sum(festive_hour) as festive_hour,
              sum(fix_company) as sum_fix,


              sum(hour) as sum_hour,
              CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
              CEILING(SUM(sum_net)) as sum_net,
              CEILING(SUM(sum_net)*1.27) as sum_gross,

              sum(night_hour) as night_hour,
              CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
              CEILING(Sum( night_sum_net))as night_sum_net,
              CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,

              sum(festive_hour) as festive_hour,
              CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
              CEILING(Sum( festive_sum_net))as festive_sum_net,
              CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,

              sum(sunday_hour) as sunday_hour,
              CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
              CEILING(Sum( sunday_sum_net))as sunday_sum_net,
              CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,

              sum(saturday_hour) as saturday_hour,
             CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
              CEILING(Sum( saturday_sum_net))as saturday_sum_net,
               CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
              payment_deadline,
              invoice_status,
                student_name,
	         teaor,
	          sum(company_price) as amrest_gross,
              CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross"))->orderBy('company_name', 'asc')->get();
            foreach ($list as $student) {

                $sum = $student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix;
                if ($student->company_id == 863 || $student->company_id == 1047) {
                    $sum += $student->amrest_gross;
                }
                $month = (int)str_replace($year, "", $student->month);
                if(isset( $response[$student->company_id])) {
                    $response[$student->company_id][$month] = $sum;
                }
            }
        }
        if($type==2){
            $list = DB::table("fixing_view")->where('month', 'LIKE', $year . '%')->groupBy('company_id')->groupBy('month')
                ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,month,  sum(hour) as sum_hour "))->get();
            foreach ($list as $student) {


                $month = (int)str_replace($year, "", $student->month);
                if(isset( $response[$student->company_id])) {
                $response[$student->company_id][$month] = (int)($student->sum_hour*100)/100;
                }
            }
        }
        if($type==3){
            $list = DB::table("fixing_view")->where('month', 'LIKE', $year . '%')->groupBy('company_id')->groupBy('month')
                ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,month,  COUNT(DISTINCT(student_id)) as students "))->get();
            foreach ($list as $student) {


                $month = (int)str_replace($year, "", $student->month);
                if(isset( $response[$student->company_id])) {
                    $response[$student->company_id][$month] = $student->students;
                }
            }
        }
        foreach ($response as $k=>$v){

            $response[$k]['sum'] =(int)(($response[$k][1]+
                $response[$k][2]+
                $response[$k][3]+
                $response[$k][4]+
                $response[$k][5]+
                $response[$k][6]+
                $response[$k][7]+
                $response[$k][8]+
                $response[$k][9]+
                $response[$k][10]+
                $response[$k][11]+
                $response[$k][12])*100)/100;
        }

        return response()->json(['data'=>$response,'users'=>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id")]);
    }
    public function contractStatistic(Request $request, $excel = null)
    {
        $statistic = StudentDocuments::where(function($query){
            $query->where('type','LIKE','%membership%')->orWhere('student_documents.name','LIKE','%regisztracio%');
        })->whereNull('student_documents.deleted_at')->whereNotNull('students.id')->orderBy('student_documents.updated_at','asc');
        if(!$request->has('month')){
            $statistic = $statistic->where('student_documents.updated_at','LIKE',$request->input('year').'-%')->groupBy(DB::raw('MONTH(student_documents.updated_at)'))->groupBy(DB::raw('YEAR(student_documents.updated_at)'))
                ->select(DB::raw('MONTH(student_documents.updated_at) as date, sum(if(document_type_id<>0,1,0)) as online,sum(if(document_type_id=0,1,0)) as cnt,count(*) as allCnt'));
        }else{
            $statistic = $statistic->where('student_documents.updated_at','LIKE',$request->input('month').'%')->groupBy(DB::raw('DATE(updated_at)'))
                ->select(DB::raw('DATE(student_documents.updated_at) as date, sum(if(document_type_id<>0,1,0)) as online,sum(if(document_type_id=0,1,0)) as cnt,count(*) as allCnt'));
        }
        $statistic = $statistic->leftJoin('students','students.id','=','student_id')->whereNull('students.deleted_at')->get()->toArray();
        if ($excel) {
            $export = new BaseArrayExport(['Hónap/Nap', 'Elektronikus', 'Nem elektronikus', 'Összes',], "Szerződés statisztika", $statistic);

            return $export->download(Str::slug('szerzodes-statisztika') . '.xlsx');
        }
        return response()->json($statistic, 200);
    }
    public function contractStatisticDetails(Request $request)
    {

        $statistic = StudentDocuments::where(function($query){
            $query->where('type','LIKE','%membership%')->orWhere('student_documents.name','LIKE','%regisztracio%');
        })->whereNull('student_documents.deleted_at')->whereNotNull('students.id')->orderBy('student_documents.updated_at','asc');
        if($request->input('type','online') == 'online'){
            $statistic = $statistic->where('student_documents.updated_at','LIKE',$request->input('date').'%')->where('document_type_id','<>',0)
                ->select(DB::raw('student_documents.updated_at as date, students.name as student_name, students.id, student_id, students.student_number as student_number '));
        }else{
            $statistic = $statistic->where('student_documents.updated_at','LIKE',$request->input('date').'%')->where('document_type_id','=',0)
                ->select(DB::raw('student_documents.updated_at as date, students.name as student_name, students.id, student_id, students.student_number as student_number '));
        }
        $statistic = $statistic->leftJoin('students','students.id','=','student_id')->get()->toArray();

        return response()->json($statistic, 200);
    }
        public function declineWorkSafety(Request $request, $id){
        try {
            $safety = StudentWorkSafety::where('id', $id)->firstOrFail();
            $safety->forceDelete();
            return \response()->json('OK');
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
    }
    public function approveWorkSafety(Request $request, $id)
    {
        try {
            $safety = StudentWorkSafety::where('id', $id)->firstOrFail();
            $safety->fill($request->all());
            $safety->approved = Auth::user()->id;
            $safety->save();
            return \response()->json('OK');
        } catch (ModelNotFoundException $e) {
            abort(404);
        }
    }

    public function membershipDocuments(Request $request){
        $filters =[
            [
                'name' => 'search',
                'title' => 'Keresés',
                'type' => 'text',
            ],
            [
                'name' => 'type',
                'title' => 'Típus',
                'type' => 'select',
                'data' => ['adhock'=>'Eseti','membership'=>'Tagsági'],
            ],
           ];
        $docs = StudentDocuments::whereIn('type',['adhock','adhock_en','membership','membership_en'])->where('document_type_id','>',0)->whereNull('student_documents.deleted_at')->whereNotNull('created')
            ->leftJoin('students','students.id','=','student_documents.student_id')
            ->leftJoin(DB::raw('(select student_id, project_id from project_students where status_id =2) as ps'),'ps.student_id','=','students.id')
            ->leftJoin('projects','projects.id','=','ps.project_id')
            ->leftJoin('companies','companies.id','=','projects.company_id')
            ->leftJoin('company_industries','company_industries.id','=','projects.industry_id')
            ->leftJoin('users','users.id','=','student_documents.created')->select(DB::raw("student_documents.*,concat_ws('/',companies.name,company_industries.name,projects.name) as project_name,concat(users.lastname,' ',users.firstname) as user,students.name as student_name, students.email as email, students.phone as phone,students.student_number as student_number"));
        if($request->has('type')){
            if($request->input('type')=='membership'){
                $docs = $docs->whereIn('type',['membership','membership_en']);
            }
            if($request->input('type')=='adhock'){
                $docs = $docs->whereIn('type',['adhock','adhock_en']);
            }
        }
        if($request->has('search')){
            $search = $request->input('search');
            $docs = $docs->where(function($query) use($search){
                $query->where('students.student_number','LIKE','%'.$search.'%')
                    ->orWhere('project_name','LIKE','%'.$search.'%')
                    ->orWhere('students.name','LIKE','%'.$search.'%')
                    ->orWhere('students.phone','LIKE','%'.$search.'%')
                    ->orWhere('students.email','LIKE','%'.$search.'%');
            });
        }
        if($request->has('user')){
            $docs = $docs->where('student_documents.created','=',$request->input('user'));
        }
        if (!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {

            $docs = $docs->where(function ($query) {
                $query->where('student_documents.created', '=', \Auth::id());
            });
        }else{
            $users =User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id");
            $filters[] =[
                'name' => 'user',
                'title' => 'Felhasználó',
                'type' => 'select',
                'data' => $users,
            ];
        }
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $docs = $docs->paginate($pagination)->withPath($request->path());
        return StudentDocumentsResource::collection($docs)->additional(['filters' => $filters]);
    }
    public function workSafety(Request $request)
    {
        $list = StudentWorkSafety::leftJoin('students', 'students.id', '=', 'student_id');
        if ($request->input('approve', 'false') == 'true') {
            $list = $list->where('approve_is_required', 1)->where(function ($query) {
                $query->whereNotNull('approved');
            });;
        } else {
            $list = $list->where('approve_is_required', 1)->whereNull('approved');
        }

        if($request->has('students_name')){
            $list = $list->where('students.name', 'LIKE', '%' . $request->input('students_name') . '%');
        }
        if($request->has('students_student_number')){
            $list = $list->where('students.student_number', 'LIKE', '%' . $request->input('students_student_number') . '%');
        }
        if ($request->has('search')) {
            $search = $request->input('search');
            $list = $list->where(function ($query) use ($search) {
                $query->where('students.name', 'LIKE', '%' . $search . '%')->orWhere('students.student_number', 'LIKE', '%' . $search . '%');
            });
        }
        $list = $list->leftJoin(DB::raw("(select project_id,student_id from project_students where status_id=2 group by student_id) as prs"),'prs.student_id','=','students.id')
            ->leftJoin('projects','projects.id','=','prs.project_id')->leftJoin('companies','companies.id','=','projects.company_id')->leftJoin('company_industries','company_industries.id','=','projects.industry_id');
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->select(DB::raw("student_work_safety.*, students.name as student_name,students.student_number,concat_ws('/',companies.name,company_industries.name ,projects.name) as project "))->paginate($pagination)->withPath($request->path());
        // dd(DB::getQueryLog());
        return StudentWorkSafetyResource::collection($list)->additional(['filters' => (new StudentWorkSafety())->getFilters()]);
    }
}
