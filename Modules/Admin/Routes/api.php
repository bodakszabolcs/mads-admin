<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/notifications', 'AdminController@notifications');
    Route::get('/menu', 'AdminController@getMenu');
    Route::get('/dashboard', 'AdminController@dashboard')->name('Dashboard');
    Route::get('/expiring-certificates', 'AdminController@expiringCertificates')->name('expiringCertificates');
    Route::get('/projects-status', 'AdminController@projectStudents')->name('Project status');
    Route::get('/get-documents', 'AdminController@getDocuments')->name('getDocuments');
    Route::get('/admin/company-user/presents/{month?}', 'AdminCompanyController@getPresents')->name('Jelenlétik lekérdezése');
    Route::post('/admin/company-user/save-presents', 'AdminCompanyController@approvePresents')->name('Jelenlétik jóváhagyása');
    Route::get('/admin/company/workers-list', 'AdminCompanyController@listWorker')->name('Dolgozó diákok');
    Route::get('/admin/company/presents-summation/{month}', 'AdminCompanyController@getPresentsSummation')->name('Jelenléti összesítő');

    Route::post('/upload-file', 'AdminController@uploadFile')->name('Upload file');
	Route::get('/get-document/{any}', 'AdminController@getDocument')->name('get pdf Document')->where('any', '.*');
	Route::get('/get-any-document/{any}', 'AdminController@getAnyDocument')->name('get any Document')->where('any', '.*');
    Route::post('/upload-private-file', 'AdminController@uploadPrivateFile')->name('Upload non public file');

});
Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => ['auth:sanctum','role']], function () {
    Route::post('/update-document/{id}', 'AdminController@updateDocument')->name("Dashboard upload doucment");
    Route::post('/no-sign/{id}', 'AdminController@noSign')->name("Dashboard document handle");
    Route::post('/apply-handled/{id}', 'AdminController@applyHandled')->name("Handle email registration dashboard");
    Route::get('/admin/region-statistic/{excel?}', 'AdminController@regionStatistic')->name("Régói összesítő");
    Route::get('/admin/fixing-statistic/{type}/{excel?}', 'AdminController@getFixingStatistic')->name("Bevétel összesítő");
    Route::get('/admin/contract-statistic/{excel?}', 'AdminController@contractStatistic')->name("Szerződés összesítő");
    Route::get('/admin/contract-statistic-details', 'AdminController@contractStatisticDetails')->name("Szerződés összesítő részletek");
    Route::get('/admin/work-safety', 'AdminController@workSafety')->name("Orvosi igazolások jóváhagyása lista");
    Route::get('/admin/membership-documents', 'AdminController@membershipDocuments')->name("Tagsági, eseti lista");
    Route::post('/admin/approve-work-safety/{id}', 'AdminController@approveWorkSafety')->name("Orvosi igazolások jóváhagyása");
    Route::delete('/admin/delete-work-safety/{id}', 'AdminController@declineWorkSafety')->name("Orvosi igazolások elutasítása");

});
Route::group(['namespace' => 'Modules\Admin\Http\Controllers','prefix'=>'/admin', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/company/data', 'AdminCompanyController@data')->name('Company data');
    Route::get('/search', 'AdminController@search')->name('Global search top bar');
    Route::get('/company/schedule', 'AdminCompanyController@getSchedule')->name('Company schedules');
    Route::get('/company/documents', 'AdminCompanyController@documents')->name('Company documents');
    Route::put('/company/document/edit', 'AdminCompanyController@documentEdit')->name('Company document edit');
    Route::get('/company/fixing-summation/{year}', 'AdminCompanyController@fixingSummationList')->name('Company certificates');
    Route::get('/company/fixing-summation/excel/{month}/{industry}', 'AdminCompanyController@excel')->name('fixing summation excel export');
    Route::get('/company/get-my-fixings', 'AdminCompanyController@getFixings')->name('Company fixings');
    Route::get('/company/get-my-industries', 'AdminCompanyController@getIndustries')->name('Company industries');
    Route::get('/company/get-my-contacts', 'AdminCompanyController@getContacts')->name('Company contacts');
    Route::get('/company/get-my-campaign', 'AdminCompanyController@getMyCampaign')->name('Company get my campaign');
    Route::get('/company/get-my-questions', 'AdminCompanyController@getMyQuestions')->name('Company getMyQuestions');
    Route::post('/company/save-answer', 'AdminCompanyController@saveAnswer')->name('Company saveAnswer');

    Route::post('/company/decline-request', 'AdminCompanyController@declineRequest')->name('Company saveAnswer');
    Route::post('/company/call-me-back', 'AdminCompanyController@callMeBack')->name('Company saveAnswer');

    Route::post('/company/approve-request', 'AdminCompanyController@approveRequest')->name('Company approveRequest');
    Route::get('/company/get-my-requests', 'AdminCompanyController@getMyRequest')->name('Company getMyRequest');
    Route::get('/company/get-my-contracts', 'AdminCompanyController@getMyContract')->name('Company getMyContract');
    Route::match(['post'], '/company/contract/save-as-draft/{id}', 'AdminCompanyController@saveAsDraft')->name('Contract save as draft');
    Route::match(['post'], '/company/contract/approve-contract/{id}', 'AdminCompanyController@approveContract')->name('Contract approve');
    Route::match(['post'], '/company/contract/revert-last-version/{id}', 'AdminCompanyController@revertLastVersion')->name('Contract revert last version');
    Route::match(['post'], '/company/contract/save-and-approve/{id}', 'AdminCompanyController@saveAndApproveContract')->name('Contract save and approve');
    Route::match(['get'], '/company/contract/show/{id}', 'AdminCompanyController@contractShow')->name('Contract show');
    Route::match(['get'], '/company/contract/pdf-preview/{id}/0-szamu-melleklet', 'AdminCompanyController@contractShowNull')->name('Contract show');
    Route::match(['get'], '/company/contract/pdf-preview/{id}/1-szamu-melleklet', 'AdminCompanyController@contractShowFirst')->name('Contract show');
    Route::match(['get'], '/company/contract/pdf-preview/{id}/2-szamu-melleklet', 'AdminCompanyController@contractShowSecond')->name('Contract show');
    Route::match(['get'], '/company/contract/pdf-preview/{id}/szerzodes', 'AdminCompanyController@contractShowContract')->name('Contract show');


    Route::match(['put','post'],'/company/edit-contact/edit/{id?}', 'AdminCompanyController@editContact')->name('Company edit contact');
    Route::match(['post'],'/company-new-student', 'AdminCompanyController@newStudent')->name('Company new student request');
});


Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => ['auth:sanctum']], function () {
    Route::get('/register/{hash}', 'AdminController@register');
});

Route::group(['namespace' => 'Modules\Admin\Http\Controllers', 'middleware' => []], function () {
    Route::get('/company/completion-form/{type}/{month}', 'AdminCompanyController@completionForm');
    Route::get('/company/completion-form-excel/{type}/{month}', 'AdminCompanyController@completionFormExcel');
    Route::get('/company/presents-pdf/{month}/{industry}', 'AdminCompanyController@getPresentsPdf');
    Route::post('/admin/password/email','ForgotPasswordController@sendResetLinkEmail');
    Route::post('/admin/password/reset', 'ResetPasswordController@reset')->name('password.reset');
});
Route::group(['middleware' => ['student'],'namespace' => 'App\Http\Controllers\Auth'], function () {


    Route::post('/admin/password/email','ForgotPasswordController@sendResetLinkEmail');
    Route::post('/admin/password/reset', 'ResetPasswordController@reset')->name('password.reset');
    // Registration Routes...
});
