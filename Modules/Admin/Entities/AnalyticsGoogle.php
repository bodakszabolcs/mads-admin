<?php


namespace Modules\Admin\Entities;


use Analytics;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use AkkiIo\LaravelGoogleAnalytics\Facades\LaravelGoogleAnalytics;
use AkkiIo\LaravelGoogleAnalytics\Period;
use Google\Analytics\Data\V1beta\Filter\StringFilter\MatchType;
use Google\Analytics\Data\V1beta\MetricAggregation;
use Google\Analytics\Data\V1beta\Filter\NumericFilter\Operation;
/**
 * Class AnalyticsGoogle
 * @package Modules\Event\Entities
 * integration generate user, credentional https://github.com/spatie/laravel-analytics#how-to-obtain-the-credentials-to-communicate-with-google-analytics
 * metrics query explorer https://ga-dev-tools.appspot.com/query-explorer/
 */
class AnalyticsGoogle
{
  private $query=[];
  private $analyticsData=[];

  public function push(AnalyticsData $data){
      $this->query[] = $data;
  }

  public function getAnalyticsData($from,$to,$type='pie'){
        $period = Period::create(Carbon::parse($from),Carbon::parse($to));

        $data =LaravelGoogleAnalytics::dateRange($period)->metrics($this->query[0]->query->metrics)->dimension($this->query[0]->query->dimensions)->metricAggregation(MetricAggregation::TOTAL)->orderByDimensionDesc($this->query[0]->query->dimensions)->get()->table;
      if($type=='pie'){
          $this->analyticsData[0]['data'] = $this->convertAnalyticsDataToVueJSChartData($data);
      }else{
          $this->analyticsData[0]['data'] = $this->convertAnalyticsDataToVueJSChartLineData($data);
      }
      /*  $index =0;
        foreach ($this->query as $query){
            try {
                $temp = Analytics::performQuery(
                    $period,
                    'ga:sessions',
                    [
                        'metrics' => $query->query->metrics,
                        'dimensions' => $query->query->dimensions,
                        //'filter' => $query->query->filter
                    ]
                );
                if($type=='pie'){
                $this->analyticsData[$index]['data'] = $this->convertAnalyticsDataToVueJSChartData($temp);
                }else{
                    $this->analyticsData[$index]['data'] = $this->convertAnalyticsDataToVueJSChartLineData($temp);
                }
                $index++;
            }catch (\Exception $e){

                Log::error('ANALYTICS  QUERY error: '.json_encode($e->getMessage()));
            }
        }*/

        return $this->analyticsData;
  }
  public function convertAnalyticsDataToVueJSChartData($data){

      $labels=[];
      $background_colors = array(
          '#67809F',
          '#94A0B2',
          '#4DB3A2',
          '#29B4B6',
          '#FF3838');
      $datasets=['datasets'=>[],'labels'=>[]];
      $index =0;
      $j=0;
        foreach ($data as $row){
        try {
            $datasets['datasets'][$index]['data'][] = array_values($row)[1];
            $datasets['datasets'][$index]['label'][] = array_values($row)[0];
            $datasets['datasets'][$index]['backgroundColor'][] = $background_colors[$j % sizeof($background_colors)];
            $datasets['labels'][] = array_values($row)[0];

            $j++;
        }
        catch (\Exception $e){
            dd($row);
        }
        }

      return ['labels'=>$datasets['labels'],'datasets'=>$datasets['datasets']];
  }
    public function convertAnalyticsDataToVueJSChartLineData($data){

        $labels=[];

        $datasets=[];
        $index =0;
        $j=0;
        foreach ($data as $row){

            $datasets['datasets'][$index]['data'][] =  array_values($row)[1];
            $datasets['datasets'][$index]['label'] =  'Nap';
            $datasets['datasets'][$index]['backgroundColor'] = '#67809F';
            $datasets['labels'][] = array_values($row)[0];

            $j++;

        }

        return ['labels'=>$datasets['labels'],'datasets'=>$datasets['datasets']];
    }
}
