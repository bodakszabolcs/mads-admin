<?php


namespace Modules\Admin\Entities;


class AnalyticsQuery
{
    public $metrics;
    public $filter;
    public $dimensions = 'ga:year,ga:month,ga:day';

    public function __construct($metrics, $dimensions = null, $filter = null)
    {
        $this->metrics = $metrics;
        if ($dimensions) {
            $this->dimensions = $dimensions;
        }
        $this->filter = $filter;
    }
}
