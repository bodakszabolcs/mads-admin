<?php


namespace Modules\Admin\Entities;


class AnalyticsData
{
    /**
     * @var string pie|line|bar
     */
    public $chartType;
    public $title;
    /**
     * @var string CAMPAIGNID on ANAlYTICS
     */
    public $filterKey;

    public $query=[];

    public function __construct(AnalyticsQuery $query)
    {
        $this->query = $query;
    }
}
