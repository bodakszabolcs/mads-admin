<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\City\Entities\City;

$factory->define(City::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
