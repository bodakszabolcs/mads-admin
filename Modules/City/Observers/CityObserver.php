<?php

namespace Modules\City\Observers;

use Modules\City\Entities\City;

class CityObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function saved(City $model)
    {
        City::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function created(City $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function updated(City $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function deleted(City $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function restored(City $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\City\Entities\City  $model
     * @return void
     */
    public function forceDeleted(City $model)
    {
        //
    }
}
