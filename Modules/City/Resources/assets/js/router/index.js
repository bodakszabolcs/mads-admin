import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import City from '../components/City'
import CityList from '../components/CityList'
import CityCreate from '../components/CityCreate'
import CityEdit from '../components/CityEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'city',
                component: City,
                meta: {
                    title: 'Cities'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CityList',
                        component: CityList,
                        meta: {
                            title: 'Cities',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/city/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/city/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CityCreate',
                        component: CityCreate,
                        meta: {
                            title: 'Create Cities',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/city/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CityEdit',
                        component: CityEdit,
                        meta: {
                            title: 'Edit Cities',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/city/index'
                        }
                    }
                ]
            }
        ]
    }
]
