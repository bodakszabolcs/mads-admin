<?php

namespace Modules\City\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\City\Entities\City;
use Modules\City\Observers\CityObserver;

class CityServiceProvider extends ModuleServiceProvider
{
    protected $module = 'city';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        City::observe(CityObserver::class);
    }
}
