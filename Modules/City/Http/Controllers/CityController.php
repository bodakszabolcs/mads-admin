<?php

namespace Modules\City\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\City\Entities\City;
use Illuminate\Http\Request;
use Modules\City\Http\Requests\CityCreateRequest;
use Modules\City\Http\Requests\CityUpdateRequest;
use Modules\City\Transformers\CityViewResource;
use Modules\City\Transformers\CityListResource;

class CityController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new City();
        $this->viewResource = CityViewResource::class;
        $this->listResource = CityListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CityCreateRequest::class;
        $this->updateRequest = CityUpdateRequest::class;
    }

}
