<?php

namespace Modules\EducationLevel\Observers;

use Modules\EducationLevel\Entities\EducationLevel;

class EducationLevelObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function saved(EducationLevel $model)
    {
        EducationLevel::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function created(EducationLevel $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function updated(EducationLevel $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function deleted(EducationLevel $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function restored(EducationLevel $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\EducationLevel\Entities\EducationLevel  $model
     * @return void
     */
    public function forceDeleted(EducationLevel $model)
    {
        //
    }
}
