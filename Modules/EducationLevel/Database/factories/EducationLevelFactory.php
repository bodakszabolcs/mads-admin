<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\EducationLevel\Entities\EducationLevel;

$factory->define(EducationLevel::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
