<?php

namespace Modules\EducationLevel\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\EducationLevel\Entities\EducationLevel;
use Illuminate\Http\Request;
use Modules\EducationLevel\Http\Requests\EducationLevelCreateRequest;
use Modules\EducationLevel\Http\Requests\EducationLevelUpdateRequest;
use Modules\EducationLevel\Transformers\EducationLevelViewResource;
use Modules\EducationLevel\Transformers\EducationLevelListResource;

class EducationLevelController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new EducationLevel();
        $this->viewResource = EducationLevelViewResource::class;
        $this->listResource = EducationLevelListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = EducationLevelCreateRequest::class;
        $this->updateRequest = EducationLevelUpdateRequest::class;
    }

}
