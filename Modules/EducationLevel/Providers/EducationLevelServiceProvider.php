<?php

namespace Modules\EducationLevel\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\EducationLevel\Entities\EducationLevel;
use Modules\EducationLevel\Observers\EducationLevelObserver;

class EducationLevelServiceProvider extends ModuleServiceProvider
{
    protected $module = 'educationlevel';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        EducationLevel::observe(EducationLevelObserver::class);
    }
}
