import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import EducationLevel from '../components/EducationLevel'
import EducationLevelList from '../components/EducationLevelList'
import EducationLevelCreate from '../components/EducationLevelCreate'
import EducationLevelEdit from '../components/EducationLevelEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'education-level',
                component: EducationLevel,
                meta: {
                    title: 'EducationLevels'
                },
                children: [
                    {
                        path: 'index',
                        name: 'EducationLevelList',
                        component: EducationLevelList,
                        meta: {
                            title: 'EducationLevels',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/education-level/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-level/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'EducationLevelCreate',
                        component: EducationLevelCreate,
                        meta: {
                            title: 'Create EducationLevels',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-level/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'EducationLevelEdit',
                        component: EducationLevelEdit,
                        meta: {
                            title: 'Edit EducationLevels',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-level/index'
                        }
                    }
                ]
            }
        ]
    }
]
