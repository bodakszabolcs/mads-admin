<?php

namespace Modules\Menu\Transformers;

use App\Http\Resources\BaseResource;

class ItemResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order' => $this->order,
            'title' => $this->title,
            'link' => $this->link,
            'parent_id' => $this->parent_id,
            'children' => ItemResource::collection($this->childrens)
        ];
    }
}
