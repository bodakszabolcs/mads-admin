<?php

namespace Modules\Menu\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Menu\Entities\Base\BaseItem;

class Item extends BaseItem
{
    use Cachable;
}
