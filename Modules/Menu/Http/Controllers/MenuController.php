<?php

namespace Modules\Menu\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Http\Request;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Http\Requests\MenuCreateRequest;
use Modules\Menu\Transformers\MenuResource;

class MenuController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Menu();
        $this->viewResource = MenuResource::class;
        $this->listResource = MenuResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = MenuCreateRequest::class;
        $this->updateRequest = MenuCreateRequest::class;
    }
    public function update(Request $request, $id, $auth = null)
    {

        $response = parent::update($request, $id, $auth); // TODO: Change the autogenerated stub
        $menu = Menu::where('id',$id)->first();
        $menu->generateMenuJson();
        return $response;

    }
}
