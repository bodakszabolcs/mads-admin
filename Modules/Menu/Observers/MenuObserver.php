<?php

namespace Modules\Menu\Observers;

use Modules\Menu\Entities\Menu;

class MenuObserver
{
    public function saved(Menu $menu)
    {

    }

    /**
     * Handle the blog "deleted" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function deleted(Menu $menu)
    {

    }

    /**
     * Handle the blog "restored" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function restored(Menu $menu)
    {

    }

    /**
     * Handle the blog "force deleted" event.
     *
     * @param \Modules\Menu\Entities\Menu $menu
     * @return void
     */
    public function forceDeleted(Menu $menu)
    {
    }
}
