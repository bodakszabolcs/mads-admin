import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Fixing from '../components/Fixing'
import FixingList from '../components/FixingList'
import FixingEdit from '../components/FixingEdit'
import PayScale from '../components/PayScale'
import FixingControl from '../components/FixingControl'
import FixingSummation from '../components/FixingSummation'

import PresenceIndustry from '../components/PresenceIndustry'
import PresenceCompany from '../components/PresenceCompany'
import FixingAmrest from '../components/FixingAmrest'
import WorkerStatistic from '../components/WorkerStatistic.vue'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'fixing',
                component: Fixing,
                meta: {
                    title: 'Fixings'
                },
                children: [
                    {
                        path: 'index',
                        name: 'FixingList',
                        component: FixingList,
                        meta: {
                            title: 'Fixings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/fixing/index'
                        }
                    },
                    {
                        path: 'amrest',
                        name: 'FixingAmrest',
                        component: FixingAmrest,
                        meta: {
                            title: 'Fixing amrest',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/fixing/index'
                        }
                    },
                    {
                        path: 'worker',
                        name: 'WorkerStatistic',
                        component: WorkerStatistic,
                        meta: {
                            title: 'Fixing worker',
                            subheader: false,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/fixing/index'
                        }
                    },
                    {
                        path: 'presence',
                        name: 'FixingPersenceList',
                        component: PresenceCompany,
                        meta: {
                            title: 'Jelenléti',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/fixing/presence'
                        }
                    },
                    {
                        path: 'presence/company/:month/:industry',
                        name: 'FixingPresenceListCompany',
                        component: PresenceIndustry,
                        meta: {
                            title: 'Jelenléti diákok',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/fixing/presence/company/:industry'
                        }
                    },
                    {
                        path: 'edit/:month/:company?/:industry?',
                        name: 'FixingEdit',
                        component: FixingEdit,
                        meta: {
                            title: 'Edit Fixings'
                        }
                    }
                ]
            },
            {
                path: 'pay-scale/index',
                component: PayScale,
                meta: {
                    title: 'Bértábla'
                }
            },

            {
                path: 'fixing-control/index',
                component: FixingControl,
                meta: {
                    title: 'Rögzítés jóváhagyása és visszavonása'
                }
            },
            {
                path: 'fixing-summation/index',
                component: FixingSummation,
                meta: {
                    title: 'Összegzés'
                }
            }
        ]
    }
]
