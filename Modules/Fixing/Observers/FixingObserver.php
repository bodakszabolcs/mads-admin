<?php

namespace Modules\Fixing\Observers;

use Modules\Fixing\Entities\Fixing;

class FixingObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function saved(Fixing $model)
    {
        Fixing::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function created(Fixing $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function updated(Fixing $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function deleted(Fixing $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function restored(Fixing $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Fixing\Entities\Fixing  $model
     * @return void
     */
    public function forceDeleted(Fixing $model)
    {
        //
    }
}
