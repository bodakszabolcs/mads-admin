<?php

namespace Modules\Fixing\Http\Controllers;

use App\Helpers\ConstansHelper;
use App\Helpers\PdfHelper;
use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Industry;
use Modules\Fixing\Entities\Fixing;
use Illuminate\Http\Request;
use Modules\Fixing\Http\Requests\FixingCreateRequest;
use Modules\Fixing\Http\Requests\FixingUpdateRequest;
use Modules\Fixing\Transformers\FixingViewResource;
use Modules\Fixing\Transformers\FixingListResource;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ProjectSchedule;
use Modules\Project\Entities\ProjectStudent;
class FixingControlController extends Fixing
{

   public function fixingControlList(Request  $request,$month){

   	    $list = Fixing::where('month','=',str_replace('-','',$month))->groupBy('fixings.company_id')->groupBy('fixings.industry_id')->groupBy('student_id')
	        ->leftJoin('companies','companies.id','=','fixings.company_id')->disableCache()
	        ->leftJoin('students','students.id','=','fixings.student_id')
	        ->leftJoin('company_industries','company_industries.id','=','fixings.industry_id')
	        ->select(DB::raw("companies.name as company_name,company_industries.name as  industry_name,students.name as student_name,fixings.company_id as company_id, fixings.industry_id as industry_id, student_id,
	         sum(status)-count(*) as status_computed,
	         sum(hour+bonus_hour) as sum_hour, 
	         sum(bonus_hour) as sum_bonus_hour,
	         sum(student_price) as  sum_price,
	         sum(company_price) as sum_company"))->orderBy('companies.name','asc');
       if (!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {
           $list = $list->where(function ($query) {
               $query->where('companies.user_id', '=', \Auth::id());
           });
       }
	    if($request->input('search') && !empty($request->input('search'))){
	    	$list = $list->where(function($query)use($request){
	    		$query->where('students.name','LIKE','%'.$request->input('search').'%')
				    ->orWhere('companies.name','LIKE','%'.$request->input('search').'%')
				    ->orWhere('company_industries.name','LIKE','%'.$request->input('search').'%');
		    });
	    }
	   if($request->input('status','') !== '' && $request->input('status','')!==null){
	   	    $list = $list->where('fixings.status','=',$request->input('status'));
	   }

	   $list = $list->where('student_price','>',0)->get();

   	   $response =[];
   	    foreach ($list as $item){
   	    	if(!isset($response[$item->company_name.'-'.$item->company_id])){
		        $response[$item->company_name.'-'.$item->company_id] =[
		        	    "company_name"=>$item->company_name,
			            "company_id"=>$item->company_id,
			            "status_computed" =>0,
			            "sum_hour" => 0,
			            "sum_student" => 0,
			            "sum_company" => 0,
			            "industries" =>[]
		            ];

	        }
   	    	if(!isset($response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id])){
		        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id] =[
			        "industry_name"=>$item->industry_name,
			        "industry_id"=>$item->industry_id,
			        "status_computed" =>0,
			        "students"=>[],
			        "sum_hour" => 0,
			        "sum_student" => 0,
			        "sum_company" => 0,
		        ];
	        }
	        if(!isset($response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]['students'][$item->student_id])){
		        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]['students'][$item->student_id] =[
			        "student_name"=>$item->student_name,
			        "student_id"=>$item->student_id,
			        "sum_hour" => 0,
			        "sum_bonus_hour" => 0,
			        "sum_student" => 0,

		        ];
	        }
	        $response[$item->company_name.'-'.$item->company_id]["sum_hour"] += $item->sum_hour;
	        $response[$item->company_name.'-'.$item->company_id]["status_computed"] += $item->status_computed;
	        $response[$item->company_name.'-'.$item->company_id]["sum_student"] += $item->sum_price;
	        $response[$item->company_name.'-'.$item->company_id]["sum_company"] += $item->sum_company;

	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]["sum_hour"] += $item->sum_hour;
	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]["sum_student"] += $item->sum_price;

	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]["sum_company"] += $item->sum_company;
	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]["status_computed"] += $item->status_computed;

	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]['students'][$item->student_id]["sum_hour"]+=$item->sum_hour;
	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]['students'][$item->student_id]["sum_bonus_hour"]+=$item->sum_bonus_hour;
	        $response[$item->company_name.'-'.$item->company_id]['industries'][$item->industry_id]['students'][$item->student_id]["sum_student"]+=$item->sum_price;

        }
   	    return response()->json($response,200);
   }
   public function approve(Request $request,$month,$company,$industry=null){
   	$fixing = Fixing::where('month','=',str_replace('-','',$month))->where('company_id','=',$company);
   	if($industry){
   		$fixing = $fixing->where('industry_id',$industry);
    }
   	$fixing->update(['status'=>1,'updated_at'=>date('Y-m-d H:i:s')]);
   	return $this->fixingControlList($request,$month);
   }

	public function decline(Request  $request,$month,$company,$industry=null){
		$fixing = Fixing::where('month','=',str_replace('-','',$month))->where('company_id','=',$company);
		if($industry){
			$fixing = $fixing->where('industry_id',$industry);
		}
		$fixing->update(['status'=>0,'updated_at'=>date('Y-m-d H:i:s')]);
		return $this->fixingControlList($request,$month);
	}
    public function recalculate(Request  $request,$month,$company,$type=0){
        $fixingIndustry = Fixing::where('month','=',str_replace('-','',$month))->where('company_id','=',$company)->groupBy('industry_id')->get();
        DB::beginTransaction();
        foreach ($fixingIndustry as $ind){
            $industry=Industry::where('id',$ind->industry_id)->first();
            //Fixing::where('month','=',str_replace('-','',$month))->where('company_id','=',$company)->where('industry_id',$industry->id)->where('hour','=','0')->where('bonus_hour','=',0)->where('student_price','=','0')->forceDelete();
            $fixing =Fixing::where('month','=',str_replace('-','',$month))->where('company_id','=',$company)->where('industry_id',$industry->id)->get();

            foreach ($fixing as $fix){
                $fix->fix_company_multiplier =$industry->price_company_fix;
                if($type==0) {
                    $fix->price_hour = $industry->price_hour;
                }
                $fix->price_company_hourly =$industry->price_company_hourly;
                $fix->multiplier =$industry->multiplier;
                $fix->divisor =$industry->divisor;

                switch ($fix->bonus_type) {
                    case 1:{
                        $fix->bonus_multiplier= $industry->night_bonus;
                        $fix->bonus_multiplier_company=$industry->night_bonus_company;
                        break;}
                    case 2:
                        {$fix->bonus_multiplier= $industry->saturday_bonus;
                        $fix->bonus_multiplier_company=$industry->saturday_bonus_company;

                        break;}
                    case 3:
                        {$fix->bonus_multiplier= $industry->sunday_bonus;
                        $fix->bonus_multiplier_company=$industry->sunday_bonus_company;

                        break;}
                    case 4:
                        {$fix->bonus_multiplier= $industry->festive_bonus;
                        $fix->bonus_multiplier_company=$industry->festive_bonus_company;

                        break;}
                }
                $fix->price_company_hourly=$fix->calculateCompanyHourly($industry->price_company_hourly,$industry->hourly_algorithm);
                $fix->student_price = $fix->calculateStudentPrice();
                $fix->company_price = $fix->calculateCompanyPrice();
                $fix->save();
            }
            DB::commit();
        }
        return $this->fixingControlList(  $request,$month);

    }


}
