<?php

	namespace Modules\Fixing\Http\Controllers;
	use App\Helpers\ConstansHelper;
	use App\Http\Controllers\AbstractLiquidController;
	use Illuminate\Console\Scheduling\Schedule;
	use Illuminate\Support\Arr;
	use Illuminate\Support\Facades\DB;
	use Modules\AreaType\Entities\AreaType;
	use Modules\City\Entities\City;
	use Modules\Country\Entities\Country;
	use Modules\Fixing\Entities\Fixing;
	use Illuminate\Http\Request;
	use Modules\Fixing\Http\Requests\FixingCreateRequest;
	use Modules\Fixing\Http\Requests\FixingUpdateRequest;
	use Modules\Fixing\Transformers\FixingViewResource;
	use Modules\Fixing\Transformers\FixingListResource;
    use Modules\Fixing\Transformers\PayScaleResource;
    use Modules\MonthlyPayment\Entities\MonthlyPayment;
    use Modules\Payroll\Entities\Payroll;
    use Modules\Project\Entities\Project;
	use Modules\Project\Entities\ProjectSchedule;
	use Modules\Project\Entities\ProjectStudent;
	use Modules\Student\Entities\Student;
	class PayScaleController extends AbstractLiquidController
	{
		public function __construct(Request $request)
		{
			parent::__construct($request);
			$this->model = new Fixing();
			$this->viewResource = FixingViewResource::class;
			$this->listResource = FixingListResource::class;
			$this->useResourceAsCollection = true;
			$this->createRequest = FixingCreateRequest::class;
			$this->updateRequest = FixingUpdateRequest::class;
		}

		public function index(Request $request, $auth = null)
		{
            $firstDateOfMonth =  $request->input('month').'-01';
			$month = str_replace('-', '', $request->input('month', date('Ym')));

			$list = DB::table("fixings")->where('fixings.month', '=',  $month)
               ->groupBy('fixings.student_id')
                ->leftJoin('company_industries', 'industry_id', 'company_industries.id')

                ->leftJoin('students', 'students.id', 'fixings.student_id')->leftJoin('feors', 'feors.id', 'feor_id')
                ->leftJoin('nationalities', 'nationalities.id', 'students.nationality')
                ->leftJoin(DB::Raw('(select sum(price) as price, student_id from monthly_payments where month='.$month.' group by student_id) as monthly_payments'), function ($join) use ($month) {
					$join->on('fixings.student_id', '=', 'monthly_payments.student_id');
				})->leftJoin(DB::raw(Student::$studentCertificateQuery), function ($join) {
					$join->on('fixings.student_id', '=', 'sCard.student_id');
				})->select(DB::raw("students.name as student_name,students.email as student_email,nationalities.code as code, nationalities.discount as discount,students.birth_date,students.afa as student_afa,fixings.month as month,students.id as student_id,
				students.part_ticket as part_ticket, tax_number, GROUP_CONCAT(DISTINCT(feors.code)) as feor, sum(student_price) as price, monthly_payments.price as monthly_price "))->orderBy('students.name','asc');
			$noCard = clone $list;
			$noCard = $noCard->where(function ($query) use ($month) {
				$query->where('sCard.expire', '<', date('Y-m-d', strtotime($month . '01')))->orWhereNull('sCard.expire');
			})->get();
			$hasCard = $list->where('sCard.expire', '>=', date('Y-m-d', strtotime($month . '01')))->get();
			$monthlyPayments = MonthlyPayment::leftJoin('students', 'students.id', 'student_id')->leftJoin('users', 'users.id', 'user_id')->leftJoin('nationalities', 'nationalities.id', 'students.nationality')
                ->select('monthly_payments.*', DB::raw("students.name as student_name,users.name as user_name,students.birth_date as birth_date,month,tax_number,nationalities.code as code, nationalities.discount as discount "))->where('month', '=', $month)->orderBy('monthly_payments.id','desc')->get();

			return response()->json(['noCard' => PayScaleResource::collection($noCard), 'hasCard' =>PayScaleResource::collection($hasCard), 'monthly' => PayScaleResource::collection($monthlyPayments)]);

		}
        public function sendEmailNoCard(Request  $request,$month){
            $list = DB::table("fixings")->where('fixings.month', '=',  $month)
                ->groupBy('fixings.student_id')
                ->leftJoin('company_industries', 'industry_id', 'company_industries.id')
                ->leftJoin('students', 'students.id', 'fixings.student_id')->leftJoin('feors', 'feors.id', 'feor_id')

                ->leftJoin(DB::Raw('(select sum(price) as price, student_id from monthly_payments where month='.$month.' group by student_id) as monthly_payments'), function ($join) use ($month) {
                    $join->on('fixings.student_id', '=', 'monthly_payments.student_id');
                })->leftJoin(DB::raw(Student::$studentCertificateQuery), function ($join) {
                    $join->on('fixings.student_id', '=', 'sCard.student_id');
                })->select(DB::raw("students.name as student_name,students.email as student_email,students.birth_date,students.afa as student_afa,fixings.month as month,students.id as student_id,
				students.part_ticket as part_ticket, tax_number, GROUP_CONCAT(DISTINCT(feors.code)) as feor, sum(student_price) as price, monthly_payments.price as monthly_price "))->orderBy('students.name','asc')->where('sCard.expire', '>=', date('Y-m-d', strtotime($month . '01')))->get();

        }
		public function payScaleExport(Request $request, $month)
		{
			$csvHeader = 'Adószám;Vezetéknév;Keresztnév;Keresztnév2;Férfi/Nő;Belépés dátuma;Születési dátum;Születési helye;TAJ szám;Személyi igazolvány száma;Születési név;Anyja vezetékneve;Anyja keresztneve;Anyja keresztneve2;Ország;Lakcím irszám;Lakcím (ir.szám, város);Lakcím utca;Közterület;Házszám;Épület;Lépcsőház;Emelet;Ajtó;Bankszámlaszám;FEOR szám;Alkalmazás minősége;Hónap;Bruttó bér;Hóközi kifizetés;Levont részjegy;Időszak kezdete;Időszak vége;Állampolgársági kód' . "\n";
			$month = str_replace('-', '', $month);
            $list = DB::table("fixings")->where('fixings.month', '=',  $month)
                ->groupBy('fixings.student_id')
                ->leftJoin('company_industries', 'industry_id', 'company_industries.id')
                ->leftJoin('students', 'students.id', 'fixings.student_id')->leftJoin('feors', 'feors.id', 'feor_id')
                ->leftJoin('nationalities', 'nationalities.id', 'students.nationality')
                ->leftJoin(DB::Raw('(select sum(price) as price, student_id from monthly_payments where month='.$month.' group by student_id) as monthly_payments'), function ($join) use ($month) {
                    $join->on('fixings.student_id', '=', 'monthly_payments.student_id');
                })->leftJoin(DB::raw(Student::$studentCertificateQuery), function ($join) {
                    $join->on('fixings.student_id', '=', 'sCard.student_id');
                })->select(DB::raw("students.*,fixings.month as month,students.id as student_id,nationalities.code as code, nationalities.discount as discount,
				students.part_ticket as part_ticket, tax_number, GROUP_CONCAT(DISTINCT(feors.code)) as feor, sum(student_price) as price, monthly_payments.price as monthly_price "))
                ->where('fixings.month','=',$month)->where('sCard.expire', '>=', date('Y-m-d', strtotime($month . '01')))->orderBy('students.name','asc')->get();
			$countries = Country::all()->pluck('name', 'id')->toArray();
			$cities = City::all()->pluck('name', 'id')->toArray();
			$areaTypes = AreaType::all()->pluck('name', 'id')->toArray();
			foreach ($list as $row) {

				$dt_start = date('Y.m.01', strtotime($month . '01 '));
				$dt_end = date('Y.m.t', strtotime($month . '01 '));
				if ($dt_start < date('Y.m.d', strtotime($row->date_of_entry))) {
					$dt_start = date('Y.m.d', strtotime($row->date_of_entry));
				}
				if ($dt_end > date('Y.m.d', strtotime($row->exit_date)) && !empty($row->exit_date)) {
					$dt_end = date('Y.m.d', strtotime($row->exit_date));
				}
				$nevArray = explode(' ', $row->name);
				$anyjaArray = explode(' ', $row->mothers_name);
				$row = $row->tax_number . ";" . (isset($nevArray[0]) ? $nevArray[0] : "") . ";" . (isset($nevArray[1]) ? $nevArray[1] : "") . ";" . (isset($nevArray[2]) ? $nevArray[2] : "") . ";" . ($row->gender == 1 ? 2 : 1) . ";" . (($row->date_of_entry) ? date('Y.m.d', strtotime($row->date_of_entry)):'') . ";" . $row->birth_date . ";" . $row->birth_location . ";" . str_replace(' ','',$row->taj) . ";" . $row->identity_card . ";" . $row->name . ";" . (isset($anyjaArray[0]) ? $anyjaArray[0] : "") . ";" . (isset($anyjaArray[1]) ? $anyjaArray[1] : "") . ";" . (isset($anyjaArray[2]) ? $anyjaArray[2] : "") . ";" . Arr::get($countries, ''.$row->country_id, '') . ";" . $row->zip . ";" . Arr::get($cities, ''.$row->city_id, '') . ";" . $row->street . ";" . Arr::get($areaTypes, ''.$row->area_type_id,'') . ";" . $row->house_number . ";" . $row->building . ";" . $row->stairs . ";" . $row->level . ";" . $row->door . ";" . $row->bank_account_number . ";" . $row->feor . ";" . "11;" . $month . ";" . (int)$row->price . ";" . ((int)$this->calcNet($row)) . ";" . (($row->part_ticket==1)?0:2000) . ";" . $dt_start . ";" . $dt_end . ";".$row->code.";" . "\n";
				$csvHeader .= $row;
			}
			$current = "\xEF\xBB\xBF" . $csvHeader;
			header('Content-type: application/csv; charset=ansi');
			header('Content-Disposition: attachment; filename="Unit-' . $month . '.csv"');
			echo $current;
			exit();
		}
        function calcNet($row){
            if(!$row->discount || ( date("Y-m-d",strtotime($row->birth_date."+ 25 year"))<date("Y-m-d",strtotime($row->month.'01')) || $row->afa))
            {
                return $row->monthly_price*0.85;
            }
            if($row->monthly_price> Payroll::$underAgeLimit){
                return Payroll::$underAgeLimit+(($row->monthly_price-Payroll::$underAgeLimit)*0.85);
            }
            return  $row->monthly_price;
        }
	}
