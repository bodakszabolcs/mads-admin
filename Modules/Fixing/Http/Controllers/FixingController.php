<?php

namespace Modules\Fixing\Http\Controllers;

use App\Exports\AmrestExport;
use App\Exports\PartnerExport;
use App\Helpers\ConstansHelper;
use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Company\Entities\Industry;
use Modules\Fixing\Entities\Fixing;
use Illuminate\Http\Request;
use Modules\Fixing\Entities\FixingAmrest;
use Modules\Fixing\Http\Requests\FixingCreateRequest;
use Modules\Fixing\Http\Requests\FixingUpdateRequest;
use Modules\Fixing\Transformers\FixingViewResource;
use Modules\Fixing\Transformers\FixingListResource;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ProjectSchedule;
use Modules\Project\Entities\ProjectStudent;
use Modules\Student\Entities\StudentAmrest;

class FixingController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Fixing();
        $this->viewResource = FixingViewResource::class;
        $this->listResource = FixingListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = FixingCreateRequest::class;
        $this->updateRequest = FixingUpdateRequest::class;
    }

    public function index(Request $request, $auth = null)
    {
        $month = str_replace('-', '', $request->input('month', date('Y-m')));


        $list = $this->model->groupBy('fixings.company_id')->groupBy('fixings.industry_id')->where('month', '=', $month)->where('companies.name', 'LIKE', '%' . $request->input('company_name') . '%')
            ->leftJoin('companies', 'companies.id', '=', 'company_id')->disableCache()
            ->leftJoin('company_industries', 'company_industries.id', '=', 'industry_id')
            ->select('month', 'invoice_status', 'invoice_id', DB::raw('sum(fix) as price, count(Distinct student_id) as students, sum(hour+bonus_hour) as hours,status,
			companies.name as company_name,company_industries.name as industry_name,companies.name as id'))->orderBy('companies.name', 'asc');

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }

    public function getStudents(Request $request, $industry, $month)
    {

        $project = Project::where('industry_id', '=', $industry)->get()->pluck('id','id');
        $month2 = date('Y-m', strtotime($month . '-01'));
        $month = date('Ym', strtotime($month . '-01'));

        //TODO fixálni a státuszokat
        $response = [];
        if ($project && sizeof($project)) {
            $students = ProjectStudent::whereIn('project_id', $project)->where('status_id', 2)
                ->leftJoin(DB::raw("(select student_id as s_id, sum(hour) as phour from student_presents where date LIKE '%$month2%' group by s_id) as presence"), 'presence.s_id', '=', 'student_id')
                ->leftJoin('students', 'students.id', '=', 'student_id')->select('name', 'tax_number','student_number', 'student_id', 'phour')->orderBy('students.name')->groupBy('students.id')->get();


            foreach ($students as $s) {
                $response[$s->student_id] = [
                    'name' => $s->name, 'tax' => $s->tax_number,'student_number'=> $s->student_number, 'student_id' => $s->student_id, 'color' => 'bg-info', 'status' => 0, 'phour' => $s->phour
                ];
            }
        }
        $fixings = Fixing::where('month', '=', $month)->where('industry_id', $industry)->leftJoin('students', 'students.id', '=', 'student_id')->disableCache()->disableCache()
            ->leftJoin(DB::raw("(select student_id as s_id, sum(hour) as phour from student_presents where date LIKE '%$month2%' group by s_id) as presence"), 'presence.s_id', '=', 'student_id')
            ->select('name', 'tax_number', 'student_id','student_number', 'status', 'phour')->orderBy('students.name')->groupBy('student_id')->get();
        foreach ($fixings as $s) {
            $response[$s->student_id] = [
                'name' => $s->name, 'tax' => $s->tax_number,'student_number'=> $s->student_number, 'student_id' => $s->student_id, 'color' => ($s->status == 1) ? 'bg-danger' : 'bg-success', 'status' => $s->status, 'phour' => $s->phour
            ];
        }

        return response()->json($response, 200);

    }

    public function getStudentFixing(Request $request, $student, $industry, $month)
    {
        $response = [];
        $price_hour = null;
        $price_company_hourly = null;
        $freedays = [];
        $fixings = Fixing::where('student_id', $student)->where('industry_id', $industry)
            ->where('month', date('Ym', strtotime($month . '-01')))->get();

        if (sizeof($fixings)) {
            $response = $this->fillEmptyDate($month, $response);
            foreach ($fixings as $f) {
                $response[$f->date] = [
                    'start' => $f->start,
                    'end' => $f->end,
                    'sum' => $f->sum,
                    'fix' => $f->fix,
                    'break_from' => $f->break_from,
                    'break_to' => $f->break_to,
                    'bonus' => is_null($f->bonus) ? true : $f->bonus,
                    'bonus_hour' => $f->bonus_hour,
                    'bonus_type' => $f->bonus_type,
                    'bonus_multiplier' => $f->bonus_multiplier,
                    'bonus_multiplier_company' => $f->bonus_multiplier_company,
                    'fix_company_multiplier' => $f->fix_company_multiplier,
                    'shortDate' => (int)(date('d', strtotime($f->date))),
                    'day' => ConstansHelper::$days[date('N', strtotime($f->date))]
                ];
                $price_hour = $f->price_hour;
                $price_company_hourly = $f->price_company_hourly;
                if ($f->bonus_type == 4) {
                    $freedays [] = $f->date;
                }
            }

            return response()->json(['price_hour' => $price_hour, 'price_company_hourly' => $price_company_hourly, 'freedays' => implode(',', $freedays), 'fixing' => $response], 200);
        }

        $schedule = ProjectSchedule::where('student_id', $student)->where('industry_id', $industry)
            ->where('schedule_date', '>=', date('Y-m-d', strtotime($month . '-01')))
            ->where('schedule_date', '<=', date('Y-m-t', strtotime($month . '-01')))->get();

        if (sizeof($schedule)) {

            foreach ($schedule as $s) {
                $response = $this->fillEmptyDate($month, $response);
                $response[$s->schedule_date] = [
                    'start' => ConstansHelper::formatTime($s->start),
                    'end' => ConstansHelper::formatTime($s->end),
                    'sum' => '',
                    'fix' => '',
                    'bonus' => true,
                    'break_from' => '',
                    'break_to' => '',
                    'bonus_hour' => '',
                    'bonus_type' => '',
                    'bonus_multiplier' => '',
                    'bonus_multiplier_company' => '',
                    'fix_company_multiplier' => '',
                    'shortDate' => (int)date('d', strtotime($s->schedule_date)),
                    'day' => ConstansHelper::$days[date('N', strtotime($s->schedule_date))]

                ];
            }


            return response()->json(['price_hour' => $price_hour, 'price_company_hourly' => $price_company_hourly, 'freedays' => implode(',', $freedays), 'fixing' => $response], 200);
        }

        $response = $this->fillEmptyDate($month, $response);
        return response()->json(['price_hour' => $price_hour, 'price_company_hourly' => $price_company_hourly, 'freedays' => implode(',', $freedays), 'fixing' => $response], 200);

    }

    public function fillEmptyDate($month, $response)
    {

        $start = date('Y-m-d', strtotime($month . '-01'));
        $end = date('Y-m-t', strtotime($month . '-01'));

        $empty = [
            'start' => '',
            'end' => '',
            'sum' => '',
            'fix' => '',
            'bonus' => true,
            'bonus_type' => '',
            'bonus_hour' => '',
            'break_from' => '',
            'break_to' => '',
            'bonus_multiplier' => '',
            'bonus_multiplier_company' => '',
            'fix_company_multiplier' => '',

        ];
        while ($start <= $end) {
            if (!isset($response[$start])) {
                $response[$start] = $empty;
                $response[$start]['shortDate'] = (int)date('d', strtotime($start));
                $response[$start]['day'] = ConstansHelper::$days[date('N', strtotime($start))];
            }
            $start = date('Y-m-d', strtotime($start . ' +1 day'));
        }
        return $response;
    }

    public function deleteFixing(Request $request)
    {
        Fixing::where('student_id', $request->input('student_id'))->where('industry_id', $request->input('industry_id'))
            ->where('month', date('Ym', strtotime($request->input('month') . '-01')))->forceDelete();
        return response($this->successMessage, $this->successStatus);
    }

    public function saveFixing(Request $request)
    {

        Fixing::where('student_id', $request->input('student_id'))->where('industry_id', $request->input('industry_id'))
            ->where('month', date('Ym', strtotime($request->input('month') . '-01')))->forceDelete();
        $industry = Industry::where('id', $request->input('industry_id'))->first();
        foreach ($request->input('fixing') as $k => $item) {
            $fixing = new Fixing();
            $fixing->status = 0;
            $fixing->industry_id = $request->input('industry_id');
            $fixing->company_id = $request->input('company_id');
            $fixing->student_id = $request->input('student_id');
            $fixing->month = date('Ym', strtotime($request->input('month') . '-01'));
            $fixing->date = $k;
            $fixing->start = $item['start'];
            $fixing->end = $item['end'];
            $fixing->hour = $item['sum'];
            $fixing->break_from = $item['break_from'];
            $fixing->break_to = $item['break_to'];
            $fixing->bonus_hour = $item['bonus_hour'];
            $fixing->bonus_multiplier = $item['bonus_multiplier'];
            $fixing->divisor = $request->input('divisor');
            $fixing->multiplier = $request->input('multiplier');
            $fixing->bonus_type = $item['bonus_type'];
            $fixing->bonus = $item['bonus'];
            $fixing->fix = $item['fix'];
            $fixing->fix_company_multiplier = $item['fix_company_multiplier'];
            $fixing->price_hour = $request->input('price_hour');
            $fixing->price_company_hourly = $fixing->calculateCompanyHourly($request->input('price_company_hourly'), $industry->hourly_algorithm);
            $fixing->student_price = $fixing->calculateStudentPrice();
            $fixing->company_price = $fixing->calculateCompanyPrice();
            $fixing->user_id = \Auth::id();
            if ($fixing->student_price > 0) {
                $fixing->save();
            }
        }
        return response()->json('OK', 200);

    }

    public function fixingAmrest(Request $request, $month, $costCenter)
    {
        $month = str_replace('-', '', $month);

        $response = [];
        $fixingAmrestStudents = FixingAmrest::where("month", $month)->leftJoin("students", 'students.id', 'fixing_amrest.student_id')->leftJoin('student_amrest', 'student_amrest.student_id', 'students.id')
            ->select(DB::raw('fixing_amrest.*,students.name as student_name, students.tax_number as tax_number'))->where("fixing_amrest.cost_center", $costCenter)->get();

        $projectStudents = ProjectStudent::leftJoin('projects', 'projects.id', 'project_id')->leftJoin("students", 'students.id', 'project_students.student_id')
            ->leftJoin('student_amrest', 'student_amrest.student_id', 'students.id')
            ->leftJoin('company_industries', 'company_industries.id', 'projects.industry_id')
            ->select(DB::raw('projects.*,amrest_id,company_industries.cost_center,company_industries.recruiter, student_amrest.price as st_price, student_amrest.exited as exited, students.name as student_name,student_amrest.position as position,company_industries.recruiter as recruiter, students.tax_number as tax_number,students.id as student_id'))
            ->where('project_students.status_id', 2)
            ->where("company_industries.cost_center", $costCenter)
            ->get();
        $allIndustry = Industry::where("cost_center", $costCenter)->get();
        foreach ($allIndustry as $i) {
            $response[$i->id] = [
                'industry_name' => $i->name,
                'price_hour' => $i->price_hour,
                'price_company_hourly' => $i->price_company_hourly,
                'teaor' => $i->teaor,
                'students' => []
            ];
        }
        foreach ($fixingAmrestStudents as $fa) {
            if (!isset($response[$fa->industry_id]['students'][$fa->student_name . "-" . $fa->student_id])) {
                $response[$fa->industry_id]['students'][$fa->student_name . "-" . $fa->student_id] = [
                    'student_name' => $fa->student_name,
                    'student_id' => $fa->student_id,
                    'tax_number' => $fa->tax_number,
                    'amrest_id' => $fa->amrest_id,
                    'industry_id' => $fa->industry_id,
                    'cost_center' => $fa->cost_center,
                    'sum_hour' => $fa->sum_hour,
                    'sum_night' => $fa->sum_night,
                    'festive' => $fa->festive,
                    'price_hourly' => $fa->price_hourly,
                    'deficit' => $fa->deficit,
                    'deduction' => $fa->deduction,
                    'bonus' => $fa->bonus,
                    'bonus_presence' => $fa->bonus_presence,
                    'company_price' => $fa->company_price,
                    'gross_student' => $fa->gross_student,
                    'net' => $fa->net,
                    'comment' => $fa->comment,
                    'month' => $month,
                    'position' => $fa->position,
                    'recruiter' => $fa->recruiter,
                    'exited' => $fa->exited,


                ];
            }
        }
        foreach ($projectStudents as $pa) {
            if (!isset($response[$pa->industry_id]['students'][$pa->student_name . "-" . $pa->student_id])) {
                $response[$pa->industry_id]['students'][$pa->student_name . "-" . $pa->student_id] = [
                    'student_name' => $pa->student_name,
                    'student_id' => $pa->student_id,
                    'amrest_id' => $pa->amrest_id,
                    'tax_number' => $pa->tax_number,
                    'industry_id' => $pa->industry_id,
                    'cost_center' => $pa->cost_center,
                    'sum_hour' => null,
                    'sum_night' => null,
                    'festive' => null,
                    'price_hourly' => $pa->st_price?((int)$pa->st_price):$response[$pa->industry_id]['price_hour'],
                    'deficit' => 0,
                    'deduction' => 0,
                    'bonus' => 0,
                    'bonus_presence' => 0,
                    'company_price' => $response[$pa->industry_id]['price_company_hourly'],
                    'gross_student' => null,
                    'net' => null,
                    'comment' => null,
                    'month' => $month,
                    'position' => $pa->position,
                    'recruiter' => $pa->recruiter,
                    'exited' => optional($pa)->exited,

                ];
            }
        }
        return response()->json(['data' => $response]);

    }

    public function costCenter(Request $request)
    {
        return response()->json(['data' => Industry::whereNotNull('cost_center')->groupBy('cost_center')->get()->pluck('cost_center', 'cost_center')]);
    }

    public function saveAll(Request $request)
    {
        DB::beginTransaction();
        foreach ($request->all() as $k => $v) {
            foreach ($v['students'] as $student) {

                $fA = FixingAmrest::where("student_id", $student['student_id'])->where("month", $student['month'])->where("cost_center", $student['cost_center'])->first();
                if (!$fA) {
                    $fA = new FixingAmrest();
                }
                $fA->fillAndSave($student);
                $studentAmrest = StudentAmrest::where("student_id", $student['student_id'])->first();
                if (!$studentAmrest) {
                    $studentAmrest = new StudentAmrest();
                }
                $studentAmrest->price = $student['price_hourly'];
                $studentAmrest->fillAndSave($student);
            }
        }
        DB::commit();
    }

    public function getCostCenter(Request $request, $month)
    {
        $month = str_replace('-', '', $month);

        $db = Industry::whereNotNull('company_industries.cost_center')
            ->leftJoin(DB::raw("(	SELECT cost_center,count(*) as fixing,month from fixing_amrest WHERE	month = '$month'GROUP BY cost_center) AS `f`"), 'f.cost_center', 'company_industries.cost_center')
            ->groupBy('company_industries.cost_center')
            ->select(DB::raw("company_industries.cost_center,f.fixing as fixing"))
            ->get();

        return response()->json(['data' => $db]);
    }
    public function searchAmrestStudent(Request $request,$month,$costcenter){
        $month = str_replace('-', '', $month);
        $industry = Industry::where("cost_center","=",$costcenter)->first();
        $response = [];

        $projectStudents = ProjectStudent::leftJoin('projects', 'projects.id', 'project_id')->leftJoin("students", 'students.id', 'project_students.student_id')
            ->leftJoin('student_amrest', 'student_amrest.student_id', 'students.id')
            ->leftJoin('company_industries', 'company_industries.id', 'projects.industry_id')
            ->select(DB::raw('projects.*,amrest_id,company_industries.cost_center,company_industries.recruiter,students.name as student_name,students.student_number as student_number,student_amrest.position as position,company_industries.recruiter as recruiter, students.tax_number as tax_number,students.id as student_id'))
            ->where('project_students.status_id', 2)
            ->whereIn("company_industries.company_id", [863,1047]);
        $search = $request->input("search");
        $projectStudents= $projectStudents->where(function($query)use ($search) {
            return $query->where('students.name','LIKE','%'.$search.'%')
                ->orWhere('students.tax_number','LIKE','%'.$search.'%')
                ->orWhere('students.student_number','LIKE','%'.$search.'%');
        }
        );
        $projectStudents= $projectStudents->limit(10)->get();
        foreach ($projectStudents as $pa) {

            $response[$pa->student_id] = [
                'student_name' => $pa->student_name,
                'student_number' => $pa->student_number,
                'student_id' => $pa->student_id,
                'amrest_id' => $pa->amrest_id,
                'tax_number' => $pa->tax_number,
                'industry_id' => $industry->id,
                'cost_center' => $costcenter,
                'sum_hour' => null,
                'sum_night' => null,
                'festive' => null,
                'price_hourly' => $industry->price_hour,
                'deficit' => 0,
                'deduction' => 0,
                'bonus' => 0,
                'bonus_presence' => 0,
                'company_price' => $industry->price_company_hourly,
                'gross_student' => null,
                'net' => null,
                'comment' => null,
                'month' => $month,
                'position' => $pa->position,
                'recruiter' => $pa->recruiter,
                'exited' => optional($pa)->exited,

            ];
        }
        return response()->json(['data'=>$response]);
    }
    public function exportAmrestAll(Request $request,$month)
    {   $m =$month;
        $month = str_replace('-', '', $month);
        return Excel::download(new AmrestExport($month), 'amrest-export-'.$month.'.xlsx');
    }
    public function moveToFixing(Request $request,$month){
        $m =$month;
        $month = str_replace('-', '', $month);
        $fixingAmrest = FixingAmrest::where('month',$month)->leftJoin('company_industries','company_industries.id','=','industry_id')->select(DB::raw("fixing_amrest.*,company_id"))->get();
        DB::beginTransaction();
        foreach ($fixingAmrest as $fa){
            Fixing::where('month',$month)->where("student_id",$fa->student_id)->where('company_id',$fa->company_id)->where("industry_id",$fa->industry_id)->forceDelete();


        }
        foreach ($fixingAmrest as $fa){
            if($fa->gross_student >0) {
                $fixing = Fixing::where('month', $month)->where("student_id", $fa->student_id)->where('company_id', $fa->company_id)->where("industry_id", $fa->industry_id)->first();
                if (!$fixing) {
                    $fixing = new Fixing();
                }

                $fixing->student_id = $fa->student_id;
                $fixing->company_id = $fa->company_id;
                $fixing->industry_id = $fa->industry_id;
                $fixing->month = $fa->month;
                $fixing->status = 0;
                $fixing->date = date('Y-m-01', strtotime($m));

                $fixing->fix += $fa->gross_student;
                $fixing->hour += $fa->sum_hour?$fa->sum_hour:0;
                $fixing->bonus_hour =0;
                $fixing->bonus_type =1;
                $fixing->bonus_multiplier =1;
                $fixing->bonus_multiplier_company =1;
                $fixing->price_hour =0;
                $fixing->price_company_hourly =0;
                $fixing->student_price += $fa->gross_student;
                $fixing->company_price += ($fa->net - $fa->gross_student);
                $fixing->fix_company_multiplier = 1;
                $fixing->user_id = \Auth::user()->id;
                $fixing->divisor = 0;
                $fixing->multiplier = 0;
                $fixing->save();
            }

        }
        DB::commit();
        return response()->json(["data"=>"ok"]);
    }
}
