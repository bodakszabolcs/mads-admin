<?php

namespace Modules\Fixing\Http\Controllers;

use App\Console\Commands\FixingPresents;
use App\Exports\BaseArrayExport;
use App\Helpers\ConstansHelper;
use App\Helpers\PdfHelper;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Industry;
use Modules\Fixing\Entities\Fixing;
use Illuminate\Http\Request;
use Modules\Fixing\Transformers\FixingPresensResource;
use Modules\Fixing\Transformers\FixingPresensStudentResource;
use Modules\MonthlyOrders\Entities\MonthlyOrders;
use Modules\Payroll\Entities\Payroll;
use Modules\Project\Entities\Project;
use Modules\Project\Entities\ProjectStudent;
use Modules\Student\Entities\Student;
use Modules\StudentPresent\Entities\StudentPresent;
use Modules\User\Entities\User;

class FixingSummationController extends Fixing
{
    private $headings = [
        'Diák', 'E-mail', 'Adószám', 'Cég', 'Tevékenyslég/Telephely', 'Dátum', 'Kezdés', 'Végzés', 'Óraszám', 'Fix/Bónusz'
    ];
    private $headingYearly = [
        'Diák', 'E-mail', 'Adószám', 'Cég','Cég adószám', 'Tevékenyslég/Telephely', 'Hónap', 'Óra', 'Fix', 'Összesen','PM','Sales',
    ];

    public function fixingInvoice(Request $request, $month, $company)
    {
        $response = [];
        $listIndustry = Fixing::where('month', '=', str_replace('-', '', $month))->groupBy('fixings.company_id')
            ->leftJoin('companies', 'companies.id', '=', 'fixings.company_id')->disableCache()
            ->leftJoin('company_industries', 'company_industries.id', '=', 'fixings.industry_id')
            ->leftJoin('company_sites', 'company_sites.id', '=', 'company_industries.site_id')
            ->select(DB::raw("companies.name as company_name,company_industries.name as  industry_name,company_industries.teaor as teaor,fixings.company_id as company_id, fixings.industry_id as industry_id,company_sites.name as site_name,company_sites.id as site_id,
	         sum(IF(bonus_type=1,bonus_hour,0)) as night_hour,
	         sum(IF(bonus_type=2,bonus_hour,0)) as saturday_hour,
	         sum(IF(bonus_type=3,bonus_hour,0)) as sunday_hour,
	         sum(IF(bonus_type=4,bonus_hour,0)) as festive_hour,
	         sum(IF(bonus_type=1,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as night_hour_price,
	         sum(IF(bonus_type=2,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as saturday_hour_price,
	         sum(IF(bonus_type=3,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as sunday_hour_price,
	         sum(IF(bonus_type=4,bonus_hour*fixings.price_hour*bonus_multiplier,0)) as festive_hour_price,
	         sum(IF(bonus_type=1,bonus_hour*fixings.price_hour*bonus_multiplier_company,0)) as night_hour_price_company,
	         sum(IF(bonus_type=2,bonus_hour*fixings.price_hour*bonus_multiplier_company,0)) as saturday_hour_price_company,
	         sum(IF(bonus_type=3,bonus_hour*fixings.price_hour*bonus_multiplier_company,0)) as sunday_hour_price_company,
	         sum(IF(bonus_type=4,bonus_hour*fixings.price_hour*bonus_multiplier_company,0)) as festive_hour_price_company,
	         sum(bonus_hour) as sum_bonus_hour,
	         sum(hour+bonus_hour) as sum_hour,
	         payment_deadline,
	         sum(fix) as sum_fix,
	         AVG(fixings.price_hour) as avg_price_hour,
	         AVG(fixings.price_company_hourly) as avg_price_company,
	         sum(student_price) as  sum_price,
	         invoice_status,
	         sum(company_price) as sum_company"))->disableCache()->orderBy('companies.name', 'asc');
        $listIndustry = $listIndustry->groupBy('industry_id');
        $response['industry'] = [];
        foreach ($listIndustry as $l) {
            $response['industry'][$l->industry_id] = [
                'name' => $l->industry_name,
                'teaor' => $l->teaor,
                'items' => []
            ];
            $response['industry'][$l->industry_id]['items']['day'] = [
                'hour' => $l->night_hour,
                'fix' => '',
                'price' => $l->night_hour_price,
                'sum' => $l->avg_price_hour + $l->avg_price_company * $l->hour,
            ];
            $response['industry'][$l->industry_id]['items']['night'] = [
                'hour' => $l->hour,
                'fix' => $l->sum_fix,
                'price' => $l->avg_price_hour + $l->avg_price_company,
                'sum' => $l->avg_price_hour + $l->avg_price_company * $l->hour,
            ];
            $response['industry'][$l->industry_id]['items']['saturday'] = [];
            $response['industry'][$l->industry_id]['items']['sunday'] = [];
            $response['industry'][$l->industry_id]['items']['sum'] = [];
        }
    }

    public function fixingSummationList(Request $request, $month)
    {
        $group = "industry";
        $list = DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->groupBy('company_id')
            ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,
	        
	         sum(saturday_hour) as saturday_hour,
	         sum(sunday_hour) as sunday_hour,
	         sum(festive_hour) as festive_hour,
	         sum(fix_company) as sum_fix,
	       
	         sum(hour) as sum_hour,
	         CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
	         CEILING(SUM(sum_net)) as sum_net,
	         CEILING(SUM(sum_net)*1.27) as sum_gross,
	         
	         sum(night_hour) as night_hour,
	         CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
	         CEILING(Sum( night_sum_net))as night_sum_net,
	         CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,
	         
	         sum(festive_hour) as festive_hour,
	         CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
	         CEILING(Sum( festive_sum_net))as festive_sum_net,
	         CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,
	         
	         sum(sunday_hour) as sunday_hour,
	         CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
	         CEILING(Sum( sunday_sum_net))as sunday_sum_net,
	         CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,
	         
	         sum(saturday_hour) as saturday_hour,
	         CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
	         CEILING(Sum( saturday_sum_net))as saturday_sum_net,
	         CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
	         payment_deadline,
	         sum(IF(invoice_status is null or invoice_status=0,1,0)) as  invoice_status,
	         sum(company_price) as amrest_gross,
	       
	         CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,status"))->orderBy('company_name', 'asc');
        if ($request->input('search') && !empty($request->input('search'))) {
            $list = $list->where(function ($query) use ($request) {
                $query->where('company_name', 'LIKE', '%' . $request->input('search') . '%')->orWhere('industry_name', 'LIKE', '%' . $request->input('search') . '%');
            });
        }

        if ($request->input('status', '') !== '' && $request->input('status', '') != "null" && $request->input('status', '') != null) {
            $list = $list->where('status', '=', $request->input('status'));
        }
        if ($request->input('invoice_status', '') !== '' && $request->input('invoice_status', '') != "null"  && $request->input('invoice_status', '') != null) {

            if($request->input('invoice_status', '') ==0) {
                $list = $list->having('invoice_status','>','0');

            }else{
                $list = $list->having('invoice_status', '=', '0');
            }

        }
        if ($request->input('group', 'industry') != 'industry' && $request->input('group', '') != "null") {
            $list = $list->groupBy('site_id');
            $group = "site";
        } else {
            $list = $list->groupBy(DB::raw('LOWER(teaor)'));
        }
        $list = $list->get();
        $response = [];
        foreach ($list as $item) {

            if (!isset($response[$item->company_name.$item->company_id])) {

                $response[$item->company_name.$item->company_id] = [
                    "company_name" => $item->company_name,
                    "payment_deadline" => $item->payment_deadline,
                    "company_id" => $item->company_id,
                    "invoice_status" => $item->invoice_status,
                    "status"=>$item->status,
                    "sum_hour" => 0, "night_hour" => 0, "saturday_hour" => 0, "sunday_hour" => 0, "festive_hour" => 0, "sum_student" => 0, "sum_company" => 0, "sum_fix" => 0,"amrest_gross" => 0,"hour" => 0,
                    "night_hour_price" => 0, "saturday_hour_price" => 0, "sunday_hour_price" => 0, "festive_hour_price" => 0, "items" => []
                ];

            }
            if ($group == "industry") {
                if (!isset($response[$item->company_name.$item->company_id]['items'][$item->teaor])) {
                    $response[$item->company_name.$item->company_id]['items'][$item->teaor] = [
                        "name" => $item->teaor,
                        "id" => $item->industry_id, "invoice_status" => $item->invoice_status,
                        "sum_hour" => 0,
                        "hour" => 0,
                        "night_hour" => 0,
                        "saturday_hour" => 0,
                        "sunday_hour" => 0,
                        "festive_hour" => 0,
                        "sum_student" => 0,
                        "sum_company" => 0,
                        "sum_fix" => 0,
                        "amrest_gross" => 0,
                        "night_hour_price" => 0,
                        "saturday_hour_price" => 0,
                        "sunday_hour_price" => 0,
                        "festive_hour_price" => 0,
                    ];
                }
            } else {
                if (!isset($response[$item->company_name.$item->company_id]['items'][$item->site_id])) {
                    $response[$item->company_name.$item->company_id]['items'][$item->site_id] = [
                        "name" => $item->site_name,
                        "id" => $item->site_id,
                        "invoice_status" => $item->invoice_status,
                        "sum_hour" => 0,
                        "hour" => 0,
                        "night_hour" => 0,
                        "saturday_hour" => 0,
                        "sunday_hour" => 0,
                        "festive_hour" => 0,
                        "sum_student" => 0,
                        "sum_company" => 0,
                        "sum_fix" => 0,
                        "amrest_gross" => 0,
                        "night_hour_price" => 0,
                        "saturday_hour_price" => 0,
                        "sunday_hour_price" => 0,
                        "festive_hour_price" => 0,
                    ];
                }
            }
            $response[$item->company_name.$item->company_id]["sum_hour"] += $item->festive_hour+$item->night_hour+$item->sunday_hour+$item->saturday_hour+$item->sum_hour;
            $response[$item->company_name.$item->company_id]["sum_student"] += $item->festive_sum_net+$item->night_sum_net+$item->sunday_sum_net+$item->saturday_sum_net+$item->sum_net;
            $response[$item->company_name.$item->company_id]["night_hour"] += $item->night_hour;
            $response[$item->company_name.$item->company_id]["hour"] += $item->sum_hour;
            $response[$item->company_name.$item->company_id]["saturday_hour"] += $item->saturday_hour;
            $response[$item->company_name.$item->company_id]["sunday_hour"] += $item->sunday_hour;
            $response[$item->company_name.$item->company_id]["festive_hour"] += $item->festive_hour;
            $response[$item->company_name.$item->company_id]["sum_fix"] += $item->sum_fix;
            $response[$item->company_name.$item->company_id]["amrest_gross"] += $item->amrest_gross;
            if($item->company_id==863  || $item->company_id==1047){
                $response[$item->company_name.$item->company_id]["sum_fix"]+=$item->amrest_gross;
            }
            $response[$item->company_name.$item->company_id]["night_hour_price"] += $item->night_sum_net;
            $response[$item->company_name.$item->company_id]["saturday_hour_price"] += $item->saturday_sum_net;
            $response[$item->company_name.$item->company_id]["sunday_hour_price"] += $item->sunday_sum_net;
            $response[$item->company_name.$item->company_id]["festive_hour_price"] += $item->festive_sum_net;
            if ($group == 'industry') {
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sum_hour"] += $item->festive_hour+$item->night_hour+$item->sunday_hour+$item->saturday_hour+$item->sum_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sum_student"] += $item->festive_sum_net+$item->night_sum_net+$item->sunday_sum_net+$item->saturday_sum_net+$item->sum_net;;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["night_hour"] += $item->night_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["hour"] += $item->sum_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["saturday_hour"] += $item->saturday_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sunday_hour"] += $item->sunday_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["festive_hour"] += $item->festive_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sum_fix"] += $item->sum_fix;
                if($item->company_id==863  || $item->company_id==1047){
                    $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sum_fix"]+=$item->amrest_gross;
                }
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["amrest_gross"] += $item->amrest_gross;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["night_hour_price"] += $item->night_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["saturday_hour_price"] += $item->saturday_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["sunday_hour_price"] += $item->sunday_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->teaor]["festive_hour_price"] += $item->festive_sum_hourly;
            } else {
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sum_hour"] += $item->festive_hour+$item->night_hour+$item->sunday_hour+$item->saturday_hour+$item->sum_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sum_student"] += $item->festive_sum_net+$item->night_sum_net+$item->sunday_sum_net+$item->saturday_sum_net+$item->sum_net;;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["night_hour"] += $item->night_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["hour"] += $item->sum_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["saturday_hour"] += $item->saturday_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sunday_hour"] += $item->sunday_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["festive_hour"] += $item->festive_hour;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sum_fix"] += $item->sum_fix;
                if($item->company_id==863  || $item->company_id==1047){
                    $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sum_fix"]+=$item->amrest_gross;
                }
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["amrest_gross"] += $item->amrest_gross;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["night_hour_price"] += $item->night_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["saturday_hour_price"] += $item->saturday_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["sunday_hour_price"] += $item->sunday_sum_hourly;
                $response[$item->company_name.$item->company_id]['items'][$item->site_id]["festive_hour_price"] += $item->festive_sum_hourly;
            }

        }

        return response()->json($response, 200);
    }

    public function createInvoice(Request $request, $month, $company, $industry)
    {
        $ind = Industry::where("id",$industry)->first();
        $fixing = Fixing::where('month', '=', str_replace('-', '', $month))->where('fixings.company_id', '=', $company)
            ->leftJoin('company_industries','company_industries.id','=','industry_id')->where('teaor','=', $ind->teaor)->update(['invoice_status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);

        $allHasInvoice = Fixing::where('company_id',$company)->where('month',str_replace('-', '', $month))->where('invoice_status','=',0)->count();
        if($allHasInvoice==0){
          $monthlyorder = MonthlyOrders::where('company_id',$company)->where('month','=',$month)->first();
          if($monthlyorder) {
              $monthlyorder->status = 5;
              $monthlyorder->save();
          }
        }

        return $this->fixingSummationList($request, $month);
    }
    public function studentWorkerList(Request $request,  $month){
        if(!$month){
            $month = date('Ym');
        }else{
            $month = str_replace('-','',$month);
        }
        $resp=[];
        $list =ProjectStudent::leftJoin('projects','projects.id','=','project_id')->leftJoin('companies','companies.id','=','projects.company_id')->leftJoin('company_industries','company_industries.id','industry_id')
            ->leftJoin('users','users.id','=','projects.user_id')->where('status_id','=',2)->whereNull('projects.deleted_at')
            ->leftJoin('students','students.id','=','student_id')->whereNull('students.deleted_at')->whereNotNull('students.id')
            ->select(DB::raw("students.name as student_name, student_number,companies.name as company_name,company_industries.name as industry_name,students.id as student_id,projects.company_id as company_id,concat(users.firstname,' ',users.lastname) as pm,industry_id "))->get();
        foreach ($list as $l){
            $resp[$l->student_id.'-'.$l->company_id.'-'.$l->industry_id]=[
                'student_name'=>$l->student_name,
                'student_number'=>$l->student_number,
                'company_name' => $l->company_name,
                'industry_name'=>$l->industry_name,
                'pm'=>$l->pm,
                'fixing'=>false
            ];
        }
        $fixing = Fixing::where('month',$month)->groupBy('student_id')->groupBy('company_id')->groupBy('industry_id')->get();
        foreach ($fixing as $f){
            if(isset($resp[$f->student_id.'-'.$f->company_id.'-'.$f->industry_id])) {
                $resp[$f->student_id . '-' . $f->company_id . '-' . $f->industry_id]['fixing'] = true;
            }
        }
        return response()->json(['data'=>$resp]);
    }
    public function invoiceView(Request $request,  $month,$company)
    {
        $list = DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->where("company_id", '=', $company)
            ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,
	        
	         sum(saturday_hour) as saturday_hour,
	         sum(sunday_hour) as sunday_hour,
	         sum(festive_hour) as festive_hour,
	         sum(fix_company) as sum_fix,
	         
	         
	         sum(hour) as sum_hour,
	         CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
	         CEILING(SUM(sum_net)) as sum_net,
	         CEILING(SUM(sum_net)*1.27) as sum_gross,
	         
	         sum(night_hour) as night_hour,
	         CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
	         CEILING(Sum( night_sum_net))as night_sum_net,
	         CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,
	         
	         sum(festive_hour) as festive_hour,
	         CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
	         CEILING(Sum( festive_sum_net))as festive_sum_net,
	         CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,
	         
	         sum(sunday_hour) as sunday_hour,
	         CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
	         CEILING(Sum( sunday_sum_net))as sunday_sum_net,
	         CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,
	         
	         sum(saturday_hour) as saturday_hour,
	        CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
	         CEILING(Sum( saturday_sum_net))as saturday_sum_net,
	          CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross
	         
	    
	         
	        
	       
	       
	   "));
        $list = $list->groupBy(DB::raw('LOWER(teaor)'))->groupBy('site_id');
        $list = $list->get();
        $response = [];
        foreach ($list as $item) {



                $response[$item->site_id][$item->teaor] = [
                    "company_name" => $item->company_name,
                    "teaor" => $item->teaor,
                    "site" => $item->site_name,
                    "company_id" => $item->company_id,
                    "sum_hour" => $item->sum_hour,
                    "sum_fix" => $item->sum_fix,
                    "sum_hourly" =>$item->sum_hourly,
                    "sum_net" =>$item->sum_net,
                    "sum_gross" =>$item->sum_gross,

                    "night_hour" => $item->night_hour,
                    "night_sum_hourly" =>$item->night_sum_hourly,
                    "night_sum_net" =>$item->night_sum_net,
                    "night_sum_gross" =>$item->night_sum_gross,

                    "festive_hour" => $item->festive_hour,
                    "festive_sum_hourly" =>$item->festive_sum_hourly,
                    "festive_sum_net" =>$item->festive_sum_net,
                    "festive_sum_gross" =>$item->festive_sum_gross,

                    "sunday_hour" => $item->sunday_hour,
                    "sunday_sum_hourly" =>$item->sunday_sum_hourly,
                    "sunday_sum_net" =>$item->sunday_sum_net,
                    "sunday_sum_gross" =>$item->sunday_sum_gross,

                    "saturday_hour" => $item->saturday_hour,
                    "saturday_sum_hourly" =>$item->saturday_sum_hourly,
                    "saturday_sum_net" =>$item->saturday_sum_net,
                    "saturday_sum_gross" =>$item->saturday_sum_gross,


                    "site_name"=>$item->site_name,


                ];



        }
        return response()->json(['data'=>$response],200);
    }

    public function stornoInvoice(Request $request, $month, $company, $industry)
    {   $ind = Industry::where("id",$industry)->first();
        $fixing = Fixing::where('month', '=', str_replace('-', '', $month))->where('fixings.company_id', '=', $company)
            ->leftJoin('company_industries','company_industries.id','=','industry_id')->where('teaor','=', $ind->teaor);
        $fixing->update(['invoice_status' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
        return $this->fixingSummationList($request, $month);
    }

    public function excel(Request $request, $month, $company, $industry)
    {
        $group = "industry";
        $list = Fixing::where('month', '=', str_replace('-', '', $month))
            ->where('fixings.company_id', '=', $company)->leftJoin('companies', 'companies.id', '=', 'fixings.company_id')->where('student_price', '>', 0)
            ->leftJoin('company_industries', 'company_industries.id', '=', 'fixings.industry_id')->leftJoin('company_sites', 'company_sites.id', '=', 'company_industries.site_id')
            ->leftJoin('students', 'students.id', 'student_id')
            ->select(DB::raw('students.name as student_name,tax_number, email,date,start,end, fix,hour+bonus_hour as  hour ,companies.name as company_name,company_industries.name as industry_name,company_sites.name as site_name'));

        if ($request->input('group', 'industry') != 'industry' && $request->input('group', '') != null) {
            $group = "site";
            $list = $list->where('site_id', '=', $industry);
        } else {
            $ind =Industry::where("id",$industry)->first();
            $list = $list->where(DB::raw('LOWER(company_industries.teaor)'), '=',strtolower($ind->teaor));
            //$list = $list->where('industry_id', '=', $industry);
        }
        $excel = [];
        $title = "";
        foreach ($list->orderBy('student_name')->get() as $row) {
            $title = $row->company_name;
            $excel[] = [
                $row->student_name, $row->email, $row->tax_number, $row->company_name, ($group == 'industry') ? $row->industry_name : $row->site_name, $row->date, $row->start, $row->end, $row->hour, $row->fix
            ];
        }
        $export = new BaseArrayExport($this->headings, "Óraszám összesítő", $excel);

        return $export->download(Str::slug($title . '-' . $month . '-Óraszám összesítő') . '.xlsx');

    }

    public function excelYear(Request $request, $month)
    {

        $year = explode('-', $month);
        $list = Fixing::where('month', 'LIKE', $year[0] . '%')->leftJoin('companies', 'companies.id', '=', 'fixings.company_id')->leftJoin('company_industries', 'company_industries.id', '=', 'fixings.industry_id')
            ->leftJoin('students', 'students.id', 'student_id')
            ->leftJoin('users','users.id','companies.user_id')
            ->select(DB::raw("students.name as student_name,tax_number, students.email,month, sum(fix)as fix,sum(student_price) as  sum_student ,companies.name as company_name,companies.tax as company_tax,company_industries.name as industry_name,concat(users.lastname,' ',users.firstname) as pm,importer"))
            ->groupBy('month')->groupBy('student_id')->where('student_price', '>', 0)->groupBy('fixings.industry_id');
        $excel = [];
        foreach ($list->orderBy('month')->orderBy('student_name')->get() as $row) {
            $excel[] = [
                $row->student_name, $row->email, $row->tax_number, $row->company_name,$row->company_tax, $row->industry_name, $row->month, $row->sum_student - $row->fix, $row->fix, $row->sum_student,$row->pm,$row->importer
            ];
        }
        $export = new BaseArrayExport($this->headingYearly, "Óraszám összesítő", $excel);

        return $export->download(Str::slug('Éves-Óraszám összesítő ' . $year[0]) . '.xlsx');

    }

    public function completionForm(Request $request, $type, $month, $company_id)
    {
        $company = Company::where('id', $company_id)->first();

        return PdfHelper::createPdf(view('company.teljesitesi', ['company' => $company, 'month' => $month, 'type' => $type]),null,null,null,[],$company->name.$month.'.pdf');
    }
    public function completionFormExcel(Request $request, $type, $month, $company_id)
    {
        $company = Company::where('id', $company_id)->first();
        $headers=[];
        $list = DB::table("fixing_view")->where('month', '=', str_replace('-', '', $month))->groupBy('company_id')
            ->select(DB::raw(" company_name, industry_name, company_id, teaor,  industry_id,site_name, site_id,

              sum(saturday_hour) as saturday_hour,
              sum(sunday_hour) as sunday_hour,
              sum(festive_hour) as festive_hour,
              sum(fix_company) as sum_fix,


              sum(hour) as sum_hour,
              CEILING(SUM(sum_net)) /sum(hour) as sum_hourly,
              CEILING(SUM(sum_net)) as sum_net,
              CEILING(SUM(sum_net)*1.27) as sum_gross,

              sum(night_hour) as night_hour,
              CEILING(Sum( night_sum_net))/sum(night_hour) as night_sum_hourly,
              CEILING(Sum( night_sum_net))as night_sum_net,
              CEILING(SUM(night_sum_net)*1.27)as night_sum_gross,

              sum(festive_hour) as festive_hour,
              CEILING(Sum( festive_sum_net))/sum(festive_hour) as festive_sum_hourly,
              CEILING(Sum( festive_sum_net))as festive_sum_net,
              CEILING(SUM(festive_sum_net)*1.27)as festive_sum_gross,

              sum(sunday_hour) as sunday_hour,
              CEILING(Sum( sunday_sum_net))/sum(sunday_hour) as sunday_sum_hourly,
              CEILING(Sum( sunday_sum_net))as sunday_sum_net,
              CEILING(SUM(sunday_sum_net)*1.27)as sunday_sum_gross,

              sum(saturday_hour) as saturday_hour,
             CEILING(Sum( saturday_sum_net))/sum(saturday_hour) as saturday_sum_hourly,
              CEILING(Sum( saturday_sum_net))as saturday_sum_net,
               CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross,
              payment_deadline,
              invoice_status,
                student_name,
	         teaor,
              CEILING(SUM(saturday_sum_net)*1.27)as saturday_sum_gross"))->orderBy('company_name', 'asc')->where('company_id',$company->id);
        if($type=='student'){
            $list =$list->groupBy('student_id')->groupBy(DB::raw('LOwER(teaor)'))->get();
            $headers =['Név','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->student_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];

        }
        if($type=='industry'){
            $list =$list->groupBy(DB::raw('LOWER(teaor)'))->get();
            $headers =['Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        if($type=='site'){
            $list =$list->groupBy('site_id')->groupBy(DB::raw('LOWER(teaor)'))->get();
            $headers =['Telephely','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->site_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        if($type=='uuid'){
            $list =$list->groupBy('industry_id')->get();
            $headers =['Megnevezés','Tevékenység','Óra nappali','Óra éjszaka','Óra szombat','Óra vasárnap','Óra ünnep','Össz óra','Bér','Fix bér','Összesen'];
            $data =[];
            $sum1=0;
            $sum2=0;
            $sum3=0;
            $sum4=0;
            $sum5=0;
            $sum6=0;
            $sum7=0;
            $sum8=0;
            $sum9=0;
            foreach($list as $student) {
                if ($student->sum_net + $student->night_sum_net + $student->sunday_sum_net + $student->saturday_sum_net + $student->festive_sum_net + $student->sum_fix > 0){
                    $data[]=[
                        $student->industry_name,
                        $student->teaor,
                        $student->sum_hour,
                        $student->night_hour,
                        $student->saturday_hour,
                        $student->sunday_hour,
                        $student->festive_hour,
                        $student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net,
                        $student->sum_fix,
                        $student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix


                    ];
                    $sum1+=$student->sum_hour;
                    $sum2+=$student->night_hour;
                    $sum3+=$student->saturday_hour;
                    $sum4+=$student->sunday_hour;
                    $sum5+=$student->festive_hour;
                    $sum6+=$student->sum_hour+$student->night_hour+$student->sunday_hour+$student->festive_hour+$student->saturday_hour;
                    $sum7+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net;
                    $sum8+=$student->sum_fix;
                    $sum9+=$student->sum_net+$student->night_sum_net+$student->sunday_sum_net+$student->saturday_sum_net+$student->festive_sum_net+$student->sum_fix;

                }
            }
            $data[]=[
                'Mindösszesen',
                '',
                $sum1,
                $sum2,
                $sum3,
                $sum4,$sum5,$sum6,$sum7,
                $sum8,$sum9


            ];
        }
        return Excel::download(new BaseArrayExport($headers,'MADS-teljesitesi-'.date('Y-m-d'),$data),'MADS-teljesitesi-'.date('Y-m-d').'.xlsx');
    }

    public function presenceCompany(Request $request, $month)
    {
        $data = StudentPresent::where('date', 'LIKE', '%' . $month . '%')->leftJoin('companies', 'companies.id', '=', 'student_presents.company_id')
            ->leftJoin('company_industries', 'company_industries.id', '=', 'student_presents.industry_id')
            ->leftJoin(DB::raw("(select industry_id as pindustry, count(*) as student_count from project_students left join projects on projects.id = project_id  where status_id=2 group by projects.industry_id ) as pstudent"),"pstudent.pindustry", "=", "industry_id")
            ->groupBy('industry_id')->orderBy('companies.name')->orderBy('company_industries.name')
            ->select(DB::raw("concat(companies.name,'/',company_industries.name) as name,count(Distinct student_id) as student,count(*) as sum_line,online_presence, sum(hour) as sum_hour,industry_id,pstudent.student_count, count(*) as cnt, sum(finalized ) as final, sum(if(ISNULL(student_presents.approved), 1,0)) as appr"));
        if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {
            $data= $data->where(function ($query) {
                $query->where('companies.user_id', '=', \Auth::id());
            });
        }

        $data= $data->get();



        return FixingPresensResource::collection($data);

    }

    public function presenceIndustry(Request $request, $month, $industry)
    {
        $data["company"] = Company::leftJoin("company_industries","companies.id","=","company_industries.company_id")->where("company_industries.id",$industry)->select("companies.name","company_industries.name as industry_name")->first();
        $data['has_presence'] =StudentPresent::where('date', 'LIKE', '%' . $month . '%')->where('industry_id', $industry)->leftJoin('students', 'students.id', '=', 'student_presents.student_id')
            ->groupBy('student_id')->orderBy('students.name')->select(DB::raw("students.name as name,student_id,tax_number, sum(hour) as sum_hour,students.student_number as student_number,industry_id,count(*) as count,sum(finalized) as finalized, sum(if(ISNULL(student_presents.approved), 1,0)) as appr "))->get();
        $ids=[];
        foreach ($data['has_presence'] as $pr){
            $ids[$pr["student_id"]] =$pr["student_id"];
        }
        $data['has_presence'] = FixingPresensStudentResource::collection($data['has_presence']);
        $data['not_has_presence'] =ProjectStudent::where("status_id","2")->leftJoin('students', 'students.id', '=', 'student_id')->whereNotIn("student_id",$ids)
            ->leftJoin("projects","projects.id","=","project_id")->where("industry_id",$industry)
            ->select(DB::raw("students.name as name,student_id,tax_number,students.student_number"))->get();
        return response()->json(["data"=>$data]);
    }
    public function getStudentPresents(Request $request,$student,$industry,$month){

        $projectIds = ProjectStudent::where('student_id',$student)->where('status_id',2)->groupBy('project_id')->get()->pluck('project_id','project_id');
        if(!$projectIds){
            return response()->json([],200);
        }
        $projects = Project::where('company_industries.id',$industry)
            ->select(DB::raw("projects.*,companies.name as company_name, company_industries.name as industry_name"))
            ->leftJoin('companies','companies.id','=','company_id')->leftJoin('company_industries','company_industries.id','=','industry_id')->groupBy('industry_id')->get();
        if(!$projects){
            return response()->json([],200);
        }
        $studentDb = Student::where("id",$student)->first();
        $response = [];
        foreach ($projects as $p){
            $industry = Industry::where('id',$p->industry_id)->first();
            $fixings= DB::table('fixings')->where('industry_id','=',$industry->id)->where('student_id',$student)->where('month',date('Ym',strtotime($month.'-01')))
                ->select(DB::raw("sum(student_price) as sum_price"))->first();

            $response[]=[
                'company'=>$p->company_name,
                'industry'=>$p->industry_name,
                'company_id'=>$p->company_id,
                'industry_id'=>$p->industry_id,
                'industry_has_break'=>$industry->break,
                'price_hourly'=>$industry->price_hour,
                'mads_all'=>$this->calcNet($studentDb,date('Ym',strtotime($month.'-01')),$fixings->sum_price),
                'presents'=> $this->generateStudentPresents($student,$p->industry_id,$month)
            ];
        }
        return response()->json($response,200);

    }
    function calcNet($student,$month,$price){
        if(date("Y-m-d",strtotime($student->birth_date."+ 25 year"))<date("Y-m-d",strtotime($month.'01')) || $student->afa)
        {
            return $price*0.85;
        }
        if($price> Payroll::$underAgeLimit){
            return Payroll::$underAgeLimit+(($price-Payroll::$underAgeLimit)*0.85);
        }
        return  $price;
    }
    public function generateStudentPresents($student,$industry,$month){
        $start = date('Y-m-d',strtotime($month.'-01'));
        $end = date('Y-m-t',strtotime($month.'-01'));
        $days = [];
        $presents = StudentPresent::where('industry_id',$industry)->where('student_id',$student)->where('date','>=',$start)->where('date','<=',$end)->get();
        while ($start <= $end){
            $days[$start] = [
                'name'=> ConstansHelper::formatDateShort($start),
                'date'=> $start,
                'start'=>'',
                'weekend'=> (date('N',strtotime($start)) >5)?true:false,
                'end'=>'',
                'break_from'=>'',
                'break_to'=>'',
                'hour'=>0,
                'approved'=> (date('Y-m-d') > date('Y-m-d',strtotime($end.' +1 day'))),
                'updated'=>'',
            ];
            $start = date('Y-m-d',strtotime($start.' +1 day'));
        }
        foreach ($presents as $pres){
            $days[$pres->date]['start']=$pres->start;
            $days[$pres->date]['end']=$pres->end;
            $days[$pres->date]['break_from']=$pres->break_from;
            $days[$pres->date]['break_to']=$pres->break_to;
            $days[$pres->date]['hour']=$pres->hour;
            $days[$pres->date]['updated']=$pres->updated_at;
            $days[$pres->date]['approved']=(date('Y-m-d') > date('Y-m-d',strtotime($end.' +1 day')))?1:$pres->approved;
        }

        return $days;
    }
    public function getFixingPresentsPdf(Request $request,$industry,$id,$month){
        //$month = date('Y').'-'.$month;
        $student = Student::where('id',$id)->first();
        $industry = Industry::where('id',$industry)->first();
        $company = Company::where('id',$industry->company_id)->first();
        $project= Project::where('industry_id',$industry->id)->first();
        $projectManager = User::where('id',$project->user_id)->first();
        $sIds = ProjectStudent::where('industry_id', $industry->id)->disableCache()->leftJoin('projects', 'projects.id', '=', 'project_id')->where('status_id', 2)->groupBy('student_id')->get()->pluck('student_id', 'student_id');


        $students = Student::whereIn('id', $sIds)->get();
        $date = date('Y', strtotime($month . '-01')) . '. ' . Arr::get(ConstansHelper::$moths, (int)explode('-', $month)[1]);
        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        foreach ($students as $s) {
            $presents = $this->generateFixingStudentPresents( $s->id,$industry->id, $month);
            PdfHelper::createPdf(view('student.jelenleti', ['student' => $s, 'projectManager' => $projectManager, 'month' => $date, 'presents' => $presents, 'task' => $industry->name, 'project' => $company->name]),
                null, null, '/tmp/' .$s->id . 'package.pdf',
                ['margin_top' => 10, 'margin_bottom' => 10]);


            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $s->id . 'package.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $s->id . 'package.pdf');
        }
        return $pdf->Output();

    }
    public function generateFixingStudentPresents($student,$industry,$month){
        $start = date('Y-m-d',strtotime($month.'-01'));
        $end = date('Y-m-t',strtotime($month.'-01'));
        $days = [];
        $presents = Fixing::where('industry_id',$industry)->where('student_id',$student)->where('date','>=',$start)->where('date','<=',$end)->get();

        while ($start <= $end){
            $days[$start] = [
                'name'=> ConstansHelper::formatDateShort($start),
                'date'=> $start,
                'start'=>'',
                'weekend'=> (date('N',strtotime($start)) >5)?true:false,
                'end'=>'',
                'break_from'=>'',
                'break_to'=>'',
                'hour'=>0,
                'approved'=> (date('Y-m-d') > date('Y-m-d',strtotime($end.' +1 day'))),
                'updated'=>'',
            ];
            $start = date('Y-m-d',strtotime($start.' +1 day'));
        }
        foreach ($presents as $pres){
            $days[$pres->date]['start']=$pres->start;
            $days[$pres->date]['end']=$pres->end;
            $days[$pres->date]['break_from']=$pres->break_from;
            $days[$pres->date]['break_to']=$pres->break_to;
            $days[$pres->date]['hour']=$pres->hour+$pres->bonus_hour;

        }

        return $days;
    }
    public function getStudentPresentPDF(Request $request,$student,$industry,$month){
        $student = Student::where('id',$student)->first();
        $industry = Industry::where('id',$industry)->first();
        $company = Company::where('id',$industry->company_id)->first();
        $project= Project::where('industry_id',$industry->id)->first();
        $projectManager = User::where('id',$project->user_id)->first();
        $presents = $this->generateStudentPresents($student->id,$industry->id,$month);

        $date = date('Y',strtotime($month.'-01')).'. '.Arr::get(ConstansHelper::$moths,(int)explode('-',$month)[1]);

        return PdfHelper::createPdf(view('student.jelenleti',['student'=>$student,'projectManager'=>$projectManager,'month'=>$date,'presents'=>$presents,'task'=>$industry->name,'project'=>$company->name]),null,null,null,['margin_top'=>10,'margin_bottom'=>10]);

    }
    public function getFixingPresentPDF(Request $request,$student,$industry,$month){
        $student = Student::where('id',$student)->first();
        $industry = Industry::where('id',$industry)->first();
        $company = Company::where('id',$industry->company_id)->first();
        $project= Project::where('industry_id',$industry->id)->first();
        $projectManager = User::where('id',$project->user_id)->first();
        $presents = $this->generateStudentPresents($student->id,$industry->id,$month);

        $date = date('Y',strtotime($month.'-01')).'. '.Arr::get(ConstansHelper::$moths,(int)explode('-',$month)[1]);

        return PdfHelper::createPdf(view('student.jelenleti',['student'=>$student,'projectManager'=>$projectManager,'month'=>$date,'presents'=>$presents,'task'=>$industry->name,'project'=>$company->name]),null,null,null,['margin_top'=>10,'margin_bottom'=>10]);

    }
    public function finalizePresents(Request $request,$industry,$month){
        $industry = Industry::where('id', $industry)->first();
        $fP = new FixingPresents();
        if($industry) {
            $presentsRows = StudentPresent::whereNull('finalized')->where('industry_id', $industry->id)->where('date','<=',date('Y-m-t',strtotime($month.'-01')))->get();

            foreach ($presentsRows as $row) {

                $fP->createFixing($industry, $row);
                $row->finalized = 1;
                $row->save();
            }
        }

    }
    public function finalizePresentsStudent(Request $request,$industry,$month,$student){
        $industry = Industry::where('id', $industry)->first();
        $fP = new FixingPresents();
        if($industry) {
            $presentsRows = StudentPresent::whereNull('finalized')->where('industry_id', $industry->id)->where('student_id','=',$student)->where('date','<=',date('Y-m-t',strtotime($month.'-01')))->get();

            foreach ($presentsRows as $row) {

                $fP->createFixing($industry, $row);
                $row->finalized = 1;
                $row->save();
            }
        }

    }
    public function banPresents(Request $request,$industry,$month){
        $industry = Industry::where('id', $industry)->first();
        $fP = new FixingPresents();
        if($industry) {
            $presentsRows = StudentPresent::whereNull('finalized')->where('industry_id', $industry->id)->where('date','<=',date('Y-m-t',strtotime($month.'-01')))->get();

            foreach ($presentsRows as $row) {

                $row->approved = null;
                $row->save();
            }
        }

    }
    public function banPresentsStudent(Request $request,$industry,$month,$student){
        $industry = Industry::where('id', $industry)->first();
        $fP = new FixingPresents();
        if($industry) {
            $presentsRows = StudentPresent::whereNull('finalized')->where('industry_id', $industry->id)->where('student_id','=',$student)->where('date','<=',date('Y-m-t',strtotime($month.'-01')))->get();

            foreach ($presentsRows as $row) {

                $row->approved = null;
                $row->save();
            }
        }

    }
}
