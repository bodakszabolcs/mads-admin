<?php

namespace Modules\Fixing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FixingCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required',
			'month' => 'required',
			'student_id' => 'required',
			'company_id' => 'required',
			'industry_id' => 'required',
			'start' => 'required',
			'end' => 'required',
			'hour' => 'required',
			'fix' => 'required',
			'hour_price' => 'required',
			'invoice_status' => 'required',
			'invoice_id' => 'required',
			'hour_company' => 'required',
			'fix_company' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'status' => __('Állapot'),
'month' => __('Hónap'),
'student_id' => __('Diák'),
'company_id' => __('Cég'),
'industry_id' => __('Tevékenység'),
'start' => __('Kezdete'),
'end' => __('Vége'),
'hour' => __('Óra'),
'fix' => __('Fix'),
'hour_price' => __('Óra díj'),
'invoice_status' => __('Számla állapot'),
'invoice_id' => __('Számla azonosító'),
'hour_company' => __('Cég óradíj'),
'fix_company' => __('Cég fix'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
