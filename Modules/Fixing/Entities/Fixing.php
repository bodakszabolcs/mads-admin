<?php

	namespace Modules\Fixing\Entities;
	use Modules\Fixing\Entities\Base\BaseFixing;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class Fixing extends BaseFixing
	{
		public static $status=[
			0=>'Rögzítve',
			1=>'Jóváhagyva',
			2=>'Véglegesítve'
		];
		public static $invoiceStatus=[
			''=>'Nem',
			1=>'Igen',
			2=>'Stornózva'
		];
		use SoftDeletes, Cachable;
		public function __construct(array $attributes = [])
		{
			parent::__construct($attributes);
		}

		public function getFilters()
		{
			return parent::getFilters();
		}
		public function calculateStudentPrice(){
			return $this->hour * $this->price_hour + $this->price_hour * $this->bonus_multiplier * $this->bonus_hour + $this->fix;
		}
		public function calculateCompanyPrice(){
			return $this->price_company_hourly * $this->hour  +$this->price_company_hourly * $this->bonus_multiplier_company * $this->bonus_hour + ($this->fix * $this->fix_company_multiplier-$this->fix);
		}
        public function calculateCompanyHourly($hourly,$algorithm=0){
            if($algorithm ==1) {
                return $hourly;
            }
            if($this->multiplier){
                return ceil(($this->price_hour*$this->multiplier)-$this->price_hour);
            }
            if($this->divisor){
                return ceil(($this->price_hour/$this->divisor)-$this->price_hour);
            }
            return $hourly;
        }

	}
