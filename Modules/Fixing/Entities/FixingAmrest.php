<?php

	namespace Modules\Fixing\Entities;
	use App\BaseModel;
    use Modules\Fixing\Entities\Base\BaseFixing;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;

	class FixingAmrest extends BaseModel
	{
        protected $table = 'fixing_amrest';

        protected $dates = ['created_at', 'updated_at'];
        protected $fillable = ['student_id','amrest_id','industry_id','cost_center','sum_hour','sum_night','festive','price_hourly','deficit','deduction','bonus','recruiter','bonus_presence','company_price','gross_student','net','comment','month','exited','position'];
	}
