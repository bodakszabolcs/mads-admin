<?php

namespace Modules\Fixing\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\Company\Entities\Company;
		use Modules\Company\Entities\Industry;
		use Modules\Student\Entities\Student;


abstract class BaseFixing extends BaseModel
{


    protected $table = 'fixings';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'month',
                    'title' => 'Hónap',
                    'type' => 'month_picker',
                    ],
                                [
	                                'name' => 'company_name',
	                                'title' => 'Cég',
	                                'type' => 'text',
                                ]
	        ];

        return parent::getFilters();
    }


    protected $fillable = ['status','month','student_id','company_id','industry_id','start','end','hour','fix','hour_price','invoice_status','invoice_id','hour_company','fix_company','break_from','break_to'];

    protected $casts = [];

     public function company()
    {
        return $this->hasOne('Modules\Company\Entities\Company','id','company_id');
    }


public function industry()
    {
        return $this->hasMany('Modules\Company\Entities\Industry', 'id', 'industry_id');
    }


 public function student()
    {
        return $this->hasOne('Modules\Student\Entities\Student','id','student_id');
    }



}
