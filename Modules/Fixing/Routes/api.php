<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
	Route::group([
		'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/fixing', 'middleware' => ['auth:sanctum', 'role']
	], function () {
		Route::get('/get-student/{industry}/{month}', 'FixingController@getStudents')->name('Fixing get student');
		Route::get('/get-student-fixing/{student}/{industry}/{month}', 'FixingController@getStudentFixing')->name('Get student fixing by month');
		Route::post('/save', 'FixingController@saveFixing')->name('Fixing save');
		Route::post('/delete', 'FixingController@deleteFixing')->name('Delete fixing');

	});
Route::group([
    'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/fixing', 'middleware' => []
], function () {
    Route::post('/fixing-amrest/move-to-fixing/{month}', 'FixingController@moveToFixing')->name('Amrest export');
    Route::get('/student-worker-statistic/{month}', 'FixingSummationController@studentWorkerList')->name('Rögzítve lista');
    Route::get('/fixing-amrest/get-exel-export-all/{month}', 'FixingController@exportAmrestAll')->name('Amrest export');
    Route::get('/fixing-amrest/get-cost-center/{month}', 'FixingController@getCostCenter')->name('Cost center fixing');
    Route::get('/fixing-amrest/student-search/{month}/{costCenter}', 'FixingController@searchAmrestStudent')->name('fixing Amrest search student');
    Route::get('/completion-form/{type}/{month}/{company}', 'FixingSummationController@completionForm');
    Route::get('/completion-form-excel/{type}/{month}/{company}', 'FixingSummationController@completionFormExcel');
    Route::get('/fixing-amrest/cost-center', 'FixingController@costCenter')->name('get Amrest cost centers');
    Route::get('/fixing-amrest/{month}/{costCenter}', 'FixingController@fixingAmrest')->name('fixing Amrest');
    Route::get('/fixing-amrest/{month}/{costCenter}', 'FixingController@fixingAmrest')->name('fixing Amrest');
    Route::get('/presents/{industry}/{id}/{month}', 'FixingSummationController@getFixingPresentsPdf')->name('Rögzítés online jelenléti generálás');

    Route::post('/fixing-amrest/save', 'FixingController@saveAll')->name('fixing Amrest save');



});
	Route::group([
		'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/pay-scale', 'middleware' => ['auth:sanctum', 'role']
	], function () {
		Route::get('/{month?}', 'PayScaleController@index')->name('Pay scale list');
		Route::get('/{month}/export', 'PayScaleController@payScaleExport')->name('Pay scale UNIT export');
	});
	Route::group([
		'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/fixing-control', 'middleware' => ['auth:sanctum', 'role']
	], function () {
		Route::get('/{month?}', 'FixingControlController@fixingControlList')->name('fixing control list');
		Route::post('/approve/{month}/{company}/{industry?}', 'FixingControlController@approve')->name('fixing control approve');
		Route::post('/decline/{month}/{company}/{industry?}', 'FixingControlController@decline')->name('fixing control decline');
		Route::post('/recalculate/{month}/{company}/{type?}', 'FixingControlController@recalculate')->name('fixing control recalculate');
	});
	Route::group([
		'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/fixing-summation', 'middleware' => ['auth:sanctum', 'role']
	], function () {
		Route::get('/{month?}', 'FixingSummationController@fixingSummationList')->name('fixing summation list');
		Route::post('/invoice/{month}/{company}/{industry}', 'FixingSummationController@createInvoice')->name('fixing summation create invoice');

		Route::post('/storno/{month}/{company}/{industry}', 'FixingSummationController@stornoInvoice')->name('fixing summation storno invoice');
		Route::get('/excel/{month}/{company}/{industry}', 'FixingSummationController@excel')->name('fixing summation excel export');
		Route::get('/excel-year/{month}', 'FixingSummationController@excelYear')->name('fixing summation excel yearly export');
        Route::get('/presence/{month}', 'FixingSummationController@presenceCompany')->name('presence company');
        Route::get('/presence/{month}/{industry}', 'FixingSummationController@presenceIndustry')->name('presence industry');
        Route::get('/presence/{month}/{student}', 'FixingSummationController@presenceStudent')->name('presence student');
        Route::get('/get-invoice-data/{month}/{company}', 'FixingSummationController@invoiceView')->name('get invoice view by company');
        Route::get('/view-student-presence/{student}/{industry}/{month}', 'FixingSummationController@getStudentPresents')->name('view student presence');
        Route::post('/finalize-presence/{industry}/{month}', 'FixingSummationController@finalizePresents')->name('Jelenléti véglegesítés');
        Route::post('/finalize-presence/{industry}/{month}/{student}', 'FixingSummationController@finalizePresentsStudent')->name('Jelenléti véglegesítés diákonként');
        Route::post('/ban-presence/{industry}/{month}', 'FixingSummationController@banPresents')->name('Jóváhagyás visszaállítás');
        Route::post('/ban-presence/{industry}/{month}/{student}', 'FixingSummationController@banPresentsStudent')->name('Jóváhagyás visszaállítás diákonként');


	});
        Route::group([
            'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/fixing', 'middleware' => ['auth:sanctum']
        ], function () {
            Route::get('/get-bonus-by-company/{company}/{industry}', 'CompanyController@getBonusByCompanyAndIndustry')->name('get bonus');

        });
Route::group([
    'namespace' => 'Modules\Fixing\Http\Controllers', 'prefix' => '/fixing-summation', 'middleware' => ['auth:sanctum']
], function () {
    Route::get('/view-student-presence-pdf/{student}/{industry}/{month}', 'FixingSummationController@getStudentPresentPDF')->name('view student presence pdf');

});


