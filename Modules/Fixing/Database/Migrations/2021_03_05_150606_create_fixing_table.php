<?php
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	class CreateFixingTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::dropIfExists('fixings');
			Schema::create('fixings', function (Blueprint $table) {
				$table->bigIncrements("id");
				$table->tinyInteger("status")->unsigned()->nullable();
				$table->integer("month")->unsigned()->nullable();
				$table->date("date")->nullable();
				$table->bigInteger("student_id")->unsigned()->nullable();
				$table->bigInteger("company_id")->unsigned()->nullable();
				$table->bigInteger("industry_id")->nullable();
				$table->text("start")->nullable();
				$table->text("end")->nullable();
				$table->float("hour")->nullable();
				$table->float("bonus_hour")->nullable();
				$table->tinyInteger("bonus")->nullable();
				$table->tinyInteger("bonus_type")->nullable();
				$table->float("bonus_multiplier")->nullable();
				$table->float("bonus_multiplier_company")->nullable();
				$table->float("fix")->nullable();
				$table->float("fix_company_multiplier")->nullable();
				$table->float("price_hour")->nullable();
				$table->float("price_company_hourly")->nullable();
				$table->float("student_price")->nullable();
				$table->float("company_price")->nullable();
				$table->tinyInteger("invoice_status")->unsigned()->nullable();
				$table->bigInteger("invoice_id")->unsigned()->nullable();
				$table->bigInteger("user_id")->unsigned()->nullable();
				$table->timestamps();
				$table->softDeletes();
				$table->index(["deleted_at"]);

			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('fixings');
		}
	}
