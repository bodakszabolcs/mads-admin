<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Fixing\Entities\Fixing;

$factory->define(Fixing::class, function (Faker $faker) {
    return [
        "status" => rand(1,10),
"month" => rand(1000,5000),
"student_id" => rand(1000,5000),
"company_id" => rand(1000,5000),
"industry_id" => $faker->realText(),
"start" => $faker->dateTime(),
"end" => $faker->realText(),
"hour" => rand(1000,5000),
"fix" => rand(1000,5000),
"hour_price" => rand(1000,5000),
"invoice_status" => rand(1,10),
"invoice_id" => rand(1000,5000),
"hour_company" => rand(1000,5000),
"fix_company" => rand(1000,5000),

    ];
});
