<?php

namespace Modules\Fixing\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Fixing\Entities\Fixing;
use Modules\Fixing\Observers\FixingObserver;

class FixingServiceProvider extends ModuleServiceProvider
{
    protected $module = 'fixing';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Fixing::observe(FixingObserver::class);
    }
}
