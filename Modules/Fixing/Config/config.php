<?php
return [
    'name' => 'Fixing', 'menu_order' => 36, 'menu' => [
        [
            'icon' => 'flaticon-price-tag',
            'title' => 'Rögzítés',
            'route' => '#', 'submenu' =>
            [
                [

                    'icon' => 'fa fa-list-ol',

                    'title' => 'Havi megrendelések',

                    'route' => '/' . env('ADMIN_URL') . '/monthly-orders/index',

                ],
                [
                    'icon' => 'fa fa-clock', 'title' => 'Jelenléti összesítő', 'route' => '/' . env('ADMIN_URL') . '/fixing/presence'
                ],
                [
                    'icon' => 'flaticon-calendar-with-a-clock-time-tools', 'title' => 'Munkarögzítés', 'route' => '/' . env('ADMIN_URL') . '/fixing/index'
                ],
                [
                    'icon' => 'fa fa-file-excel', 'title' => 'Amrest', 'route' => '/' . env('ADMIN_URL') . '/fixing/amrest'
                ],
                [

                    'icon' =>'fa fa-university',

                    'title' =>'Előleg kérések',

                    'route' =>'/'.env('ADMIN_URL').'/down-payment/index',

                ],
                [
                    'icon' => 'fa fa-credit-card', 'title' => 'Hóközi kifizetés', 'route' => '/' . env('ADMIN_URL') . '/monthly_payments/index'
                ],
                [
                    'icon' => 'flaticon2-checkmark', 'title' => 'Ellenőrzés', 'route' => '/' . env('ADMIN_URL') . '/fixing-control/index'
                ],
                [
                    'icon' => 'flaticon-line-graph', 'title' => 'Összegzés', 'route' => '/' . env('ADMIN_URL') . '/fixing-summation/index'
                ],
                [
                    'icon' => 'flaticon2-list', 'title' => 'Bértábla', 'route' => '/' . env('ADMIN_URL') . '/pay-scale/index'
                ],
                [
                    'icon' => ' la la-list', 'title' => 'Rögzítve lista', 'route' => '/' . env('ADMIN_URL') . '/fixing/worker'
                ],
            ]
        ]
    ]
];
