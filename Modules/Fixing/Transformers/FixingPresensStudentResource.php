<?php

namespace Modules\Fixing\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\Fixing\Entities\Fixing;
class FixingPresensStudentResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "name" => $this->name,
            "tax_number" => $this->tax_number,
            "student_id" => $this->student_id,
            "industry_id" => $this->industry_id,
            "student_number" => $this->student_number,
            "count" => $this->count,
            "finalized" => $this->finalized,
            "expire" => $this->expire,
            "industry_name" => $this->industry_name,
            "appruved_user" => $this->appruved_user,
            "price" => $this->price,
            "price_hourly" => $this->price_hourly,
            "approved" => $this->appr,
		    "hours" => round($this->sum_hour,2),

		];
    }
}
