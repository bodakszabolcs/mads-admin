<?php

namespace Modules\Fixing\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\Fixing\Entities\Fixing;
class FixingListResource extends BaseResource
{
    public $preserveKeys = false;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "company_name" => $this->company_name,
            "company_id" => $this->company_id,
		    "status" => Arr::get(Fixing::$status,$this->status,''),
		    "month" => $this->month,
		    "industry_id" => $this->industry_id,
		    "industry_name" => $this->industry_name,
		    "students" => $this->students,
		    "invoice_status" => Arr::get(Fixing::$invoiceStatus,''.$this->invoice_status),
		    "hours" => $this->hours,
		    "price" => ConstansHelper::formatPrice($this->price),

		     ];
    }
}
