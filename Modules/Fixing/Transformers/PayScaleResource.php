<?php

namespace Modules\Fixing\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Company;
		use Modules\Company\Entities\Industry;
use Modules\Payroll\Entities\Payroll;
use Modules\Student\Entities\Student;


class PayScaleResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [

            "birth_date" => $this->birth_date,
            "feor"=>$this->feor,
            "comment"=>(isset($this->comment))?$this->comment:"",
            "monthly_price_gross"=> ($this->monthly_price)?$this->monthly_price*1:0,
            "monthly_price_net"=> ($this->calcNetMonthly())?$this->calcNetMonthly()*1:0,
            "part"=> (($this->part_ticket==1)?0:2000),
            "gross"=> $this->price,
            "net"=>$this->calcNet(),
            "student_id"=> $this->student_id,
            "code"=> $this->code,
            "discount"=> $this->discount,
            "student_name"=> $this->student_name,
            "student_email"=> $this->student_email,
            "tax_number"=> $this->tax_number,
		     ];
    }
    function calcNet(){
        if(!$this->discount || (date("Y-m-d",strtotime($this->birth_date."+ 25 year"))<date("Y-m-d",strtotime($this->month.'01')) || $this->student_afa))
        {
            return $this->price*0.85;
        }
        if($this->price > Payroll::$underAgeLimit){
            return Payroll::$underAgeLimit+(($this->price-Payroll::$underAgeLimit)*0.85);
        }
        return  $this->price;
    }
    function calcNetMonthly(){
        if(!$this->discount || (date("Y-m-d",strtotime($this->birth_date."+ 25 year"))<date("Y-m-d",strtotime($this->month.'01')) || $this->student_afa))
        {
            return $this->monthly_price*0.85;
        }
        if($this->monthly_price > Payroll::$underAgeLimit){
            return Payroll::$underAgeLimit+(($this->monthly_price-Payroll::$underAgeLimit)*0.85);
        }
        return  $this->monthly_price;
    }
}
