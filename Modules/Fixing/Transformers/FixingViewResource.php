<?php

namespace Modules\Fixing\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Company;
		use Modules\Company\Entities\Industry;
		use Modules\Student\Entities\Student;
		

class FixingViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "status" => $this->status,
		    "month" => $this->month,
		    "student_id" => $this->student_id,
		    "company_id" => $this->company_id,
		    "industry_id" => $this->industry_id,
		    "start" => $this->start,
		    "end" => $this->end,
		    "hour" => $this->hour,
		    "fix" => $this->fix,
		    "hour_price" => $this->hour_price,
		    "invoice_status" => $this->invoice_status,
		    "invoice_id" => $this->invoice_id,
		    "hour_company" => $this->hour_company,
		    "fix_company" => $this->fix_company,
		"selectables" => [
		"company" => Company::pluck("name","id"),
		"industry" => Industry::pluck("name","id"),
		"student" => Student::pluck("name","id"),
		]
		     ];
    }
}
