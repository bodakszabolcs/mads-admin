<?php

namespace Modules\Fixing\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\Fixing\Entities\Fixing;
class FixingPresensResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "name" => $this->name,
            "student" => $this->student,
            "line" => $this->sum_line,
            "online_presence" => $this->online_presence,
            "industry_id" => $this->industry_id,
		    "hours" => round($this->sum_hour,2),
		    "student_count" => $this->student_count,
		    "all" => $this->cnt,
		    "final" => $this->final,
		    "approved" => $this->appr,

		];
    }
}
