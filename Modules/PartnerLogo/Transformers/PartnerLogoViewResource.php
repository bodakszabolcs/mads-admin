<?php

namespace Modules\PartnerLogo\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PartnerLogoViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "log" => $this->log,
		"selectables" => [
		]
		     ];
    }
}
