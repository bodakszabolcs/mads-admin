<?php

namespace Modules\PartnerLogo\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PartnerLogoCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'log' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Partner neve'),
'log' => __('Partner Logo'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
