<?php

namespace Modules\PartnerLogo\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\PartnerLogo\Entities\PartnerLogo;
use Illuminate\Http\Request;
use Modules\PartnerLogo\Http\Requests\PartnerLogoCreateRequest;
use Modules\PartnerLogo\Http\Requests\PartnerLogoUpdateRequest;
use Modules\PartnerLogo\Transformers\PartnerLogoViewResource;
use Modules\PartnerLogo\Transformers\PartnerLogoListResource;

class PartnerLogoController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PartnerLogo();
        $this->viewResource = PartnerLogoViewResource::class;
        $this->listResource = PartnerLogoListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PartnerLogoCreateRequest::class;
        $this->updateRequest = PartnerLogoUpdateRequest::class;
    }

}
