<?php

namespace Modules\PartnerLogo\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PartnerLogo\Entities\PartnerLogo;
use Modules\PartnerLogo\Observers\PartnerLogoObserver;

class PartnerLogoServiceProvider extends ModuleServiceProvider
{
    protected $module = 'partnerlogo';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PartnerLogo::observe(PartnerLogoObserver::class);
    }
}
