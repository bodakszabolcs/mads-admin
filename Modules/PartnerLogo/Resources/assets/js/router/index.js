import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PartnerLogo from '../components/PartnerLogo'
import PartnerLogoList from '../components/PartnerLogoList'
import PartnerLogoCreate from '../components/PartnerLogoCreate'
import PartnerLogoEdit from '../components/PartnerLogoEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'partner-logo',
                component: PartnerLogo,
                meta: {
                    title: 'PartnerLogos'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PartnerLogoList',
                        component: PartnerLogoList,
                        meta: {
                            title: 'PartnerLogos',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/partner-logo/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner-logo/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PartnerLogoCreate',
                        component: PartnerLogoCreate,
                        meta: {
                            title: 'Create PartnerLogos',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner-logo/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PartnerLogoEdit',
                        component: PartnerLogoEdit,
                        meta: {
                            title: 'Edit PartnerLogos',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/partner-logo/index'
                        }
                    }
                ]
            }
        ]
    }
]
