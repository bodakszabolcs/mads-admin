<?php

namespace Modules\PartnerLogo\Observers;

use Modules\PartnerLogo\Entities\PartnerLogo;

class PartnerLogoObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function saved(PartnerLogo $model)
    {
        PartnerLogo::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function created(PartnerLogo $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function updated(PartnerLogo $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function deleted(PartnerLogo $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function restored(PartnerLogo $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PartnerLogo\Entities\PartnerLogo  $model
     * @return void
     */
    public function forceDeleted(PartnerLogo $model)
    {
        //
    }
}
