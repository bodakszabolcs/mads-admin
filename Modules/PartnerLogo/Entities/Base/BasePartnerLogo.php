<?php

namespace Modules\PartnerLogo\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BasePartnerLogo extends BaseModel
{
    

    protected $table = 'partner_logos';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Partner neve',
                    'type' => 'text',
                    ],[
                    'name' => 'log',
                    'title' => 'Partner Logo',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','log'];

    protected $casts = [];

    
}
