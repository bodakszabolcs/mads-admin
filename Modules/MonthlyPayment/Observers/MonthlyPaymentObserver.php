<?php

namespace Modules\MonthlyPayment\Observers;

use Modules\MonthlyPayment\Entities\MonthlyPayment;

class MonthlyPaymentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function saved(MonthlyPayment $model)
    {
        MonthlyPayment::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function created(MonthlyPayment $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function updated(MonthlyPayment $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function deleted(MonthlyPayment $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function restored(MonthlyPayment $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\MonthlyPayment\Entities\MonthlyPayment  $model
     * @return void
     */
    public function forceDeleted(MonthlyPayment $model)
    {
        //
    }
}
