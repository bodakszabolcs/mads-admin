<?php

namespace Modules\MonthlyPayment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MonthlyPaymentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
			'month' => 'required',
			'price' => 'required',


        ];
    }

    public function attributes()
        {
            return [
                'student_id' => __('Diák'),
'month' => __('Hónap'),
'price' => __('Összeg'),
'comment' => __('Megjegyzés'),
'user_id' => __('Rögzitő'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
