<?php

namespace Modules\MonthlyPayment\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Student\Entities\Student;
		

class MonthlyPaymentViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "month" => $this->month,
		    "price" => $this->price,
		    "comment" => $this->comment,
		    "user_id" => $this->user_id,
		"selectables" => [
		"student" => Student::pluck("name","id"),
		]
		     ];
    }
}
