<?php

namespace Modules\MonthlyPayment\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class MonthlyPaymentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_name" => $this->student_name,
		    "student_id" => $this->student_id,
		    "month" => $this->month,
		    "price" => ConstansHelper::formatPrice($this->price),
		    "comment" => $this->comment,
		    "user_name" => $this->user_name,
		     ];
    }
}
