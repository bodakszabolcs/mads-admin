<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\MonthlyPayment\Entities\MonthlyPayment;

$factory->define(MonthlyPayment::class, function (Faker $faker) {
    return [
        "student_id" => rand(1000,5000),
"month" => rand(1000,5000),
"price" => rand(1000,5000),
"comment" => $faker->realText(),
"user_id" => rand(1000,5000),

    ];
});
