<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMonthlyPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('monthly_payments');
        Schema::create('monthly_payments', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("student_id")->unsigned()->nullable();
$table->integer("month")->unsigned()->nullable();
$table->integer("price")->unsigned()->nullable();
$table->text("comment")->nullable();
$table->bigInteger("user_id")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('monthly_payments');
    }
}
