<?php

namespace Modules\MonthlyPayment\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\MonthlyPayment\Entities\MonthlyPayment;
use Modules\MonthlyPayment\Observers\MonthlyPaymentObserver;

class MonthlyPaymentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'monthlypayment';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        MonthlyPayment::observe(MonthlyPaymentObserver::class);
    }
}
