<?php

namespace Modules\MonthlyPayment\Entities;

use Modules\MonthlyPayment\Entities\Base\BaseMonthlyPayment;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class MonthlyPayment extends BaseMonthlyPayment
{
    use SoftDeletes, Cachable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        return parent::getFilters();
    }
    public function fillAndSave(array $request)
    {
    	$request['user_id']= \Auth::id();
	    return parent::fillAndSave($request);
    }
}
