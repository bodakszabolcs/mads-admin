import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import MonthlyPayment from '../components/MonthlyPayment'
import MonthlyPaymentList from '../components/MonthlyPaymentList'
import MonthlyPaymentCreate from '../components/MonthlyPaymentCreate'
import MonthlyPaymentEdit from '../components/MonthlyPaymentEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'monthly_payments',
                component: MonthlyPayment,
                meta: {
                    title: 'MonthlyPayments'
                },
                children: [
                    {
                        path: 'index',
                        name: 'MonthlyPaymentList',
                        component: MonthlyPaymentList,
                        meta: {
                            title: 'MonthlyPayments',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly_payments/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly_payments/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'MonthlyPaymentCreate',
                        component: MonthlyPaymentCreate,
                        meta: {
                            title: 'Create MonthlyPayments',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly_payments/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'MonthlyPaymentEdit',
                        component: MonthlyPaymentEdit,
                        meta: {
                            title: 'Edit MonthlyPayments',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/monthly_payments/index'
                        }
                    }
                ]
            }
        ]
    }
]
