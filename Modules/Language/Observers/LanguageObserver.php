<?php

namespace Modules\Language\Observers;

use Modules\Language\Entities\Language;

class LanguageObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function saved(Language $model)
    {
        Language::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function created(Language $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function updated(Language $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function deleted(Language $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function restored(Language $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Language\Entities\Language  $model
     * @return void
     */
    public function forceDeleted(Language $model)
    {
        //
    }
}
