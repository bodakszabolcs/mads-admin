import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Language from '../components/Language'
import LanguageList from '../components/LanguageList'
import LanguageCreate from '../components/LanguageCreate'
import LanguageEdit from '../components/LanguageEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'language',
                component: Language,
                meta: {
                    title: 'Languages'
                },
                children: [
                    {
                        path: 'index',
                        name: 'LanguageList',
                        component: LanguageList,
                        meta: {
                            title: 'Languages',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/language/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/language/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'LanguageCreate',
                        component: LanguageCreate,
                        meta: {
                            title: 'Create Languages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/language/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'LanguageEdit',
                        component: LanguageEdit,
                        meta: {
                            title: 'Edit Languages',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/language/index'
                        }
                    }
                ]
            }
        ]
    }
]
