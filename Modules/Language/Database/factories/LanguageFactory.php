<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Language\Entities\Language;

$factory->define(Language::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
