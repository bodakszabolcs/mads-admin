<?php

namespace Modules\Language\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Language\Entities\Language;
use Modules\Language\Observers\LanguageObserver;

class LanguageServiceProvider extends ModuleServiceProvider
{
    protected $module = 'language';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Language::observe(LanguageObserver::class);
    }
}
