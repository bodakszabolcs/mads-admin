<?php

namespace Modules\Language\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Language\Entities\Language;
use Illuminate\Http\Request;
use Modules\Language\Http\Requests\LanguageCreateRequest;
use Modules\Language\Http\Requests\LanguageUpdateRequest;
use Modules\Language\Transformers\LanguageViewResource;
use Modules\Language\Transformers\LanguageListResource;

class LanguageController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Language();
        $this->viewResource = LanguageViewResource::class;
        $this->listResource = LanguageListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = LanguageCreateRequest::class;
        $this->updateRequest = LanguageUpdateRequest::class;
    }

}
