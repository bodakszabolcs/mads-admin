<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationAreaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('education_area');
        Schema::create('education_area', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_area');
    }
}
