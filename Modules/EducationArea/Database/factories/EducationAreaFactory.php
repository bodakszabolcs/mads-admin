<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\EducationArea\Entities\EducationArea;

$factory->define(EducationArea::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
