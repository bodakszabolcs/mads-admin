<?php

namespace Modules\EducationArea\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\EducationArea\Entities\EducationArea;
use Modules\EducationArea\Observers\EducationAreaObserver;

class EducationAreaServiceProvider extends ModuleServiceProvider
{
    protected $module = 'educationarea';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        EducationArea::observe(EducationAreaObserver::class);
    }
}
