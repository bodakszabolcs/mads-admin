<?php

namespace Modules\EducationArea\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\EducationArea\Entities\EducationArea;
use Illuminate\Http\Request;
use Modules\EducationArea\Http\Requests\EducationAreaCreateRequest;
use Modules\EducationArea\Http\Requests\EducationAreaUpdateRequest;
use Modules\EducationArea\Transformers\EducationAreaViewResource;
use Modules\EducationArea\Transformers\EducationAreaListResource;

class EducationAreaController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new EducationArea();
        $this->viewResource = EducationAreaViewResource::class;
        $this->listResource = EducationAreaListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = EducationAreaCreateRequest::class;
        $this->updateRequest = EducationAreaUpdateRequest::class;
    }

}
