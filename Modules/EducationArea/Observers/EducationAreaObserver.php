<?php

namespace Modules\EducationArea\Observers;

use Modules\EducationArea\Entities\EducationArea;

class EducationAreaObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function saved(EducationArea $model)
    {
        EducationArea::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function created(EducationArea $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function updated(EducationArea $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function deleted(EducationArea $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function restored(EducationArea $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\EducationArea\Entities\EducationArea  $model
     * @return void
     */
    public function forceDeleted(EducationArea $model)
    {
        //
    }
}
