import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import EducationArea from '../components/EducationArea'
import EducationAreaList from '../components/EducationAreaList'
import EducationAreaCreate from '../components/EducationAreaCreate'
import EducationAreaEdit from '../components/EducationAreaEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'education-area',
                component: EducationArea,
                meta: {
                    title: 'EducationAreas'
                },
                children: [
                    {
                        path: 'index',
                        name: 'EducationAreaList',
                        component: EducationAreaList,
                        meta: {
                            title: 'EducationAreas',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/education-area/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-area/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'EducationAreaCreate',
                        component: EducationAreaCreate,
                        meta: {
                            title: 'Create EducationAreas',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-area/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'EducationAreaEdit',
                        component: EducationAreaEdit,
                        meta: {
                            title: 'Edit EducationAreas',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/education-area/index'
                        }
                    }
                ]
            }
        ]
    }
]
