<?php

namespace Modules\PostDelivery\Observers;

use Modules\PostDelivery\Entities\PostDelivery;

class PostDeliveryObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function saved(PostDelivery $model)
    {
        PostDelivery::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function created(PostDelivery $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function updated(PostDelivery $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function deleted(PostDelivery $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function restored(PostDelivery $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PostDelivery\Entities\PostDelivery  $model
     * @return void
     */
    public function forceDeleted(PostDelivery $model)
    {
        //
    }
}
