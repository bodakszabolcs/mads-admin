import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PostDelivery from '../components/PostDelivery'
import PostDeliveryList from '../components/PostDeliveryList'
import PostDeliveryCreate from '../components/PostDeliveryCreate'
import PostDeliveryEdit from '../components/PostDeliveryEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'post-delivery',
                component: PostDelivery,
                meta: {
                    title: 'PostDeliveries'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PostDeliveryList',
                        component: PostDeliveryList,
                        meta: {
                            title: 'PostDeliveries',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PostDeliveryCreate',
                        component: PostDeliveryCreate,
                        meta: {
                            title: 'Create PostDeliveries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PostDeliveryEdit',
                        component: PostDeliveryEdit,
                        meta: {
                            title: 'Edit PostDeliveries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery/index'
                        }
                    }
                ]
            }
        ]
    }
]
