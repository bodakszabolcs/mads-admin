<?php

namespace Modules\PostDelivery\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PostDelivery\Entities\PostDelivery;
use Modules\PostDelivery\Observers\PostDeliveryObserver;

class PostDeliveryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'postdelivery';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PostDelivery::observe(PostDeliveryObserver::class);
    }
}
