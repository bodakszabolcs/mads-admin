<?php

namespace Modules\PostDelivery\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\PostDelivery\Entities\PostDelivery;
use Illuminate\Http\Request;
use Modules\PostDelivery\Http\Requests\PostDeliveryCreateRequest;
use Modules\PostDelivery\Http\Requests\PostDeliveryUpdateRequest;
use Modules\PostDelivery\Transformers\PostDeliveryViewResource;
use Modules\PostDelivery\Transformers\PostDeliveryListResource;

class PostDeliveryController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PostDelivery();
        $this->viewResource = PostDeliveryViewResource::class;
        $this->listResource = PostDeliveryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PostDeliveryCreateRequest::class;
        $this->updateRequest = PostDeliveryUpdateRequest::class;
    }

}
