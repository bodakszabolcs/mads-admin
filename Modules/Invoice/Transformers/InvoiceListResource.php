<?php

namespace Modules\Invoice\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class InvoiceListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "invoice_transporter_id" => Optional($this->invoicetransporter)->name,
		    "invoice_date" => $this->invoice_date,
		    "invoice_number" => $this->invoice_number,
		    "payment_deadline" => $this->payment_deadline,
		    "completion_date" => $this->completion_date,
		    "subject" => $this->subject,
		    "net" => $this->net,
		    "vat" => $this->vat,
		    "gross" => $this->gross,
		    "pay" => $this->pay,
		    "payed" => $this->payed,
		    "payed_date" => $this->payed_date,
		     ];
    }
}
