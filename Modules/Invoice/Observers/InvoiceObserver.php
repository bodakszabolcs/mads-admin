<?php

namespace Modules\Invoice\Observers;

use Modules\Invoice\Entities\Invoice;

class InvoiceObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function saved(Invoice $model)
    {
        Invoice::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function created(Invoice $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function updated(Invoice $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function deleted(Invoice $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function restored(Invoice $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Invoice\Entities\Invoice  $model
     * @return void
     */
    public function forceDeleted(Invoice $model)
    {
        //
    }
}
