<?php

return [
    'name' => 'Invoice',

    'menu_order' => 45,

    'menu' => [
        [

            'icon' => 'fa fa-info-circle',

            'title' => 'Számlák',
            'route' => '#invoice',
            'submenu' => [

                [

                    'icon' => 'fa fa-truck',

                    'title' => 'Szállítók',

                    'route' => '/' . env('ADMIN_URL') . '/invoice-transporters/index',

                ],
                [

                    'icon' => 'fa fa-info-circle',

                    'title' => 'Számlák',

                    'route' => '/' . env('ADMIN_URL') . '/invoices/index',

                ]

            ],

        ],
        [
            'icon' => 'flaticon-pie-chart',
            'title' => 'Statisztika',
            'route' => '#stat',
            'submenu' => [
                [

                    'icon' => 'fa fa-map',

                    'title' => 'Régió',

                    'route' => '/' . env('ADMIN_URL') . '/region/statistic',

                ],
                [

                    'icon' => 'fa fa-users',

                    'title' => 'Létszám igények',

                    'route' => '/' . env('ADMIN_URL') . '/staffing/statistic',

                ],
                [

                    'icon' => 'flaticon2-line-chart',

                    'title' => 'Projectek',

                    'route' => '/' . env('ADMIN_URL') . '/user/statistic',

                ],
                [

                'icon' => 'flaticon2-pie-chart',

                'title' => 'Tevékenység',

                'route' => '/' . env('ADMIN_URL') . '/user/pm-statistic',

            ],
                [

                    'icon' => 'flaticon2-pie-chart-4',

                    'title' => 'Diák',

                    'route' => '/' . env('ADMIN_URL') . '/user/pm-student-statistic',

                ],
                [

                    'icon' => 'fa fa-file',

                    'title' => 'Szerződés',

                    'route' => '/' . env('ADMIN_URL') . '/contract/statistic',

                ],
                [

                    'icon' => 'flaticon2-chart',

                    'title' => 'Bevétel',

                    'route' => '/' . env('ADMIN_URL') . '/fixing/statistic',

                ],
            ]
        ]

    ]
];
