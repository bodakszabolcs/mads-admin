<?php

namespace Modules\Invoice\Http\Controllers;

use App\Exports\BaseArrayExport;
use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Modules\Fixing\Entities\Fixing;
use Modules\Invoice\Entities\Invoice;
use Illuminate\Http\Request;
use Modules\Invoice\Http\Requests\InvoiceCreateRequest;
use Modules\Invoice\Http\Requests\InvoiceUpdateRequest;
use Modules\Invoice\Transformers\InvoiceViewResource;
use Modules\Invoice\Transformers\InvoiceListResource;

class InvoiceController extends AbstractLiquidController
{
    private $headings =[
        'ID',
        'Szállító',
        'Számla kelte',
        'Számla sorszáma',
        'Fizetési határidő',
        'Teljesítés dátuma',
        'Tárgy',
        'Nettó',
        'Bruttó',
        'Fizetve',
        'Fizetés dátuma'
    ];
    public function index(Request $request, $auth = null)
    {
        if(empty($request->input('sort')) ){

            $request->merge(['sort'=>'invoice_date|desc']);

        }
        $list = $this->model->searchInModel($request->input());

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Invoice();
        $this->viewResource = InvoiceViewResource::class;
        $this->listResource = InvoiceListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = InvoiceCreateRequest::class;
        $this->updateRequest = InvoiceUpdateRequest::class;
    }
    public function excel(Request $request)
    {
        $list = $this->model->searchInModel($request->input());
        $excel=[];
        $title = "Mads-szamlak-".date('Y-m-d');
        foreach ($list->orderBy('created_at')->get() as $row) {
            $excel[] = [
                "id" => $row->id,
                "invoice_transporter_id" => optional($row->invoicetransporter)->name,
                "invoice_date" => $row->invoice_date,
                "invoice_number" => $row->invoice_number,
                "payment_deadline" => $row->payment_deadline,
                "completion_date" => $row->completion_date,
                "subject" => $row->subject,
                "net" => $row->net,
                "gross" => $row->gross,
                "payed" => $row->payed,
                "payed_date" => $row->payed_date,
            ];
        }
        $export = new BaseArrayExport($this->headings, "Számlák", $excel);

        return $export->download(Str::slug($title) . '.xlsx');

    }

}
