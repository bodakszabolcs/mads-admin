<?php

namespace Modules\Invoice\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice_transporter_id' => 'required',


        ];
    }

    public function attributes()
    {
        return [
            'invoice_transporter_id' => __('Szállító'),
            'invoice_date' => __('Számla kelte'),
            'invoice_number' => __('Számla sorszáma'),
            'payment_deadline' => __('Fizetési határidő'),
            'completion_date' => __('Teljesítés dátuma'),
            'subject' => __('Tárgy'),
            'net' => __('Nettó'),
            'vat' => __('Áfa értéke'),
            'gross' => __('Bruttó'),
            'pay' => __('Fizetendő'),
            'payed' => __('Fizetve'),
            'payed_date' => __('Fizetés dátuma'),

        ];
    }

    public function authorize()
    {
        return true;
    }
}
