<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Invoice\Entities\Invoice;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        "invoice_transporter_id" => rand(1000,5000),
"invoice_date" => $faker->date(),
"invoice_number" => $faker->realText(),
"payment_deadline" => $faker->date(),
"completion_date" => $faker->date(),
"subject" => $faker->realText(),
"net" => rand(1000,5000),
"vat" => rand(1000,5000),
"gross" => rand(1000,5000),
"pay" => rand(1000,5000),
"payed" => rand(1000,5000),
"payed_date" => $faker->date(),

    ];
});
