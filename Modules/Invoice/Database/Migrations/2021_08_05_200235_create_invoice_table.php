<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('invoices');
        Schema::create('invoices', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("invoice_transporter_id")->unsigned()->nullable();
$table->date("invoice_date")->nullable();
$table->text("invoice_number")->nullable();
$table->date("payment_deadline")->nullable();
$table->date("completion_date")->nullable();
$table->text("subject")->nullable();
$table->float("net")->nullable();
$table->float("vat")->nullable();
$table->float("gross")->nullable();
$table->float("pay")->nullable();
$table->float("payed")->nullable();
$table->date("payed_date")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
