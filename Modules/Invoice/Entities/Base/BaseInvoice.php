<?php

namespace Modules\Invoice\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;


abstract class BaseInvoice extends BaseModel
{


    protected $table = 'invoices';

    protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'invoice_transporter_id',
                'title' => 'Szállító',
                'type' => 'select',
                'data' => InvoiceTransporter::pluck('name', 'id')
            ],[
                'name' => 'invoice_date',
                'title' => 'Számla kelte',
                'type' => 'date',
            ], [
                'name' => 'invoice_number',
                'title' => 'Számla sorszáma',
                'type' => 'text',
            ], [
                'name' => 'completion_date',
                'title' => 'Teljesítés dátuma',
                'type' => 'date',
            ], [
                'name' => 'subject',
                'title' => 'Tárgy',
                'type' => 'text',
            ], [
                'name' => 'payed_date',
                'title' => 'Fizetés dátuma',
                'type' => 'date',
            ],];

        return parent::getFilters();
    }

    protected $with = ["invoicetransporter"];

    protected $fillable = ['invoice_transporter_id', 'invoice_date', 'invoice_number', 'payment_deadline', 'completion_date', 'subject', 'net', 'vat', 'gross', 'pay', 'payed', 'payed_date','file'];

    protected $casts = [];

    public function invoicetransporter()
    {
        return $this->hasOne('Modules\InvoiceTransporter\Entities\InvoiceTransporter', 'id', 'invoice_transporter_id');
    }


}
