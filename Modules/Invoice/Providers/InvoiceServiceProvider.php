<?php

namespace Modules\Invoice\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Invoice\Entities\Invoice;
use Modules\Invoice\Observers\InvoiceObserver;

class InvoiceServiceProvider extends ModuleServiceProvider
{
    protected $module = 'invoice';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Invoice::observe(InvoiceObserver::class);
    }
}
