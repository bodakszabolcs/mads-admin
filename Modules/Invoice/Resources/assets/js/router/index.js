import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Invoice from '../components/Invoice'
import InvoiceList from '../components/InvoiceList'
import InvoiceCreate from '../components/InvoiceCreate'
import InvoiceEdit from '../components/InvoiceEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'invoices',
                component: Invoice,
                meta: {
                    title: 'Invoices'
                },
                children: [
                    {
                        path: 'index',
                        name: 'InvoiceList',
                        component: InvoiceList,
                        meta: {
                            title: 'Invoices',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/invoices/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoices/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'InvoiceCreate',
                        component: InvoiceCreate,
                        meta: {
                            title: 'Create Invoices',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoices/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'InvoiceEdit',
                        component: InvoiceEdit,
                        meta: {
                            title: 'Edit Invoices',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoices/index'
                        }
                    }
                ]
            }
        ]
    }
]
