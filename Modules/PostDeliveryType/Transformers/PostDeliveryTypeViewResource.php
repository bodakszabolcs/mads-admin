<?php

namespace Modules\PostDeliveryType\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PostDeliveryTypeViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		"selectables" => [
		]
		     ];
    }
}
