<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\PostDeliveryType\Entities\PostDeliveryType;

$factory->define(PostDeliveryType::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
