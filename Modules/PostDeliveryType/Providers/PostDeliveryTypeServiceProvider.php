<?php

namespace Modules\PostDeliveryType\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PostDeliveryType\Entities\PostDeliveryType;
use Modules\PostDeliveryType\Observers\PostDeliveryTypeObserver;

class PostDeliveryTypeServiceProvider extends ModuleServiceProvider
{
    protected $module = 'postdeliverytype';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PostDeliveryType::observe(PostDeliveryTypeObserver::class);
    }
}
