import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PostDeliveryType from '../components/PostDeliveryType'
import PostDeliveryTypeList from '../components/PostDeliveryTypeList'
import PostDeliveryTypeCreate from '../components/PostDeliveryTypeCreate'
import PostDeliveryTypeEdit from '../components/PostDeliveryTypeEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'post-delivery-type',
                component: PostDeliveryType,
                meta: {
                    title: 'PostDeliveryTypes'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PostDeliveryTypeList',
                        component: PostDeliveryTypeList,
                        meta: {
                            title: 'PostDeliveryTypes',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery-type/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery-type/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PostDeliveryTypeCreate',
                        component: PostDeliveryTypeCreate,
                        meta: {
                            title: 'Create PostDeliveryTypes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery-type/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PostDeliveryTypeEdit',
                        component: PostDeliveryTypeEdit,
                        meta: {
                            title: 'Edit PostDeliveryTypes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/post-delivery-type/index'
                        }
                    }
                ]
            }
        ]
    }
]
