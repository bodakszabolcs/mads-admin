<?php

namespace Modules\PostDeliveryType\Observers;

use Modules\PostDeliveryType\Entities\PostDeliveryType;

class PostDeliveryTypeObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function saved(PostDeliveryType $model)
    {
        PostDeliveryType::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function created(PostDeliveryType $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function updated(PostDeliveryType $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function deleted(PostDeliveryType $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function restored(PostDeliveryType $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PostDeliveryType\Entities\PostDeliveryType  $model
     * @return void
     */
    public function forceDeleted(PostDeliveryType $model)
    {
        //
    }
}
