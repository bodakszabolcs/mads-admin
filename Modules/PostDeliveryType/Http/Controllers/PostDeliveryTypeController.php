<?php

namespace Modules\PostDeliveryType\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\PostDeliveryType\Entities\PostDeliveryType;
use Illuminate\Http\Request;
use Modules\PostDeliveryType\Http\Requests\PostDeliveryTypeCreateRequest;
use Modules\PostDeliveryType\Http\Requests\PostDeliveryTypeUpdateRequest;
use Modules\PostDeliveryType\Transformers\PostDeliveryTypeViewResource;
use Modules\PostDeliveryType\Transformers\PostDeliveryTypeListResource;

class PostDeliveryTypeController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PostDeliveryType();
        $this->viewResource = PostDeliveryTypeViewResource::class;
        $this->listResource = PostDeliveryTypeListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PostDeliveryTypeCreateRequest::class;
        $this->updateRequest = PostDeliveryTypeUpdateRequest::class;
    }

}
