<?php

namespace Modules\PostDeliveryType\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostDeliveryTypeCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Név'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
