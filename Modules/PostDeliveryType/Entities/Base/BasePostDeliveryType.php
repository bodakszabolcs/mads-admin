<?php

namespace Modules\PostDeliveryType\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BasePostDeliveryType extends BaseModel
{
    

    protected $table = 'post_delivery_type';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Név',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name'];

    protected $casts = [];

    
}
