<?php

namespace Modules\InvoiceTransporter\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseInvoiceTransporter extends BaseModel
{


    protected $table = 'invoice_transporters';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Név',
                    'type' => 'text',
                    ],[
                    'name' => 'address',
                    'title' => 'Cím',
                    'type' => 'text',
                    ],[
                    'name' => 'product',
                    'title' => 'Termék/szolgáltatás',
                    'type' => 'text',
                    ],[
                    'name' => 'payment_deadline',
                    'title' => 'Fizetési határidő',
                    'type' => 'text',
                    ],[
                    'name' => 'contact',
                    'title' => 'Kapcsolattartó',
                    'type' => 'text',
                    ],[
                    'name' => 'email',
                    'title' => 'E-mail',
                    'type' => 'text',
                    ],[
                    'name' => 'phone',
                    'title' => 'Telefon',
                    'type' => 'text',
                    ]];

        return parent::getFilters();
    }

    protected $with = [];
    protected $fillable = ['name','address','product','payment_method','payment_deadline','completion','vat','multiplier','contact','email','phone','web','identifier','password','pin_code'];
    protected $casts = [];

}
