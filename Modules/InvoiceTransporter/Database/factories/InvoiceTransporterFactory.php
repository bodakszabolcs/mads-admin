<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;

$factory->define(InvoiceTransporter::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"address" => $faker->realText(),
"product" => $faker->realText(),
"payment_method" => rand(1,10),
"payment_deadline" => $faker->realText(),
"completion" => rand(1,10),
"vat" => rand(1,10),
"multiplier" => rand(1000,5000),
"contact" => $faker->realText(),
"email" => $faker->realText(),
"phone" => $faker->realText(),
"web" => $faker->realText(),
"identifier" => $faker->realText(),
"password" => $faker->realText(),
"pin_code" => $faker->realText(),

    ];
});
