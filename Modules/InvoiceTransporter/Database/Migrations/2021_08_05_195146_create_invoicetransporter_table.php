<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTransporterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('invoice_transporters');
        Schema::create('invoice_transporters', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();
$table->text("address")->nullable();
$table->text("product")->nullable();
$table->tinyInteger("payment_method")->unsigned()->nullable();
$table->text("payment_deadline")->nullable();
$table->tinyInteger("completion")->unsigned()->nullable();
$table->tinyInteger("vat")->unsigned()->nullable();
$table->float("multiplier")->nullable();
$table->text("contact")->nullable();
$table->text("email")->nullable();
$table->text("phone")->nullable();
$table->text("web")->nullable();
$table->text("identifier")->nullable();
$table->text("password")->nullable();
$table->text("pin_code")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_transporters');
    }
}
