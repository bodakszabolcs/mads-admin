<?php

namespace Modules\InvoiceTransporter\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class InvoiceTransporterListResource extends BaseResource
{
    private  $paymentMethods =[
        1=>'Utalás',2=>'Kézpénz'
        ];
    private  $completions =[
        1=>'Folyamatos',2=>'Alkalmi'
    ];
    private  $vats =[
        1=>'Normal 27%',2=>'AM 0%',3=>'TM 0%'
    ];
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "address" => $this->address,
		    "product" => $this->product,
		    "payment_method" => \Arr::get($this->paymentMethods,$this->payment_method,''),
		    "payment_deadline" => $this->payment_deadline,
		    "completion" => \Arr::get($this->completions,$this->completion,''),
		    "vat" => \Arr::get($this->vats,$this->vat,''),
		    "multiplier" => $this->multiplier,
		    "contact" => $this->contact,
		    "email" => $this->email,
		    "phone" => $this->phone,
		    "web" => $this->web,
		    "identifier" => $this->identifier,
		    "password" => $this->password,
		    "pin_code" => $this->pin_code,
		     ];
    }
}
