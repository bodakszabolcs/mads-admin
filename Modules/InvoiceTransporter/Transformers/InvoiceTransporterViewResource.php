<?php

namespace Modules\InvoiceTransporter\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class InvoiceTransporterViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "address" => $this->address,
		    "product" => $this->product,
		    "payment_method" => $this->payment_method,
		    "payment_deadline" => $this->payment_deadline,
		    "completion" => $this->completion,
		    "vat" => $this->vat,
		    "multiplier" => $this->multiplier,
		    "contact" => $this->contact,
		    "email" => $this->email,
		    "phone" => $this->phone,
		    "web" => $this->web,
		    "identifier" => $this->identifier,
		    "password" => $this->password,
		    "pin_code" => $this->pin_code,
		"selectables" => [
		]
		     ];
    }
}
