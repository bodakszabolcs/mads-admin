import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import InvoiceTransporter from '../components/InvoiceTransporter'
import InvoiceTransporterList from '../components/InvoiceTransporterList'
import InvoiceTransporterCreate from '../components/InvoiceTransporterCreate'
import InvoiceTransporterEdit from '../components/InvoiceTransporterEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'invoice-transporters',
                component: InvoiceTransporter,
                meta: {
                    title: 'InvoiceTransporters'
                },
                children: [
                    {
                        path: 'index',
                        name: 'InvoiceTransporterList',
                        component: InvoiceTransporterList,
                        meta: {
                            title: 'InvoiceTransporters',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/invoice-transporters/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoice-transporters/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'InvoiceTransporterCreate',
                        component: InvoiceTransporterCreate,
                        meta: {
                            title: 'Create InvoiceTransporters',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoice-transporters/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'InvoiceTransporterEdit',
                        component: InvoiceTransporterEdit,
                        meta: {
                            title: 'Edit InvoiceTransporters',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/invoice-transporters/index'
                        }
                    }
                ]
            }
        ]
    }
]
