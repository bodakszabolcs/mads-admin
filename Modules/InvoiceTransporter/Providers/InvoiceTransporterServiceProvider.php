<?php

namespace Modules\InvoiceTransporter\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;
use Modules\InvoiceTransporter\Observers\InvoiceTransporterObserver;

class InvoiceTransporterServiceProvider extends ModuleServiceProvider
{
    protected $module = 'invoicetransporter';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        InvoiceTransporter::observe(InvoiceTransporterObserver::class);
    }
}
