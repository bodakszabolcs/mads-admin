<?php

namespace Modules\InvoiceTransporter\Observers;

use Modules\InvoiceTransporter\Entities\InvoiceTransporter;

class InvoiceTransporterObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function saved(InvoiceTransporter $model)
    {
        InvoiceTransporter::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function created(InvoiceTransporter $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function updated(InvoiceTransporter $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function deleted(InvoiceTransporter $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function restored(InvoiceTransporter $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\InvoiceTransporter\Entities\InvoiceTransporter  $model
     * @return void
     */
    public function forceDeleted(InvoiceTransporter $model)
    {
        //
    }
}
