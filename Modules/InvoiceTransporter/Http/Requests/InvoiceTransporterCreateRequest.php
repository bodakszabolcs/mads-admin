<?php

namespace Modules\InvoiceTransporter\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceTransporterCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Név'),
            'address' => __('Cím'),
            'product' => __('Termék/szolgáltatás'),
            'payment_method' => __('Fizetés módja'),
            'payment_deadline' => __('Fizetési határidő'),
            'completion' => __('Teljesítés'),
            'vat' => __('Áfa'),
            'multiplier' => __('Szorzó'),
            'contact' => __('Kapcsolattartó'),
            'email' => __('E-mail'),
            'phone' => __('Telefon'),
            'web' => __('Web'),
            'identifier' => __('Azonositó'),
            'password' => __('Jelszó'),
            'pin_code' => __('PIN'),

        ];
    }

    public function authorize()
    {
        return true;
    }
}
