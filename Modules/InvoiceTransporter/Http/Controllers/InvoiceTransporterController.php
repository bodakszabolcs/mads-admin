<?php

namespace Modules\InvoiceTransporter\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;
use Illuminate\Http\Request;
use Modules\InvoiceTransporter\Http\Requests\InvoiceTransporterCreateRequest;
use Modules\InvoiceTransporter\Http\Requests\InvoiceTransporterUpdateRequest;
use Modules\InvoiceTransporter\Transformers\InvoiceTransporterViewResource;
use Modules\InvoiceTransporter\Transformers\InvoiceTransporterListResource;

class InvoiceTransporterController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new InvoiceTransporter();
        $this->viewResource = InvoiceTransporterViewResource::class;
        $this->listResource = InvoiceTransporterListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = InvoiceTransporterCreateRequest::class;
        $this->updateRequest = InvoiceTransporterUpdateRequest::class;
    }

}
