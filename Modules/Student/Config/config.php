<?php

return [
    'name' => 'Student',

                 'menu_order' => 25,

                 'menu' => [
                     [

                      'icon' =>'fa fa-users',

                      'title' =>'Diákok',

                      'route' =>'/'.env('ADMIN_URL').'/student/index',

                     ],
                     [

                         'icon' =>'fa fa-search',

                         'title' =>'Diák kereső',

                         'route' =>'/'.env('ADMIN_URL').'/student/search',

                     ],
                     [

                         'icon' =>'fa fa-link',

                         'title' =>'Elküldött regisztrációs linkek',

                         'route' =>'/'.env('ADMIN_URL').'/student/register',

                     ],
                     [

                         'icon' =>'fa fa-file',

                         'title' =>'Aláírásra váró dokumentumok',

                         'route' =>'/'.env('ADMIN_URL').'/student/my-documents',

                     ]

                 ]
];
