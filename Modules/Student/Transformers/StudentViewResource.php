<?php

namespace Modules\Student\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\DB;
use Modules\AreaType\Entities\AreaType;
use Modules\Attribute\Entities\Attribute;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Language\Entities\Language;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Semester\Entities\Semester;
use Modules\Student\Entities\Student;
use Modules\User\Entities\User;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkLocation\Entities\WorkLocation;


class StudentViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "identity_card" => $this->identity_card,
		    "birth_location" => $this->birth_location,
		    "birth_date" => $this->birth_date,
		    "mothers_name" => $this->mothers_name,
		    "nationality" => $this->nationality,
		    "passport" => $this->passport,
		    "eu_expire_date" => $this->eu_expire_date,
		    "exit_date" => $this->exit_date,
		    "gender" => $this->gender,
		    "phone" => $this->phone,
		    "email" => $this->email,
		    "country_id" => $this->country_id,
		    "zip" => $this->zip,
		    "city_id" => $this->city_id,
		    "street" => $this->street,
		    "area_type_id" => $this->area_type_id,
		    "house_number" => $this->house_number,
		    "building" => $this->building,
		    "stairs" => $this->stairs,
		    "level" => $this->level,
		    "door" => $this->door,
		    "notification_address" => $this->notification_address,
		    "bank_account_number" => $this->bank_account_number,
		    "tax_number" => $this->tax_number,
		    "taj" => $this->taj,
		    "date_of_entry" => $this->date_of_entry,
		    "driver_licence" => $this->driver_licence,
		    "om_code" => $this->om_code,
		    "nav_declaration" => $this->nav_declaration,
		    "membership_agreement" => $this->membership_agreement,
		    "employment_contract" => $this->employment_contract,
		    "part_ticket" => $this->part_ticket,
		    "prefered_work_location" => $this->prefered_work_location,
		    "newsletter" => $this->newsletter,
		    "work_categories" => $this->work_categories,
		    "comment" => $this->comment,
            "certificate" => optional($this->certificate()->orderBy('end','desc')->first())->end > date('Y-m-d'),
		    "ability_to_work" => $this->ability_to_work,
		    "registration_token_expire" => $this->registration_token_expire,
		    "registration_approved" => $this->registration_approved,
		    "registration_created" => $this->registration_created,
		    "eu_taj" => $this->eu_taj,
		    "student_number" => $this->student_number,
            "semesterCard"=> $this->getSemesterCard(),
            "semesterCertificate" => $this->getSemesterCertificate(),
            "getStudentYear" =>$this->getStudentYear(),
            "year25"=> date('Y-m-d',strtotime($this->birth_date." +25 year")),
            "cv"=> $this->cv,
            "cv_name"=> $this->cv_name,
            "cv_date"=> date('Y.m.d H:i',strtotime($this->cv_date)),
		    "avatar" => $this->avatar,
				"selectables" => [
                    "users" =>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
					"genders" => json_decode(json_encode(Student::$gender)),
					"countries" =>Country::all()->pluck('name','id'),
					"cities" => City::all()->pluck('name','id'),
					"area_types"=> AreaType::all()->pluck('name','id'),
					"languages"=> Language::all()->pluck('name','id'),
					"nationalities"=> Nationality::all()->pluck('name','id'),
                    "work_categories"=> WorkCategory::whereNull('parent_id')->get()->pluck('name','id'),
                    "work_location" => WorkLocation::all()->pluck('name','id'),

				]
		     ];
    }
    public function getSemesterCard(){
        $semester = Semester::where('type',1)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($semester as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }
        return $response;
    }
    public function getStudentYear(){
        $tanev = Semester::where('type',2)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($tanev as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }

        return $response;
    }
    public function getSemesterCertificate(){


        $semester = Semester::where('type',3)->where('active',1)->orderBy('start')->limit(2)->get();
        $response = [];
        foreach ($semester as $t){
            $data = [
                'key'   => $t->key,
                'start' => $t->start,
                'end'   => $t->end,
                'name'  => $t->name
            ];
            $response[$t->key]=$data;
        }
        return $response;
    }
}
