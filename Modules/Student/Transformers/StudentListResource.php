<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Student\Entities\StudentWorkSafety;


class StudentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "student_number" => $this->student_number,
		    "name" => $this->name,
		    "phone" => $this->phone,
		    "email" => $this->email,
		    "tax_number" => $this->tax_number,
		    "date_of_entry" => $this->date_of_entry,
		    "nav_declaration" => $this->nav_declaration,
		    "membership_agreement" => $this->membership_agreement,
		    "cv" => $this->cv,
		    "employment_contract" => $this->employment_contract,
		    "prefered_work_location" => $this->prefered_work_location,
		    "newsletter" => ConstansHelper::bool($this->newsletter),
		    "work_categories" => $this->work_categories,
            "type"=>($this->type)?\Arr::get(StudentWorkSafety::$types,$this->type):"",
            "value"=>($this->value)?$this->value:"",
            "expire_safety"=>($this->expire_safety)?$this->expire_safety:"",
            "dept"=>($this->dept)?$this->dept:"",
		    "comment" => $this->comment,
            "expire" => ($this->expire< date('Y-m-d')),
            "underage" => $this->birth_date > date('Y-m-d',strtotime('-18 year')),
            "registration_web" =>ConstansHelper::formatShortDate($this->exit_date),
            "exit_date" =>ConstansHelper::formatShortDate($this->exit_date),
            "created_at" =>ConstansHelper::formatShortDate($this->created_at),
            "membership" =>($this->nav_declaration)?ConstansHelper::formatShortDate($this->nav_declaration):'',
            "registration_date" =>($this->registration_created)?ConstansHelper::formatShortDate($this->registration_created):'',
            "registration_created" =>($this->registration_created)?ConstansHelper::formatShortDate($this->registration_created):'',
            "registration_link_sended" =>($this->registration_link_sended)?ConstansHelper::formatShortDate($this->registration_link_sended):'',

        ];
    }
}
