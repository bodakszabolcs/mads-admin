<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class StudentLanguageListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "language_id" => $this->language_id,
            "student_id" => $this->student_id,
            "language_name" => $this->language_name,
            "value" => $this->value,

        ];
    }
}
