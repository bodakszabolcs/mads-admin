<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\AreaType\Entities\AreaType;
use Modules\Attribute\Entities\Attribute;
use Modules\City\Entities\City;
use Modules\Country\Entities\Country;
use Modules\Language\Entities\Language;
use Modules\Nationality\Entities\Nationality;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentCard;
use Modules\Student\Entities\StudentCertificate;
use Modules\User\Entities\User;


class StudentSchoolViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "school"=>optional($this->school)->name,
            "school_address"=>optional($this->school)->address,
            "education_area" => optional($this->educationArea)->name,
            "education_level" => optional($this->educationLevel)->name,
            "education_type" => Arr::get(Student::$EDUCATIONTYPES,$this->education_type),
            "start" => ConstansHelper::formatMonth($this->start),
            "end" => ConstansHelper::formatMonth($this->end),
            "certificates" => StudentCertificate::where('student_id','=',$this->student_id)->where('school_id','=',$this->id)->orderBy('end','desc')->get(),
            "cards" => StudentCard::where('student_id','=',$this->student_id)->where('school_id','=',$this->id)->orderBy('temporary_student_card_expire','desc')->get(),
            ];

    }
}
