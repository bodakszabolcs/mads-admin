<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Student\Entities\StudentWorkSafety;


class StudentDocumentViewListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "student_number" => $this->student_number,
		    "student_name" => $this->student_name,
		    "student_id" => $this->student_id,
            "name" => $this->name,
            "type" => $this->document_type_id,
            "file" => $this->file,
            "created" => ConstansHelper::formatDate($this->created_at),
            "signed_date" => ConstansHelper::formatDate($this->signed_date),
            "mads_signed" => ConstansHelper::formatDate($this->mads_signed),

        ];
    }
}
