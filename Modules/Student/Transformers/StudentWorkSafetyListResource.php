<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
use Modules\Student\Entities\StudentWorkSafety;


class StudentWorkSafetyListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "type" => Arr::get(StudentWorkSafety::$types,$this->type),
            "student_id" => $this->student_id,
            "expire" => $this->expire,
            "created" => ConstansHelper::formatDate($this->created_at),
            "date" => $this->date,
            "value" => $this->value,
            "size" => $this->size,
            "file" => $this->file,
            "price" => $this->price,
            "approved" => $this->approved,
            "approve_is_required" => $this->approve_is_required,
        ];
    }
}
