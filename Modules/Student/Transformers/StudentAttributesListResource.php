<?php

namespace Modules\Student\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class StudentAttributesListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "attribute_id" => $this->attribute_id,
		    "student_id" => $this->student_id,
		    "attribute_name" => $this->attribute_name,
            "value"=>$this->value
        ];
    }
}
