<?php

namespace Modules\Student\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Student\Entities\Student;
use Modules\Student\Observers\StudentObserver;

class StudentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'student';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Student::observe(StudentObserver::class);
    }
}
