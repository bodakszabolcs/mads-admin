<?php

namespace Modules\Student\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CardCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
			'temporary_student_card_expire' => 'required',
        ];
    }

    public function attributes()
        {
            return [
                'school_id' =>  __('Iskola'),
                'temporary_student_card_expire' =>  __('Lejárat dátuma'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
