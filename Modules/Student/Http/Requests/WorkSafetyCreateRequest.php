<?php

namespace Modules\Student\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkSafetyCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'expire' => 'required',
        ];
    }

    public function attributes()
        {
            return [
                'type' =>  __('Jelleg'),
                'value' =>  __('Érték'),
                'expire' =>  __('Lejárat dátuma'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
