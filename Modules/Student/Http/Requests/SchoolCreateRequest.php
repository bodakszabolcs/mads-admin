<?php

namespace Modules\Student\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'school_id' => 'required',
			'education_type' => 'required',
			'education_area_id' => 'required',
			'start' => 'required',
			'end' => 'required',

			'education_level_id' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'school_id' =>  __('Iskola'),
                'education_type' =>  __('Képzés típusa'),
                'education_area_id' =>  __('Képzés terület'),
                'start' =>  __('Kezdés'),
                'end' =>  __('Várható befejezés'),
                'education_level_id' =>  __('Képzési szint'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
