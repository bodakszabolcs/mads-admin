<?php

namespace Modules\Student\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id' => 'required',
			'student_id' => 'required',
			'department' => 'required_with:has_department',
			'schedules.*.start' => 'required_with:schedules.*.have_time',
            'schedules.*.end' => 'required_with:schedules.*.have_time',
        ];
    }

    public function attributes()
        {
            return [
                'project_id' =>  __('Projekt'),
                'student_id' =>  __('Diák'),
                'department' =>  __('Részleg'),
                'has_department' => __('Részlegek'),
                'schedules.*.start' =>  __('Kezdés'),
                'schedules.*.end' =>  __('Végzés'),
                'schedules.*.have_time' =>  __('Ráér'),



            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
