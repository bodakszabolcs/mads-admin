<?php

namespace Modules\Student\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //'birth_date' => 'required',
            //'gender' => 'required',
            'email' => 'required',
            //'street' => 'required',
            //'area_type_id' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'name' => __('Név'),
            'identity_card' => __('Személyi igazolvány szám'),
            'birth_location' => __('Születési hely'),
            'birth_date' => __('Születési dátum'),
            'mothers_name' => __('Anyja leánykori neve'),
            'nationality' => __('Állampolgársága'),
            'passport' => __('Útlevélszám (ha nincs személyi igazolvány)'),
            'eu_expire_date' => __('EÜ lejárat dátuma'),
            'exit_date' => __('Kilépés dátuma'),
            'gender' => __('Neme'),
            'phone' => __('Mobil'),
            'email' => __('E-mail'),
            'country_id' => __('Ország'),
            'zip' => __('Irányítószám'),
            'city_id' => __('Város'),
            'street' => __('Utca'),
            'area_type_id' => __('Közterület jellege (utca, út, tér, stb.)'),
            'house_number' => __('Házszám'),
            'building' => __('Épület'),
            'stairs' => __('Lépcsőház'),
            'level' => __('Emelet'),
            'door' => __('Ajtó'),
            'notification_address' => __('Értesítési cím'),
            'bank_account_number' => __('Bankszámlaszám'),
            'tax_number' => __('Adószám'),
            'taj' => __('Tajszám'),
            'date_of_entry' => __('Belépés dátuma'),
            'nav_declaration' => __('Nav bejelentés'),
            'membership_agreement' => __('Tagsági megállapodás'),
            'employment_contract' => __('Munkaszerződés'),
            'part_ticket' => __('Részjegy fizetve'),
            'prefered_work_location' => __('Hol szeretne dolgozni?'),
            'newsletter' => __('Hírlevelet kér?'),
            'work_categories' => __('Milyen munka érdekli'),
            'comment' => __('Megjegyzés'),
            'avatar' => __('Profilkép'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
