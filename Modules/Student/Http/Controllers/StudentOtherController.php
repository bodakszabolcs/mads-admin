<?php

namespace Modules\Student\Http\Controllers;

use App\Helpers\ConstansHelper;
use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Industry;
use Modules\Fixing\Entities\Fixing;
use Modules\MonthlyPayment\Entities\MonthlyPayment;
use Modules\Payroll\Entities\Payroll;
use Modules\Project\Entities\ProjectStudent;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentAttributes;
use Modules\Student\Entities\StudentDocumentView;
use Modules\Student\Entities\StudentLanguages;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\Student\Http\Requests\AttributeCreateRequest;
use Modules\Student\Http\Requests\LanguageCreateRequest;
use Modules\Student\Http\Requests\WorkSafetyCreateRequest;
use Modules\Student\Transformers\StudentAttributesListResource;
use Modules\Student\Transformers\StudentDocumentViewListResource;
use Modules\Student\Transformers\StudentLanguageListResource;
use Modules\Student\Transformers\StudentWorkSafetyListResource;

class StudentOtherController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);

    }
    public function listLanguage(Request $request, $id){
        $studentLanguages = StudentLanguages::where('student_id',$id)->leftJoin('languages','language_id','languages.id')->select(DB::raw('student_languages.*,languages.name as language_name'))->get();
        return StudentLanguageListResource::collection($studentLanguages);
    }
    public function listMyDocuments(Request $request){
        $list = new StudentDocumentView();
        $list = $list->searchInModel($request->input());

        if (!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {
            $list = $list->where(function ($query) {
                $query->where('user_id', '=', \Auth::id())->orWhere('replacement_user_id', '=', \Auth::id());
            });
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }



        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }
        $filters =new StudentDocumentView();
        return StudentDocumentViewListResource::collection($list)->additional(['filters' => $filters->getFilters()]);
    }

    public function addLanguage(LanguageCreateRequest $request, $id){
        $studentLanguage = new StudentLanguages();
        $studentLanguage->fill($request->all());
        $studentLanguage->student_id = $id;
        $studentLanguage->save();
        $student= Student::where('id',$id)->first();
        $student->updateSendingBlue();
        return $this->listLanguage($request,$id);
    }
    public function deleteLanguage(Request  $request, $id){
        try {
            $studentLanguage = StudentLanguages::where('id',$id)->firstOrFail();
            $studentLanguage->delete();
            $student= Student::where('id',$id)->first();
            $student->updateSendingBlue();
            return $this->listLanguage($request,$id);
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }

    public function listWorkSafety(Request  $request, $id){
        $studentWorkSafety = StudentWorkSafety::where('student_id',$id)->orderBy('expire','desc')->get();
        return StudentWorkSafetyListResource::collection($studentWorkSafety);
    }
    public function addWorkSafety(WorkSafetyCreateRequest  $request, $id){
        $studentWorkSafety = new StudentWorkSafety();
        $studentWorkSafety->fill($request->all());
        $studentWorkSafety->student_id = $id;
        $studentWorkSafety->save();
        return $this->listWorkSafety($request,$id);
    }
    public function deleteWorkSafety(Request $request, $id){
        try {

            $studentWorkSafety = StudentWorkSafety::where('id',$id)->firstOrFail();
            $studentWorkSafety->delete();
            return $this->listWorkSafety($request,$id);
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function listAttribute(Request  $request, $id){
        $studentAttribute = StudentAttributes::where('student_id',$id)->leftJoin('attributes','attribute_id','attributes.id')->select(DB::raw('student_attributes.*,attributes.name as attribute_name'))->get();
        return StudentAttributesListResource::collection($studentAttribute);
    }
    public function addAttribute(AttributeCreateRequest  $request, $id){
        $studentAttribute = new StudentAttributes();
        $studentAttribute->fill($request->all());
        $studentAttribute->student_id = $id;
        $studentAttribute->save();
        return $this->listAttribute($request,$id);
    }
    public function deleteAttribute(Request  $request, $id){
        try {
            $studentAttribute = StudentAttributes::where('id',$id)->firstOrFail();
            $studentAttribute->delete();
            return $this->listAttribute($request,$id);
        }catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getStudentFinance(Request $request,$id,$month){
        $month = date('Y-m',strtotime($month));
        $fixingsCompany =   Fixing::where('student_id',$id)->where('month','=',str_replace('-','',$month))->groupBy('industry_id')->orderBy('date','asc')->get();
        $student = Student::where('id',$id)->first();
        $response =['fixings'=>[],'days'=>[],'summation'=>[]];
        if(!$fixingsCompany){
            return response()->json($response,'200');
        }
        $start = date('Y-m-01',strtotime($month));
        $end = date('Y-m-t',strtotime($month));
        while ($start<=$end){
            $response['days'][$start]=[
                'date'=>$start,
                'name'=>Arr::get(ConstansHelper::$days,date('N',strtotime($start))),
                'work'=>false
            ];
            $start = date('Y-m-d',strtotime($start.' +1 day'));
        }
        $sum_hour =0;
        $sum_price=0;
        foreach ($fixingsCompany as $fc) {
            $fixings = Fixing::where('student_id', $id)->where('month', '=', str_replace('-', '', $month))->where('industry_id','=',$fc->industry_id)->orderBy('date', 'asc')->get();
            $company = Company::where('id',$fc->company_id)->first();
            $industry = Industry::where('id',$fc->industry_id)->first();
            $response['fixings'][$fc->industry_id]['company_name']=$company->name.' - '.$industry->name;
            $sum= [
                'date' => 'Összesen',
                'start' => '',
                'end' => '',
                'hour' => 0,
                'fix' => 0,
                'price' => 0,
                'szja' => 0,
                'price_net' => 0,
            ];

            foreach ($fixings as $f) {
                $response['fixings'][$fc->industry_id]['data'][$f->date] = [
                    'date' => $f->date,
                    'start' => $f->start,
                    'end' => $f->end,
                    'hour' => $f->hour + $f->bonus_hour,
                    'fix' => $f->fix,
                    'price' => $f->student_price,
                    'szja' => $f->student_price-$this->calcNet($student,str_replace('-', '', $month),$f->student_price),
                    'price_net' => $this->calcNet($student,str_replace('-', '', $month),$f->student_price),
                ];
                $response['days'][$f->date]['work']=true;
                $sum['hour']+=$f->hour + $f->bonus_hour;
                $sum['fix']+=$f->fix;
                $sum['price']+= $f->student_price;
                $sum['szja']+=$f->student_price-$this->calcNet($student,str_replace('-', '', $month),$f->student_price);
                $sum['price_net']+=$this->calcNet($student,str_replace('-', '', $month),$f->student_price);
                $sum_price +=$f->student_price;
                $sum_hour += $f->hour + $f->bonus_hour;
            }
            $response['fixings'][$fc->industry_id]['data']['Összesen']=$sum;
        }
        $response['days'] = array_chunk($response['days'],7);
        $monthly = MonthlyPayment::where('month',str_replace('-','',$month))->where('student_id',$id)->get();
        if($monthly){
            $response['monthly']=[
                'price'=>0,
                'szja'=>0,
                'price_net'=>0
            ];
            foreach ($monthly as $m){
                $response['monthly']['price']+=$m->price;
                $response['monthly']['szja']+=$m->price-$this->calcNet($student,str_replace('-', '', $month),$m->price);
                $response['monthly']['price_net']+=$this->calcNet($student,str_replace('-', '', $month),$m->price);
            }
        }
        $response['summation']['Összesen']= [
            'hour'=>$sum_hour,
            'price'=>$sum_price,
            'price_net'=>$this->calcNet($student,str_replace('-', '', $month),$sum_price),
            'szja'=>$sum_price-$this->calcNet($student,str_replace('-', '', $month),$sum_price),

        ];
        $response['summation']['Hóközi kifizetés']= [
            'hour'=>'',
            'price'=>$response['monthly']['price'],
            'szja'=>$response['monthly']['szja'],
            'price_net'=>$response['monthly']['price_net'],
        ];
        $response['summation']['Részjegy']= [
            'hour'=>'',
            'price'=>'',
            'szja'=>'',
            'price_net'=>0,
        ];
        $student =  Student::where('id',$id)->first();
        if($student->part_ticket){
            $response['summation']['Részjegy']['price_net']=2000;
        }
        $response['summation']['Mindösszesen']= [
            'hour'=>$sum_hour,
            'price'=> $response['summation']['Összesen']['price']-$response['summation']['Hóközi kifizetés']['price'],
            'szja'=>$response['summation']['Összesen']['szja']+$response['summation']['Hóközi kifizetés']['szja'],
            'price_net'=>$response['summation']['Összesen']['price_net']-$response['summation']['Hóközi kifizetés']['price_net'] -$response['summation']['Részjegy']['price_net'],
        ];
        return response()->json($response,200);

    }
    public function studentWorks(Request  $request,$id){
        $projects = ProjectStudent::where('student_id','=',$id)->leftJoin('projects','projects.id','project_id')->where('status_id','=',2)->leftJoin('companies','companies.id','=','projects.company_id')->leftJoin('company_industries','company_industries.id','projects.industry_id')
            ->select(DB::raw("companies.name as company_name,company_industries.name as industry_name,companies.id as company_id,company_industries.id as industry_id"))->where('student_id',$id)
            ->get();
            return response()->json(['data'=>$projects],200);
    }
    public function studentProjects(Request  $request,$id){
        $projects = ProjectStudent::where('student_id','=',$id)->leftJoin('projects','projects.id','project_id')
            ->leftJoin('companies','companies.id','=','projects.company_id')->leftJoin('company_industries','company_industries.id','projects.industry_id')
            ->leftJoin('users','users.id','=','projects.user_id')
            ->leftJoin('project_status','status_id','=','project_status.id')
            ->select(DB::raw("companies.name as company_name,start_date,company_industries.name as industry_name,projects.comment as project_name, projects.id as project_id,project_status.name as status_name,concat(users.lastname,' ',users.firstname) as pm"))->where('student_id',$id)
            ->get();
        return response()->json(['data'=>$projects],200);
    }
    function calcNet($student,$month,$price){
        if(date("Y-m-d",strtotime($student->birth_date."+ 25 year"))<date("Y-m-d",strtotime($month.'01')) || $student->afa)
        {
            return $price*0.85;
        }
        if($price> Payroll::$underAgeLimit){
            return Payroll::$underAgeLimit+(($price-Payroll::$underAgeLimit)*0.85);
        }
        return  $price;
    }
}
