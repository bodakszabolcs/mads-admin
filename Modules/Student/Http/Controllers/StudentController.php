<?php

namespace Modules\Student\Http\Controllers;

use App\Exports\BaseArrayExport;
use App\Helpers\PdfHelper;
use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\SaveStatistic;
use App\Mail\RegistrationEmail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Str;
use Modules\City\Entities\City;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Forms;
use Modules\Company\Entities\Industry;
use Modules\EducationArea\Entities\EducationArea;
use Modules\EducationLevel\Entities\EducationLevel;

use Modules\Email\Entities\Email;
use Modules\Fixing\Entities\Fixing;
use Modules\Language\Entities\Language;
use Modules\Project\Entities\ProjectStudent;
use Modules\Realization\Entities\Realization;
use Modules\Realization\Http\Requests\RealizationCreateRequest;
use Modules\Realization\Transformers\RealizationListResource;
use Modules\Realization\Transformers\RealizationStudentListResource;
use Modules\School\Entities\School;
use Modules\Student\Entities\Student;
use Illuminate\Http\Request;
use Modules\Student\Entities\StudentCard;
use Modules\Student\Entities\StudentCertificate;
use Modules\Student\Entities\StudentDocuments;
use Modules\Student\Entities\StudentSchool;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\Student\Http\Requests\CardCreateRequest;
use Modules\Student\Http\Requests\DocumentCreateRequest;
use Modules\Student\Http\Requests\SchoolCreateRequest;
use Modules\Student\Http\Requests\StudentCreateRequest;
use Modules\Student\Http\Requests\StudentUpdateRequest;
use Modules\Student\Transformers\StudentDocumentListResource;
use Modules\Student\Transformers\StudentSchoolViewResource;
use Modules\Student\Transformers\StudentViewResource;
use Modules\Student\Transformers\StudentListResource;
use Modules\User\Entities\User;
use Modules\User\Http\Requests\ChangePasswordRequest;
use Modules\User\Notifications\VerifyEmail;
use Modules\WorkCategory\Entities\WorkCategory;
use Modules\WorkLocation\Entities\WorkLocation;

class StudentController extends AbstractLiquidController
{
    public $search=[

    ];
    public $search2=[

    ];
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->search = [
            [
                'name' => 'gender',
                'title' => 'Nem',
                'type' => 'select',
                'data'=> json_decode(json_encode(Student::$gender))
            ],
            [
                'name' => 'eu',
                'title' => 'EÜ kiskönyv, érvényesség',
                'type' => 'date',
            ],
            [
                'name' => 'eu_expire',
                'title' => 'EÜ kiskönyv lejár',
                'type' => 'select',
                'data'=> ['1'=>'1 hónap mulva','2'=>'2 hónap mulva']
            ],

            [
                'name' => 'ability_to_work',
                'title' => 'Megváltozott munkaképességű',
                'type' => 'select',
                'data'=> Student::$TRUEFALSE
            ],
            [
                'name' => 'education_type',
                'title' => 'Képzési forma',
                'type' => 'select',
                'data'=> Student::$EDUCATIONTYPES
            ],

            [
                'title' => 'NAV bejelentés',
                'name' => 'nav_registration',
                'type' => 'select',
                'data'=> Student::$TRUEFALSE
            ],
            [
                'title' => 'Tag',
                'name' => 'registration_backend',
                'type' => 'select',
                'data'=> Student::$TRUEFALSE
            ],
            [
                'title' => 'Érvényes jogviszony',
                'name' => 'student_certificate',
                'type' => 'select',
                'data'=> Student::$TRUEFALSE
            ],
            [
                'name' => 'student_certificate_expire',
                'title' => 'Jogviszony lejár',
                'type' => 'select',
                'data'=> ['1'=>'1 hónap mulva','2'=>'2 hónap mulva']
            ],
            [
                'name' => 'company_id',
                'title' => 'Cégnél dolgozik (3 hónap)',
                'type' => 'select',
                'data'=> Company::all()->pluck('name','id')->toArray()
            ],
            [
                'title' => 'Hol szeretne dolgozni?',
                'name' => 'prefered_work_location',
                'type' => 'select_checkbox',
                'data'=> WorkLocation::all()->pluck('name','id')
            ],
            [
                'title' => 'Milyen munka érdekli?',
                'name' => 'work_categories',
                'type' => 'select_checkbox',
                'data'=> WorkCategory::whereNull('parent_id')->get()->pluck('name','id')
            ],
            [
                'title' => 'Képzés kategória',
                'name' => 'education_area',
                'type' => 'select_checkbox',
                'data'=> EducationArea::get()->pluck('name','id')
            ],
            [
                'title' => 'Település',
                'name' => 'city_id',
                'type' => 'select',
                'data'=> City::get()->pluck('name','id')
            ],
            [
                'title' => 'Képzés szint',
                'name' => 'education_level',
                'type' => 'select_checkbox',
                'data'=> EducationLevel::get()->pluck('name','id')
            ],
            [
                'title' => 'Megyjegyzés',
                'name' => 'comment',
                'type' => 'text'
            ],
            [
                'title' => 'Irányítószám',
                'name' => 'zip',
                'type' => 'text'
            ],
            [
                'title' => 'Nyelv ismeret',
                'name' => 'languages',
                'type' => 'select_checkbox',
                'data'=> Language::get()->pluck('name','id')
            ],
            [
                'title' => 'Van tapasztalata',
                'name' => 'fixings',
                'type' => 'select_checkbox',
                'data'=> WorkCategory::whereNotNull('parent_id')->get()->pluck('name','id')
            ],
            [
                'title' => 'Hírlevél?',
                'name' => 'newsletter',
                'type' => 'select',
                'data'=> Student::$TRUEFALSE
            ],
            [
                'title' => 'Regisztráció',
                'name' => 'created_at',
                'type' => 'date_interval'
            ],

        ];
        $this->search2 = [
            [
                'name' => 'search',
                'title' => 'Keresés',
                'type' => 'text'
            ],
            [
                'name' => 'register_user_id',
                'title' => 'Projekt menedzser',
                'type' => 'select',
                'data'=> User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id")
            ],

        ];
        $this->model = new Student();
        $this->viewResource = StudentViewResource::class;
        $this->listResource = StudentListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = StudentCreateRequest::class;
        $this->updateRequest = StudentUpdateRequest::class;
    }
    public function create(Request $request)
    {
        if ($this->createRequest) {
            app()->make($this->createRequest);
        }
        $s = Student::where("email",$request->input("email"))->first();
        if($s){
            return response()->json(['data' => ['message' => "Az email cím már foglalt"]], $this->errorStatus);
        }
        $this->model->fillAndSave($request->all());

        return new $this->viewResource($this->model);
    }

    public function destroy($id, $auth = null)
    {
        $ps =ProjectStudent::where("student_id",$id)->where("status_id",2)->get();
        if(sizeof($ps)>0){
            return response()->json(['data' => ['message' => "A diák nem törölhető, mert dolgozik"]], $this->errorStatus);
        }
        $ps =Fixing::where("student_id",$id)->get();
        if(sizeof($ps)>0){
            return response()->json(['data' => ['message' => "A diák nem törölhető, mert van rá rögzítve"]], $this->errorStatus);
        }
        $this->model = $this->model->where('id', '=', $id);

        try {
            if ($auth == null) {
                $this->model = $this->model->firstOrFail();
            } else {
                $this->model = $this->model->where('user_id', '=', $auth)->firstOrfail();
            }
        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
        }
        $this->model->delete();
        $this->model->updateSendingBlue();

        return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
    }
    public function studentListRealization(Request $request){
        $realization = Realization::where('end','>=',date('Y-m-d'))
            ->leftJoin('students','students.id','=','student_id')->leftJoin('work_categories','work_categories.id','=','work')
            ->select(DB::raw('realizations.*,students.name as name, students.phone as phone, students.email as email,students.birth_date as birth_date, work_categories.name as work_name'));
        if($request->has('start')){
            $realization = $realization->where('start','<=',$request->input('start'))->where('end','>=',$request->input('start'));
        }
        if($request->has('end')){
            $realization = $realization->where('start','<=',$request->input('end'))->where('end','>=',$request->input('end'));
        }
        if($request->has('monday') &&  !empty($request->input('monday'))){
            $realization = $realization->where('monday','=',$request->input('monday'));
        }
        if($request->has('tuesday')  &&  !empty($request->input('tuesday'))){
            $realization = $realization->where('tuesday','=',$request->input('tuesday'));
        }
        if($request->has('wednesday')  &&  !empty($request->input('wednesday'))){
            $realization = $realization->where('wednesday','=',$request->input('wednesday'));
        }
        if($request->has('thursday')  &&  !empty($request->input('thursday'))) {
            $realization = $realization->where('thursday','=',$request->input('thursday'));
        }
        if($request->has('friday')  &&  !empty($request->input('friday'))){
            $realization = $realization->where('friday','=',$request->input('friday'));
        }
        if($request->has('saturday')  &&  !empty($request->input('saturday'))){
            $realization = $realization->where('saturday','=',$request->input('saturday'));
        }
        if($request->has('sunday')  &&  !empty($request->input('sunday'))){
            $realization = $realization->where('sunday','=',$request->input('sunday'));
        }
        if($request->has('prefered_work_location')  &&  !empty($request->input('prefered_work_location'))){
            $data = explode(',',$request->input('prefered_work_location'));
            $realization = $realization->where(function($query)use($data){
                foreach ($data as $v) {
                    $query =$query->orWhere('location', 'LIKE', '%"' . $v . '"%');
                    }

            });

        }
        if($request->has('work_categories')  &&  !empty($request->input('work_categories'))){
            $data = explode(',',$request->input('work_categories'));
            $realization = $realization->where(function($query)use($data){
                foreach ($data as $v) {
                    $query =$query->orWhere('realizations.work', 'LIKE',  $v );
                }

            });

        }
        if($request->input('sort')){
            $sort = explode('|',$request->input('sort'));
            $realization = $realization->orderBy($sort[0],$sort[1]);
        }else {
            $realization = $realization->orderBy('end','desc');
        }
        if($request->has('excel')){
           $cats = WorkCategory::whereNull('parent_id')->get()->pluck('name','id');
            $realization= $realization->get()->toArray();
            $excelArray =[];
            foreach ($realization as $k =>$v){
                $excelArray[$k]['name']=$realization[$k]['name'];
                $excelArray[$k]['phone']=$realization[$k]['phone'];
                $excelArray[$k]['email']=$realization[$k]['email'];
                $excelArray[$k]['date']=$realization[$k]['start'].' - '.$realization[$k]['end'];
                $excelArray[$k]['monday'] = $this->getRealicationName($realization[$k]['monday']);
                $excelArray[$k]['tuesday'] = $this->getRealicationName($realization[$k]['tuesday']);
                $excelArray[$k]['wednesday'] = $this->getRealicationName($realization[$k]['wednesday']);
                $excelArray[$k]['thursday'] = $this->getRealicationName($realization[$k]['thursday']);
                $excelArray[$k]['friday'] = $this->getRealicationName($realization[$k]['friday']);
                $excelArray[$k]['saturday'] = $this->getRealicationName($realization[$k]['saturday']);
                $excelArray[$k]['sunday'] = $this->getRealicationName($realization[$k]['sunday']);
                $excelArray[$k]['work'] = array_get($cats, $realization[$k]['work']);
            }
            $export = new BaseArrayExport(['Név','Mobil','E-mail','Dátum','Hé','Ke','Sze','Csüt','Pé','Szo','Vas','Munka',], "Ráérés", $excelArray);

            return $export->download(Str::slug('Ráérés') . '.xlsx');
        }
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $realization = $realization->paginate($pagination)->withPath($request->path());
        return RealizationStudentListResource::collection($realization)->additional(['filters' => [
            [
                'name' => 'start',
                'title' => 'Mettől',
                'type' => 'date',
            ],
            [
                'name' => 'end',
                'title' => 'Meddig',
                'type' => 'date',
            ],
            [
                'title' => 'Hol szeretne dolgozni?',
                'name' => 'prefered_work_location',
                'type' => 'select_checkbox',
                'data'=> WorkLocation::all()->pluck('name','id')
            ],
            [
                'title' => 'Milyen munka érdekli?',
                'name' => 'work_categories',
                'type' => 'select_checkbox',
                'data'=> WorkCategory::whereNull('parent_id')->get()->pluck('name','id')
            ],
            [
                'name' => 'monday',
                'title' => 'Hétfő',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'tuesday',
                'title' => 'Kedd',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'wednesday',
                'title' => 'Szerda',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'thursday',
                'title' => 'Csütörtök',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'friday',
                'title' => 'Péntek',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'saturday',
                'title' => 'Szombat',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
            [
                'name' => 'sunday',
                'title' => 'Vasárnap',
                'type' => 'select',
                'data'=> ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér']
            ],
        ]]);
    }
    private function getRealicationName($number){
        $arr = ['0'=>'Nem ér rá','1'=>'Egész nap ráér','2'=>'Délelőtt ráér','3'=>'Délután ráér'];
        return array_get($arr,$number);
    }
    public function changePassword(ChangePasswordRequest $request) {
        $user = Student::find(Auth::id());

        $user->password = Hash::make($request->input('password'));
        $user->save();

        return parent::show($request, Auth::id());
    }

    public function studentSearch(Request $request){
        $list = new Student();
        if($request->input('gender',null)){
          $list =  $list->where('gender',$request->input('gender',null));
        }
        if($request->input('comment',null)){
            $words = explode(" ",$request->input('comment',null));
             foreach($words as $w){
                 $list =  $list->where('comment',"LIKE","%$w%");
             }

        }
        if($request->input('ability_to_work',null)){
            $list =  $list->where('ability_to_work',$request->input('ability_to_work',null));

        }
        if($request->input('',null)){
            $list =  $list->where('ability_to_work',$request->input('ability_to_work',null));

        }
        if($request->input('city_id',null)){
            $list =  $list->where('city_id',$request->input('city_id',null));

        }
        if($request->input('created_at',null)){
            $data = explode(" - ",$request->input('created_at',null));
            $list =  $list->where('students.registration_created','>=',$data[0])->where('students.registration_created','<=',$data[1]);

        }
        if($request->input('zip',null)){
            $list =  $list->where('zip',$request->input('zip',null));

        }
        if($request->input('newsletter',null)){
              if($request->input('newsletter',null) ==1) {
                  $list = $list->where('newsletter', 1);
              }
            if($request->input('newsletter',null) ==2) {
                $list = $list->where('newsletter', 0);
            }

        }
        if($request->input('eu',null)){
            $list =  $list->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety where deleted_at is null group by student_id,type) as exp'),'exp.student_id','=','students.id')->where('type','=',1)->where('expire','>=',$request->input('eu',null));

        }
        if($request->input('eu_expire',null)){
            if($request->input('eu_expire',null) ==1) {
                $list = $list->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety  where deleted_at is null group by student_id,type) as exp'), 'exp.student_id', '=', 'students.id')->where('type', '=', 1)
                    ->where('expire', '>=', date('Y-m-d'))->where('expire', '<=', date('Y-m-d',strtotime('+1 month')));
            }else{
                $list = $list->leftJoin(DB::raw('(select student_id, max(expire) as expire,type from student_work_safety where deleted_at is null group by student_id,type) as exp'), 'exp.student_id', '=', 'students.id')->where('type', '=', 1)
                    ->where('expire', '>=', date('Y-m-d'))->where('expire', '<=', date('Y-m-d',strtotime('+2 month')));
            }

        }
        if($request->input('student_certificate',null)){
            $list = $list->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates  where deleted_at is null group by student_id ) as cert'), 'cert.student_id', '=', 'students.id');
            if($request->input('student_certificate',null) == 1) {
                $list =  $list->where('cert.end', '>=',date('Y-m-d'));
            }else{
                $list =   $list->where('cert.end', '<', date('Y-m-d'));
            }

        }
        if($request->input('student_certificate_expire',null)){
            $list = $list->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates  where deleted_at is null group by student_id) as cert'), 'cert.student_id', '=', 'students.id');
            if($request->input('student_certificate_expire',null) == 1) {
               $list= $list->where('cert.end', '>=',date('Y-m-d'))->where('cert.end', '<',date('Y-m-d',strtotime('+1 month')));
            }else{
               $list = $list->where('cert.end', '>=', date('Y-m-d'))->where('cert.end', '<',date('Y-m-d',strtotime('+2 month')));
            }

        }
        if($request->input('company_id',null)){
            $date = date('Y-m-d',strtotime('-3 month'));
            $list =  $list->leftJoin(DB::raw("(select student_id from fixings where date >= '$date' and company_id=".$request->input('company_id',null)." group by student_id) as fix"),'fix.student_id','=','students.id')->whereNotNull('fix.student_id');

        }
        if($request->input('education_type',null)){
            $type = $request->input('education_type',null);
            $date = date('Y-m-d');
            $list =  $list->leftJoin(DB::raw("(select student_id from student_schools where education_type=$type and end <= '$date' group by student_id) as est"),'est.student_id','=','students.id')->whereNotNull('est.student_id');
        }
        if($request->input('card_expire',null)){
            $type = $request->input('card_expire',null);
            $date = date('Y-m-d');
            $list =  $list->leftJoin(DB::raw(Student::$studentCardQuery),'sCard.student_id','=','students.id');
            if($type==1){
                $list = $list->where('expire','>=',$date);
            }else{
                $list = $list->where('expire','<=',$date)->orWhereNull('expire');
            }
        }
        if($request->input('card_expire_month',null)){
            $type = $request->input('card_expire',null);
            $date = date('Y-m-d');
            $list =  $list->leftJoin(DB::raw(Student::$studentCardQuery),'sCard.student_id','=','students.id');
            if($request->input('card_expire_month',null)==1){
                $list = $list->where('expire','>=',date('Y-m-d'))->where('expire','>=',date('Y-m-d',strtotime('+1 month')));
            }else{
                $ $list = $list->where('expire','>=',date('Y-m-d'))->where('expire','>=',date('Y-m-d',strtotime('+2 month')));
            }
        }
        if($request->input('nav_registration',null)){
            $type = $request->input('nav_registration',null);
            if($type==1){
                $list = $list->whereNotNull('nav_declaration');
            }else{
                $list = $list->whereNull('nav_declaration');
            }
        }
        if($request->input('registration_backand',null)){
            $type = $request->input('nav_registration',null);
            if($type==1){
                $list = $list->whereNotNull('membership_agreement');
            }else{
                $list = $list->whereNotNull('membership_agreement');
            }
        }
        if($request->input('prefered_work_location',null)){
            $locations = explode(',',$request->input('prefered_work_location',null));
            $list = $list->where(function($query) use ($locations){
                foreach ($locations as $l){
                    $query = $query->orWhere('prefered_work_location',$l);
                }
                return $query;
            });
        }

        if($request->input('work_categories',null)){
            $categories = explode(',',$request->input('work_categories',null));
            $list = $list->where(function($query) use ($categories){
                foreach ($categories as $l){
                    $query = $query->orWhere('work_categories','LIKE','%"'.$l.'"%');
                }
                return $query;
            });
        }
        if($request->input('education_area',null)){
            $type = explode(',',$request->input('education_area',null));
            $orWhere = implode(' or education_area_id =',$type);
            $list =  $list->leftJoin(DB::raw("(select student_id from student_schools where ( education_area_id=$orWhere) group by student_id) as estarea"),'estarea.student_id','=','students.id')->whereNotNull('estarea.student_id');
        }
        if($request->input('education_level',null)){
            $type = explode(',',$request->input('education_level',null));
            $orWhere = implode(' or education_level_id =',$type);
            $list =  $list->leftJoin(DB::raw("(select student_id from student_schools where ( education_level_id=$orWhere) group by student_id) as estlevel"),'estlevel.student_id','=','students.id')->whereNotNull('estlevel.student_id');
        }
        if($request->input('languages',null)){
            $type = explode(',',$request->input('languages',null));
            $orWhere = implode(' or language_id =',$type);
            $list =  $list->leftJoin(DB::raw("(select student_id,count(*) as cnt from student_languages where ( language_id=$orWhere) group by student_id) as lang"),'lang.student_id','=','students.id')->where('lang.cnt',sizeof($type));
        }
        if($request->input('fixings',null)){
            $type = explode(',',$request->input('fixings',null));
            $orWhere = implode(' or work_category_id =',$type);
            $list =  $list->leftJoin(DB::raw("(select student_id,industry_id from fixings left join company_industries on company_industries.id = fixings.industry_id where ( work_category_id=$orWhere) group by student_id) as wcategory"),'wcategory.student_id','=','students.id')->whereNotNull('wcategory.student_id');
        }
        $list = $list->orderBy("students.name","asc");
        if($request->has('query')) {
            return $list;

        }
        $list =$list->whereNull('students.deleted_at');
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return $this->listResource::collection($list)->additional(['filters' => $this->search]);
    }
    public function studentRegister(Request $request){
        $list = new Student();
        $list = $list->whereNotNull('registration_link_sended')->orderBy("registration_link_sended","desc");
        if($request->has('register_user_id')){
            $list= $list->where('register_user_id',$request->input('register_user_id'));
        }else{
            $list= $list->where('register_user_id',Auth::user()->id);
        }
        if($request->has("search")) {
            $search =  $request->input('search', null);
            $list = $list->where(function ($query) use ($search) {

                    $query = $query->orWhere('name','LIKE','%'.$search.'%')->orWhere('student_number','LIKE','%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%')->orWhere('phone','LIKE','%'.$search.'%');

                return $query;
            });
        }
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return $this->listResource::collection($list)->additional(['filters' => $this->search2]);
    }
    public function searchExport(Request $request){

        $request->merge(['query'=>'true']);
        $list = $this->studentSearch($request);
        $excel = [];
        foreach ($list->orderBy('students.name')->get() as $row) {
            $title = $row->company_name;
            $excel[] = [
                $row->name, $row->email
            ];
        }
        $export = new BaseArrayExport(['Diák','Email'], "Diák keresés", $excel);

        return $export->download(Str::slug('Diak-export') . '.xlsx');
    }
    public function getProfile(Request $request)
    {
        return parent::show($request, Auth::id());
    }
    public function sendRegistrationLink(Request $request,$student_id){
        try {
            $student = Student::where('id', $student_id)->firstOrFail();
            $student->registration_token = md5($student->email.date('YmdHis').$student_id);
            $student->registration_token_expire = date('Y-m-d H:i:s',strtotime('+1 day'));
            $student->register_user_id = Auth::user()->id;
            $student->registration_link_sended =date('Y-m-d H:i:s');
            $student->save();
            SaveStatistic::dispatch(Auth::user()->id,'registration_send');
            Mail::to($student->email)->send(new RegistrationEmail($student));
           // Mail::to('bodak.szabolcs@gmail.com')->send(new RegistrationEmail($student));
            return new StudentViewResource($student);
        }catch (ModelNotFoundException $exception){
            abort(404);
        }
    }
    public function studentDept(Request $request){

        $list =StudentWorkSafety::leftJoin("students","students.id",'student_work_safety.student_id')->disableCache()
            ->select(DB::raw('students.*,student_work_safety.type as type,student_work_safety.expire as expire_safety,student_work_safety.price as value,(select if(sum(fixings.company_price) <  5*student_work_safety.price,true,false) from fixings  where fixings.student_id = student_work_safety.student_id and student_work_safety.price >0 and fixings.date >= DATE(student_work_safety.created_at)) as dept'));
        $search = $request->input('search');
        if(!empty($search)){
            $list = $list->where(function($query) use($search){
                $query->where('name','LIKE','%'.$search.'%')
                    ->orWhere('tax_number','LIKE','%'.$search.'%')
                    ->orWhere('phone','LIKE','%'.$search.'%')
                    ->orWhere('email','LIKE','%'.$search.'%')
                    ->orWhere('student_number','LIKE','%'.$search.'%');
            });
        }
        $list =$list->where('price','>',0);
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());
        return $this->listResource::collection($list)->additional(['filters' => []]);
    }
    public function studentList(Request $request,$type='all'){
        $dateFrom = date('Y-m-d',strtotime('-1 month'));
        $dateTo = date('Y-m-d');
        $list =Student::leftJoin(DB::raw(Student::$studentCertificateQuery),'sCard.student_id','=','students.id')
            ->leftJoin(DB::raw("(select student_id,count(*) as schedule from project_schedules  where schedule_date >= $dateFrom and schedule_date <= $dateTo group by student_id) as sch"),'sch.student_id','=','students.id')->disableCache()
            ->select(DB::raw('students.*,expire, schedule'));



        $search = $request->input('search');
        if(!empty($search)){
            $list = $list->where(function($query) use($search){
                $query->where('name','LIKE','%'.$search.'%')
                    ->orWhere('tax_number','LIKE','%'.$search.'%')
                    ->orWhere('phone','LIKE','%'.$search.'%')
                    ->orWhere('email','LIKE','%'.$search.'%')
                    ->orWhere('student_number','LIKE','%'.$search.'%');
            });
        }
        if($type != 'new' && $type!= 'all'){
            $list = $list->where('registration_backend','=',0);
        }
        if($type !='exited'){
            $list= $list->whereNull('exit_date');
        }else{
            $list= $list->whereNotNull('exit_date');
        }
        switch($type){
            case 'active': {
                $list = $list->where('expire','>=',date('Y-m-d'));
                break;
            }
            case 'inactive': {
                $list = $list->where(function($query){
                    $query->where('expire','<=',date('Y-m-d'))->orWhereNull('expire');
                });
                break;
            }
            case 'webreg': {
                $list = $list->whereNotNull('registration_web');
                break;
            }
            case 'new': {
                $list = $list->where('registration_backend','=',1)->orderBy('registration_approved','desc');
                break;
            }
            case 'members': {
                $list = $list->whereNull('registration_approved')->whereNotNull('registration_created');
                break;
            }
            case 'has-work': {
                $list = $list->leftJoin('project_students','project_students.student_id','=','students.id')->where('status_id',2);
                break;
            }
            case 'dept': {
                $list = $list->leftJoin(DB::raw('(select student_id, sum(student_price) as ballance from fixings group by student_id) as ball'),'ball.student_id','=','students.id')
                    ->leftJoin('(select student_id, 10*sum(price) as debit  from student_work_safety where price is not null group by student_id) as dept','dept.student_id','=','students.id')
                    ->where('ball.ballance','<',DB::raw('dept.debit'))->where('dept.debit','>',0)->whereNotNull('dept.student_id');
                break;
            }
            case 'underage': {
                $list = $list->where('birth_date','>=',date('Y-m-d',strtotime('-18 year')));
                break;
            }
        }
        $list = $list->orderBy("id","desc");

        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());

        return $this->listResource::collection($list)->additional(['filters' => []]);

    }
    public function approveRegistration(Request $request,$id){
        try{
            $student = Student::where('id','=',$id)->firstOrFail();
            $student->fill($request->all());
            $student->registration_approved= Auth::id();
            $student->registration_date= date('Y-m-d H:i:s');
            $student->registration_backend= 1;
            $student->updateSendingBlue();
            $student->save();
            return new  StudentViewResource($student);
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getSchools(Request $request,$id){
		try{
			$student = Student::where('id','=',$id)->firstOrFail();

			return StudentSchoolViewResource::collection($student->schools()->orderBy('id','desc')->get())->additional([
				'education_types'=>Student::$EDUCATIONTYPES,
				'education_levels'=>EducationLevel::all()->pluck('name','id'),
				'education_areas'=>EducationArea::all()->pluck('name','id'),
				'school_list'=>School::all()->pluck('name','id'),
			]);
		}
		catch (ModelNotFoundException $e){
			abort(404);
		}

    }

    public function getRealization(Request $request,$id){
            $realization = Realization::where('student_id','=',$id)->where('end','>=',date('Y-m-d'))->orderBy('end','desc')->get();
            return RealizationListResource::collection($realization);
    }
    public function addRealization(RealizationCreateRequest $request){

        $school =new Realization();
        $school->fill($request->all());
        $school->save();
        return response()->json($this->successMessage,$this->successStatus);
    }
    public function deleteRealization(Request $request,$id){
        try {
            $school = Realization::where('id','=',$id)->firstOrFail();
            $school->delete();
            return response()->json($this->successMessage ,$this->successStatus);

        }	catch (ModelNotFoundException $e){
            abort(404);
        }
    }
    public function getEmails(Request $request,$id){

            $student = Student::where('id','=',$id)->firstOrFail();
            $emails =Email::where('recipient_email','=',$student->email)->leftJoin("users",'users.id','=','user_id')->select(DB::raw("concat(users.lastname,' ',users.firstname) as user_name, emails.*"))->orderBy('sent_date','desc')->limit(25)->get();

            return response()->json(['data'=>$emails]);



    }
    public function getEuORMedicalCertificate(Request $request,$id){
        $projects = ProjectStudent::where('student_id',$id)->select(DB::raw('*, IF
	(( `company_industries`.`medical_certificate` > 0 ), `company_industries`.`medical_certificate`, `companies`.`medical_certificate` ) AS `medical_certificate_type` '))
            ->leftJoin('projects','projects.id','=','project_id')
            ->leftJoin('companies','companies.id','=','projects.company_id')
            ->leftJoin('company_industries','company_industries.id','=','projects.industry_id')
            ->where(function($query){
                $query->where('companies.medical_certificate','>',0)->orWhere('company_industries.medical_certificate','>',0);
            })
            ->where('project_students.status_id','2')->get();
        $respnse=[
            'eu'=>[
                'required'=>false,
                'expire'=>false,
            ],
            'medical'=>[
                'required'=>false,
                'expire'=>false,
            ]
        ];
        foreach ($projects as $p){
            if($p->medical_certificate_type ==2){
                $respnse['eu']['required']=true;
            }
            if($p->medical_certificate_type ==1){
                $respnse['medical']['required']=true;
            }
        }
        if( $respnse['eu']['required']){
            $studentWorkSafety= StudentWorkSafety::where('expire','>=',date('Y-m-d'))->where('student_id',$id)->where('type','=',1)->first();
                if($studentWorkSafety){
                    $respnse['eu']['expire'] = $studentWorkSafety->expire;
                }
        }
        if( $respnse['medical']['required']){
            $studentWorkSafety= StudentWorkSafety::where('expire','>=',date('Y-m-d'))->where('student_id',$id)->where('type','=',3)->first();
                if($studentWorkSafety){
                    $respnse['medical']['expire'] = $studentWorkSafety->expire;
                }
        }
        return $respnse;

    }
    public function documentTemplate(Request $request,$type="entry",$student){

        try {
            $student = Student::where('id','=',$student)->firstOrFail();
            switch ($type) {
                case 'entry':
                    return PdfHelper::createPdf(view('student.belepesi',['student'=>$student,'params'=>$request->all()]),
                        view('student.templateHeader',['title'=>'BELÉPÉSI NYILATKOZAT - TAGSÁGI MEGÁLLAPODÁS']),
                        view('student.belepesiFooter',['student'=>$student,'params'=>$request->all()])
                    );

                case 'membership':
                    return PdfHelper::createPdf(view('student.tagsagi',['student'=>$student,'params'=>$request->all()]),
                        view('student.templateHeader',['title'=>'TAGSÁGI MEGÁLLAPODÁS'])
                    );
                case 'membership2017':
                    return PdfHelper::createPdf(view('student.tagsagi_2017',['student'=>$student,'params'=>$request->all()]),
                        view('student.templateHeader',['title'=>'TAGSÁGI MEGÁLLAPODÁS'])
                    );
                case 'membershipGDPR':
                    $pdf =PdfHelper::createPdf(view('student.tagsagi_gdpr',['student'=>$student,'params'=>$request->all()]),
                        view('student.templateHeader',['title'=>'TAGSÁGI MEGÁLLAPODÁS']),null,'S'
                    );
                    $doc = new StudentDocuments();
                    $doc->student_id = $student->id;
                    $doc->name = Str::slug($student->student_number.'_'.$student->name.'_tagsagi_','_');
                    $path = md5($doc->name . microtime()).'.pdf';
                    $doc->project_id = $request->input('project');
                    $doc->type = 'membership';
                    Storage::disk('s3')->put('private/student_documents/'.$student->id.'/'.$path,$pdf);
                    $doc->file = 'private/student_documents/'.$student->id.'/'.$path;
                    $doc->updated = Auth::user()->id;
                    $exist = StudentDocuments::where('name',$doc->name )->where('student_id',$student->id)->where('type','membership')->first();
                    if(!$exist) {
                        if($request->input('sign') && $request->input('sign') == 'true'){
                            $doc->document_type_id =1;
                            $doc->save();
                            $doc->sendEmail();
                        }
                        $doc->save();
                    }
                    return Response::make($pdf, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.$doc->name.'"'
                    ]);
                case 'membershipGDPREn':
                    $pdf = PdfHelper::createPdf(view('student.tagsagi_gdpr_en',['student'=>$student,'params'=>$request->all()]),
                        view('student.templateHeader',['title'=>'MEMBERSHIP AGREEMENT'],),null,'S'

                    );
                    $doc = new StudentDocuments();
                    $doc->student_id = $student->id;
                    $doc->name = Str::slug($student->student_number.'_'.$student->name.'_membership_','_');
                    $path = md5($doc->name . microtime()).'.pdf';
                    $doc->project_id = $request->input('project');
                    $doc->type = 'membership_en';
                    Storage::disk('s3')->put('private/student_documents/'.$student->id.'/'.$path,$pdf);
                    $doc->file = 'private/student_documents/'.$student->id.'/'.$path;
                    $doc->updated = Auth::user()->id;
                    $exist = StudentDocuments::where('name',$doc->name )->where('student_id',$student->id)->where('type','membership_en')->first();
                    if(!$exist) {
                        if($request->input('sign') && $request->input('sign') == 'true'){
                            $doc->document_type_id =1;
                            $doc->save();
                            $doc->sendEmail();
                        }
                        $doc->save();
                    }
                    return Response::make($pdf, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.$doc->name.'"'
                    ]);
                case 'job':
                    $pdf = PdfHelper::createPdf(view('student.munkakori',['student'=>$student,'params'=>$request->all()]), view('student.templateHeader',['title'=>'MUNKAKÖRI LEÍRÁS']),null,'S');
                    $doc = new StudentDocuments();
                    $doc->student_id = $request->input('student_id');
                    $doc->project_id = $request->input('project');
                    $doc->type = 'munkakori';
                    $doc->name = Str::slug($student->number.'_'.$student->name.'_Munkakori_'.$request->input('company').'_'.$request->input('task'),'_');
                    $path= md5($doc->ame . microtime()).'.pdf';
                    Storage::disk('s3')->put('private/student_documents/'.$student->id.'/'.$path,$pdf);
                    $doc->file = 'private/student_documents/'.$student->id.'/'.$path;
                    $doc->name = $request->input('company').' '.$request->input('task').' munkaköri';
                    $doc->updated = Auth::user()->id;
                    $exist = StudentDocuments::where('name',$doc->name)->where('student_id',$student->id)->where('type','munkakori')->first();
                    if(!$exist) {
                        $doc->save();
                    }
                    return Response::make($pdf, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.$doc->name.'"'
                    ]);

                case 'adhoc':


                    $pdf = $student->generateEseti('hu',$student,$request);
                    $doc = new StudentDocuments();
                    $doc->student_id = $student->id;
                    $doc->name = Str::slug($student->student_number.'_'.$student->name.'_Eseti_'.$request->input('company').'_'.$request->input('task'),'_');

                    $path = md5($doc->name . microtime()).'.pdf';
                    $doc->project_id = $request->input('project');
                    $doc->type = 'adhock';
                    Storage::disk('s3')->put('private/student_documents/'.$student->id.'/'.$path,$pdf);
                    $doc->student_id = $request->input('student_id');
                    $doc->file = 'private/student_documents/'.$student->id.'/'.$path;
                    $doc->updated = Auth::user()->id;
                    $exist = StudentDocuments::where('name',$doc->name )->where('student_id',$student->id)->where('type','adhock')->first();

                        SaveStatistic::dispatch(\Auth::user()->id,'contract');
                        if($request->input('sign') && $request->input('sign') == 'true'){
                            $doc->document_type_id =1;
                            $doc->sendEmail();
                        }


                    $doc->save();
                    return Response::make($pdf, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.$doc->name.'"'
                    ]);
                case 'adhoc-en':

                    $pdf = $student->generateEseti('en',$student,$request);

                    $doc = new StudentDocuments();
                    $doc->student_id = $student->id;
                    $doc->name = Str::slug($student->student_number.'_'.$student->name.'_INDIVIDUAL AGREEMENT_'.$request->input('company').'_'.$request->input('task'),'_');
                    $path = md5($doc->name . microtime()).'.pdf';
                    $doc->project_id = $request->input('project');
                    $doc->type = 'adhock_en';
                    Storage::disk('s3')->put('private/student_documents/'.$student->id.'/'.$path,$pdf);
                    $doc->student_id = $request->input('student_id');
                    $doc->file = 'private/student_documents/'.$student->id.'/'.$path;
                    $doc->updated = Auth::user()->id;
                    $exist = StudentDocuments::where('name',$doc->name )->where('student_id',$student->id)->where('type','adhock_en')->first();

                        SaveStatistic::dispatch(\Auth::user()->id,'contract');
                        if($request->input('sign') && $request->input('sign') == 'true'){
                            $doc->document_type_id =1;
                            $doc->sendEmail();
                        }


                    $doc->save();
                    return Response::make($pdf, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="'.$doc->name.'"'
                    ]);
                case 'adhoc-and-member':


                    $student->generateEseti('hu',$student,$request,'/tmp/' . $student->id . 'eseti.pdf');
                    $pdfReg = $student->generateTagsagiEseti('/tmp/' . $student->id . 'eseti.pdf',$request->input('sign','false'),$request->all());
                    SaveStatistic::dispatch(\Auth::user()->id,'contract');

                    return Response::make($pdfReg, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="Regisztráció.pdf"'
                    ]);

                case 'adhoc-and-member-en':

                    $student->generateEseti('en',$student,$request,'/tmp/' . $student->id . 'eseti_en.pdf');
                    $pdfReg = $student->generateTagsagiEsetiEn('/tmp/' . $student->id . 'eseti_en.pdf',$request->input('sign','false'),$request->all());
                    SaveStatistic::dispatch(\Auth::user()->id,'contract');

                    return Response::make($pdfReg, 200, [
                        'Content-Type' => 'application/pdf',
                        'Content-Disposition' => 'inline; filename="Registration.pdf"'
                    ]);

            }

            return response()->json($this->successMessage ,$this->successStatus);

        }	catch (ModelNotFoundException $e){
            abort(404);
        }


    }
	public function addSchools(SchoolCreateRequest $request,$id){

    	$school =new StudentSchool();
    	$school->fill($request->all());
    	$school->student_id = $id;
    	$school->save();
        $student= Student::where('id',$id)->first();
        $student->updateSendingBlue();
    	return response()->json($this->successMessage,$this->successStatus);
	}
	public function deleteSchools(Request $request,$id){
		try {
			$school = StudentSchool::where('id','=',$id)->firstOrFail();
			$studentId = $school->student_id;
			$school->delete();
            $student= Student::where('id',$id)->first();
            $student->updateSendingBlue();
			return response()->json($this->successMessage ,$this->successStatus);

		}	catch (ModelNotFoundException $e){
			abort(404);
		}
	}
	public function addCertificate(Request $request,$id){
		$school =new StudentCertificate();
		$school->fill($request->all());
		$school->student_id = $id;
		$school->save();
		return response()->json($this->successMessage,$this->successStatus);
	}
	public function deleteCertificate(Request $request,$id){
		try {
			$school = StudentCertificate::where('id','=',$id)->firstOrFail();
			$school->delete();
			return response()->json($this->successMessage ,$this->successStatus);

		}	catch (ModelNotFoundException $e){
			abort(404);
		}
	}
	public function addCard(CardCreateRequest $request,$id){
		$school =new StudentCard();
		$school->fill($request->all());
		$school->student_id = $id;
		$school->save();
		return response()->json($this->successMessage,$this->successStatus);
	}
	public function deleteCard(Request $request,$id){
		try {
			$school = StudentCard::where('id','=',$id)->firstOrFail();
			$school->delete();
			return response()->json($this->successMessage ,$this->successStatus);

		}	catch (ModelNotFoundException $e){
			abort(404);
		}
	}
	public function addComment(Request $request,$id){
        try {
            $student = Student::where('id', $id)->firstOrFail();
            if ($request->input('newComment', 'false')) {
                $student->comment = date('Y.m.d').' '.$request->input('new_comment')."\n".$student->comment;
            }
            else {
                $student->comment = $request->input('comment');
            }
            $student->save();

            SaveStatistic::dispatch(Auth::user()->id,'visited_students');
        }
        catch (ModelNotFoundException $e){
            abort(404);
        }
    }

	public function getStudentOther(Request $request,$id){

	}
	public function getStudentDocuments(Request $request,$id){
        return StudentDocumentListResource::collection(StudentDocuments::where('student_id',$id)->orderBy('id','desc')->get())->additional(['company'=>Company::all()->pluck('name','id')]);
	}
	public function deleteStudentDocuments(Request $request,$id){
        try {
            $school = StudentDocuments::where('id','=',$id)->firstOrFail();
            $school->delete();
            return response()->json($this->successMessage ,$this->successStatus);

        }	catch (ModelNotFoundException $e){
            abort(404);
        }
	}
	public function addStudentDocuments(DocumentCreateRequest $request,$id){

        $doc = new StudentDocuments();
        $doc->student_id = $id;
        $doc->fill($request->all());
        $doc->save();
        if($request->input('send_email','0') ==1){
            $doc->sendEmail();
        }

        return response()->json([$this->successMessage,$this->successStatus]);
	}



}
