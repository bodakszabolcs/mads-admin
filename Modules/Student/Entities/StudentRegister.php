<?php

namespace Modules\Student\Entities;

use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentRegister extends Student
{
    use SoftDeletes, Cachable;
	protected $table='student_register';
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        return parent::getFilters();
    }








}
