<?php

namespace Modules\Student\Entities\Base;

use App\BaseModel;
use App\Contracts\BaseModelInterface;
use App\Helpers\PdfHelper;
use App\Helpers\SearchHelper;
use App\Helpers\SendingBlue;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Laravel\Sanctum\HasApiTokens;
use Modules\Company\Entities\Company;
use Modules\Company\Entities\Forms;
use Modules\Company\Entities\Industry;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentDocuments;
use Modules\User\Notifications\MailResetPasswordNotification;
use Mpdf\Mpdf;
use Spatie\Translatable\HasTranslations;
use Modules\User\Entities\User;


abstract class BaseStudent extends User
{
    use Notifiable, HasApiTokens;

    protected $guard = 'student';
    protected $table = 'students';
    protected $dates = ['created_at', 'updated_at'];
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotification($token, $this->email));
    }
    public function getFilters()
    {
        $this->searchColumns = [[
            'name' => 'name',
            'title' => 'Név',
            'type' => 'text',
        ],
            [
                'name' => 'tax_number',
                'title' => 'Adószám',
                'type' => 'text',
            ],
            [
                'name' => 'student_number',
                'title' => 'Azonosító',
                'type' => 'text',
            ],
            [
                'name' => 'phone',
                'title' => 'Mobil',
                'type' => 'text',
            ], [
                'name' => 'email',
                'title' => 'E-mail',
                'type' => 'text',
            ]];

        return parent::getFilters();
    }


    protected $fillable = ['name', 'identity_card', 'birth_location', 'birth_date', 'mothers_name', 'nationality', 'passport', 'eu_expire_date', 'exit_date', 'gender', 'phone', 'email', 'country_id', 'zip',
        'city_id', 'street', 'area_type_id', 'house_number', 'building', 'stairs', 'level', 'door', 'notification_address', 'bank_account_number', 'tax_number', 'taj', 'date_of_entry', 'nav_declaration', 'membership_agreement',
        'employment_contract', 'part_ticket', 'prefered_work_location', 'newsletter', 'work_categories', 'comment', 'avatar', 'ability_to_work','driver_licence','om_code'];

    protected $casts = [
        'work_categories' => 'array'
    ];

    public function address()
    {
        return $this->zip . ' ' . optional($this->city)->name . ', ' . $this->street . ' ' . optional($this->areaType)->name . ' ' . $this->house_number . ' ' . $this->building . ' ' . $this->stairs . ' ' . $this->level . ' ' . $this->door;
    }

    public function user()
    {
        return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id');
    }
    public function sendingBlue()
    {
        return $this->hasOne('Modules\Student\Entities\StudentNewsletter', 'id', 'id');
    }
    public function city()
    {
        return $this->hasOne('Modules\City\Entities\City', 'id', 'city_id');
    }

    public function country()
    {
        return $this->hasOne('Modules\Country\Entities\Country', 'id', 'country_id');
    }

    public function nationalities()
    {
        return $this->hasOne('Modules\Nationality\Entities\Nationality', 'id', 'nationality');
    }

    public function areaType()
    {
        return $this->hasOne('Modules\AreaType\Entities\AreaType', 'id', 'area_type_id');
    }

    public function schools()
    {
        return $this->hasMany('Modules\Student\Entities\StudentSchool', 'student_id', 'id');
    }

    public function school()
    {
        return $this->belongsToMany('Modules\School\Entities\School', 'student_schools', 'student_id', 'school_id')->withPivot(['start', 'end']);
    }

    public function card()
    {
        return $this->hasMany('Modules\Student\Entities\StudentCard', 'student_id', 'id');
    }

    public function certificate()
    {
        return $this->hasMany('Modules\Student\Entities\StudentCertificate', 'student_id', 'id');
    }

    public function searchInModel(array $filter)
    {
        return SearchHelper::searchInModel($this, $filter);
    }

    public function fillAndSave(array $request)
    {
        if (!isset($request['registration_date'])) {
            $request['registration_date'] = date('Y-m-d H:i:s');
            $request['registration_backend'] = 1;
            $request['email_verified_at'] = date('Y-m-d H:i:s');
        }

        $this->fill($request);
        $this->updateSendingBlue();
        $this->save();
        if (!$this->student_number) {

                $this->student_number = 'MADS'. str_pad($this->id.'',6,'0',STR_PAD_LEFT);

        }
        if($this->exit_date){
            DB::statement("delete from project_students where student_id =".$this->id);
        }
        $this->save();

    }
    public function updateSendingBlue(){
        try {
            $sendingBlue = new SendingBlue();

            $sendingBlue->createOrUpdateStudent($this);
        }catch (\Exception $e){

        }
    }
    public function generateEseti($lang, $student,$request,$path=null){
        $forms= null;
        if($request->has('industry_id') && $request->input('industry_id')>0){
            $industry=Industry::where('id',$request->input('industry_id'))->first();
            $company= Company::where('id',$industry->company_id)->first();
            $forms = Forms::where('company_id',$company->id)->where('membership',1)->get();

        }
        if(!$forms) {
            if($lang=='hu') {
                return PdfHelper::createPdf(view('student.eseti', ['student' => $student, 'params' => $request->all()]),
                    view('student.templateHeader', ['title' => 'ESETI MEGÁLLAPODÁS'])
                    , null, 'S');
            }else{
                return PdfHelper::createPdf(view('student.eseti_en',['student'=>$student,'params'=>$request->all()]),
                    view('student.templateHeader',['title'=>'INDIVIDUAL AGREEMENT'])
                    ,null,'S');
            }
        }else{

            if($lang=='hu') {
                PdfHelper::createPdf(view('student.eseti', ['student' => $student, 'params' => $request->all()]),
                    view('student.templateHeader', ['title' => 'ESETI MEGÁLLAPODÁS'])
                    , null, '/tmp/' . $student->id . 'eseti.pdf');
            }else{
                 PdfHelper::createPdf(view('student.eseti_en',['student'=>$student,'params'=>$request->all()]),
                    view('student.templateHeader',['title'=>'INDIVIDUAL AGREEMENT'])
                    ,null,'/tmp/' . $student->id . 'eseti.pdf');
            }
            $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
            $pdf->curlAllowUnsafeSslRequests = true;
            $pdf->debug = true;
            $pdf->AddPage();
            $pageCount = $pdf->setSourceFile('/tmp/' . $student->id . 'eseti.pdf');
            for ($i = 1; $i <= $pageCount; $i++) {

                $tplIdx = $pdf->importPage($i);
                //$wh = $pdf->getTemplateSize($tplIdx);
                if (($i == 1)) {
                    //$pdf->state = 0;
                    $pdf->UseTemplate($tplIdx);
                } else {
                    //$pdf->state = 1;
                    $pdf->AddPage();
                    $pdf->UseTemplate($tplIdx);
                }

            }
            unlink('/tmp/' . $student->id . 'eseti.pdf');
            foreach ($forms as $form) {

                $student = Student::where('id',$student->id)->first();
                $industry = Industry::where('id',$request->input('industry_id'))->first();

                $content = $form->replaceData($industry->id,$student->id);
                $date = date('Y-m-d');
                $params = $request->all();
                if(isset($params['document_date']) && !empty($params['document_date'])  && $params['document_date'] != 'undefined'){
                    $date = date('Y-m-d',strtotime($params['document_date']));
                }
                $content = str_replace('[date]',$date,$content);
                $config= PdfHelper::$config;
                if($form->title){
                    $config['margin_top'] = 55;


                }else {
                    $config['margin_top'] = 15;
                }

                $pdf2 = new Mpdf($config);
                if($form->title) {
                    $header = view('student.templateHeader', ['title' => $form->title]);
                    $pdf2->SetHTMLHeader($header->render());
                    $pdf2->showWatermarkImage=true;

                    $pdf2->SetDefaultBodyCSS('background', 'url('.base_path('storage/doc_bg.png').')');
                    $pdf2->SetDefaultBodyCSS('background-image-resize', 6);
                    $pdf2->SetDefaultBodyCSS('background-repeat', 'no-repeat');
                }
                $pdf2->curlAllowUnsafeSslRequests = true;
                $pdf2->debug=true;

                $pdf2->WriteHTML($content);

                $pdf2->Output('/tmp/' . $student->id . '-'.$form->id.'form.pdf',\Mpdf\Output\Destination::FILE);

                $pdf->AddPage();
                $pageCount = $pdf->setSourceFile('/tmp/' . $student->id . '-'.$form->id.'form.pdf');
                for ($i = 1; $i <= $pageCount; $i++) {

                    $tplIdx = $pdf->importPage($i);
                    if (($i == 1)) {
                        $pdf->UseTemplate($tplIdx);
                    } else {
                        $pdf->AddPage();
                        $pdf->UseTemplate($tplIdx);
                    }

                }
                unlink('/tmp/' . $student->id . '-'.$form->id.'form.pdf');
            }
            if($path){
                return $pdf->Output($path,\Mpdf\Output\Destination::FILE);
            }
            return $pdf->Output('', 'S');
        }
    }
    public function generateTagsagiEseti(string $esetiPath, string $sign,$params)
    {
        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        PdfHelper::createPdf(view('student.tagsagi_gdpr', ['student' => $this,'params'=>$params]), view('student.templateHeader', ['title' => 'TAGSÁGI MEGÁLLAPODÁS']), null, '/tmp/' . $this->id . 'tagsagi.pdf');
        $pdf->AddPage();
        $this->membership_agreement = date('Y-m-d');
        $this->save();
        $pageCount = $pdf->setSourceFile('/tmp/' . $this->id . 'tagsagi.pdf');

        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink('/tmp/' . $this->id . 'tagsagi.pdf');
        $pdf->AddPage();
        $pageCount = $pdf->setSourceFile($esetiPath);
        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink($esetiPath);


        $file = $pdf->Output('', 'S');

        $path = md5('Regisztráció' . microtime()) . '.pdf';
        Storage::disk('s3')->put('private/student_documents/' . $this->id . '/' . $path, $file);
        $doc = new StudentDocuments();
        $doc->student_id = $this->id;
        $doc->created = Auth::user()->id;
        $doc->name = \Str::slug($this->student_number.'_'.$this->name.'_Regisztráció_','_');
        $doc->student_id = $this->id;
        $doc->type='membership';
        $doc->file = 'private/student_documents/' . $this->id . '/' . $path;

        $doc->updated = Auth::user()->id;
        if($sign == 'true'){
            $doc->document_type_id = 1;
            $doc->sendEmail();
        }

        $doc->save();
        return $file;
    }
    public function generateTagsagiEsetiEn(string $esetiPath,$sign,$params)
    {
        $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
        $pdf->curlAllowUnsafeSslRequests = true;
        $pdf->debug = true;
        PdfHelper::createPdf(view('student.tagsagi_gdpr_en', ['student' => $this,'params'=>$params]), view('student.templateHeader', ['title' => 'MEMBERSHIP AGREEMENT']), null, '/tmp/' . $this->id . 'tagsagi_en.pdf');
        $pdf->AddPage();
        $this->membership_agreement = date('Y-m-d');
        $this->save();
        $pageCount = $pdf->setSourceFile('/tmp/' . $this->id . 'tagsagi_en.pdf');

        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink('/tmp/' . $this->id . 'tagsagi_en.pdf');
        $pdf->AddPage();
        $pageCount = $pdf->setSourceFile($esetiPath);
        for ($i = 1; $i <= $pageCount; $i++) {

            $tplIdx = $pdf->importPage($i);
            //$wh = $pdf->getTemplateSize($tplIdx);
            if (($i == 1)) {
                //$pdf->state = 0;
                $pdf->UseTemplate($tplIdx);
            } else {
                //$pdf->state = 1;
                $pdf->AddPage();
                $pdf->UseTemplate($tplIdx);
            }

        }
        unlink($esetiPath);

        $file = $pdf->Output('', 'S');

        $path = md5('REGISTRAtion' . microtime()) . '.pdf';
        Storage::disk('s3')->put('private/student_documents/' . $this->id . '/' . $path, $file);
        $doc = new StudentDocuments();
        $doc->student_id = $this->id;
        $doc->name = \Str::slug($this->student_number.'_'.$this->name.'_Registration_','_');
        $doc->student_id = $this->id;
        $doc->type='membership_en';
        $doc->file = 'private/student_documents/' . $this->id . '/' . $path;
        if($sign == 'true'){
            $doc->document_type_id = 1;
            $doc->created = Auth::user()->id;
            $doc->sendEmail();
        }
        $doc->updated = Auth::user()->id;
        $doc->save();
        return $file;
    }
}
