<?php

namespace Modules\Student\Entities;

use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Student extends BaseStudent
{
    use SoftDeletes, Cachable;
	public static $studentCardQuery= "( select max(temporary_student_card_expire) as expire,student_id from student_cards where deleted_at is null group by student_id) as sCard";
	public static $studentCertificateQuery= "( select max(end) as expire,student_id from student_certificates where deleted_at is null group by student_id ) as sCard";
    public static $gender = [
    	1=>'Nő',
	    2=>'Férfi'
    ];

	public static $TRUEFALSE = [
		1=>'Igen',
		2=>'Nem'
	];
	public static $EDUCATIONTYPES = [
		1=>'Nappali',
		2=>'25 év alatti passzív'
	];




    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
