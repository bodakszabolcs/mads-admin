<?php

namespace Modules\Student\Entities;

use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentDocuments extends BaseModel
{
    use SoftDeletes, Cachable;
    protected $fillable = ['student_id','document_type_id','file','name','type'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }

    public function sendEmail(){

            $student = Student::where('id', $this->student_id)->first();

            $email = new \Modules\Email\Entities\Email();
            $email->user_id = Auth::user()->id;
            $email->subject = 'MADS - új aláírásra váró dokumentumod érkezett';
            $email->recipient_name = $student->name;
           // $email->recipient_email ='bodak.szabolcs@gmail.com';
            $email->recipient_email = $student->email;
            $email->link_button_enable = true;
            $email->content = 'Egy új aláírandó dokumentumod érkezett a MADS-tól, ' . $this->name . ' néven. ' . "<br>" . ' A weboldalunkon belépve, akár eleketronikusan is aláírhatod ügyfélkapu segítségével!';

            $send = Mail::to($email->recipient_email)->send(new \App\Mail\SendEmail($email));


    }
    public function sendSignEmail(){
        try {
        $student =Student::where('id',$this->student_id)->first();

        $email = new \Modules\Email\Entities\Email();
        $email->user_id = Auth::user()->id;
        $email->subject = 'MADS - dokumentum aláírva';
        $email->recipient_name = $student->name;
        $email->recipient_email = $student->email;
        $email->link_button_enable = true;
        $email->content = 'Az általad feltöltötött('.$this->name.') dokumentum aláírásra került a mads részéről! '."<br>".' A dokumentumot letöltheted a mads profilodból!';
        $send =Mail::to($email->recipient_email)->send(new \App\Mail\SendEmail($email));
            }catch (\Exception $e){

        }
    }






}
