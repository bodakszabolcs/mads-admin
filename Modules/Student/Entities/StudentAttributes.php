<?php

namespace Modules\Student\Entities;

use App\BaseModel;
use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentAttributes extends BaseModel
{
    use SoftDeletes, Cachable;
    protected $table = 'student_attributes';
    protected $fillable = ['student_id','attribute_id','value'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
