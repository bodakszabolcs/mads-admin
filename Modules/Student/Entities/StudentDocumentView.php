<?php

namespace Modules\Student\Entities;

use App\BaseModel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentDocumentView extends BaseModel
{
    use SoftDeletes, Cachable;
    protected $fillable = [];
    protected $table='student_document_view';

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }

}
