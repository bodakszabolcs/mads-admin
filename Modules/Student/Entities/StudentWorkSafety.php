<?php

namespace Modules\Student\Entities;

use App\BaseModel;
use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentWorkSafety extends BaseModel
{
    use SoftDeletes, Cachable;
    protected $table = 'student_work_safety';
    protected $fillable = ['student_id','type','value','expire','price','file','size','date','approve_is_required'];
    public static $types = [
        1=>'Egészségügyi kiskönyv',
        2=>'Munkavédelmi bakancs',
        3=>'Orvosi igazolás',
        4=>'Munkaruha'
    ];
    public function __construct(array $attributes = [])
    {
        $this->searchColumns = [[
            'name' => 'students.name',
            'title' => 'Diák',
            'type' => 'text',
        ],[
            'name' => 'students.student_number',
            'title' => 'MADS azonosító',
            'type' => 'text',
        ]
            ];
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return $this->searchColumns;
    }








}
