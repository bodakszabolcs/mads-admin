<?php

namespace Modules\Student\Entities;

use App\BaseModel;
use Modules\Student\Entities\Base\BaseStudent;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class StudentSchool extends BaseModel
{
    use SoftDeletes, Cachable;

	protected $table ="student_schools";

    protected $fillable = ['school_id','student_id','education_type','start','end','education_area_id','education_level_id','updated'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        return parent::getFilters();
    }
	public function school()
	{
		return $this->hasOne('Modules\School\Entities\School','id','school_id');
	}
	public function educationArea()
	{
		return $this->hasOne('Modules\EducationArea\Entities\EducationArea','id','education_area_id');
	}
	public function educationLevel()
	{
		return $this->hasOne('Modules\EducationLevel\Entities\EducationLevel','id','education_level_id');
	}








}
