import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Student from '../components/Student'
import StudentList from '../components/StudentList'
import StudentCreate from '../components/StudentCreate'
import StudentEdit from '../components/StudentEdit'
import StudentSearch from '../components/search/StudentSearch'
import MyDocumentList from '../components/MyDocumentList'
import StudentRegister from '../components/register/StudentRegister'
import StudentListRealization from '../components/StudentListRealization.vue'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'student',
                component: Student,
                meta: {
                    title: 'Students'
                },
                children: [
                    {
                        path: 'index',
                        name: 'StudentList',
                        component: StudentList,
                        meta: {
                            title: 'Students',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/student/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/student/index'
                        }
                    },
                    {
                        path: 'register',
                        name: 'StudentRegister',
                        component: StudentRegister,
                        meta: {
                            title: 'Elküldött regisztrációk',
                            subheader: false
                        }
                    },
                    {
                        path: 'realization',
                        name: 'StudentRealization',
                        component: StudentListRealization,
                        meta: {
                            title: 'Ráérések',
                            subheader: false
                        }
                    },
                    {
                        path: 'my-documents',
                        component: MyDocumentList,
                        meta: {
                            title: 'Alíárásra váró dokumentumok'
                        }
                    },
                    {
                        path: 'search',
                        name: 'StudentSearch',
                        component: StudentSearch,
                        meta: {
                            title: 'Student search',
                            subheader: false
                        }
                    },
                    {
                        path: 'create',
                        name: 'StudentCreate',
                        component: StudentCreate,
                        meta: {
                            title: 'Create Students',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/student/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'StudentEdit',
                        component: StudentEdit,
                        meta: {
                            title: 'Edit Students',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/student/index'
                        }
                    }
                ]
            }

        ]
    }
]
