<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Student\Entities\Student;

$factory->define(Student::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"identity_card" => $faker->realText(),
"birth_location" => $faker->realText(),
"birth_date" => $faker->date(),
"mothers_name" => $faker->realText(),
"nationality" => rand(1000,5000),
"passport" => $faker->realText(),
"eu_expire_date" => $faker->date(),
"exit_date" => $faker->date(),
"gender" => rand(1,10),
"phone" => $faker->realText(),
"email" => $faker->realText(),
"country_id" => rand(1000,5000),
"zip" => $faker->realText(),
"city_id" => rand(1000,5000),
"street" => $faker->realText(),
"area_type_id" => rand(1000,5000),
"house_number" => $faker->realText(),
"building" => $faker->realText(),
"stairs" => $faker->realText(),
"level" => $faker->realText(),
"door" => $faker->realText(),
"notification_address" => $faker->realText(),
"bank_account_number" => $faker->realText(),
"tax_number" => $faker->realText(),
"taj" => $faker->realText(),
"date_of_entry" => $faker->date(),
"nav_declaration" => $faker->date(),
"membership_agreement" => $faker->date(),
"employment_contract" => $faker->date(),
"part_ticket" => rand(1000,5000),
"prefered_work_location" => $faker->realText(),
"newsletter" => rand(1,10),
"work_categories" => $faker->realText(),
"comment" => $faker->realText(),
"avatar" => $faker->realText(),

    ];
});
