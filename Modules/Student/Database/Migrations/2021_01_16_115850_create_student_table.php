<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('students');
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();
$table->text("identity_card")->nullable();
$table->text("birth_location")->nullable();
$table->date("birth_date")->nullable();
$table->text("mothers_name")->nullable();
$table->integer("nationality")->unsigned()->nullable();
$table->text("passport")->nullable();
$table->date("eu_expire_date")->nullable();
$table->date("exit_date")->nullable();
$table->tinyInteger("gender")->unsigned()->nullable();
$table->text("phone")->nullable();
$table->text("email")->nullable();
$table->integer("country_id")->unsigned()->nullable();
$table->text("zip")->nullable();
$table->integer("city_id")->unsigned()->nullable();
$table->text("street")->nullable();
$table->integer("area_type_id")->unsigned()->nullable();
$table->text("house_number")->nullable();
$table->text("building")->nullable();
$table->text("stairs")->nullable();
$table->text("level")->nullable();
$table->text("door")->nullable();
$table->text("notification_address")->nullable();
$table->text("bank_account_number")->nullable();
$table->text("tax_number")->nullable();
$table->text("taj")->nullable();
$table->date("date_of_entry")->nullable();
$table->date("nav_declaration")->nullable();
$table->date("membership_agreement")->nullable();
$table->date("employment_contract")->nullable();
$table->integer("part_ticket")->unsigned()->nullable();
$table->json("prefered_work_location")->nullable();
$table->tinyInteger("newsletter")->unsigned()->nullable();
$table->json("work_categories")->nullable();
$table->text("comment")->nullable();
$table->text("avatar")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
