<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
	Route::group(['namespace' => 'Modules\Student\Http\Controllers',
		'prefix' => '/student',
		'middleware' => ['auth:sanctum','role']
	], function () {
        Route::match(['get'], '/student-list/{type}', 'StudentController@studentList')->name('Student list by type');
        Route::match(['get'], '/get-certificate-by-company/{id}', 'StudentController@getEuORMedicalCertificate')->name('Diák Eü- orvosi ellenőrzés az adatlapon');
        Route::match(['get'], '/student-list-realization', 'StudentController@studentListRealization')->name('Diák ráérés lista');
        Route::match(['get'], '/get-my-documents-pm', 'StudentOtherController@listMyDocuments')->name('list my documents');
        Route::match(['get'], '/student/dept', 'StudentController@studentDept')->name('Student list has dept');
        Route::match(['get'], '/send-student-registration-link/{student_id}', 'StudentController@sendRegistrationLink')->name('Student send registration link');
        Route::match(['get'], '/student-search', 'StudentController@studentSearch')->name('Student search');
        Route::match(['get'], '/student-register', 'StudentController@studentRegister')->name('Registration links sent');
        Route::match(['get'], '/search/export', 'StudentController@searchExport')->name('Student search export');
        Route::match(['post'], '/add-comment/{id}', 'StudentController@addComment')->name('Student comment');
		Route::match(['get'], '/get-schools/{id}', 'StudentController@getSchools')->name('Student get school list');

		Route::match(['get'], '/get-other/{id}', 'StudentController@getStudentOther')->name('Student get other data');
		Route::match(['get'], '/get-documents/{id}', 'StudentController@getStudentDocuments')->name('Student get documents');

        Route::match(['post'], '/add-realization-backend', 'StudentController@addRealization')->name('Diák ráérés rögzítése');
        Route::match(['delete'], '/delete-realization-backend', 'StudentController@deleteRealization')->name('Diák ráérés törlése');
        Route::match(['get'], '/get-realization/{id}', 'StudentController@getRealization')->name('Diák ráérések lekérdezése');

		Route::match(['post'], '/add-school/{id}', 'StudentController@addSchools')->name('Student add school');
		Route::match(['post'], '/add-certificate/{id}', 'StudentController@addCertificate')->name('Student add school certificate');
		Route::match(['post'], '/add-card/{id}', 'StudentController@addCard')->name('Student add card');
		Route::match(['post'], '/add-documents/{id}', 'StudentController@addStudentDocuments')->name('Student add document');
		Route::match(['post'], '/approve-registration/{id}', 'StudentController@approveRegistration')->name('Student approve registration');

		Route::match(['delete'], '/delete-schools/{id}', 'StudentController@deleteSchools')->name('Student delete school ');
		Route::match(['delete'], '/delete-certificate/{id}', 'StudentController@deleteCertificate')->name('Student delete school certificate');
		Route::match(['delete'], '/delete-card/{id}', 'StudentController@deleteCard')->name('Student delete card');

        Route::match(['post'], '/add-documents/{id}', 'StudentController@addStudentDocuments')->name('Student new document');
        Route::match(['get'], '/get-documents/{id}', 'StudentController@getStudentDocuments')->name('Student document list');
        Route::match(['delete'], '/delete-document/{id}', 'StudentController@deleteStudentDocuments')->name('Student delete document');

        Route::match(['get'], '/get-emails/{id}', 'StudentController@getEmails')->name('Student get emails');

        Route::match(['post'], '/add-language/{id}', 'StudentOtherController@addLanguage')->name('Student new language');
        Route::match(['get'], '/get-language/{id}', 'StudentOtherController@listLanguage')->name('Student language list');
        Route::match(['delete'], '/delete-language/{id}', 'StudentOtherController@deleteLanguage')->name('Student delete language');

        Route::match(['post'], '/add-attribute/{id}', 'StudentOtherController@addAttribute')->name('Student new attribute');
        Route::match(['get'], '/get-attribute/{id}', 'StudentOtherController@listAttribute')->name('Student attribute list');
        Route::match(['delete'], '/delete-attribute/{id}', 'StudentOtherController@deleteAttribute')->name('Student delete attribute');

        Route::match(['post'], '/add-work-safety/{id}', 'StudentOtherController@addWorkSafety')->name('Student new work safety');
        Route::match(['get'], '/get-work-safety/{id}', 'StudentOtherController@listWorkSafety')->name('Student work safety list');
        Route::match(['delete'], '/delete-work-safety-admin/{id}', 'StudentOtherController@deleteWorkSafety')->name('Student work safety language');

        Route::match(['get'], '/get-finance/{id}/{month}', 'StudentOtherController@getStudentFinance')->name('Student work finance by month');
        Route::match(['get'], '/get-works/{id}', 'StudentOtherController@studentWorks')->name('Student work  list');
        Route::match(['get'], '/get-projects/{id}', 'StudentOtherController@studentProjects')->name('Student projects list');
	});
Route::group(['namespace' => 'Modules\Student\Http\Controllers',
    'prefix' => '/student',
    'middleware' => ['auth:sanctum','role']
], function () {
Route::match(['get'], '/get-document-template/{type}/{student}', 'StudentController@documentTemplate')->name('Student document templates');
});
