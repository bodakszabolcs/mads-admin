<?php

namespace Modules\Student\Observers;

use Modules\Student\Entities\Student;

class StudentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function saved(Student $model)
    {
        Student::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function created(Student $model)
    {
        //

    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function updated(Student $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function deleted(Student $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function restored(Student $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Student\Entities\Student  $model
     * @return void
     */
    public function forceDeleted(Student $model)
    {
        //
    }
}
