<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\OnlineStudentCard\Entities\OnlineStudentCard;

$factory->define(OnlineStudentCard::class, function (Faker $faker) {
    return [
        "student_id" => rand(1000,5000),
"document_type" => rand(1,10),
"school_type" => rand(1,10),
"school_year" => rand(1000,5000),
"semester" => rand(1000,5000),
"status" => rand(1,10),
"selected_school" => rand(1000,5000),
"new_school" => rand(1,10),
"om_code" => $faker->realText(),
"new_file" => $faker->realText(),
"user_id" => rand(1000,5000),
"card_status" => rand(1,10),

    ];
});
