<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineStudentCardTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('online_student_cards');
        Schema::create('online_student_cards', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("student_id")->unsigned()->nullable();
$table->tinyInteger("document_type")->unsigned()->nullable();
$table->tinyInteger("school_type")->unsigned()->nullable();
$table->integer("school_year")->unsigned()->nullable();
$table->integer("semester")->unsigned()->nullable();
$table->tinyInteger("status")->unsigned()->nullable();
$table->bigInteger("selected_school")->unsigned()->nullable();
$table->tinyInteger("new_school")->unsigned()->nullable();
$table->text("om_code")->nullable();
$table->text("new_file")->nullable();
$table->bigInteger("user_id")->unsigned()->nullable();
$table->tinyInteger("card_status")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_student_cards');
    }
}
