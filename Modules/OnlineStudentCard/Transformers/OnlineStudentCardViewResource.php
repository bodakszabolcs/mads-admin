<?php

namespace Modules\OnlineStudentCard\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Student\Entities\Student;
		use Modules\User\Entities\User;
		

class OnlineStudentCardViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "document_type" => $this->document_type,
		    "school_type" => $this->school_type,
		    "school_year" => $this->school_year,
		    "semester" => $this->semester,
		    "status" => $this->status,
		    "selected_school" => $this->selected_school,
		    "new_school" => $this->new_school,
		    "om_code" => $this->om_code,
		    "new_file" => $this->new_file,
		    "user_id" => $this->user_id,
		    "card_status" => $this->card_status,
		"selectables" => [
		"student" => Student::pluck("name","id"),
		"user" => User::pluck("name","id"),
		]
		     ];
    }
}
