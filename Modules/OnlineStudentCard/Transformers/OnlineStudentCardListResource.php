<?php

namespace Modules\OnlineStudentCard\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class OnlineStudentCardListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "student_number" => $this->student_number,
		    "student_name" => $this->student_name,
           // "student_25_year"=>date('Y.m.d',strtotime($this->birth_date.' +25 year')),
            "student_25_year"=>date('Y.m.d',strtotime($this->birth_date.' +24 year')),
		    "document_type" => ($this->document_type==1)?'Diák':'Jogviszony',
		    "school_type" => ($this->school_type==1)?'Középiskola / szakközépiskola':'Egyetem / főiskola',
		    "school_year" =>$this->replace(($this->school_type==1)?$this->school_year:$this->semester),
		    "selected_school" => $this->selected_school,
		    "semester" => $this->semester,
		    "status" => ($this->status==1)?'Aktív':'Passzív',
		    "new_school" => ConstansHelper::bool($this->new_school),
		    "om_code" => $this->om_code,
		    "file" => $this->new_file,
		    "user_id" => $this->user_id,
		    "start" => date('Y.m.d',strtotime($this->start)),
		    "end" => date('Y.m.d',strtotime($this->end)),
            "start_date" => $this->start,
            "end_date" => $this->end,
		    "card_status" => $this->getCardStatus($this->card_status),
		     ];
    }
    public function getCardStatus($status){
        $statusArray=[
            '0'=>'Beküldve',
            '1'=>'Elfogadva',
            '2'=>'Elutasítva',
        ];
        return $statusArray[$status];
    }
    public function replace($val){
        $val =str_replace('osz',' Őszi félév',$val);
        $val =str_replace('tavasz',' Tavaszi félév',$val);
        $val =str_replace('tanev',' Tanév',$val);
        return $val;
}
}
