<?php

namespace Modules\OnlineStudentCard\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OnlineStudentCardCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

			'document_type' => 'required',
			'school_type' => 'required',
			//'school_year' => 'required',
			//'semester' => 'required',
			'status' => 'required',
			'selected_school' => 'required',
			//'new_school' => 'required',
			'om_code' => 'required',
			'new_file' => 'required',
			//'user_id' => 'required',
			//'card_status' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'student_id' => __('Diák'),
'document_type' => __('Dokumentum'),
'school_type' => __('Képzés típusa'),
'school_year' => __('Tanév'),
'semester' => __('Félév'),
'status' => __('Státusz'),
'selected_school' => __('Iskola'),
'new_school' => __('Változás'),
'om_code' => __('Diák/Om azonosító'),
'new_file' => __('File'),
'user_id' => __('felelős'),
'card_status' => __('mads állapot'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
