<?php

namespace Modules\OnlineStudentCard\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\SendOnlineCardEmail;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\OnlineStudentCard\Entities\OnlineStudentCard;
use Illuminate\Http\Request;
use Modules\OnlineStudentCard\Http\Requests\OnlineStudentCardCreateRequest;
use Modules\OnlineStudentCard\Http\Requests\OnlineStudentCardUpdateRequest;
use Modules\OnlineStudentCard\Transformers\OnlineStudentCardViewResource;
use Modules\OnlineStudentCard\Transformers\OnlineStudentCardListResource;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentCard;
use Modules\Student\Entities\StudentCertificate;
use Modules\Student\Entities\StudentDocuments;

class OnlineStudentCardController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new OnlineStudentCard();
        $this->viewResource = OnlineStudentCardViewResource::class;
        $this->listResource = OnlineStudentCardListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = OnlineStudentCardCreateRequest::class;
        $this->updateRequest = OnlineStudentCardUpdateRequest::class;
    }
    public function index(Request $request, $auth = null)
    {

        $list = new OnlineStudentCard();
        if($request->has('document_type')){
            $list= $list->where("document_type",$request->input("document_type"));
        }
        if($request->has('card_status')){
            $list= $list->whereIn("card_status",explode(",",$request->input("card_status")));
        }
        $list= $list->leftJoin('students','students.id','=','online_student_cards.student_id')->select(DB::raw('online_student_cards.*,students.name as student_name, students.student_number as student_number,birth_date'));
        if (!\Auth::user()->isSuperAdmin() && !\Auth::user()->isUgyfelszolgalat() && \Auth::user()->position!=2 && \Auth::user()->position !=3) {
            $list= $list->leftJoin(DB::raw("(select student_id, group_concat(concat('|',user_id,'|',',|',replacement_user_id,'|')) as users from project_students left join projects on projects.id = project_id group by student_id) as ps"),'ps.student_id','=','online_student_cards.student_id');
            $list = $list->where(function ($query) {
                $query->where('ps.users', 'LIKE','%|'.\Auth::id()."|%");
            });
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }



        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function handle(Request $request){
        $osc = OnlineStudentCard::where('id',$request->input('id'))->first();
        $student = Student::where('id',$request->input('student_id'))->first();
        $osc->start = $request->input('start_date');
        $osc->end = $request->input('end_date');
        $osc->comment=$request->input("comment");
        $osc->card_status = $request->input('card_status');
        $osc->user_id = \Auth::user()->id;
        $osc->save();

        if( $request->input('card_status') == 1) {
            if($osc->document_type ==1){
                $sd = new StudentCard();
                $sd->student_id = $osc->student_id;
                $sd->school_id= $osc->selected_school;
                $sd->student_card = $osc->om_code;
                $sd->temporary_student_card_expire=  $request->input('end_date');
                $sd->save();
            }
            $sc = new StudentCertificate();
            $sc->student_id = $osc->student_id;
            $sc->school_id = $osc->selected_school;
            $sc->start = $osc->start;
            $sc->end = $osc->end;
            $sc->document = $osc->new_file;
            $sc->save();
        }else{
             StudentDocuments::where('file',$osc->new_file)->forceDelete();
        }


        SendOnlineCardEmail::dispatch($osc,$student->name,$student->email);
        return response()->json(['data'=>'ok']);

    }

}
