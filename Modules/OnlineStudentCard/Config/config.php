<?php

return [
    'name' => 'OnlineStudentCard',

                 'menu_order' => 5,

                 'menu' => [
                     [
                      'icon' =>'flaticon-open-box',
                      'title' =>'Online diák/jogviszony',
                      'route' =>'/'.env('ADMIN_URL').'/online-student-card/index',

                     ],
                     [
                         'icon' =>'fa fa-ambulance',
                         'title' =>'Online orvosi igazolás',
                         'route' =>'/'.env('ADMIN_URL').'/medical-certificate',

                     ],
                     [
                         'icon' =>'flaticon-open-box',
                         'title' =>'Lejáró igazolások',
                         'route' =>'/'.env('ADMIN_URL').'/certificates/index',

                     ],
                     [
                         'icon' =>'fa fa-file-pdf',
                         'title' =>'Online eseti, tagsági',
                         'route' =>'/'.env('ADMIN_URL').'/adhock-membership/index',

                     ],
                     [
                         'icon' =>'fa fa-clock',
                         'title' =>'Ráérés',
                         'route' =>'/'.env('ADMIN_URL').'/student/realization',

                     ]

                 ]
];
