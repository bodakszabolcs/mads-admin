<?php

namespace Modules\OnlineStudentCard\Observers;

use Modules\OnlineStudentCard\Entities\OnlineStudentCard;

class OnlineStudentCardObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function saved(OnlineStudentCard $model)
    {
        OnlineStudentCard::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function created(OnlineStudentCard $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function updated(OnlineStudentCard $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function deleted(OnlineStudentCard $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function restored(OnlineStudentCard $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\OnlineStudentCard\Entities\OnlineStudentCard  $model
     * @return void
     */
    public function forceDeleted(OnlineStudentCard $model)
    {
        //
    }
}
