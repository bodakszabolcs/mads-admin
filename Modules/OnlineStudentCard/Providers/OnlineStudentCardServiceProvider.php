<?php

namespace Modules\OnlineStudentCard\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\OnlineStudentCard\Entities\OnlineStudentCard;
use Modules\OnlineStudentCard\Observers\OnlineStudentCardObserver;

class OnlineStudentCardServiceProvider extends ModuleServiceProvider
{
    protected $module = 'onlinestudentcard';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        OnlineStudentCard::observe(OnlineStudentCardObserver::class);
    }
}
