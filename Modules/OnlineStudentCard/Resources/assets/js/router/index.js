import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import OnlineStudentCard from '../components/OnlineStudentCard'
import OnlineStudentCardList from '../components/OnlineStudentCardList'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'online-student-card',
                component: OnlineStudentCard,
                meta: {
                    title: 'OnlineStudentCards'
                },
                children: [
                    {
                        path: 'index',
                        name: 'OnlineStudentCardList',
                        component: OnlineStudentCardList,
                        meta: {
                            title: 'OnlineStudentCards',
                            subheader: false

                        }
                    }

                ]
            }
        ]
    }
]
