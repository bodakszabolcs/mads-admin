<?php

namespace Modules\OnlineStudentCard\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\Student\Entities\Student;
		use Modules\User\Entities\User;


abstract class BaseOnlineStudentCard extends BaseModel
{


    protected $table = 'online_student_cards';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [
                    [
                    'name' => 'search',
                    'title' => 'Diák, azonosító ...',
                    'type' => 'text',
                    ],
                     ];

        return parent::getFilters();
    }

    protected $with = ["student","user"];

    protected $fillable = ['student_id','document_type','school_type','school_year','semester','status','selected_school','new_school','om_code','new_file','user_id','card_status'];

    protected $casts = [];

     public function student()
    {
        return $this->hasOne('Modules\Student\Entities\Student','id','student_id');
    }


 public function user()
    {
        return $this->hasOne('Modules\User\Entities\User','id','user_id');
    }
}
