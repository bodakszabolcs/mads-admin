<?php

namespace Modules\Content\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Content\Entities\Content;
use Illuminate\Http\Request;
use Modules\Content\Http\Requests\ContentCreateRequest;
use Modules\Content\Http\Requests\ContentUpdateRequest;
use Modules\Content\Transformers\ContentViewResource;
use Modules\Content\Transformers\ContentListResource;

class ContentController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Content();
        $this->viewResource = ContentViewResource::class;
        $this->listResource = ContentListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ContentCreateRequest::class;
        $this->updateRequest = ContentUpdateRequest::class;
    }

}
