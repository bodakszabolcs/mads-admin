<?php

namespace Modules\Content\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Modules\Content\Entities\Content;
use Spatie\Translatable\HasTranslations;


abstract class BaseContent extends BaseModel
{


    protected $table = 'contents';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'title',
                    'title' => 'Megnevezés',
                    'type' => 'text',
                    ]];

        return parent::getFilters();
    }
    public function fillAndSave(array $request)
    {
        $this->fill($request);
        if(empty($this->slug)) {
            $this->slug = Str::slug($this->title);
        }
        $this->save();
        $file=[];
        $data = Content::all();
        foreach ($data as $d){
            $file[$d->slug] = [
                'title' => $d->title,
                'content' => $d->content,
            ];
        }
        Storage::disk(config('filesystems.default_upload_filesystem'))->delete('content.json');
        Storage::disk(config('filesystems.default_upload_filesystem'))->put('content.json',json_encode($file));
        return $this;
    }
    public function generateJson(){

    }
    protected $fillable = ['title','content'];

    protected $casts = [];


}
