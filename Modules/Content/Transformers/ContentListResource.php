<?php

namespace Modules\Content\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ContentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "title" => $this->title,
		    "content" => $this->content,
		    "slug" => $this->slug,
		     ];
    }
}
