<?php

namespace Modules\Content\Observers;

use Modules\Content\Entities\Content;

class ContentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function saved(Content $model)
    {
        Content::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function created(Content $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function updated(Content $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function deleted(Content $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function restored(Content $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Content\Entities\Content  $model
     * @return void
     */
    public function forceDeleted(Content $model)
    {
        //
    }
}
