<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Content\Entities\Content;

$factory->define(Content::class, function (Faker $faker) {
    return [
        "title" => $faker->realText(),
"content" => $faker->realText(),
"slug" => $faker->realText(),

    ];
});
