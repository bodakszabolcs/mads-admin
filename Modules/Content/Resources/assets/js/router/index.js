import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Content from '../components/Content'
import ContentList from '../components/ContentList'
import ContentCreate from '../components/ContentCreate'
import ContentEdit from '../components/ContentEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'content',
                component: Content,
                meta: {
                    title: 'Contents'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ContentList',
                        component: ContentList,
                        meta: {
                            title: 'Contents',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/content/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/content/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ContentCreate',
                        component: ContentCreate,
                        meta: {
                            title: 'Create Contents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/content/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ContentEdit',
                        component: ContentEdit,
                        meta: {
                            title: 'Edit Contents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/content/index'
                        }
                    }
                ]
            }
        ]
    }
]
