<?php

namespace Modules\Content\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Content\Entities\Content;
use Modules\Content\Observers\ContentObserver;

class ContentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'content';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Content::observe(ContentObserver::class);
    }
}
