<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Country\Entities\Country;

$factory->define(Country::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
