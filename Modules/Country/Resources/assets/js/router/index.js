import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Country from '../components/Country'
import CountryList from '../components/CountryList'
import CountryCreate from '../components/CountryCreate'
import CountryEdit from '../components/CountryEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'country',
                component: Country,
                meta: {
                    title: 'Countries'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CountryList',
                        component: CountryList,
                        meta: {
                            title: 'Countries',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/country/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/country/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CountryCreate',
                        component: CountryCreate,
                        meta: {
                            title: 'Create Countries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/country/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CountryEdit',
                        component: CountryEdit,
                        meta: {
                            title: 'Edit Countries',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/country/index'
                        }
                    }
                ]
            }
        ]
    }
]
