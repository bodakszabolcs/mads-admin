<?php

namespace Modules\Country\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Country\Entities\Country;
use Illuminate\Http\Request;
use Modules\Country\Http\Requests\CountryCreateRequest;
use Modules\Country\Http\Requests\CountryUpdateRequest;
use Modules\Country\Transformers\CountryViewResource;
use Modules\Country\Transformers\CountryListResource;

class CountryController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Country();
        $this->viewResource = CountryViewResource::class;
        $this->listResource = CountryListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CountryCreateRequest::class;
        $this->updateRequest = CountryUpdateRequest::class;
    }

}
