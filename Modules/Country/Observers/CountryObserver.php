<?php

namespace Modules\Country\Observers;

use Modules\Country\Entities\Country;

class CountryObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function saved(Country $model)
    {
        Country::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function created(Country $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function updated(Country $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function deleted(Country $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function restored(Country $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Country\Entities\Country  $model
     * @return void
     */
    public function forceDeleted(Country $model)
    {
        //
    }
}
