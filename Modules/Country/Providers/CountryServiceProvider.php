<?php

namespace Modules\Country\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Country\Entities\Country;
use Modules\Country\Observers\CountryObserver;

class CountryServiceProvider extends ModuleServiceProvider
{
    protected $module = 'country';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Country::observe(CountryObserver::class);
    }
}
