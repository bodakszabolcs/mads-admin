<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
	Route::group([
		'namespace' => 'Modules\ProjectStatus\Http\Controllers',
		'prefix' => '/projectstatus',
		'middleware' => ['auth:sanctum', 'role']
	], function () {

		Route::post( '/save-order', 'ProjectStatusController@saveOrder')->name('Save project order status');
	});
