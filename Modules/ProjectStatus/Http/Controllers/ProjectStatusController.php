<?php

namespace Modules\ProjectStatus\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Illuminate\Http\Request;
use Modules\ProjectStatus\Http\Requests\ProjectStatusCreateRequest;
use Modules\ProjectStatus\Http\Requests\ProjectStatusUpdateRequest;
use Modules\ProjectStatus\Transformers\ProjectStatusViewResource;
use Modules\ProjectStatus\Transformers\ProjectStatusListResource;

class ProjectStatusController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProjectStatus();
        $this->viewResource = ProjectStatusViewResource::class;
        $this->listResource = ProjectStatusListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProjectStatusCreateRequest::class;
        $this->updateRequest = ProjectStatusUpdateRequest::class;
    }
    public function saveOrder(Request $request){
    	$index = 0;
    	foreach ($request->input('list',[]) as $ps){
    		ProjectStatus::where('id',$ps['id'])->update(['o'=>$index]);
    		$index++;
	    }
    	return response()->json($this->successMessage,$this->successStatus);
    }

}
