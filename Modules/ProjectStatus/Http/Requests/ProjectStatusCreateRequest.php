<?php

namespace Modules\ProjectStatus\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectStatusCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'o'=>'required'


        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Állapot megnevezése'),
'o' => __('Sorrend'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
