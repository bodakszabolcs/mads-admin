import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProjectStatus from '../components/ProjectStatus'
import ProjectStatusList from '../components/ProjectStatusList'
import ProjectStatusCreate from '../components/ProjectStatusCreate'
import ProjectStatusEdit from '../components/ProjectStatusEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'project-status',
                component: ProjectStatus,
                meta: {
                    title: 'ProjectStatuses'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProjectStatusList',
                        component: ProjectStatusList,
                        meta: {
                            title: 'ProjectStatuses',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/project-status/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-status/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProjectStatusCreate',
                        component: ProjectStatusCreate,
                        meta: {
                            title: 'Create ProjectStatuses',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-status/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProjectStatusEdit',
                        component: ProjectStatusEdit,
                        meta: {
                            title: 'Edit ProjectStatuses',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-status/index'
                        }
                    }
                ]
            }
        ]
    }
]
