<?php

namespace Modules\ProjectStatus\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProjectStatus\Entities\ProjectStatus;
use Modules\ProjectStatus\Observers\ProjectStatusObserver;

class ProjectStatusServiceProvider extends ModuleServiceProvider
{
    protected $module = 'projectstatus';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProjectStatus::observe(ProjectStatusObserver::class);
    }
}
