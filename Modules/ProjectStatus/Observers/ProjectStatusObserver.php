<?php

namespace Modules\ProjectStatus\Observers;

use Modules\ProjectStatus\Entities\ProjectStatus;

class ProjectStatusObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function saved(ProjectStatus $model)
    {
        ProjectStatus::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function created(ProjectStatus $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function updated(ProjectStatus $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function deleted(ProjectStatus $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function restored(ProjectStatus $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProjectStatus\Entities\ProjectStatus  $model
     * @return void
     */
    public function forceDeleted(ProjectStatus $model)
    {
        //
    }
}
