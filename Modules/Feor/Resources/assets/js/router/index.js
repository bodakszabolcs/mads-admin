import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Feor from '../components/Feor'
import FeorList from '../components/FeorList'
import FeorCreate from '../components/FeorCreate'
import FeorEdit from '../components/FeorEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'feor',
                component: Feor,
                meta: {
                    title: 'Feors'
                },
                children: [
                    {
                        path: 'index',
                        name: 'FeorList',
                        component: FeorList,
                        meta: {
                            title: 'Feors',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/feor/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/feor/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'FeorCreate',
                        component: FeorCreate,
                        meta: {
                            title: 'Create Feors',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/feor/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'FeorEdit',
                        component: FeorEdit,
                        meta: {
                            title: 'Edit Feors',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/feor/index'
                        }
                    }
                ]
            }
        ]
    }
]
