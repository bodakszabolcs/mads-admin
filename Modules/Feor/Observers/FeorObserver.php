<?php

namespace Modules\Feor\Observers;

use Modules\Feor\Entities\Feor;

class FeorObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function saved(Feor $model)
    {
        Feor::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function created(Feor $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function updated(Feor $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function deleted(Feor $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function restored(Feor $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Feor\Entities\Feor  $model
     * @return void
     */
    public function forceDeleted(Feor $model)
    {
        //
    }
}
