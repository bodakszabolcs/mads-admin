<?php

namespace Modules\Feor\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Feor\Entities\Feor;
use Modules\Feor\Observers\FeorObserver;

class FeorServiceProvider extends ModuleServiceProvider
{
    protected $module = 'feor';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Feor::observe(FeorObserver::class);
    }
}
