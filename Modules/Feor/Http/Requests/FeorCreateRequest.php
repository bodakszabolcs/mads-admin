<?php

namespace Modules\Feor\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FeorCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code' => 'required',
			'name' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'code' => __('Kód'),
'name' => __('Feor'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
