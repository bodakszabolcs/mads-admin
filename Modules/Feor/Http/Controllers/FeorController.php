<?php

namespace Modules\Feor\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Feor\Entities\Feor;
use Illuminate\Http\Request;
use Modules\Feor\Http\Requests\FeorCreateRequest;
use Modules\Feor\Http\Requests\FeorUpdateRequest;
use Modules\Feor\Transformers\FeorViewResource;
use Modules\Feor\Transformers\FeorListResource;

class FeorController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Feor();
        $this->viewResource = FeorViewResource::class;
        $this->listResource = FeorListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = FeorCreateRequest::class;
        $this->updateRequest = FeorUpdateRequest::class;
    }

}
