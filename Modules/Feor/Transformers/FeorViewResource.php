<?php

namespace Modules\Feor\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class FeorViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "code" => $this->code,
		    "name" => $this->name,
		"selectables" => [
		]
		     ];
    }
}
