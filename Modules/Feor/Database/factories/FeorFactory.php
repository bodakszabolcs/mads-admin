<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Feor\Entities\Feor;

$factory->define(Feor::class, function (Faker $faker) {
    return [
        "code" => rand(1000,5000),
"name" => $faker->realText(),

    ];
});
