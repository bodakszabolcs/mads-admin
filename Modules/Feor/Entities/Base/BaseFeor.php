<?php

namespace Modules\Feor\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseFeor extends BaseModel
{
    

    protected $table = 'feors';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'code',
                    'title' => 'Kód',
                    'type' => 'text',
                    ],[
                    'name' => 'name',
                    'title' => 'Feor',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['code','name'];

    protected $casts = [];

    
}
