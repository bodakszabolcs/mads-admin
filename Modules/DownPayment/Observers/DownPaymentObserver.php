<?php

namespace Modules\DownPayment\Observers;

use Modules\DownPayment\Entities\DownPayment;

class DownPaymentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function saved(DownPayment $model)
    {
        DownPayment::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function created(DownPayment $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function updated(DownPayment $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function deleted(DownPayment $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function restored(DownPayment $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\DownPayment\Entities\DownPayment  $model
     * @return void
     */
    public function forceDeleted(DownPayment $model)
    {
        //
    }
}
