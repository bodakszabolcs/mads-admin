<?php

namespace Modules\DownPayment\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseDownPayment extends BaseModel
{


    protected $table = 'down_payments';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'month',
                    'title' => 'Hónap',
                    'type' => 'month_picker',
                    ],[
                    'name' => 'approved',
                    'title' => 'Jóváhagyva',
                    'type' => 'select',
                    'data'=>[0=>'Nem',1=>'Igen']
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['student_id','month','percent','approved','price'];

    protected $casts = [];
}
