import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import DownPayment from '../components/DownPayment'
import DownPaymentList from '../components/DownPaymentList'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'down-payment',
                component: DownPayment,
                meta: {
                    title: 'DownPayments'
                },
                children: [
                    {
                        path: 'index',
                        name: 'DownPaymentList',
                        component: DownPaymentList,
                        meta: {
                            title: 'DownPayments',
                            subheader: false
                        }
                    }
                ]
            }
        ]
    }
]
