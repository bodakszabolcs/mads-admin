<?php

namespace Modules\DownPayment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownPaymentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_id' => 'required',
			'month' => 'required',
			'price' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'student_id' => __('Diák'),
'month' => __('Hónap'),
'prive' => __('Összeg'),
'approved' => __('Jóváhagyva'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
