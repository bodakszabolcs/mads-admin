<?php

namespace Modules\DownPayment\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Modules\DownPayment\Entities\DownPayment;
use Illuminate\Http\Request;
use Modules\DownPayment\Http\Requests\DownPaymentCreateRequest;
use Modules\DownPayment\Http\Requests\DownPaymentUpdateRequest;
use Modules\DownPayment\Transformers\DownPaymentViewResource;
use Modules\DownPayment\Transformers\DownPaymentListResource;
use Modules\MonthlyPayment\Entities\MonthlyPayment;
use Modules\Student\Entities\Student;

class DownPaymentController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new DownPayment();
        $this->viewResource = DownPaymentViewResource::class;
        $this->listResource = DownPaymentListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = DownPaymentCreateRequest::class;
        $this->updateRequest = DownPaymentUpdateRequest::class;
    }
    public function index(Request $request, $auth = null)
    {
        $list = $this->model->searchInModel($request->input());
        $list = $list->leftJoin("students","students.id",'student_id')->select(DB::raw("down_payments.*,students.name as name, students.student_number as mads_number"));
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }

        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function approve(DownPaymentUpdateRequest $request){
        $downPayment = DownPayment::where('id',$request->input('id'))->first();
        $downPayment->fill($request->all());
        $downPayment->approved=1;
        $downPayment->save();
        $monthlyPayment = new MonthlyPayment();
        $monthlyPayment->student_id = $downPayment->student_id;
        $monthlyPayment->month = str_replace('-','',$downPayment->month);
        $monthlyPayment->price = $downPayment->price;
        $monthlyPayment->comment = $request->input("comment");
        $monthlyPayment->save();
        $student = Student::where('id',$downPayment->student_id)->first();
        try {
            Mail::to($student->email)->send(new \App\Mail\SendDownPaymentResponseEmail($student->name));
        }catch (\Exception $e){

        }
        return response()->json("ok");
    }

}
