<?php

namespace Modules\DownPayment\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class DownPaymentViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "month" => $this->month,
		    "percent" => $this->percent,
		    "approved" => $this->approved,
		"selectables" => [
		]
		     ];
    }
}
