<?php

namespace Modules\DownPayment\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class DownPaymentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "student_id" => $this->student_id,
		    "student_name" => $this->name,
		    "mads_number" => $this->mads_number,
		    "month" => $this->month,
		    "percent" => $this->percent,
            "price"=> $this->price,
            "created_at"=>$this->created_at,
		    "approved_val" => ConstansHelper::bool($this->approved),
		    "approved" => $this->approved,
		     ];
    }
}
