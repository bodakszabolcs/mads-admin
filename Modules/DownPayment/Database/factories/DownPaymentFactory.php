<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\DownPayment\Entities\DownPayment;

$factory->define(DownPayment::class, function (Faker $faker) {
    return [
        "student_id" => rand(1000,5000),
"month" => rand(1000,5000),
"percent" => rand(1000,5000),
"approved" => rand(1,10),

    ];
});
