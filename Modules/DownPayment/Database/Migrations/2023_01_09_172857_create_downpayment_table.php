<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownPaymentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('down_payments');
        Schema::create('down_payments', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("student_id")->unsigned()->nullable();
$table->integer("month")->unsigned()->nullable();
$table->integer("percent")->unsigned()->nullable();
$table->tinyInteger("approved")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('down_payments');
    }
}
