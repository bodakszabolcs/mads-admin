<?php

namespace Modules\DownPayment\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\DownPayment\Entities\DownPayment;
use Modules\DownPayment\Observers\DownPaymentObserver;

class DownPaymentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'downpayment';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        DownPayment::observe(DownPaymentObserver::class);
    }
}
