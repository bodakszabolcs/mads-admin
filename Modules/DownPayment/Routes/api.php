<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\DownPayment\Http\Controllers', 'prefix' => '/down-payment', 'middleware' => ['auth:sanctum', 'role']
], function () {
    Route::post('/approve', 'DownPaymentController@approve')->name('Down payment approve');
});
