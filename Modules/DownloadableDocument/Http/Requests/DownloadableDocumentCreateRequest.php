<?php

namespace Modules\DownloadableDocument\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DownloadableDocumentCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'file' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Document name'),
'file' => __('File'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
