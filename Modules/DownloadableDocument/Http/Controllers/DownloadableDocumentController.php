<?php

namespace Modules\DownloadableDocument\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Illuminate\Support\Arr;
use Modules\DownloadableDocument\Entities\DownloadableDocument;
use Illuminate\Http\Request;
use Modules\DownloadableDocument\Http\Requests\DownloadableDocumentCreateRequest;
use Modules\DownloadableDocument\Http\Requests\DownloadableDocumentUpdateRequest;
use Modules\DownloadableDocument\Transformers\DownloadableDocumentViewResource;
use Modules\DownloadableDocument\Transformers\DownloadableDocumentListResource;

class DownloadableDocumentController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new DownloadableDocument();
        $this->viewResource = DownloadableDocumentViewResource::class;
        $this->listResource = DownloadableDocumentListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = DownloadableDocumentCreateRequest::class;
        $this->updateRequest = DownloadableDocumentUpdateRequest::class;

    }

    public function frontendIndex(Request $request, $category)
    {

        $feature = DownloadableDocument::where('is_main','=',1)->where('category','=',$category)->get();
        $down = DownloadableDocument::orderBy('category_id','ASC')->get();
        $returnArr = [];
        foreach ($down as $d) {
            $returnArr[optional($d->category)->name][] = new DownloadableDocumentListResource($d);
        }

        return response()->json(['docs' => $returnArr, 'featured' => DownloadableDocumentListResource::collection($feature)]);
    }
}
