<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDownloadableDocumentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('downloadable_documents');
        Schema::create('downloadable_documents', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("name")->nullable();
            $table->text("file")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('downloadable_documents');
    }
}
