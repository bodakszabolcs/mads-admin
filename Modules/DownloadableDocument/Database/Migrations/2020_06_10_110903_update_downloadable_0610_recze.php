<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateDownloadable0610Recze extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('downloadable_documents', function (Blueprint $table) {
            $table->tinyInteger('is_main')->default(0)->nullable();
            $table->string('main_image')->nullable();
            $table->text('category')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('downloadable_documents', function (Blueprint $table) {

        });
    }
}
