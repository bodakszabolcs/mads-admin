<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\DownloadableDocument\Entities\DownloadableDocument;

$factory->define(DownloadableDocument::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"file" => $faker->realText(),

    ];
});
