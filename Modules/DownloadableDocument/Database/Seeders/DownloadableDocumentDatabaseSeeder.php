<?php

namespace Modules\DownloadableDocument\Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DownloadableDocumentDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('downloadable_documents')->delete();

        \DB::table('downloadable_documents')->insert(array (
            0 =>
                array (
                    'id' => 1,
                    'name' => 'User guide',
                    'file' => 'https://#####.digitaloceanspaces.com/files/shares/IzLpyH1lG7ITGF3kUhCMGPwMTCO9cyYfGfM57U7M.xlsx',
                    'created_at' => '2020-03-24 11:30:00',
                    'updated_at' => '2020-03-24 11:30:00',
                    'deleted_at' => NULL,
                ),
            1 =>
                array (
                    'id' => 2,
                    'name' => 'Document 2',
                    'file' => 'https://#####.digitaloceanspaces.com/files/shares/1STM7FmOABByYanMigBhiBIweXEoXCnU2n9DNJNK.txt',
                    'created_at' => '2020-03-24 11:31:00',
                    'updated_at' => '2020-03-24 11:31:00',
                    'deleted_at' => NULL,
                ),
        ));

    }
}
