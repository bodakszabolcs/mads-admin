<?php

namespace Modules\DownloadableDocument\Observers;

use Modules\DownloadableDocument\Entities\DownloadableDocument;

class DownloadableDocumentObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function saved(DownloadableDocument $model)
    {
        DownloadableDocument::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function created(DownloadableDocument $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function updated(DownloadableDocument $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function deleted(DownloadableDocument $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function restored(DownloadableDocument $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\DownloadableDocument\Entities\DownloadableDocument  $model
     * @return void
     */
    public function forceDeleted(DownloadableDocument $model)
    {
        //
    }
}
