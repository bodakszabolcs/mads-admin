<?php

namespace Modules\DownloadableDocument\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\DownloadableDocument\Entities\DownloadableDocument;
use Modules\DownloadableDocument\Observers\DownloadableDocumentObserver;

class DownloadableDocumentServiceProvider extends ModuleServiceProvider
{
    protected $module = 'downloadabledocument';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        DownloadableDocument::observe(DownloadableDocumentObserver::class);
    }
}
