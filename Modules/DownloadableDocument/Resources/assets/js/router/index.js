import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import DownloadableDocument from '../components/DownloadableDocument'
import DownloadableDocumentList from '../components/DownloadableDocumentList'
import DownloadableDocumentCreate from '../components/DownloadableDocumentCreate'
import DownloadableDocumentEdit from '../components/DownloadableDocumentEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'downloadable-contents',
                component: DownloadableDocument,
                meta: {
                    title: 'Downloadable Contents'
                },
                children: [
                    {
                        path: 'index',
                        name: 'DownloadableDocumentList',
                        component: DownloadableDocumentList,
                        meta: {
                            title: 'Downloadable Contents',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/downloadable-contents/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/downloadable-contents/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'DownloadableDocumentCreate',
                        component: DownloadableDocumentCreate,
                        meta: {
                            title: 'Create Downloadable Contents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/downloadable-contents/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'DownloadableDocumentEdit',
                        component: DownloadableDocumentEdit,
                        meta: {
                            title: 'Edit Downloadable Contents',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/downloadable-contents/index'
                        }
                    }
                ]
            }
        ]
    }
]
