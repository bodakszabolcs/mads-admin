<?php

namespace Modules\DownloadableDocument\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


abstract class BaseDownloadableDocument extends BaseModel
{


    protected $table = 'downloadable_documents';

    protected $dates = ['created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Document name',
                    'type' => 'text',
                    ]
            ];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','file','is_main','main_image', 'category'];

    protected $casts = [];


}
