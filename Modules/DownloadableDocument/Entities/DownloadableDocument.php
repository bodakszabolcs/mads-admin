<?php

namespace Modules\DownloadableDocument\Entities;

use Modules\DownloadableDocument\Entities\Base\BaseDownloadableDocument;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;


class DownloadableDocument extends BaseDownloadableDocument
{
    use SoftDeletes, Cachable;
    public $categories =[];

    public function __construct(array $attributes = [])
    {

        $this->categories = [
            'az-erem' => __('Az erem'),
            'acta-numismatica-hungarica' => __('Acta Numizmatika Hungarica'),
            'numizmatikai-kozlony' => __('Numizmatikai közlöny'),
            'numizmatikai-evkonyv' => __('Numizmatikai évkönyv'),
            'egyeb' => __('Egyéb szakirodalom')
        ];
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }


}
