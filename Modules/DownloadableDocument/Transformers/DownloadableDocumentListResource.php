<?php

namespace Modules\DownloadableDocument\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Arr;
class DownloadableDocumentListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "file" =>  $this->file,
            "is_main" => $this->is_main,
            "main_image" => $this->main_image,
        ];
    }
}
