<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\CampaignStatus\Http\Controllers',
    'prefix' => '/campaignstatus',
    'middleware' => ['auth:sanctum', 'role']
], function () {

    Route::post( '/save-order', 'CampaignStatusController@saveOrder')->name('Save campaign order status');
});
