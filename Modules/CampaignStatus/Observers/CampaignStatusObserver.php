<?php

namespace Modules\CampaignStatus\Observers;

use Modules\CampaignStatus\Entities\CampaignStatus;

class CampaignStatusObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function saved(CampaignStatus $model)
    {
        CampaignStatus::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function created(CampaignStatus $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function updated(CampaignStatus $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function deleted(CampaignStatus $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function restored(CampaignStatus $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\CampaignStatus\Entities\CampaignStatus  $model
     * @return void
     */
    public function forceDeleted(CampaignStatus $model)
    {
        //
    }
}
