import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import CampaignStatus from '../components/CampaignStatus'
import CampaignStatusList from '../components/CampaignStatusList'
import CampaignStatusCreate from '../components/CampaignStatusCreate'
import CampaignStatusEdit from '../components/CampaignStatusEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'campaign-status',
                component: CampaignStatus,
                meta: {
                    title: 'CampaignStatuses'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CampaignStatusList',
                        component: CampaignStatusList,
                        meta: {
                            title: 'CampaignStatuses',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign-status/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign-status/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CampaignStatusCreate',
                        component: CampaignStatusCreate,
                        meta: {
                            title: 'Create CampaignStatuses',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign-status/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CampaignStatusEdit',
                        component: CampaignStatusEdit,
                        meta: {
                            title: 'Edit CampaignStatuses',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/campaign-status/index'
                        }
                    }
                ]
            }
        ]
    }
]
