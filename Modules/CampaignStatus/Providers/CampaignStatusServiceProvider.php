<?php

namespace Modules\CampaignStatus\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\CampaignStatus\Entities\CampaignStatus;
use Modules\CampaignStatus\Observers\CampaignStatusObserver;

class CampaignStatusServiceProvider extends ModuleServiceProvider
{
    protected $module = 'campaignstatus';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        CampaignStatus::observe(CampaignStatusObserver::class);
    }
}
