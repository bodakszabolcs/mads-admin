<?php

namespace Modules\CampaignStatus\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\CampaignStatus\Entities\CampaignStatus;
use Illuminate\Http\Request;
use Modules\CampaignStatus\Http\Requests\CampaignStatusCreateRequest;
use Modules\CampaignStatus\Http\Requests\CampaignStatusUpdateRequest;
use Modules\CampaignStatus\Transformers\CampaignStatusViewResource;
use Modules\CampaignStatus\Transformers\CampaignStatusListResource;

class CampaignStatusController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new CampaignStatus();
        $this->viewResource = CampaignStatusViewResource::class;
        $this->listResource = CampaignStatusListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = CampaignStatusCreateRequest::class;
        $this->updateRequest = CampaignStatusUpdateRequest::class;
    }
    public function saveOrder(Request $request){
        $index = 0;
        foreach ($request->input('list',[]) as $ps){
            CampaignStatus::where('id',$ps['id'])->update(['o'=>$index]);
            $index++;
        }
        return response()->json($this->successMessage,$this->successStatus);
    }

}
