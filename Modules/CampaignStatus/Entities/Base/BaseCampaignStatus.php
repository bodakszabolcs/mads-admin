<?php

namespace Modules\CampaignStatus\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseCampaignStatus extends BaseModel
{
    

    protected $table = 'campaign_status';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'name',
                    'title' => 'Név',
                    'type' => 'text',
                    ],[
                    'name' => 'o',
                    'title' => 'Sorrend',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['name','o'];

    protected $casts = [];

    
}
