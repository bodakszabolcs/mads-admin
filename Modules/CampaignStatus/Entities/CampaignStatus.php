<?php

namespace Modules\CampaignStatus\Entities;

use Modules\CampaignStatus\Entities\Base\BaseCampaignStatus;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class CampaignStatus extends BaseCampaignStatus
{
    use SoftDeletes, Cachable;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
