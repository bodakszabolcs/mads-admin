<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;



class Contact extends BaseModel
{
    use SoftDeletes, Cachable;

	protected $table = "company_contacts";
	protected $fillable = ["name","position","company_id","email","phone","fax"];
	protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }

    public function company()
    {
        return $this->hasOne('Modules\Company\Entities\Company', 'id', 'company_id');
    }






}
