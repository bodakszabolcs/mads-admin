<?php

namespace Modules\Company\Entities;

use Modules\Company\Entities\Base\BaseCompany;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Company extends BaseCompany
{
    use SoftDeletes, Cachable;
    public static $completitionType= [
        'student'=>'Diákonként',
        'industry'=>'Tevékenységenként',
        'site'=>'Telephelyenként',
        'empty'=>'Csak összeg'
    ];






    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }








}
