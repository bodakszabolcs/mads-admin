<?php

	namespace Modules\Company\Entities\Base;
	use App\BaseModel;
	use Modules\User\Entities\User;
	use Modules\City\Entities\City;
	use Modules\Country\Entities\Country;
	abstract class BaseCompany extends BaseModel
	{
		protected $table = 'companies';
		protected $dates = ['created_at', 'updated_at'];

		public function __construct(array $attributes = [])
		{
			parent::__construct($attributes);
		}

		public function getFilters()
		{
			return parent::getFilters();
		}

		protected $fillable = ['name', 'tax', 'tax_eu', 'company_number', 'delegate', 'delegate_position', 'delegate_comment', 'contract_date', 'contract_expire', 'contract_type', 'agancy_contract_date', 'works_contarct_date', 'payment_deadline',
            'paper_contract', 'mads_company', 'user_id', 'pob', 'zip', 'country_id', 'city_id', 'postal_address', 'work_address','completition_type','completition_text','street','number','importer','reject','reject_message',
            'invoice_type','invoice_name','invoice_email','invoice_phone','gdpr','online_presence','medical_certificate'
            ];
		protected $casts = [];

		public function user()
		{
			return $this->hasOne('Modules\User\Entities\User', 'id', 'user_id');
		}

		public function city()
		{
			return $this->hasMany('Modules\City\Entities\City', 'id', 'city_id');
		}
		public function contacts()
		{
			return $this->hasMany('Modules\Company\Entities\Contact', 'company_id', 'id');
		}
		public function country()
		{
			return $this->hasMany('Modules\Country\Entities\Country', 'id', 'country_id');
		}
        public function forms()
        {
            return $this->hasMany('Modules\Country\Entities\Forms', 'id', 'company_id');
        }
        public function industry()
        {
            return $this->hasMany('Modules\Company\Entities\Industry', 'company_id', 'id');
        }
	}
