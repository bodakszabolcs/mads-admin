<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\User\Entities\User;


class Comments extends BaseModel
{
    use Cachable, SoftDeletes;

	protected $table = "company_comments";
	protected $fillable = ["company_id","comment"];
	protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        return parent::getFilters();
    }








}
