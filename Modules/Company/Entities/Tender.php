<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;



class Tender extends BaseModel
{
    use Cachable;

	protected $table = "company_tenders";
	protected $fillable = ["frame_limit","start","end","company_id"];
	protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    public function fillAndSave(array $request)
    {
        $request['start']=str_replace('-','',$request['start']);
        $request['end']=str_replace('-','',$request['end']);
        $this->fill($request);

        $this->save();

        return $this;
    }








}
