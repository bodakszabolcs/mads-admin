<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;



class Bonus extends BaseModel
{
    use SoftDeletes, Cachable;

	protected $table = "company_bonus";
	protected $fillable = ["name","bonus_algorithm","industries","active","company_id"];
	protected $dates = ['created_at', 'updated_at'];

    protected $casts =    ['bonus_algorithm'=>'array',"industries"=>'array'];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }

    public function company()
    {
        return $this->hasOne('Modules\Company\Entities\Company', 'id', 'company_id');
    }






}
