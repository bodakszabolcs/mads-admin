<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;



class Site extends BaseModel
{
    use SoftDeletes, Cachable;

	protected $table = "company_sites";
	protected $fillable = ["name","work_location","company_id"];
	protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
