<?php

	namespace Modules\Company\Entities;
	use App\BaseModel;
	use Illuminate\Database\Eloquent\SoftDeletes;
	use GeneaLabs\LaravelModelCaching\Traits\Cachable;
	class Industry extends BaseModel
	{
		use SoftDeletes, Cachable;
		protected $table = "company_industries";
		protected $fillable = [
			"name", "company_id", 'site_id', 'user_id','user2_id', 'feor_id', 'instructor', 'work_schedule','work_category_id', 'night_bonus', 'night_bonus_from', 'night_bonus_to', 'night_bonus_company',
			'divisor', 'multiplier', 'sunday_bonus', 'sunday_bonus_from', 'sunday_bonus_to', 'sunday_bonus_company', 'saturday_bonus', 'saturday_bonus_from', 'saturday_bonus_to', 'saturday_bonus_company', 'festive_bonus',
			'festive_bonus_from', 'festive_bonus_to', 'festive_bonus_company', 'break', 'break_from', 'break_to', 'price_hour', 'price_company_hourly', 'price_company_fix','teaor','gdpr','contact_id','hourly_algorithm','cost_center','recruiter','amrest_position','region','archive','order','medical_certificate'
		];
		protected $dates = ['created_at', 'updated_at'];

		public function __construct(array $attributes = [])
		{
			parent::__construct($attributes);
		}

		public function getFilters()
		{

			return parent::getFilters();
		}

		public function user()
		{
			return $this->hasOne('Modules\User\Entities\User', 'id','user_id' );
		}
        public function user2()
        {
            return $this->hasOne('Modules\User\Entities\User', 'id','user2_id' );
        }
        public function instructor()
        {
            return $this->hasOne('Modules\Company\Entities\Contact', 'id','contact_id' );
        }
        public function feor()
        {
            return $this->hasOne('Modules\Feor\Entities\Feor', 'id','feor_id' );
        }

		public function site()
		{
			return $this->hasOne('Modules\Company\Entities\Site', 'id', 'site_id');
		}
        public function category()
        {
            return $this->hasOne('Modules\WorkCategory\Entities\WorkCategory', 'id', 'work_category_id');
        }
        public function company()
        {
            return $this->hasOne('Modules\Company\Entities\Company', 'id', 'company_id');
        }
	}
