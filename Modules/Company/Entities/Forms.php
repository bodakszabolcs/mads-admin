<?php

namespace Modules\Company\Entities;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\Student\Entities\Student;
use Modules\Student\Entities\StudentWorkSafety;
use Modules\User\Entities\User;


class Forms extends BaseModel
{
    use Cachable;

	protected $table = "company_forms";
	protected $fillable = ["name","data","company_id","type","title","membership"];
	protected $dates = ['created_at', 'updated_at'];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }
    public function replaceData($industry_id,$student_id){
        $content = $this->data;

        $industry = Industry::where('id',$industry_id)->first();
        $project = User::where('id',$industry->user_id)->first();
        $student = Student::where('id',$student_id)->first();
        $safety1 = StudentWorkSafety::where('student_id',$student_id)->where('type',1)->orderBy('expire','desc')->first();
        $safety2 = StudentWorkSafety::where('student_id',$student_id)->where('type',2)->orderBy('expire','desc')->first();
        $safety3 = StudentWorkSafety::where('student_id',$student_id)->where('type',3)->orderBy('expire','desc')->first();
        $safety4 = StudentWorkSafety::where('student_id',$student_id)->where('type',4)->orderBy('expire','desc')->first();

        $content =str_replace('[name]',$student->name,$content);
        $content =str_replace('[student_number]',$student->student_number,$content);
        $content =str_replace('[identity_card]',$student->identity_card,$content);
        $content =str_replace('[birth_location]',$student->birth_location,$content);
        $content =str_replace('[birth_date]',$student->birth_date,$content);
        $content =str_replace('[mothers_name]',$student->mothers_name,$content);
        $content =str_replace('[nationality]',optional($student->nationalities)->name,$content);
        $content =str_replace('[passport]',$student->passport,$content);
        $content =str_replace('[eu_expire_date]',$student->eu_expire_date,$content);
        $content =str_replace('[exit_date]',$student->exit_date,$content);
        $content =str_replace('[gender]',array_get(Student::$gender,''.$student->gender),$content);
        $content =str_replace('[phone]',$student->phone,$content);
        $content =str_replace('[email]',$student->email,$content);
        $content =str_replace('[country_id]',optional($student->country)->name,$content);
        $content =str_replace('[zip]',$student->zip,$content);
        $content =str_replace('[city_id]',optional($student->city)->name,$content);
        $content =str_replace('[street]',$student->street,$content);
        $content =str_replace('[area_type_id]',optional($student->areatype)->name,$content);
        $content =str_replace('[house_number]',$student->house_number,$content);
        $content =str_replace('[building]',$student->building,$content);
        $content =str_replace('[stairs]',$student->stairs,$content);
        $content =str_replace('[level]',$student->level,$content);
        $content =str_replace('[door]',$student->door,$content);
        $content =str_replace('[notification_address]',$student->notitication_address,$content);
        $content =str_replace('[bank_account_number]',$student->bank_account_number,$content);
        $content =str_replace('[tax_number]',$student->tax_number,$content);
        $content =str_replace('[taj]',$student->taj,$content);
        $content =str_replace('[date_of_entry]',$student->date_of_entry,$content);
        $content =str_replace('[nav_declaration]',$student->nav_declaration,$content);
        $content =str_replace('[membership_agreement]',$student->membership_agreement,$content);
        $content =str_replace('[employment_contract]',$student->employment_contract,$content);


        $content =str_replace('[industry_name]',$industry->name,$content);
        $content =str_replace('[industry_site_id]',optional($industry->site)->name,$content);
        $content =str_replace('[industry_work_category_id]',optional($industry->category)->name,$content);
        $content =str_replace('[industry_price_hour]',$student->price_hour.' ft/óra',$content);
        $content =str_replace('[industry_instructor]',$student->instructor,$content);

        $content =str_replace('[project_name]',optional($project)->name,$content);
        $content =str_replace('[project_email]',optional($project)->email,$content);

        $content =str_replace('[work_safety1_date]',(optional($safety1)->created)?date('Y.m.d',strtotime(optional($safety1)->created)):'',$content);
        $content =str_replace('[work_safety1_price]',optional($safety1)->price. ' ft',$content);
        $content =str_replace('[work_safety2_date]',(optional($safety2)->created)?date('Y.m.d',strtotime(optional($safety2)->created)):'',$content);
        $content =str_replace('[work_safety2_price]',optional($safety2)->price. ' ft',$content);
        $content =str_replace('[work_safety3_date]',(optional($safety3)->created)?date('Y.m.d',strtotime(optional($safety3)->created)):'',$content);
        $content =str_replace('[work_safety3_price]',optional($safety3)->price. ' ft',$content);
        $content =str_replace('[work_safety4_date]',(optional($safety4)->created)?date('Y.m.d',strtotime(optional($safety4)->created)):'',$content);
        $content =str_replace('[work_safety4_price]',optional($safety4)->price. ' ft',$content);

       $content = str_replace('[project_phone]',optional($project)->phone,$content);
        $content =preg_replace('/(<table.+?)style=".+?"(>.+?)/i', "$1$2", $content);
        $content =preg_replace('/(<table.+?)style=".+?"(>.+?)/i', "$1$2", $content);
        $content =preg_replace('/(<td.+?)width=".+?"(>.+?)/i', "$1$2", $content);
        $content =str_replace('width=', "data-width=", $content);
        $content =str_replace('td style=', "td data-stale=", $content);
        $content =str_replace('table style=', "table data-stale=", $content);
        $content = ' <div class="paper" id="eseti" >
        <style>
          p , strong{
            font-size:13px; !important;
          }
            .paper div, .paper span, .paper p, .paper strong
            {
                font-size:13px; !important;
            }
            table{
                width:100% !important;
            }
            .paper table tr td,.paper table tr th,
            {
                margin: 0px;
                padding: 1px;
                vertical-align: top;
                font-size:15px;
                line-height: normal;
            }
            .paper{
               
                font-size: 11px;
                text-align: justify;
                font-size:15px;
            }
            .parag, .para10 {
                text-indent: 30pt;
                text-align: justify;
                line-height: initial;
                font-size:15px;
            }
        </style>'.$content.'</div>';

        return $content;

    }








}
