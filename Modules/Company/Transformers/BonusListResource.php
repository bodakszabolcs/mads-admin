<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class BonusListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "active" => ConstansHelper::bool($this->active),
            "industries_size" => sizeof($this->industries),
            "industries" => $this->industries,
            "bonus_algorithm" => $this->bonus_algorithm,
            "company_id" => $this->company_id,
		     ];
    }
}
