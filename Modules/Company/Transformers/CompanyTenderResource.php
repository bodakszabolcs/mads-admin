<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CompanyTenderResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company_name" => $this->company_name,
		    "frame_limit" => $this->frame_limit,
		    "frame_used" => $this->frame_used,
		    "frame_left" => $this->frame_limit-$this->frame_used,
		    "start" => $this->start,
		    "end" => $this->end,
		];
    }
}
