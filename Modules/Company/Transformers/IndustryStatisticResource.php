<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\DB;
use Modules\Company\Entities\Contact;
use Modules\Company\Entities\Site;
use Modules\Feor\Entities\Feor;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class IndustryStatisticResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company_id" => $this->company_id,
		    "name" => $this->name,
            "project_count" => $this->project_count,
            "student_count" => $this->student_count,
            "number_of_employees" => $this->number_of_employees,
            ];


    }
}
