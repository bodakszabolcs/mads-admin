<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\DB;
use Modules\Company\Entities\Contact;
use Modules\Company\Entities\Site;
use Modules\Feor\Entities\Feor;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class IndustryViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company_id" => $this->company_id,
		    "work_category_id" => $this->work_category_id,
		    "site_id" => $this->site_id,
		    "site" => $this->site,
		    "user_id" => $this->user_id,
		    "user2_id" => $this->user2_id,
		    "feor_id" => $this->feor_id,
		    "feor" => $this->feor,
		    "name" => $this->name,
		    "instructor" => $this->instructor,
		    "instructor_name" => Contact::where('id',$this->contact_id)->first(),
		    "contact_id" => $this->contact_id,
		    "work_schedule" => ($this->work_schedule)?$this->work_schedule:'Részmunkaidő',
		    "work_location" => optional($this->site)->work_location,
		    "delegate" => $this->delegate,
		    "importer" => $this->importer,
		    "divisor" => $this->divisor,
		    "multiplier" => $this->multiplier,
            'contact' =>$this->user,
            'contact2' =>$this->user2,
            'teaor' =>$this->teaor,
            'gdpr' =>optional($this->company)->gdpr,
            "project_count" => optional($this)->project_count,
            "student_count" => optional($this)->student_count,
            "number_of_employees" => optional($this)->number_of_employees,
		    "night_bonus" => $this->night_bonus,
		    "night_bonus_from" => ConstansHelper::formatTime($this->night_bonus_from),
		    "night_bonus_to" => ConstansHelper::formatTime($this->night_bonus_to),
		    "night_bonus_company" => $this->night_bonus_company,

            "sunday_bonus" => $this->sunday_bonus,
            "sunday_bonus_from" => ConstansHelper::formatTime($this->sunday_bonus_from),
            "sunday_bonus_to" => ConstansHelper::formatTime($this->sunday_bonus_to),
            "sunday_bonus_company" => $this->sunday_bonus_company,

            "saturday_bonus" => $this->saturday_bonus,
            "saturday_bonus_from" => ConstansHelper::formatTime($this->saturday_bonus_from),
            "saturday_bonus_to" => ConstansHelper::formatTime($this->saturday_bonus_to),
            "saturday_bonus_company" => $this->saturday_bonus_company,

            "festive_bonus" => $this->festive_bonus,
            "festive_bonus_from" => ConstansHelper::formatTime($this->festive_bonus_from),
            "festive_bonus_to" => ConstansHelper::formatTime($this->festive_bonus_to),
            "festive_bonus_company" => $this->festive_bonus_company,

            "break" => $this->break,
            "break_from" => ConstansHelper::formatTime($this->break_from),
            "break_to" => ConstansHelper::formatTime($this->break_to),

            "price_company_fix" => $this->price_company_fix,
            "price_fix" => $this->price_company_fix,
            "price_hour" => $this->price_hour,
            "price_company_hourly" => $this->price_company_hourly,
            "hourly_algorithm" => $this->hourly_algorithm,
            "cost_center" => $this->cost_center,
            "recruiter" => $this->recruiter,
            "amrest_position" => $this->amrest_position,
            "region" => $this->region,
            "archive" => ($this->archive===null)?0:$this->archive,
            "order" => ($this->order===null)?0:$this->order,
            'employees_change'=>$this->employees_change,
            'completion_date'=>$this->completion_date,
             "medical_certificate" => $this->medical_certificate,
            ];


    }
}
