<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Contact;


class ContactListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "position" => $this->position,
		    "email" => $this->email,
		    "phone" => $this->phone,
		    "company_id" => $this->company_id,

		     ];
    }
}
