<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\DB;
use Modules\Company\Entities\Company;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class CompanyViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "tax" => $this->tax,
		    "tax_eu" => $this->tax_eu,
		    "company_number" => $this->company_number,
		    "delegate" => $this->delegate,
		    "delegate_position" => $this->delegate_position,
		    "delegate_comment" => $this->delegate_comment,
		    "contract_date" => $this->contract_date,
		    "contract_expire" => $this->contract_expire,
		    "contract_type" => $this->contract_type,
		    "agancy_contract_date" => $this->agancy_contract_date,
		    "works_contarct_date" => $this->works_contarct_date,
		    "payment_deadline" => $this->payment_deadline,
		    "paper_contract" => $this->paper_contract,
		    "mads_company" => $this->mads_company,
		    "user_id" => $this->user_id,
		    "pob" => $this->pob,
		    "zip" => $this->zip,
		    "online_presence" => $this->online_presence,
		    "country_id" => $this->country_id,
		    "city_id" => $this->city_id,
		    "postal_address" => $this->postal_address,
		    "completition_type" => $this->completition_type,
		    "completition_text" => $this->completition_text,
            "street"=>$this->street,
            "number"=>$this->number,
            "importer"=>$this->importer,
            "invoice_type"=>$this->invoice_type,
            "invoice_name"=>$this->invoice_name,
            "invoice_email"=>$this->invoice_email,
            "invoice_phone"=>$this->invoice_phone,
		    "work_address" => $this->work_address,
		    "gdpr" => $this->gdpr,
		    "reject" => $this->reject,
		    "reject_message" => $this->reject_message,
		    "medical_certificate" => $this->medical_certificate,
				"selectables" => [
				"user" => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
				"city" => City::pluck("name","id"),
				"country" => Country::pluck("name","id"),
                "completions"=>Company::$completitionType,
				]
		     ];
    }
}
