<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class TenderListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "frame_limit" => $this->frame_limit,
		    "start" => $this->start,
		    "end" => $this->end,
		    "company_id" => $this->company_id,
		     ];
    }
}
