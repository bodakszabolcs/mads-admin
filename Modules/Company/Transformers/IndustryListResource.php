<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class IndustryListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "contact_id" => $this->contact_id,
		    "project_count" => optional($this)->project_count,
            "student_count" => optional($this)->student_count,
		    "work_schedule" => $this->work_schedule,
		    "site" => $this->site->name,
		    "work_location" => $this->site->work_location,
		    "user" => $this->user->lastname.' '.$this->user->firstname,
		     ];
    }
}
