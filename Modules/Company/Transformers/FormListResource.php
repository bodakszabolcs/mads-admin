<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Contact;


class FormListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {

        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "data" => $this->data,
		    "type" => $this->type,
		    "company_id" => $this->company_id,
            "membership" => $this->membership,
            "title" =>$this->title,

		     ];
    }
}
