<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CompanyContactResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company_name" => $this->company_name,
		    "name" => $this->name,
		    "phone" => $this->phone,
		    "email" => $this->email,
		    "position" => $this->position,
		];
    }
}
