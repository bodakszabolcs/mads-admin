<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class CompanyListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->company_name,
		    "tax" => $this->tax,
		    "industry_name" => $this->industry_name,
		    "campaign_name" => optional($this)->campaign_name,
		    "user_name" => $this->user_name,
		    "delegate" => $this->delegate,
		    "online_presence" => $this->online_presence,
		    "created_at" => date('Y.m.d',strtotime($this->created_at)),
		    "reject" => $this->reject,
		    "reject_message" => $this->reject_message,
		    "cnt" => $this->cnt,
		    "contract_date" => ($this->contract_date)?ConstansHelper::formatShortDate($this->contract_date):'Nincs',
		    "contract_expire" => ($this->contract_expire)?ConstansHelper::formatShortDate($this->contract_expire):(($this->contract_date)?'Határozatlan':''),
		    "paper_contract" => ($this->paper_contract)?'Van':'Nincs',
		    "agancy_contract_date" => ($this->agancy_contract_date)?ConstansHelper::formatShortDate($this->agancy_contract_date):'Nincs',
		    "works_contract_date" => ($this->works_contarct_date)?ConstansHelper::formatShortDate($this->works_contarct_date):'Nincs',
		];
    }
}
