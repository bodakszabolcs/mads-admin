<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class ContactViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "company_id" => $this->company_id,
		    "name" => $this->name,
		    "position" => $this->position,
		    "email" => $this->email,
		    "phone" => $this->phone,
        ];
    }
}
