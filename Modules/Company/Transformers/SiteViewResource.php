<?php

namespace Modules\Company\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class SiteViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "work_location" => $this->work_location,
		    "company_id" => $this->company_id,
		     ];
    }
}
