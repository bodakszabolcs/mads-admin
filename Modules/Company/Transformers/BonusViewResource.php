<?php

namespace Modules\Company\Transformers;

use App\Helpers\ConstansHelper;
use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\Company\Entities\Industry;
use Modules\User\Entities\User;
		use Modules\City\Entities\City;
		use Modules\Country\Entities\Country;


class BonusViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "name" => $this->name,
		    "active" => ConstansHelper::bool($this->active),
            "industries" => $this->industries,
            "bonus_algorithm" => $this->bonus_algorithm,
		    "company_id" => $this->company_id,
            "all_industry"=>Industry::where('company_id',$this->company_id)->get()->pluck('name','id')->toArray()
		     ];
    }
}
