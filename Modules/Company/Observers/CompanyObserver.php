<?php

namespace Modules\Company\Observers;

use Modules\Company\Entities\Company;

class CompanyObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function saved(Company $model)
    {
        Company::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function created(Company $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function updated(Company $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function deleted(Company $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function restored(Company $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Company\Entities\Company  $model
     * @return void
     */
    public function forceDeleted(Company $model)
    {
        //
    }
}
