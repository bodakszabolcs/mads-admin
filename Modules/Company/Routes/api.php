<?php
	/*
	|--------------------------------------------------------------------------
	| API Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register API routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| is assigned the "api" middleware group. Enjoy building your API!
	|
	*/
    Route::group([
        'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/company', 'middleware' => ['auth:sanctum', 'role']
    ], function () {
        Route::get('/list/{type}', 'CompanyController@listByType')->name('company list by type');
        Route::get('/active-export', 'CompanyController@activeExport')->name('Active industry export');
        Route::get('/monthly-fixing/{company}/{year}', 'CompanyController@monthlyFixing')->name('company monthly fixing');
        Route::get('/preview-form/{company}/{form}', 'CompanyController@previewForm')->name('company document preview');
        Route::get('/get-worker/{id}', 'CompanyController@getWorker')->name('company worker modal');
        Route::get('/get-fixings/{company}', 'CompanyController@getFixingByCompany')->name('company view fixings');
        Route::get('/list-company', 'CompanyController@listCompany')->name('company list');
        Route::get('/list-users/{id}', 'CompanyController@listUsers')->name('company user list');
        Route::post('/create-users/{id}', 'CompanyController@createUserFromContact')->name('company create access');
        Route::post('/edit-user/{id}', 'CompanyController@editUser')->name('company contact edit');
        Route::delete('/delete/{id}', 'CompanyController@deleteUser')->name('company delete user');
        Route::post('/generate-password/{id}', 'CompanyController@generatePassword')->name('company contact generate password');
        Route::get('/tenders', 'CompanyController@listTenders')->name('company list tender');
        Route::get('/list-company-contact/{export?}', 'CompanyController@listCompanyContact')->name('company list contact');
        Route::get('/list-worker/{id}', 'CompanyController@listWorker')->name('company list workers');
        Route::get('/list-requests/{id}', 'CompanyController@companyRequests')->name('Árajánlatok megtekintése');
        Route::get('/list-questions/{id}', 'CompanyController@companyQuestions')->name('Kérdásek megtekintése');
        Route::post('/set-contact-all-project/{id}', 'CompanyController@setContactAllProject')->name('Kapcsolattartó beállítása a projectekhez');
        Route::post('/set-contact-all-industry/{id}', 'CompanyController@setContactAllIndustry')->name('Kapcsolattartó beállítása a tevékenyéghez');
    });
	Route::group([
		'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/industry', 'middleware' => ['auth:sanctum',  'role']
	], function () {
		Route::put('/edit', 'CompanyController@editIndustry')->name('create industry');
		Route::get('/list/{id}', 'CompanyController@listIndustry')->name('list industry');
		Route::get('/show/{id}', 'CompanyController@showIndustry')->name('show industry');
		Route::post('/edit/{id}', 'CompanyController@editIndustry')->name('edit industry');
		Route::post('/archive/{id}', 'CompanyController@archiveIndustry')->name('archive industry');
        Route::post('/delete-archive/{id}', 'CompanyController@deleteArchiveIndustry')->name('delete archive industry');
        Route::post('/save-order', 'CompanyController@saveIndustryOrder')->name('save industry order');
		Route::delete('/delete/{id}', 'CompanyController@deleteIndustry')->name('delete industry');
	});
	Route::group([
		'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/site', 'middleware' => ['auth:sanctum', 'role']
	], function () {
		Route::put('/edit', 'CompanyController@editSite')->name('create site');
		Route::get('/show/{id}', 'CompanyController@showSite')->name('show site');
		Route::get('/list/{id}', 'CompanyController@listSite')->name('list site');
		Route::post('/edit/{id}', 'CompanyController@editSite')->name('edit site');
		Route::delete('/delete/{id}', 'CompanyController@deleteSite')->name('delete site');
	});
    Route::group([
        'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/bonus', 'middleware' => ['auth:sanctum', 'role']
    ], function () {
        Route::put('/edit', 'CompanyController@editBonus')->name('create bonus');
        Route::get('/show/{id}', 'CompanyController@showBonus')->name('show bonus');
        Route::get('/list/{id}', 'CompanyController@listBonus')->name('list bonus');
        Route::post('/edit/{id}', 'CompanyController@editBonus')->name('edit bonus');
        Route::delete('/delete/{id}', 'CompanyController@deleteBonus')->name('delete bonus');
    });
	Route::group([
		'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/contact', 'middleware' => ['auth:sanctum', 'accesslog', 'role']
	], function () {
		Route::put('/edit', 'CompanyController@editContact')->name('create contact');
		Route::get('/show/{id}', 'CompanyController@showContact')->name('show contact');
		Route::get('/list/{id}', 'CompanyController@listContact')->name('list contact');
		Route::post('/edit/{id}', 'CompanyController@editContact')->name('edit contact');
		Route::delete('/delete/{id}', 'CompanyController@deleteContact')->name('delete contact');
	});
    Route::group([
        'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/tender', 'middleware' => ['auth:sanctum', 'role']
    ], function () {
        Route::put('/edit', 'CompanyController@editTender')->name('create tender');
        Route::get('/show/{id}', 'CompanyController@showTender')->name('show tender');
        Route::get('/list/{id}', 'CompanyController@listTender')->name('list tender');
        Route::post('/edit/{id}', 'CompanyController@editTender')->name('edit tender');
        Route::delete('/delete/{id}', 'CompanyController@deleTender')->name('delete tender');
    });
    Route::group([
        'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/document', 'middleware' => ['auth:sanctum', 'role']
    ], function () {
        Route::put('/edit', 'CompanyController@editDocument')->name('create document');
        Route::get('/show/{id}', 'CompanyController@showDocument')->name('show document');
        Route::get('/list/{id}', 'CompanyController@listDocument')->name('list document');
        Route::post('/edit/{id}', 'CompanyController@editDocument')->name('edit document');
        Route::delete('/delete/{id}', 'CompanyController@deleteDocument')->name('delete document');
    });
    Route::group([
        'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/form', 'middleware' => ['auth:sanctum', 'role']
    ], function () {
        Route::put('/edit', 'CompanyController@editForm')->name('create form');
        Route::get('/show/{id}', 'CompanyController@showForm')->name('show form');
        Route::get('/list/{id}', 'CompanyController@listForm')->name('list form');
        Route::post('/edit/{id}', 'CompanyController@editForm')->name('edit form');
        Route::delete('/delete/{id}', 'CompanyController@deleteForm')->name('delete form');
    });

Route::group([
    'namespace' => 'Modules\Company\Http\Controllers', 'prefix' => '/comments', 'middleware' => ['auth:sanctum', 'role']
], function () {
    Route::put('/edit', 'CompanyController@editComments')->name('create comment');
    Route::get('/list/{id}', 'CompanyController@listComments')->name('list comments');

});

