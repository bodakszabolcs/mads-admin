<?php

return [
    'name' => 'Company',

                 'menu_order' => 33,

                 'menu' => [
                     [

                      'icon' =>'fa fa-address-card',

                      'title' =>'Cégek',

                      'route' =>'/'.env('ADMIN_URL').'/company/index',

                     ]

                 ]
];
