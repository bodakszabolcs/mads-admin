import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Company from '../components/Company'
import CompanyList from '../components/CompanyList'
import CompanyCreate from '../components/CompanyCreate'
import CompanyEdit from '../components/CompanyEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'company',
                component: Company,
                meta: {
                    title: 'Companies'
                },
                children: [
                    {
                        path: 'index',
                        name: 'CompanyList',
                        component: CompanyList,
                        meta: {
                            title: 'Companies',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/company/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/company/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'CompanyCreate',
                        component: CompanyCreate,
                        meta: {
                            title: 'Create Companies',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/company/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'CompanyEdit',
                        component: CompanyEdit,
                        meta: {
                            title: 'Edit Companies',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/company/index'
                        }
                    }
                ]
            }
        ]
    }
]
