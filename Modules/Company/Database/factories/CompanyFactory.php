<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Company\Entities\Company;

$factory->define(Company::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),
"tax" => $faker->realText(),
"tax_eu" => $faker->realText(),
"company_number" => $faker->realText(),
"delegate" => $faker->realText(),
"delegate_position" => $faker->realText(),
"delegate_comment" => $faker->realText(),
"contract_date" => $faker->date(),
"contract_expire" => $faker->date(),
"contract_type" => rand(1,10),
"agancy_contract_date" => $faker->date(),
"works_contarct_date" => $faker->date(),
"payment_deadline" => $faker->realText(),
"paper_contract" => rand(1,10),
"mads_company" => rand(1000,5000),
"user_id" => rand(1000,5000),
"pob" => $faker->realText(),
"zip" => $faker->realText(),
"country_id" => rand(1000,5000),
"city_id" => rand(1000,5000),
"postal_address" => $faker->realText(),
"work_address" => $faker->realText(),

    ];
});
