<?php
	use Illuminate\Support\Facades\Schema;
	use Illuminate\Database\Schema\Blueprint;
	use Illuminate\Database\Migrations\Migration;
	class CreateCompanyTable extends Migration
	{
		/**
		 * Run the migrations.
		 *
		 * @return void
		 */
		public function up()
		{
			Schema::dropIfExists('companies');
			Schema::create('companies', function (Blueprint $table) {
				$table->bigIncrements("id");
				$table->text("name")->nullable();
				$table->text("tax")->nullable();
				$table->text("tax_eu")->nullable();
				$table->text("company_number")->nullable();
				$table->text("delegate")->nullable();
				$table->text("delegate_position")->nullable();
				$table->text("delegate_comment")->nullable();
				$table->date("contract_date")->nullable();
				$table->date("contract_expire")->nullable();
				$table->tinyInteger("contract_type")->unsigned()->nullable();
				$table->date("agancy_contract_date")->nullable();
				$table->date("works_contarct_date")->nullable();
				$table->text("payment_deadline")->nullable();
				$table->tinyInteger("paper_contract")->unsigned()->nullable();
				$table->integer("mads_company")->unsigned()->nullable();
				$table->integer("user_id")->unsigned()->nullable();
				$table->text("pob")->nullable();
				$table->text("zip")->nullable();
				$table->integer("country_id")->unsigned()->nullable();
				$table->integer("city_id")->unsigned()->nullable();
				$table->text("postal_address")->nullable();
				$table->text("work_address")->nullable();
				$table->timestamps();
				$table->softDeletes();
				$table->index(["deleted_at"]);

			});
		}

		/**
		 * Reverse the migrations.
		 *
		 * @return void
		 */
		public function down()
		{
			Schema::dropIfExists('companies');
		}
	}
