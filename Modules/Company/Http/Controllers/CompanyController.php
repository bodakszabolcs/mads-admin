<?php

	namespace Modules\Company\Http\Controllers;
	use App\Exports\BaseArrayExport;
    use App\Helpers\ConstansHelper;
    use App\Helpers\PdfHelper;
    use App\Http\Controllers\AbstractLiquidController;

    use Illuminate\Database\Eloquent\Model;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Support\Arr;
    use Illuminate\Support\Facades\App;
    use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Hash;
    use Illuminate\Support\Facades\Mail;
    use Illuminate\Support\Facades\Response;
    use Illuminate\Support\Str;
    use Maatwebsite\Excel\Facades\Excel;
    use Modules\Campaig\Entities\CampaignCompanyQuestion;
    use Modules\Campaig\Entities\CampaignCompanyRequest;
    use Modules\Campaig\Transformers\CampaignRequestListResource;
    use Modules\Campaig\Transformers\CampaigQuestionListResource;
    use Modules\Company\Entities\Bonus;
    use Modules\Company\Entities\Comments;
    use Modules\Company\Entities\Company;
	use Illuminate\Http\Request;
	use Modules\Company\Entities\Contact;
    use Modules\Company\Entities\Document;
    use Modules\Company\Entities\Forms;
    use Modules\Company\Entities\Industry;
	use Modules\Company\Entities\Site;
    use Modules\Company\Transformers\CommentsListResource;
    use Modules\Project\Entities\Project;
    use Modules\Company\Entities\Tender;
    use Modules\Company\Http\Requests\BonusEditRequest;
    use Modules\Company\Http\Requests\CompanyCreateRequest;
	use Modules\Company\Http\Requests\CompanyUpdateRequest;
	use Modules\Company\Http\Requests\ContactEditRequest;
    use Modules\Company\Http\Requests\DocumentEditRequest;
    use Modules\Company\Http\Requests\FormEditRequest;
    use Modules\Company\Http\Requests\IndustryEditRequest;
	use Modules\Company\Http\Requests\SiteEditRequest;
    use Modules\Company\Http\Requests\TenderEditRequest;
    use Modules\Company\Transformers\BonusListResource;
    use Modules\Company\Transformers\BonusViewResource;
    use Modules\Company\Transformers\CompanyContactResource;
    use Modules\Company\Transformers\CompanyTenderResource;
    use Modules\Company\Transformers\CompanyViewResource;
	use Modules\Company\Transformers\CompanyListResource;
	use Modules\Company\Transformers\ContactListResource;
	use Modules\Company\Transformers\ContactViewResource;
    use Modules\Company\Transformers\DocumentListResource;
    use Modules\Company\Transformers\DocumentViewResource;
    use Modules\Company\Transformers\FormListResource;
    use Modules\Company\Transformers\FormViewResource;
    use Modules\Company\Transformers\IndustryViewResource;
	use Modules\Company\Transformers\SiteListResource;
	use Modules\Company\Transformers\SiteViewResource;
    use Modules\Company\Transformers\TenderListResource;
    use Modules\Company\Transformers\TenderViewResource;
    use Modules\Feor\Entities\Feor;
    use Modules\Fixing\Entities\Fixing;
    use Modules\Fixing\Http\Controllers\FixingSummationController;
    use Modules\Project\Entities\ProjectStudent;
    use Modules\Student\Entities\Student;
    use Modules\User\Emails\GeneratePassword;
    use Modules\User\Entities\User;
    use Modules\User\Transformers\UserListAccessResource;
    use Modules\User\Transformers\UserListResource;
    use Modules\WorkCategory\Entities\WorkCategory;
    use Mpdf\Form;
    use Mpdf\Mpdf;

    class CompanyController extends AbstractLiquidController
	{
		public function __construct(Request $request)
		{
			parent::__construct($request);
			$this->model = new Company();
			$this->viewResource = CompanyViewResource::class;
			$this->listResource = CompanyListResource::class;
			$this->useResourceAsCollection = true;
			$this->createRequest = CompanyCreateRequest::class;
			$this->updateRequest = CompanyUpdateRequest::class;
		}
        public function setContactAllIndustry(Request $request,$id){
            $company = Company::where('id',$id)->first();
            $industries = Industry::where('company_id',$id)->get();
            foreach ($industries as $ind){
                $ind->user_id = $company->user_id;
                $ind->save();
            }
            return response()->json("OK");

        }
        public function setContactAllProject(Request $request,$id){
            $company = Company::where('id',$id)->first();
            $industries = Project::where('company_id',$id)->get();
            foreach ($industries as $ind){
                $ind->user_id = $company->user_id;
                $ind->save();
            }
            return response()->json("OK");

        }
        public function activeExport(Request $request){


            $list = Industry::leftJoin('companies','companies.id','=','company_id')->select(DB::raw("companies.name, companies.tax, company_industries.name as industry_name, company_industries.divisor, company_industries.multiplier,price_hour, price_company_hourly, price_hour + price_company_hourly"))
                ->where('company_industries.archive','=', 0)
                ->whereNull('company_industries.deleted_at')
                ->whereNull('companies.deleted_at')
                ->orderBy('companies.name')->get()->toArray();
            $export = new BaseArrayExport(['Cég','Adószám','Tevékenység','Osztó','Szorzó','Órabér','Cég órabér','Számlázandó'], "Aktív tevékenységek", $list);

            return $export->download(Str::slug('Aktív tevékenységek') . '.xlsx');
        }
        public function previewForm(Request $request,$company,$form){

            $form = Forms::where('id',$form)->first();
            $company = Company::where('id',$company)->first();
            $student = Student::whereNotNull('nav_declaration')->first();
            $industry = Industry::where('company_id',$company->id)->whereNotNull('user_id')->first();

            $content = $form->replaceData($industry->id,$student->id);
            $content = str_replace('[date]',date('Y-m-d'),$content);
            $config= PdfHelper::$config;
            if($form->title){
                $config['margin_top'] = 55;


            }else {
                $config['margin_top'] = 15;
            }

            $pdf = new Mpdf($config);
            if($form->title) {
                $header = view('student.templateHeader', ['title' => $form->title]);
                $pdf->SetHTMLHeader($header->render());
                $pdf->showWatermarkImage=true;

                $pdf->SetDefaultBodyCSS('background', 'url('.base_path('storage/doc_bg.png').')');
                $pdf->SetDefaultBodyCSS('background-image-resize', 6);
                $pdf->SetDefaultBodyCSS('background-repeat', 'no-repeat');
            }
            $pdf->curlAllowUnsafeSslRequests = true;
            $pdf->debug=true;

            $pdf->WriteHTML($content);
            return Response::make($pdf->Output('pdf',\Mpdf\Output\Destination::STRING_RETURN), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$form->name.'"'
            ]);

        }
        public function destroy($id, $auth = null)
        {
            $fixing = Fixing::where('company_id',$id)->get();

            if(sizeof($fixing)>0){
                return response()->json(['data' => ['message' => "A Cég nem törölhető, mert van rá rögzítve"]], $this->errorStatus);
            }
            $project = Project::where('company_id',$id)->get();
            if(sizeof($project)>0){
                return response()->json(['data' => ['message' => "A Cég nem törölhető, mert van hozzá project"]], $this->errorStatus);
            }
            $this->model = $this->model->where('id', '=', $id);

            try {
                if ($auth == null) {
                    $this->model = $this->model->firstOrFail();
                } else {
                    $this->model = $this->model->where('user_id', '=', $auth)->firstOrfail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $this->model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }
		public function getFixingByCompany(Request $request,$company){
		    $fixings = Fixing::where('company_id','=',$company)
                ->select(DB::raw("SUM(hour+bonus_hour) as sum_hour, month"))->groupBy('month')
                ->where('month','>',date('Ym',strtotime('-5 year')))->disableCache()->orderBy('month','desc')->get();
            $fixingsStudent = Fixing::where('company_id','=',$company)
                ->select(DB::raw('sum(hour+bonus_hour) as sum_hour,student_id,students.name as student_name'))
                ->leftJoin('students','students.id','=','student_id')->disableCache()->groupBy('student_id')->orderBy('students.name')->get();
		    $dateTable=[];
		    foreach ($fixings as $fixing){
		        $year = substr($fixing->month,0,4);
		        $month = substr($fixing->month,4);
		        if(!isset($dateTable[$year])){
		            foreach (ConstansHelper::$moths as $k =>$v)
		            $dateTable[$year][str_pad($k,2,'0',STR_PAD_LEFT)]=[
		                'month'=>$v,
                        'hours'=>''
                    ];
                }
		        $dateTable[$year][$month]['hours'] = $fixing->sum_hour.' óra' ;
            }
		    return response()->json(['fixing'=>$dateTable,'students'=>$fixingsStudent->toArray()]);
        }
		public function listTenders(Request $request){
		    $list = Fixing::leftJoin('company_tenders','company_tenders.company_id','=','fixings.company_id')->leftJoin('companies','companies.id','=','fixings.company_id')->groupBy('companies.id')->disableCache()
                ->select(DB::raw('companies.name as company_name,company_tenders.*, sum(IF(fixings.month>=company_tenders.start and fixings.month <=company_tenders.end ,student_price+company_price,0)) as frame_used'));
                if(!empty($request->input('search'))){
                    $search =$request->input('search');
                    $list= $list->where(function($query)use ($search){
                        $query->where('companies.name','LIKE','%'.$search.'%');
                    });
                }
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->where('frame_limit','>',0)->where('company_tenders.end','>=',date('Ym',strtotime('-2 month')))->paginate($pagination)->withPath($request->path());
            return CompanyTenderResource::collection($list);
        }
		public function listCompanyContact(Request $request,$export=null){
		    $list = Contact::leftJoin('companies','company_id','=','companies.id')->select(DB::raw('companies.name as company_name,company_contacts.*'))->disableCache();
            if(!empty($request->input('search'))){
                $search =$request->input('search');
                $list= $list->where(function($query)use ($search){
                    $query->where('companies.name','LIKE','%'.$search.'%')
                        ->orWhere('company_contacts.name','LIKE','%'.$search.'%')
                        ->orWhere('company_contacts.phone','LIKE','%'.$search.'%')
                        ->orWhere('company_contacts.email','LIKE','%'.$search.'%')
                        ->orWhere('company_contacts.position','LIKE','%'.$search.'%');
                });
            }
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));

            if($export){
                $headers = ['Cég neve','Kapcsolattartó','Beosztás','Email','Telefon'];
                $data =[];
                foreach ($list->get() as $contact){
                    $data[]=[
                        $contact->company_name,
                        $contact->name,
                        $contact->position,
                        $contact->email,
                        $contact->phone
                    ];
                }
                $export = new BaseArrayExport($headers, "Kapcsolattartók", $data);

                return $export->download(Str::slug('Kapcsolattartók') . '.xlsx');
            }
            $list = $list->paginate($pagination)->withPath($request->path());
            return CompanyContactResource::collection($list);
        }
        public function listByType(Request $request,$type='active')
        {
            $month= date('Ym',strtotime('-3 month'));

            $list = Industry::leftJoin('companies','companies.id','=','company_industries.company_id')
                ->leftJoin('users','users.id','=','company_industries.user_id')->disableCache()
                ->groupBy('companies.id')
                ->leftJoin(DB::raw('(select count(*) as cnt,company_id from project_students left join projects on projects.id= project_id where status_id=2 group by company_id) as pc '),'pc.company_id','=','companies.id');

            if($type=='active'){
                $month= date('Ym',strtotime('-3 month'));
                $list = $list->leftJoin(DB::raw("(select count(*) as fix_count,company_id from fixings where month >= $month  group by company_id)as fix "),'fix.company_id','=','companies.id')->where('fix_count','>=',0);
            }
            if($type == 'sleeping'){
                $month= date('Ym',strtotime('-3 month'));
                $month2= date('Ym',strtotime('-6 month'));

                $list = $list->leftJoin(DB::raw("(select sum(IF(month <= $month,1,0)) as fix_greater, sum(IF(month >= $month2,1,0)) as fix_less, company_id from fixings  group by company_id)as fix"),'fix.company_id','=','companies.id')->where(function($query){
                    return $query->whereNull('fix_less')
                        ->whereNull('fix_greater');
                });
            }
            if($type == 'inactive'){
                if($request->input('fixing') && $request->input('fixing')>0){
                    $month = date('Ym', strtotime('-'.$request->input('fixing').' month'));
                }else {
                    $month = date('Ym', strtotime('-3 month'));
                }
                $list = $list->leftJoin(DB::raw("(select count(*) as fix_count,company_id from fixings where month >= $month group by company_id)as fix"),'fix.company_id','=','companies.id')->whereNull('fix_count')
              ->leftJoin(DB::raw("(select count(*) as fix2_count,company_id from fixings where month <= $month group by company_id)as fix2 "),'fix2.company_id','=','companies.id')->where('fix2_count','>',0);

            }
            if($type == 'reject'){
               $list= $list->where('reject',1);
            }
            $list = $list->select(DB::raw('companies.id as id,reject, reject_message,companies.created_at as created_at,companies.name as company_name,company_industries.name as industry_name,concat(users.firstname,\' \',users.lastname) as user_name,cnt,tax,delegate,contract_date, contract_expire,paper_contract,agancy_contract_date,works_contarct_date'));

            if(!empty($request->input('search'))){
                $search =$request->input('search');
                $list= $list->where(function($query)use ($search){
                    $query->where('companies.name','LIKE','%'.$search.'%')
                        ->orWhere('company_industries.name','LIKE','%'.$search.'%')
                        ->orWhere('users.name','LIKE','%'.$search.'%')
                        ->orWhere('tax','LIKE','%'.$search.'%')
                        ->orWhere('delegate','LIKE','%'.$search.'%');
                });
            }
            if(strlen($request->input('contract',''))){
                if($request->input('contract','') == 1) {
                    $list =$list->whereNotNull('contract_date');
                }else{
                    $list =$list->whereNull('contract_date');
                }
            }
            if(strlen($request->input('has_worker',''))){
                if($request->input('has_worker','') == 1) {
                    $list =$list->whereNotNull('cnt');
                }else{
                    $list =$list->whereNull('cnt');
                }
            }

            if(strlen($request->input('paper',''))){
                $list->where('paper_contract','=',$request->input('paper',''));
            }
            if(strlen($request->input('user_id',''))){
                $list->where('companies.user_id','=',$request->input('user_id',''));
            }

            if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position!=2 && \Auth::user()->position !=3 && \Auth::user()->position !=4 ) {
                $list->where('companies.user_id','=',\Auth::id());
            }
            if(strlen($request->input('works',''))){
                if($request->input('works','') == 1) {
                    $list =$list->whereNotNull('works_contarct_date');
                }else{
                    $list =$list->whereNull('works_contarct_date');
                }
            }
            if(strlen($request->input('agancy',''))){
                if($request->input('agancy','') == 1) {
                    $list =$list->whereNotNull('agancy_contract_date');
                }else{
                    $list =$list->whereNull('agancy_contract_date');
                }
            }
            if($request->input('sort')){
                $sort = explode(',',$request->input('sort'));
                $list = $list->orderBy($sort[0],$sort[1]);
            }
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());

            return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
        }
        public function listCompany(Request  $request){
		    $list = Company::leftJoin('users','users.id','=','companies.user_id')->select(DB::raw("c.campaign_name,companies.created_at as created_at,companies.id as id,companies.name as company_name, '' as industry_name,concat(users.lastname,' ',users.firstname) as user_name,tax,delegate,cnt,contract_date, contract_expire,paper_contract,agancy_contract_date,works_contarct_date,online_presence"))
                ->leftJoin(DB::raw('(select group_concat(campaigns.name) as campaign_name, campaign_companies.company_id as c_id from campaign_companies left join campaigns on campaigns.id = campaign_companies.campaign_id group by campaign_companies.company_id) as c'),'c.c_id','=','companies.id' )
                ->disableCache()->leftJoin(DB::raw('(select count(*) as cnt,company_id from project_students left join projects on projects.id= project_id where status_id=2 group by company_id) as pc '),'pc.company_id','=','companies.id');
		    if(!empty($request->input('search'))){
		        $search =$request->input('search');
		        $list= $list->where(function($query)use ($search){
		              $query->where('companies.name','LIKE','%'.$search.'%')
                          ->orWhere('users.name','LIKE','%'.$search.'%')
                          ->orWhere('tax','LIKE','%'.$search.'%')
                          ->orWhere('delegate','LIKE','%'.$search.'%');
                });
            }
		    if(strlen($request->input('contract',''))){
		        if($request->input('contract','') == 1) {
                    $list =$list->whereNotNull('contract_date');
                }else{
                    $list =$list->whereNull('contract_date');
                }
            }
            if(strlen($request->input('has_worker',''))){
                if($request->input('has_worker','') == 1) {
                    $list =$list->whereNotNull('cnt');
                }else{
                    $list =$list->whereNull('cnt');
                }
            }
            if(strlen($request->input('paper',''))){
                $list->where('paper_contract','=',$request->input('paper',''));
            }
            if(strlen($request->input('user_id',''))){
                $list->where('companies.user_id','=',$request->input('user_id',''));
            }
            if (!\Auth::user()->isSuperAdmin() && !\Auth::user()) {
                $list->where('companies.user_id','=',\Auth::id());
            }
            if(strlen($request->input('works',''))){
                if($request->input('works','') == 1) {
                    $list =$list->whereNotNull('works_contarct_date');
                }else{
                    $list =$list->whereNull('works_contarct_date');
                }
            }

            if (!\Auth::user()->isSuperAdmin() && \Auth::user()->position!=2 && \Auth::user()->position !=3 ) {
                $list->where('companies.user_id','=',\Auth::id());
            }
            if(strlen($request->input('agancy',''))){
                if($request->input('agancy','') == 1) {
                    $list =$list->whereNotNull('agancy_contract_date');
                }else{
                    $list =$list->whereNull('agancy_contract_date');
                }
            }
            if($request->input('sort')){
                $sort = explode(',',$request->input('sort'));
                $list = $list->orderBy($sort[0],$sort[1]);
            }
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
            return $this->listResource::collection($list)->additional(['attributes' => User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),]);

        }
        public function companyRequests(Request $request,$id){
            return CampaignRequestListResource::collection(CampaignCompanyRequest::where('company_id',$id)->orderBy('id','desc')->get());
        }
        public function companyQuestions(Request $request,$id){
            return CampaigQuestionListResource::collection(CampaignCompanyQuestion::where('company_id',$id)->orderBy('id','desc')->get());
        }
        public function getWorker(Request $request,$id){

            $pStudent =ProjectStudent::leftJoin("projects",'projects.id','=','project_id')->where("projects.company_id",$id)->where("status_id",2)->disableCache()
                ->leftJoin("company_industries",'industry_id','company_industries.id')->leftJoin("students","student_id","students.id")
                ->select(DB::raw("students.name as student_name,company_industries.name as industry_name, student_id,tax_number as tax"))->orderBy("industry_name")->orderBy("student_name")->get();
            $company = Company::where("id",$id)->first();
            return response()->json(['data'=>['worker'=>$pStudent->toArray(),'industry'=>IndustryViewResource::collection($company->industry),'contact'=>ContactViewResource::collection($company->contacts)]],200);
        }
		public function listSite(Request $request,$id)
		{
			$model = new Site();
			$list = $model->searchInModel($request->input())->where('company_id','=',$id);
			$list = $list->get();

			return SiteListResource::collection($list);
		}

		public function showSite(Request $request, $id = 0)
		{
			$model = Site::where('id', '=', $id);
			try {
				$model = $model->firstOrFail();

			} catch (ModelNotFoundException $e) {
				$model = new Site();
			}

			return new SiteViewResource($model);
		}

		public function editSite(SiteEditRequest $request, $id = null)
		{
			$model = new Site();
			if ($id) {
				$model = Site::where('id', '=', $id);
			}
			try {
				if ($id) {
					$model = $model->firstOrFail();
				}
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->fillAndSave($request->all());

			return new SiteViewResource($model);
		}

		public function deleteSite(Request $request, $id)
		{
            $industry = Industry::where('site_id',$id)->get();

            if(sizeof($industry)>0){
                return response()->json(['data' => ['message' => "A telephely nem törölhető mert tartozik hozzá tevékenyésg"]], $this->errorStatus);
            }
			try {
				$model = Site::where('id', '=', $id)->firstOrfail();
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();

			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}
        public function saveIndustryOrder(Request $reguest){
            $industryList = $reguest->input('industryList');
            foreach ($industryList as $k=>$v){
                $ind = Industry::where('id',$v['id'])->first();
                $ind->order= $v['order'];
                $ind->save();
            }
            return response()->json('OK');
        }
        public function archiveIndustry(Request $reguest,$id){
                $ind = Industry::where('id',$id)->first();
                $ind->archive= 1;
                $ind->save();
            return response()->json('OK');
        }
        public function deleteArchiveIndustry(Request $reguest,$id){
            $ind = Industry::where('id',$id)->first();
            $ind->archive= 0;
            $ind->save();
            return response()->json('OK');
        }
        public function monthlyFixing(Request $request,$company,$year){
            $resp=[];
            for($i =1; $i<13;$i++){
                $resp[$year.str_pad($i,2,'0',STR_PAD_LEFT)]=[
                    'month'=>ConstansHelper::$moths[$i],
                    'student_count'=>0,
                    'sum_hour'=>0,
                    'background'=>date('Ym')>$year.str_pad($i,2,'0',STR_PAD_LEFT)?'bg-danger':'',
                ];
            }
            $data = Fixing::select(DB::raw('count(distinct student_id) as student_count,sum(hour+bonus_hour) as sum_hour,month '))->where('company_id','=',$company)->groupBy('month')->groupBy('company_id')->where('month','LIKE','%'.$year.'%')->get();
            foreach ($data as $d){
                $resp[$d->month]['student_count']=$d->student_count;
                $resp[$d->month]['sum_hour']=$d->sum_hour;
                $resp[$d->month]['background']='bg-success';

            }
            return response()->json(['data'=>$resp]);
        }
		public function listContact(Request $request,$id)
		{

			$model = new Contact();
			$list = $model->searchInModel($request->input())->where('company_id','=',$id);
			$list = $list->get();

			return ContactListResource::collection($list);
		}

		public function showContact(Request $request, $id = 0)
		{
			$model = Contact::where('id', '=', $id);
			try {
				$model = $model->firstOrFail();

			} catch (ModelNotFoundException $e) {
				$model = new Contact();
			}

			return new ContactViewResource($model);
		}

		public function editContact(ContactEditRequest $request, $id = null)
		{

			$model = new Contact();
			if ($id) {
				$model = Contact::where('id', '=', $id);
			}
			try {
				if ($id) {
					$model = $model->firstOrFail();
				}
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->fillAndSave($request->all());

			return new ContactViewResource($model);
		}

		public function deleteContact(Request $request, $id)
		{
			try {
				$model = Contact::where('id', '=', $id)->firstOrfail();
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();

			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}

		public function listIndustry(Request $request,$id)
		{
            $request->merge(['sort'=>'order|asc']);

			$model = new Industry();
			$list = $model->searchInModel($request->input())->where('company_industries.company_id','=',$id);
            $list = $list->leftJoin(DB::raw('(select * from projects where  projects.deleted_at is null) as projects'),'projects.industry_id','company_industries.id')
                ->leftJoin(DB::raw('(select project_id,count(*) as student_count from project_students where status_id=2 group by project_id) as project_students' ),'project_students.project_id','projects.id')
                ->groupBy('company_industries.id')
                ->select(DB::raw('company_industries.*,count( projects.id) as project_count, sum(project_students.student_count) as student_count, sum(projects.number_of_employees) as number_of_employees,projects.completion_date,projects.employees_change'));

			$list = $list->orderBy('company_industries.order','asc')->get();

			return IndustryViewResource::collection($list)->additional(['selectables'=> [
                "users" =>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id"),
				"site" => Site::where('company_id',$id)->pluck("name","id"),
                "contact"=> Contact::select(DB::raw("concat(name,' - ',email) as name, id"))->where("company_id",$id)->pluck("name","id"),
                "feor" => Feor::select(DB::raw("concat(code,'-',name) as name, id"))->get()->pluck("name","id"),
				"work_category"=> WorkCategory::leftJoin('work_categories as w2','w2.id','=','work_categories.parent_id')->disableCache()
					->select(DB::raw("work_categories.id as id, concat_ws('/',w2.name,work_categories.name) as name"))->where('work_categories.parent_id','>',0)->pluck("name","id"),
			]]);
		}

		public function showIndustry(Request $request, $id = 0)
		{
			$model = Industry::where('id', '=', $id);
			try {
				$model = $model->firstOrFail();

			} catch (ModelNotFoundException $e) {
				$model = new Industry();
			}

			return new IndustryViewResource($model);
		}

		public function editIndustry(IndustryEditRequest $request, $id = null)
		{
			$model = new Industry();
			if ($id) {
				$model = Industry::where('id', '=', $id);
			}
			try {
				if ($id) {
					$model = $model->firstOrFail();
				}
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
            $c =Contact::where("id",$request->contact_id)->first();
            $model->instructor = $c->name;
			$model->fillAndSave($request->all());

			return new IndustryViewResource($model);
		}

		public function deleteIndustry(Request $request, $id)
		{
            $fixings = Fixing::where("industry_id",$id)->get();
            if(sizeof($fixings)>0){
                return response()->json(['data' => ['message' => "A tevékenyésg nem törölhető, mert van rá rögzítve"]], $this->errorStatus);
            }
            $industry = Project::where("industry_id",$id)->get();
            if(sizeof($industry)>0){
                return response()->json(['data' => ['message' => "A tevékenyésg nem törölhető, mert van project hozzárendelve"]], $this->errorStatus);
            }
			try {
				$model = Industry::where('id', '=', $id)->firstOrfail();
			} catch (ModelNotFoundException $e) {
				return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
			}
			$model->delete();

			return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
		}

        public function listDocument(Request $request,$id)
        {

            $model = new Document();
            $list = $model->searchInModel($request->input())->where('company_id','=',$id);
            $list = $list->get();

            return DocumentListResource::collection($list);
        }

        public function showDocument(Request $request, $id = 0)
        {
            $model = Document::where('id', '=', $id);
            try {
                $model = $model->firstOrFail();

            } catch (ModelNotFoundException $e) {
                $model = new Document();
            }

            return new DocumentViewResource($model);
        }

        public function editDocument(DocumentEditRequest $request, $id = null)
        {

            $model = new Document();
            if ($id) {
                $model = Document::where('id', '=', $id);
            }
            try {
                if ($id) {
                    $model = $model->firstOrFail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->fillAndSave($request->all());

            return new DocumentViewResource($model);
        }

        public function deleteDocument(Request $request, $id)
        {
            try {
                $model = Document::where('id', '=', $id)->firstOrfail();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }

        public function listTender(Request $request,$id)
        {

            $model = new Tender();
            $list = $model->searchInModel($request->input())->where('company_id','=',$id);
            $list = $list->get();

            return TenderListResource::collection($list);
        }
        public function listUsers(Request $request,$id)
        {
            $model = new User();
            $list = $model->searchInModel($request->input())->where('company_id','=',$id);
            $list = $list->get();
            return UserListAccessResource::collection($list)->additional(['contacts'=>Contact::where('company_id','=',$id)->get()->pluck('name','id'),'industries'=>Industry::where('company_id',$id)->get()->pluck('name','id')]);
        }
        public function deleteUser(Request $request,$id){
            try {
                $model = User::where('id', '=', $id)->firstOrfail();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }
        public function showTender(Request $request, $id = 0)
        {
            $model = Tender::where('id', '=', $id);
            try {
                $model = $model->firstOrFail();

            } catch (ModelNotFoundException $e) {
                $model = new Tender();
            }

            return new TenderViewResource($model);
        }

        public function editTender(TenderEditRequest $request, $id = null)
        {

            $model = new Tender();
            if ($id) {
                $model = Tender::where('id', '=', $id);
            }
            try {
                if ($id) {
                    $model = $model->firstOrFail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->fillAndSave($request->all());

            return new TenderViewResource($model);
        }

        public function deleteTender(Request $request, $id)
        {
            try {
                $model = Tender::where('id', '=', $id)->firstOrfail();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }
        public function createUserFromContact(Request $request,$contact){
		        $contact = Contact::where('id','=',$contact)->first();
		        $user = User::where('email',$contact->email)->first();
		        if($user){
		            return response()->json(['data' => ['message' => 'Az email cím már foglalt!']],$this->errorStatus);
                }
		        $user = new User();
		        $user->name = $contact->name;
		        $name =explode(' ',$contact->name);
		        $user->firstname = $name[0];
                $user->lastname = isset($name[1])?$name[1]:'';
		        $user->name = $contact->name;
		        $user->is_company = 1;
		        $user->company_id = $contact->company->id;
		        $user->industries = explode(',',$request->input('industries'));
		        $user->email = $contact->email;
		        $user->email_verified_at = date('Y-m-d H:i:s');
		        $user->save();
                $user->roles()->sync([11]);
                $pass = Str::random(12);
                $user->password = Hash::make($pass);
                $user->save();

                Mail::to($user)->locale(App::getLocale())->send(new GeneratePassword($user, $pass));


        }
        public function editUser(Request $request,$id){

		    try {
                $user = User::where('id', $id)->firstOrFail();

                $user->is_company = 1;
                $user->industries = explode(',',$request->input('industries'));
                $user->save();
                return response($this->successMessage,$this->successStatus);

            }catch (ModelNotFoundException $e){
		        abort(404);
            }


        }
        public function generatePassword(Request $request,$id){
            try {
                $user = User::where('id', $id)->firstOrFail();
                $pass = Str::random(12);
                $user->password = Hash::make($pass);
                $user->save();

                Mail::to($user)->locale(App::getLocale())->send(new GeneratePassword($user, $pass));
                return response($this->successMessage,$this->successStatus);
            }catch (ModelNotFoundException $e){
                abort(404);
            }

        }
        public function listForm(Request $request,$id)
        {
            $model = new Forms();
            $list = $model->searchInModel($request->input())->where('company_id','=',$id);
            $list = $list->get();

            return FormListResource::collection($list);
        }

        public function showForm(Request $request, $id = 0)
        {
            $model = Forms::where('id', '=', $id);
            try {
                $model = $model->firstOrFail();

            } catch (ModelNotFoundException $e) {
                $model = new Site();
            }

            return new FormViewResource($model);
        }

        public function editForm(FormEditRequest $request, $id = null)
        {
            $model = new Forms();
            if ($id) {
                $model = Forms::where('id', '=', $id);
            }
            try {
                if ($id) {
                    $model = $model->firstOrFail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->fillAndSave($request->all());

            return new FormViewResource($model);
        }

        public function deleteForm(Request $request, $id)
        {

            try {
                $model = Forms::where('id', '=', $id)->firstOrfail();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }

        public function listBonus(Request $request,$id)
        {
            $model = new Bonus();
            $list = $model->searchInModel($request->input())->where('company_id','=',$id);
            $list = $list->get();

            return BonusListResource::collection($list);
        }
        public function listComments(Request $request,$id)
        {
            $model = new Comments();
            $list = $model->searchInModel($request->input())->where('company_comments.company_id','=',$id);
            $list = $list->leftJoin('users','users.id','=','user_id')->select(DB::raw("company_comments.*, concat(users.lastname,' ',users.firstname) as user"))->orderBy('created_at','desc')->get();

            return CommentsListResource::collection($list);
        }
        public function editComments(Request $request, $id = null)
        {
            $model = new Comments();
            if ($id) {
                $model = Comments::where('id', '=', $id);
            }
            try {
                if ($id) {
                    $model = $model->firstOrFail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->fill($request->all());
            $model->user_id= \Auth::id();
            $model->save();
            return new CommentsListResource($model);
        }
        public function listWorker(Request $request,$id)
        {
            $company = Company::where('id',$id)->first();

            $date= date('Ym',strtotime('-1 month'));
            if(date('d')<10){
                $date= date('Ym',strtotime('-2 month'));
            }


            $list = DB::table("fixing_view")->groupBy('fixing_view.student_id')
                ->where('fixing_view.company_id','=',$id)->leftJoin('students','students.id', 'fixing_view.student_id')->whereNotNull('students.id')
                ->leftJoin(DB::raw('(select project_students.student_id,max(start_date) as start_date, company_id from project_students left join projects on projects.id = project_id group by company_id,student_id) as ps'), function($join)
                {
                    $join->on('ps.company_id', '=', 'fixing_view.company_id');
                    $join->on('ps.student_id','=','fixing_view.student_id');
                })
                ->leftJoin(DB::raw('(select student_id, max(end) as end from student_certificates  where deleted_at is null group by student_id ) as cert'), 'cert.student_id', '=', 'fixing_view.student_id');
             //   ->select(DB::raw("students.name as name,birth_date,ps.start_date, students.student_number as student_number,students.newsletter,students.tax_number, students.phone as phone,students.email as email,GROUP_CONCAT(DISTINCT teaor SEPARATOR ', ') as teaor,industry_name , ceiling(sum(hour+bonus_hour)) as hour, cert.end as certificate"));

                $list = $list->leftJoin(DB::raw("(select student_id, max(expire) as expire from student_work_safety where type=1 group by student_id) as works_med"),'works_med.student_id','=','students.id')
                    ->leftJoin(DB::raw("(select student_id, max(expire) as expire_medical from student_work_safety where type=3 group by student_id) as works_eu"),'works_eu.student_id','=','students.id')
                        ->select(DB::raw("students.name as name,medical_certificate_type,ps.start_date,birth_date, students.student_number as student_number,students.newsletter,students.tax_number, students.phone as phone,students.email as email,GROUP_CONCAT(DISTINCT teaor SEPARATOR ', ') as teaor,industry_name , ceiling(sum(hour+bonus_hour)) as hour, cert.end as certificate,works_med.expire,works_eu.expire_medical"));

            if($request->input('all') == "false") {
                if ($request->input('month') && $request->input('month') != null) {
                    $date = date('Ym', strtotime($request->input('month') . '-01'));
                    $list = $list->where("month", '=', $date);
                }
            }
            $list =$list->get();
            if($request->has('excel')){
                $headers=[
                  'Mads azonosító',
                  'Név',
                  'Születési dátum',
                  'Adószám',
                  'Email',
                  'Telefon',
                  'Jogviszony',
                  'Eü',
                  'Orvosi',
                  'Teljesítés kezdete',
                  'Munkakör',
                  'Azonosító',
                  'Óraszám',
                ];
                $data =[];
                foreach ($list as $s){
                    $data[]=[
                      $s->student_number,
                      $s->name,
                      $s->birth_date,
                      $s->tax_number,
                      $s->email,
                      $s->phone,
                      $s->certificate,
                      $s->expire,
                      $s->expire_medical,
                      $s->start_date,
                      $s->teaor,
                      $s->industry_name,
                      $s->hour,


                    ];
                }
              return Excel::download(new BaseArrayExport($headers,$company->name.'-dolgozok-'.date('Y-m-d'),$data),str_slug($company->name).'-dolgozok-'.date('Y-m-d').'.xlsx');
            }
            return response()->json(["data"=>$list]);
        }

        public function showBonus(Request $request, $id = 0)
        {
            $model = Bonus::where('id', '=', $id);
            try {
                $model = $model->firstOrFail();

            } catch (ModelNotFoundException $e) {
                $model = new Bonus();
            }

            return new BonusViewResource($model);
        }
        public function getBonusByCompanyAndIndustry(Request $request,$company,$industry){
            $bonus = Bonus::where('company_id',$company)->where("active","=",1)->get();
            $algorithms=[];
            if($bonus){
                foreach ($bonus as $b){

                    if(isset($b->industries['#'.$industry]) && $b->industries['#'.$industry] == "true"){

                        foreach ($b->bonus_algorithm as $alg){
                            $algorithms[$alg['hour']]= $alg;
                            $algorithms[$alg['hour']]['text']= "<b class='text-danger'>&nbsp".$alg['hour']." óra </b>&nbsp
 felett. Az alapbér  ".(($alg['type']=="1")?"<b class='text-danger'>&nbsp + ".$alg['value'].' Ft </b>&nbsp óránként ': "<b class='text-danger'> &nbsp". $alg['value'].' %-a </b>&nbsp óránként');
                        }
                    }
                }

            }
            sort($algorithms);
           $responseAlg = array_reverse($algorithms);
           return response()->json($responseAlg);

        }
        public function editBonus(BonusEditRequest $request, $id = null)
        {
            $model = new Bonus();
            if ($id) {
                $model = Bonus::where('id', '=', $id);
            }
            try {
                if ($id) {
                    $model = $model->firstOrFail();
                }
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->fillAndSave($request->all());

            return new BonusViewResource($model);
        }

        public function deleteBonus(Request $request, $id)
        {
            try {
                $model = Bonus::where('id', '=', $id)->firstOrfail();
            } catch (ModelNotFoundException $e) {
                return response()->json(['data' => ['message' => $this->errorMessage]], $this->errorStatus);
            }
            $model->delete();

            return response()->json(['data' => ['message' => $this->successMessage]], $this->successStatus);
        }
	}
