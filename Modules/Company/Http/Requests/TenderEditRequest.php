<?php

namespace Modules\Company\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TenderEditRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
			'frame_limit' => 'required',
			'start' => 'required',
			'end' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'frame_limit' => __('Keretösszeg'),
				'start' => __('Mettől'),
				'end' => __('Meddig'),


            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
