<?php

namespace Modules\Company\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IndustryEditRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'company_id' => 'required',
			'site_id' => 'required',
			'user_id' => 'required',
			'contact_id' => 'required',
            'work_category_id'=> 'required',
            'price_hour' => 'required',
            'price_company_hourly' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Tevékenység neve'),
                'company_id' => __('Cég'),
                'contact_id' => __('Utasítást adó személy'),
                'site_id' => __('Telephely'),
                'user_id' => __('Felelős'),
                'work_category_id' => __('Kategória'),
                'price_hour' => __('Órabér'),
                'price_company_hourly' => __('Óránkénti bér'),
                'price_company_fix' => __('Fix bér (%)'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
