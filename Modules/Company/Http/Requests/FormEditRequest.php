<?php

namespace Modules\Company\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FormEditRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_id' => 'required',
			'name' => 'required',
			'data' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Sablon név'),
				'company_id' => __('Cég'),
				'data' => __('tartalom'),


            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
