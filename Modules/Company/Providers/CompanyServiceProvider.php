<?php

namespace Modules\Company\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Company\Entities\Company;
use Modules\Company\Observers\CompanyObserver;

class CompanyServiceProvider extends ModuleServiceProvider
{
    protected $module = 'company';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Company::observe(CompanyObserver::class);
    }
}
