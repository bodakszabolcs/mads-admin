<?php

namespace Modules\Questions\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuestionsCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'question' => __('Kérdés'),
'o' => __('Sorrend'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
