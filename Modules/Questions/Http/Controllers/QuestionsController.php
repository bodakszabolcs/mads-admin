<?php

namespace Modules\Questions\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Questions\Entities\Questions;
use Illuminate\Http\Request;
use Modules\Questions\Http\Requests\QuestionsCreateRequest;
use Modules\Questions\Http\Requests\QuestionsUpdateRequest;
use Modules\Questions\Transformers\QuestionsViewResource;
use Modules\Questions\Transformers\QuestionsListResource;

class QuestionsController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Questions();
        $this->viewResource = QuestionsViewResource::class;
        $this->listResource = QuestionsListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = QuestionsCreateRequest::class;
        $this->updateRequest = QuestionsUpdateRequest::class;
    }


    public function saveOrder(Request $request){
        $index = 0;
        foreach ($request->input('list',[]) as $ps){
            Questions::where('id',$ps['id'])->update(['o'=>$index]);
            $index++;
        }
        return response()->json($this->successMessage,$this->successStatus);
    }
}
