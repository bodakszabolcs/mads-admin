import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Questions from '../components/Questions'
import QuestionsList from '../components/QuestionsList'
import QuestionsCreate from '../components/QuestionsCreate'
import QuestionsEdit from '../components/QuestionsEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'question',
                component: Questions,
                meta: {
                    title: 'Questions'
                },
                children: [
                    {
                        path: 'index',
                        name: 'QuestionsList',
                        component: QuestionsList,
                        meta: {
                            title: 'Questions',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/question/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/question/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'QuestionsCreate',
                        component: QuestionsCreate,
                        meta: {
                            title: 'Create Questions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/question/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'QuestionsEdit',
                        component: QuestionsEdit,
                        meta: {
                            title: 'Edit Questions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/question/index'
                        }
                    }
                ]
            }
        ]
    }
]
