<?php

namespace Modules\Questions\Entities;

use Modules\Questions\Entities\Base\BaseQuestions;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


class Questions extends BaseQuestions
{
    use SoftDeletes, Cachable;


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {


        return parent::getFilters();
    }
}
