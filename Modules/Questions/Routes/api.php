<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\Questions\Http\Controllers',
    'prefix' => '/questions',
    'middleware' => ['auth:sanctum', 'role']
], function () {

    Route::post( '/save-order', 'QuestionsController@saveOrder')->name('Save question order');
});
