<?php

namespace Modules\Questions\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Questions\Entities\Questions;
use Modules\Questions\Observers\QuestionsObserver;

class QuestionsServiceProvider extends ModuleServiceProvider
{
    protected $module = 'questions';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Questions::observe(QuestionsObserver::class);
    }
}
