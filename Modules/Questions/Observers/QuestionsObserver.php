<?php

namespace Modules\Questions\Observers;

use Modules\Questions\Entities\Questions;

class QuestionsObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function saved(Questions $model)
    {
        Questions::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function created(Questions $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function updated(Questions $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function deleted(Questions $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function restored(Questions $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Questions\Entities\Questions  $model
     * @return void
     */
    public function forceDeleted(Questions $model)
    {
        //
    }
}
