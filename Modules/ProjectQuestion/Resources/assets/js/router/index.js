import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import ProjectQuestion from '../components/ProjectQuestion'
import ProjectQuestionList from '../components/ProjectQuestionList'
import ProjectQuestionCreate from '../components/ProjectQuestionCreate'
import ProjectQuestionEdit from '../components/ProjectQuestionEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'project-question',
                component: ProjectQuestion,
                meta: {
                    title: 'ProjectQuestions'
                },
                children: [
                    {
                        path: 'index',
                        name: 'ProjectQuestionList',
                        component: ProjectQuestionList,
                        meta: {
                            title: 'ProjectQuestions',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/project-question/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-question/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'ProjectQuestionCreate',
                        component: ProjectQuestionCreate,
                        meta: {
                            title: 'Create ProjectQuestions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-question/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'ProjectQuestionEdit',
                        component: ProjectQuestionEdit,
                        meta: {
                            title: 'Edit ProjectQuestions',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/project-question/index'
                        }
                    }
                ]
            }
        ]
    }
]
