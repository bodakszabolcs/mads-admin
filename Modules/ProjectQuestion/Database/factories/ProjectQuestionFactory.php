<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\ProjectQuestion\Entities\ProjectQuestion;

$factory->define(ProjectQuestion::class, function (Faker $faker) {
    return [
        "question" => $faker->realText(),
"answer_type" => $faker->realText(),
"answer" => $faker->realText(),

    ];
});
