<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('project_question');
        Schema::create('project_question', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("question")->nullable();
$table->text("answer_type")->nullable();
$table->text("answer")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_question');
    }
}
