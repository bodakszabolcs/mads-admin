<?php

namespace Modules\ProjectQuestion\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\ProjectQuestion\Entities\ProjectQuestion;
use Modules\ProjectQuestion\Observers\ProjectQuestionObserver;

class ProjectQuestionServiceProvider extends ModuleServiceProvider
{
    protected $module = 'projectquestion';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        ProjectQuestion::observe(ProjectQuestionObserver::class);
    }
}
