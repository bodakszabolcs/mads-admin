<?php

namespace Modules\ProjectQuestion\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProjectQuestionListResource extends BaseResource
{
    private $arr =[
        '1'=>'Intervallum',
        '2'=>'Felsorolás szöveg',
        '3'=>'Szám',
        '4'=>'Szöveg'
    ];
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "question" => $this->question,
		    "answer_type" => $this->arr[$this->answer_type],
		    "answer" => $this->answer,
		     ];
    }
}
