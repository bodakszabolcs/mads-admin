<?php

namespace Modules\ProjectQuestion\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class ProjectQuestionViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "question" => $this->question,
		    "answer_type" => $this->answer_type,
		    "answer" => $this->answer,
		"selectables" => [
		]
		     ];
    }
}
