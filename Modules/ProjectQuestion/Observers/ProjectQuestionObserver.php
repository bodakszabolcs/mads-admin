<?php

namespace Modules\ProjectQuestion\Observers;

use Modules\ProjectQuestion\Entities\ProjectQuestion;

class ProjectQuestionObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function saved(ProjectQuestion $model)
    {
        ProjectQuestion::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function created(ProjectQuestion $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function updated(ProjectQuestion $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function deleted(ProjectQuestion $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function restored(ProjectQuestion $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\ProjectQuestion\Entities\ProjectQuestion  $model
     * @return void
     */
    public function forceDeleted(ProjectQuestion $model)
    {
        //
    }
}
