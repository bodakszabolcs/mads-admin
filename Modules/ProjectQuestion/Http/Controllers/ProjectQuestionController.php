<?php

namespace Modules\ProjectQuestion\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\ProjectQuestion\Entities\ProjectQuestion;
use Illuminate\Http\Request;
use Modules\ProjectQuestion\Http\Requests\ProjectQuestionCreateRequest;
use Modules\ProjectQuestion\Http\Requests\ProjectQuestionUpdateRequest;
use Modules\ProjectQuestion\Transformers\ProjectQuestionViewResource;
use Modules\ProjectQuestion\Transformers\ProjectQuestionListResource;

class ProjectQuestionController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new ProjectQuestion();
        $this->viewResource = ProjectQuestionViewResource::class;
        $this->listResource = ProjectQuestionListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = ProjectQuestionCreateRequest::class;
        $this->updateRequest = ProjectQuestionUpdateRequest::class;
    }

}
