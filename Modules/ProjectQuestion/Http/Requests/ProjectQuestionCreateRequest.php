<?php

namespace Modules\ProjectQuestion\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectQuestionCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question' => 'required',
			'answer_type' => 'required',
			//'answer' => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'question' => __('Kérdés'),
'answer_type' => __('Válasz típusa'),
'answer' => __('Válasz'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
