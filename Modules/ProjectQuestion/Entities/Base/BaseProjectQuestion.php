<?php

namespace Modules\ProjectQuestion\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseProjectQuestion extends BaseModel
{
    

    protected $table = 'project_question';

    protected $dates = ['created_at', 'updated_at'];

    

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'question',
                    'title' => 'Kérdés',
                    'type' => 'text',
                    ],[
                    'name' => 'answer',
                    'title' => 'Válasz',
                    'type' => 'text',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['question','answer_type','answer'];

    protected $casts = [];

    
}
