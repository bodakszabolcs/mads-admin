<?php

namespace Modules\User\Http\Controllers;

use App\AccessLog;
use App\Exports\BaseArrayExport;
use App\Http\Controllers\AbstractLiquidController;
use App\MailLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

use Modules\Company\Entities\Industry;
use Modules\Company\Transformers\IndustryStatisticResource;
use Modules\Company\Transformers\IndustryViewResource;
use Modules\Student\Entities\Student;
use Modules\User\Entities\UserCalendar;
use Modules\User\Entities\UserStatistic;
use Modules\User\Http\Requests\UserCalendarRequest;
use Modules\User\Emails\GeneratePassword;
use Modules\User\Entities\User;
use Modules\User\Entities\UserFilter;
use Modules\User\Http\Requests\ChangePasswordRequest;
use Modules\User\Http\Requests\UserCreateRequest;
use Modules\User\Http\Requests\UserUpdateRequest;
use Modules\User\Transformers\AccessLogResource;
use Modules\User\Transformers\MailLogResource;
use Modules\User\Transformers\UserListResource;
use Modules\User\Transformers\UserStatisticResource;
use Modules\User\Transformers\UserViewResource;

class UserController extends AbstractLiquidController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new User();
        $this->viewResource = UserViewResource::class;
        $this->listResource = UserListResource::class;
        $this->useResourceAsCollection = true;
        $this->pagination = true;
        $this->createRequest = UserCreateRequest::class;
        $this->updateRequest = UserUpdateRequest::class;
    }

    public function getUser(Request $request)
    {
        return response()->json(new UserViewResource(Auth::user()), $this->successStatus);
    }
    public function staffingStatistic(Request $request)
    {
        $list = Industry::leftJoin('companies', 'companies.id', 'company_industries.company_id')
            ->leftJoin(DB::raw('(select * from projects where status = 0 and projects.deleted_at is null) as projects'), 'projects.industry_id', 'company_industries.id')
            ->leftJoin(DB::raw('(select project_id,count(*) as student_count from project_students where status_id= 2  group by project_id) as project_students'), 'project_students.project_id', 'projects.id')
            ->groupBy('company_industries.company_id')
            ->select(DB::raw('companies.name as name,companies.id as company_id, count( projects.id) as project_count, sum(project_students.student_count) as student_count, sum(projects.number_of_employees) as number_of_employees,if(sum(number_of_employees) and sum(student_count),sum(student_count)/sum(number_of_employees),0)*100 as percent'));
           if($request->has('user_id')){
               $list= $list->where('companies.user_id','=',$request->input('user_id'));
           }
           $sort= explode(',',$request->input('sort'));
           if($request->has('sort')){

                   $list = $list->orderBy(DB::raw($sort[0]), $sort[1]);

           }else{
               $list = $list->orderBy(DB::raw('sum(projects.number_of_employees)-sum(project_students.student_count)'), 'asc');
           }
        $list =$list->get();
        $users = User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name", "id");
        return IndustryStatisticResource::collection($list)->additional(['users' => $users]);
    }
    public function userStatistic(Request $request){
        $list = UserStatistic::whereNotNull("date")->leftJoin("users","users.id","=","user_id")->select(DB::raw("concat(lastname,' ',firstname) as name, user_statistics.id, 
         sum(if(isnull(new_project),0,new_project)) as new_project
        ,sum(if(isnull(new_order),0,new_order)) as new_order
        ,sum(if(isnull(new_adversting),0,new_adversting)) as new_adversting
        ,sum(if(isnull(refreshed_adversting),0,refreshed_adversting)) as refreshed_adversting
        ,sum(if(isnull(new_apply),0,new_apply)) as new_apply
        ,sum(if(isnull(new_apply_email),0,new_apply_email)) as new_apply_email
        ,sum(if(isnull(cv_request),0,cv_request)) as cv_request
        ,sum(if(isnull(cv_send),0,cv_send)) as cv_send
        ,sum(if(isnull(registration_send),0,registration_send)) as registration_send
        ,sum(if(isnull(start_work),0,start_work)) as start_work
        ,sum(if(isnull(start_work_old),0,start_work_old)) as start_work_old
        ,sum(if(isnull(all_apply),0,all_apply)) as all_apply
        ,sum(if(isnull(visited_students),0,visited_students)) as visited_students
        ,sum(if(isnull(contract),0,contract)) as contract,date
        "));
        if($request->input("date",date("Y-m-01")." - ".date("Y-m-d",strtotime("+1 month")))){

            $date = explode(" - ",$request->input("date",date("Y-m-01")." - ".date("Y-m-d",strtotime("+1 month"))));
            $list = $list->where("date",">=",$date[0])->where("date","<=",$date[1]);
        }
        if($request->has("user_id") && $request->input("user_id")>0){
            $list =$list->where("user_id",$request->input("user_id"))->groupBy("date");
        }else{
            $list = $list->groupBy("user_id");
        }
        $users =User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id");
        return UserStatisticResource::collection($list->get())->additional(['users'=>$users]);
    }
    public function index(Request $request, $auth = null)
    {
        $role_id= $request->input('role_id');
        $request->merge(['role_id'=>null]);
        $list = $this->model->searchInModel($request->input());

        if($role_id) {
            $list->whereHas('roles', function ($query) use ($role_id) {
                $query->where('role_id', '=', $role_id);
            });
            }
        $list =  $list->whereNull("company_id");
        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }
        if($request->has('name')){
            $list = $list->where(DB::raw("contact(firstname,' ',lastname)"),'LIKE','%'.$request->input('name').'%');
        }

        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }
        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function company(Request $request, $auth = null)
    {
        $role_id= $request->input('role_id');
        $request->merge(['role_id'=>null]);
        $list = $this->model->searchInModel($request->input());


        if($role_id) {
            $list->whereHas('roles', function ($query) use ($role_id) {
                $query->where('role_id', '=', $role_id);
            });
        }
       $list = $list->where("company_id",">",0);
        if (!is_null($auth)) {
            $list = $list->where('user_id', '=', $auth);
        }
        if ($this->pagination) {
            $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
            $list = $list->paginate($pagination)->withPath($request->path());
        } else {
            $list = $list->get();
        }
        if (!$this->useResourceAsCollection) {
            return new $this->collection($list);
        }

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function destroy($id, $auth = null)
    {
        if ($id == 1) {
            abort(422);
        }
        return parent::destroy($id, $auth);
    }
    public function listProjectmanangerStatistic(Request $request,$month){
        if(!$month){
            $month=date('Y-m');
        }
        $month = str_replace("-",'',$month);
        $users = DB::table(DB::raw("(select companies.user_id as pm_id, count(distinct company_id) as count_company,count(distinct student_id) count_student,
           sum(student_price+company_price) as sum_price,sum(hour+bonus_hour) as sum_hour
          from fixings left join companies on companies.id = fixings.company_id where month='".$month."' group by companies.user_id) as fix_company"))->leftJoin('users','users.id','=','fix_company.pm_id')
            ->leftJoin(DB::raw("(select user_id, count(*) as count_project from projects where projects.deleted_at is null and projects.status=0 and projects.deleted_at is null and created_at <= '".date('Y-m-t H:i:s',strtotime($month.'01'))."' group by user_id) as pr_users"),'pr_users.user_id','=','users.id')
            ->leftJoin(DB::raw("(select count(*) as count_apply, projects.user_id as user_id from project_apply left join works on works.id = work_id left join projects on projects.id = works.project_id where project_apply.created_at LIKE '%".date('Y-m',strtotime($month.'01'))."%' group by projects.user_id )as apply"),'apply.user_id','=','users.id')
            ->select(DB::raw("users.firstname,users.lastname,fix_company.sum_price,cmp.all_company,cmp.new_company,fix_company.count_company,fix_company.sum_hour,fix_company.count_student,pr_users.count_project,apply.count_apply "))
            ->leftJoin(DB::raw("(select user_id, count(*) as all_company,sum(if(companies.created_at LIKE '%".date('Y-m',strtotime($month.'01'))."%',1,0)) as new_company from companies where companies.deleted_at is null and created_at <= '".date('Y-m-t H:i:s',strtotime($month.'01'))."' group by companies.user_id) as cmp"),"cmp.user_id",'=','users.id')

           // ->where("all_company",">",0)
            ->orderBy("sum_price",'desc')
             ->get();
        return response()->json(["data"=>$users->toArray()]);
    }
    public function saveFilter(Request $request)
    {
        $uf = new UserFilter();
        $uf->user_id = Auth::id();
        $uf->path = $request->input('path');
        $uf->name = $request->input('name');
        $uf->params = $request->input('params');
        $uf->query = $request->input('query');
        $uf->save();

        return response()->json(UserFilter::where('user_id', '=', Auth::id())->get(), $this->successStatus);
    }
    public function changePassword(ChangePasswordRequest $request) {
        $user = User::find(Auth::id());

        $user->password = Hash::make($request->input('password'));
        $user->save();

        return parent::show($request, Auth::id());
    }
    public function deleteFilter(Request $request, $id)
    {
        try {
            $uf = UserFilter::where('id', '=', $id)->where('user_id', '=', Auth::id())->firstOrFail();
            $uf->delete();

            return response()->json(UserFilter::where('user_id', '=', Auth::id())->get(), $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }

    public function forceLogin(Request $request, $id)
    {
        try {
            $user = User::where('id', '=', $id)->firstOrFail();
            $user->api_token = $user->createToken('force-login-'.$user->email.'-'.date("YmdHi"))->plainTextToken;
            $user->save();
            Auth::guard("web")->login($user);
            return response()->json($user, $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }

    public function update(Request $request, $id, $auth = null)
    {
        if ($request->has('new_password') && $request->input('new_password') != null) {
            $validator = Validator::make($request->all(), [
                'new_password' => 'required|string|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            ]);

            if ($validator->fails()) {
                return response()->json($validator->errors(), $this->errorStatus);
            }
        }

        return parent::update($request, $id, $auth);
    }
    public function deleteProfile(Request $request)
    {
        try {
            $user = User::where('id', '=', Auth::user()->id)->where('id','<>',1)->firstOrFail();
            $user->delete();
            return response()->json($this->successMessage, $this->successStatus);
        } catch (ModelNotFoundException $e) {
            return response()->json(['errors'=>[__('The user cannot be deleted')]], $this->errorStatus);
        }
    }
    public function getProfile(Request $request)
    {
        return parent::show($request, Auth::id());
    }

    public function updateProfile(UserUpdateRequest $request)
    {
        return $this->update($request, Auth::id());
    }

    public function accessLogs(Request $request, $id = 0)
    {
        $access = AccessLog::where('id', '>', 0);

        if ($id != 0) {
            $access = $access->where('user_id', '=', $id);
        }

        $access = $access->orderBy('id', 'DESC')->paginate();

        return AccessLogResource::collection($access);
    }

    public function mailLogs(Request $request, $id = 0)
    {
        $mail = MailLog::where('id', '>', 0);

        if ($id != 0) {
            $user = User::find($id);

            $mail = $mail->where('to', '=', optional($user)->email);
        }

        $mail = $mail->orderBy('id', 'DESC')->paginate();

        return MailLogResource::collection($mail);
    }

    public function accessLogsProfile(Request $request)
    {
        return $this->accessLogs($request, Auth::id());
    }

    public function mailLogsProfile(Request $request)
    {
        return $this->mailLogs($request, Auth::id());
    }

    public function generatePassword(Request $request, $id)
    {
        try {
            $user = User::where('id','=',$id)->firstOrFail();

            $pass = Str::random(12);
            $user->password = Hash::make($pass);
            $user->save();

            Mail::to($user)->locale(App::getLocale())->send(new GeneratePassword($user, $pass));

            return response()->json(['status' => 'OK'], $this->successStatus);
        } catch (ModelNotFoundException $e) {
            abort(422);
        }
    }
    public function exportCalendar(Request $request,$year,$month){

        $date = $year."-".str_pad($month+1,2,'0',STR_PAD_LEFT).'-01';
        $start =  date('Y-m-01',strtotime($date));
        $end = date('Y-m-t',strtotime($date));
        $tableArray=[];
        $schedulesCount = UserCalendar::where('date','>=',$start)->where("date",'<=',$end)->where('user_id',Auth::user()->id)
            ->orderBy('date')->orderBy("start")->get();
        foreach ($schedulesCount as $c){
            $tableArray[]= [
                'data'=>$c->date,
                'info'=>$c->info,
                'start'=>$c->start,
                'end'=>$c->end
            ];
        }
        $export = new BaseArrayExport(['Dátum','Infó','Kezdete','Vége'], "Naptár", $tableArray);

        return $export->download(Str::slug('Naptár') . '.xlsx');


    }

    public function editEvent(UserCalendarRequest  $request){
        $userCalendar = new UserCalendar();
        if($request->has("id") && $request->input("id")>0 ){
            $userCalendar = UserCalendar::where("id",$request->input("id"))->first();
        }

        $userCalendar->fill($request->all());
        $userCalendar->user_id = Auth::user()->id;
        $sender = User::where('id',Auth::user()->id)->first();
        if($userCalendar->participants){
            foreach ($userCalendar->participants as $u){
                $user = User::where('id',$u)->first();

                $from = [];
                $from['name']=$sender->firstname.' '.$sender->lastname;
                $from['email']=$sender->email;
                $send = Mail::to($user->email)->send(new \App\Mail\SendCalendarEmail($userCalendar,$from,$from['name'],$user->firstname.' '.$user->lastname));
            }
        }
        $userCalendar->save();
        return response()->json("ok");
    }
    public function deleteEvent(Request $request,$id){

        $this->model = UserCalendar::where('id', '=', $id);

        try {

            $this->model = $this->model->firstOrFail();

        } catch (ModelNotFoundException $e) {
            return response()->json(['data' => ['message' => 'ERROR'.$e->getMessage()]], 422);
        }
        $this->model->delete();

        return response()->json(['data' => ['message' => 'ok']], 200);
    }
    public function getCalendar(Request $request,$year,$month){
        $m = date("Y-m",strtotime($year.'-'.$month));
        $calendarData = UserCalendar::where("date",'LIKE','%'.$m.'%')
            ->where(function($query){
                $query->where("user_id",Auth::user()->id)->orWhere('participants','LIKE','%'.Auth::user()->id."%");
            })
            ->orderBy("date",'asc')->orderBy('start')->get();
        $response =[];
        foreach ($calendarData as $c){
            if(!isset($response[$c->date])){
                $response[$c->date] =[];
            }
            $response[$c->date][]=$c->toArray();
        }

        return response()->json(['calendar'=>$response,'users'=>User::select(DB::raw("concat(lastname,' ',firstname) as name, id "))->whereNull("company_id")->get()->pluck("name","id")]);
    }
    public function companyPmStudents(Request $request){
        $userList = User::whereNotNull("students")->leftJoin('companies','companies.id','company_id')->select(DB::raw('users.*, companies.name as company_name'))->whereNull('users.deleted_at')->get();
        $response =[];
        foreach ($userList as $us){
            if(!isset($response[$us->company_name])) {
                $response[$us->company_name] = [
                    'name'=>$us->company_name,
                    'userList'=>[]
                    ];
                }
            if(!isset($response[$us->company_name]['userList'][$us->name])){
                $response[$us->company_name]['userList'][$us->name]['students']= [];
            }
            $response[$us->company_name]['userList'][$us->name]['students'] = Student::whereIn('student_number',explode(',',$us->students))->select(DB::raw('name, tax_number, email, phone, student_number'))->get()->toArray();
        }
        return response()->json($response);
    }
}
