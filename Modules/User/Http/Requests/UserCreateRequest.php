<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lastname' => 'required_if:name,NULL',
            'firstname' => 'required_if:name,NULL',
            'email' => 'required|email|unique:users,email,'.$this->id.',id,deleted_at,NULL',
        ];
    }

    public function attributes()
    {
        return [
            'lastname' => __('Last Name'),
            'firstname' => __('First Name'),
            'email' => __('E-mail'),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
