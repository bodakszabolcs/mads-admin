@component('mail::message')
# {{__('A MADS Iskolaszövetkezet meghívta Önt együttműködésre!')}}
Kérem látogasson el oldalunkra és belépés után változtassa meg jelszavát: <a href="{{config('app.url').'/admin/login'}}"> MADS CRM</a>
{{__('Az Ön jelszava')}}: {{$pass}}

{{__('Üdvözlettel')}},<br>
{{ config('app.name') }} csapata
@endcomponent
