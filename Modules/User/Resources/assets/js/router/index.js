import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import User from '../components/User'
import UserList from '../components/UserList'
import UserCreate from '../components/UserCreate'
import UserEdit from '../components/UserEdit'
import Profile from '../components/Profile'
import ProfileView from '../components/ProfileView'
import AccessLog from '../components/AccessLog'
import MailLog from '../components/MailLog'
import UserCompanyList from '../components/UserCompanyList'
import UserStatisticList from '../components/UserStatisticList'
import UserCalendar from '../components/UserCalendar'
import PMStatisticList from '../components/PMStatisticList'
import PMStudentStatisticList from '../components/PMStudentStatisticList'
import CompanyUserPresence from '../components/CompanyUserPresence'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'profile',
                component: Profile,
                meta: {
                    title: 'Profile'
                },
                children: [
                    {
                        path: '',
                        name: 'ProfileView',
                        component: ProfileView,
                        meta: {
                            title: 'Profile',
                            subheader: true
                        }
                    },
                    {
                        path: 'accesslog',
                        name: 'AccessLog',
                        component: AccessLog,
                        meta: {
                            title: 'Activities',
                            subheader: true
                        }
                    },
                    {
                        path: 'messages',
                        name: 'MailLog',
                        component: MailLog,
                        meta: {
                            title: 'Messages',
                            subheader: true
                        }
                    }
                ]
            },
            {
                path: 'activities/:id',
                name: 'Activities',
                component: AccessLog,
                meta: {
                    title: 'User Activities',
                    subheader: true
                }
            },
            {
                path: 'messages/:id',
                name: 'Messages',
                component: MailLog,
                meta: {
                    title: 'User messages',
                    subheader: true
                }
            },
            {
                path: 'user',
                component: User,
                meta: {
                    title: 'Users'
                },
                children: [
                    {
                        path: 'index',
                        name: 'UserList',
                        component: UserList,
                        meta: {
                            title: 'Users',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/user/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/user/index'
                        }
                    },
                    {
                        path: 'presence',
                        name: 'CompanyUserPresence',
                        component: CompanyUserPresence,
                        meta: {
                            title: 'Kapcsolattartó - diákok',
                            subheader: false
                        }
                    },
                    {
                        path: 'calendar',
                        name: 'Calendar',
                        component: UserCalendar,
                        meta: {
                            title: 'User calendar',
                            subheader: false
                        }
                    },
                    {
                        path: 'statistic',
                        name: 'UserStatisticList',
                        component: UserStatisticList,
                        meta: {
                            title: 'Projectmenedzser statisztika',
                            subheader: false
                        }
                    },
                    {
                        path: 'pm-statistic',
                        name: 'PM statistic',
                        component: PMStatisticList,
                        meta: {
                            title: 'Tevékenység statisztika',
                            subheader: false
                        }
                    },
                    {
                        path: 'pm-student-statistic',
                        name: 'PM student statistic',
                        component: PMStudentStatisticList,
                        meta: {
                            title: 'Diák statisztika',
                            subheader: false
                        }
                    },
                    {
                        path: 'company',
                        name: 'UserListCompany',
                        component: UserCompanyList,
                        meta: {
                            title: 'Users',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/user/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/user/company'
                        }
                    },
                    {
                        path: 'create',
                        name: 'UserCreate',
                        component: UserCreate,
                        meta: {
                            title: 'Create User',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/user/index'
                        }
                    },
                    {
                        path: 'edit/:id/:tab?',
                        name: 'UserEdit',
                        component: UserEdit,
                        meta: {
                            title: 'Edit User',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/user/index'
                        }
                    }
                ]
            }
        ]
    }
]
