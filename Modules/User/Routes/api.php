<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => '/user',
    'middleware' => ['auth:sanctum', 'accesslog', 'role']
], function () {
    Route::get('/accesslogs/{id?}', 'UserController@accessLogs')->name('User activities');
    Route::get('/maillogs/{id?}', 'UserController@mailLogs')->name('User messages');

    Route::match(['get'], '/get-pm-statistic', 'UserController@userStatistic')->name('PM statistic');
    Route::match(['get'], '/get-staffing-statistic', 'UserController@staffingStatistic')->name('Staffing requirements statistic');
    Route::post('/generate-password/{id}', 'UserController@generatePassword')->name('Generate Password');
    Route::post( '/force-login/{id?}', 'UserController@forceLogin')->name('Login as the selected user');
    Route::get( '/project-manager-statistic/{month?}', 'UserController@listProjectmanangerStatistic')->name('Project manager statistic');
});

Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => '/user',
    'middleware' => ['auth:sanctum']
], function () {
    Route::get( '/', 'UserController@getUser');
    Route::post( '/filter/save', 'UserController@saveFilter');
    Route::delete( '/filter/delete/{id}', 'UserController@deleteFilter');

    Route::match(['get'], '/profile', 'UserController@getProfile')->name('Profile show');
    Route::match(['get'], '/company', 'UserController@company')->name('Company user list');
    Route::match(['put'], '/update-profile', 'UserController@updateProfile')->name('Profile update');
    Route::match(['put'], '/change-password', 'UserController@changePassword')->name('Profile change password');
    Route::delete('/delete-profile', 'UserController@deleteProfile')->name('User delete profile');

    Route::get('/profile/accesslogs', 'UserController@accessLogsProfile')->name('Profile activities');
    Route::get('/profile/maillogs', 'UserController@mailLogsProfile')->name('Profile messages');
    Route::get('/company-pm-students', 'UserController@companyPmStudents')->name('Contact person - student');
});
Route::group([
    'namespace' => 'Modules\Student\Http\Controllers',
    'prefix' => '/student',
    'middleware' => ['auth:sanctum']
], function () {
    Route::match(['get'], '/profile', 'StudentController@getProfile')->name('Profile show');

    Route::match(['put'], '/change-password', 'StudentController@changePassword')->name('Profile change password');
});
Route::group([
    'namespace' => 'Modules\User\Http\Controllers',
    'prefix' => '/user/calendar',
    'middleware' => ['auth:sanctum','role']
], function () {
    Route::match(['get'], '/get-calendar/{year}/{month}', 'UserController@getCalendar')->name('Get user calendar');

    Route::match(['post'], '/edit', 'UserController@editEvent')->name('User calendar edit event');
    Route::match(['delete'], '/delete/{id}', 'UserController@deleteEvent')->name('User calendar delete event');
    Route::match(['get'], '/get-calendar-excel/{year}/{month}', 'UserController@exportCalendar')->name('User calendar export');
});
