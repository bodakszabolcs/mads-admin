<?php

namespace Modules\User\Observers;

use Modules\User\Entities\User;

class UserObserver
{
    /**
     * Handle the blog "deleted" event.
     *
     * @param  \Modules\User\Entities\User  $user
     * @return void
     */
    public function deleted(User $user)
    {
        $user->email = $user->email . '+DELETED+' . $user->id;
        $user->save();
    }

    /**
     * Handle the blog "restored" event.
     *
     * @param  \Modules\User\Entities\User  $user
     * @return void
     */
    public function restored(User $user)
    {
        $user->email = str_replace('+DELETED+'.$user->id,'',$user->email);
        $user->save();
    }
}
