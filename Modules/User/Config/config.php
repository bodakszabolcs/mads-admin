<?php

return [
    'name' => 'User',
    'menu_order' => 3,
    'menu' => [
        [
            'icon' => 'fa fa-calendar-day',
            'title' => 'Naptár',
            'route' => '/'.env('ADMIN_URL').'/user/calendar'
        ],[

                'icon' => 'la la-users',
                'title' => 'Felhasználók',
                'route' => '#users',
                'submenu' => [
                    [
                        'icon' => 'la la-users',
                        'title' => 'Admin felhasználók',
                        'route' => '/'.env('ADMIN_URL').'/user/index'
                    ],
                    [
                        'icon' => 'la la-users',
                        'title' => 'Céges felhasználók',
                        'route' => '/'.env('ADMIN_URL').'/user/company'
                    ],
                    [
                        'icon' => 'fa fa-clock',
                        'title' => 'Kapcsolattartó - Diákok',
                        'route' => '/'.env('ADMIN_URL').'/user/presence'
                    ],

                ]
    ]
        ]
];
