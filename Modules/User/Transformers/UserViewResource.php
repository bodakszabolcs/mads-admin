<?php

namespace Modules\User\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\Loyalty\Entities\Loyalty;
use Modules\Loyalty\Transformers\LoyaltyViewResource;
use Modules\User\Entities\User;
use Modules\Webshop\Entities\Country;
use Modules\Webshop\Entities\Stock;
use Modules\Webshop\Transformers\Country\CountryListResource;
use Modules\Webshop\Transformers\Country\CountryViewResource;
use Modules\WebshopFrontend\Transformers\ShopResource;


class UserViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */

    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'name_formatted' => $this->getName(App::getLocale()),
            'email' => $this->email,
            'phone' => $this->phone,
            'mads_email' => $this->mads_email,
            'signature' => $this->signature,
            'position' => $this->position,
            'avatar' => $this->avatar,
            'facebook_id' => $this->facebook_id,
            'monogram' => $this->getMono(App::getLocale()),
            'filters' => $this->filters(),
            'roles' => $this->roles,
            'is_company' => $this->is_company,
            'students' => $this->students,
            'email_verified_at' => format_date($this->email_verified_at, $this->getDateFormat()),
            'created_at' => format_date($this->created_at, $this->getDateFormat()),
            'updated_at' => format_date($this->updated_at, $this->getDateFormat()),
            'selectables' => [
                'positions'=> [0=>'Nem jelenik meg a honlapon']+User::$positions
            ]
        ];
    }
}
