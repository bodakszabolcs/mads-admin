<?php

namespace Modules\User\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\Company\Entities\Contact;

class UserStatisticResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date' => $this->date,
            'new_project' => $this->new_project,
            'new_order' => $this->new_order,
            'new_adversting' => $this->new_adversting,
            'refreshed_adversting' => $this->refreshed_adversting,
            'new_apply' => $this->new_apply,
            'new_apply_email' => $this->new_apply_email,
            'cv_request' => $this->cv_request,
            'cv_send' => $this->cv_send,
            'registration_send' => $this->registration_send,
            'start_work' => $this->start_work,
            'start_work_old' => $this->start_work_old,
            'visited_students' => $this->visited_students,
            'all_apply' => $this->all_apply,
            'contract' => $this->contract,
        ];
    }
}
