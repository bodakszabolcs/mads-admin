<?php

namespace Modules\User\Transformers;

use App\Http\Resources\BaseResource;
use Illuminate\Support\Facades\App;
use Modules\Company\Entities\Contact;

class UserListAccessResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'name_formatted' => $this->getName(App::getLocale()),
            'email' => $this->email,
            'avatar' => $this->avatar,
            'phone' => $this->phone,
            'facebook_id' => $this->facebook_id,
            'monogram' => $this->getMono(App::getLocale()),
            'filters' => $this->filters(),
            'roles' => $this->roles,
            'shipping' => $this->shipping,
            'billing' => $this->billing,
            "industries" => ($this->industries)?implode(',',$this->industries):[],
            "contact"=> optional(Contact::where('email',$this->email)->first())->id,
            'email_verified_at' => format_date($this->email_verified_at, $this->getDateFormat()),
            'created_at' => format_date($this->created_at, $this->getDateFormat()),
            'updated_at' => format_date($this->updated_at, $this->getDateFormat()),
        ];
    }
}
