<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\Base\BaseUser;
use Modules\Webshop\Entities\Cart;
use Modules\Webshop\Entities\Country;
use Modules\Webshop\Entities\Order;
use Modules\Webshop\Entities\Variation;
use Modules\Webshop\Entities\VariationPrice;
class User extends BaseUser
{
    use SoftDeletes;
    public static $positions= [
        1=>'HR MENEDZSER',
        2=>'PÉNZÜGY',
        3=>'ÉRTÉKESÍTÉSI VEZETŐ',
        4=>'HR VEZETŐ',
        5=>'BACK OFFICE ASSZISZTENS',
    ];


}
