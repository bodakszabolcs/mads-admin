<?php

namespace Modules\User\Entities;

use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\User\Entities\Base\BaseUserShipping;

class UserStatistic extends Model
{
    use Cachable, SoftDeletes;
    protected $table="user_statistics";

}
