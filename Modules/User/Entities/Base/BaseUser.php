<?php

namespace Modules\User\Entities\Base;

use App\Contracts\BaseModelInterface;
use App\Helpers\SearchHelper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Translation\HasLocalePreference;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Modules\Role\Entities\Role;
use Modules\User\Entities\UserBilling;
use Modules\User\Entities\UserShipping;
use Modules\User\Notifications\MailResetPasswordNotification;
use Modules\User\Notifications\MailResetPasswordNotificationAdmin;
use Modules\User\Notifications\VerifyEmail;

abstract class BaseUser extends Authenticatable implements BaseModelInterface, HasLocalePreference
{
    use Notifiable, HasApiTokens;

    protected $dateFormat = 'Y-m-d H:i';

    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'avatar',
        'phone',
        'position',
        'firstname',
        'lastname',
        'mads_email',
        'signature',
        'customer_groups_id',
        'previous_orders_total',
        'shop_orders_total',
        'webshop_orders_total',
        'sum_orders_total',
        'students',
        'introduction'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'mads_password',
        'remember_token',
    ];

    /**
     * Get the user's preferred locale.
     *
     * @return string
     */

    public function preferredLocale()
    {
        return $this->locale;
    }

    public $searchColumns = [];

    public function getFilters()
    {
        $this->searchColumns = [
            [
                'name' => 'name',
                'title' => __('Name'),
                'type' => 'text'
            ],
            [
                'name' => 'email',
                'title' => __('Email'),
                'type' => 'text'
            ],
            [
                'name' => 'role_id',
                'title' => __('Role Name'),
                'type' => 'select',
                'data' => Role::pluck('name', 'id')
            ],
            [
                'name' => 'created_at',
                'title' => __('Registration date'),
                'type' => 'date_interval'
            ]
        ];

        return $this->searchColumns;
    }

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'industries'=>'array'
    ];

    /**
     * The channels the user receives notification broadcasts on.
     *
     * @return string
     */
    public function receivesBroadcastNotificationsOn()
    {
        return 'users.' . $this->id;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MailResetPasswordNotificationAdmin($token, $this->email));
    }

    public function sendEmailVerificationNotification()
    {
       // $this->notify(new VerifyEmail());
    }

    protected $with = [
        'filters',
        'roles',
        'shipping',
        'billing'
    ];

    public function filters()
    {
        return $this->hasMany('Modules\User\Entities\UserFilter', 'user_id', 'id');
    }

    public function roles()
    {
        return $this->belongsToMany('Modules\Role\Entities\Role', 'roles_users', 'user_id', 'role_id');
    }
    public function roles_users()
    {
        return $this->belongsToMany('Modules\Role\Entities\Role', 'roles_users', 'user_id', 'role_id');
    }
    public function searchInModel(array $filter)
    {
        return SearchHelper::searchInModel($this, $filter);
    }

    public function shipping()
    {
        return $this->hasOne('Modules\User\Entities\UserShipping', 'user_id', 'id');
    }
    public function company()
    {
        return $this->hasOne('Modules\Company\Entities\Company', 'id', 'company_id');
    }

    public function billing()
    {
        return $this->hasOne('Modules\User\Entities\UserBilling', 'user_id', 'id');
    }

    public function isSuperAdmin()
    {
        return $this->whereHas('roles', function ($query) {
            $query->where('id', '=', 1);
        })->where('id', '=', $this->id)->count();
    }
    public function isUgyfelszolgalat()
    {

        return $this->whereHas('roles', function ($query) {
            $query->where('id', '=', 17);
        })->where('id', '=', $this->id)->count();
    }

    public function getName($locale = 'en')
    {
        if (!is_null($this->name)) {
            return $this->name;
        }

        if ($locale == 'hu') {
            return $this->lastname . ' ' . $this->firstname;
        }

        return $this->firstname . ' ' . $this->lastname;
    }

    public function getMono($locale = 'en')
    {
        $mg = '';
        $getName = Str::camel(Str::slug($this->getName($locale), " "));
        $name = explode(" ", $getName);
        foreach ($name as $n) {
            $mg .= substr($n, 0, 1);
        }

        return trim($mg);
    }

    public function getAvatar()
    {
        if ($this->avatar) {
            return $this->avatar;
        }

        return null;
    }

    public function fillAndSave(array $request)
    {
        $this->fill($request);
        $this->email_verified_at= date('Y-m-d H:i:s');
        if (empty($this->password)) {
            $this->password = Hash::make(Str::random(12));
        }

        if (Arr::get($request, 'email_verification', false)) {
            $this->email_verified_at = now();
        }

        if (Arr::get($request, 'new_password', null) !== null) {
            $this->password = Hash::make(Arr::get($request, 'new_password', null));
        }
        if (Arr::get($request, 'mads_password', null) !== null) {
            $this->mads_password = Crypt::encrypt(Arr::get($request, 'new_password', null));
        }

        $this->save();

        $ship = UserShipping::firstOrCreate([
            'user_id' => $this->id
        ]);
        $ship->fill(Arr::get($request, 'shipping', []));
        $ship->save();

        $bill = UserBilling::firstOrCreate([
            'user_id' => $this->id
        ]);

        $bill->fill(Arr::get($request, 'billing', []));
        $bill->save();
        if (is_array(Arr::get($request, 'user_roles'))) {
            $roles = [];
            foreach (Arr::get($request, 'user_roles', []) as $k => $r) {
                if (is_null($r) || $r == false) {
                    continue;
                }
                $roles[] = $k;
            }

            $this->roles()->sync($roles);
        }

        return $this;
    }
}
