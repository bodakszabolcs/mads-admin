<?php

namespace Modules\User\Entities;

use App\BaseModel;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;

class UserCalendar extends BaseModel
{
    use  Cachable;
	protected $table = "user_calendar";
	protected $fillable =['date','start','end','info','participants'];
    protected $casts=['participants'=>'json'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {

        return parent::getFilters();
    }








}
