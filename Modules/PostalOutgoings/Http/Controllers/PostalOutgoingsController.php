<?php

namespace Modules\PostalOutgoings\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\PostalOutgoings\Entities\PostalOutgoings;
use Illuminate\Http\Request;
use Modules\PostalOutgoings\Http\Requests\PostalOutgoingsCreateRequest;
use Modules\PostalOutgoings\Http\Requests\PostalOutgoingsUpdateRequest;
use Modules\PostalOutgoings\Transformers\PostalOutgoingsViewResource;
use Modules\PostalOutgoings\Transformers\PostalOutgoingsListResource;

class PostalOutgoingsController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PostalOutgoings();
        $this->viewResource = PostalOutgoingsViewResource::class;
        $this->listResource = PostalOutgoingsListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PostalOutgoingsCreateRequest::class;
        $this->updateRequest = PostalOutgoingsUpdateRequest::class;
    }

}
