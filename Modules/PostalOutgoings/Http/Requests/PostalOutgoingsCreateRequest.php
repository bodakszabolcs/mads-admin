<?php

namespace Modules\PostalOutgoings\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostalOutgoingsCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',

            'recipient' => 'required',
            'recipient_address' => 'required',

        ];
    }

    public function attributes()
    {
        return [
            'number' => __('Sorszám'),
            'postal_date' => __('Postázás dátuma'),
            'recipient' => __('Címzett'),
            'recipient_address' => __('Cím'),
            'post_delivery_id' => __('Küldemény tartalma'),
            'post_delivery_type_id' => __('Postázás módja'),
            'comment' => __('Megjegyzés'),
            'contrackt_back_date' => __('Szerődés visszaérkezés dátuma'),
            'transporter' => __('Szállító'),

        ];
    }

    public function authorize()
    {
        return true;
    }
}
