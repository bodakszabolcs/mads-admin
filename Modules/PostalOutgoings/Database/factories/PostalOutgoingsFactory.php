<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\PostalOutgoings\Entities\PostalOutgoings;

$factory->define(PostalOutgoings::class, function (Faker $faker) {
    return [
        "number" => $faker->realText(),
"postal_date" => $faker->date(),
"recipient" => $faker->realText(),
"recipient_address" => $faker->realText(),
"post_delivery_id" => rand(1000,5000),
"post_delivery_type_id" => rand(1000,5000),
"comment" => $faker->realText(),
"contrackt_back_date" => $faker->date(),
"transporter" => $faker->realText(),

    ];
});
