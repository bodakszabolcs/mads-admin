<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostalOutgoingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('postal_outgoings');
        Schema::create('postal_outgoings', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("number")->nullable();
$table->date("postal_date")->nullable();
$table->text("recipient")->nullable();
$table->text("recipient_address")->nullable();
$table->bigInteger("post_delivery_id")->unsigned()->nullable();
$table->bigInteger("post_delivery_type_id")->unsigned()->nullable();
$table->text("comment")->nullable();
$table->date("contrackt_back_date")->nullable();
$table->text("transporter")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_outgoings');
    }
}
