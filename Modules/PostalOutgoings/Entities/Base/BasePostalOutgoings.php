<?php

namespace Modules\PostalOutgoings\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Modules\PostDeliveryType\Entities\PostDeliveryType;
use Spatie\Translatable\HasTranslations;


abstract class BasePostalOutgoings extends BaseModel
{


    protected $table = 'postal_outgoings';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'number',
                    'title' => 'Sorszám',
                    'type' => 'text',
                    ],[
                    'name' => 'postal_date',
                    'title' => 'Postázás dátuma',
                    'type' => 'date',
                    ],[
                    'name' => 'recipient',
                    'title' => 'Címzett',
                    'type' => 'text',
                    ],[
                    'name' => 'recipient_address',
                    'title' => 'Cím',
                    'type' => 'text',
                    ],[
                    'name' => 'contrackt_back_date',
                    'title' => 'Szerődés visszaérkezés dátuma',
                    'type' => 'date',
                    ],];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['number','postal_date','recipient','recipient_address','post_delivery_id','post_delivery_type_id','comment','contrackt_back_date','transporter'];

    protected $casts = [];

}
