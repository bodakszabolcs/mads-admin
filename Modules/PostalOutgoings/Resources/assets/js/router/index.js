import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PostalOutgoings from '../components/PostalOutgoings'
import PostalOutgoingsList from '../components/PostalOutgoingsList'
import PostalOutgoingsCreate from '../components/PostalOutgoingsCreate'
import PostalOutgoingsEdit from '../components/PostalOutgoingsEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'postal-outgoings',
                component: PostalOutgoings,
                meta: {
                    title: 'PostalOutgoings'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PostalOutgoingsList',
                        component: PostalOutgoingsList,
                        meta: {
                            title: 'PostalOutgoings',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-outgoings/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-outgoings/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PostalOutgoingsCreate',
                        component: PostalOutgoingsCreate,
                        meta: {
                            title: 'Create PostalOutgoings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-outgoings/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PostalOutgoingsEdit',
                        component: PostalOutgoingsEdit,
                        meta: {
                            title: 'Edit PostalOutgoings',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-outgoings/index'
                        }
                    }
                ]
            }
        ]
    }
]
