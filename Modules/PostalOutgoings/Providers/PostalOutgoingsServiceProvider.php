<?php

namespace Modules\PostalOutgoings\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PostalOutgoings\Entities\PostalOutgoings;
use Modules\PostalOutgoings\Observers\PostalOutgoingsObserver;

class PostalOutgoingsServiceProvider extends ModuleServiceProvider
{
    protected $module = 'postaloutgoings';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PostalOutgoings::observe(PostalOutgoingsObserver::class);
    }
}
