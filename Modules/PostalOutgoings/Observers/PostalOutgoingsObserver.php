<?php

namespace Modules\PostalOutgoings\Observers;

use Modules\PostalOutgoings\Entities\PostalOutgoings;

class PostalOutgoingsObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function saved(PostalOutgoings $model)
    {
        PostalOutgoings::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function created(PostalOutgoings $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function updated(PostalOutgoings $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function deleted(PostalOutgoings $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function restored(PostalOutgoings $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PostalOutgoings\Entities\PostalOutgoings  $model
     * @return void
     */
    public function forceDeleted(PostalOutgoings $model)
    {
        //
    }
}
