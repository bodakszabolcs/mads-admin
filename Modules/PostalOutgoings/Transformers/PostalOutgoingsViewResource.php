<?php

namespace Modules\PostalOutgoings\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;
use Modules\PostalOutgoings\Entities\PostalOutgoings;
use Modules\PostDelivery\Entities\PostDelivery;
use Modules\PostDeliveryType\Entities\PostDeliveryType;
use Modules\PostSubject\Entities\PostSubject;


class PostalOutgoingsViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "number" => ($this->number)?$this->number:('K-'.date('Y-m').(PostalOutgoings::where('created_at','LIKE','%'.date('Y-m').'%')->disableCache()->count()+1)),
            "postal_date" => $this->postal_date?$this->income_date:date('Y-m-d'),
		    "recipient" => $this->recipient,
		    "recipient_address" => $this->recipient_address,
		    "post_delivery_id" => $this->post_delivery_id,
		    "post_delivery_type_id" => $this->post_delivery_type_id,
		    "comment" => $this->comment,
		    "contrackt_back_date" => $this->contrackt_back_date,
		    "transporter" => $this->transporter,
		"selectables" => [
            'transporters' =>InvoiceTransporter::all()->pluck('name','id'),
            'post_deliveries'=> PostDelivery::all()->pluck('name','id'),
            'postdeliverytype'=> PostDeliveryType::all()->pluck('name','id')
		]
		     ];
    }
}
