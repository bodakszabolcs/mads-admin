<?php

namespace Modules\PostalOutgoings\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PostalOutgoingsListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "number" => $this->number,
		    "postal_date" => $this->postal_date,
		    "recipient" => $this->recipient,
		    "recipient_address" => $this->recipient_address,
		    "post_delivery_id" => $this->post_delivery_id,
		    "post_delivery_type_id" => $this->post_delivery_type_id,
		    "comment" => $this->comment,
		    "contrackt_back_date" => $this->contrackt_back_date,
		    "transporter" => $this->transporter,
		     ];
    }
}
