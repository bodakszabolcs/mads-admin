<?php

namespace Modules\Nationality\Observers;

use Modules\Nationality\Entities\Nationality;

class NationalityObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function saved(Nationality $model)
    {
        Nationality::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function created(Nationality $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function updated(Nationality $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function deleted(Nationality $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function restored(Nationality $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Nationality\Entities\Nationality  $model
     * @return void
     */
    public function forceDeleted(Nationality $model)
    {
        //
    }
}
