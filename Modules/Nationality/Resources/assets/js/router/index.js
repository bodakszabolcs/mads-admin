import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import Nationality from '../components/Nationality'
import NationalityList from '../components/NationalityList'
import NationalityCreate from '../components/NationalityCreate'
import NationalityEdit from '../components/NationalityEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'nationality',
                component: Nationality,
                meta: {
                    title: 'Nationalities'
                },
                children: [
                    {
                        path: 'index',
                        name: 'NationalityList',
                        component: NationalityList,
                        meta: {
                            title: 'Nationalities',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/nationality/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/nationality/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'NationalityCreate',
                        component: NationalityCreate,
                        meta: {
                            title: 'Create Nationalities',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/nationality/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'NationalityEdit',
                        component: NationalityEdit,
                        meta: {
                            title: 'Edit Nationalities',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/nationality/index'
                        }
                    }
                ]
            }
        ]
    }
]
