<?php

namespace Modules\Nationality\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NationalityCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',"code" => 'required',
            "discount" => 'required',

        ];
    }

    public function attributes()
        {
            return [
                'name' => __('Megnevezés'),
                'code' => __('Kód'),
                'discount' => __('Vonatkozik rá a 25 év alatti kedvezmény?'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
