<?php

namespace Modules\Nationality\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\Nationality\Entities\Nationality;
use Illuminate\Http\Request;
use Modules\Nationality\Http\Requests\NationalityCreateRequest;
use Modules\Nationality\Http\Requests\NationalityUpdateRequest;
use Modules\Nationality\Transformers\NationalityViewResource;
use Modules\Nationality\Transformers\NationalityListResource;

class NationalityController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Nationality();
        $this->viewResource = NationalityViewResource::class;
        $this->listResource = NationalityListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = NationalityCreateRequest::class;
        $this->updateRequest = NationalityUpdateRequest::class;
    }

}
