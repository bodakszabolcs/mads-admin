<?php

namespace Modules\Nationality\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Nationality\Entities\Nationality;
use Modules\Nationality\Observers\NationalityObserver;

class NationalityServiceProvider extends ModuleServiceProvider
{
    protected $module = 'nationality';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Nationality::observe(NationalityObserver::class);
    }
}
