<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Nationality\Entities\Nationality;

$factory->define(Nationality::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
