<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'namespace' => 'Modules\Blog\Http\Controllers',
    'prefix' => '/blog',
    'middleware' => ['auth:sanctum']
], function () {
    Route::match(['get'], '/credentials', 'BlogController@credentials')->name('Blog tags');
});

Route::group([
    'namespace' => 'Modules\Blog\Http\Controllers',
    'prefix' => '/blog'
], function () {
    Route::get('/get-list/{categorySlug?}', 'BlogController@getList')->name('Get Frontend Blog');
    Route::get('/get/{slug?}', 'BlogController@getBlog')->name('Get Frontend Blog page');
});
