<?php

namespace Modules\Blog\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Observers\BlogObserver;

class BlogServiceProvider extends ModuleServiceProvider
{
    protected $module = 'blog';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Blog::observe(BlogObserver::class);
    }

}
