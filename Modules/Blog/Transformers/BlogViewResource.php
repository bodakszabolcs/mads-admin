<?php

namespace Modules\Blog\Transformers;

use App\Http\Resources\BaseResource;
use Modules\Category\Entities\Category;
use Modules\User\Entities\User;

class BlogViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'lead' => $this->lead,
            'content' => $this->content,
            'main_image' => $this->main_image,
            'og_image' => $this->main_image,
            'publication_date' => format_date($this->publication_date, $this->getDateFormat()),
            'author_name' => optional(User::where('id','=',$this->user_id)->first())->getName(),
            'author_image' => optional(User::where('id','=',$this->user_id)->first())->avatar,
            'created_at' => format_date($this->created_at, $this->getDateFormat()),
            'tags' => $this->tags->pluck('name','id'),
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'categories_list' => $this->categories,
            'categories' => $this->getCategories($this->categories)
        ];
    }

    private function getCategories($category)
    {
        $max = Category::count();
        $keys = array_fill(0, $max+1, false);
        foreach ($category as $cat) {
            $keys[$cat->id] = true;
        }

        return $keys;
    }
}
