<?php

namespace Modules\Blog\Transformers;

use App\Helpers\ConstansHelper;
use App\Http\Resources\BaseResource;
use Illuminate\Support\Carbon;
use Modules\Category\Transformers\CategoryListResource;
use Modules\Category\Transformers\TagListResource;
use Modules\User\Entities\User;

class BlogListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'title' => $this->title,
            'lead' => $this->lead,
            'content' => $this->content,
            'main_image' => $this->main_image,
            'og_image' => $this->main_image,
            'publication_date' => ConstansHelper::formatDate($this->publication_date),
            'author_name' => optional(User::where('id','=',$this->user_id)->first())->getName(),
            'author_image' => optional(User::where('id','=',$this->user_id)->first())->avatar,
            'created_at' => format_date($this->created_at, $this->getDateFormat()),
            'tags' => TagListResource::collection($this->tags),
            'meta_title' => $this->meta_title,
            'meta_description' => $this->meta_description,
            'categories_list' => CategoryListResource::collection($this->categories)
        ];
    }
}
