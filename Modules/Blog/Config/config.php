<?php

return [
    'name' => 'Blog',
    'menu_order' => 18,
    'menu' => [
        [
            'icon' => 'fa fa-globe',
            'title' => 'Site',
            'route' => '#content',
            'submenu' => [
                [
                    'icon' => 'la la-book',
                    'title' => 'Blog/News',
                    'route' => '/'.env('ADMIN_URL').'/blog/index',
                ],
                [
                    'icon' => 'la la-list-ul',
                    'title' => 'Categories',
                    'route' => '/'.env('ADMIN_URL').'/category/index'
                ],
                [
                    'icon' => 'la la-sitemap',
                    'title' => 'Menu',
                    'route' => '/'.env('ADMIN_URL').'/menu/index'
                ],
                [
                    'icon' => 'la la-windows',
                    'title' => 'Page',
                    'route' => '/'.env('ADMIN_URL').'/page/index'
                ],
                [

                    'icon' =>'fa fa-text-width',

                    'title' =>'Egyéb szöveges tartalmak',

                    'route' =>'/'.env('ADMIN_URL').'/content/index',

                ],
                [
                    'icon' => 'flaticon2-website',
                    'title' => 'Slider',
                    'route' => '/'.env('ADMIN_URL').'/sliders/index'
                ],
                [

                    'icon' =>'fa fa-image',

                    'title' =>'Downloadable contents',

                    'route' =>'/'.env('ADMIN_URL').'/downloadable-contents/index',

                ],
                [
                    'icon' => 'la la-book',
                    'title' => 'Contact',
                    'route' => '/'.env('ADMIN_URL').'/page/contact',
                ],
                [

                    'icon' =>'fa fa-newspaper',

                    'title' =>'Munka hírdetések',

                    'route' =>'/'.env('ADMIN_URL').'/work/index',

                ],
                [

                    'icon' =>'fa fa-language',

                    'title' =>'PartnerLogo',

                    'route' =>'/'.env('ADMIN_URL').'/partner-logo/index',

                ]
            ]
        ]
    ]
];
