<?php

namespace Modules\AutomaticEmail\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;


abstract class BaseAutomaticEmail extends BaseModel
{


    protected $table = 'automatic_emails';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'recipient_name',
                    'title' => 'cimzett',
                    'type' => 'text',
                    ],[
                    'name' => 'recipient_email',
                    'title' => 'email',
                    'type' => 'text',
                    ],[
                    'name' => 'subject',
                    'title' => 'Tárgy',
                    'type' => 'text',
                    ],
           [ 'name' => 'created_at',
            'title' => 'Dátum',
            'type' => 'date_interval',
        ]
            ];

        return parent::getFilters();
    }

    protected $with = [];

    protected $fillable = ['recipient_name','recipient_email','subject'];

    protected $casts = [];


}
