<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\AutomaticEmail\Entities\AutomaticEmail;

$factory->define(AutomaticEmail::class, function (Faker $faker) {
    return [
        "recipient_name" => $faker->realText(),
"recipient_email" => $faker->realText(),
"subject" => $faker->realText(),

    ];
});
