import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import AutomaticEmail from '../components/AutomaticEmail'
import AutomaticEmailList from '../components/AutomaticEmailList'
import AutomaticEmailCreate from '../components/AutomaticEmailCreate'
import AutomaticEmailEdit from '../components/AutomaticEmailEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'automatic-email',
                component: AutomaticEmail,
                meta: {
                    title: 'AutomaticEmails'
                },
                children: [
                    {
                        path: 'index',
                        name: 'AutomaticEmailList',
                        component: AutomaticEmailList,
                        meta: {
                            title: 'AutomaticEmails',
                            subheader: false
                        }
                    },
                    {
                        path: 'create',
                        name: 'AutomaticEmailCreate',
                        component: AutomaticEmailCreate,
                        meta: {
                            title: 'Create AutomaticEmails',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/automatic-email/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'AutomaticEmailEdit',
                        component: AutomaticEmailEdit,
                        meta: {
                            title: 'Edit AutomaticEmails',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/automatic-email/index'
                        }
                    }
                ]
            }
        ]
    }
]
