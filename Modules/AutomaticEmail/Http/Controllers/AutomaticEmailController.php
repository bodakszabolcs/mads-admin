<?php

namespace Modules\AutomaticEmail\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Illuminate\Http\Request;
use Modules\AutomaticEmail\Http\Requests\AutomaticEmailCreateRequest;
use Modules\AutomaticEmail\Http\Requests\AutomaticEmailUpdateRequest;
use Modules\AutomaticEmail\Transformers\AutomaticEmailViewResource;
use Modules\AutomaticEmail\Transformers\AutomaticEmailListResource;

class AutomaticEmailController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new AutomaticEmail();
        $this->viewResource = AutomaticEmailViewResource::class;
        $this->listResource = AutomaticEmailListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = AutomaticEmailCreateRequest::class;
        $this->updateRequest = AutomaticEmailUpdateRequest::class;
    }

}
