<?php

namespace Modules\AutomaticEmail\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutomaticEmailCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'recipient_name' => 'required',
			'recipient_email' => 'required',
			'subject' => 'required',
			
        ];
    }

    public function attributes()
        {
            return [
                'recipient_name' => __('cimzett'),
'recipient_email' => __('email'),
'subject' => __('Tárgy'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
