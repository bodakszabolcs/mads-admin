<?php

namespace Modules\AutomaticEmail\Observers;

use Modules\AutomaticEmail\Entities\AutomaticEmail;

class AutomaticEmailObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function saved(AutomaticEmail $model)
    {
        AutomaticEmail::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function created(AutomaticEmail $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function updated(AutomaticEmail $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function deleted(AutomaticEmail $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function restored(AutomaticEmail $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\AutomaticEmail\Entities\AutomaticEmail  $model
     * @return void
     */
    public function forceDeleted(AutomaticEmail $model)
    {
        //
    }
}
