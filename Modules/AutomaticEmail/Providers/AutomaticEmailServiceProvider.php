<?php

namespace Modules\AutomaticEmail\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\AutomaticEmail\Entities\AutomaticEmail;
use Modules\AutomaticEmail\Observers\AutomaticEmailObserver;

class AutomaticEmailServiceProvider extends ModuleServiceProvider
{
    protected $module = 'automaticemail';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        AutomaticEmail::observe(AutomaticEmailObserver::class);
    }
}
