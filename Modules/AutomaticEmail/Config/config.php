<?php

return [
    'name' => 'AutomaticEmail',

                 'menu_order' => 49,

                 'menu' => [
                     [

                      'icon' =>'flaticon2-rocket',

                      'title' =>'Automatikus emailek',

                      'route' =>'/'.env('ADMIN_URL').'/automatic-email/index',

                     ]

                 ]
];
