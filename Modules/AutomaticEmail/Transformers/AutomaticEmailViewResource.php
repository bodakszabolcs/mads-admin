<?php

namespace Modules\AutomaticEmail\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class AutomaticEmailViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "recipient_name" => $this->recipient_name,
		    "recipient_email" => $this->recipient_email,
		    "subject" => $this->subject,
            "created_at" => $this->created_at,
		"selectables" => [
		]
		     ];
    }
}
