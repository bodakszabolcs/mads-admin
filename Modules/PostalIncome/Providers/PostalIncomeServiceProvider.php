<?php

namespace Modules\PostalIncome\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\PostalIncome\Entities\PostalIncome;
use Modules\PostalIncome\Observers\PostalIncomeObserver;

class PostalIncomeServiceProvider extends ModuleServiceProvider
{
    protected $module = 'postalincome';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        PostalIncome::observe(PostalIncomeObserver::class);
    }
}
