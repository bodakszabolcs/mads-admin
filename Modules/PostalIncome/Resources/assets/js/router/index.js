import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import PostalIncome from '../components/PostalIncome'
import PostalIncomeList from '../components/PostalIncomeList'
import PostalIncomeCreate from '../components/PostalIncomeCreate'
import PostalIncomeEdit from '../components/PostalIncomeEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'postal-income',
                component: PostalIncome,
                meta: {
                    title: 'PostalIncomes'
                },
                children: [
                    {
                        path: 'index',
                        name: 'PostalIncomeList',
                        component: PostalIncomeList,
                        meta: {
                            title: 'PostalIncomes',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-income/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-income/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'PostalIncomeCreate',
                        component: PostalIncomeCreate,
                        meta: {
                            title: 'Create PostalIncomes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-income/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'PostalIncomeEdit',
                        component: PostalIncomeEdit,
                        meta: {
                            title: 'Edit PostalIncomes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/postal-income/index'
                        }
                    }
                ]
            }
        ]
    }
]
