<?php

namespace Modules\PostalIncome\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\PostalIncome\Entities\PostalIncome;
use Modules\PostSubject\Entities\PostSubject;


class PostalIncomeViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "number" => ($this->number)?$this->number:('B-'.date('Y-m').(PostalIncome::where('created_at','LIKE','%'.date('Y-m').'%')->disableCache()->count()+1)),
		    "income_date" => $this->income_date?$this->income_date:date('Y-m-d'),
		    "recipient" => $this->recipient,
		    "sender_name" => $this->sender_name,
		    "sender_address" => $this->sender_address,
		    "comment" => $this->comment,
		    "post_subject_id" => $this->post_subject_id,
		    "archive_date" => $this->archive_date,
		"selectables" => [
		"postsubject" => PostSubject::pluck("name","id"),
		]
		     ];
    }
}
