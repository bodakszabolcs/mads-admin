<?php

namespace Modules\PostalIncome\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class PostalIncomeListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
		    "number" => $this->number,
		    "income_date" => $this->income_date,
		    "recipient" => $this->recipient,
		    "sender_name" => $this->sender_name,
		    "sender_address" => $this->sender_address,
		    "comment" => $this->comment,
		    "post_subject_id" => optional($this->postsubject)->name,
		    "archive_date" => $this->archive_date,
		     ];
    }
}
