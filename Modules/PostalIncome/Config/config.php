<?php

return [
    'name' => 'PostalIncome',

                 'menu_order' => 42,

                 'menu' => [
                     [

                      'icon' =>'fa fa-archive',

                      'title' =>'Posta',


                         'route' => '#data',
                         'submenu' => [
                             [
                                 'icon' => 'fa fa-arrow-left',
                                 'title' => 'Bejövő',
                                 'route' =>'/'.env('ADMIN_URL').'/postal-income/index',
                             ],
                             [
                                 'icon' => 'fa fa-arrow-right',
                                 'title' => 'Kimenő',
                                 'route' => '/'.env('ADMIN_URL').'/postal-outgoings/index',
                             ],
                         ]
                     ]

                 ]
];
