<?php

namespace Modules\PostalIncome\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\PostSubject\Entities\PostSubject;


abstract class BasePostalIncome extends BaseModel
{


    protected $table = 'postal_income';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [[
                    'name' => 'number',
                    'title' => 'Sorszám',
                    'type' => 'text',
                    ],[
                    'name' => 'income_date',
                    'title' => 'Érkezés dátuma',
                    'type' => 'date',
                    ],[
                    'name' => 'recipient',
                    'title' => 'Címzett',
                    'type' => 'text',
                    ],[
                    'name' => 'sender_name',
                    'title' => 'Küldő neve',
                    'type' => 'text',
                    ],[
                    'name' => 'sender_address',
                    'title' => 'Küldő címe',
                    'type' => 'text',
                    ],[
                    'name' => 'archive_date',
                    'title' => 'Irattárba helyezés dátuma',
                    'type' => 'date',
                    ],];

        return parent::getFilters();
    }

    protected $with = ["postsubject"];

    protected $fillable = ['number','income_date','recipient','sender_name','sender_address','comment','post_subject_id','archive_date'];

    protected $casts = [];

    public function postsubject()
    {
        return $this->hasOne('Modules\PostSubject\Entities\PostSubject','id','post_subject_id');
    }



}
