<?php

namespace Modules\PostalIncome\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostalIncomeCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number' => 'required',
			'income_date' => 'required',
			'sender_name' => 'required',
			'sender_address' => 'required',



        ];
    }

    public function attributes()
        {
            return [
                'number' => __('Sorszám'),
'income_date' => __('Érkezés dátuma'),
'recipient' => __('Címzett'),
'sender_name' => __('Küldő neve'),
'sender_address' => __('Küldő címe'),
'comment' => __('Megyjegyzés'),
'post_subject_id' => __('Tárgy'),
'archive_date' => __('Irattárba helyezés dátuma'),

            ];
        }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
