<?php

namespace Modules\PostalIncome\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\SendPostalEmail;
use Modules\Company\Entities\Company;
use Modules\InvoiceTransporter\Entities\InvoiceTransporter;
use Modules\PostalIncome\Entities\PostalIncome;
use Illuminate\Http\Request;
use Modules\PostalIncome\Http\Requests\PostalIncomeCreateRequest;
use Modules\PostalIncome\Http\Requests\PostalIncomeUpdateRequest;
use Modules\PostalIncome\Transformers\PostalIncomeViewResource;
use Modules\PostalIncome\Transformers\PostalIncomeListResource;
use Modules\Project\Entities\ProjectStudent;
use Modules\Student\Entities\Student;

class PostalIncomeController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new PostalIncome();
        $this->viewResource = PostalIncomeViewResource::class;
        $this->listResource = PostalIncomeListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = PostalIncomeCreateRequest::class;
        $this->updateRequest = PostalIncomeUpdateRequest::class;
    }
    public function search(Request $request){
        $student = Student::where("name",'LIKE','%'.$request->input("search").'%')->limit(5)->get();
        $company = Company::where("name",'LIKE','%'.$request->input("search").'%')->limit(5)->get();
        $transporter = InvoiceTransporter::where("name",'LIKE','%'.$request->input("search").'%')->limit(5)->get();
        $resp =[];
        foreach ($student as $s){
            $resp[] =[
                'name'=> $s->name,
                'address'=> $s->notification_address
            ];
        }
        foreach ($company as $s){
            $resp[] =[
                'name'=> $s->name,
                'address'=> $s->postal_address
            ];
        }
        foreach ($transporter as $s){
            $resp[] =[
                'name'=> $s->name,
                'address'=> $s->address
            ];
        }
        return response()->json(["data"=>$resp],200);
    }
    public function create(Request $request)
    {

        if ($this->createRequest) {
            app()->make($this->createRequest);
        }
        $this->model->fillAndSave($request->all());
        if($request->has('student_id') && $request->input('student_id') >0){
            SendPostalEmail::dispatch( $request->input('student_id'),$this->model);
        }

        return new $this->viewResource($this->model);

    }

}
