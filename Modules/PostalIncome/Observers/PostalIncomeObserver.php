<?php

namespace Modules\PostalIncome\Observers;

use Modules\PostalIncome\Entities\PostalIncome;

class PostalIncomeObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function saved(PostalIncome $model)
    {
        PostalIncome::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function created(PostalIncome $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function updated(PostalIncome $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function deleted(PostalIncome $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function restored(PostalIncome $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\PostalIncome\Entities\PostalIncome  $model
     * @return void
     */
    public function forceDeleted(PostalIncome $model)
    {
        //
    }
}
