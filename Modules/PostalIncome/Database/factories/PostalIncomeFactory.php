<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\PostalIncome\Entities\PostalIncome;

$factory->define(PostalIncome::class, function (Faker $faker) {
    return [
        "number" => $faker->realText(),
"income_date" => $faker->date(),
"recipient" => $faker->realText(),
"sender_name" => $faker->realText(),
"sender_address" => $faker->realText(),
"comment" => $faker->realText(),
"post_subject_id" => rand(1000,5000),
"archive_date" => $faker->date(),

    ];
});
