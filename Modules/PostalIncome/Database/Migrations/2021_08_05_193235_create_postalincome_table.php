<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostalIncomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('postal_income');
        Schema::create('postal_income', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->text("number")->nullable();
$table->date("income_date")->nullable();
$table->text("recipient")->nullable();
$table->text("sender_name")->nullable();
$table->text("sender_address")->nullable();
$table->text("comment")->nullable();
$table->bigInteger("post_subject_id")->unsigned()->nullable();
$table->date("archive_date")->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postal_income');
    }
}
