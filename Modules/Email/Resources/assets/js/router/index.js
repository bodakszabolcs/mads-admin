import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'

import EmailList from '../components/EmailList'
import Email from '../components/Email'
import EmailView from '../components/EmailView'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'email',
                component: Email,
                meta: {
                    title: 'Emails'
                },
                children: [
                    {
                        path: 'index',
                        name: 'EmailList',
                        component: EmailList,
                        meta: {
                            title: 'Emails',
                            subheader: false
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'EmailView',
                        component: EmailView,
                        meta: {
                            title: 'Email megtekintése',
                            subheader: false,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/email/index'
                        }
                    }

                ]
            }
        ]
    }
]
