<?php

namespace Modules\Email\Observers;

use Modules\Email\Entities\Email;

class EmailObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function saved(Email $model)
    {
        Email::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function created(Email $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function updated(Email $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function deleted(Email $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function restored(Email $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\Email\Entities\Email  $model
     * @return void
     */
    public function forceDeleted(Email $model)
    {
        //
    }
}
