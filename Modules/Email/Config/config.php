<?php

return [
    'name' => 'Email',

                 'menu_order' => 38,

                 'menu' => [
                     [

                      'icon' =>'flaticon-squares',

                      'title' =>'Email',

                      'route' =>'/'.env('ADMIN_URL').'/email/index',

                     ]

                 ]
];
