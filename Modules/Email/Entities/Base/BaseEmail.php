<?php

namespace Modules\Email\Entities\Base;

use App\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use GeneaLabs\LaravelModelCaching\Traits\Cachable;
use Spatie\Translatable\HasTranslations;
use Modules\User\Entities\User;


abstract class BaseEmail extends BaseModel
{


    protected $table = 'emails';

    protected $dates = ['created_at', 'updated_at'];



    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    public function getFilters()
    {
        $this->searchColumns = [];

        return parent::getFilters();
    }


    protected $fillable = ['user_id','subject','recipient_name','recipient_email','sent_date','view_date','sent_group','project_sablon_id'];

    protected $casts = ['attachments'=>'array'];

     public function user()
    {
        return $this->hasOne('Modules\User\Entities\User','id','user_id');
    }



}
