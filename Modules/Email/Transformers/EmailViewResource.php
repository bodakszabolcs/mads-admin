<?php

namespace Modules\Email\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;
use Modules\User\Entities\User;


class EmailViewResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [

		    "subject" => $this->subject,
		    "content" => $this->content,
		    "recipient_name" => $this->recipient_name,
		    "recipient_email" => $this->recipient_email,
		    "sent_date" => $this->sent_date,
		    "view_date" => $this->view_date,

        ];
    }
}
