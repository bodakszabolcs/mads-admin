<?php

namespace Modules\Email\Transformers;

use Illuminate\Http\Resources\Json\Resource;
use App\Http\Resources\BaseResource;


class EmailListResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->sent_group,
		    "subject" => $this->subject,
		    "recipient_name" => $this->cnt.' címzett',
		    "sent_date" => $this->sent_date,
		    "viewed" => $this->viewed,
        ];
    }
}
