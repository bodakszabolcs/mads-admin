<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'namespace' => 'Modules\Email\Http\Controllers', 'prefix' => '/email', 'middleware' => ['auth:sanctum', 'role']
], function () {
    Route::post('/send-email', 'EmailController@sendEmail')->name('Send email from system');
    Route::post('/send-email-cv', 'EmailController@sendEmailCV')->name('Önéletrajz továbbítás');
    Route::post('/send-email-introduction', 'EmailController@sendEmailIntroduction')->name('Bemutatkozó küldése');

});
