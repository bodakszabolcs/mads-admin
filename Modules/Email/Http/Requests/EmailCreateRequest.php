<?php

namespace Modules\Email\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmailCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'subject' => 'required',
            'content' => 'required',
            'recipients' => 'required',
        ];
    }

    public function attributes()
    {
        return [
            'user_id' => __('User'),
            'subject' => __('Tárgy'),
            'content' => __('Tartalom'),
            'recipient_name' => __('Cimzett'),
            'recipient_email' => __('Címzett email'),
            'sent_date' => __('Küldés időpontja'),
            'view_date' => __('Megtekintés időpontja'),
            'sent_group' => __('Email csoport'),

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
