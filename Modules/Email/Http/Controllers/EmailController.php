<?php

namespace Modules\Email\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use App\Jobs\SendEmail;
use App\Jobs\SendIntroduction;
use App\Mail\SendIntroductionEmail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Modules\Email\Entities\Email;
use Illuminate\Http\Request;
use Modules\Email\Http\Requests\EmailCreateRequest;
use Modules\Email\Http\Requests\EmailUpdateRequest;
use Modules\Email\Transformers\EmailViewResource;
use Modules\Email\Transformers\EmailListResource;

class EmailController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new Email();
        $this->viewResource = EmailViewResource::class;
        $this->listResource = EmailListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = EmailCreateRequest::class;
        $this->updateRequest = EmailUpdateRequest::class;
    }
    public function index(Request $request, $auth = null)
    {
        $list = Email::where('user_id',\Auth::id())->groupBy('sent_group')->select(DB::raw('sent_group, subject, count(recipient_email) as cnt,sum(if(isnull(view_date),0,1)) as viewed, max(sent_date) as sent_date, group_concat(recipient_name) as recipient_name,group_concat(recipient_email) as recipient_email'));
        if($request->input('search')){
            $search = $request->input('search');
            $list = $list->where(function($query) use ($search){
                $query->where('recipient_name','LIKE','%'.$search.'%')->orWhere('recipient_email','LIKE','%'.$search.'%')->orWhere('subject','LIKE','%'.$search.'%');
            });
        }

        if($request->input('sort')){
            $sort = explode('|',$request->input('sort'));
            $list = $list->orderBy($sort[0],$sort[1]);
        }else {
            $list = $list->orderBy('sent_date','desc');
        }
        $pagination = (int)Arr::get($request, 'per_page', config('app.paginate'));
        $list = $list->paginate($pagination)->withPath($request->path());

        return $this->listResource::collection($list)->additional(['filters' => $this->model->getFilters()]);
    }
    public function show(Request $request, $id = 0, $auth = null)
    {
       // try{

        $this->model = $this->model->where('sent_group', '=', $id)->where('user_id', '=', \Auth::id())->firstOrFail();

        $response = [
            "subject" => $this->model->subject,
            "content" => (new \App\Mail\SendEmail($this->model))->render(),
            "data" => Email::where('sent_group','=',$id)->select(DB::raw('recipient_name,recipient_email,view_date,sent_date'))->get()->toArray(),
            "attachments" =>$this->model->attachments,
        ];

       /* } catch (ModelNotFoundException $e) {
            dd($e->getMessage());
            abort(404);
        }*/
        return  response()->json($response,$this->successStatus);

    }
    public function sendEmail(EmailCreateRequest $request){
        ini_set("max_execution_time","-1");
        try {
           // DB::beginTransaction();
            $groupid = \Auth::id() . date('YmdHis').round(microtime(true) * 1000);
            foreach ($request->input('recipients') as $rec) {

                $email = new Email();
                $email->fill($request->all());
                $email->user_id = \Auth::id();
                $email->sent_group = $groupid;
                $email->content = $request->input('content');
                $email->recipient_name = $rec['name'];
                $email->recipient_email = $rec['email'];
                $email->attachments = $request->input('attachments');
                $email->save();
                //SendEmail::dispatch($email);
            }
           // DB::commit();
        }catch (\Exception $e){
            return response()->json($e->getMessage(),$this->errorStatus);
        }
        return response()->json($this->successMessage,$this->successStatus);
    }
    public function sendEmailCv(EmailCreateRequest $request){
        ini_set("max_execution_time","-1");
        try {
            // DB::beginTransaction();
            $groupid = \Auth::id() . date('YmdHis').round(microtime(true) * 1000);


                $email = new Email();
                $email->fill($request->all());
                $email->user_id = \Auth::id();
                $email->sent_group = $groupid;
                $email->content = $request->input('content');
                $email->recipient_name =  $request->input('recipient_name');
                $email->recipient_email = $request->input('recipients');
                $email->attachments = [['path'=>$request->input('cv'),'name'=>$request->input('cv_name')]];
                $email->save();
                SendEmail::dispatch($email);

            // DB::commit();
        }catch (\Exception $e){
            return response()->json($e->getMessage(),$this->errorStatus);
        }
        return response()->json($this->successMessage,$this->successStatus);
    }
    public function sendEmailIntroduction(EmailCreateRequest $request){
        try {
            // DB::beginTransaction();
            $link= '/ajanlat-keres?id='.\Auth::id().'&cid='.$request->input('id');
            SendIntroduction::dispatch(\Auth::user(),$request->input('recipients'), $request->input('recipient_name'), $request->input('subject'),$link, $request->input('content'));
            if($request->has('other_recipients') && isset($request->input('other_recipients')[0])){
                foreach ($request->input('other_recipients',[]) as $rec){
                    SendIntroduction::dispatch(\Auth::user(),$rec['email'], $rec['name'], $request->input('subject'),$link, $request->input('content'));
                }

            }
            // DB::commit();
        }catch (\Exception $e){
            return response()->json($e->getMessage(),$this->errorStatus);
        }
        return response()->json($this->successMessage,$this->successStatus);
    }

}
