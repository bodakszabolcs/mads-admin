<?php

namespace Modules\Email\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\Email\Entities\Email;
use Modules\Email\Observers\EmailObserver;

class EmailServiceProvider extends ModuleServiceProvider
{
    protected $module = 'email';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        Email::observe(EmailObserver::class);
    }
}
