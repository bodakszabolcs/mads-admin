<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('emails');
        Schema::create('emails', function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->unsigned()->nullable();
            $table->text("subject")->nullable();
            $table->text("content")->nullable();
            $table->text("recipient_name")->nullable();
            $table->text("recipient_email")->nullable();
            $table->timestamp("sent_date")->nullable();
            $table->timestamp("view_date")->nullable();
            $table->bigInteger("sent_group")->unsigned()->nullable();

            $table->timestamps();
            $table->softDeletes();
            $table->index(["deleted_at"]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
