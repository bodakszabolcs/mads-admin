<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\Email\Entities\Email;

$factory->define(Email::class, function (Faker $faker) {
    return [
        "user_id" => rand(1000,5000),
"subject" => $faker->realText(),
"content" => $faker->realText(),
"recipient_name" => $faker->realText(),
"recipient_email" => $faker->realText(),
"sent_date" => $faker->dateTime(),
"view_date" => $faker->dateTime(),
"sent_group" => rand(1000,5000),

    ];
});
