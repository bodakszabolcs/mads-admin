<?php

namespace Modules\AreaType\Http\Controllers;

use App\Http\Controllers\AbstractLiquidController;
use Modules\AreaType\Entities\AreaType;
use Illuminate\Http\Request;
use Modules\AreaType\Http\Requests\AreaTypeCreateRequest;
use Modules\AreaType\Http\Requests\AreaTypeUpdateRequest;
use Modules\AreaType\Transformers\AreaTypeViewResource;
use Modules\AreaType\Transformers\AreaTypeListResource;

class AreaTypeController extends AbstractLiquidController
{

    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->model = new AreaType();
        $this->viewResource = AreaTypeViewResource::class;
        $this->listResource = AreaTypeListResource::class;
        $this->useResourceAsCollection = true;
        $this->createRequest = AreaTypeCreateRequest::class;
        $this->updateRequest = AreaTypeUpdateRequest::class;
    }

}
