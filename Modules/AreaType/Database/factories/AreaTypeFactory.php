<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use Faker\Generator as Faker;
use Modules\AreaType\Entities\AreaType;

$factory->define(AreaType::class, function (Faker $faker) {
    return [
        "name" => $faker->realText(),

    ];
});
