import Admin from './../../../../../Admin/Resources/assets/js/components/Admin'
import AreaType from '../components/AreaType'
import AreaTypeList from '../components/AreaTypeList'
import AreaTypeCreate from '../components/AreaTypeCreate'
import AreaTypeEdit from '../components/AreaTypeEdit'

export default [
    {
        path: `/${process.env.MIX_ADMIN_URL}`,
        component: Admin,
        meta: {
            requiresAuth: true,
            title: 'Admin'
        },
        children: [
            {
                path: 'area-type',
                component: AreaType,
                meta: {
                    title: 'AreaTypes'
                },
                children: [
                    {
                        path: 'index',
                        name: 'AreaTypeList',
                        component: AreaTypeList,
                        meta: {
                            title: 'AreaTypes',
                            subheader: true,
                            add_new_link: `/${process.env.MIX_ADMIN_URL}` + '/area-type/create',
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/area-type/index'
                        }
                    },
                    {
                        path: 'create',
                        name: 'AreaTypeCreate',
                        component: AreaTypeCreate,
                        meta: {
                            title: 'Create AreaTypes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/area-type/index'
                        }
                    },
                    {
                        path: 'edit/:id',
                        name: 'AreaTypeEdit',
                        component: AreaTypeEdit,
                        meta: {
                            title: 'Edit AreaTypes',
                            subheader: true,
                            list_link: `/${process.env.MIX_ADMIN_URL}` + '/area-type/index'
                        }
                    }
                ]
            }
        ]
    }
]
