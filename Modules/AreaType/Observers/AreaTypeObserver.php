<?php

namespace Modules\AreaType\Observers;

use Modules\AreaType\Entities\AreaType;

class AreaTypeObserver
{
    /**
     * Handle the model "saved" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function saved(AreaType $model)
    {
        AreaType::unsetEventDispatcher();
        //
    }

    /**
     * Handle the model "created" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function created(AreaType $model)
    {
        //
    }

    /**
     * Handle the model "updated" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function updated(AreaType $model)
    {
        //
    }

    /**
     * Handle the model "deleted" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function deleted(AreaType $model)
    {
        //
    }

    /**
     * Handle the model "restored" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function restored(AreaType $model)
    {
        //
    }

    /**
     * Handle the model "force deleted" event.
     *
     * @param  \Modules\AreaType\Entities\AreaType  $model
     * @return void
     */
    public function forceDeleted(AreaType $model)
    {
        //
    }
}
