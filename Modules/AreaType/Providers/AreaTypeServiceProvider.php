<?php

namespace Modules\AreaType\Providers;

use App\Providers\ModuleServiceProvider;
use Modules\AreaType\Entities\AreaType;
use Modules\AreaType\Observers\AreaTypeObserver;

class AreaTypeServiceProvider extends ModuleServiceProvider
{
    protected $module = 'areatype';
    protected $directory = __DIR__;

    public function boot()
    {
        parent::boot();

        AreaType::observe(AreaTypeObserver::class);
    }
}
