<?php

return [
    /*
     * The property id of which you want to display data.
     */
    'property_id' => 403526650,

    /*
     * Path to the client secret json file.
     */
    'service_account_credentials_json' =>  app_path('google/google-service.json')
];
