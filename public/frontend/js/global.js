$(function () {
    'use strict'

    /* ================ */
    /* 01 - VARIABLES */
    /* ================ */
    var swipers = []; var winW; var winH; var _isresponsive; var xsPoint = 767; var smPoint = 991; var mdPoint = 1199; var enableScroll = 0
    var isSafari = /constructor/i.test(window.HTMLElement)
    if (isSafari) $('body').addClass('safari')

    /* ======================== */
    /* 02 - page calculations */
    /* ======================== */
    function page(){
        $(function () {
            var canvas = document.querySelector('canvas')
            var ctx = canvas.getContext('2d')
            var color = 'white'
            redraw()

            function redraw () {
                canvas.width = window.innerWidth
                canvas.height = window.innerHeight
                canvas.style.display = 'block'
                ctx.fillStyle = color
                ctx.lineWidth = 0.3
                ctx.strokeStyle = color
            }

            $(window).resize(function () {
                redraw()
            })
            window.addEventListener('orientationchange', function () {
                redraw()
            }, false)

            var mousePosition = {
                x: 30 * canvas.width / 100,
                y: 30 * canvas.height / 100
            }

            var dots = {
                nb: 150,
                distance: 80,
                d_radius: 150,
                array: []
            }

            function Dot () {
                this.x = Math.random() * canvas.width
                this.y = Math.random() * canvas.height

                this.vx = -0.5 + Math.random()
                this.vy = -0.5 + Math.random()

                this.radius = Math.random()
            }

            Dot.prototype = {
                create: function () {
                    ctx.beginPath()
                    ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
                    ctx.fill()
                },

                animate: function () {
                    for (i = 0; i < dots.nb; i++) {
                        var dot = dots.array[i]

                        if (dot.y < 0 || dot.y > canvas.height) {
                            dot.vx = dot.vx
                            dot.vy = -dot.vy
                        } else if (dot.x < 0 || dot.x > canvas.width) {
                            dot.vx = -dot.vx
                            dot.vy = dot.vy
                        }
                        dot.x += dot.vx
                        dot.y += dot.vy
                    }
                },

                line: function () {
                    for (i = 0; i < dots.nb; i++) {
                        for (j = 0; j < dots.nb; j++) {
                            const i_dot = dots.array[i]
                            const j_dot = dots.array[j]

                            if ((i_dot.x - j_dot.x) < dots.distance && (i_dot.y - j_dot.y) < dots.distance && (i_dot.x - j_dot.x) > -dots.distance && (i_dot.y - j_dot.y) > -dots.distance) {
                                if ((i_dot.x - mousePosition.x) < dots.d_radius && (i_dot.y - mousePosition.y) < dots.d_radius && (i_dot.x - mousePosition.x) > -dots.d_radius && (i_dot.y - mousePosition.y) > -dots.d_radius) {
                                    ctx.beginPath()
                                    ctx.moveTo(i_dot.x, i_dot.y)
                                    ctx.lineTo(j_dot.x, j_dot.y)
                                    ctx.stroke()
                                    ctx.closePath()
                                }
                            }
                        }
                    }
                }
            }

            function createDots () {
                ctx.clearRect(0, 0, canvas.width, canvas.height)
                for (i = 0; i < dots.nb; i++) {
                    dots.array.push(new Dot())
                    const dot = dots.array[i]

                    dot.create()
                }

                dot.line()
                dot.animate()
            }

            $('body').on('mousemove mouseleave', function (e) {
                if (e.type === 'mousemove') {
                    mousePosition.x = e.pageX
                    mousePosition.y = e.pageY
                }
                if (e.type === 'mouseleave') {
                    mousePosition.x = canvas.width / 2
                    mousePosition.y = canvas.height / 2
                }
            })
            setInterval(createDots, 1000 / 30)
        })
    }
    function pageCalculations () {
        winW = $(window).width()
        winH = $(window).height();
        if ($('.mob-icon').is(':visible')) _isresponsive = true
        else _isresponsive = false
        $('.block.type-2 .col-md-6.col-md-push-6').height($('.block.type-2 .col-md-4.col-md-pull-6').height())

        if (winH < 700 && $('.bottom-fixed-control-class').length) $('header').removeClass('bottom-fixed-control-class bottom-fixed')
        $('.teaser-container').css({ height: winH })
    }

    /* ================================= */
    /* 03 - function on document ready */
    /* ================================= */
    pageCalculations()

    // center all images inside containers
    $('.center-image').each(function () {
        var bgSrc = $(this).attr('src')
        $(this).parent().css({ 'background-image': 'url(' + bgSrc + ')' })
        $(this).hide()
    })

    /* ============================ */
    /* 04 - function on page load */
    /* ============================ */
    $(window).on('load', function () {
        // $('body, html').animate({'scrollTop':'0'}, 0);

        // window.scrollTo(0, 0);

        if (window.location.hash) {
            if($('.scroll-to-block[data-id="' + window.location.hash.substr(1) + '"]')) {
                setTimeout(function () {
                    $('body, html').animate({scrollTop: $('.scroll-to-block[data-id="' + window.location.hash.substr(1) + '"]').offset().top - (($('.sidebar-menu-added').length) ? 0 : $('header').height()) + 1}, 10)

                    // window.scrollTo(0, $('.scroll-to-block').eq(index).offset().top - $('header').height() + 1);
                }, 100)
            }
        } else {
            $('body, html').animate({ scrollTop: 0 }, 10)
        }

        $('body').addClass('loaded')
    })

    /* ============================== */
    /* 05 - function on page resize */
    /* ============================== */
    function resizeCall () {
        pageCalculations()
    }
    $(window).resize(function () {
        resizeCall()
    })
    $(window).scroll(function(){
        pageCalculations()
        scrollCall();
    });

    function scrollCall(){

        var winScroll = $(window).scrollTop();

        if(!_isresponsive && !$('header').hasClass('default-act') && !$('.sidebar-menu-added').length) {
            if($(window).scrollTop()>=25) $('header').addClass('act');
            else $('header').removeClass('act');
        }

        if($('header').hasClass('bottom-fixed-control-class')){
            if(winScroll>=(winH-75)) $('header').removeClass('bottom-fixed');
            else $('header').addClass('bottom-fixed');
        }

        if($('.scroll-to-block').length && enableScroll){
            var headerHeight = ($('.sidebar-menu-added').length)?0:75;
            $('.scroll-to-block').each(function( index, element ) {
                if($(element).offset().top<=(winScroll+headerHeight) && ($(element).offset().top+$(element).height()) > (winScroll+headerHeight) ){
                    $('.scroll-to-link.act').removeClass('act');
                    $('.scroll-to-link[href="#'+$(element).attr('data-id')+'"]').addClass('act');
                    if(window.location.hash!='#'+$(element).attr('data-id')) window.location.hash = '#'+$(element).attr('data-id');
                    return false;
                }
            });
        }

    }
    window.addEventListener('orientationchange', function () {
        resizeCall()
    }, false)

    /* ============================== */
    /* 06 - function on page scroll */
    /* ============================== */

    // scrolling to some block
    $(document).on('click', '.scroll-to-link', function () {
        var headerHeight = ($('.sidebar-menu-added').length) ? 0 : 75
        var index = $(this).attr('href').substr(1)
        if ($('.scroll-to-block[data-id="' + index + '"]')) {
            $('body, html').animate({ scrollTop: $('.scroll-to-block[data-id="' + index + '"]').offset().top - headerHeight + 1 }, 500)
        }
        $('header').removeClass('act-mob')
        $('.mob-icon').removeClass('act')
        return false
    })
    /* ============================== */
    /* 08 - buttons, clicks, hovers */
    /* ============================== */

    // menu click in responsive
    $(document).on('click', '.mob-icon', function () {
        if ($(this).hasClass('act')) {
            $('.mob-icon').removeClass('act')
            $('header').removeClass('act-mob')
        } else {
            $('.mob-icon').addClass('act')
            $('header').addClass('act-mob')
        }
    })

    // tabs
    var tabsFinish = 0
    $('.tabs-switch').on('click', function () {
        if ($(this).hasClass('active') || tabsFinish) return false
        tabsFinish = 1
        $(this).parent().find('.active').removeClass('active')
        $(this).addClass('active')
        var tabIndex = $(this).parent().find('.tabs-switch').index(this)
        var tabsContainer = $(this).closest('.tabs-switch-container')
        tabsContainer.find('.tabs-entry:visible').fadeOut('fast', function () {
            tabsContainer.find('.tabs-entry').eq(tabIndex).fadeIn('fast', function () { tabsFinish = 0 })
        })
    })

    // accordeon
    $('.accordeon .entry .title').on('click', function () {
        $(this).parent().toggleClass('active')
        $(this).parent().find('.text').slideToggle('fast')
    })

    $('.categories-wrapper .entry.toggle').on('click', function () {
        $(this).toggleClass('active')
        $(this).next('.sub-wrapper').slideToggle('fast')
    })

    // gallery detail popup
    $('.sorting-item.open-popup').on('click', function () {
        $('.gallery-popup').addClass('active')
        var index = $(this).parent().find('.open-popup').index(this)
        swipers['swiper-' + $('.gallery-popup').find('.swiper-container').attr('id')].swipeTo(index, 0)
        $('body, html').toggleClass('overflow-hidden')
    })

    $('.gallery-popup .close-popup').on('click', function () {
        $('.gallery-popup').removeClass('active')
        $('body, html').toggleClass('overflow-hidden')
    })

    // open fullscreen preview popup
    $('.open-fullscreen').on('click', function () {
        $('.screen-preview-popup').addClass('active').find('.overflow img').attr('src', $(this).data('fullscreen'))
        $('body, html').toggleClass('overflow-hidden')
    })

    $('.screen-preview-popup .close-popup').on('click', function () {
        $('.screen-preview-popup').removeClass('active')
        $('body, html').toggleClass('overflow-hidden')
    })

    // checkbox
    $(document).on('click', '.checkbox-entry.checkbox label', function () {
        $(this).parent().toggleClass('active')
        $(this).parent().find('input').click()
    })

    $(document).on('click', '.checkbox-entry.radio label', function () {
        $(this).parent().find('input').click()
        if (!$(this).parent().hasClass('active')) {
            var nameVar = $(this).parent().find('input').attr('name')
            $('.checkbox-entry.radio input[name="' + nameVar + '"]').parent().removeClass('active')
            $(this).parent().addClass('active')
        }
    })

    // responsive drop-down in gallery
    $('.responsive-filtration-title').on('click', function () {
        $(this).closest('.sorting-menu').toggleClass('active')
    })

    // mousehover on phone-icons block
    $('.phone-icons-description .entry').on('mouseover', function () {
        var thisWrapper = $(this).closest('.phone-icons-box'); var val = thisWrapper.find('.phone-icons-description .entry').index(this) + 1
        thisWrapper.find('.phone-icons-image').eq(val).addClass('visible')
    })

    $('.phone-icons-description .entry').on('mouseleave', function () {
        $('.phone-icons-image').removeClass('visible')
    })

    /* ========================== */
    /* 09 - contact form submit */
    /* ========================== */
    var formPopupTimeout
    $('.contact-form').on('submit', function () {
        clearTimeout(formPopupTimeout)

        $('.error-class').removeClass('error-class')
        var msg = 'The following fields should be filled:'
        var error = 0
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i)

        if ($.trim($('.contact-form input[name="name"]').val()) === '') { error = 1; $('.contact-form input[name="name"]').addClass('error-class'); msg = msg + '\n - Name' }
        if (!pattern.test($.trim($('.contact-form input[name="email"]').val()))) { error = 1; $('.contact-form input[name="email"]').addClass('error-class'); msg = msg + '\n - Email' }
        if ($.trim($('.contact-form textarea[name="text"]').val()) === '') { error = 1; $('.contact-form textarea[name="text"]').addClass('error-class'); msg = msg + '\n - Your Message' }

        if (error) {
            $('.form-popup .text').text(msg)
            $('.form-popup').fadeIn(300)
            formPopupTimeout = setTimeout(function () { $('.form-popup').fadeOut(300) }, 3000)
        } else {
            var url = 'send_mail.php'
            	var name = $.trim($('.contact-form input[name="name"]').val())
            	var email = $.trim($('.contact-form input[name="email"]').val())
            	var subject = $.trim($('.contact-form input[name="subject"]').val())
            	var department = $.trim($('.contact-form select[name="department"]').val())
            	var text = $.trim($('.contact-form textarea[name="text"]').val())
            	var mailto = $.trim($('.contact-form input[name="mailto"]').val())

            $.post(url, { name: name, email: email, subject: subject, department: department, text: text, mailto: mailto }, function (data) {
	        	$('.form-popup .text').text('Thank You for contacting us!')
                $('.form-popup').fadeIn(300)
                formPopupTimeout = setTimeout(function () { $('.form-popup').fadeOut(300) }, 3000)

	        	$('.contact-form').append('<input type="reset" class="reset-button"/>')
	        	$('.reset-button').click().remove()
            })
        }
	  	return false
    })

    $(document).on('focus', '.error-class', function () {
        $(this).removeClass('error-class')
    })

    $('.form-popup-close-layer').on('click', function () {
        clearTimeout(formPopupTimeout)
        $('.form-popup').fadeOut(300)
    })

    $('.mouse-icon').on('click', function () {
        $('body, html').animate({ 'scroll-top': winH })
    })

    $('.back-to-top').on('click', function () {
        $('body, html').animate({ 'scroll-top': 0 })
    })

})
