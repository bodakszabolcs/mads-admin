<?php

use App\Helpers\PdfHelper;
use App\Jobs\SendEmail;
use Illuminate\Support\Facades\Mail;

Route::match(['get'], '/test-email', function() {

    return  (new \App\Mail\CompanyQuestionEmail(\Modules\Campaig\Entities\CampaignCompanyQuestion::where('id',2)->first()))->render();


});
Route::match(['get'], '/request', function() {
    $r =\Modules\Campaig\Entities\CampaignCompanyRequest::where('id',2)->first();
        return $r->genereteRequestPDF(true);

});
Route::match(['get'], '/1-melleklet', function() {
    $contract =\Modules\Campaig\Entities\CampaignCompanyContract::where('id',8)->first();
    return $contract->genereteFirstMADSPDF();

});
Route::match(['get'], '/0-melleklet', function() {



    $contract =\Modules\Campaig\Entities\CampaignCompanyContract::where('id',8)->first();
    return $contract->genereteNullMADSPDF();

});
Route::match(['get'], '/2-melleklet', function() {


    $contract =\Modules\Campaig\Entities\CampaignCompanyContract::where('id',8)->first();
    return $contract->genereteSecondMADSPDF();
});
Route::match(['get'], '/szerzodes', function() {


    $pdf = new \Mpdf\Mpdf(PdfHelper::$config);
    $pdf->curlAllowUnsafeSslRequests = true;
    $pdf->debug = true;
    $contract =\Modules\Campaig\Entities\CampaignCompanyContract::where('id',8)->first();

    return $contract->genereteContractMADSPDF();
});

Route::match(['get'], '/xml/{id}', 'Controller@XML')->name('frontend_xml');
Route::any('/'.config('app.admin_url').'/{any?}', 'Controller@Admin')->where('any', '.*');
Route::any('/resize-image/{any?}', 'Controller@ImageRezize')->where('any', '.*');
Route::any('/{any?}', 'Controller@Frontend')->where('any', '.*');

