<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['api'],'namespace' => 'App\Http\Controllers\Auth'], function () {
    Route::post('/login','LoginController@login')->name("login");

    Route::post('/logout','LoginController@logout');
});
Route::group(['namespace' => 'App\Http\Controllers\StudentAuth'], function () {

    $this->get('/email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');
    $this->post('/email/resend', 'VerificationController@resend')->name('verification.resend');
});
Route::post('/student/refresh-token', function (Request $request) {
    $accessToken = $request->user()->createToken('access_token', ['access-api'], config('sanctum.expiration'));

    return ['token' => $accessToken->plainTextToken];
})->middleware([
    'auth:sanctum',
    'ability:'.'issue-access-token',
]);
Route::group(['middleware' => ['auth:sanctum','api'],'namespace' => 'App\Http\Controllers\StudentAuth'], function () {
    Route::get('/student/check-login', function (Request $request) {
        return response()->json("ok");
    });
    Route::post('/student/logout','LoginController@logout');

});
Route::group(['middleware' => ['student','api'],'namespace' => 'App\Http\Controllers\StudentAuth'], function () {

    Route::post('/student/login','LoginController@login');


    Route::post('/student/password/email','ForgotPasswordController@sendResetLinkEmail');
    Route::post('/student/password/reset', 'ResetPasswordController@reset')->name('password.reset');
    // Registration Routes...
    Route::post('/student/register', 'RegisterController@register');


});

