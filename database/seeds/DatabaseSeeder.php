<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\App::setLocale('hu');
        $this->call(UsersTableSeeder::class);
        $this->call(\Modules\Admin\Database\Seeders\AdminDatabaseSeeder::class);
        $this->call(\Modules\Blog\Database\Seeders\BlogDatabaseSeeder::class);
        $this->call(\Modules\Slider\Database\Seeders\SliderDatabaseSeeder::class);
        $this->call(\Modules\Category\Database\Seeders\CategoryDatabaseSeeder::class);
        $this->call(\Modules\Menu\Database\Seeders\MenuDatabaseSeeder::class);
        $this->call(\Modules\ModuleBuilder\Database\Seeders\ModuleBuilderDatabaseSeeder::class);
        $this->call(\Modules\Page\Database\Seeders\PageDatabaseSeeder::class);
        $this->call(\Modules\Role\Database\Seeders\RoleDatabaseSeeder::class);
        $this->call(\Modules\System\Database\Seeders\SystemDatabaseSeeder::class);
        $this->call(\Modules\Translation\Database\Seeders\TranslationDatabaseSeeder::class);
        $this->call(\Modules\User\Database\Seeders\UserDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\WebshopFrontendDatabaseSeeder::class);
       // $this->call(\Modules\WebshopFrontend\Database\Seeders\MenusTableSeeder::class);
      //  $this->call(\Modules\WebshopFrontend\Database\Seeders\MenuItemsTableSeeder::class);
        $this->call(\Modules\Color\Database\Seeders\ColorDatabaseSeeder::class);
        $this->call(\Modules\Pack\Database\Seeders\PackDatabaseSeeder::class);
        $this->call(\Modules\Type\Database\Seeders\TypeDatabaseSeeder::class);
        $this->call(\Modules\Product\Database\Seeders\ProductDatabaseSeeder::class);
        $this->call(\Modules\ProductVariation\Database\Seeders\ProductVariationDatabaseSeeder::class);
        $this->call(\Modules\ProductQuestion\Database\Seeders\ProductQuestionDatabaseSeeder::class);
        $this->call(\Modules\ProductRating\Database\Seeders\ProductRatingDatabaseSeeder::class);
        $this->call(\Modules\ProductCategory\Database\Seeders\ProductCategoryDatabaseSeeder::class);

$this->call(\Modules\OrderItem\Database\Seeders\OrderItemDatabaseSeeder::class);

$this->call(\Modules\Banner\Database\Seeders\BannerDatabaseSeeder::class);

$this->call(\Modules\Order\Database\Seeders\OrderDatabaseSeeder::class);

$this->call(\Modules\Order\Database\Seeders\OrderDatabaseSeeder::class);

$this->call(\Modules\Coupon\Database\Seeders\CouponDatabaseSeeder::class);

$this->call(\Modules\People\Database\Seeders\PeopleDatabaseSeeder::class);

$this->call(\Modules\Event\Database\Seeders\EventDatabaseSeeder::class);

$this->call(\Modules\Members\Database\Seeders\MembersDatabaseSeeder::class);

$this->call(\Modules\WorkCategory\Database\Seeders\WorkCategoryDatabaseSeeder::class);

$this->call(\Modules\Work\Database\Seeders\WorkDatabaseSeeder::class);

$this->call(\Modules\Feor\Database\Seeders\FeorDatabaseSeeder::class);

$this->call(\Modules\Language\Database\Seeders\LanguageDatabaseSeeder::class);

$this->call(\Modules\City\Database\Seeders\CityDatabaseSeeder::class);

$this->call(\Modules\PostSubject\Database\Seeders\PostSubjectDatabaseSeeder::class);

$this->call(\Modules\PostDelivery\Database\Seeders\PostDeliveryDatabaseSeeder::class);

$this->call(\Modules\PostDeliveryType\Database\Seeders\PostDeliveryTypeDatabaseSeeder::class);

$this->call(\Modules\Content\Database\Seeders\ContentDatabaseSeeder::class);

$this->call(\Modules\Content\Database\Seeders\ContentDatabaseSeeder::class);

$this->call(\Modules\PartnerLogo\Database\Seeders\PartnerLogoDatabaseSeeder::class);

$this->call(\Modules\Student\Database\Seeders\StudentDatabaseSeeder::class);

$this->call(\Modules\Nationality\Database\Seeders\NationalityDatabaseSeeder::class);

$this->call(\Modules\AreaType\Database\Seeders\AreaTypeDatabaseSeeder::class);

$this->call(\Modules\EducationLevel\Database\Seeders\EducationLevelDatabaseSeeder::class);

$this->call(\Modules\EducationArea\Database\Seeders\EducationAreaDatabaseSeeder::class);

$this->call(\Modules\School\Database\Seeders\SchoolDatabaseSeeder::class);

$this->call(\Modules\Attribute\Database\Seeders\AttributeDatabaseSeeder::class);

$this->call(\Modules\Country\Database\Seeders\CountryDatabaseSeeder::class);

$this->call(\Modules\Company\Database\Seeders\CompanyDatabaseSeeder::class);

$this->call(\Modules\ProjectStatus\Database\Seeders\ProjectStatusDatabaseSeeder::class);

$this->call(\Modules\Project\Database\Seeders\ProjectDatabaseSeeder::class);

$this->call(\Modules\Fixing\Database\Seeders\FixingDatabaseSeeder::class);

$this->call(\Modules\MonthlyPayment\Database\Seeders\MonthlyPaymentDatabaseSeeder::class);

$this->call(\Modules\Email\Database\Seeders\EmailDatabaseSeeder::class);

$this->call(\Modules\WorkLocation\Database\Seeders\WorkLocationDatabaseSeeder::class);

$this->call(\Modules\StuentPresent\Database\Seeders\StuentPresentDatabaseSeeder::class);

$this->call(\Modules\StudentPresent\Database\Seeders\StudentPresentDatabaseSeeder::class);

$this->call(\Modules\Payroll\Database\Seeders\PayrollDatabaseSeeder::class);

$this->call(\Modules\PostalIncome\Database\Seeders\PostalIncomeDatabaseSeeder::class);

$this->call(\Modules\PostalOutgoings\Database\Seeders\PostalOutgoingsDatabaseSeeder::class);

$this->call(\Modules\InvoiceTransporter\Database\Seeders\InvoiceTransporterDatabaseSeeder::class);

$this->call(\Modules\Invoice\Database\Seeders\InvoiceDatabaseSeeder::class);

$this->call(\Modules\MonthlyOrders\Database\Seeders\MonthlyOrdersDatabaseSeeder::class);

$this->call(\Modules\ProjectEmailSablon\Database\Seeders\ProjectEmailSablonDatabaseSeeder::class);

$this->call(\Modules\ImportantInfo\Database\Seeders\ImportantInfoDatabaseSeeder::class);

$this->call(\Modules\AutomaticEmail\Database\Seeders\AutomaticEmailDatabaseSeeder::class);

$this->call(\Modules\DownPayment\Database\Seeders\DownPaymentDatabaseSeeder::class);

$this->call(\Modules\OnlineStudentCard\Database\Seeders\OnlineStudentCardDatabaseSeeder::class);

$this->call(\Modules\Realization\Database\Seeders\RealizationDatabaseSeeder::class);

$this->call(\Modules\Campaig\Database\Seeders\CampaigDatabaseSeeder::class);

$this->call(\Modules\CampaignStatus\Database\Seeders\CampaignStatusDatabaseSeeder::class);

$this->call(\Modules\Request\Database\Seeders\RequestDatabaseSeeder::class);

$this->call(\Modules\Questions\Database\Seeders\QuestionsDatabaseSeeder::class);

$this->call(\Modules\Semester\Database\Seeders\SemesterDatabaseSeeder::class);

$this->call(\Modules\ProjectQuestion\Database\Seeders\ProjectQuestionDatabaseSeeder::class);

/* ModuleBuilderSeedArea *//* ModuleBuilderSeedAreaEnd */

        if (config('app.env') == 'local') {
            \Modules\User\Entities\User::unsetEventDispatcher();
            \Modules\Role\Entities\Role::unsetEventDispatcher();
            \Modules\Blog\Entities\Blog::unsetEventDispatcher();
            \Modules\Page\Entities\Page::unsetEventDispatcher();
            \Modules\Color\Entities\Color::unsetEventDispatcher();
            \Modules\Pack\Entities\Pack::unsetEventDispatcher();
            \Modules\Type\Entities\Type::unsetEventDispatcher();
            \Modules\Product\Entities\Product::unsetEventDispatcher();
            \Modules\ProductVariation\Entities\ProductVariation::unsetEventDispatcher();
            \Modules\ProductCategory\Entities\ProductCategory::unsetEventDispatcher();

\Modules\OrderItem\Entities\OrderItem::unsetEventDispatcher();

\Modules\Banner\Entities\Banner::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Order\Entities\Order::unsetEventDispatcher();

\Modules\Coupon\Entities\Coupon::unsetEventDispatcher();

\Modules\People\Entities\People::unsetEventDispatcher();

\Modules\Event\Entities\Event::unsetEventDispatcher();

\Modules\Members\Entities\Members::unsetEventDispatcher();

\Modules\WorkCategory\Entities\WorkCategory::unsetEventDispatcher();

\Modules\Work\Entities\Work::unsetEventDispatcher();

\Modules\Feor\Entities\Feor::unsetEventDispatcher();

\Modules\Language\Entities\Language::unsetEventDispatcher();

\Modules\City\Entities\City::unsetEventDispatcher();

\Modules\PostSubject\Entities\PostSubject::unsetEventDispatcher();

\Modules\PostDelivery\Entities\PostDelivery::unsetEventDispatcher();

\Modules\PostDeliveryType\Entities\PostDeliveryType::unsetEventDispatcher();

\Modules\Content\Entities\Content::unsetEventDispatcher();

\Modules\Content\Entities\Content::unsetEventDispatcher();

\Modules\PartnerLogo\Entities\PartnerLogo::unsetEventDispatcher();

\Modules\Student\Entities\Student::unsetEventDispatcher();

\Modules\Nationality\Entities\Nationality::unsetEventDispatcher();

\Modules\AreaType\Entities\AreaType::unsetEventDispatcher();

\Modules\EducationLevel\Entities\EducationLevel::unsetEventDispatcher();

\Modules\EducationArea\Entities\EducationArea::unsetEventDispatcher();

\Modules\School\Entities\School::unsetEventDispatcher();

\Modules\Attribute\Entities\Attribute::unsetEventDispatcher();

\Modules\Country\Entities\Country::unsetEventDispatcher();

\Modules\Company\Entities\Company::unsetEventDispatcher();

\Modules\ProjectStatus\Entities\ProjectStatus::unsetEventDispatcher();

\Modules\Project\Entities\Project::unsetEventDispatcher();

\Modules\Fixing\Entities\Fixing::unsetEventDispatcher();

\Modules\MonthlyPayment\Entities\MonthlyPayment::unsetEventDispatcher();

\Modules\Email\Entities\Email::unsetEventDispatcher();

\Modules\WorkLocation\Entities\WorkLocation::unsetEventDispatcher();

\Modules\StuentPresent\Entities\StuentPresent::unsetEventDispatcher();

\Modules\StudentPresent\Entities\StudentPresent::unsetEventDispatcher();

\Modules\Payroll\Entities\Payroll::unsetEventDispatcher();

\Modules\PostalIncome\Entities\PostalIncome::unsetEventDispatcher();

\Modules\PostalOutgoings\Entities\PostalOutgoings::unsetEventDispatcher();

\Modules\InvoiceTransporter\Entities\InvoiceTransporter::unsetEventDispatcher();

\Modules\Invoice\Entities\Invoice::unsetEventDispatcher();

\Modules\MonthlyOrders\Entities\MonthlyOrders::unsetEventDispatcher();

\Modules\ProjectEmailSablon\Entities\ProjectEmailSablon::unsetEventDispatcher();

\Modules\ImportantInfo\Entities\ImportantInfo::unsetEventDispatcher();

\Modules\AutomaticEmail\Entities\AutomaticEmail::unsetEventDispatcher();

\Modules\DownPayment\Entities\DownPayment::unsetEventDispatcher();

\Modules\OnlineStudentCard\Entities\OnlineStudentCard::unsetEventDispatcher();

\Modules\Realization\Entities\Realization::unsetEventDispatcher();

\Modules\Campaig\Entities\Campaig::unsetEventDispatcher();

\Modules\CampaignStatus\Entities\CampaignStatus::unsetEventDispatcher();

\Modules\Request\Entities\Request::unsetEventDispatcher();

\Modules\Questions\Entities\Questions::unsetEventDispatcher();

\Modules\Semester\Entities\Semester::unsetEventDispatcher();

\Modules\ProjectQuestion\Entities\ProjectQuestion::unsetEventDispatcher();

/* EventDispatcherArea *//* EventDispatcherAreaEnd */

            $user = factory(\Modules\User\Entities\User::class, 20)->create()
                ->each(function ($user) {
                    $user->shipping()->save(factory(\Modules\User\Entities\UserShipping::class)->make([
                        'user_id' => $user->id
                    ]));

                    $user->billing()->save(factory(\Modules\User\Entities\UserBilling::class)->make([
                        'user_id' => $user->id
                    ]));

    });

            $role = factory(\Modules\Role\Entities\Role::class, 8)->create();

            $blog = factory(\Modules\Blog\Entities\Blog::class, 20)->create()
                ->each(function ($b) {
                    $b->categories()->save(factory(\Modules\Category\Entities\Category::class)->make());
                    $b->tags()->save(factory(\Modules\Category\Entities\Tag::class)->make());
                });

            $model = factory(\Modules\Color\Entities\Color::class, 5)->create();
            $model = factory(\Modules\Slider\Entities\Slider::class, 5)->create();

            $model = factory(\Modules\Pack\Entities\Pack::class, 5)->create();

            $model = factory(\Modules\Type\Entities\Type::class, 5)->create();
            $model = factory(\Modules\ProductCategory\Entities\ProductCategory::class, 20)->create();
            foreach (\Modules\ProductCategory\Entities\ProductCategory::limit(10)->offset(10)->get()  as $c){
                $c->parent_id = rand(1,10);
                $c->save();
            }

            $model = factory(\Modules\Product\Entities\Product::class, 300)->create();
            foreach (\Modules\Product\Entities\Product::all() as $p){
                $p->categories()->sync(rand(1,20));
            }
            $model = factory(\Modules\ProductQuestion\Entities\ProductQuestion::class, 300)->create();
            $model = factory(\Modules\ProductRating\Entities\ProductRating::class, 300)->create();

            $model = factory(\Modules\ProductVariation\Entities\ProductVariation::class, 900)->create();
            foreach (\Modules\ProductVariation\Entities\ProductVariation::all() as $v){
                for($i = 0; $i< 5;$i++) {
                    $k = new \Modules\ProductVariation\Entities\ProductVariationImage();
                    $k->variation_id = $v->id;
                    $k->url = 'https://source.unsplash.com/800x800/?salt&'.rand(0,9999);
                    $k->save();
                }
            }


            /*$model = factory(\Modules\OrderItem\Entities\OrderItem::class, 5)->create();*/

            $model = factory(\Modules\Banner\Entities\Banner::class, 6)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Order\Entities\Order::class, 5)->create();

$model = factory(\Modules\Coupon\Entities\Coupon::class, 5)->create();

$model = factory(\Modules\People\Entities\People::class, 5)->create();

$model = factory(\Modules\Event\Entities\Event::class, 5)->create();

$model = factory(\Modules\Members\Entities\Members::class, 5)->create();

$model = factory(\Modules\WorkCategory\Entities\WorkCategory::class, 5)->create();

$model = factory(\Modules\Work\Entities\Work::class, 5)->create();

$model = factory(\Modules\Feor\Entities\Feor::class, 5)->create();

$model = factory(\Modules\Language\Entities\Language::class, 5)->create();

$model = factory(\Modules\City\Entities\City::class, 5)->create();

$model = factory(\Modules\PostSubject\Entities\PostSubject::class, 5)->create();

$model = factory(\Modules\PostDelivery\Entities\PostDelivery::class, 5)->create();

$model = factory(\Modules\PostDeliveryType\Entities\PostDeliveryType::class, 5)->create();

$model = factory(\Modules\Content\Entities\Content::class, 5)->create();

$model = factory(\Modules\Content\Entities\Content::class, 5)->create();

$model = factory(\Modules\PartnerLogo\Entities\PartnerLogo::class, 5)->create();

$model = factory(\Modules\Student\Entities\Student::class, 5)->create();

$model = factory(\Modules\Nationality\Entities\Nationality::class, 5)->create();

$model = factory(\Modules\AreaType\Entities\AreaType::class, 5)->create();

$model = factory(\Modules\EducationLevel\Entities\EducationLevel::class, 5)->create();

$model = factory(\Modules\EducationArea\Entities\EducationArea::class, 5)->create();

$model = factory(\Modules\School\Entities\School::class, 5)->create();

$model = factory(\Modules\Attribute\Entities\Attribute::class, 5)->create();

$model = factory(\Modules\Country\Entities\Country::class, 5)->create();

$model = factory(\Modules\Company\Entities\Company::class, 5)->create();

$model = factory(\Modules\ProjectStatus\Entities\ProjectStatus::class, 5)->create();

$model = factory(\Modules\Project\Entities\Project::class, 5)->create();

$model = factory(\Modules\Fixing\Entities\Fixing::class, 5)->create();

$model = factory(\Modules\MonthlyPayment\Entities\MonthlyPayment::class, 5)->create();

$model = factory(\Modules\Email\Entities\Email::class, 5)->create();

$model = factory(\Modules\WorkLocation\Entities\WorkLocation::class, 5)->create();

$model = factory(\Modules\StuentPresent\Entities\StuentPresent::class, 5)->create();

$model = factory(\Modules\StudentPresent\Entities\StudentPresent::class, 5)->create();

$model = factory(\Modules\Payroll\Entities\Payroll::class, 5)->create();

$model = factory(\Modules\PostalIncome\Entities\PostalIncome::class, 5)->create();

$model = factory(\Modules\PostalOutgoings\Entities\PostalOutgoings::class, 5)->create();

$model = factory(\Modules\InvoiceTransporter\Entities\InvoiceTransporter::class, 5)->create();

$model = factory(\Modules\Invoice\Entities\Invoice::class, 5)->create();

$model = factory(\Modules\MonthlyOrders\Entities\MonthlyOrders::class, 5)->create();

$model = factory(\Modules\ProjectEmailSablon\Entities\ProjectEmailSablon::class, 5)->create();

$model = factory(\Modules\ImportantInfo\Entities\ImportantInfo::class, 5)->create();

$model = factory(\Modules\AutomaticEmail\Entities\AutomaticEmail::class, 5)->create();

$model = factory(\Modules\DownPayment\Entities\DownPayment::class, 5)->create();

$model = factory(\Modules\OnlineStudentCard\Entities\OnlineStudentCard::class, 5)->create();

$model = factory(\Modules\Realization\Entities\Realization::class, 5)->create();

$model = factory(\Modules\Campaig\Entities\Campaig::class, 5)->create();

$model = factory(\Modules\CampaignStatus\Entities\CampaignStatus::class, 5)->create();

$model = factory(\Modules\Request\Entities\Request::class, 5)->create();

$model = factory(\Modules\Questions\Entities\Questions::class, 5)->create();

$model = factory(\Modules\Semester\Entities\Semester::class, 5)->create();

$model = factory(\Modules\ProjectQuestion\Entities\ProjectQuestion::class, 5)->create();

/* FakerArea *//* FakerAreaEnd */

        }
    }
}
