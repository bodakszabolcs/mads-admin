<?php

use Modules\User\Entities\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $token = Str::random(60);

        $user = User::create([
            'name' => 'Liquid Admin',
            'firstname' => 'Liquid',
            'lastname' => 'Admin',
            'email' => config('app.superuser_email'),
            'email_verified_at' => now(),
            'password' => Hash::make('aA123456')
        ]);
    }
}
