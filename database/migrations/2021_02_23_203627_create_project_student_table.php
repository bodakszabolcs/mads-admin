<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectStudentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

	    Schema::defaultStringLength(191);

	    Schema::create('project_students', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->bigInteger('project_id')->comment('Project');
		    $table->bigInteger('student_id')->comment('Diák');
		    $table->timestamp('viewed')->nullable();
		    $table->integer('status_id')->comment('Állapot')->nullable();
		    $table->text('comment')->comment('Megjegyzés');
		    $table->timestamps();
		    $table->index(['project_id','student_id']);
		   // $table->foreign('id')->references('student_id')->on('students');
		   // $table->foreign('project_id')->references('id')->on('projects');
	    });
	    Schema::create('project_task', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->bigInteger('project_id')->comment('Project')->unsigned();
		    $table->integer('completed');
		    $table->text('description')->nullable();
		    $table->date('deadline')->nullable();
		    $table->index(['project_id']);
		 //   $table->foreign('id')->references('project_id')->on('projects');
		    $table->timestamps();

	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_students', function (Blueprint $table) {
            //
        });
    }
}
