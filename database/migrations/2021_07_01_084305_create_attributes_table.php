<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_languages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('language_id')->default(null);
            $table->bigInteger('student_id')->default(null);;
            $table->string('value')->default(null);;
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('student_work_safety', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type')->default(null);
            $table->bigInteger('student_id')->default(null);;
            $table->date('expire')->default(null);
            $table->string('value')->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
