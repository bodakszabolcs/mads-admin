<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_schools', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('school_id');
            $table->integer('student_id');
            $table->integer('education_type');
            $table->date('start')->nullable();
            $table->date('end')->nullable();
	        $table->integer('education_area_id');
	        $table->integer('education_level_id');
	        $table->integer('updated');
            $table->timestamps();
            $table->softDeletes();
        });
	    Schema::create('student_cards', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->integer('school_id');
		    $table->integer('student_id');
		    $table->text('student_card');
		    $table->text('temporary_student_card');
		    $table->date('temporary_student_card_expire');
		    $table->integer('updated');
		    $table->timestamps();
		    $table->softDeletes();
	    });
	    Schema::create('student_certificates', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->integer('school_id');
		    $table->integer('student_id');
		    $table->date('start');
		    $table->date('end');
		    $table->string('document');
		    $table->integer('updated');
		    $table->timestamps();
		    $table->softDeletes();
	    });
	    Schema::create('student_documents', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->integer('student_id');
		    $table->integer('document_type_id');
		    $table->text('file');
		    $table->text('name');
		    $table->integer('updated');
		    $table->timestamps();
		    $table->softDeletes();
	    });
	    Schema::create('student_attributes', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->integer('student_id');
		    $table->integer('attribute_id');
		    $table->text('value');
		    $table->integer('updated');
		    $table->timestamps();
		    $table->softDeletes();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_schools');
        Schema::dropIfExists('student_documents');
        Schema::dropIfExists('student_attributes');
        Schema::dropIfExists('student_certificates');
        Schema::dropIfExists('student_cards');
    }
}
