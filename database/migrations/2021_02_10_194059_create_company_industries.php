<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyIndustries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_industries', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id')->nullable();
            $table->bigInteger('site_id')->nullable();
            $table->bigInteger('user_id')->nullable();
            $table->bigInteger('feor_id')->nullable();
            $table->bigInteger('category_id')->nullable();
            $table->text('name')->nullable();
            $table->text('instructor')->nullable();
            $table->text('work_schedule')->nullable();
	        $table->float('divisor')->nullable();
	        $table->float('multiplier')->nullable();
            $table->integer('price_hour')->nullable();
            $table->integer('price_company_hourly')->nullable();
            $table->float('price_company_fix')->nullable();
	        $table->float('night_bonus')->nullable();
	        $table->float('night_bonus_company')->nullable();
	        $table->time('night_bonus_from')->nullable();
	        $table->time('night_bonus_to')->nullable();

	        $table->float('saturday_bonus')->nullable();
	        $table->float('saturday_bonus_company')->nullable();
	        $table->time('saturday_bonus_from')->nullable();
	        $table->time('saturday_bonus_to')->nullable();

	        $table->float('sunday_bonus')->nullable();
	        $table->float('sunday_bonus_company')->nullable();
	        $table->time('sunday_bonus_from')->nullable();
	        $table->time('sunday_bonus_to')->nullable();

	        $table->float('festive_bonus')->nullable();
	        $table->float('festive_bonus_company')->nullable();
	        $table->time('festive_bonus_from')->nullable();
	        $table->time('festive_bonus_to')->nullable();


	        $table->integer('break')->nullable();
	        $table->time('break_from')->nullable();
	        $table->time('break_to')->nullable();
	        $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_industries');
    }
}
