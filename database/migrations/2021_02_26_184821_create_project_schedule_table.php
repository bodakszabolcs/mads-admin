<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_schedules', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('project_id');
            $table->bigInteger('industry_id');
            $table->bigInteger('student_id');
            $table->date('schedule_date');
            $table->time('start');
            $table->time('end');
            $table->string('department')->nullable();
            $table->tinyInteger('presence')->nullable();
            $table->bigInteger('presence_user_id')->nullable();
            $table->dateTime('presence_date')->nullable();
            $table->tinyInteger('down')->nullable();
            $table->string('down_message')->nullable();
            $table->tinyInteger('company_show')->nullable();
            $table->timestamps();
        });
	    Schema::table('projects', function (Blueprint $table) {
		    $table->text('departments')->nullable();
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_schedule');
    }
}
