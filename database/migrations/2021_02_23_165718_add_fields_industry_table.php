<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsIndustryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('company_industries', function (Blueprint $table) {
	        $table->integer('price_hour')->nullable();
	        $table->integer('price_company_hour')->nullable();
	        $table->integer('price_fix')->nullable();
	        $table->integer('price_company_fix')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('company_industries', function (Blueprint $table) {

        });
    }
}
